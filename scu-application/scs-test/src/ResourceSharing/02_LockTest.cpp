/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <gtest/gtest.h>
#include <ResourceSharing/MessageLockRequest.h>
#include <ResourceSharing/Lock.h>

namespace ResourceSharing {

class LockTest : public testing::Test {
 public:
  LockTest() {}
};

TEST_F(LockTest,  AcquireLock) {
  auto lock = Lock(1);
  lock.AddSite(13);

  auto message = MessageLockRequest(
    1,
    "this",
    "that",
    TimeStamp(1, 2));

  lock.OnMessageReceived(&message);
}

}  // namespace ResourceSharing
