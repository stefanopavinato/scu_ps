/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <ResourceSharing/MessageLockRequest.h>
#include <ResourceSharing/Lock.h>
#include <ResourceSharing/LockHandler.h>
#include <gtest/gtest.h>

namespace ResourceSharing {

TEST(MessageVisitorTest, asdf) {
  std::unique_ptr<MessageBase> msg = std::make_unique<MessageLockRequest>(
    13,
    "sender",
    "receiver",
    TimeStamp(1, 2));
  auto lock = Lock(13);
  auto lockHandler = LockHandler();
  lock.OnMessageReceived(msg.get());

  lockHandler.AddLock(std::make_unique<Lock>(13));
  lockHandler.OnMessageReceived(msg.get());

  EXPECT_EQ(true, msg->Accept(&lock));
  EXPECT_EQ(true, msg->Accept(&lockHandler));
}

}  // namespace ResourceSharing
