/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <gtest/gtest.h>
#include <ResourceSharing/TimeStamp.h>

namespace ResourceSharing {

TEST(TimeStampTest,  Compare) {
  auto ta = TimeStamp(1, 1);
  auto tb = TimeStamp(1, 1);
  auto tc = TimeStamp(1, 2);
  auto td = TimeStamp(2, 1);

  EXPECT_EQ(true, ta == tb);
  EXPECT_NE(true, ta == tc);
  EXPECT_NE(true, ta == td);

  EXPECT_NE(true, ta != tb);
  EXPECT_EQ(true, ta != tc);
  EXPECT_EQ(true, ta != td);

  EXPECT_NE(true, ta < tb);
  EXPECT_EQ(true, ta < tc);
  EXPECT_EQ(true, ta < td);

  EXPECT_NE(true, ta > tb);
  EXPECT_NE(true, ta > tc);
  EXPECT_NE(true, ta > td);
}

}  // namespace ResourceSharing
