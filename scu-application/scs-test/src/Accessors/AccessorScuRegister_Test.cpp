/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <gtest/gtest.h>
#include <Logger/LoggerDebug.h>
#include <SystemModules/Framework/Accessors/AccessorScuRegister.h>

namespace SystemModules::Framework::Accessors {

TEST(AccessorScuRegister_Test, StaticRead) {
  auto logger = Logger::LoggerDebug(Types::AlertSeverity::Enum::DEBUG);
  auto a = AccessorScuRegister<uint32_t>::Read(
    &logger,
    1,
    0,
    8);
  ASSERT_NE(std::experimental::nullopt, a);
}

}  // namespace SystemModules::Framework::Accessors
