/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <gtest/gtest.h>
#include <Logger/LoggerDebug.h>
#include <SystemModules/Framework/Accessors/AccessorScuRegisterVector.h>

namespace SystemModules::Framework::Accessors {

TEST(AccessorScuRegisterVector_Test, StaticRead) {
  auto logger = Logger::LoggerDebug(Types::AlertSeverity::Enum::DEBUG);
  auto a = AccessorScuRegisterVector<std::vector<uint32_t>>::Read(
    &logger,
    1,
    10);
  ASSERT_NE(std::experimental::nullopt, a);
  ASSERT_EQ(10, a.value().size());
  for (const auto &value : a.value()) {
    std::cout << value << " ";
  }
}

}  // namespace SystemModules::Framework::Accessors
