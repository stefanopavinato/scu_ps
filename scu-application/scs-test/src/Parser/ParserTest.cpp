/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <gtest/gtest.h>
#include <Utils/Parser/BooleanEquationParser.h>
#include <Utils/Parser/TokensParser.h>

class ParserTest :
  public testing::Test {
 protected:
  ParserTest() {
  }

  std::unordered_map<std::string, bool const *> items;
};

TEST_F(ParserTest, asdf) {
  auto a = Utils::Parser::TokensParser::Parse("A && !(B || C) || D");
  bool valA = false;
  bool valB = false;
  bool valC = false;
  bool valD = false;

  items.emplace("A", &valA);
  items.emplace("B", &valB);
  items.emplace("C", &valC);
  items.emplace("D", &valD);

  auto b = Utils::Parser::BooleanEquationParser::Parse(*a.get(), items);

  EXPECT_EQ(false, b.value().get()->Evaluate());
  valD = true;
  EXPECT_EQ(true, b.value().get()->Evaluate());
  valA = true;
  valD = false;
  EXPECT_EQ(true, b.value().get()->Evaluate());
// Test
/*
loggerHandler.Debug("debug");
loggerHandler.Info("info");
loggerHandler.Warning("warning");
loggerHandler.Error("error");
*/
}
