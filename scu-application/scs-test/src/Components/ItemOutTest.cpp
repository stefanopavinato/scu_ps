/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <gtest/gtest.h>
#include <SystemModules/Framework/Actions/ActionTemplate.h>
#include <SystemModules/Framework/Components/ItemOut.h>
#include <SystemModules/Framework/Builder/Accessors/WriteAccessorBuilder.h>
#include <SystemModules/Framework/Builder/Actions/ActionBuilder.h>

/*
class ItemOutTest:
    public SystemModules::Framework::Components::ItemOut<Types::Value<int32_t>>,
    public testing::Test {
 protected:
  ItemOutTest()
      : ItemOut<Types::Value<int32_t>>(
      nullptr,
      "Name",
      "Description",
      std::chrono::milliseconds(0),
      false,
      Types::Value<int32_t>(
          100,
          Types::Unit::Enum::NONE,
          Types::UnitPrefix::Enum::NONE)) {
  }
};

TEST_F(ItemOutTest, UpdateInterval) {
}

TEST_F(ItemOutTest, ForceValue) {
  auto staticValue = Types::Value<int32_t>();
  staticValue.SetValue(100);
  auto forceValue = Types::Value<int32_t>();
  forceValue.SetValue(200);
  std::chrono::steady_clock::time_point timePoint;
  // Set action
  CreateAction().StaticValue(staticValue);
  // Test initial value
  ProcessOutputs(timePoint);
  EXPECT_EQ(staticValue, *GetValue());
  // Test forced value
  ASSERT_EQ(true, ForceValue(forceValue));
  ProcessOutputs(timePoint);
  EXPECT_EQ(forceValue, *GetValue());
  // Test released value
  ReleaseValueForced();
  ProcessOutputs(timePoint);
  EXPECT_EQ(staticValue, *GetValue());
}

TEST_F(ItemOutTest, asdf) {
  std::chrono::steady_clock::time_point timePoint;
  (void) timePoint;
}
*/
