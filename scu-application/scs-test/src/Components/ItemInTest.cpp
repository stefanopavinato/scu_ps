/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <gtest/gtest.h>
#include <SystemModules/Framework/Accessors/AccessorTemplate.h>
#include <SystemModules/Framework/MonitoringFunctions/MFTemplate.h>
#include <SystemModules/Framework/Components/ItemIn.h>
#include <SystemModules/Framework/Builder/Accessors/ReadAccessorBuilder.h>
#include <SystemModules/Framework/Builder/MonitoringFunctions/MFBuilder.h>

class ItemInTest :
  public SystemModules::Framework::Components::ItemIn<Types::Value<int32_t>>,
  public testing::Test {
 protected:
  ItemInTest()
    : SystemModules::Framework::Components::ItemIn<Types::Value<int32_t>>(
    nullptr,
    "PWM1",
    "Sets the speed of the fan",
    Types::ValueGroup::Enum::SPEED,
    std::chrono::milliseconds(0)) {
  }
};

TEST_F(ItemInTest, UpdateInterval) {
  // Forcing of values
}

TEST_F(ItemInTest, ParentAlertSeverity) {
  // Behaviour in case of an external error
}

TEST_F(ItemInTest, DISABLED_ForceValue) {
  auto staticValue = Types::Value<int32_t>();
  staticValue.SetValue(100);
  auto forceValue = Types::Value<int32_t>();
  forceValue.SetValue(200);
  auto parentAlertSeverity = Types::AlertSeverity(Types::AlertSeverity::Enum::OK);
  std::chrono::steady_clock::time_point timePoint;
  // Set hardware accessor
  CreateAccessor().InternalStaticLocation(staticValue);
  // Test initial value
  ProcessInputs(parentAlertSeverity, timePoint);
  EXPECT_EQ(staticValue, *GetValue());
  // Test forced value
  ASSERT_EQ(true, ForceValue(forceValue));
  ProcessInputs(parentAlertSeverity, timePoint);
  EXPECT_EQ(forceValue, *GetValue());
  // Test released value
  ReleaseValueForced();
  ProcessInputs(parentAlertSeverity, timePoint);
  EXPECT_EQ(staticValue, *GetValue());
}

TEST_F(ItemInTest, DISABLED_AlertSeverity) {
  auto staticValue = Types::Value<int32_t>();
  staticValue.SetValue(100);
  auto forceValue = Types::Value<int32_t>();
  auto parentAlertSeverity = Types::AlertSeverity(Types::AlertSeverity::Enum::OK);
  std::chrono::steady_clock::time_point timePoint;
  // Set hardware accessor
  CreateAccessor().InternalStaticLocation(staticValue);
  // Add notice to value
//  CreateMonitoringFunction().IsLowerThan(
//      Types::AlertSeverity::Enum::NOTICE,
//      1,
//      std::chrono::milliseconds(0),
//      std::chrono::milliseconds(0),
//      Types::Value<int32_t>(
//          50,
//          Types::Unit::Enum::NONE,
//          Types::UnitPrefix::Enum::NONE)).Add();
//  // Add warning to value
//  CreateMonitoringFunction().IsLowerThan(
//      Types::AlertSeverity::Enum::WARNING,
//      2,
//      std::chrono::milliseconds(0),
//      std::chrono::milliseconds(0),
//      Types::Value<int32_t>(
//          40,
//          Types::Unit::Enum::NONE,
//          Types::UnitPrefix::Enum::NONE)).Add();
//  // Add error to value
//  CreateMonitoringFunction().IsLowerThan(
//      Types::AlertSeverity::Enum::ERROR,
//      3,
//      std::chrono::milliseconds(0),
//      std::chrono::milliseconds(0),
//      Types::Value<int32_t>(
//          30,
//          Types::Unit::Enum::NONE,
//          Types::UnitPrefix::Enum::NONE)).Add();

  // Test value in case everything is ok
  ProcessInputs(parentAlertSeverity, timePoint += std::chrono::milliseconds(1000));
  EXPECT_EQ(Types::AlertSeverity::Enum::OK, ComponentStatus()->AlertSeverity().GetEnum());

  // Test value in case a notice should be activated
  forceValue.SetValue(45);
  ASSERT_EQ(true, ForceValue(forceValue));
  ProcessInputs(parentAlertSeverity, timePoint += std::chrono::milliseconds(1000));
  EXPECT_EQ(Types::AlertSeverity::Enum::NOTICE, ComponentStatus()->AlertSeverity().GetEnum());

  // Test value in case a warning should be activated
  forceValue.SetValue(35);
  ASSERT_EQ(true, ForceValue(forceValue));
  ProcessInputs(parentAlertSeverity, timePoint += std::chrono::milliseconds(1000));
  EXPECT_EQ(Types::AlertSeverity::Enum::WARNING, ComponentStatus()->AlertSeverity().GetEnum());

  // Test value in case an error should be activated
  forceValue.SetValue(25);
  ASSERT_EQ(true, ForceValue(forceValue));
  ProcessInputs(parentAlertSeverity, timePoint += std::chrono::milliseconds(1000));
  EXPECT_EQ(Types::AlertSeverity::Enum::ERROR, ComponentStatus()->AlertSeverity().GetEnum());

  // Test value in case an error is not reset
  forceValue.SetValue(35);
  ASSERT_EQ(true, ForceValue(forceValue));
  ProcessInputs(parentAlertSeverity, timePoint += std::chrono::milliseconds(1000));
  EXPECT_EQ(Types::AlertSeverity::Enum::ERROR, ComponentStatus()->AlertSeverity().GetEnum());

  // Test value in case an error is reset
  ResetMonitoringFunctions();
  ProcessInputs(parentAlertSeverity, timePoint += std::chrono::milliseconds(1000));
  EXPECT_EQ(Types::AlertSeverity::Enum::WARNING, ComponentStatus()->AlertSeverity().GetEnum());
}

TEST_F(ItemInTest, DISABLED_Delay) {
  auto staticValue = Types::Value<int32_t>();
  staticValue.SetValue(100);
  auto forceValue = Types::Value<int32_t>();
  auto parentAlertSeverity = Types::AlertSeverity(Types::AlertSeverity::Enum::OK);
  std::chrono::steady_clock::time_point timePoint;
  // Set hardware accessor
  CreateAccessor().InternalStaticLocation(staticValue);
  // Add notice to value
//  CreateMonitoringFunction().IsLowerThan(
//      Types::AlertSeverity::Enum::NOTICE,
//      1,
//      std::chrono::milliseconds(1000),
//      std::chrono::milliseconds(1000),
//      Types::Value<int32_t>(
//          50,
//          Types::Unit::Enum::NONE,
//          Types::UnitPrefix::Enum::NONE)).Add();


  // Set value and update monitoring functions
  forceValue.SetValue(45);
  ASSERT_EQ(true, ForceValue(forceValue));
  ProcessInputs(parentAlertSeverity, timePoint += std::chrono::milliseconds(0));
  EXPECT_EQ(Types::AlertSeverity::Enum::OK, ComponentStatus()->AlertSeverity().GetEnum());

  // Update monitoring functions before on timer has elapsed
  ProcessInputs(parentAlertSeverity, timePoint += std::chrono::milliseconds(999));
  EXPECT_EQ(Types::AlertSeverity::Enum::OK, ComponentStatus()->AlertSeverity().GetEnum());

  // Update monitoring functions when on timer has elapsed
  ProcessInputs(parentAlertSeverity, timePoint += std::chrono::milliseconds(1));
  EXPECT_EQ(Types::AlertSeverity::Enum::NOTICE, ComponentStatus()->AlertSeverity().GetEnum());

  // Set value and update monitoring functions
  ReleaseValueForced();
  ProcessInputs(parentAlertSeverity, timePoint += std::chrono::milliseconds(1000));
  EXPECT_EQ(Types::AlertSeverity::Enum::NOTICE, ComponentStatus()->AlertSeverity().GetEnum());

  // Update monitoring functions before on timer has elapsed
  ProcessInputs(parentAlertSeverity, timePoint += std::chrono::milliseconds(999));
  EXPECT_EQ(Types::AlertSeverity::Enum::NOTICE, ComponentStatus()->AlertSeverity().GetEnum());

  // Update monitoring functions when on timer has elapsed
  ProcessInputs(parentAlertSeverity, timePoint += std::chrono::milliseconds(1));
  EXPECT_EQ(Types::AlertSeverity::Enum::OK, ComponentStatus()->AlertSeverity().GetEnum());
}
