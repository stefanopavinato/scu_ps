/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <gtest/gtest.h>
#include <SystemModules/Framework/Actions/ActionStaticValue.h>

// namespace Actions {

template<class T>
class ActionStaticTest :
  public SystemModules::Framework::Actions::ActionStaticValue<T>,
  public testing::Test {
 protected:
  /*
  ActionStaticTest()
      : SystemModules::Framework::Actions::StaticValue<T>(13) {
  }
  */
/*
  SystemModules::ModuleHandler *handler;

  SystemModules::Framework::Components::ItemOut<double> speed_self;
  SystemModules::Framework::Components::ItemOut<double> speed_other1;
  SystemModules::Framework::Components::ItemOut<double> speed_other2;

  SystemModules::Framework::Components::ItemOut<bool> enable_self;
  SystemModules::Framework::Components::ItemOut<bool> enable_other1;
  SystemModules::Framework::Components::ItemOut<bool> enable_other2;
  */
};

/*
typedef testing::Types<uint32_t, uint16_t, uint8_t, int32_t, int16_t, int8_t, double, float, bool> MyTypes;

TYPED_TEST_SUITE(ActionStaticTest, MyTypes,);  // NOLINT

TYPED_TEST(ActionStaticTest, Test1) {
  EXPECT_EQ(true, true);
}

TYPED_TEST(ActionStaticTest, Test2) {
  EXPECT_EQ(true, true);
}
*/

// }  // namespace Actions
