/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <memory>
#include <gtest/gtest.h>
#include <SystemModules/Framework/Components/ItemInOut.h>
#include <SystemModules/Framework/Actions/ActionOperatingTimeCounter.h>

namespace SystemModules::Framework::Actions {

class ActionOperatingTimeCounterTest :
  public testing::Test {
 protected:
  ActionOperatingTimeCounterTest()
    : alertSeverity(Types::AlertSeverity::Enum::OK), valueIn(std::chrono::minutes(0)),
      valueOut(std::chrono::minutes(0)), timePoint(std::chrono::steady_clock::time_point::min()),
      actionOperatingTime(std::make_unique<ActionOperatingTimeCounter>(
        &alertSeverity,
        &valueIn,
        timePoint)) {
  }

  Types::AlertSeverity alertSeverity;

  std::chrono::minutes valueIn;

  std::chrono::minutes valueOut;

  std::chrono::steady_clock::time_point timePoint;

  std::unique_ptr<ActionOperatingTimeCounter> actionOperatingTime;
};

TEST_F(ActionOperatingTimeCounterTest, AlertSeverity) {
  alertSeverity.SetEnum(Types::AlertSeverity::Enum::OK);
  ASSERT_EQ(true, actionOperatingTime->Update(timePoint, &valueOut));
  alertSeverity.SetEnum(Types::AlertSeverity::Enum::WARNING);
  ASSERT_EQ(false, actionOperatingTime->Update(timePoint, &valueOut));
}

TEST_F(ActionOperatingTimeCounterTest, DISABLED_Update) {
  valueIn = std::chrono::minutes(100);
  ASSERT_EQ(true, actionOperatingTime->Update(timePoint += std::chrono::minutes(5), &valueOut));
  EXPECT_EQ(valueIn.count(), valueOut.count());
  ASSERT_EQ(true, actionOperatingTime->Update(timePoint += std::chrono::minutes(1), &valueOut));
  EXPECT_EQ(valueIn.count(), valueOut.count());
  ASSERT_EQ(true, actionOperatingTime->Update(timePoint += std::chrono::minutes(3), &valueOut));
  EXPECT_EQ(valueIn.count(), valueOut.count());
  ASSERT_EQ(true, actionOperatingTime->Update(timePoint += std::chrono::minutes(0), &valueOut));
  EXPECT_EQ(valueIn.count(), valueOut.count());
}

}  // namespace SystemModules::Framework::Actions
