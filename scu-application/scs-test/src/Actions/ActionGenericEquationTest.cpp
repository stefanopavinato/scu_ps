/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <memory>
#include <gtest/gtest.h>
#include <SystemModules/Framework/Components/ItemIn.h>
#include <SystemModules/Framework/Actions/ActionGenericBooleanEquation.h>

namespace SystemModules::Framework::Actions {
class ActionGenericEquationTest : public testing::Test {
 public:
  ActionGenericEquationTest() {
    action_ = std::make_unique<ActionGenericBooleanEquation>(
      "A && B",
      Types::AlertSeverity::Enum::ERROR);
  }

  ActionGenericBooleanEquation *Action() {
    return action_.get();
  }

 private:
  std::unique_ptr<ActionGenericBooleanEquation> action_;
};

TEST_F(ActionGenericEquationTest, DISABLED_TEst) {
  bool result;
  auto itemA = std::make_unique<Components::ItemIn<bool>>(
    nullptr,
    "itemA",
    "",
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(1));
  auto itemB = std::make_unique<Components::ItemIn<bool>>(
    nullptr,
    "itemB",
    "",
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(1));
  Action()->AddVariable("A", &*itemA);
  Action()->AddVariable("B", &*itemB);
  Action()->Setup();

  EXPECT_EQ(true, this->Action()->Update(std::chrono::steady_clock::now(), &result));
  EXPECT_EQ(true, result);
}
}  // namespace SystemModules::Framework::Actions
