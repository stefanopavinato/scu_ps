/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <cstdint>
#include "ResourceSharing/TimeStamp.h"
#include "ResourceSharing/LockStatus.h"
#include "MessageBase.h"

namespace Messages {
class MessageLock : public MessageBase {
 public:
  MessageLock(
    const uint32_t &lockId,
    const ResourceSharing::TimeStamp &timeStamp,
    const ResourceSharing::LockStatus & lockStatus);

  MessageLock(
    const ResourceSharing::Address &sender,
    const ResourceSharing::Address &receiver,
    const uint32_t &lockId,
    const ResourceSharing::TimeStamp &timeStamp,
    const ResourceSharing::LockStatus & lockStatus);

  bool Accept(MessageVisitorBase *visitor) override;

  [[nodiscard]] uint32_t GetLockId() const;

  [[nodiscard]] ResourceSharing::LockStatus GetLockStatus() const;

  [[nodiscard]] ResourceSharing::TimeStamp GetTimeStamp() const;

 private:
  const uint32_t lockId_;

  const ResourceSharing::LockStatus lockStatus_;

  const ResourceSharing::TimeStamp timeStamp_;
};
}  // namespace Messages
