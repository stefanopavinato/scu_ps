/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Logger/ILogger.h"
#include "Messages/Visitor/VisitorBase.h"
#include "SystemModules/Framework/Components/Component.h"
#include "SystemModules/Framework/Components/IComponent.h"
#include "MessageItem.h"
#include "Message.h"
#include "MessageVisitorBase.h"
#include "MessageHandler.h"

namespace Messages {

MessageHandler::MessageHandler(Logger::ILogger *logger)
  : logger_(logger), moduleHandler_(nullptr), clientHandler_(nullptr) {
}

bool MessageHandler::Initialize(
  SystemModules::Framework::Components::IComponent *moduleHandler,
  Messages::MessageVisitorBase *clientHandler) {
  if (moduleHandler == nullptr) {
    logger_->Error("Module handler not set");
    return false;
  }
  if (clientHandler == nullptr) {
    logger_->Error("Client handler not set");
    return false;
  }
  moduleHandler_ = moduleHandler;
  clientHandler_ = clientHandler;
  return true;
}

bool MessageHandler::OnMessageReceived(Messages::MessageBase *message) {
  return message->Accept(this);
}

bool MessageHandler::Visit(Message *message) {
  // Test if there is already an error
  if ((message->MessageStatus()->GetValue() != Messages::Types::MessageStatus::Enum::OK) ||
      (message->DataStatus()->GetValue() != Messages::Types::DataStatus::Enum::OK)) {
    // Return the message to the client handler for further processing
    clientHandler_->OnMessageReceived(message);
    return false;
  }
  // Process all message items
  for (const auto &messageItem : message->GetMessageItems()) {
    // Get component
    auto component = moduleHandler_->GetComponent(messageItem->GetPath());
    if (!component) {
      logger_->Error(std::string("Item not found: ").append(messageItem->GetName()));
      messageItem->ItemState()->SetValue(
        Types::MessageItemState::Enum::COMPONENT_NOT_FOUND);
      message->DataStatus()->SetValue(
        Messages::Types::DataStatus::Enum::INFORMATION_NOT_AVAILABLE);
      continue;
      // break;
    }
    // Visit component
    if (!messageItem->Visit(component)) {
      logger_->Error(std::string("Failed to visit component: ").append(messageItem->GetName()));
// gasi debug only
//      message->DataStatus()->SetValue(
//          Messages::Types::DataStatus::Enum::INFORMATION_NOT_AVAILABLE);
      continue;
    }
  }
  // Send message
  return clientHandler_->OnMessageReceived(message);
}
}  // namespace Messages
