/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <list>
#include <memory>
#include "ComponentBase.h"

namespace Messages {
namespace Components {
class MessageContainer : ComponentBase {
 public:
  ~MessageContainer() override = default;

 private:
  std::list<std::unique_ptr<ComponentBase>> components;
};
}  // namespace Components
}  // namespace Messages
