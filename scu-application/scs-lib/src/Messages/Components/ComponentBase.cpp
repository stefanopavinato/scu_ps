/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "ComponentBase.h"

namespace Messages {
namespace Components {
std::string ComponentBase::GetName() {
  return name;
}
}  // namespace Components
}  // namespace Messages
