/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <string>
#include "Messages/Visitor/IVisitor.h"

namespace Messages {
namespace Components {
class ComponentBase : public Messages::Visitor::IVisitor {
 public:
  ~ComponentBase() override = default;

  std::string GetName();

 private:
  std::string name;
};
}  // namespace Components
}  // namespace Messages
