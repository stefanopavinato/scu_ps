/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "ComponentBase.h"

namespace Messages::Components {
template<class T>
class MessageValue : public ComponentBase {
 public:
  ~MessageValue() override = default;

  T GetValue();

 private:
  T value;
};
}  // namespace Messages::Components
