/************************************************************
* @Description  :
* @Author       : ZHAW-IAMP-SKS, Philippe Hindermann
* @Copyright    :
************************************************************/

#include "MessageValue.h"

namespace Messages::Components {
template<class T>
T MessageValue<T>::GetValue() {
  return value;
}

}  // namespace Messages::Components
