/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Messages/MessageBase.h"
#include "Messages/MessageLock.h"
#include "MessageVisitorBase.h"

namespace Messages {

bool MessageVisitorBase::Visit(MessageLock *message) {
  (void) message;
  return false;
}

bool MessageVisitorBase::Visit(Message *message) {
  (void) message;
  return false;
}

}  // namespace Messages
