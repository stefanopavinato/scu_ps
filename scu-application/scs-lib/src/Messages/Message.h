/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <list>
#include "Communication/Types/RequestId.h"
#include "Messages/Types/MessageStatus.h"
#include "Messages/Types/DataStatus.h"
#include "MessageBase.h"

namespace Messages {
class MessageItem;

class Message : public MessageBase {
 public:
  Message();

  ~Message() override = default;

  bool Accept(MessageVisitorBase * visitor) override;

  [[nodiscard]] Communication::Types::RequestId GetRequestId() const;

  void SetRequestId(const Communication::Types::RequestId::Enum &requestId);

  Messages::Types::MessageStatus *MessageStatus();

  Messages::Types::DataStatus *DataStatus();

  [[nodiscard]] Types::DataStatus::Enum GetDataStatus() const;

  [[nodiscard]] virtual std::list<MessageItem *> GetMessageItems() const = 0;

 private:
  Communication::Types::RequestId requestId_;

  Messages::Types::MessageStatus messageStatus_;

  Messages::Types::DataStatus dataStatus_;

  // ToDo: add timestamps (future extension)
  // ToDo: add state of message item task (future extension)
};


}  // namespace Messages

