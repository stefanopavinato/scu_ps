/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "MessageBase.h"

namespace Messages {

MessageBase::MessageBase()
  : sender_({})
  , receiver_({}) {
}

MessageBase::MessageBase(
  const ResourceSharing::Address &sender,
  const ResourceSharing::Address &receiver)
  : sender_(sender)
  , receiver_(receiver) {
}

bool MessageBase::Accept(MessageVisitorBase * visitor) {
  (void) visitor;
  return false;
}

int32_t MessageBase::GetClientFileDescriptor() const {
  return clientFileDescriptor_;
}

void MessageBase::SetClientFileDescriptor(const int32_t &clientFileDescriptor) {
  clientFileDescriptor_ = clientFileDescriptor;
}

ResourceSharing::Address MessageBase::GetSender() const {
  return sender_;
}

void MessageBase::SetSender(const ResourceSharing::Address &sender) {
  sender_ = sender;
}

ResourceSharing::Address MessageBase::GetReceiver() const {
  return receiver_;
}

void MessageBase::SetReceiver(const ResourceSharing::Address &receiver) {
  receiver_ = receiver;
}

}  // namespace Messages
