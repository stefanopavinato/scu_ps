/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

namespace Messages {
class MessageBase;
class Message;
class MessageLock;
class MessageVisitorBase {
 public:
  virtual bool OnMessageReceived(MessageBase * message) = 0;

  virtual bool Visit(MessageLock * message);

  virtual bool Visit(Message * message);
};
}  // namespace Messages
