/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "MessageItemJson.h"

namespace Messages {

MessageItemJson::MessageItemJson(
  const std::string &name,
  const std::string &path,
  std::unique_ptr<Messages::Visitor::VisitorJsonBase> visitor)
  : MessageItem(
  name,
  path)
  , visitor_(std::move(visitor)) {
}

Messages::Visitor::VisitorJsonBase *MessageItemJson::GetVisitor() const {
  return visitor_.get();
}

Messages::Visitor::VisitorBase *MessageItemJson::Visitor() {
  return visitor_.get();
}

}  // namespace Messages
