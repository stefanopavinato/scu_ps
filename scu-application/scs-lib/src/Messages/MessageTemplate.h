/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <memory>
#include <unordered_map>
#include "Message.h"

namespace Messages {
template<typename T>
class MessageTemplate : public Message {
 public:
  MessageTemplate();

  ~MessageTemplate() override = default;

  void AddMessageItem(std::unique_ptr<T> messageItem);

  [[nodiscard]] std::experimental::optional<T *> GetMessageItem(
    const std::string &name) const;

  [[nodiscard]] std::list<MessageItem *> GetMessageItems() const override;

  [[nodiscard]] std::list<T *> GetMessageTemplateItems() const;

 private:
  std::unordered_map<std::string, std::unique_ptr<T>> messageItems_;

  std::list<T *> messageItemsTemplatePtr_;

  std::list<MessageItem *> messageItemsPtr_;
};
}  // namespace Messages

#include "MessageTemplate.tpp"

