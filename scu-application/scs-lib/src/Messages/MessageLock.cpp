/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "MessageVisitorBase.h"
#include "MessageLock.h"

#include <utility>

namespace Messages {

MessageLock::MessageLock(
  const ResourceSharing::Address &sender,
  const ResourceSharing::Address &receiver,
  const uint32_t &lockId,
  const ResourceSharing::TimeStamp &timeStamp,
  const ResourceSharing::LockStatus &lockStatus)
  : MessageBase(
    sender,
    receiver)
  , lockId_(lockId)
  , timeStamp_(timeStamp)
  , lockStatus_(lockStatus) {
}

MessageLock::MessageLock(
  const uint32_t &lockId,
  const ResourceSharing::TimeStamp &timeStamp,
  const ResourceSharing::LockStatus &lockStatus)
  : lockId_(lockId)
  , timeStamp_(timeStamp)
  , lockStatus_(lockStatus) {
}

bool MessageLock::Accept(MessageVisitorBase *visitor) {
  return visitor->Visit(this);
}

uint32_t MessageLock::GetLockId() const {
  return lockId_;
}

ResourceSharing::LockStatus MessageLock::GetLockStatus() const {
  return lockStatus_;
}

ResourceSharing::TimeStamp MessageLock::GetTimeStamp() const {
  return timeStamp_;
}

}  // namespace Messages
