/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <memory>
#include <queue>
#include <variant>
#include "Types/VariantValue.h"
#include "Types/AlertSeverity.h"
#include "SystemModules/Framework/Components/ToBeNamed.h"
#include "Messages/Visitor/VisitorBase.h"
#include "Messages/Visitor/Types/AttributeType.h"
#include "Messages/Types/MessageItemState.h"

namespace Messages {
class MessageItem {
 public:
  MessageItem(
    std::string name,
    const std::string &path);

  [[nodiscard]] std::string GetName() const;

  std::queue<std::string> *GetPath();

  [[nodiscard]] bool Visit(SystemModules::Framework::Components::ToBeNamed *component);

  [[nodiscard]] Types::MessageItemState *ItemState();

 protected:
  virtual Messages::Visitor::VisitorBase *Visitor() = 0;

 private:
  const std::string name_;

  std::queue<std::string> path_;

  Types::MessageItemState state_;

  static const char kDelimiter;
};
}  // namespace Messages
