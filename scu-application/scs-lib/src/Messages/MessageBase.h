/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <cstdint>
#include "ResourceSharing/Address.h"

namespace Messages {
class MessageVisitorBase;
class MessageBase {
 public:
  MessageBase();

  MessageBase(
    const ResourceSharing::Address &sender,
    const ResourceSharing::Address &receiver);

  virtual ~MessageBase() = default;

  // Returns false if the message does not accept the visitor
  virtual bool Accept(MessageVisitorBase * visitor) = 0;

  [[nodiscard]] int32_t GetClientFileDescriptor() const;

  void SetClientFileDescriptor(const int32_t &clientFileDescriptor);

  [[nodiscard]] ResourceSharing::Address GetSender() const;

  void SetSender(const ResourceSharing::Address & sender);

  [[nodiscard]] ResourceSharing::Address GetReceiver() const;

  void SetReceiver(const ResourceSharing::Address & receiver);

 private:
  int32_t clientFileDescriptor_ = {};

  ResourceSharing::Address sender_;

  ResourceSharing::Address receiver_;
};
}  // namespace Messages
