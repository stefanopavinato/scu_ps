/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Messages/Visitor/VisitorJsonBase.h"
#include "MessageItem.h"

namespace Messages {
class MessageItemJson : public MessageItem {
 public:
  MessageItemJson(
    const std::string &name,
    const std::string &path,
    std::unique_ptr<Messages::Visitor::VisitorJsonBase> visitor);

  [[nodiscard]] Messages::Visitor::VisitorJsonBase *GetVisitor() const;

 protected:
  Messages::Visitor::VisitorBase *Visitor() override;

 private:
  std::unique_ptr<Messages::Visitor::VisitorJsonBase> visitor_;
};
}  // namespace Messages
