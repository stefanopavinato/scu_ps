/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <string>

namespace Messages {
namespace Types {
class MessageStatus {
 public:
  enum class Enum {
    OK = 0,
    INVALID_REQUEST_ID = 1,
    CRC_ERROR = 2,
    INVALID_DATA_LENGTH = 3,
    INTERNAL_ERROR = 4,
    WRONG_SYSTEM_COMPONENT = 5,
    NONE
  };

  MessageStatus();

  explicit MessageStatus(const Enum &value);

  [[nodiscard]] static std::string GetTypeName();

  [[nodiscard]] Enum GetValue() const;

  void SetValue(const Enum &value);

  [[nodiscard]] bool operator==(const MessageStatus &other) const;

  [[nodiscard]] std::string ToString() const;

  void FromString(const std::string &str);

 private:
  Enum value_;

  static const char kTypeName[];

  static const char kOK[];
  static const char kInvalidRequestID[];
  static const char kCRCError[];
  static const char kInvalidDataLength[];
  static const char kInternalError[];
  static const char kWrongSystemComponent[];
  static const char kNone[];
};
}  // namespace Types
}  // namespace Messages
