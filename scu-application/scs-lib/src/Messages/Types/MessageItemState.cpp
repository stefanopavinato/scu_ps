/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "MessageItemState.h"

namespace Messages {
namespace Types {

const char MessageItemState::kTypeName[] = "MessageItemState";

const char MessageItemState::kOK[] = "OK";
const char MessageItemState::kFailure[] = "FAILURE";
const char MessageItemState::kVisitorNotFound[] = "VisitorNotFound";
const char MessageItemState::kComponentNotFound[] = "ComponentNotFound";
const char MessageItemState::kNone[] = "NONE";


MessageItemState::MessageItemState()
  : value_(Enum::NONE) {
}

MessageItemState::MessageItemState(const Enum &value)
  : value_(value) {
}

std::string MessageItemState::GetTypeName() {
  return kTypeName;
}

MessageItemState::Enum MessageItemState::GetValue() const {
  return value_;
}

void MessageItemState::SetValue(const Enum &value) {
  value_ = value;
}

bool MessageItemState::operator==(const MessageItemState &other) const {
  return value_ == other.GetValue();
}

std::string MessageItemState::ToString() const {
  switch (value_) {
    case Enum::OK: {
      return kOK;
    }
    case Enum::VISITOR_NOT_FOUND: {
      return kVisitorNotFound;
    }
    case Enum::COMPONENT_NOT_FOUND: {
      return kComponentNotFound;
    }
    case Enum::FAILURE: {
      return kFailure;
    }
    case Enum::NONE: {
      return kNone;
    }
  }
  return kNone;
}

void MessageItemState::FromString(const std::string &str) {
  if (str == kOK) {
    value_ = Enum::OK;
  } else if (str == kFailure) {
    value_ = Enum::FAILURE;
  } else if (str == kVisitorNotFound) {
    value_ = Enum::VISITOR_NOT_FOUND;
  } else if (str == kComponentNotFound) {
    value_ = Enum::COMPONENT_NOT_FOUND;
  } else {
    value_ = Enum::NONE;
  }
}
}  // namespace Types
}  // namespace Messages
