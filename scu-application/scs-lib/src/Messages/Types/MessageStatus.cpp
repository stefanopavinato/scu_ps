/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "MessageStatus.h"

namespace Messages {
namespace Types {

const char MessageStatus::kTypeName[] = "MessageStatus";

const char MessageStatus::kOK[] = "OK";
const char MessageStatus::kInvalidRequestID[] = "InvalidRequestID";
const char MessageStatus::kCRCError[] = "CRCError";
const char MessageStatus::kInvalidDataLength[] = "InvalidDataLength";
const char MessageStatus::kInternalError[] = "InternalError";
const char MessageStatus::kWrongSystemComponent[] = "WrongSystemComponent";
const char MessageStatus::kNone[] = "None";

MessageStatus::MessageStatus()
  : value_(Enum::NONE) {
}

MessageStatus::MessageStatus(const Enum &value)
  : value_(value) {
}

std::string MessageStatus::GetTypeName() {
  return kTypeName;
}

MessageStatus::Enum MessageStatus::GetValue() const {
  return value_;
}

void MessageStatus::SetValue(const Enum &value) {
  value_ = value;
}

bool MessageStatus::operator==(const MessageStatus &other) const {
  return value_ == other.GetValue();
}

std::string MessageStatus::ToString() const {
  switch (value_) {
    case Enum::OK: {
      return kOK;
    }
    case Enum::INVALID_REQUEST_ID: {
      return kInvalidRequestID;
    }
    case Enum::CRC_ERROR: {
      return kCRCError;
    }
    case Enum::INVALID_DATA_LENGTH: {
      return kInvalidDataLength;
    }
    case Enum::INTERNAL_ERROR: {
      return kInternalError;
    }
    case Enum::WRONG_SYSTEM_COMPONENT: {
      return kWrongSystemComponent;
    }
    case Enum::NONE: {
      return kNone;
    }
  }
  return kNone;
}

void MessageStatus::FromString(const std::string &str) {
  if (str == kOK) {
    value_ = Enum::OK;
  } else if (str == kInvalidRequestID) {
    value_ = Enum::INVALID_REQUEST_ID;
  } else if (str == kCRCError) {
    value_ = Enum::CRC_ERROR;
  } else if (str == kInvalidDataLength) {
    value_ = Enum::INVALID_DATA_LENGTH;
  } else if (str == kInternalError) {
    value_ = Enum::INTERNAL_ERROR;
  } else if (str == kWrongSystemComponent) {
    value_ = Enum::WRONG_SYSTEM_COMPONENT;
  } else {
    value_ = Enum::NONE;
  }
}


}  // namespace Types
}  // namespace Messages
