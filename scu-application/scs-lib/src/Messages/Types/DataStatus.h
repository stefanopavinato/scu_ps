/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <string>

namespace Messages {
namespace Types {
class DataStatus {
 public:
  enum class Enum {
    OK = 0,
    INFORMATION_NOT_AVAILABLE = 100,
    MC_NOT_PRESENT = 101,
    SLOT_NOT_VALID = 102,
    INVALID_REQUEST_FORMAT = 103,
    WRONG_MEZZANINE_CARD = 104,
    INVALID_SIGNAL_INDEX = 105,
    INVALID_SIGNAL_VALUE = 106,
    COMMAND_NOT_IMPLEMENTED = 107,
    NONE
  };

  DataStatus();

  explicit DataStatus(const Enum &value);

  [[nodiscard]] static std::string GetTypeName();

  [[nodiscard]] Enum GetValue() const;

  void SetValue(const Enum &value);

  [[nodiscard]] bool operator==(const DataStatus &other) const;

  [[nodiscard]] std::string ToString() const;

  void FromString(const std::string &str);

 private:
  Enum value_;

  static const char kTypeName[];

  static const char kOK[];
  static const char kInformationNotAvailable[];
  static const char kMezzanineCardNotPresent[];
  static const char kSlotNotValid[];
  static const char kInvalidRequestFormat[];
  static const char kWrongMezzanineCard[];
  static const char kSignalIndexNotValid[];
  static const char kSignalValueNotValid[];
  static const char kCommandNotImplemented[];
  static const char kNone[];
};
}  // namespace Types
}  // namespace Messages
