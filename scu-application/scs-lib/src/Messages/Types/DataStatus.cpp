/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "DataStatus.h"

namespace Messages {
namespace Types {

const char DataStatus::kTypeName[] = "DataStatus";

const char DataStatus::kOK[] = "OK";
const char DataStatus::kInformationNotAvailable[] = "InformationNotAvailable";
const char DataStatus::kMezzanineCardNotPresent[] = "MezzanineCardNotPresent";
const char DataStatus::kSlotNotValid[] = "SlotNotValid";
const char DataStatus::kInvalidRequestFormat[] = "InvalidRequestFormat";
const char DataStatus::kWrongMezzanineCard[] = "WrongMezzanineCard";
const char DataStatus::kSignalIndexNotValid[] = "SignalIndexNotValid";
const char DataStatus::kSignalValueNotValid[] = "SignalValueNotValid";
const char DataStatus::kCommandNotImplemented[] = "CommandNotImplemented";
const char DataStatus::kNone[] = "None";

DataStatus::DataStatus()
  : value_(Enum::NONE) {
}

DataStatus::DataStatus(const Enum &value)
  : value_(value) {
}

std::string DataStatus::GetTypeName() {
  return kTypeName;
}

DataStatus::Enum DataStatus::GetValue() const {
  return value_;
}

void DataStatus::SetValue(const Enum &value) {
  value_ = value;
}

bool DataStatus::operator==(const DataStatus &other) const {
  return value_ == other.GetValue();
}

std::string DataStatus::ToString() const {
  switch (value_) {
    case Enum::OK: {
      return kOK;
    }
    case Enum::INFORMATION_NOT_AVAILABLE: {
      return kInformationNotAvailable;
    }
    case Enum::MC_NOT_PRESENT: {
      return kMezzanineCardNotPresent;
    }
    case Enum::SLOT_NOT_VALID: {
      return kSlotNotValid;
    }
    case Enum::INVALID_REQUEST_FORMAT: {
      return kInvalidRequestFormat;
    }
    case Enum::WRONG_MEZZANINE_CARD: {
      return kWrongMezzanineCard;
    }
    case Enum::INVALID_SIGNAL_INDEX: {
      return kSignalIndexNotValid;
    }
    case Enum::INVALID_SIGNAL_VALUE: {
      return kSignalValueNotValid;
    }
    case Enum::COMMAND_NOT_IMPLEMENTED: {
      return kCommandNotImplemented;
    }
    case Enum::NONE: {
      return kNone;
    }
  }
  return kNone;
}

void DataStatus::FromString(const std::string &str) {
  if (str == kOK) {
    value_ = Enum::OK;
  } else if (str == kInformationNotAvailable) {
    value_ = Enum::INFORMATION_NOT_AVAILABLE;
  } else if (str == kMezzanineCardNotPresent) {
    value_ = Enum::MC_NOT_PRESENT;
  } else if (str == kSlotNotValid) {
    value_ = Enum::SLOT_NOT_VALID;
  } else if (str == kInvalidRequestFormat) {
    value_ = Enum::INVALID_REQUEST_FORMAT;
  } else if (str == kWrongMezzanineCard) {
    value_ = Enum::WRONG_MEZZANINE_CARD;
  } else {
    value_ = Enum::NONE;
  }
}


}  // namespace Types
}  // namespace Messages
