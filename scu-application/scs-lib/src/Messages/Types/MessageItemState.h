/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <string>

namespace Messages {
namespace Types {
class MessageItemState {
 public:
  enum class Enum {
    OK,
    VISITOR_NOT_FOUND = 200,
    COMPONENT_NOT_FOUND = 202,
    FAILURE = 204,
    NONE = 205
  };

  MessageItemState();

  explicit MessageItemState(const Enum &value);

  [[nodiscard]] static std::string GetTypeName();

  [[nodiscard]] Enum GetValue() const;

  void SetValue(const Enum &value);

  [[nodiscard]] bool operator==(const MessageItemState &other) const;

  [[nodiscard]] std::string ToString() const;

  void FromString(const std::string &str);

 private:
  Enum value_;

  static const char kTypeName[];

  static const char kOK[];
  static const char kVisitorNotFound[];
  static const char kComponentNotFound[];
  static const char kFailure[];
  static const char kNone[];
};
}  // namespace Types
}  // namespace Messages
