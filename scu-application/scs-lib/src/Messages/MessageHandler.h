/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "MessageVisitorBase.h"

namespace Logger {
class ILogger;
}  // namespace Logger
namespace SystemModules::Framework::Components {
class IComponent;
}  // namespace SystemModules::Framework::Components
namespace Messages {
class MessageVisitorBase;
class MessageHandler : public Messages::MessageVisitorBase {
 public:
  explicit MessageHandler(Logger::ILogger *logger);

  bool Initialize(
    SystemModules::Framework::Components::IComponent *moduleHandler,
    Messages::MessageVisitorBase *clientHandler);

  bool OnMessageReceived(Messages::MessageBase *message) override;

  bool Visit(Message *message) override;

 private:
  Logger::ILogger *const logger_;

  SystemModules::Framework::Components::IComponent *moduleHandler_;

  Messages::MessageVisitorBase *clientHandler_;
};
}  // namespace Messages
