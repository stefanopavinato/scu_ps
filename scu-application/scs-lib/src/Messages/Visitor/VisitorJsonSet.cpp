/************************************************************
* @Description  :
* @Author       : ZHAW-IAMP-SKS, Philippe Hindermann
* @Copyright    :
************************************************************/

#include "Types/ForceStatus.h"
#include "WorkloadBalancing/WorkloadBalanceHandler.h"
#include "WorkloadBalancing/ItemHandler.h"
#include "SystemModules/Framework/MonitoringFunctions/MFBase.h"
#include "SystemModules/Framework/Components/ModuleHandler.h"
#include "SystemModules/Framework/Components/ModuleBase.h"
#include "SystemModules/Framework/Components/Container.h"
#include "SystemModules/Framework/Components/ItemInBase.h"
#include "SystemModules/Framework/Components/ItemOutBase.h"
#include "SystemModules/Framework/Components/ItemInOutBase.h"
#include "VisitorJsonSet.h"

#include <utility>

namespace Messages::Visitor {

const char VisitorJsonSet::kResult[] = "Result";
const char VisitorJsonSet::kSuccess[] = "Success";
const char VisitorJsonSet::kFailure[] = "Failure";

VisitorJsonSet::VisitorJsonSet(
  const Types::AttributeType::Enum &attribute,
  const ::Types::ValueType::Enum &valueType,
  ::Types::TestVariantValue value)
  : VisitorJsonBase()
  , attribute_(attribute)
  , valueType_(valueType)
  , value_(std::move(value)) {
}

::Json::Value VisitorJsonSet::GetJson() const {
  if (SuccessfulVisit()) {
    Json::Value retVal;
    retVal[kResult] = kSuccess;
    return retVal;
  }
  Json::Value retVal;
  retVal[kResult] = kFailure;
  return retVal;
}

void VisitorJsonSet::Visit(WorkloadBalancing::WorkloadBalanceHandler *workloadBalanceHandler) {
  (void) workloadBalanceHandler;
}

void VisitorJsonSet::Visit(WorkloadBalancing::ItemHandler *itemHandler) {
  switch (attribute_.GetValue()) {
    case Types::AttributeType::Enum::PAUSE: {
      itemHandler->Pause();
      SetSuccessfulVisit(true);
      break;
    }
    case Types::AttributeType::Enum::PAUSE_RESUME: {
      itemHandler->Resume();
      SetSuccessfulVisit(true);
      break;
    }
    default: {
    }
  }
}

void VisitorJsonSet::Visit(
  SystemModules::Framework::Components::ModuleHandler *moduleHandler) {
  switch (attribute_.GetValue()) {
    case Types::AttributeType::Enum::MODULE_RESET: {
      moduleHandler->ResetAllModules();
      SetSuccessfulVisit(true);
      break;
    }
    case Types::AttributeType::Enum::MODULE_STOP: {
      moduleHandler->StopAllModules();
      SetSuccessfulVisit(true);
      break;
    }
    case Types::AttributeType::Enum::MODULE_STOP_RELEASE: {
      moduleHandler->ReleaseStopAllModules();
      SetSuccessfulVisit(true);
      break;
    }
    default: {
    }
  }
}

void VisitorJsonSet::Visit(
  SystemModules::Framework::Components::ModuleBase *module) {
  switch (attribute_.GetValue()) {
    case Types::AttributeType::Enum::MODULE_RESET: {
      module->Reset();
      SetSuccessfulVisit(true);
      break;
    }
    case Types::AttributeType::Enum::MODULE_STOP: {
      module->Stop();
      SetSuccessfulVisit(true);
      break;
    }
    case Types::AttributeType::Enum::MODULE_STOP_RELEASE: {
      module->StopRelease();
      SetSuccessfulVisit(true);
      break;
    }
    case Types::AttributeType::Enum::STATUS_RESET: {
      module->ResetAlertSeverity();
      SetSuccessfulVisit(true);
      break;
    }
    default: {
    }
  }
}

void VisitorJsonSet::Visit(
  SystemModules::Framework::Components::Container *container) {
  switch (attribute_.GetValue()) {
    case Types::AttributeType::Enum::STATUS_RESET: {
      container->ResetAlertSeverity();
      SetSuccessfulVisit(true);
      break;
    }
    default: {
    }
  }
}

void VisitorJsonSet::Visit(
  SystemModules::Framework::Components::ItemInBase *itemIn) {
  switch (attribute_.GetValue()) {
    case Types::AttributeType::Enum::UPDATE_INTERVAL: {
      SetSuccessfulVisit(
        itemIn->SetUpdateInterval(
          value_.GetVariantValue()));
      break;
    }
    case Types::AttributeType::Enum::VALUE_IN_FORCE: {
      SetSuccessfulVisit(
        itemIn->ForceValue(
          value_.GetVariantValue(),
          ::Types::ForceStatus::Enum::PERMANENT));
      break;
    }
    case Types::AttributeType::Enum::VALUE_IN_FORCE_ONCE: {
      SetSuccessfulVisit(
        itemIn->ForceValue(
          value_.GetVariantValue(),
          ::Types::ForceStatus::Enum::ONCE));
      break;
    }
    case Types::AttributeType::Enum::VALUE_IN_FORCE_RELEASE: {
      SetSuccessfulVisit(
        itemIn->ReleaseValueForced());
      break;
    }
    case Types::AttributeType::Enum::STATUS_RESET: {
      itemIn->ResetAlertSeverity();
      SetSuccessfulVisit(true);
      break;
    }
    default: {
    }
  }
}

void VisitorJsonSet::Visit(
  SystemModules::Framework::Components::ItemOutBase *itemOut) {
  switch (attribute_.GetValue()) {
    case Types::AttributeType::Enum::UPDATE_INTERVAL: {
      SetSuccessfulVisit(
        itemOut->SetUpdateInterval(
          value_.GetVariantValue()));
      break;
    }
    case Types::AttributeType::Enum::VALUE_OUT_FORCE: {
      SetSuccessfulVisit(
        itemOut->ForceValue(
          value_.GetVariantValue(),
          ::Types::ForceStatus::Enum::PERMANENT));
      break;
    }
    case Types::AttributeType::Enum::VALUE_OUT_FORCE_ONCE: {
      SetSuccessfulVisit(
        itemOut->ForceValue(
          value_.GetVariantValue(),
          ::Types::ForceStatus::Enum::ONCE));
      break;
    }
    case Types::AttributeType::Enum::VALUE_OUT_FORCE_RELEASE: {
      SetSuccessfulVisit(
        itemOut->ReleaseValueForced());
      break;
    }
    case Types::AttributeType::Enum::STATUS_RESET: {
      itemOut->ResetAlertSeverity();
      SetSuccessfulVisit(true);
      break;
    }
    default: {
    }
  }
}

void VisitorJsonSet::Visit(
  SystemModules::Framework::Components::ItemInOutBase *itemInOut) {
  switch (attribute_.GetValue()) {
    case Types::AttributeType::Enum::UPDATE_INTERVAL: {
      SetSuccessfulVisit(
        itemInOut->SetUpdateInterval(
          value_.GetVariantValue()));
      break;
    }
    case Types::AttributeType::Enum::VALUE_IN_FORCE: {
      SetSuccessfulVisit(
        itemInOut->ForceValueIn(
          value_.GetVariantValue(),
          ::Types::ForceStatus::Enum::PERMANENT));
      break;
    }
    case Types::AttributeType::Enum::VALUE_IN_FORCE_ONCE: {
      SetSuccessfulVisit(
        itemInOut->ForceValueIn(
          value_.GetVariantValue(),
          ::Types::ForceStatus::Enum::ONCE));
      break;
    }
    case Types::AttributeType::Enum::VALUE_IN_FORCE_RELEASE: {
      SetSuccessfulVisit(
        itemInOut->ReleaseValueInForced());
      break;
    }
    case Types::AttributeType::Enum::VALUE_OUT_FORCE: {
      SetSuccessfulVisit(
        itemInOut->ForceValueOut(
          value_.GetVariantValue(),
          ::Types::ForceStatus::Enum::PERMANENT));
      break;
    }
    case Types::AttributeType::Enum::VALUE_OUT_FORCE_ONCE: {
      SetSuccessfulVisit(
        itemInOut->ForceValueOut(
          value_.GetVariantValue(),
          ::Types::ForceStatus::Enum::ONCE));
      break;
    }
    case Types::AttributeType::Enum::VALUE_OUT_FORCE_RELEASE: {
      SetSuccessfulVisit(
        itemInOut->ReleaseValueOutForced());
      break;
    }
    case Types::AttributeType::Enum::STATUS_RESET: {
      itemInOut->ResetAlertSeverity();
      SetSuccessfulVisit(true);
      break;
    }
    default: {
    }
  }
}

void VisitorJsonSet::Visit(
  SystemModules::Framework::Components::ParameterItemInBase *parameterItemIn) {
  (void) parameterItemIn;
  SetSuccessfulVisit(false);
}

void VisitorJsonSet::Visit(
  SystemModules::Framework::Components::ParameterItemOutBase *parameterItemOut) {
  (void) parameterItemOut;
  SetSuccessfulVisit(false);
}

void VisitorJsonSet::Visit(
  SystemModules::Framework::MonitoringFunctions::MFBase *monitoringFunction) {
  switch (attribute_.GetValue()) {
    case Types::AttributeType::Enum::STATUS_RESET: {
      monitoringFunction->RequestReset();
      SetSuccessfulVisit(true);
      break;
    }
    default: {
    }
  }
}

void VisitorJsonSet::Visit(
  SystemModules::Framework::Actions::ActionBase *action) {
  (void) action;
}

void VisitorJsonSet::Visit(
  SystemModules::Framework::Accessors::AccessorBase *accessor) {
  (void) accessor;
}

void VisitorJsonSet::Visit(::Types::TestVariantValue *accessor) {
  (void) accessor;
}

void VisitorJsonSet::Visit(::Types::ComponentStatus *status) {
  (void) status;
}

void VisitorJsonSet::Visit(::Types::MonitoringFunctionStatus *status) {
  (void) status;
}

}  // namespace Messages::Visitor
