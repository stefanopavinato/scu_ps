/************************************************************
* @Description  :
* @Author       : ZHAW-IAMP-SKS, Philippe Hindermann
* @Copyright    :
************************************************************/

#pragma once

#include "VisitorBase.h"

namespace Messages::Visitor {
class VisitorJsonBase : public VisitorBase {
 public:
  VisitorJsonBase();

  [[nodiscard]] virtual ::Json::Value GetJson() const = 0;
};
}  // namespace Messages::Visitor
