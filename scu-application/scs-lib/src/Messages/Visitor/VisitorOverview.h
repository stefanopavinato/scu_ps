/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <memory>
#include <stack>
#include <json/json.h>
#include "Types/JsonDetails.h"
#include "VisitorJsonBase.h"

namespace Messages::Visitor {
class VisitorOverview : public VisitorJsonBase {
 public:
  explicit VisitorOverview(
    const uint32_t &maxLevel,
    const ::Types::JsonDetails &details);

  ~VisitorOverview() override = default;

  [[nodiscard]] ::Json::Value GetJson() const override;

  void Visit(WorkloadBalancing::WorkloadBalanceHandler *workloadBalanceHandler) override;

  void Visit(WorkloadBalancing::ItemHandler *itemHandler) override;

  void Visit(SystemModules::Framework::Components::ModuleHandler *moduleHandler) override;

  void Visit(SystemModules::Framework::Components::ModuleBase *module) override;

  void Visit(SystemModules::Framework::Components::Container *container) override;

  void Visit(SystemModules::Framework::Components::ItemInBase *itemIn) override;

  void Visit(SystemModules::Framework::Components::ItemOutBase *itemOut) override;

  void Visit(SystemModules::Framework::Components::ItemInOutBase *itemInOut) override;

  void Visit(SystemModules::Framework::Components::ParameterItemInBase *parameterItemIn) override;

  void Visit(SystemModules::Framework::Components::ParameterItemOutBase *parameterItemOut) override;

  void Visit(SystemModules::Framework::MonitoringFunctions::MFBase *monitoringFunction) override;

  void Visit(SystemModules::Framework::Actions::ActionBase *action) override;

  void Visit(SystemModules::Framework::Accessors::AccessorBase *accessor) override;

  void Visit(::Types::TestVariantValue *accessor) override;

  void Visit(::Types::ComponentStatus *status) override;

  void Visit(::Types::MonitoringFunctionStatus *status) override;

 private:
  std::stack<std::unique_ptr<Json::Value>> jsonStack_;

  uint32_t level_;

  const uint32_t maxLevel_;

  const ::Types::JsonDetails details_;

  static const char kFailure[];
  static const char kVisitFailure[];
  static const char kNoDataCollected[];
};
}  // namespace Messages::Visitor
