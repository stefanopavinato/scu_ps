/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "VisitorType.h"

namespace Messages {
namespace Visitor {
namespace Types {

const char VisitorType::kTypeName[] = "VisitorType";

const char VisitorType::kOverview[] = "Overview";
const char VisitorType::kComponent[] = "Component";
const char VisitorType::kPropertyGet[] = "PropertyGet";
const char VisitorType::kPropertySet[] = "PropertySet";
const char VisitorType::kNone[] = "None";

VisitorType::VisitorType()
  : value_(Enum::NONE) {
}

VisitorType::VisitorType(const Enum &value)
  : value_(value) {
}

std::string VisitorType::GetTypeName() {
  return kTypeName;
}

VisitorType::Enum VisitorType::GetValue() const {
  return value_;
}

void VisitorType::SetValue(const Enum &value) {
  value_ = value;
}

bool VisitorType::Equals(const VisitorType &other) const {
  return value_ == other.GetValue();
}

bool VisitorType::Equals(const Enum &value) const {
  return value_ == value;
}

std::string VisitorType::ToString() const {
  switch (value_) {
    case Enum::OVERVIEW: {
      return kOverview;
    }
    case Enum::COMPONENT: {
      return kComponent;
    }
    case Enum::PROPERTY_GET: {
      return kPropertyGet;
    }
    case Enum::PROPERTY_SET: {
      return kPropertySet;
    }
    case Enum::NONE: {
      return kNone;
    }
  }
  return kNone;
}

void VisitorType::FromString(const std::string &str) {
  if (str == kOverview) {
    value_ = Enum::OVERVIEW;
  } else if (str == kComponent) {
    value_ = Enum::COMPONENT;
  } else if (str == kPropertyGet) {
    value_ = Enum::PROPERTY_GET;
  } else if (str == kPropertySet) {
    value_ = Enum::PROPERTY_SET;
  } else {
    value_ = Enum::NONE;
  }
}

}  // namespace Types
}  // namespace Visitor
}  // namespace Messages
