/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <string>

namespace Messages::Visitor::Types {
class AttributeType {
 public:
  enum class Enum {
    COMPONENT_TYPE,
    NAME,
    DESCRIPTION,
    ALERT_SEVERITY,
    CLASS,
    UPDATE_INTERVAL,
    MODULE_STATE,
    MODULE_RESET,
    MODULE_STOP,
    MODULE_STOP_RELEASE,
    VALUE_IN,
    VALUE_IN_READ,
    VALUE_IN_FORCE,
    VALUE_IN_FORCE_RELEASE,
    VALUE_IN_IS_FORCED,
    VALUE_IN_FORCE_ONCE,
    VALUE_OUT,
    VALUE_OUT_WRITE,
    VALUE_OUT_FORCE,
    VALUE_OUT_FORCE_RELEASE,
    VALUE_OUT_FORCE_ONCE,
    VALUE_OUT_IS_FORCED,
    STATUS_RESET,
    PAUSE,
    PAUSE_RESUME,
    NONE
  };

  AttributeType();

  explicit AttributeType(const Enum &value);

  explicit AttributeType(const std::string &name);

  [[nodiscard]] static std::string GetTypeName();

  [[nodiscard]] Enum GetValue() const;

  void SetValue(const Enum &value);

  bool operator==(const AttributeType &other) const;

  [[nodiscard]] std::string ToString() const;

  void FromString(const std::string &str);

  static const char kComponentType[];
  static const char kName[];
  static const char kDescription[];
  static const char kAlertSeverity[];
  static const char kClass[];
  static const char kUpdateInterval[];
  static const char kModuleState[];
  static const char kModuleReset[];
  static const char kModuleStop[];
  static const char kModuleStopRelease[];
  static const char kValueIn[];
  static const char kValueInRead[];
  static const char kValueInForce[];
  static const char kValueInForceRelease[];
  static const char kValueInForceOnce[];
  static const char kValueInIsForced[];
  static const char kValueOut[];
  static const char kValueOutWrite[];
  static const char kValueOutForce[];
  static const char kValueOutForceRelease[];
  static const char kValueOutForceOnce[];
  static const char kValueOutIsForced[];
  static const char kStatusReset[];
  static const char kPause[];
  static const char kPauseResume[];

 private:
  Enum value_;

  static const char kTypeName[];

  static const char kNone[];
};
}  // namespace Messages::Visitor::Types
