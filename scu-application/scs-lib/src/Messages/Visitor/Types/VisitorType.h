/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <string>

namespace Messages {
namespace Visitor {
namespace Types {
class VisitorType {
 public:
  enum class Enum {
    OVERVIEW,
    COMPONENT,
    PROPERTY_GET,
    PROPERTY_SET,
    NONE
  };

  VisitorType();

  explicit VisitorType(const Enum &value);

  [[nodiscard]] static std::string GetTypeName();

  [[nodiscard]] Enum GetValue() const;

  void SetValue(const Enum &value);

  [[nodiscard]] bool Equals(const VisitorType &other) const;

  [[nodiscard]] bool Equals(const Enum &value) const;

  [[nodiscard]] std::string ToString() const;

  void FromString(const std::string &str);

 private:
  Enum value_;

  static const char kTypeName[];

  static const char kOverview[];
  static const char kComponent[];
  static const char kPropertyGet[];
  static const char kPropertySet[];
  static const char kNone[];
};
}  // namespace Types
}  // namespace Visitor
}  // namespace Messages
