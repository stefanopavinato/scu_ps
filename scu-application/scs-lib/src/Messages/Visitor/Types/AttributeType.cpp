/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "AttributeType.h"

namespace Messages::Visitor::Types {

const char AttributeType::kTypeName[] = "AttributeType";

const char AttributeType::kComponentType[] = "ComponentType";
const char AttributeType::kName[] = "Name";
const char AttributeType::kDescription[] = "Description";
const char AttributeType::kAlertSeverity[] = "AlertSeverity";
const char AttributeType::kClass[] = "Class";
const char AttributeType::kUpdateInterval[] = "UpdateInterval";
const char AttributeType::kModuleState[] = "ModuleState";
const char AttributeType::kModuleReset[] = "ModuleReset";
const char AttributeType::kModuleStop[] = "ModuleStop";
const char AttributeType::kModuleStopRelease[] = "ModuleStopRelease";
const char AttributeType::kValueIn[] = "ValueIn";
const char AttributeType::kValueInRead[] = "ValueInRead";
const char AttributeType::kValueInForce[] = "ValueInForce";
const char AttributeType::kValueInForceRelease[] = "ValueInForceRelease";
const char AttributeType::kValueInForceOnce[] = "ValueInForceOnce";
const char AttributeType::kValueInIsForced[] = "ValueInIsForced";
const char AttributeType::kValueOut[] = "ValueOut";
const char AttributeType::kValueOutWrite[] = "ValueOutWrite";
const char AttributeType::kValueOutForce[] = "ValueOutForce";
const char AttributeType::kValueOutForceRelease[] = "ValueOutForceRelease";
const char AttributeType::kValueOutForceOnce[] = "ValueOutForceOnce";
const char AttributeType::kValueOutIsForced[] = "ValueOutIsForced";
const char AttributeType::kStatusReset[] = "StatusReset";
const char AttributeType::kPause[] = "Pause";
const char AttributeType::kPauseResume[] = "PauseResume";
const char AttributeType::kNone[] = "None";

AttributeType::AttributeType()
  : value_(Enum::NONE) {
}

AttributeType::AttributeType(const Enum &value)
  : value_(value) {
}

AttributeType::AttributeType(const std::string &name) {
  FromString(name);
}

std::string AttributeType::GetTypeName() {
  return kTypeName;
}

AttributeType::Enum AttributeType::GetValue() const {
  return value_;
}

void AttributeType::SetValue(const Enum &value) {
  value_ = value;
}

bool AttributeType::operator==(const AttributeType &other) const {
  return value_ == other.GetValue();
}

std::string AttributeType::ToString() const {
  switch (value_) {
    case Enum::COMPONENT_TYPE: {
      return kComponentType;
    }
    case Enum::NAME: {
      return kName;
    }
    case Enum::DESCRIPTION: {
      return kDescription;
    }
    case Enum::ALERT_SEVERITY: {
      return kAlertSeverity;
    }
    case Enum::CLASS: {
      return kClass;
    }
    case Enum::UPDATE_INTERVAL: {
      return kUpdateInterval;
    }
    case Enum::MODULE_STATE: {
      return kModuleState;
    }
    case Enum::MODULE_RESET: {
      return kModuleReset;
    }
    case Enum::MODULE_STOP: {
      return kModuleStop;
    }
    case Enum::MODULE_STOP_RELEASE: {
      return kModuleStopRelease;
    }
    case Enum::VALUE_IN: {
      return kValueIn;
    }
    case Enum::VALUE_IN_READ: {
      return kValueInRead;
    }
    case Enum::VALUE_IN_FORCE: {
      return kValueInForce;
    }
    case Enum::VALUE_IN_FORCE_ONCE: {
      return kValueInForceOnce;
    }
    case Enum::VALUE_IN_IS_FORCED: {
      return kValueInIsForced;
    }
    case Enum::VALUE_IN_FORCE_RELEASE: {
      return kValueInForceRelease;
    }
    case Enum::VALUE_OUT: {
      return kValueOut;
    }
    case Enum::VALUE_OUT_WRITE: {
      return kValueOutWrite;
    }
    case Enum::VALUE_OUT_FORCE: {
      return kValueOutForce;
    }
    case Enum::VALUE_OUT_IS_FORCED: {
      return kValueOutIsForced;
    }
    case Enum::VALUE_OUT_FORCE_RELEASE: {
      return kValueOutForceRelease;
    }
    case Enum::VALUE_OUT_FORCE_ONCE: {
      return kValueOutForceOnce;
    }
    case Enum::STATUS_RESET: {
      return kStatusReset;
    }
    case Enum::PAUSE: {
      return kPause;
    }
    case Enum::PAUSE_RESUME: {
      return kPauseResume;
    }
    case Enum::NONE: {
      return kNone;
    }
  }
  return kNone;
}

void AttributeType::FromString(const std::string &str) {
  if (str == kComponentType) {
    value_ = Enum::COMPONENT_TYPE;
  } else if (str == kName) {
    value_ = Enum::NAME;
  } else if (str == kDescription) {
    value_ = Enum::DESCRIPTION;
  } else if (str == kAlertSeverity) {
    value_ = Enum::ALERT_SEVERITY;
  } else if (str == kClass) {
    value_ = Enum::CLASS;
  } else if (str == kUpdateInterval) {
    value_ = Enum::UPDATE_INTERVAL;
  } else if (str == kModuleState) {
    value_ = Enum::MODULE_STATE;
  } else if (str == kModuleReset) {
    value_ = Enum::MODULE_RESET;
  } else if (str == kModuleStop) {
    value_ = Enum::MODULE_STOP;
  } else if (str == kModuleStopRelease) {
    value_ = Enum::MODULE_STOP_RELEASE;
  } else if (str == kValueIn) {
    value_ = Enum::VALUE_IN;
  } else if (str == kValueInRead) {
    value_ = Enum::VALUE_IN_READ;
  } else if (str == kValueInForce) {
    value_ = Enum::VALUE_IN_FORCE;
  } else if (str == kValueInIsForced) {
    value_ = Enum::VALUE_IN_IS_FORCED;
  } else if (str == kValueInForceRelease) {
    value_ = Enum::VALUE_IN_FORCE_RELEASE;
  } else if (str == kValueInForceOnce) {
    value_ = Enum::VALUE_IN_FORCE_ONCE;
  } else if (str == kValueOut) {
    value_ = Enum::VALUE_OUT;
  } else if (str == kValueOutWrite) {
    value_ = Enum::VALUE_OUT_WRITE;
  } else if (str == kValueOutForce) {
    value_ = Enum::VALUE_OUT_FORCE;
  } else if (str == kValueOutIsForced) {
    value_ = Enum::VALUE_OUT_IS_FORCED;
  } else if (str == kValueOutForceRelease) {
    value_ = Enum::VALUE_OUT_FORCE_RELEASE;
  } else if (str == kValueOutForceOnce) {
    value_ = Enum::VALUE_OUT_FORCE_ONCE;
  } else if (str == kStatusReset) {
    value_ = Enum::STATUS_RESET;
  } else if (str == kPause) {
    value_ = Enum::PAUSE;
  } else if (str == kPauseResume) {
    value_ = Enum::PAUSE_RESUME;
  } else {
    value_ = Enum::NONE;
  }
}

}  // namespace Messages::Visitor::Types
