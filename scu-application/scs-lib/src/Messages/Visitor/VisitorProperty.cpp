/************************************************************
* @Description  :
* @Author       : ZHAW-IAMP-SKS, Philippe Hindermann
* @Copyright    :
************************************************************/

#include "VisitorProperty.h"

namespace Messages::Visitor {

VisitorProperty::VisitorProperty(
  const Types::AttributeType::Enum &attribute)
  : VisitorBase(), attribute_(attribute) {
}

VisitorProperty::VisitorProperty(
  const Types::AttributeType::Enum &attribute,
  const ::Types::ValueType::Enum &valueType,
  const ::Types::TestVariantValue &value)
  : VisitorBase(), attribute_(attribute), valueType_(valueType), value_(value) {
}

Types::AttributeType VisitorProperty::GetAttribute() const {
  return attribute_;
}

::Types::ValueType VisitorProperty::GetValueType() const {
  return valueType_;
}

::Types::TestVariantValue VisitorProperty::GetVariantValue() const {
  return value_;
}

}  // namespace Messages::Visitor
