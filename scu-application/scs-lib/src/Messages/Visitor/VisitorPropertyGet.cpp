/************************************************************
* @Description  :
* @Author       : ZHAW-IAMP-SKS, Philippe Hindermann
* @Copyright    :
************************************************************/

#include "SystemModules/Framework/Components/Container.h"
#include "SystemModules/Framework/Components/ItemInBase.h"
#include "SystemModules/Framework/Components/ItemOutBase.h"
#include "SystemModules/Framework/Components/ItemInOutBase.h"
#include "SystemModules/Framework/Components/ParameterItemInBase.h"
#include "SystemModules/Framework/Components/ParameterItemOutBase.h"
#include "SystemModules/Framework/Components/ModuleBase.h"
#include "VisitorPropertyGet.h"

namespace Messages::Visitor {

VisitorPropertyGet::VisitorPropertyGet(
  const Types::AttributeType::Enum &attribute)
  : VisitorProperty(attribute) {
}

void VisitorPropertyGet::Visit(WorkloadBalancing::WorkloadBalanceHandler *workloadBalanceHandler) {
  (void) workloadBalanceHandler;
}

void VisitorPropertyGet::Visit(WorkloadBalancing::ItemHandler *itemHandler) {
  (void) itemHandler;
}

void VisitorPropertyGet::Visit(
  SystemModules::Framework::Components::ModuleHandler *moduleHandler) {
  (void) moduleHandler;
}

void VisitorPropertyGet::Visit(
  SystemModules::Framework::Components::ModuleBase *module) {
  switch (GetAttribute().GetValue()) {
    case Types::AttributeType::Enum::MODULE_STATE: {
      SetValue(module->GetStateId());
      SetSuccessfulVisit(true);
      break;
    }
    default: {
    }
  }
}

void VisitorPropertyGet::Visit(
  SystemModules::Framework::Components::Container *container) {
  switch (GetAttribute().GetValue()) {
    case Types::AttributeType::Enum::COMPONENT_TYPE: {
      SetValue(container->GetComponentType());
      SetSuccessfulVisit(true);
      break;
    }
    case Types::AttributeType::Enum::NAME: {
      SetValue(container->GetName());
      SetSuccessfulVisit(true);
      break;
    }
    case Types::AttributeType::Enum::DESCRIPTION: {
      SetValue(container->GetDescription());
      SetSuccessfulVisit(true);
      break;
    }
    default: {
    }
  }
}

void VisitorPropertyGet::Visit(
  SystemModules::Framework::Components::ItemInBase *itemIn) {
  switch (GetAttribute().GetValue()) {
    case Types::AttributeType::Enum::COMPONENT_TYPE: {
      SetValue(itemIn->GetComponentType());
      SetSuccessfulVisit(true);
      break;
    }
    case Types::AttributeType::Enum::NAME: {
      SetValue(itemIn->GetName());
      SetSuccessfulVisit(true);
      break;
    }
    case Types::AttributeType::Enum::DESCRIPTION: {
      SetValue(itemIn->GetDescription());
      SetSuccessfulVisit(true);
      break;
    }
    case Types::AttributeType::Enum::ALERT_SEVERITY: {
      SetValue(::Types::AlertSeverity(itemIn->GetAlertSeverity()));
      SetSuccessfulVisit(true);
      break;
    }
    case Types::AttributeType::Enum::UPDATE_INTERVAL: {
      SetValue(itemIn->GetUpdateInterval());
      SetSuccessfulVisit(true);
      break;
    }
    case Types::AttributeType::Enum::VALUE_IN_READ: {
      SetValue(itemIn->GetVariantValueRead().GetVariantValue());
      SetSuccessfulVisit(true);
      break;
    }
    case Types::AttributeType::Enum::VALUE_IN: {
      SetValue(itemIn->GetVariantValue().GetVariantValue());
      SetSuccessfulVisit(true);
      break;
    }
    case Types::AttributeType::Enum::VALUE_IN_IS_FORCED: {
      SetValue(itemIn->IsValueInForced());
      SetSuccessfulVisit(true);
      break;
    }
    default: {
    }
  }
}

void VisitorPropertyGet::Visit(
  SystemModules::Framework::Components::ItemOutBase *itemOut) {
  switch (GetAttribute().GetValue()) {
    case Types::AttributeType::Enum::COMPONENT_TYPE: {
      SetValue(itemOut->GetComponentType());
      SetSuccessfulVisit(true);
      break;
    }
    case Types::AttributeType::Enum::NAME: {
      SetValue(itemOut->GetName());
      SetSuccessfulVisit(true);
      break;
    }
    case Types::AttributeType::Enum::DESCRIPTION: {
      SetValue(itemOut->GetDescription());
      SetSuccessfulVisit(true);
      break;
    }
    case Types::AttributeType::Enum::ALERT_SEVERITY: {
      SetValue(::Types::AlertSeverity(itemOut->GetAlertSeverity()));
      SetSuccessfulVisit(true);
      break;
    }
    case Types::AttributeType::Enum::UPDATE_INTERVAL: {
      SetValue(itemOut->GetUpdateInterval());
      SetSuccessfulVisit(true);
      break;
    }
    case Types::AttributeType::Enum::VALUE_OUT_WRITE: {
      SetValue(itemOut->GetVariantValueWrite().GetVariantValue());
      SetSuccessfulVisit(true);
      break;
    }
    case Types::AttributeType::Enum::VALUE_OUT: {
      SetValue(itemOut->GetVariantValue().GetVariantValue());
      SetSuccessfulVisit(true);
      break;
    }
    case Types::AttributeType::Enum::VALUE_OUT_IS_FORCED: {
      SetValue(itemOut->IsValueOutForced());
      SetSuccessfulVisit(true);
      break;
    }
    default: {
    }
  }
}

void VisitorPropertyGet::Visit(
  SystemModules::Framework::Components::ItemInOutBase *itemInOut) {
  switch (GetAttribute().GetValue()) {
    case Types::AttributeType::Enum::COMPONENT_TYPE: {
      SetValue(itemInOut->GetComponentType());
      SetSuccessfulVisit(true);
      break;
    }
    case Types::AttributeType::Enum::NAME: {
      SetValue(itemInOut->GetName());
      SetSuccessfulVisit(true);
      break;
    }
    case Types::AttributeType::Enum::DESCRIPTION: {
      SetValue(itemInOut->GetDescription());
      SetSuccessfulVisit(true);
      break;
    }
    case Types::AttributeType::Enum::ALERT_SEVERITY: {
      SetValue(::Types::AlertSeverity(itemInOut->GetAlertSeverity()));
      SetSuccessfulVisit(true);
      break;
    }
    case Types::AttributeType::Enum::UPDATE_INTERVAL: {
      SetValue(itemInOut->GetUpdateInterval());
      SetSuccessfulVisit(true);
      break;
    }
    case Types::AttributeType::Enum::VALUE_IN_READ: {
      SetValue(itemInOut->GetVariantValueInRead().GetVariantValue());
      SetSuccessfulVisit(true);
      break;
    }
    case Types::AttributeType::Enum::VALUE_IN: {
      SetValue(itemInOut->GetVariantValueIn().GetVariantValue());
      SetSuccessfulVisit(true);
      break;
    }
    case Types::AttributeType::Enum::VALUE_IN_IS_FORCED: {
      SetValue(itemInOut->IsValueInForced());
      SetSuccessfulVisit(true);
      break;
    }
    case Types::AttributeType::Enum::VALUE_OUT_WRITE: {
      SetValue(itemInOut->GetVariantValueOutWrite().GetVariantValue());
      SetSuccessfulVisit(true);
      break;
    }
    case Types::AttributeType::Enum::VALUE_OUT: {
      SetValue(itemInOut->GetVariantValueOutWrite().GetVariantValue());
      SetSuccessfulVisit(true);
      break;
    }
    case Types::AttributeType::Enum::VALUE_OUT_IS_FORCED: {
      SetValue(itemInOut->IsValueOutForced());
      SetSuccessfulVisit(true);
      break;
    }
    default: {
    }
  }
}

void VisitorPropertyGet::Visit(
  SystemModules::Framework::Components::ParameterItemInBase *parameterItemIn) {
  switch (GetAttribute().GetValue()) {
    case Types::AttributeType::Enum::COMPONENT_TYPE: {
      SetValue(parameterItemIn->GetComponentType());
      SetSuccessfulVisit(true);
      break;
    }
    case Types::AttributeType::Enum::NAME: {
      SetValue(parameterItemIn->GetName());
      SetSuccessfulVisit(true);
      break;
    }
    case Types::AttributeType::Enum::DESCRIPTION: {
      SetValue(parameterItemIn->GetDescription());
      SetSuccessfulVisit(true);
      break;
    }
    case Types::AttributeType::Enum::ALERT_SEVERITY: {
      SetValue(::Types::AlertSeverity(parameterItemIn->GetAlertSeverity()));
      SetSuccessfulVisit(true);
      break;
    }
    case Types::AttributeType::Enum::VALUE_IN: {
      SetValue(parameterItemIn->GetVariantValue().GetVariantValue());
      SetSuccessfulVisit(true);
      break;
    }
    default: {
    }
  }
}

void VisitorPropertyGet::Visit(
  SystemModules::Framework::Components::ParameterItemOutBase *parameterItemOut) {
  switch (GetAttribute().GetValue()) {
    case Types::AttributeType::Enum::COMPONENT_TYPE: {
      SetValue(parameterItemOut->GetComponentType());
      SetSuccessfulVisit(true);
      break;
    }
    case Types::AttributeType::Enum::NAME: {
      SetValue(parameterItemOut->GetName());
      SetSuccessfulVisit(true);
      break;
    }
    case Types::AttributeType::Enum::DESCRIPTION: {
      SetValue(parameterItemOut->GetDescription());
      SetSuccessfulVisit(true);
      break;
    }
    case Types::AttributeType::Enum::ALERT_SEVERITY: {
      SetValue(::Types::AlertSeverity(parameterItemOut->GetAlertSeverity()));
      SetSuccessfulVisit(true);
      break;
    }
    case Types::AttributeType::Enum::VALUE_OUT: {
      SetValue(parameterItemOut->GetVariantValue().GetVariantValue());
      SetSuccessfulVisit(true);
      break;
    }
    default: {
    }
  }
}

void VisitorPropertyGet::Visit(
  SystemModules::Framework::MonitoringFunctions::MFBase *monitoringFunction) {
  (void) monitoringFunction;
}

void VisitorPropertyGet::Visit(
  SystemModules::Framework::Actions::ActionBase *action) {
  (void) action;
}

void VisitorPropertyGet::Visit(
  SystemModules::Framework::Accessors::AccessorBase *accessor) {
  (void) accessor;
}

void VisitorPropertyGet::Visit(::Types::TestVariantValue *accessor) {
  (void) accessor;
}

void VisitorPropertyGet::Visit(::Types::ComponentStatus *status) {
  (void) status;
}

void VisitorPropertyGet::Visit(::Types::MonitoringFunctionStatus *status) {
  (void) status;
}

}  // namespace Messages::Visitor
