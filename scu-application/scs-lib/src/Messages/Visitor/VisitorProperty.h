/************************************************************
* @Description  :
* @Author       : ZHAW-IAMP-SKS, Philippe Hindermann
* @Copyright    :
************************************************************/

#pragma once

#include <list>
#include "Types/VariantValue.h"
#include "Types/AttributeType.h"
#include "VisitorBase.h"

namespace Messages::Visitor {
class VisitorProperty : public VisitorBase {
 public:
  explicit VisitorProperty(
    const Types::AttributeType::Enum &attribute);

  VisitorProperty(
    const Types::AttributeType::Enum &attribute,
    const ::Types::ValueType::Enum &valueType,
    const ::Types::TestVariantValue &value);

  ~VisitorProperty() override = default;

  [[nodiscard]] Types::AttributeType GetAttribute() const;

  [[nodiscard]] ::Types::ValueType GetValueType() const;

  [[nodiscard]] ::Types::TestVariantValue GetVariantValue() const;

  template<typename T>
  void SetValue(const T &value);

 private:
  Types::AttributeType attribute_;

  ::Types::ValueType valueType_;

  ::Types::TestVariantValue value_;
};

template<typename T>
void VisitorProperty::SetValue(const T &value) {
  value_.template SetValue(value);
}

}  // namespace Messages::Visitor
