/************************************************************
* @Description  :
* @Author       : ZHAW-IAMP-SKS, Philippe Hindermann
* @Copyright    :
************************************************************/

#include "VisitorJsonBase.h"

namespace Messages::Visitor {

VisitorJsonBase::VisitorJsonBase()
  : VisitorBase() {
}

}  // namespace Messages::Visitor
