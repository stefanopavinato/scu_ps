/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <variant>
#include "Utils/TimeFormatter.h"
#include "Types/ComponentStatus.h"
#include "WorkloadBalancing/WorkloadBalanceHandler.h"
#include "WorkloadBalancing/ItemHandler.h"
#include "SystemModules/Framework/Accessors/AccessorBase.h"
#include "SystemModules/Framework/MonitoringFunctions/MFBase.h"
#include "SystemModules/Framework/Actions/ActionBase.h"
#include "SystemModules/Framework/Components/ItemInBase.h"
#include "SystemModules/Framework/Components/ItemOutBase.h"
#include "SystemModules/Framework/Components/ItemInOutBase.h"
#include "SystemModules/Framework/Components/ParameterItemInBase.h"
#include "SystemModules/Framework/Components/ParameterItemOutBase.h"
#include "SystemModules/Framework/Components/Container.h"
#include "SystemModules/Framework/Components/ModuleBase.h"
#include "SystemModules/Framework/Components/ModuleHandler.h"
#include "VisitorOverview.h"

namespace Messages::Visitor {

const char VisitorOverview::kFailure[] = "Failure";
const char VisitorOverview::kVisitFailure[] = "Failed to visit components.";
const char VisitorOverview::kNoDataCollected[] = "No data collected.";

VisitorOverview::VisitorOverview(
  const uint32_t &maxLevel,
  const ::Types::JsonDetails &details)
  : level_(0), maxLevel_(maxLevel), details_(details) {
}

::Json::Value VisitorOverview::GetJson() const {
  if (!SuccessfulVisit()) {
    Json::Value retVal;
    retVal[kFailure] = kVisitFailure;
    return retVal;
  }
  if (jsonStack_.empty()) {
    Json::Value retVal;
    retVal[kFailure] = kNoDataCollected;
    return retVal;
  }
  return *jsonStack_.top();
}

void VisitorOverview::Visit(WorkloadBalancing::WorkloadBalanceHandler *workloadBalanceHandler) {
  auto jsonValue = std::make_unique<Json::Value>();
  switch (details_.GetEnum()) {
    case Types::JsonDetails::Enum::VERBOSE:
    case Types::JsonDetails::Enum::COMPACT:
    case Types::JsonDetails::Enum::MINIMAL:
    case Types::JsonDetails::Enum::CONFIG: {
      // Front handle
      // Set name
      (*jsonValue)[kName] = workloadBalanceHandler->GetName();
      // Set component type
      (*jsonValue)[kComponentType] = workloadBalanceHandler->GetComponentType();
      // Set components
      /*
      if (level_ < maxLevel_) {
        level_++;
        uint32_t i = 0;
        for (const auto &component : *workloadBalanceHandler->GetModules()) {
          component.second->Accept(this);
          (*jsonValue)[kModules][i++] = *jsonStack_.top();
          jsonStack_.pop();
        }
        level_--;
      }
      */
    }
  }
  // Push json value
  jsonStack_.push(std::move(jsonValue));
  SetSuccessfulVisit(true);
}

void VisitorOverview::Visit(WorkloadBalancing::ItemHandler *itemHandler) {
  auto jsonValue = std::make_unique<Json::Value>();
  switch (details_.GetEnum()) {
    case Types::JsonDetails::Enum::VERBOSE:
    case Types::JsonDetails::Enum::COMPACT: {
      // Set description
//      (*jsonValue)[kDescription] = itemHandler->GetDescription();
    }
    case Types::JsonDetails::Enum::MINIMAL: {
      // Set pause status
      (*jsonValue)[kIsPaused] = itemHandler->IsPaused();
    }
    case Types::JsonDetails::Enum::CONFIG: {
      // Set name
      (*jsonValue)[kDescription] = itemHandler->GetName();
      // Set component type
      (*jsonValue)[kComponentType] = itemHandler->GetComponentType();
    }
  }
  // Push json value
  jsonStack_.push(std::move(jsonValue));
  SetSuccessfulVisit(true);
}

void VisitorOverview::Visit(SystemModules::Framework::Components::ModuleHandler *moduleHandler) {
  auto jsonValue = std::make_unique<Json::Value>();
  switch (details_.GetEnum()) {
    case Types::JsonDetails::Enum::VERBOSE: {
    }
    case Types::JsonDetails::Enum::COMPACT: {
      // Set description
      (*jsonValue)[kDescription] = moduleHandler->GetDescription();
      // Group alerts
      {
        uint32_t i = 0;
        for (const auto &group : *moduleHandler->GetGroupAlertSeverities()) {
          (*jsonValue)[kGroupAlertSeverities][i][kGroup] = Types::ValueGroup(group.first).ToString();
          (*jsonValue)[kGroupAlertSeverities][i++][kAlertSeverity] = group.second.ToString();
        }
      }
    }
    case Types::JsonDetails::Enum::MINIMAL:
    case Types::JsonDetails::Enum::CONFIG: {
      // Set name
      (*jsonValue)[kName] = moduleHandler->GetName();
      // Set component type
      (*jsonValue)[kComponentType] = moduleHandler->GetComponentType();
      // Set components
      if (level_ < maxLevel_) {
        level_++;
        uint32_t i = 0;
        for (const auto &component : *moduleHandler->GetModules()) {
          component.second->Accept(this);
          (*jsonValue)[kModules][i++] = *jsonStack_.top();
          jsonStack_.pop();
        }
        level_--;
      }
    }
  }
  // Push json value
  jsonStack_.push(std::move(jsonValue));
  SetSuccessfulVisit(true);
}

void VisitorOverview::Visit(SystemModules::Framework::Components::ModuleBase *module) {
  auto jsonValue = std::make_unique<Json::Value>();
  switch (details_.GetEnum()) {
    case Types::JsonDetails::Enum::VERBOSE: {
      // Set state
      (*jsonValue)[kState] = module->GetStateId().ToString();
    }
    case Types::JsonDetails::Enum::COMPACT: {
      // Set alert severity
      (*jsonValue)[kAlertSeverity] = Types::AlertSeverity(module->GetAlertSeverity()).ToString();
      // Set description
      (*jsonValue)[kDescription] = module->GetDescription();
    }
    case Types::JsonDetails::Enum::MINIMAL:
    case Types::JsonDetails::Enum::CONFIG: {
      // Set name
      (*jsonValue)[kName] = module->GetName();
      // Set component type
      (*jsonValue)[kComponentType] = module->GetComponentType();
      // Set components
      if (level_ < maxLevel_) {
        level_++;
        uint32_t i = 0;
        for (const auto &component : *module->GetComponents()) {
          component.second->Accept(this);
          (*jsonValue)[kComponents][i++] = *jsonStack_.top();
          jsonStack_.pop();
        }
        level_--;
      }
    }
  }
  // Push json value
  jsonStack_.push(std::move(jsonValue));
  SetSuccessfulVisit(true);
}

void VisitorOverview::Visit(SystemModules::Framework::Components::Container *container) {
  auto jsonValue = std::make_unique<Json::Value>();
  switch (details_.GetEnum()) {
    case Types::JsonDetails::Enum::VERBOSE: {
    }
    case Types::JsonDetails::Enum::COMPACT: {
      // Set alert severity
      (*jsonValue)[kAlertSeverity] = Types::AlertSeverity(container->GetAlertSeverity()).ToString();
      // Set description
      (*jsonValue)[kDescription] = container->GetDescription();
    }
    case Types::JsonDetails::Enum::MINIMAL:
    case Types::JsonDetails::Enum::CONFIG: {
      // Set name
      (*jsonValue)[kName] = container->GetName();
      // Set type
      (*jsonValue)[kComponentType] = container->GetComponentType();
      // Set components
      if (level_ < maxLevel_) {
        level_++;
        uint32_t i = 0;
        for (const auto &component : *container->GetComponents()) {
          component.second->Accept(this);
          (*jsonValue)[kComponents][i++] = *jsonStack_.top();
          jsonStack_.pop();
        }
        level_--;
      }
    }
  }
  // Push json value
  jsonStack_.push(std::move(jsonValue));
  SetSuccessfulVisit(true);
}

void VisitorOverview::Visit(SystemModules::Framework::Components::ItemInBase *itemIn) {
  auto jsonValue = std::make_unique<Json::Value>();
  switch (details_.GetEnum()) {
    case Types::JsonDetails::Enum::VERBOSE: {
      // Set monitoring functions
      uint32_t i = 0;
      for (const auto &monitoringFunction : itemIn->GetMonitoringFunctions()) {
        monitoringFunction->Accept(this);
        (*jsonValue)[kMonitoringFunctions][i++] = *jsonStack_.top();
        jsonStack_.pop();
      }
      // Set accessor
      auto accessor = itemIn->GetAccessor();
      if (accessor != nullptr) {
        accessor->Accept(this);
        (*jsonValue)[kAccessor] = *jsonStack_.top();
        jsonStack_.pop();
      } else {
        (*jsonValue)[kAccessor] = "not set";
      }
      // Set update interval
      (*jsonValue)[kUpdateInterval] = itemIn->GetUpdateInterval().count();
      // Set value type
      (*jsonValue)[kValueType] = itemIn->GetValueType().ToString();
    }
    case Types::JsonDetails::Enum::COMPACT: {
      // Set description
      (*jsonValue)[kDescription] = itemIn->GetDescription();
      // Set group
      (*jsonValue)[kGroup] = itemIn->GetGroup().ToString();
      // Set value forced indicator
      (*jsonValue)[kIsValueInForced] = itemIn->IsValueInForced();
      // Set value in read
      (*jsonValue)[kValueInRead] = itemIn->GetVariantValueRead().ToString();
      // Set alert severity
      (*jsonValue)[kAlertSeverity] = Types::AlertSeverity(itemIn->GetAlertSeverity()).ToString();
    }
    case Types::JsonDetails::Enum::MINIMAL: {
      // Set value in
      (*jsonValue)[kIsValid] = itemIn->IsValid();
      (*jsonValue)[kValueIn] = itemIn->GetVariantValue().ToString();
    }
    case Types::JsonDetails::Enum::CONFIG: {
      // Set name
      (*jsonValue)[kName] = itemIn->GetName();
      // Set component type
      (*jsonValue)[kComponentType] = itemIn->GetComponentType();
    }
  }
  // Push json value
  jsonStack_.push(std::move(jsonValue));
  SetSuccessfulVisit(true);
}

void VisitorOverview::Visit(SystemModules::Framework::Components::ItemOutBase *itemOut) {
  auto jsonValue = std::make_unique<Json::Value>();
  switch (details_.GetEnum()) {
    case Types::JsonDetails::Enum::VERBOSE: {
      // Set action
      auto action = itemOut->GetAction();
      if (action != nullptr) {
        action->Accept(this);
        (*jsonValue)[kAction] = *jsonStack_.top();
        jsonStack_.pop();
      } else {
        (*jsonValue)[kAction] = "not set";
      }
      // Set accessor
      auto accessor = itemOut->GetAccessor();
      if (accessor != nullptr) {
        accessor->Accept(this);
        (*jsonValue)[kAccessor] = *jsonStack_.top();
        jsonStack_.pop();
      } else {
        (*jsonValue)[kAccessor] = "not set";
      }
      // Set update interval
      (*jsonValue)[kUpdateInterval] = itemOut->GetUpdateInterval().count();
      // Set value type
      (*jsonValue)[kValueType] = itemOut->GetValueType().ToString();
    }
    case Types::JsonDetails::Enum::COMPACT: {
      // Set description
      (*jsonValue)[kDescription] = itemOut->GetDescription();
      // Set group
      (*jsonValue)[kGroup] = itemOut->GetGroup().ToString();
      // Set value forced indicator
      (*jsonValue)[kIsValueOutForced] = itemOut->IsValueOutForced();
      // Set value out
      (*jsonValue)[kValueOut] = itemOut->GetVariantValue().ToString();
      // Set alert severity
      (*jsonValue)[kAlertSeverity] = Types::AlertSeverity(itemOut->GetAlertSeverity()).ToString();
    }
    case Types::JsonDetails::Enum::MINIMAL: {
      // Set value out write
      (*jsonValue)[kIsValid] = itemOut->IsValid();
      (*jsonValue)[kValueOutWrite] = itemOut->GetVariantValueWrite().ToString();
    }
    case Types::JsonDetails::Enum::CONFIG: {
      // Set name
      (*jsonValue)[kName] = itemOut->GetName();
      // Set component type
      (*jsonValue)[kComponentType] = itemOut->GetComponentType();
    }
  }
  // Push json value
  jsonStack_.push(std::move(jsonValue));
  SetSuccessfulVisit(true);
}

void VisitorOverview::Visit(SystemModules::Framework::Components::ItemInOutBase *itemInOut) {
  auto jsonValue = std::make_unique<Json::Value>();
  switch (details_.GetEnum()) {
    case Types::JsonDetails::Enum::VERBOSE: {
      // Set monitoring functions
      uint32_t i = 0;
      for (const auto &monitoringFunction : itemInOut->GetMonitoringFunctions()) {
        monitoringFunction->Accept(this);
        (*jsonValue)[kMonitoringFunctions][i++] = *jsonStack_.top();
        jsonStack_.pop();
      }
      // Set action
      auto action = itemInOut->GetAction();
      if (action != nullptr) {
        action->Accept(this);
        (*jsonValue)[kAction] = *jsonStack_.top();
        jsonStack_.pop();
      } else {
        (*jsonValue)[kAction] = "not set";
      }
      // Set accessor
      auto accessor = itemInOut->GetAccessor();
      if (accessor != nullptr) {
        accessor->Accept(this);
        (*jsonValue)[kAccessor] = *jsonStack_.top();
        jsonStack_.pop();
      } else {
        (*jsonValue)[kAccessor] = "not set";
      }
      // Set update interval
      (*jsonValue)[kUpdateInterval] = itemInOut->GetUpdateInterval().count();
      // Set value type
      (*jsonValue)[kValueType] = itemInOut->GetValueType().ToString();
    }
    case Types::JsonDetails::Enum::COMPACT: {
      // Set description
      (*jsonValue)[kDescription] = itemInOut->GetDescription();
      // Set group
      (*jsonValue)[kGroup] = itemInOut->GetGroup().ToString();
      // Set value forced indicator
      (*jsonValue)[kIsValueInForced] = itemInOut->IsValueInForced();
      (*jsonValue)[kIsValueOutForced] = itemInOut->IsValueOutForced();
      // Set value in read
      (*jsonValue)[kValueInRead] = itemInOut->GetVariantValueInRead().ToString();
      (*jsonValue)[kValueOut] = itemInOut->GetVariantValueOut().ToString();
      // Set alert severity
      (*jsonValue)[kAlertSeverity] = Types::AlertSeverity(itemInOut->GetAlertSeverity()).ToString();
    }
    case Types::JsonDetails::Enum::MINIMAL: {
      // Set value in
      (*jsonValue)[kIsValid] = itemInOut->IsValid();
      (*jsonValue)[kValueIn] = itemInOut->GetVariantValueIn().ToString();
      (*jsonValue)[kValueOutWrite] = itemInOut->GetVariantValueOutWrite().ToString();
    }
    case Types::JsonDetails::Enum::CONFIG: {
      // Set name
      (*jsonValue)[kName] = itemInOut->GetName();
      // Set component type
      (*jsonValue)[kComponentType] = itemInOut->GetComponentType();
    }
  }
  // Push json value
  jsonStack_.push(std::move(jsonValue));
  SetSuccessfulVisit(true);
}

void VisitorOverview::Visit(
  SystemModules::Framework::Components::ParameterItemInBase *parameterItemIn) {
  auto jsonValue = std::make_unique<Json::Value>();
  switch (details_.GetEnum()) {
    case Types::JsonDetails::Enum::VERBOSE: {
      // Set accessor
      auto accessor = parameterItemIn->GetAccessor();
      if (accessor != nullptr) {
        accessor->Accept(this);
        (*jsonValue)[kAccessor] = *jsonStack_.top();
        jsonStack_.pop();
      } else {
        (*jsonValue)[kAccessor] = "not set";
      }
      // Set value type
//      (*jsonValue)[kValueType] = parameterItemIn->GetValueType().ToString();
    }
    case Types::JsonDetails::Enum::COMPACT: {
      // Set description
      (*jsonValue)[kDescription] = parameterItemIn->GetDescription();
    }
    case Types::JsonDetails::Enum::MINIMAL: {
      // Set value in
      (*jsonValue)[kValueIn] = parameterItemIn->GetVariantValue().ToString();
    }
    case Types::JsonDetails::Enum::CONFIG: {
      // Set name
      (*jsonValue)[kName] = parameterItemIn->GetName();
      // Set component type
      (*jsonValue)[kComponentType] = parameterItemIn->GetComponentType();
    }
  }
  // Push json value
  jsonStack_.push(std::move(jsonValue));
  SetSuccessfulVisit(true);
}

void VisitorOverview::Visit(
  SystemModules::Framework::Components::ParameterItemOutBase *parameterItemOut) {
  auto jsonValue = std::make_unique<Json::Value>();
  switch (details_.GetEnum()) {
    case Types::JsonDetails::Enum::VERBOSE: {
      // Set accessor
      auto accessor = parameterItemOut->GetAccessor();
      if (accessor != nullptr) {
        accessor->Accept(this);
        (*jsonValue)[kAccessor] = *jsonStack_.top();
        jsonStack_.pop();
      } else {
        (*jsonValue)[kAccessor] = "not set";
      }
      // Set value type
//      (*jsonValue)[kValueType] = parameterItemOut->GetValueType().ToString();
    }
    case Types::JsonDetails::Enum::COMPACT: {
      // Set description
      (*jsonValue)[kDescription] = parameterItemOut->GetDescription();
    }
    case Types::JsonDetails::Enum::MINIMAL: {
      // Set value in
      (*jsonValue)[kValueOut] = parameterItemOut->GetVariantValue().ToString();
    }
    case Types::JsonDetails::Enum::CONFIG: {
      // Set name
      (*jsonValue)[kName] = parameterItemOut->GetName();
      // Set component type
      (*jsonValue)[kComponentType] = parameterItemOut->GetComponentType();
    }
  }
  // Push json value
  jsonStack_.push(std::move(jsonValue));
  SetSuccessfulVisit(true);
}

void VisitorOverview::Visit(SystemModules::Framework::MonitoringFunctions::MFBase *monitoringFunction) {
  auto jsonValue = std::make_unique<Json::Value>();
  (*jsonValue)[kComponentType] = monitoringFunction->GetComponentType();
  (*jsonValue)[kOnDelay] = monitoringFunction->GetOnDelay().count();
  (*jsonValue)[kOffDelay] = monitoringFunction->GetOffDelay().count();
  (*jsonValue)[kOnEvent] = Utils::TimeFormatter::ToString(monitoringFunction->GetOnEventTime());
  (*jsonValue)[kOffEvent] = Utils::TimeFormatter::ToString(monitoringFunction->GetOffEventTime());
  (*jsonValue)[kIsActive] = monitoringFunction->IsActive();
  (*jsonValue)[kCompareValues] = monitoringFunction->GetCompareValues();
  // Set status
  monitoringFunction->Status().Accept(this);
  (*jsonValue)[kStatus] = *jsonStack_.top();
  // Push json value
  jsonStack_.push(std::move(jsonValue));
  SetSuccessfulVisit(true);
}

void VisitorOverview::Visit(SystemModules::Framework::Actions::ActionBase *action) {
  auto jsonValue = std::make_unique<Json::Value>();
  (*jsonValue)[kName] = action->GetDescription();
  // Push json value
  jsonStack_.push(std::move(jsonValue));
  SetSuccessfulVisit(true);
}

void VisitorOverview::Visit(SystemModules::Framework::Accessors::AccessorBase *accessor) {
  auto jsonValue = std::make_unique<Json::Value>();
  (*jsonValue)[kComponentType] = accessor->GetTypeName();
  (*jsonValue)[kDescription] = accessor->GetDescription();
  (*jsonValue)[kIsInitialized] = accessor->IsInitialized();
  // Push json value
  jsonStack_.push(std::move(jsonValue));
  SetSuccessfulVisit(true);
}

void VisitorOverview::Visit(::Types::TestVariantValue *variantValue) {
  auto jsonValue = std::make_unique<Json::Value>();
  (*jsonValue)[kValue] = variantValue->ToString();
  // Push json value
  jsonStack_.push(std::move(jsonValue));
  SetSuccessfulVisit(true);
}

void VisitorOverview::Visit(::Types::ComponentStatus *status) {
  auto jsonValue = std::make_unique<Json::Value>();
  (*jsonValue)[kId] = static_cast<uint32_t>(status->GetId().GetEnum());
  (*jsonValue)[kIsValid] = status->IsValid();
  (*jsonValue)[kAlertSeverity] = status->AlertSeverity().ToString();
  (*jsonValue)[kDescription] = status->GetId().ToString();
  (*jsonValue)[kEvent] = Utils::TimeFormatter::ToString(status->GetTimeStamp());
  // Push json value
  jsonStack_.push(std::move(jsonValue));
  SetSuccessfulVisit(true);
}

void VisitorOverview::Visit(::Types::MonitoringFunctionStatus *status) {
  auto jsonValue = std::make_unique<Json::Value>();
  (*jsonValue)[kId] = status->Id().ToString();
  (*jsonValue)[kGroup] = status->Group().ToString();
  (*jsonValue)[kAlertSeverity] = status->AlertSeverity().ToString();
  (*jsonValue)[kDescription] = status->Description();
  // Push json value
  jsonStack_.push(std::move(jsonValue));
  SetSuccessfulVisit(true);
}

}  // namespace Messages::Visitor
