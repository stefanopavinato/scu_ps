/************************************************************
* @Description  :
* @Author       : ZHAW-IAMP-SKS, Philippe Hindermann
* @Copyright    :
************************************************************/

#pragma once

#include "Types/VariantValue.h"
#include "Types/AttributeType.h"
#include "VisitorProperty.h"

namespace Messages::Visitor {
class VisitorPropertySet : public VisitorProperty {
 public:
  VisitorPropertySet(
    const Types::AttributeType::Enum &attribute,
    const ::Types::ValueType::Enum &valueType,
    const ::Types::TestVariantValue &value);

  ~VisitorPropertySet() override = default;

  void Visit(WorkloadBalancing::WorkloadBalanceHandler *workloadBalanceHandler) override;

  void Visit(WorkloadBalancing::ItemHandler *itemHandler) override;

  void Visit(SystemModules::Framework::Components::ModuleHandler *moduleHandler) override;

  void Visit(SystemModules::Framework::Components::ModuleBase *module) override;

  void Visit(SystemModules::Framework::Components::Container *container) override;

  void Visit(SystemModules::Framework::Components::ItemInBase *itemIn) override;

  void Visit(SystemModules::Framework::Components::ItemOutBase *itemOut) override;

  void Visit(SystemModules::Framework::Components::ItemInOutBase *itemInOut) override;

  void Visit(SystemModules::Framework::Components::ParameterItemInBase *parameterItemIn) override;

  void Visit(SystemModules::Framework::Components::ParameterItemOutBase *parameterItemOut) override;

  void Visit(SystemModules::Framework::MonitoringFunctions::MFBase *monitoringFunction) override;

  void Visit(SystemModules::Framework::Actions::ActionBase *action) override;

  void Visit(SystemModules::Framework::Accessors::AccessorBase *accessor) override;

  void Visit(::Types::TestVariantValue *accessor) override;

  void Visit(::Types::ComponentStatus *status) override;

  void Visit(::Types::MonitoringFunctionStatus *status) override;
};

}  // namespace Messages::Visitor
