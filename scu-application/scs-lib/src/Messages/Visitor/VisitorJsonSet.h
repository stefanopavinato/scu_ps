/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <json/json.h>
#include "Types/VariantValue.h"
#include "Types/AttributeType.h"
#include "VisitorJsonBase.h"

namespace Messages::Visitor {
class VisitorJsonSet : public VisitorJsonBase {
 public:
  VisitorJsonSet(
    const Types::AttributeType::Enum &attribute,
    const ::Types::ValueType::Enum &valueType,
    ::Types::TestVariantValue value);

  ~VisitorJsonSet() override = default;

  [[nodiscard]] ::Json::Value GetJson() const override;

  void Visit(WorkloadBalancing::WorkloadBalanceHandler *workloadBalanceHandler) override;

  void Visit(WorkloadBalancing::ItemHandler *itemHandler) override;

  void Visit(SystemModules::Framework::Components::ModuleHandler *moduleHandler) override;

  void Visit(SystemModules::Framework::Components::ModuleBase *module) override;

  void Visit(SystemModules::Framework::Components::Container *container) override;

  void Visit(SystemModules::Framework::Components::ItemInBase *itemIn) override;

  void Visit(SystemModules::Framework::Components::ItemOutBase *itemOut) override;

  void Visit(SystemModules::Framework::Components::ItemInOutBase *itemInOut) override;

  void Visit(SystemModules::Framework::Components::ParameterItemInBase *parameterItemIn) override;

  void Visit(SystemModules::Framework::Components::ParameterItemOutBase *parameterItemOut) override;

  void Visit(SystemModules::Framework::MonitoringFunctions::MFBase *monitoringFunction) override;

  void Visit(SystemModules::Framework::Actions::ActionBase *action) override;

  void Visit(SystemModules::Framework::Accessors::AccessorBase *accessor) override;

  void Visit(::Types::TestVariantValue *accessor) override;

  void Visit(::Types::ComponentStatus *status) override;

  void Visit(::Types::MonitoringFunctionStatus *status) override;

 private:
  Types::AttributeType attribute_;

  ::Types::ValueType valueType_;

  ::Types::TestVariantValue value_;

  static const char kResult[];
  static const char kSuccess[];
  static const char kFailure[];
};
}  // namespace Messages::Visitor
