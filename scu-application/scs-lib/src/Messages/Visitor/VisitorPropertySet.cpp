/************************************************************
* @Description  :
* @Author       : ZHAW-IAMP-SKS, Philippe Hindermann
* @Copyright    :
************************************************************/

#include "SystemModules/Framework/MonitoringFunctions/MFBase.h"
#include "SystemModules/Framework/Components/Container.h"
#include "SystemModules/Framework/Components/ModuleBase.h"
#include "SystemModules/Framework/Components/ItemInBase.h"
#include "SystemModules/Framework/Components/ItemOutBase.h"
#include "SystemModules/Framework/Components/ItemInOutBase.h"
#include "VisitorPropertySet.h"

namespace Messages::Visitor {

VisitorPropertySet::VisitorPropertySet(
  const Types::AttributeType::Enum &attribute,
  const ::Types::ValueType::Enum &valueType,
  const ::Types::TestVariantValue &value)
  : VisitorProperty(
  attribute,
  valueType,
  value) {
}

void VisitorPropertySet::Visit(WorkloadBalancing::WorkloadBalanceHandler *workloadBalanceHandler) {
  (void) workloadBalanceHandler;
}

void VisitorPropertySet::Visit(WorkloadBalancing::ItemHandler *itemHandler) {
  (void) itemHandler;
}

void VisitorPropertySet::Visit(
  SystemModules::Framework::Components::ModuleHandler *moduleHandler) {
  (void) moduleHandler;
}

void VisitorPropertySet::Visit(
  SystemModules::Framework::Components::ModuleBase *module) {
  switch (GetAttribute().GetValue()) {
    case Types::AttributeType::Enum::ALERT_SEVERITY: {
      module->ResetAlertSeverity();
      SetSuccessfulVisit(true);
      break;
    }
    default: {
    }
  }
}

void VisitorPropertySet::Visit(
  SystemModules::Framework::Components::Container *container) {
  switch (GetAttribute().GetValue()) {
    case Types::AttributeType::Enum::ALERT_SEVERITY: {
      container->ResetAlertSeverity();
      SetSuccessfulVisit(true);
      break;
    }
    default: {
    }
  }
}

void VisitorPropertySet::Visit(
  SystemModules::Framework::Components::ItemInBase *itemIn) {
  switch (GetAttribute().GetValue()) {
    case Types::AttributeType::Enum::UPDATE_INTERVAL: {
      SetSuccessfulVisit(
        itemIn->SetUpdateInterval(
          GetVariantValue().GetVariantValue()));
      break;
    }
    case Types::AttributeType::Enum::VALUE_IN_FORCE: {
      SetSuccessfulVisit(
        itemIn->ForceValue(
          GetVariantValue().GetVariantValue(),
          ::Types::ForceStatus::Enum::PERMANENT));
      break;
    }
    case Types::AttributeType::Enum::VALUE_IN_FORCE_ONCE: {
      SetSuccessfulVisit(
        itemIn->ForceValue(
          GetVariantValue().GetVariantValue(),
          ::Types::ForceStatus::Enum::ONCE));
      break;
    }
    case Types::AttributeType::Enum::VALUE_IN_FORCE_RELEASE: {
      SetSuccessfulVisit(
        itemIn->ReleaseValueForced());
      break;
    }
    case Types::AttributeType::Enum::ALERT_SEVERITY: {
      itemIn->ResetAlertSeverity();
      SetSuccessfulVisit(true);
      break;
    }
    default: {
    }
  }
}

void VisitorPropertySet::Visit(
  SystemModules::Framework::Components::ItemOutBase *itemOut) {
  switch (GetAttribute().GetValue()) {
    case Types::AttributeType::Enum::UPDATE_INTERVAL: {
      SetSuccessfulVisit(
        itemOut->SetUpdateInterval(
          GetVariantValue().GetVariantValue()));
      break;
    }
    case Types::AttributeType::Enum::VALUE_OUT_FORCE: {
      SetSuccessfulVisit(
        itemOut->ForceValue(
          GetVariantValue().GetVariantValue(),
          ::Types::ForceStatus::Enum::PERMANENT));
      break;
    }
    case Types::AttributeType::Enum::VALUE_OUT_FORCE_ONCE: {
      SetSuccessfulVisit(
        itemOut->ForceValue(
          GetVariantValue().GetVariantValue(),
          ::Types::ForceStatus::Enum::ONCE));
      break;
    }
    case Types::AttributeType::Enum::VALUE_OUT_FORCE_RELEASE: {
      SetSuccessfulVisit(
        itemOut->ReleaseValueForced());
      break;
    }
    case Types::AttributeType::Enum::ALERT_SEVERITY: {
      itemOut->ResetAlertSeverity();
      SetSuccessfulVisit(true);
      break;
    }
    default: {
    }
  }
}

void VisitorPropertySet::Visit(
  SystemModules::Framework::Components::ItemInOutBase *itemInOut) {
  switch (GetAttribute().GetValue()) {
    case Types::AttributeType::Enum::UPDATE_INTERVAL: {
      SetSuccessfulVisit(
        itemInOut->SetUpdateInterval(
          GetVariantValue().GetVariantValue()));
      break;
    }
    case Types::AttributeType::Enum::VALUE_IN_FORCE: {
      SetSuccessfulVisit(
        itemInOut->ForceValueIn(
          GetVariantValue().GetVariantValue(),
          ::Types::ForceStatus::Enum::PERMANENT));
      break;
    }
    case Types::AttributeType::Enum::VALUE_IN_FORCE_ONCE: {
      SetSuccessfulVisit(
        itemInOut->ForceValueIn(
          GetVariantValue().GetVariantValue(),
          ::Types::ForceStatus::Enum::ONCE));
      break;
    }
    case Types::AttributeType::Enum::VALUE_IN_FORCE_RELEASE: {
      SetSuccessfulVisit(
        itemInOut->ReleaseValueInForced());
      break;
    }
    case Types::AttributeType::Enum::VALUE_OUT_FORCE: {
      SetSuccessfulVisit(
        itemInOut->ForceValueOut(
          GetVariantValue().GetVariantValue(),
          ::Types::ForceStatus::Enum::PERMANENT));
      break;
    }
    case Types::AttributeType::Enum::VALUE_OUT_FORCE_ONCE: {
      SetSuccessfulVisit(
        itemInOut->ForceValueOut(
          GetVariantValue().GetVariantValue(),
          ::Types::ForceStatus::Enum::ONCE));
      break;
    }
    case Types::AttributeType::Enum::VALUE_OUT_FORCE_RELEASE: {
      SetSuccessfulVisit(
        itemInOut->ReleaseValueOutForced());
      break;
    }
    case Types::AttributeType::Enum::ALERT_SEVERITY: {
      itemInOut->ResetAlertSeverity();
      SetSuccessfulVisit(true);
      break;
    }
    default: {
    }
  }
}

void VisitorPropertySet::Visit(
  SystemModules::Framework::Components::ParameterItemInBase *parameterItemIn) {
  (void) parameterItemIn;
  SetSuccessfulVisit(false);
}

void VisitorPropertySet::Visit(
  SystemModules::Framework::Components::ParameterItemOutBase *parameterItemOut) {
  (void) parameterItemOut;
  SetSuccessfulVisit(false);
}

void VisitorPropertySet::Visit(
  SystemModules::Framework::MonitoringFunctions::MFBase *monitoringFunction) {
  switch (GetAttribute().GetValue()) {
    case Types::AttributeType::Enum::STATUS_RESET: {
      monitoringFunction->RequestReset();
      SetSuccessfulVisit(true);
      break;
    }
    default: {
    }
  }
}

void VisitorPropertySet::Visit(
  SystemModules::Framework::Actions::ActionBase *action) {
  (void) action;
}

void VisitorPropertySet::Visit(
  SystemModules::Framework::Accessors::AccessorBase *accessor) {
  (void) accessor;
}

void VisitorPropertySet::Visit(::Types::TestVariantValue *accessor) {
  (void) accessor;
}

void VisitorPropertySet::Visit(::Types::ComponentStatus *status) {
  (void) status;
}

void VisitorPropertySet::Visit(::Types::MonitoringFunctionStatus *status) {
  (void) status;
}

}  // namespace Messages::Visitor
