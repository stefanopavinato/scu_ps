/************************************************************
* @Description  :
* @Author       : ZHAW-IAMP-SKS, Philippe Hindermann
* @Copyright    :
************************************************************/

#include "VisitorBase.h"

namespace Messages::Visitor {
// Components
const char VisitorBase::kComponents[] = "Components";
const char VisitorBase::kComponentType[] = "ComponentType";
const char VisitorBase::kName[] = "Name";
const char VisitorBase::kDescription[] = "Description";
const char VisitorBase::kAlertSeverity[] = "AlertSeverity";
const char VisitorBase::kId[] = "Id";
const char VisitorBase::kIsValid[] = "IsValid";
const char VisitorBase::kGroupAlertSeverities[] = "GroupAlertSeverities";
// ModuleHandler
const char VisitorBase::kModules[] = "Modules";
// Items
const char VisitorBase::kGroup[] = "Group";
const char VisitorBase::kStatus[] = "Status";
const char VisitorBase::kUpdateInterval[] = "UpdateInterval";
const char VisitorBase::kValueType[] = "ValueType";
const char VisitorBase::kValue[] = "Value";
const char VisitorBase::kValueInRead[] = "ValueInRead";
const char VisitorBase::kValueIn[] = "ValueIn";
const char VisitorBase::kValueOut[] = "ValueOut";
const char VisitorBase::kValueOutWrite[] = "ValueOutWrite";
const char VisitorBase::kIsValueOutForced[] = "ValueOutForced";
const char VisitorBase::kIsValueInForced[] = "ValueInForced";
// State machine
const char VisitorBase::kState[] = "State";
// Accessors
const char VisitorBase::kAccessor[] = "Accessor";
const char VisitorBase::kIsInitialized[] = "IsInitialized";
// Actions
const char VisitorBase::kAction[] = "Action";
// Monitoring functions
const char VisitorBase::kMonitoringFunctions[] = "MonitoringFunctions";
const char VisitorBase::kIsActive[] = "IsActive";
const char VisitorBase::kCompareValues[] = "CompareValues";

const char VisitorBase::kOnDelay[] = "OnDelay";
const char VisitorBase::kOffDelay[] = "OffDelay";

const char VisitorBase::kEvent[] = "Event";
const char VisitorBase::kOnEvent[] = "OnEvent";
const char VisitorBase::kOffEvent[] = "OffEvent";

const char VisitorBase::kIsPaused[] = "IsPaused";

VisitorBase::VisitorBase()
  : successfulVisit_(false) {
}

bool VisitorBase::SuccessfulVisit() const {
  return successfulVisit_;
}

void VisitorBase::SetSuccessfulVisit(const bool &value) {
  successfulVisit_ = value;
}

}  // namespace Messages::Visitor
