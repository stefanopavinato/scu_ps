/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <experimental/optional>
#include "Messages/Visitor/VisitorOverview.h"
#include "Messages/Visitor/VisitorProperty.h"
#include "MessageItem.h"

namespace Messages {
class MessageItemProperty : public MessageItem {
 public:
  MessageItemProperty(
    const std::string &name,
    const std::string &path,
    std::unique_ptr<Messages::Visitor::VisitorProperty> visitor);

  template<typename T>
  std::experimental::optional<T> GetValue() const;

 protected:
  Messages::Visitor::VisitorBase *Visitor() override;

 private:
  std::unique_ptr<Messages::Visitor::VisitorProperty> visitor_;
};

template<typename T>
std::experimental::optional<T> MessageItemProperty::GetValue() const {
  return visitor_->GetVariantValue().template GetValue<T>();
}
}  // namespace Messages
