/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "MessageItemProperty.h"

namespace Messages {

MessageItemProperty::MessageItemProperty(
  const std::string &name,
  const std::string &path,
  std::unique_ptr<Messages::Visitor::VisitorProperty> visitor)
  : MessageItem(
  name,
  path), visitor_(std::move(visitor)) {
}

Messages::Visitor::VisitorBase *MessageItemProperty::Visitor() {
  return visitor_.get();
}

}  // namespace Messages
