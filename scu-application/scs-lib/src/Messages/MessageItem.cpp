/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <sstream>
#include <utility>
#include "MessageItem.h"

namespace Messages {

const char MessageItem::kDelimiter = '/';

MessageItem::MessageItem(
  std::string name,
  const std::string &path)
  : name_(std::move(name)) {
  // Set path
  std::stringstream streamPath(path);
  std::string segment;
  while (std::getline(streamPath, segment, kDelimiter)) {
    if (!segment.empty()) {
      path_.push(segment);
    }
  }
}

std::string MessageItem::GetName() const {
  return name_;
}

std::queue<std::string> *MessageItem::GetPath() {
  return &path_;
}

bool MessageItem::Visit(SystemModules::Framework::Components::ToBeNamed *component) {
  // Visit component
  component->Accept(Visitor());
  // Check if visit was successful
  auto retVal = Visitor()->SuccessfulVisit();
  if (!retVal) {
    state_.SetValue(Types::MessageItemState::Enum::FAILURE);
    return false;
    // break;
  }
  state_.SetValue(Types::MessageItemState::Enum::OK);
  return true;
}

Types::MessageItemState *MessageItem::ItemState() {
  return &state_;
}

}  // namespace Messages
