/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

namespace Messages {

template<typename T>
MessageTemplate<T>::MessageTemplate()
  : Message() {
}

template<typename T>
void MessageTemplate<T>::AddMessageItem(std::unique_ptr<T> messageItem) {
  messageItemsPtr_.push_back(messageItem.get());
  messageItemsTemplatePtr_.push_back(messageItem.get());
  messageItems_.insert(std::make_pair(messageItem->GetName(), std::move(messageItem)));
}

template<typename T>
std::experimental::optional<T *> MessageTemplate<T>::GetMessageItem(
  const std::string &name) const {
  auto messageItem = messageItems_.find(name);
  if (messageItem == messageItems_.end()) {
    return std::experimental::nullopt;
  }
  return messageItem->second.get();
}

template<typename T>
std::list<MessageItem *> MessageTemplate<T>::GetMessageItems() const {
  return messageItemsPtr_;
}

template<typename T>
std::list<T *> MessageTemplate<T>::GetMessageTemplateItems() const {
  return messageItemsTemplatePtr_;
}

}  // namespace Messages
