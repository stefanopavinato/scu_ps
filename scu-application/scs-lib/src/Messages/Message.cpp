/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "MessageItem.h"
#include "MessageVisitorBase.h"
#include "Message.h"

namespace Messages {

Message::Message()
  : requestId_(Communication::Types::RequestId::Enum::NONE), messageStatus_(Messages::Types::MessageStatus::Enum::OK),
    dataStatus_(Messages::Types::DataStatus::Enum::OK) {
}

bool Message::Accept(MessageVisitorBase *visitor) {
  return visitor->Visit(this);
}

Messages::Types::MessageStatus *Message::MessageStatus() {
  return &messageStatus_;
}

Messages::Types::DataStatus *Message::DataStatus() {
  return &dataStatus_;
}

void Message::SetRequestId(const Communication::Types::RequestId::Enum &requestId) {
  requestId_.SetValue(requestId);
}

Communication::Types::RequestId Message::GetRequestId() const {
  return requestId_;
}

Types::DataStatus::Enum Message::GetDataStatus() const {
  return dataStatus_.GetValue();
}
}  // namespace Messages
