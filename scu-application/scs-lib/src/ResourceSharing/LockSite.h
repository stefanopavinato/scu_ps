/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <string>
#include "Address.h"
#include "TimeStamp.h"
#include "LockStatus.h"

namespace ResourceSharing {
class LockSite {
 public:
  explicit LockSite(
    const Address&  address);

  [[nodiscard]] Address GetAddress() const;

  [[nodiscard]] LockStatus::Enum GetLocalLockStatus() const;

  void SetLocalLockStatus(const LockStatus::Enum & lockStatus);

  [[nodiscard]] LockStatus::Enum GetRemoteLockStatus() const;

  void SetRemoteLockStatus(const LockStatus::Enum & lockStatus);

 private:
  const Address address_;

  LockStatus::Enum localLockStatus_;

  LockStatus::Enum remoteLockStatus_;
};

}  // namespace ResourceSharing
