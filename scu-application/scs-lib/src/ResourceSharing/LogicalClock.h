/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <cstdint>
#include <shared_mutex>

namespace ResourceSharing {
template <typename T>
class LogicalClock {
 public:
  LogicalClock();

  [[nodiscard]] T GetTime();

  void SetTime(const T & time);

 private:
  T time_;

  mutable std::shared_mutex mutex_;
};

}  // namespace ResourceSharing

#include "LogicalClock.tpp"
