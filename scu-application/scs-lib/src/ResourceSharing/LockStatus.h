/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <cstdint>

namespace ResourceSharing {
class LockStatus {
 public:
  enum class Enum {
    REQUEST,
    ACCEPT,
    NONE
  };

  LockStatus();

  explicit LockStatus(const Enum &value);

  [[nodiscard]] Enum GetValue() const;

  static Enum ToEnum(const uint32_t &value);

 private:
  Enum value_;
};
}  // namespace ResourceSharing
