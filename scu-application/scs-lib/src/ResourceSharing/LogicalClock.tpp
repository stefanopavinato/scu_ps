/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

namespace ResourceSharing {

template <typename T>
LogicalClock<T>::LogicalClock()
  : time_(0) {
}

template <typename T>
T LogicalClock<T>::GetTime() {
  std::shared_lock lock(mutex_);
  time_++;
  return time_;
}

template <typename T>
void LogicalClock<T>::SetTime(const T & time) {
  std::shared_lock lock(mutex_);
  time_ = std::max(time, time_);
}

}  // namespace ResourceSharing
