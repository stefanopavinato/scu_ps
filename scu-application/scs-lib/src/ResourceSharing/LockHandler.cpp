/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <sstream>
#include <chrono>
#include <iostream>
#include <utility>
#include "Logger/ILogger.h"
#include "Communication/StreamSplitter/StreamSplitterTerminal.h"
#include "Communication/IClientHandlerAddClient.h"
#include "Communication/Clients/ClientTCP.h"
#include "Communication/Protocols/ProtocolBase.h"
#include "Messages/MessageLock.h"
#include "LockHandler.h"

namespace ResourceSharing {

const char LockHandler::kName[] = "LockHandler";

LockHandler::LockHandler(
  Logger::ILogger *logger,
  const Address& address,
  Communication::Protocols::ProtocolBase *protocol,
  const std::chrono::microseconds &updateInterval)
  : Utils::ThreadBase(logger, kName)
  , logger_(logger)
  , clientHandler_(nullptr)
  , clock_(LogicalClock<uint32_t>())
  , address_(address)
  , protocol_(protocol)
  , updateInterval_(updateInterval) {
}

void LockHandler::SetClientHandler(Communication::IClientHandlerAddClient *clientHandler) {
  if (clientHandler == nullptr) {
    logger_->Error("Client handler not set");
    return;
  }
  // Set client handler
  clientHandler_ = clientHandler;
}

LogicalClock<uint32_t> * LockHandler::GetClock() {
  return &clock_;
}

void LockHandler::AddLock(std::unique_ptr<Lock> lock) {
  // Set lock handler
  lock->SetLockHandler(this);
  // Insert lock into list
  locks_.insert(std::make_pair(lock->GetId(), std::move(lock)));
}

std::experimental::optional<Lock *> LockHandler::GetLock(const uint32_t & id) {
  auto lock = locks_.find(id);
  if (lock == locks_.end()) {
    return std::experimental::nullopt;
  }
  return lock->second.get();
}

void LockHandler::AddSite(const Address &address) {
  sites_.push_back(address);
}

Address LockHandler::GetAddress() const {
  return address_;
}

bool LockHandler::Send(Messages::MessageBase * message) const {
  return clientHandler_->OnMessageReceived(message);
}

bool LockHandler::OnMessageReceived(Messages::MessageBase *message) {
  return message->Accept(this);
}

bool LockHandler::Visit(Messages::MessageLock * message) {
  auto lock = GetLock(message->GetLockId());
  if (!lock) {
    return false;
  }
  lock.value()->OnMessageReceived(message);
  return true;
}

bool LockHandler::Run() {
  auto timeNext = std::chrono::steady_clock::now() + updateInterval_;
  // Check connections
  for (const auto &site : sites_) {
    // Notify all locks if client exists
    if (clientHandler_->HasClient(site.GetIp())) {
      for (const auto &lock : locks_) {
        lock.second->AddSite(ResourceSharing::Address(site));
      }
      continue;
    }
    // Create client, if local address is smaller then site address
    if (address_ < site) {
      // Create stream splitter
      auto streamSplitter = std::make_unique<Communication::StreamSplitter::StreamSplitterTerminal>();
      // Create client
      auto client = std::make_unique<Communication::Clients::ClientTCP>(
        logger_,
        protocol_,
        std::move(streamSplitter));
      // Connect client
      // ToDo: No static port
      if (!client->Connect(
        site.GetIp(),
        20003)) {
        // Notify locks
        for (const auto &lock : locks_) {
          lock.second->RemoveSite(ResourceSharing::Address(site));
        }
        continue;
      }
      // Set message receiver
      client->SetMessageHandler(this);
      // Add client
      clientHandler_->AddClient(
        std::move(client));
    } else {
      // Notify locks
      for (const auto &lock : locks_) {
        lock.second->RemoveSite(ResourceSharing::Address(site));
      }
    }
  }
  std::this_thread::sleep_until(timeNext);
  return true;
}

bool LockHandler::Initialize() {
  return true;
}

bool LockHandler::DeInitialize() {
  return false;
}

}  // namespace ResourceSharing
