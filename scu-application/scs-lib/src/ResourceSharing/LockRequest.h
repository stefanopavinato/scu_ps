/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <cstdint>
#include <unordered_map>
#include "TimeStamp.h"
#include "LockSite.h"
#include "Messages/MessageVisitorBase.h"

namespace ResourceSharing {
class LockRequest {
 public:
  LockRequest(
    const TimeStamp &timeStamp,
    const LockSite &site);

  [[nodiscard]] TimeStamp GetTimeStamp() const;

  [[nodiscard]] LockSite GetSite() const;

  // muss auch einen Zustand haben um zu überprüfen, ob der Lock gewährt werden kann.



 private:
  const TimeStamp timeStamp_;

  const LockSite site_;

//  std::unordered_map<uint32_t, Status> requestStatus_;
};
}  // namespace ResourceSharing
