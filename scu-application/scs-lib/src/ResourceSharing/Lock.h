/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <cstdint>
#include <shared_mutex>
#include <list>
#include <map>
#include "Logger/ILogger.h"
#include "Messages/MessageVisitorBase.h"
#include "Address.h"
#include "LogicalClock.h"
#include "LockStatus.h"
#include "LockSite.h"
#include "LockRequest.h"

//  RicartAgrawala algorithm
//  https://en.wikipedia.org/wiki/Ricart%E2%80%93Agrawala_algorithm

namespace ResourceSharing {
class LockHandler;
class Lock : public Messages::MessageVisitorBase {
 public:
  Lock(
    Logger::ILogger *logger,
    std::string name,
    const uint32_t & id);

  bool OnMessageReceived(Messages::MessageBase *message) override;

  bool Visit(Messages::MessageLock * message) override;

  void SetLockHandler(LockHandler * lockHandler);

  [[nodiscard]] uint32_t GetId() const;

  void AddSite(const Address &address);

  void RemoveSite(const Address &address);

  bool Acquire();

  void Release();

 private:
  Logger::ILogger *logger_;

  // Reference to the lock handler
  LockHandler * lockHandler_{};

  // Name of the lock
  const std::string name_;

  // Id of the lock
  const uint32_t id_;

  bool acquireLock_;

  mutable std::shared_mutex mutex_;

  // Timestamp of the local lock request
  TimeStamp timeStamp_;

  std::chrono::steady_clock::time_point timeout_;

  // Status of the other lock sites (lock granted)
  std::unordered_map<Address,
                     std::unique_ptr<LockSite>,
                     AddressHash> sites_;

  // Aktuell habe ich nur Requests von Client Seite und von lokaler Seite abgebildet

  // Wo werden die anderen Sites verwaltet?
  // Was muss von anderen Sites verwaltet werden:
  // * Addresse um diese zu finden
  // * Connection status -> Dient dazu festzustaellen ob Daten ausgetauscht werden müssen.
};
}  // namespace ResourceSharing
