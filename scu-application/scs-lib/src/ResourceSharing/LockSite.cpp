/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "LockSite.h"

namespace ResourceSharing {

LockSite::LockSite(
  const Address& address)
  : address_(address)
  , localLockStatus_(LockStatus::Enum::NONE)
  , remoteLockStatus_(LockStatus::Enum::NONE) {
}

Address LockSite::GetAddress() const {
  return address_;
}

LockStatus::Enum LockSite::GetLocalLockStatus() const {
  return localLockStatus_;
}

void LockSite::SetLocalLockStatus(const LockStatus::Enum & lockStatus) {
  localLockStatus_ = lockStatus;
}

LockStatus::Enum LockSite::GetRemoteLockStatus() const {
  return remoteLockStatus_;
}

void LockSite::SetRemoteLockStatus(const LockStatus::Enum & lockStatus) {
  remoteLockStatus_ = lockStatus;
}

}  // namespace ResourceSharing
