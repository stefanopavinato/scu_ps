/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "LockRequest.h"

namespace ResourceSharing {

LockRequest::LockRequest(
  const TimeStamp & timeStamp,
  const LockSite & site)
  : timeStamp_(timeStamp)
  , site_(site) {
}

TimeStamp LockRequest::GetTimeStamp() const {
  return timeStamp_;
}

LockSite LockRequest::GetSite() const {
  return site_;
}

}  // namespace ResourceSharing
