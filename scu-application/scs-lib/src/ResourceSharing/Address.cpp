/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <sstream>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "Address.h"


namespace ResourceSharing {

Address::Address(
  std::string ip)
  : ip_(std::move(ip)) {
}

std::string Address::GetIp() const {
  return ip_;
}

uint32_t Address::ToUInt32() const {
  auto i = inet_addr(ip_.c_str());
  return i;
}

Address &Address::operator=(const Address &other) {
  if (this != &other) {
    ip_ = other.ip_;
  }
  return *this;
}

bool Address::operator==(const Address& other) const {
  return ip_ == other.ip_;
}

bool Address::operator!=(const Address& other) const {
  return ip_ != other.ip_;
}

bool Address::operator>(const Address& other) const {
  return ToUInt32() > other.ToUInt32();
}

bool Address::operator<(const Address& other) const {
  return ToUInt32() < other.ToUInt32();
}

size_t AddressHash::operator()(const Address& other) const {
}


}  // namespace ResourceSharing
