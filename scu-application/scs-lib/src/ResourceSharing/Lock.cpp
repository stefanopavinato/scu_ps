/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <iostream>
#include <utility>
#include "Messages/MessageLock.h"
#include "LockHandler.h"
#include "Lock.h"

namespace ResourceSharing {

Lock::Lock(
  Logger::ILogger *logger,
  std::string name,
  const uint32_t &id)
  : logger_(logger)
  , name_(std::move(name))
  , id_(id)
  , acquireLock_(false)
  , timeStamp_(ResourceSharing::TimeStamp(0, 0)) {
}

void Lock::SetLockHandler(LockHandler *lockHandler) {
  lockHandler_ = lockHandler;
  timeStamp_ = ResourceSharing::TimeStamp(
    lockHandler->GetClock()->GetTime(),
    lockHandler->GetAddress().ToUInt32());
}

uint32_t Lock::GetId() const {
  return id_;
}

void Lock::AddSite(const Address &address) {
  if (sites_.find(address) == sites_.end()) {
    logger_->Info(std::string("Connected ip: ").append(address.GetIp()));
    sites_.insert(std::make_pair(address, std::make_unique<LockSite>(address)));
  }
}

void Lock::RemoveSite(const Address & address) {
  auto site = sites_.find(address);
  if (site != sites_.end()) {
    logger_->Info(std::string("Disconnected ip: ").append(address.GetIp()));
    sites_.erase(site);
  }
}

bool Lock::Acquire() {
  std::unique_lock lock(mutex_);
  // Lease timestamp if lock is newly acquired
  if (!acquireLock_) {
    timeStamp_ = ResourceSharing::TimeStamp(
      lockHandler_->GetClock()->GetTime(),
      lockHandler_->GetAddress().ToUInt32());
    acquireLock_ = true;
  }
  // Enter the critical section only after receiving all reply messages
  bool retVal = true;
  // Sends a lock request to all sites
  for (auto &site : sites_) {
    switch (site.second->GetLocalLockStatus()) {
      case LockStatus::Enum::ACCEPT: {
//        logger_->Info(std::string("Acquire [time = ").append(std::to_string(timeStamp_.GetTime()))
//                      .append(", priority = ").append(std::to_string(timeStamp_.GetPriority())).append("]"));
        break;
      }
      case LockStatus::Enum::REQUEST: {
        if (timeout_ < std::chrono::steady_clock::now()) {
//          logger_->Warning("Lock request timeout");
          site.second->SetLocalLockStatus(LockStatus::Enum::NONE);
        }
        retVal = false;
        break;
      }
      case LockStatus::Enum::NONE: {
        timeout_ = std::chrono::steady_clock::now() + std::chrono::milliseconds(500);
        auto message = Messages::MessageLock(
          ResourceSharing::Address(lockHandler_->GetAddress()),
          ResourceSharing::Address(site.second->GetAddress()),
          id_,
          timeStamp_,
          LockStatus(
            LockStatus::Enum::REQUEST));
        if (lockHandler_->Send(&message)) {
          site.second->SetLocalLockStatus(LockStatus::Enum::REQUEST);
        } else {
          logger_->Info("Failed to send lock request");
        }
        retVal = false;
        break;
      }
    }
  }
  return retVal;
}

void Lock::Release() {
  std::unique_lock lock(mutex_);
//  logger_->Info(std::string("Release [time = ").append(std::to_string(timeStamp_.GetTime()))
//                .append(", priority = ").append(std::to_string(timeStamp_.GetPriority())).append("]"));
  // Clear acquire lock flag
  acquireLock_ = false;
  // Send a timestamped reply message to all sites requesting the lock
  for (auto& site : sites_) {
    // Reset local lock status
    site.second->SetLocalLockStatus(LockStatus::Enum::NONE);
    // Handle remote lock status
    if (site.second->GetRemoteLockStatus() == LockStatus::Enum::REQUEST) {
      auto message = Messages::MessageLock(
        ResourceSharing::Address(lockHandler_->GetAddress()),
        ResourceSharing::Address(site.second->GetAddress()),
        id_,
        ResourceSharing::TimeStamp(
          lockHandler_->GetClock()->GetTime(),
          lockHandler_->GetAddress().ToUInt32()),
        LockStatus(
          LockStatus::Enum::ACCEPT));
      if (lockHandler_->Send(&message)) {
        site.second->SetRemoteLockStatus(LockStatus::Enum::NONE);
      } else {
        logger_->Info("Failed to send remote lock accept");
      }
    }
  }
}

bool Lock::OnMessageReceived(Messages::MessageBase *message) {
  return message->Accept(this);
}

bool Lock::Visit(Messages::MessageLock *message) {
  // Lock
  std::unique_lock lock(mutex_);
  // Update logical clock
  lockHandler_->GetClock()->SetTime(message->GetTimeStamp().GetTime());

  switch (message->GetLockStatus().GetValue()) {
    case LockStatus::Enum::ACCEPT: {
//      logger_->Info(std::string("Accept local lock [time = ")
//      .append(std::to_string(message->GetTimeStamp().GetTime())).append("]"));
      // Set accept status for local site
      auto site = sites_.find(message->GetSender());
      if (site != sites_.end()) {
        site->second->SetLocalLockStatus(LockStatus::Enum::ACCEPT);
      } else {
        logger_->Info("Remote lock not found");
      }
      break;
    }
    case LockStatus::Enum::REQUEST: {
//      logger_->Info(std::string("Request remote lock [time = ")
//        .append(std::to_string(message->GetTimeStamp().GetTime())).append("]"));
      // Immediately send a timestamped reply message if
      //  * the receiving process is not currently interested in the critical section OR
      //  * the receiving process has a lower priority
      if (!acquireLock_ || timeStamp_ > message->GetTimeStamp()) {
        auto messageReply = Messages::MessageLock(
          ResourceSharing::Address(lockHandler_->GetAddress()),
          message->GetSender(),
          id_,
          ResourceSharing::TimeStamp(
            lockHandler_->GetClock()->GetTime(),
            lockHandler_->GetAddress().ToUInt32()),
          LockStatus(
            LockStatus::Enum::ACCEPT));
        if (!lockHandler_->Send(&messageReply)) {
          logger_->Warning("Failed to send message");
        }
        break;
      } else {
      // Otherwise defer the reply message
      auto site = sites_.find(message->GetSender());
      if (site != sites_.end()) {
        site->second->SetRemoteLockStatus(LockStatus::Enum::REQUEST);
        }
      }
      break;
    }
    case LockStatus::Enum::NONE: {
      logger_->Info("None");
      break;
    }
  }
  return true;
}

}  // namespace ResourceSharing
