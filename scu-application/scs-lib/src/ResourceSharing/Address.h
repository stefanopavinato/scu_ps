/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <cstdint>
#include <string>

namespace ResourceSharing {
class Address {
 public:
  Address(const Address &address) = default;

  explicit Address(
    std::string ip);

  [[nodiscard]] std::string GetIp() const;

  [[nodiscard]] uint32_t ToUInt32() const;

  Address &operator=(const Address &other);

  bool operator==(const Address& other) const;

  bool operator!=(const Address& other) const;

  bool operator>(const Address& other) const;

  bool operator<(const Address& other) const;

 private:
  std::string ip_;
};

class AddressHash {
 public:
  size_t operator()(const Address& other) const;
};
}  // namespace ResourceSharing
