/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <experimental/optional>
#include <unordered_map>
#include <memory>
#include "Utils/ThreadBase.h"
#include "Messages/MessageVisitorBase.h"
#include "LogicalClock.h"
#include "Lock.h"

namespace Logger {
class ILogger;
}  // namespace Logger
namespace Communication {
class IClientHandlerAddClient;
namespace Protocols {
class ProtocolBase;
}  // namespace Protocols
}  // namespace Communication
namespace Messages {
class MessageBase;
}  // namespace Messages
namespace ResourceSharing {
class LockHandler : public Utils::ThreadBase, public Messages::MessageVisitorBase {
 public:
  LockHandler(
    Logger::ILogger *logger,
    const Address&  address,
    Communication::Protocols::ProtocolBase *protocol,
    const std::chrono::microseconds &updateInterval);

  void SetClientHandler(Communication::IClientHandlerAddClient *clientHandler);

  void AddLock(std::unique_ptr<Lock> lock);

  [[nodiscard]] std::experimental::optional<Lock *> GetLock(const uint32_t & id);

  void AddSite(const Address &address);

  bool OnMessageReceived(Messages::MessageBase *message) override;

  bool Visit(Messages::MessageLock * message) override;

  [[nodiscard]] LogicalClock<uint32_t> * GetClock();

  [[nodiscard]] Address GetAddress() const;

  // Send
  [[nodiscard]] bool Send(Messages::MessageBase * message) const;

 private:
  [[nodiscard]] bool Run() override;

  [[nodiscard]] bool Initialize() override;

  [[nodiscard]] bool DeInitialize() override;

  Logger::ILogger *const logger_;

  Communication::IClientHandlerAddClient *clientHandler_;

  LogicalClock<uint32_t> clock_;

  Address address_;

  Communication::Protocols::ProtocolBase *protocol_;

  std::unordered_map<uint32_t, std::unique_ptr<Lock>> locks_;

  std::list<Address> sites_;

  std::chrono::microseconds updateInterval_;

  static const char kName[];
};
}  // namespace ResourceSharing
