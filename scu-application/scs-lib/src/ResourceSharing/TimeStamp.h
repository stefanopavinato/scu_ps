/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <cstdint>

namespace ResourceSharing {
class TimeStamp {
 public:
  TimeStamp(
    const uint32_t & time,
    const uint32_t & priority);

  [[nodiscard]] uint32_t GetTime() const;

  [[nodiscard]] uint32_t GetPriority() const;

  TimeStamp& operator=(const TimeStamp & other);

  bool operator==(const TimeStamp& other) const;

  bool operator!=(const TimeStamp& other) const;

  bool operator<(const TimeStamp& other) const;

  bool operator>(const TimeStamp& other) const;

 private:
  uint32_t time_;

  uint32_t priority_;
};
}  // namespace ResourceSharing
