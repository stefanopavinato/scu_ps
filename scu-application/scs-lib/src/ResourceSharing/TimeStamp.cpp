/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "TimeStamp.h"

namespace ResourceSharing {

TimeStamp::TimeStamp(
  const uint32_t & time,
  const uint32_t & priority)
  : time_(time)
  , priority_(priority) {
}

uint32_t TimeStamp::GetTime() const {
  return time_;
}

uint32_t TimeStamp::GetPriority() const {
  return priority_;
}

TimeStamp& TimeStamp::operator=(const TimeStamp & other) {
  // Guard self assignment
  if (this == &other) {
    return *this;
  }
  // Set properties
  time_ = other.time_;
  priority_ = other.priority_;
}

bool TimeStamp::operator==(const TimeStamp& other) const {
  return time_ == other.time_ &&
         priority_ == other.priority_;
}

bool TimeStamp::operator!=(const TimeStamp &other) const {
  return time_ != other.time_ ||
         priority_ != other.priority_;
}

bool TimeStamp::operator<(const TimeStamp& other) const {
  if (time_ < other.time_) {
    return true;
  }
  if (other.time_ < time_) {
    return false;
  }
  return priority_ < other.priority_;
}

bool TimeStamp::operator>(const TimeStamp &other) const {
  if (time_ > other.time_) {
    return true;
  }
  if (other.time_ > time_) {
    return false;
  }
  return priority_ > other.priority_;
}

}  // namespace ResourceSharing
