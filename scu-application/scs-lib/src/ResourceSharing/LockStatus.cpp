/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "LockStatus.h"

namespace ResourceSharing {

LockStatus::LockStatus()
  : value_(Enum::NONE) {
}

LockStatus::LockStatus(const Enum &value)
  : value_(value) {
}

LockStatus::Enum LockStatus::GetValue() const {
  return value_;
}

LockStatus::Enum LockStatus::ToEnum(const uint32_t &value) {
  if (value == 0) {
    return LockStatus::Enum::REQUEST;
  } else if (value == 1) {
    return LockStatus::Enum::ACCEPT;
  } else {
    return LockStatus::Enum::NONE;
  }
}

}  // namespace ResourceSharing
