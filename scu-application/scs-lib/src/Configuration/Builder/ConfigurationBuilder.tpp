/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

namespace Configuration::Builder {

template <typename T>
ConfigKeyValue<T> * ConfigurationBuilder::KeyValue(
  const std::string &key,
  const T &value) {
  auto scuAddress = std::make_unique<ConfigKeyValue<T>>(
    key,
    value);
  auto scuAddressPtr = scuAddress.get();
  AddComponent(std::move(scuAddress));
  return scuAddressPtr;
}

template<typename T>
ConfigItem <T> *ConfigurationBuilder::Value(
  const std::string &key,
  const T &value,
  const std::string &source) {
  auto item = std::make_unique<ConfigItem<T>>(
    key,
    value,
    source);
  auto itemPtr = item.get();
  AddComponent(std::move(item));
  return itemPtr;
}

template<typename T>
ConfigItem <T> *ConfigurationBuilder::Value(
  const std::string &key,
  const std::string &source) {
  auto item = std::make_unique<ConfigItem<T>>(
    key,
    source);
  auto itemPtr = item.get();
  AddComponent(std::move(item));
  return itemPtr;
}

}  // namespace Configuration::Builder
