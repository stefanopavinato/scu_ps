/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "ConfigurationBuilder.h"

namespace Configuration::Builder {

ConfigurationBuilder::ConfigurationBuilder(
  ConfigCollection *collection)
  : ConfigurationBuilderBase(collection) {
}

ConfigContainer *ConfigurationBuilder::Container(
  const std::string &key) {
  auto container = std::make_unique<ConfigContainer>(
    key);
  auto containerPtr = container.get();
  AddComponent(std::move(container));
  return containerPtr;
}

ConfigArray *ConfigurationBuilder::Array(
  const std::string &key) {
  auto array = std::make_unique<ConfigArray>(
    key);
  auto arrayPtr = array.get();
  AddComponent(std::move(array));
  return arrayPtr;
}

ConfigScuRegister *ConfigurationBuilder::ScuRegister(
  const std::string &key,
  const uint64_t &address,
  const uint32_t &bit,
  const size_t &size) {
  auto scuRegister = std::make_unique<ConfigScuRegister>(
    key,
    address,
    bit,
    size);
  auto scuRegisterPtr = scuRegister.get();
  AddComponent(std::move(scuRegister));
  return scuRegisterPtr;
}

ConfigScuRegisterArray *ConfigurationBuilder::ScuRegisterArray(
  const std::string &key,
  const uint64_t &address,
  const size_t &size) {
  auto scuRegister = std::make_unique<ConfigScuRegisterArray>(
    key,
    address,
    size);
  auto scuRegisterPtr = scuRegister.get();
  AddComponent(std::move(scuRegister));
  return scuRegisterPtr;
}

[[nodiscard]] ConfigGpio *ConfigurationBuilder::Gpio(
  const std::string &key,
  const uint32_t &address,
  const bool &isInverted) {
  auto gpio = std::make_unique<ConfigGpio>(
    key,
    address,
    isInverted);
  auto gpioPtr = gpio.get();
  AddComponent(std::move(gpio));
  return gpioPtr;
}

}  // namespace Configuration::Builder
