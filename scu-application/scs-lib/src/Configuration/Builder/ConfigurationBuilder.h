/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Configuration/ConfigCollection.h"
#include "Configuration/ConfigContainer.h"
#include "Configuration/ConfigArray.h"
#include "Configuration/ConfigItem.h"
#include "Configuration/ConfigGpio.h"
#include "Configuration/ConfigKeyValue.h"
#include "Configuration/ConfigScuRegister.h"
#include "Configuration/ConfigScuRegisterArray.h"
#include "ConfigurationBuilderBase.h"

namespace Configuration::Builder {
class ConfigurationBuilder : ConfigurationBuilderBase {
 public:
  explicit ConfigurationBuilder(
    ConfigCollection *container);

  [[nodiscard]] ConfigContainer *Container(
    const std::string &key);

  [[nodiscard]] ConfigArray *Array(
    const std::string &key);

  template <typename T>
  [[nodiscard]] ConfigKeyValue<T> * KeyValue(
    const std::string &key,
    const T &address);

  [[nodiscard]] ConfigScuRegister *ScuRegister(
    const std::string &key,
    const uint64_t &address,
    const uint32_t &bit,
    const size_t &size);

  [[nodiscard]] ConfigScuRegisterArray *ScuRegisterArray(
    const std::string &key,
    const uint64_t &address,
    const size_t &size);

  [[nodiscard]] ConfigGpio *Gpio(
    const std::string &key,
    const uint32_t &address,
    const bool &isInverted);

  template<typename T>
  ConfigItem<T> *Value(
    const std::string &key,
    const T &value,
    const std::string &source);

  template<typename T>
  ConfigItem<T> *Value(
    const std::string &key,
    const std::string &source);
};
}  // namespace Configuration::Builder

#include "ConfigurationBuilder.tpp"
