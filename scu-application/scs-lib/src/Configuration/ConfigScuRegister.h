/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "ConfigComponent.h"

namespace Configuration {
class ConfigScuRegister : public ConfigComponent {
 public:
  ConfigScuRegister(
    const std::string &key,
    const uint64_t &address,
    const uint32_t &bit,
    const size_t &size);

  [[nodiscard]] uint64_t Address() const;

  [[nodiscard]] uint32_t Bit() const;

  [[nodiscard]] size_t Size() const;

  [[nodiscard]] Json::Value ToJson() const override;

 private:
  const uint64_t address_;

  const uint32_t bit_;

  const size_t size_;

  static const char kAddress[];
  static const char kBit[];
  static const char kSize[];
};
}  // namespace Configuration
