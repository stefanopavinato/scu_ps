/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <string>
#include <json/json.h>

namespace Configuration {
class ConfigComponent {
 public:
  explicit ConfigComponent(
    std::string key);

  virtual ~ConfigComponent() = default;

  [[nodiscard]] std::string Key() const;

  [[nodiscard]] virtual Json::Value ToJson() const = 0;

 protected:
  static const char kKey[];

 private:
  std::string key_;
};
}  // namespace Configuration
