/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "ConfigScuRegister.h"

namespace Configuration {

const char ConfigScuRegister::kAddress[] = "Address";
const char ConfigScuRegister::kBit[] = "Bit";
const char ConfigScuRegister::kSize[] = "Size";

ConfigScuRegister::ConfigScuRegister(
  const std::string &key,
  const uint64_t &address,
  const uint32_t &bit,
  const size_t &size)
  : ConfigComponent(
  key), address_(address), bit_(bit), size_(size) {
}

uint64_t ConfigScuRegister::Address() const {
  return address_;
}

uint32_t ConfigScuRegister::Bit() const {
  return bit_;
}

size_t ConfigScuRegister::Size() const {
  return size_;
}

Json::Value ConfigScuRegister::ToJson() const {
  auto json = Json::Value();
  json[kKey] = Key();
  json[kAddress] = address_;
  json[kBit] = bit_;
  json[kSize] = size_;
  return json;
}

}  // namespace Configuration
