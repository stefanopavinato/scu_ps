/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <string>
#include "ConfigComponent.h"

namespace Configuration {
template<class T>
class ConfigItem : public ConfigComponent {
 public:
  ConfigItem(
    const std::string &key,
    const T &value,
    const std::string &source);

  [[nodiscard]] T GetValue() const;

  [[nodiscard]] std::string GetSource() const;

  [[nodiscard]] Json::Value ToJson() const override;

 protected:
  static const char kValue[];

  static const char kSource[];

  static const char kIsValid[];

 private:
  T value_;

  bool isValid_;

  std::string source_;
};
}  // namespace Configuration

#include "ConfigItem.tpp"
