/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "ConfigComponent.h"

namespace Configuration {
class ConfigScuRegisterArray : public ConfigComponent {
 public:
  ConfigScuRegisterArray(
    const std::string &key,
    const uint64_t &address,
    const size_t &size);

  [[nodiscard]] uint64_t Address() const;

  [[nodiscard]] size_t Size() const;

  [[nodiscard]] Json::Value ToJson() const override;

 private:
  const uint64_t address_;

  const size_t size_;

  static const char kAddress[];
  static const char kSize[];
};
}  // namespace Configuration
