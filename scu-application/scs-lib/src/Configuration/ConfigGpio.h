/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "ConfigComponent.h"

namespace Configuration {
class ConfigGpio : public ConfigComponent {
 public:
  ConfigGpio(
    const std::string &key,
    const uint32_t &address,
    const bool &isInverted);

  [[nodiscard]] uint32_t Address() const;

  [[nodiscard]] bool IsInverted() const;

  [[nodiscard]] Json::Value ToJson() const override;

 private:
  const uint32_t address_;

  const bool isInverted_;

  static const char kAddress[];
  static const char kIsInverted[];
};
}  // namespace Configuration
