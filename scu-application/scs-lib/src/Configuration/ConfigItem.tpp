/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <utility>

namespace Configuration {

template<class T>
const char ConfigItem<T>::kValue[] = "Value";
template<class T>
const char ConfigItem<T>::kSource[] = "Source";
template<class T>
const char ConfigItem<T>::kIsValid[] = "IsValid";

template<class T>
ConfigItem<T>::ConfigItem(
  const std::string &key,
  const T &value,
  const std::string &source)
  : ConfigComponent(key), value_(value), source_(source) {
}

template<class T>
T ConfigItem<T>::GetValue() const {
  return value_;
}

template<class T>
std::string ConfigItem<T>::GetSource() const {
  return source_;
}

template<class T>
Json::Value ConfigItem<T>::ToJson() const {
  auto json = Json::Value();
  json[kKey] = Key();
  json[kValue] = GetValue();
  json[kSource] = GetSource();
  return json;
}

}  // namespace Configuration
