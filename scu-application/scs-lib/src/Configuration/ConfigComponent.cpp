/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "ConfigComponent.h"

namespace Configuration {

const char ConfigComponent::kKey[] = "Key";

ConfigComponent::ConfigComponent(
  std::string key)
  : key_(std::move(key)) {
}

std::string ConfigComponent::Key() const {
  return key_;
}

}  // namespace Configuration
