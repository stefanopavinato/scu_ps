/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Builder/ConfigurationBuilder.h"
#include "ConfigContainer.h"

namespace Configuration {

const char ConfigContainer::kContainer[] = "Container";

ConfigContainer::ConfigContainer(
  const std::string &key)
  : ConfigCollection(key) {
}

Json::Value ConfigContainer::ToJson() const {
  auto json = Json::Value();
  int i = 0;
  for (const auto &component : components_) {
    json[kKey] = Key();
    json[kContainer][i++] = component.second->ToJson();
  }
  return json;
}
}  // namespace Configuration
