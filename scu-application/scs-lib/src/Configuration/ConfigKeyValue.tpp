/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

namespace Configuration {

template <typename T>
const char ConfigKeyValue<T>::kAddress[] = "Value";

template <typename T>
ConfigKeyValue<T>::ConfigKeyValue(
  const std::string &key,
  const T value)
  : ConfigComponent(key)
  , value_(value) {
}

template <typename T>
T ConfigKeyValue<T>::Value() const {
  return value_;
}

template <typename T>
Json::Value ConfigKeyValue<T>::ToJson() const {
  auto json = Json::Value();
  json[kKey] = Key();
  json[kAddress] = value_;
  return json;
}

}  // namespace Configuration
