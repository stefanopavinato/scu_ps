/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Builder/ConfigurationBuilder.h"
#include "ConfigCollection.h"

namespace Configuration {

ConfigCollection::ConfigCollection(
  const std::string &key)
  : ConfigComponent(key) {
}

ConfigComponent const *ConfigCollection::GetComponent(
  const std::string &key) const {
  auto component = components_.find(key);
  if (component == components_.end()) {
    return nullptr;
  }
  return component->second.get();
}

Builder::ConfigurationBuilder ConfigCollection::Add() {
  return Builder::ConfigurationBuilder(this);
}

void ConfigCollection::AddComponent(std::unique_ptr<ConfigComponent> component) {
  components_.insert(std::make_pair(component->Key(), std::move(component)));
}

}  // namespace Configuration
