/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <string>

namespace Types {
class AlertSeverity {
 public:
  // Note: names are taken from syslog
  enum class Enum {
    OK,
    DEBUG,
    INFORMATIONAL,
    NOTICE,
    WARNING,
    ERROR
  };

  AlertSeverity();

  explicit AlertSeverity(const Enum &value);

  [[nodiscard]] static std::string GetTypeName();

  [[nodiscard]] Enum GetEnum() const;

  void SetEnum(const Enum &value);

  bool operator==(const AlertSeverity &other) const;

  bool operator!=(const AlertSeverity &other) const;

  bool operator>(const AlertSeverity &other) const;

  bool operator<(const AlertSeverity &other) const;

  bool operator<=(const AlertSeverity &other) const;

  bool operator>=(const AlertSeverity &other) const;

  [[nodiscard]] std::string ToString() const;

  void FromString(const std::string &str);

 private:
  Enum value_;

  static const char kTypeName[];

  static const char kEmergency[];
  static const char kAlert[];
  static const char kCritical[];
  static const char kError[];
  static const char kWarning[];
  static const char kNotice[];
  static const char kInformational[];
  static const char kDebug[];
  static const char kOK[];
};

}  // namespace Types
