/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <chrono>
#include <variant>
#include <string>
#include <cstdint>
#include <vector>
#include <unordered_map>
#include <experimental/optional>
#include "Messages/Visitor/IVisitor.h"
#include "Types/Value.h"
#include "Types/ValueType.h"
#include "Types/ModuleType.h"
#include "Types/ModuleState.h"
#include "Types/AlertSeverity.h"
#include "Types/ScuStatus.h"
#include "Types/OplStatus.h"

namespace Types {
using VariantValue = std::variant<
  Types::Value<int32_t>,
  Types::Value<int16_t>,
  Types::Value<int8_t>,
  Types::Value<uint32_t>,
  Types::Value<uint16_t>,
  Types::Value<uint8_t>,
  Types::Value<double>,
  Types::Value<float>,
  int32_t,
  int16_t,
  int8_t,
  uint32_t,
  uint16_t,
  uint8_t,
  double,
  float,
  bool,
  std::string,
  Types::AlertSeverity,
  Types::ModuleType,
  Types::ModuleState,
  std::chrono::minutes,
  std::chrono::milliseconds,
  Types::ScuStatus,
  Types::OplStatus,
  std::vector<uint32_t>>;

class TestVariantValue : public Messages::Visitor::IVisitor {
 public:
  TestVariantValue() = default;

  void Accept(Messages::Visitor::VisitorBase *visitor) final;

  [[nodiscard]] Types::ValueType GetValueType() const;

  template<typename T>
  std::experimental::optional<T> GetValue() const;

  template<typename T>
  void SetValue(const T &value);

  [[nodiscard]] VariantValue GetVariantValue() const;

  [[nodiscard]] std::string ToString() const;

  bool FromString(
    const Types::ValueType &type,
    const std::string &str);

 private:
  VariantValue value_;
};

template<typename T>
std::experimental::optional<T> TestVariantValue::GetValue() const {
  if (!std::holds_alternative<T>(value_)) {
    return std::experimental::nullopt;
  }
  return std::get<T>(value_);
}

template<typename T>
void TestVariantValue::SetValue(const T &value) {
  value_ = value;
}

}  // namespace Types
