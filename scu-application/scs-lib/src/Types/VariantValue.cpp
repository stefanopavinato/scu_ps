/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <iomanip>
#include <iostream>
#include <sstream>
#include <cstdint>
#include "Messages/Visitor/VisitorBase.h"
#include "VariantValue.h"

namespace Types {

void TestVariantValue::Accept(Messages::Visitor::VisitorBase *visitor) {
  visitor->Visit(this);
}

Types::ValueType TestVariantValue::GetValueType() const {
  return Types::ValueType(static_cast<Types::ValueType::Enum>(value_.index()));
}

VariantValue TestVariantValue::GetVariantValue() const {
  return value_;
}

bool TestVariantValue::FromString(
  const Types::ValueType &type,
  const std::string &str) {
  try {
    switch (type.GetValue()) {
      case Types::ValueType::Enum::VALUE_INT32: {
        auto val = Value<int32_t>();
        val.SetValue(std::stoi(str));
        value_ = val;
        return true;
      }
      case Types::ValueType::Enum::VALUE_INT16: {
        auto val = Value<int16_t>();
        val.SetValue(std::stoi(str));
        value_ = val;
        return true;
      }
      case Types::ValueType::Enum::VALUE_INT8: {
        auto val = Value<int8_t>();
        val.SetValue(std::stoi(str));
        value_ = val;
        return true;
      }
      case Types::ValueType::Enum::VALUE_UINT32: {
        auto val = Value<uint32_t>();
        val.SetValue(std::stoul(str));
        value_ = val;
        return true;
      }
      case Types::ValueType::Enum::VALUE_UINT16: {
        auto val = Value<uint16_t>();
        val.SetValue(std::stoul(str));
        value_ = val;
        return true;
      }
      case Types::ValueType::Enum::VALUE_UINT8: {
        auto val = Value<uint8_t>();
        val.SetValue(std::stoul(str));
        value_ = val;
        return true;
      }
      case Types::ValueType::Enum::VALUE_DOUBLE: {
        auto val = Value<double>();
        val.SetValue(std::stod(str));
        value_ = val;
        return true;
      }
      case Types::ValueType::Enum::VALUE_FLOAT: {
        auto val = Value<float>();
        val.SetValue(std::stof(str));
        value_ = val;
        return true;
      }
      case Types::ValueType::Enum::INT32: {
        value_ = static_cast<int32_t>(std::stoi(str));
        return true;
      }
      case Types::ValueType::Enum::INT16: {
        value_ = static_cast<int16_t>(std::stoi(str));
        return true;
      }
      case Types::ValueType::Enum::INT8: {
        value_ = static_cast<int8_t>(std::stoi(str));
        return true;
      }
      case Types::ValueType::Enum::UINT32: {
        value_ = static_cast<uint32_t>(std::stoul(str));
        return true;
      }
      case Types::ValueType::Enum::UINT16: {
        value_ = static_cast<uint16_t>(std::stoul(str));
        return true;
      }
      case Types::ValueType::Enum::UINT8: {
        value_ = static_cast<uint8_t>(std::stoul(str));
        return true;
      }
      case Types::ValueType::Enum::DOUBLE: {
        value_ = std::stod(str);
        return true;
      }
      case Types::ValueType::Enum::FLOAT: {
        value_ = std::stof(str);
        return true;
      }
      case Types::ValueType::Enum::BOOL: {
        if (str == "0" || str == "false" || str == "FALSE") {
          value_ = false;
        } else {
          value_ = true;
        }
        return true;
      }
      case Types::ValueType::Enum::STRING: {
        value_ = str;
        return true;
      }
      case Types::ValueType::Enum::ALERT_SEVERITY: {
        return false;
      }
      case Types::ValueType::Enum::MODULE_TYPE: {
        return false;
      }
      case Types::ValueType::Enum::MODULE_STATE: {
        return false;
      }
      case Types::ValueType::Enum::CHRONO_MINUTES: {
        return false;
      }
      case Types::ValueType::Enum::CHRONO_MILLISECONDS: {
        return false;
      }
      case Types::ValueType::Enum::NONE: {
        return false;
      }
    }
  } catch (...) {
    return false;
  }
  return false;
}

template<class... Ts>
struct overloaded : Ts ... {
  using Ts::operator()...;
};
template<class... Ts> overloaded(Ts...) -> overloaded<Ts...>;

std::string TestVariantValue::ToString() const {
  std::string out;
  std::visit(overloaded{
    [&out](const Types::Value<uint32_t> &arg) { out.append(arg.ToString()); },
    [&out](const Types::Value<uint16_t> &arg) { out.append(arg.ToString()); },
    [&out](const Types::Value<uint8_t> &arg) { out.append(arg.ToString()); },
    [&out](const Types::Value<int32_t> &arg) { out.append(arg.ToString()); },
    [&out](const Types::Value<int16_t> &arg) { out.append(arg.ToString()); },
    [&out](const Types::Value<int8_t> &arg) { out.append(arg.ToString()); },
    [&out](const Types::Value<double> &arg) { out.append(arg.ToString()); },
    [&out](const Types::Value<float> &arg) { out.append(arg.ToString()); },
    [&out](const uint32_t &arg) { out.append(std::to_string(arg)); },
    [&out](const uint16_t &arg) { out.append(std::to_string(arg)); },
    [&out](const uint8_t &arg) { out.append(std::to_string(arg)); },
    [&out](const int32_t &arg) { out.append(std::to_string(arg)); },
    [&out](const int16_t &arg) { out.append(std::to_string(arg)); },
    [&out](const int8_t &arg) { out.append(std::to_string(arg)); },
    [&out](const double &arg) { out.append(std::to_string(arg)); },
    [&out](const float &arg) { out.append(std::to_string(arg)); },
    [&out](bool arg) { out.append(std::to_string(arg)); },
    [&out](const std::string &arg) { out.append(arg); },
    [&out](const Types::ModuleType &arg) { out.append(arg.ToString()); },
    [&out](const Types::ModuleState &arg) { out.append(arg.ToString()); },
    [&out](Types::AlertSeverity arg) { out.append(arg.ToString()); },
    [&out](std::chrono::minutes arg) { out.append(std::to_string(arg.count())); },
    [&out](std::chrono::milliseconds arg) { out.append(std::to_string(arg.count())); },
    [&out](Types::ScuStatus arg) { out.append(arg.ToString()); },
    [&out](Types::OplStatus arg) { out.append(arg.ToString()); },
    [&out](const std::vector<uint32_t> &arg) {
      for (const auto &value : arg) {
        out.append(std::to_string(value)).append(", ");
      }
      if (!out.empty()) {
        out.resize(out.size() - 2);
      }
    }
  }, value_);
  return out;
}

}  // namespace Types
