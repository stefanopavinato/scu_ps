/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <string>

namespace Types {
class Direction {
 public:
  enum class Enum {
    IN,
    OUT,
    INOUT,
    NONE
  };

  Direction();

  Direction(const Direction &other);

  explicit Direction(const Enum &value);

  [[nodiscard]] static std::string GetTypeName();

  [[nodiscard]] Enum GetEnum() const;

  void SetEnum(const Enum &value);

  Direction &operator=(const Direction &other);

  bool operator==(const Direction &other) const;

  bool operator!=(const Direction &other) const;

  bool operator>(const Direction &other) const;

  bool operator<(const Direction &other) const;

  [[nodiscard]] std::string ToString() const;

  void FromString(const std::string &str);

  friend std::ostream &operator<<(std::ostream &out, const Direction &moduleType) {
    return out << moduleType.ToString();
  }

 private:
  Enum value_;

  static const char kTypeName[];

  static const char kInName[];
  static const char kOutName[];
  static const char kInOutName[];
  static const char kNoneName[];
};

}  // namespace Types
