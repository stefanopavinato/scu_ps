/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Affiliation.h"

namespace Types {

const char Affiliation::kExclusive[] = "Exclusive";
const char Affiliation::kShared[] = "Shared";
const char Affiliation::kNone[] = "None";

Affiliation::Affiliation()
  : id_(Enum::NONE) {
}

Affiliation::Affiliation(const Enum &id)
  : id_(id) {
}


Affiliation::Enum Affiliation::GetValue() const {
  return id_;
}

std::string Affiliation::ToString() const {
  switch (id_) {
    case Enum::SHARED: {
      return kShared;
    }
    case Enum::EXCLUSIVE: {
      return kExclusive;
    }
    case Enum::NONE: {
      return kNone;
    }
  }
  return kNone;
}

bool Affiliation::operator==(const Affiliation &other) const {
  return id_ == other.id_;
}

bool Affiliation::operator!=(const Affiliation &other) const {
  return id_ != other.id_;
}

}  // namespace Types
