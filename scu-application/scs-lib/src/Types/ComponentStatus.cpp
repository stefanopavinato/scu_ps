/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Messages/Visitor/VisitorBase.h"
#include "ComponentStatus.h"

namespace Types {

ComponentStatus::ComponentStatus()
  : isValid_(false), alertSeverity_(Types::AlertSeverity::Enum::OK), id_(Types::ComponentStatusId::Enum::Unknown),
    timeStamp_(std::chrono::system_clock::now()) {
}

void ComponentStatus::Accept(Messages::Visitor::VisitorBase *visitor) {
  visitor->Visit(this);
}

void ComponentStatus::Set(
  const bool &isValid,
  const AlertSeverity::Enum &alertSeverity,
  const ComponentStatusId::Enum &id) {
  isValid_ = isValid;
  alertSeverity_ = Types::AlertSeverity(alertSeverity);
  id_ = Types::ComponentStatusId(id);
  timeStamp_ = std::chrono::system_clock::now();
}

bool ComponentStatus::IsValid() const {
  return isValid_;
}

Types::ComponentStatusId ComponentStatus::GetId() const {
  return id_;
}

Types::AlertSeverity ComponentStatus::AlertSeverity() const {
  return alertSeverity_;
}

std::chrono::system_clock::time_point ComponentStatus::GetTimeStamp() const {
  return timeStamp_;
}

bool ComponentStatus::operator==(const ComponentStatus &other) const {
  return isValid_ == other.IsValid() &&
         alertSeverity_ == other.AlertSeverity() &&
         id_ == other.GetId();
}

bool ComponentStatus::operator!=(const ComponentStatus &other) const {
  return isValid_ != other.IsValid() ||
         alertSeverity_ != other.AlertSeverity() ||
         id_ != other.GetId();
}

}  // namespace Types
