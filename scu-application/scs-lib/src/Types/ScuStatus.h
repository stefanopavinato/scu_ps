/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <string>

namespace Types {
class ScuStatus {
 public:
  enum class Enum {
    STARTUP = 1,
    PBIT = 2,
    OPERATIONAL_NORMAL = 3,
    OPERATIONAL_WARNING = 4,
    ERROR = 5,
    FATAL_ERROR = 6,
    SHUTDOWN = 7,
    NONE
  };

  ScuStatus();

  [[nodiscard]] Enum GetEnum() const;

  void SetEnum(const Enum &value);

  [[nodiscard]] std::string ToString() const;

  bool operator==(const ScuStatus &other) const;

  bool operator!=(const ScuStatus &other) const;

 private:
  Enum value_;

  static const char kStartup[];
  static const char kPBIT[];
  static const char kOperationalNormal[];
  static const char kOperationalWarning[];
  static const char kError[];
  static const char kFatalError[];
  static const char kShutdown[];
  static const char kNone[];
};
}  // namespace Types
