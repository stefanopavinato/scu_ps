/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "OplStatus.h"

namespace Types {

const char OplStatus::kOK[] = "OK";
const char OplStatus::kBI[] = "BI";
const char OplStatus::kRBI[] = "RBI";
const char OplStatus::kEBI[] = "EBI";
const char OplStatus::kNone[] = "None";

OplStatus::OplStatus()
  : value_(Enum::NONE) {
}

void OplStatus::SetEnum(const Enum &value) {
  value_ = value;
}

OplStatus::Enum OplStatus::GetEnum() const {
  return value_;
}

std::string OplStatus::ToString() const {
  switch (value_) {
    case Enum::OK: {
      return kOK;
    }
    case Enum::BI: {
      return kBI;
    }
    case Enum::RBI: {
      return kRBI;
    }
    case Enum::EBI: {
      return kEBI;
    }
    case Enum::NONE: {
      return kNone;
    }
  }
  return kNone;
}

bool OplStatus::operator==(const OplStatus &other) const {
  return value_ == other.value_;
}

bool OplStatus::operator!=(const OplStatus &other) const {
  return value_ != other.value_;
}

}  // namespace Types
