/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <string>

namespace Types {
class WriteStrategy {
 public:
  enum class Enum {
    ON_UPDATE,
    ON_CHANGE,
    NONE
  };

  WriteStrategy();

  explicit WriteStrategy(const Enum &value);

  [[nodiscard]] Enum GetEnum() const;

  void SetEnum(const Enum &value);

  [[nodiscard]] std::string ToString() const;

  bool operator==(const WriteStrategy &other) const;

  bool operator!=(const WriteStrategy &other) const;

 private:
  Enum value_;

  static const char kOnUpdate[];
  static const char kOnChange[];
  static const char kNone[];
};
}  // namespace Types
