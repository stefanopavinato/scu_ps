/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "ModuleType.h"

namespace Types {

const char ModuleType::kTypeName[] = "ModuleType";

const char ModuleType::kSCU_PWSUPName[] = "FBIS-SCU-PWSUP";
const char ModuleType::kSCU_SERName[] = "FBIS-SCU-SER";
const char ModuleType::kSCU_FUName[] = "FBIS-SCU-FU";
const char ModuleType::kSCU_CLMCName[] = "FBIS-SCU-CLMC";
const char ModuleType::kSCU_RS485MCName[] = "FBIS-SCU-RS485MC";
const char ModuleType::kSCU_LVDSMCName[] = "FBIS-SCU-LVDSMC";
const char ModuleType::kSCU_ISMC1Name[] = "FBIS-SCU-ISMC_1";
const char ModuleType::kSCU_ISMC2Name[] = "FBIS-SCU-ISMC_2";
const char ModuleType::kSCU_CHMCName[] = "FBIS-SCU-CHMC";
const char ModuleType::kSCU_DYMCName[] = "FBIS-SCU-DYMC";
const char ModuleType::kSCU_BUMCName[] = "FBIS-SCU-BUMC";
const char ModuleType::kNoneName[] = "NONE";

ModuleType::ModuleType()
  : value_(Enum::NONE) {
}

ModuleType::ModuleType(const ModuleType &other)
  : value_(other.GetEnum()) {
}

ModuleType::ModuleType(const Enum &value)
  : value_(value) {
}

std::string ModuleType::GetTypeName() {
  return kTypeName;
}

ModuleType::Enum ModuleType::GetEnum() const {
  return value_;
}

void ModuleType::SetEnum(const Enum &value) {
  value_ = value;
}

ModuleType &ModuleType::operator=(const ModuleType &other) {
  if (this != &other) {
    value_ = other.GetEnum();
  }
  return *this;
}

bool ModuleType::operator==(const ModuleType &other) const {
  return (value_ == other.GetEnum());
}

bool ModuleType::operator!=(const ModuleType &other) const {
  return (value_ != other.GetEnum());
}

bool ModuleType::operator>(const ModuleType &other) const {
  return (value_ > other.GetEnum());
}

bool ModuleType::operator<(const ModuleType &other) const {
  return (value_ < other.GetEnum());
}

bool ModuleType::operator>=(const ModuleType &other) const {
  return (value_ >= other.GetEnum());
}

bool ModuleType::operator<=(const ModuleType &other) const {
  return (value_ <= other.GetEnum());
}

std::string ModuleType::ToString() const {
  switch (value_) {
    case Enum::SCU_PWSUP: {
      return kSCU_PWSUPName;
    }
    case Enum::SCU_SER: {
      return kSCU_SERName;
    }
    case Enum::SCU_FU: {
      return kSCU_FUName;
    }
    case Enum::SCU_DYMC: {
      return kSCU_DYMCName;
    }
    case Enum::SCU_BUMC: {
      return kSCU_BUMCName;
    }
    case Enum::SCU_CLMC: {
      return kSCU_CLMCName;
    }
    case Enum::SCU_RS485MC: {
      return kSCU_RS485MCName;
    }
    case Enum::SCU_LVDSMC: {
      return kSCU_LVDSMCName;
    }
    case Enum::SCU_ISMC1: {
      return kSCU_ISMC1Name;
    }
    case Enum::SCU_ISMC2: {
      return kSCU_ISMC2Name;
    }
    case Enum::SCU_CHMC: {
      return kSCU_CHMCName;
    }
    case Enum::NONE: {
      return kNoneName;
    }
  }
  return kNoneName;
}

void ModuleType::FromString(const std::string &str) {
  if (str == kSCU_PWSUPName) {
    value_ = Enum::SCU_PWSUP;
  } else if (str == kSCU_SERName) {
    value_ = Enum::SCU_SER;
  } else if (str == kSCU_FUName) {
    value_ = Enum::SCU_FU;
  } else if (str == kSCU_CLMCName) {
    value_ = Enum::SCU_CLMC;
  } else if (str == kSCU_RS485MCName) {
    value_ = Enum::SCU_RS485MC;
  } else if (str == kSCU_LVDSMCName) {
    value_ = Enum::SCU_LVDSMC;
  } else if (str == kSCU_ISMC1Name) {
    value_ = Enum::SCU_ISMC1;
  } else if (str == kSCU_ISMC2Name) {
    value_ = Enum::SCU_ISMC2;
  } else if (str == kSCU_CHMCName) {
    value_ = Enum::SCU_CHMC;
  } else if (str == kSCU_DYMCName) {
    value_ = Enum::SCU_DYMC;
  } else if (str == kSCU_BUMCName) {
    value_ = Enum::SCU_BUMC;
  } else {
    value_ = Enum::NONE;
  }
}

}  // namespace Types
