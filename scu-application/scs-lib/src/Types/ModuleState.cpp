/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "ModuleState.h"

namespace Types {

const char ModuleState::kDeInitialize[] = "DeInitialize";

const char ModuleState::kInitialize1[] = "Initialize1";
const char ModuleState::kInitialize2[] = "Initialize2";
const char ModuleState::kInitialize3[] = "Initialize3";

const char ModuleState::kEmpty[] = "Empty";
const char ModuleState::kStopped[] = "Stopped";
const char ModuleState::kRunning[] = "Running";

const char ModuleState::kInserted1[] = "Inserted1";
const char ModuleState::kInserted2[] = "Inserted2";
const char ModuleState::kInserted3[] = "Inserted3";
const char ModuleState::kInserted4[] = "Inserted4";

const char ModuleState::kRemoved1[] = "Removed1";
const char ModuleState::kRemoved2[] = "Removed2";
const char ModuleState::kRemoved3[] = "Removed3";
const char ModuleState::kRemoved4[] = "Removed4";


ModuleState::ModuleState(const Types::ModuleState::Enum &id)
  : id_(id) {
}

ModuleState::Enum ModuleState::GetEnum() const {
  return id_;
}

std::string ModuleState::ToString() const {
  switch (id_) {
    case Enum::INITIALIZE1: {
      return kInitialize1;
    }
    case Enum::INITIALIZE2: {
      return kInitialize2;
    }
    case Enum::INITIALIZE3: {
      return kInitialize3;
    }
    case Enum::DE_INITIALIZE: {
      return kDeInitialize;
    }
    case Enum::EMPTY: {
      return kEmpty;
    }
    case Enum::STOPPED: {
      return kStopped;
    }
    case Enum::RUNNING: {
      return kRunning;
    }
    case Enum::INSERTED1: {
      return kInserted1;
    }
    case Enum::INSERTED2: {
      return kInserted2;
    }
    case Enum::INSERTED3: {
      return kInserted3;
    }
    case Enum::INSERTED4: {
      return kInserted4;
    }
    case Enum::REMOVED1: {
      return kRemoved1;
    }
    case Enum::REMOVED2: {
      return kRemoved2;
    }
    case Enum::REMOVED3: {
      return kRemoved3;
    }
    case Enum::REMOVED4: {
      return kRemoved4;
    }
  }
}

}  // namespace Types


