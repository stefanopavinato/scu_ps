/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "JsonDetails.h"

namespace Types {

const char JsonDetails::kTypeName[] = "JsonDetails";

const char JsonDetails::kVerboseName[] = "Verbose";
const char JsonDetails::kCompactName[] = "Compact";
const char JsonDetails::kMinimalName[] = "Minimal";
const char JsonDetails::kConfigName[] = "Config";

JsonDetails::JsonDetails()
  : value_(Enum::VERBOSE) {
}

JsonDetails::JsonDetails(const JsonDetails &other)
  : value_(other.GetEnum()) {
}

JsonDetails::JsonDetails(const Enum &value)
  : value_(value) {
}

JsonDetails::JsonDetails(const uint32_t &value)
  : value_(static_cast<Enum>(value)) {
}

std::string JsonDetails::GetTypeName() {
  return kTypeName;
}

JsonDetails::Enum JsonDetails::GetEnum() const {
  return value_;
}

void JsonDetails::SetEnum(const Enum &value) {
  value_ = value;
}

}  // namespace Types
