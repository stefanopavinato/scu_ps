/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "ScuStatus.h"

namespace Types {

const char ScuStatus::kStartup[] = "Startup";
const char ScuStatus::kPBIT[] = "PBIT";
const char ScuStatus::kOperationalNormal[] = "OperationalNormal";
const char ScuStatus::kOperationalWarning[] = "OperationalWarning";
const char ScuStatus::kError[] = "Error";
const char ScuStatus::kFatalError[] = "FatalError";
const char ScuStatus::kShutdown[] = "Shutdown";
const char ScuStatus::kNone[] = "None";

ScuStatus::ScuStatus()
  : value_(Enum::OPERATIONAL_NORMAL) {
}

void ScuStatus::SetEnum(const Enum &value) {
  value_ = value;
}

ScuStatus::Enum ScuStatus::GetEnum() const {
  return value_;
}

std::string ScuStatus::ToString() const {
  switch (value_) {
    case Enum::STARTUP: {
      return kStartup;
    }
    case Enum::PBIT: {
      return kPBIT;
    }
    case Enum::OPERATIONAL_NORMAL: {
      return kOperationalNormal;
    }
    case Enum::OPERATIONAL_WARNING: {
      return kOperationalWarning;
    }
    case Enum::ERROR: {
      return kError;
    }
    case Enum::FATAL_ERROR: {
      return kFatalError;
    }
    case Enum::SHUTDOWN: {
      return kShutdown;
    }
    case Enum::NONE: {
      return kNone;
    }
  }
  return kNone;
}

bool ScuStatus::operator==(const ScuStatus &other) const {
  return value_ == other.value_;
}

bool ScuStatus::operator!=(const ScuStatus &other) const {
  return value_ != other.value_;
}

}  // namespace Types
