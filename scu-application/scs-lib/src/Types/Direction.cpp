/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Direction.h"

namespace Types {

const char Direction::kTypeName[] = "Direction";

const char Direction::kInName[] = "IN";
const char Direction::kOutName[] = "OUT";
const char Direction::kInOutName[] = "INOUT";
const char Direction::kNoneName[] = "NONE";


Direction::Direction()
  : value_(Enum::NONE) {
}

Direction::Direction(const Direction &other)
  : value_(other.GetEnum()) {
}

Direction::Direction(const Enum &value)
  : value_(value) {
}

std::string Direction::GetTypeName() {
  return kTypeName;
}

Direction::Enum Direction::GetEnum() const {
  return value_;
}

void Direction::SetEnum(const Enum &value) {
  value_ = value;
}

Direction &Direction::operator=(const Direction &other) {
  if (this != &other) {
    value_ = other.GetEnum();
  }
  return *this;
}

bool Direction::operator==(const Direction &other) const {
  return (value_ == other.GetEnum());
}

bool Direction::operator!=(const Direction &other) const {
  return (value_ != other.GetEnum());
}

bool Direction::operator>(const Direction &other) const {
  return (value_ > other.GetEnum());
}

bool Direction::operator<(const Direction &other) const {
  return (value_ < other.GetEnum());
}

std::string Direction::ToString() const {
  switch (value_) {
    case Enum::IN: {
      return kInName;
    }
    case Enum::OUT: {
      return kOutName;
    }
    case Enum::INOUT: {
      return kInOutName;
    }
    case Enum::NONE: {
      return kNoneName;
    }
  }
  return kNoneName;
}

void Direction::FromString(const std::string &str) {
  if (str == kInName) {
    value_ = Enum::IN;
  } else if (str == kOutName) {
    value_ = Enum::OUT;
  } else if (str == kInOutName) {
    value_ = Enum::INOUT;
  } else {
    value_ = Enum::NONE;
  }
}

}  // namespace Types
