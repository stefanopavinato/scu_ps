/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <chrono>
#include "Messages/Visitor/IVisitor.h"
#include "Types/AlertSeverity.h"
#include "Types/ComponentStatusId.h"

namespace Types {
class ComponentStatus : public Messages::Visitor::IVisitor {
 public:
  ComponentStatus();

  void Accept(Messages::Visitor::VisitorBase *visitor) final;

  void Set(
    const bool &isValid,
    const AlertSeverity::Enum &alertSeverity,
    const ComponentStatusId::Enum &id);

  [[nodiscard]] bool IsValid() const;

  [[nodiscard]] Types::AlertSeverity AlertSeverity() const;

  [[nodiscard]] Types::ComponentStatusId GetId() const;

  [[nodiscard]] std::chrono::system_clock::time_point GetTimeStamp() const;

  bool operator==(const ComponentStatus &other) const;

  bool operator!=(const ComponentStatus &other) const;

 private:
  bool isValid_;

  Types::AlertSeverity alertSeverity_;

  Types::ComponentStatusId id_;

  std::chrono::system_clock::time_point timeStamp_;
};
}  // namespace Types
