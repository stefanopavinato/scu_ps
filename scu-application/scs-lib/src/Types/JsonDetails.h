/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <string>

namespace Types {
class JsonDetails {
 public:
  enum class Enum {
    VERBOSE = 0,
    COMPACT = 1,
    MINIMAL = 2,
    CONFIG = 3
  };

  JsonDetails();

  JsonDetails(const JsonDetails &other);

  explicit JsonDetails(const Enum &value);

  explicit JsonDetails(const uint32_t &value);

  [[nodiscard]] static std::string GetTypeName();

  [[nodiscard]] Enum GetEnum() const;

  void SetEnum(const Enum &value);

 private:
  Enum value_;

  static const char kTypeName[];

  static const char kVerboseName[];
  static const char kCompactName[];
  static const char kMinimalName[];
  static const char kConfigName[];
};
}  // namespace Types
