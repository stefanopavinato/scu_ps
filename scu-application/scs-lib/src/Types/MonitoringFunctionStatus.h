/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <string>
#include "Messages/Visitor/IVisitor.h"
#include "Types/AlertSeverity.h"
#include "Types/MonitoringFunctionId.h"
#include "Types/ValueGroup.h"

namespace Types {
class MonitoringFunctionStatus : public Messages::Visitor::IVisitor {
 public:
  MonitoringFunctionStatus();

  MonitoringFunctionStatus(
    const Types::AlertSeverity::Enum &alertSeverity,
    const Types::MonitoringFunctionId::Enum &id,
    const Types::ValueGroup::Enum &group,
    const std::string &description);

  void Accept(Messages::Visitor::VisitorBase *visitor) final;

  [[nodiscard]] Types::AlertSeverity AlertSeverity() const;

  [[nodiscard]] Types::MonitoringFunctionId Id() const;

  [[nodiscard]] Types::ValueGroup Group() const;

  [[nodiscard]] std::string Description() const;

 private:
  Types::AlertSeverity alertSeverity_;

  Types::MonitoringFunctionId id_;

  Types::ValueGroup group_;

  std::string description_;
};
}  // namespace Types
