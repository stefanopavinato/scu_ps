/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Messages/Visitor/VisitorBase.h"
#include "MonitoringFunctionStatus.h"

namespace Types {

MonitoringFunctionStatus::MonitoringFunctionStatus()
  : alertSeverity_(Types::AlertSeverity::Enum::OK) {
}

MonitoringFunctionStatus::MonitoringFunctionStatus(
  const Types::AlertSeverity::Enum &alertSeverity,
  const Types::MonitoringFunctionId::Enum &id,
  const Types::ValueGroup::Enum &group,
  const std::string &description)
  : alertSeverity_(alertSeverity), id_(id), group_(group), description_(description) {
}

void MonitoringFunctionStatus::Accept(Messages::Visitor::VisitorBase *visitor) {
  visitor->Visit(this);
}

Types::AlertSeverity MonitoringFunctionStatus::AlertSeverity() const {
  return alertSeverity_;
}

Types::MonitoringFunctionId MonitoringFunctionStatus::Id() const {
  return id_;
}

Types::ValueGroup MonitoringFunctionStatus::Group() const {
  return group_;
}

std::string MonitoringFunctionStatus::Description() const {
  return description_;
}


}  // namespace Types
