/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Unit.h"

namespace Types {

const char Unit::kTypeName[] = "StateMachineType";

const char Unit::kVolt[] = "V";
const char Unit::kAmpere[] = "A";
const char Unit::kDegreeCelsius[] = "°C";
const char Unit::kPercent[] = "%";
const char Unit::kRotationalSpeed[] = "rpm";
const char Unit::kNone[] = "";

Unit::Unit()
  : enum_(Enum::NONE) {
}

Unit::Unit(const Enum &value)
  : enum_(value) {
}

Unit::Unit(const Unit &other)
  : enum_(other.GetEnum()) {
}

std::string Unit::GetTypeName() {
  return kTypeName;
}

Unit::Enum Unit::GetEnum() const {
  return enum_;
}

void Unit::SetEnum(const Enum &value) {
  enum_ = value;
}

Unit &Unit::operator=(const Unit &other) {
  if (this != &other) {
    this->enum_ = other.GetEnum();
  }
  return *this;
}

bool Unit::operator==(const Unit &other) const {
  return (enum_ == other.GetEnum());
}

std::string Unit::ToString() const {
  switch (enum_) {
    case Enum::VOLTAGE: {
      return kVolt;
    }
    case Enum::AMPERE: {
      return kAmpere;
    }
    case Enum::DEGREE_CELSIUS: {
      return kDegreeCelsius;
    }
    case Enum::PERCENT: {
      return kPercent;
    }
    case Enum::ROTATIONAL_SPEED: {
      return kRotationalSpeed;
    }
    case Enum::NONE: {
      return kNone;
    }
  }
  return kNone;
}

void Unit::FromString(const std::string &str) {
  if (str == kVolt) {
    enum_ = Enum::VOLTAGE;
  } else if (str == kAmpere) {
    enum_ = Enum::AMPERE;
  } else if (str == kDegreeCelsius) {
    enum_ = Enum::DEGREE_CELSIUS;
  } else if (str == kPercent) {
    enum_ = Enum::PERCENT;
  } else if (str == kRotationalSpeed) {
    enum_ = Enum::ROTATIONAL_SPEED;
  } else {
    enum_ = Enum::NONE;
  }
}
}  // namespace Types
