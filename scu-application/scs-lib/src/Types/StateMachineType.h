/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <string>

namespace Types {
class StateMachineType {
 public:
  enum class Enum {
    INTERNAL_MODULE,
    EXTERNAL,
    NONE
  };

  StateMachineType();

  [[nodiscard]] static std::string GetTypeName();

  [[nodiscard]] Enum GetValue() const;

  void SetValue(const Enum &value);

  bool operator==(const StateMachineType &other) const;

  [[nodiscard]] std::string ToString() const;

  void FromString(const std::string &str);

 private:
  Enum value_;

  static const char kTypeName[];

  static const char kInternal[];
  static const char kExternal[];
  static const char kNone[];
};
}  // namespace Types
