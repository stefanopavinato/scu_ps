/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "UnitPrefix.h"

namespace Types {

const char UnitPrefix::kTypeName[] = "StateMachineType";

const char UnitPrefix::kMega[] = "M";
const char UnitPrefix::kKilo[] = "k";
const char UnitPrefix::kNone[] = "";
const char UnitPrefix::kDeci[] = "d";
const char UnitPrefix::kMilli[] = "m";
const char UnitPrefix::kMicro[] = "U";
const char UnitPrefix::kNano[] = "n";
const char UnitPrefix::kPico[] = "p";

UnitPrefix::UnitPrefix()
  : enum_(Enum::NONE) {
}

UnitPrefix::UnitPrefix(const Enum &value)
  : enum_(value) {
}

std::string UnitPrefix::GetTypeName() {
  return kTypeName;
}

UnitPrefix::Enum UnitPrefix::GetEnum() const {
  return enum_;
}

void UnitPrefix::SetEnum(const Enum &value) {
  enum_ = value;
}

uint32_t UnitPrefix::GetBase10Exponent(const UnitPrefix &other) const {
  return static_cast<uint32_t>(enum_) - static_cast<uint32_t>(other.GetEnum());
}

uint32_t UnitPrefix::GetBase10Exponent(const Enum &value) const {
  return static_cast<uint32_t>(enum_) - static_cast<uint32_t>(value);
}

bool UnitPrefix::operator==(const UnitPrefix &other) const {
  return enum_ == other.GetEnum();
}

int32_t UnitPrefix::operator-(const UnitPrefix &other) const {
  return (static_cast<int32_t>(enum_) - static_cast<int32_t>(other.GetEnum()));
}

std::string UnitPrefix::ToString() const {
  switch (enum_) {
    case Enum::MEGA: {
      return kMega;
    }
    case Enum::KILO: {
      return kKilo;
    }
    case Enum::NONE: {
      return kNone;
    }
    case Enum::DECI: {
      return kDeci;
    }
    case Enum::MILLI: {
      return kMilli;
    }
    case Enum::MICRO: {
      return kMicro;
    }
    case Enum::NANO: {
      return kNano;
    }
    case Enum::PICO: {
      return kPico;
    }
  }
  return kNone;
}

void UnitPrefix::FromString(const std::string &str) {
  if (str == kMega) {
    enum_ = Enum::MEGA;
  } else if (str == kKilo) {
    enum_ = Enum::KILO;
  } else if (str == kDeci) {
    enum_ = Enum::DECI;
  } else if (str == kMilli) {
    enum_ = Enum::MILLI;
  } else if (str == kMicro) {
    enum_ = Enum::MICRO;
  } else if (str == kNano) {
    enum_ = Enum::NANO;
  } else if (str == kPico) {
    enum_ = Enum::PICO;
  } else {
    enum_ = Enum::NONE;
  }
}
}  // namespace Types
