/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <string>

namespace Types {
class ValueGroup {
 public:
  enum class Enum {
    TEMPERATURE,
    SUPPLY,
    SPEED,
    ANY,
    NONE
  };

  ValueGroup();

  explicit ValueGroup(const Enum &value);

  [[nodiscard]] static std::string GetTypeName();

  [[nodiscard]] Enum GetValue() const;

  void SetValue(const Enum &value);

  [[nodiscard]] std::string ToString() const;

  void FromString(const std::string &str);

  bool operator==(const ValueGroup &other) const;

  bool operator!=(const ValueGroup &other) const;

 private:
  Enum value_;

  static const char kTypeName[];

  static const char kTemperature[];
  static const char kSupply[];
  static const char kSpeed[];
  static const char kAny[];
  static const char kNone[];
};
}  // namespace Types
