/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <string>

namespace Types {
class Affiliation {
 public:
  enum class Enum {
    EXCLUSIVE,
    SHARED,
    NONE
  };

  Affiliation();

  explicit Affiliation(const Enum &id);

  [[nodiscard]] Enum GetValue() const;

  [[nodiscard]] std::string ToString() const;

  bool operator==(const Affiliation &other) const;

  bool operator!=(const Affiliation &other) const;

 private:
  Enum id_;

  static const char kExclusive[];
  static const char kShared[];
  static const char kNone[];
};
}  // namespace Types
