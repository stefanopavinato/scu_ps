/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "AlertSeverity.h"

namespace Types {

const char AlertSeverity::kTypeName[] = "AlertSeverity";

const char AlertSeverity::kError[] = "ERROR";
const char AlertSeverity::kWarning[] = "WARNING";
const char AlertSeverity::kNotice[] = "NOTICE";
const char AlertSeverity::kInformational[] = "INFO";
const char AlertSeverity::kDebug[] = "DEBUG";
const char AlertSeverity::kOK[] = "OK";

AlertSeverity::AlertSeverity()
  : value_(Enum::OK) {
}

AlertSeverity::AlertSeverity(const Enum &value)
  : value_(value) {
}

std::string AlertSeverity::GetTypeName() {
  return kTypeName;
}

AlertSeverity::Enum AlertSeverity::GetEnum() const {
  return value_;
}

void AlertSeverity::SetEnum(const Enum &value) {
  value_ = value;
}

bool AlertSeverity::operator==(const AlertSeverity &other) const {
  return value_ == other.GetEnum();
}

bool AlertSeverity::operator!=(const AlertSeverity &other) const {
  return value_ != other.GetEnum();
}

bool AlertSeverity::operator>(const AlertSeverity &other) const {
  return value_ > other.GetEnum();
}

bool AlertSeverity::operator<(const AlertSeverity &other) const {
  return value_ < other.GetEnum();
}

bool AlertSeverity::operator<=(const AlertSeverity &other) const {
  return value_ <= other.GetEnum();
}

bool AlertSeverity::operator>=(const AlertSeverity &other) const {
  return value_ >= other.GetEnum();
}

std::string AlertSeverity::ToString() const {
  switch (value_) {
    case Enum::ERROR: {
      return kError;
    }
    case Enum::WARNING: {
      return kWarning;
    }
    case Enum::NOTICE: {
      return kNotice;
    }
    case Enum::INFORMATIONAL: {
      return kInformational;
    }
    case Enum::DEBUG: {
      return kDebug;
    }
    case Enum::OK: {
      return kOK;
    }
  }
  return kOK;
}

void AlertSeverity::FromString(const std::string &str) {
  if (str == kError) {
    value_ = Enum::ERROR;
  } else if (str == kWarning) {
    value_ = Enum::WARNING;
  } else if (str == kNotice) {
    value_ = Enum::NOTICE;
  } else if (str == kInformational) {
    value_ = Enum::INFORMATIONAL;
  } else if (str == kDebug) {
    value_ = Enum::DEBUG;
  } else {
    value_ = Enum::OK;
  }
}

}  // namespace Types
