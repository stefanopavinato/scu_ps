/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <string>

namespace Types {
class ForceStatus {
 public:
  enum class Enum {
    NONE,
    PERMANENT,
    ONCE
  };

  explicit ForceStatus(const Enum &value);

  [[nodiscard]] Enum GetValue() const;

  void SetValue(const Enum &value);

  [[nodiscard]] std::string ToString() const;

 private:
  Enum value_;

  static const char kNone[];
  static const char kPermanent[];
  static const char kOnce[];
};
}  // namespace Types
