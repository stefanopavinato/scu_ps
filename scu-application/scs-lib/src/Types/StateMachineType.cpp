/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "StateMachineType.h"

namespace Types {

const char StateMachineType::kTypeName[] = "StateMachineType";

const char StateMachineType::kInternal[] = "INTERNAL";
const char StateMachineType::kExternal[] = "EXTERNAL";
const char StateMachineType::kNone[] = "NONE";

StateMachineType::StateMachineType()
  : value_(Enum::NONE) {
}

std::string StateMachineType::GetTypeName() {
  return kTypeName;
}

StateMachineType::Enum StateMachineType::GetValue() const {
  return value_;
}

void StateMachineType::SetValue(const Enum &value) {
  value_ = value;
}

bool StateMachineType::operator==(const StateMachineType &other) const {
  return value_ == other.GetValue();
}

std::string StateMachineType::ToString() const {
  switch (value_) {
    case Enum::INTERNAL_MODULE: {
      return kInternal;
    }
    case Enum::EXTERNAL: {
      return kExternal;
    }
    case Enum::NONE: {
      return kNone;
    }
  }
  return kNone;
}

void StateMachineType::FromString(const std::string &str) {
  if (str == kInternal) {
    value_ = Enum::INTERNAL_MODULE;
  } else if (str == kExternal) {
    value_ = Enum::EXTERNAL;
  } else {
    value_ = Enum::NONE;
  }
}
}  // namespace Types
