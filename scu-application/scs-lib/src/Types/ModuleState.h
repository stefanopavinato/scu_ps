/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <string>

namespace Types {
class ModuleState {
 public:
  enum class Enum {
    INITIALIZE1 = 0,
    INITIALIZE2 = 14,
    INITIALIZE3 = 15,
    DE_INITIALIZE = 1,
    EMPTY = 2,
    STOPPED = 3,
    RUNNING = 4,
    INSERTED1 = 5,
    INSERTED2 = 6,
    INSERTED3 = 7,
    INSERTED4 = 8,
    REMOVED1 = 9,
    REMOVED2 = 10,
    REMOVED3 = 11,
    REMOVED4 = 12
  };

  explicit ModuleState(const Types::ModuleState::Enum &id);

  [[nodiscard]] Enum GetEnum() const;

  [[nodiscard]] std::string ToString() const;

 private:
  Enum id_;

  static const char kDeInitialize[];

  static const char kInitialize1[];
  static const char kInitialize2[];
  static const char kInitialize3[];

  static const char kEmpty[];
  static const char kStopped[];
  static const char kRunning[];

  static const char kInserted1[];
  static const char kInserted2[];
  static const char kInserted3[];
  static const char kInserted4[];

  static const char kRemoved1[];
  static const char kRemoved2[];
  static const char kRemoved3[];
  static const char kRemoved4[];
};
}  // namespace Types
