/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "TypeConverter.h"

namespace Utils {

template<typename TYPE_IN, typename TYPE_OUT>
bool TypeConverter::InnerTypeConverter<TYPE_IN, TYPE_OUT>::Convert(
  const TYPE_IN &in,
  TYPE_OUT *out) {
  *out = static_cast<TYPE_OUT>(in);
  return true;
}

template<>
bool TypeConverter::InnerTypeConverter<Types::Value<int32_t>, uint64_t>::Convert(
  const Types::Value<int32_t> &in,
  uint64_t *out) {
  *out = static_cast<uint64_t>(in.GetValue());
  return true;
}

template<>
bool TypeConverter::InnerTypeConverter<Types::Value<int16_t>, uint64_t>::Convert(
  const Types::Value<int16_t> &in,
  uint64_t *out) {
  *out = static_cast<uint64_t>(in.GetValue());
  return true;
}

template<>
bool TypeConverter::InnerTypeConverter<Types::Value<int8_t>, uint64_t>::Convert(
  const Types::Value<int8_t> &in,
  uint64_t *out) {
  *out = static_cast<uint64_t>(in.GetValue());
  return true;
}

template<>
bool TypeConverter::InnerTypeConverter<Types::Value<uint32_t>, uint64_t>::Convert(
  const Types::Value<uint32_t> &in,
  uint64_t *out) {
  *out = static_cast<uint64_t>(in.GetValue());
  return true;
}

template<>
bool TypeConverter::InnerTypeConverter<Types::Value<uint16_t>, uint64_t>::Convert(
  const Types::Value<uint16_t> &in,
  uint64_t *out) {
  *out = static_cast<uint64_t>(in.GetValue());
  return true;
}

template<>
bool TypeConverter::InnerTypeConverter<Types::Value<uint8_t>, uint64_t>::Convert(
  const Types::Value<uint8_t> &in,
  uint64_t *out) {
  *out = static_cast<uint64_t>(in.GetValue());
  return true;
}

template<>
bool TypeConverter::InnerTypeConverter<Types::Value<double>, uint64_t>::Convert(
  const Types::Value<double> &in,
  uint64_t *out) {
  *out = static_cast<uint64_t>(in.GetValue());
  return true;
}

template<>
bool TypeConverter::InnerTypeConverter<Types::Value<float>, uint64_t>::Convert(
  const Types::Value<float> &in,
  uint64_t *out) {
  *out = static_cast<uint64_t>(in.GetValue());
  return true;
}

template<>
bool TypeConverter::InnerTypeConverter<std::string, uint64_t>::Convert(
  const std::string &in,
  uint64_t *out) {
  try {
    *out = std::stoull(in);
    return true;
  } catch (...) {
    *out = 0;
    return false;
  }
}

template<>
bool TypeConverter::InnerTypeConverter<Types::ModuleType, uint64_t>::Convert(
  const Types::ModuleType &in,
  uint64_t *out) {
  *out = static_cast<uint64_t>(in.GetEnum());
  return true;
}

template<>
bool TypeConverter::InnerTypeConverter<Types::OplStatus, uint64_t>::Convert(
  const Types::OplStatus &in,
  uint64_t *out) {
  *out = static_cast<uint64_t>(in.GetEnum());
  return true;
}

template<>
bool TypeConverter::InnerTypeConverter<Types::ScuStatus, uint64_t>::Convert(
  const Types::ScuStatus &in,
  uint64_t *out) {
  *out = static_cast<uint64_t>(in.GetEnum());
  return true;
}

template<>
bool TypeConverter::InnerTypeConverter<std::chrono::minutes, uint64_t>::Convert(
  const std::chrono::minutes &in,
  uint64_t *out) {
  *out = static_cast<uint64_t>(in.count());
  return true;
}

template<>
bool TypeConverter::InnerTypeConverter<Types::Value<int32_t>, int32_t>::Convert(
  const Types::Value<int32_t> &in,
  int32_t *out) {
  *out = in.GetValue();
  return true;
}

template<>
bool TypeConverter::InnerTypeConverter<Types::Value<int16_t>, int16_t>::Convert(
  const Types::Value<int16_t> &in,
  int16_t *out) {
  *out = in.GetValue();
  return true;
}

template<>
bool TypeConverter::InnerTypeConverter<Types::Value<int8_t>, int8_t>::Convert(
  const Types::Value<int8_t> &in,
  int8_t *out) {
  *out = in.GetValue();
  return true;
}

template<>
bool TypeConverter::InnerTypeConverter<Types::Value<uint32_t>, uint32_t>::Convert(
  const Types::Value<uint32_t> &in,
  uint32_t *out) {
  *out = in.GetValue();
  return true;
}

template<>
bool TypeConverter::InnerTypeConverter<Types::Value<uint16_t>, uint16_t>::Convert(
  const Types::Value<uint16_t> &in,
  uint16_t *out) {
  *out = in.GetValue();
  return true;
}

template<>
bool TypeConverter::InnerTypeConverter<Types::Value<uint8_t>, uint8_t>::Convert(
  const Types::Value<uint8_t> &in,
  uint8_t *out) {
  *out = in.GetValue();
  return true;
}

template<>
bool TypeConverter::InnerTypeConverter<Types::Value<double>, double>::Convert(
  const Types::Value<double> &in,
  double *out) {
  *out = in.GetValue();
  return true;
}

template<>
bool TypeConverter::InnerTypeConverter<Types::Value<float>, float>::Convert(
  const Types::Value<float> &in,
  float *out) {
  *out = in.GetValue();
  return true;
}

template<>
bool TypeConverter::InnerTypeConverter<uint64_t, Types::Value<int32_t>>::Convert(
  const uint64_t &in,
  Types::Value<int32_t> *out) {
  out->SetValue(static_cast<int32_t>(in));
  return true;
}

template<>
bool TypeConverter::InnerTypeConverter<uint64_t, Types::Value<int16_t>>::Convert(
  const uint64_t &in,
  Types::Value<int16_t> *out) {
  out->SetValue(static_cast<int16_t>(in));
  return true;
}

template<>
bool TypeConverter::InnerTypeConverter<uint64_t, Types::Value<int8_t>>::Convert(
  const uint64_t &in,
  Types::Value<int8_t> *out) {
  out->SetValue(static_cast<int8_t>(in));
  return true;
}

template<>
bool TypeConverter::InnerTypeConverter<uint64_t, Types::Value<uint32_t>>::Convert(
  const uint64_t &in,
  Types::Value<uint32_t> *out) {
  out->SetValue(static_cast<uint32_t>(in));
  return true;
}

template<>
bool TypeConverter::InnerTypeConverter<uint64_t, Types::Value<uint16_t>>::Convert(
  const uint64_t &in,
  Types::Value<uint16_t> *out) {
  out->SetValue(static_cast<uint16_t>(in));
  return true;
}

template<>
bool TypeConverter::InnerTypeConverter<uint64_t, Types::Value<uint8_t>>::Convert(
  const uint64_t &in,
  Types::Value<uint8_t> *out) {
  out->SetValue(static_cast<uint8_t>(in));
  return true;
}

template<>
bool TypeConverter::InnerTypeConverter<uint64_t, Types::Value<double>>::Convert(
  const uint64_t &in,
  Types::Value<double> *out) {
  out->SetValue(static_cast<double>(in));
  return true;
}

template<>
bool TypeConverter::InnerTypeConverter<uint64_t, Types::Value<float>>::Convert(
  const uint64_t &in,
  Types::Value<float> *out) {
  out->SetValue(static_cast<float>(in));
  return true;
}

template<>
bool TypeConverter::InnerTypeConverter<uint64_t, std::string>::Convert(
  const uint64_t &in,
  std::string *out) {
  *out = std::to_string(in);
  return true;
}

template<>
bool TypeConverter::InnerTypeConverter<uint64_t, Types::ModuleType>::Convert(
  const uint64_t &in,
  Types::ModuleType *out) {
  out->SetEnum(static_cast<Types::ModuleType::Enum>(in));
  return true;
}

template<>
bool TypeConverter::InnerTypeConverter<uint64_t, Types::ScuStatus>::Convert(
  const uint64_t &in,
  Types::ScuStatus *out) {
  out->SetEnum(static_cast<Types::ScuStatus::Enum>(in));
  return true;
}

template<>
bool TypeConverter::InnerTypeConverter<uint64_t, Types::OplStatus>::Convert(
  const uint64_t &in,
  Types::OplStatus *out) {
  out->SetEnum(static_cast<Types::OplStatus::Enum>(in));
  return true;
}

template<>
bool TypeConverter::InnerTypeConverter<uint64_t, std::chrono::minutes>::Convert(
  const uint64_t &in,
  std::chrono::minutes *out) {
  *out = std::chrono::minutes(in);
  return true;
}

template<>
bool TypeConverter::InnerTypeConverter<int32_t, Types::Value<int32_t>>::Convert(
  const int32_t &in,
  Types::Value<int32_t> *out) {
  out->SetValue(in);
  return true;
}

template<>
bool TypeConverter::InnerTypeConverter<int16_t, Types::Value<int16_t>>::Convert(
  const int16_t &in,
  Types::Value<int16_t> *out) {
  out->SetValue(in);
  return true;
}

template<>
bool TypeConverter::InnerTypeConverter<int8_t, Types::Value<int8_t>>::Convert(
  const int8_t &in,
  Types::Value<int8_t> *out) {
  out->SetValue(in);
  return true;
}

template<>
bool TypeConverter::InnerTypeConverter<uint32_t, Types::Value<uint32_t>>::Convert(
  const uint32_t &in,
  Types::Value<uint32_t> *out) {
  out->SetValue(in);
  return true;
}

template<>
bool TypeConverter::InnerTypeConverter<uint16_t, Types::Value<uint16_t>>::Convert(
  const uint16_t &in,
  Types::Value<uint16_t> *out) {
  out->SetValue(in);
  return true;
}

template<>
bool TypeConverter::InnerTypeConverter<uint8_t, Types::Value<uint8_t>>::Convert(
  const uint8_t &in,
  Types::Value<uint8_t> *out) {
  out->SetValue(in);
  return true;
}

template<>
bool TypeConverter::InnerTypeConverter<float, Types::Value<float>>::Convert(
  const float &in,
  Types::Value<float> *out) {
  out->SetValue(in);
  return true;
}

template<>
bool TypeConverter::InnerTypeConverter<double, Types::Value<double>>::Convert(
  const double &in,
  Types::Value<double> *out) {
  out->SetValue(in);
  return true;
}

template<>
bool TypeConverter::InnerTypeConverter<Types::ModuleType, uint32_t>::Convert(
  const Types::ModuleType &in,
  uint32_t *out) {
  *out = static_cast<int32_t>(in.GetEnum());
  return true;
}


template<>
bool TypeConverter::InnerTypeConverter<uint32_t, Types::ModuleType>::Convert(
  const uint32_t &in,
  Types::ModuleType *out) {
  out->SetEnum(static_cast<Types::ModuleType::Enum>(in));
  return true;
}

template<>
bool TypeConverter::InnerTypeConverter<Types::ModuleType, std::string>::Convert(
  const Types::ModuleType &in,
  std::string *out) {
  *out = in.ToString();
  return true;
}

template<>
bool TypeConverter::InnerTypeConverter<std::string, Types::ModuleType>::Convert(
  const std::string &in,
  Types::ModuleType *out) {
  out->FromString(in);
  return true;
}

template<>
bool TypeConverter::InnerTypeConverter<std::chrono::minutes, uint32_t>::Convert(
  const std::chrono::minutes &in,
  uint32_t *out) {
  *out = static_cast<uint32_t>(in.count());
  return true;
}

template<>
bool TypeConverter::InnerTypeConverter<uint32_t, std::chrono::minutes>::Convert(
  const uint32_t &in,
  std::chrono::minutes *out) {
  *out = std::chrono::minutes(in);
  return true;
}

template<>
bool TypeConverter::InnerTypeConverter<int32_t, std::string>::Convert(
  const int32_t &in,
  std::string *out) {
  *out = std::to_string(in);
  return true;
}

template<>
bool TypeConverter::InnerTypeConverter<uint32_t, std::string>::Convert(
  const uint32_t &in,
  std::string *out) {
  *out = std::to_string(in);
  return true;
}

template<>
bool TypeConverter::InnerTypeConverter<uint8_t, std::string>::Convert(
  const uint8_t &in,
  std::string *out) {
  *out = std::to_string(in);
  return true;
}

template<>
bool TypeConverter::InnerTypeConverter<double, std::string>::Convert(
  const double &in,
  std::string *out) {
  *out = std::to_string(in);
  return true;
}

template<>
bool TypeConverter::InnerTypeConverter<std::string, int32_t>::Convert(
  const std::string &in,
  int32_t *out) {  try {
    *out = std::stoi(in);
    return true;
  } catch (...) {
    *out = 0;
    return false;
  }
}

template<>
bool TypeConverter::InnerTypeConverter<std::string, uint32_t>::Convert(
  const std::string &in,
  uint32_t *out) {  try {
    *out = std::stoul(in);
    return true;
  } catch (...) {
    *out = 0;
    return false;
  }
}

template<>
bool TypeConverter::InnerTypeConverter<std::string, uint8_t>::Convert(
  const std::string &in,
  uint8_t *out) {  try {
    *out = std::stoul(in);
    return true;
  } catch (...) {
    *out = 0;
    return false;
  }
}

template<>
bool TypeConverter::InnerTypeConverter<std::string, double>::Convert(
  const std::string &in,
  double *out) {  try {
    *out = std::stod(in);
    return true;
  } catch (...) {
    *out = 0;
    return false;
  }
}

}  // namespace Utils
