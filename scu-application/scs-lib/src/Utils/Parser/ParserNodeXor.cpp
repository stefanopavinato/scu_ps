/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "ParserNodeXor.h"


namespace Utils::Parser {
ParserNodeXor::ParserNodeXor(
  std::unique_ptr<ParserNode> expressionLeft,
  std::unique_ptr<ParserNode> expressionRight)
  : ParserNodeBinary(
  std::move(expressionLeft),
  std::move(expressionRight)) {
}

bool ParserNodeXor::Evaluate() {
  return ExpressionLeft()->Evaluate() ^ ExpressionRight()->Evaluate();
}

}  // namespace Utils::Parser
