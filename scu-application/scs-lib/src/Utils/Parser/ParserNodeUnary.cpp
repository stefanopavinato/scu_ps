/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "ParserNodeUnary.h"

namespace Utils::Parser {

ParserNodeUnary::ParserNodeUnary(
  std::unique_ptr<ParserNode> expression)
  : expression_(std::move(expression)) {
}

ParserNode *ParserNodeUnary::Expression() const {
  return expression_.get();
}

}  // namespace Utils::Parser
