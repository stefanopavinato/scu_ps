/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "ParserNodeOr.h"


namespace Utils::Parser {
ParserNodeOr::ParserNodeOr(
  std::unique_ptr<ParserNode> expressionLeft,
  std::unique_ptr<ParserNode> expressionRight)
  : ParserNodeBinary(
  std::move(expressionLeft),
  std::move(expressionRight)) {
}

bool ParserNodeOr::Evaluate() {
  return ExpressionLeft()->Evaluate() || ExpressionRight()->Evaluate();
}

}  // namespace Utils::Parser
