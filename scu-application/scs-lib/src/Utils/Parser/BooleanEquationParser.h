/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <stack>
#include <memory>
#include <string>
#include <list>
#include <unordered_map>
#include "SystemModules/Framework/Components/ItemBase.h"
#include "Token.h"
#include "ParserNode.h"

namespace Utils::Parser {
class BooleanEquationParser {
 public:
  static std::experimental::optional<std::unique_ptr<ParserNode>> Parse(
    const std::list<Token> &tokens,
    const std::unordered_map<std::string, bool const *> &variables);

  static bool CreateOperatorNode(
    std::stack<std::unique_ptr<ParserNode>> *nodes,
    std::stack<Token> *stack);
};
}  // namespace Utils::Parser
