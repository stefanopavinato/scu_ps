/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "ParserNodeAnd.h"
#include "ParserNodeOr.h"
#include "ParserNodeXor.h"
#include "ParserNodeNot.h"
#include "ParserNodeVariable.h"
#include "Token.h"
#include "BooleanEquationParser.h"

namespace Utils::Parser {
std::experimental::optional<std::unique_ptr<ParserNode>> BooleanEquationParser::Parse(
  const std::list<Token> &tokens,
  const std::unordered_map<std::string, bool const *> &variables) {
  std::stack<std::unique_ptr<ParserNode>> nodes;
  std::stack<Token> stack;
  // Iterate tokens
  for (const auto &token : tokens) {
    switch (token.GetType()) {
      // Create variable node
      case Token::Type::Variable: {
        auto item = variables.find(token.GetString());
        if (item == variables.end()) {
          // Error, variable not found
          return std::experimental::nullopt;
        }
        nodes.push(std::make_unique<ParserNodeVariable>(item->second));
        break;
      }
        // Push operator on stack, build nodes if necessary
      case Token::Type::Operator: {
        while (!stack.empty() &&
               stack.top().GetType() == Token::Type::Operator &&
               token.IsLeftAssociative() &&
               token.GetPrecedence() <= stack.top().GetPrecedence()) {
          CreateOperatorNode(&nodes, &stack);
        }
        stack.push(token);
        break;
      }
        // Push left parenthesis on stack
      case Token::Type::LeftParen: {
        stack.push(token);
        break;
      }
        // Handle right parenthesis
      case Token::Type::RightParen: {
        while (!stack.empty() &&
               stack.top().GetType() != Token::Type::LeftParen) {
          CreateOperatorNode(&nodes, &stack);
        }
        if (stack.top().GetType() != Token::Type::LeftParen) {
          // Error no left parenthesis found
          return std::experimental::nullopt;
        }
        stack.pop();
        break;
      }
        // Handle unknown tokens
      case Token::Type::Unknown: {
        return std::experimental::nullopt;
      }
      default: {
        return std::experimental::nullopt;
      }
    }
  }
  // All tokens read
  while (!stack.empty()) {
    CreateOperatorNode(&nodes, &stack);
  }
  return std::move(nodes.top());
}

bool BooleanEquationParser::CreateOperatorNode(
  std::stack<std::unique_ptr<ParserNode>> *nodes,
  std::stack<Token> *stack) {
  switch (stack->top().GetOperator()) {
    case Token::Operator::Xor: {
      auto var1 = std::move(nodes->top());
      nodes->pop();
      auto var2 = std::move(nodes->top());
      nodes->pop();
      nodes->push(std::make_unique<ParserNodeXor>(std::move(var1), std::move(var2)));
      break;
    }
    case Token::Operator::And: {
      auto var1 = std::move(nodes->top());
      nodes->pop();
      auto var2 = std::move(nodes->top());
      nodes->pop();
      nodes->push(std::make_unique<ParserNodeAnd>(std::move(var1), std::move(var2)));
      break;
    }
    case Token::Operator::Or: {
      auto var1 = std::move(nodes->top());
      nodes->pop();
      auto var2 = std::move(nodes->top());
      nodes->pop();
      nodes->push(std::make_unique<ParserNodeOr>(std::move(var1), std::move(var2)));
      break;
    }
    case Token::Operator::Not: {
      auto var1 = std::move(nodes->top());
      nodes->pop();
      nodes->push(std::make_unique<ParserNodeNot>(std::move(var1)));
      break;
    }
    default: {
      break;
    }
  }
  stack->pop();
  return true;
}

}  // namespace Utils::Parser
