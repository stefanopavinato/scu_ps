/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "ParserNodeBinary.h"

namespace Utils::Parser {
class ParserNodeOr : public ParserNodeBinary {
 public:
  ParserNodeOr(
    std::unique_ptr<ParserNode> expressionLeft,
    std::unique_ptr<ParserNode> expressionRight);

  bool Evaluate() override;
};
}  // namespace Utils::Parser
