/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <string>
#include "SystemModules/Framework/Components/ItemBase.h"
#include "ParserNode.h"

namespace Utils::Parser {
class ParserNodeVariable : public ParserNode {
 public:
  explicit ParserNodeVariable(
    bool const *variable);

  bool Evaluate() override;

 private:
  bool const *variable_;
};
}  // namespace Utils::Parser
