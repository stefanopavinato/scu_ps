/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <thread>
#include <condition_variable>
#include <mutex>
#include <atomic>
#include "Logger/ILogger.h"

namespace Utils {
class ThreadBase {
 public:
  ThreadBase(
    Logger::ILogger *logger,
    std::string name);

  virtual ~ThreadBase() = default;

  [[nodiscard]] bool Start();

  void Stop();

  [[nodiscard]] bool IsRunning() const;

 protected:
  Logger::ILogger * Logger();

  [[nodiscard]] virtual bool Run() = 0;

  [[nodiscard]] virtual bool Initialize() = 0;

  [[nodiscard]] virtual bool DeInitialize() = 0;

 private:
  static void Runner(ThreadBase *thread);

  Logger::ILogger *logger_;

  std::atomic<bool> run_;

  std::atomic<bool> isRunning_;

  std::thread thread_;

  std::condition_variable cv_;

  std::mutex mutex_;

  const std::string name_;
};
}  // namespace Utils
