/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "ThreadBase.h"

#include <utility>

namespace Utils {

ThreadBase::ThreadBase(
  Logger::ILogger *logger,
  std::string name)
  : logger_(logger)
  , run_(false)
  , isRunning_(false)
  , name_(std::move(name)) {
}

bool ThreadBase::Start() {
  Logger()->Info(name_);
  if (isRunning_) {
    logger_->Error(std::string("Thread already running: ").append(name_));
    return false;
  }
  if (!Initialize()) {
    logger_->Error(std::string("Failed to initialize thread: ").append(name_));
    return false;
  }
  run_ = true;
  isRunning_ = true;
  thread_ = std::thread(Runner, this);
  return true;
}

void ThreadBase::Stop() {
  Logger()->Info(name_);
  run_ = false;
  cv_.notify_one();
  thread_.join();
}

bool ThreadBase::IsRunning() const {
  return isRunning_;
}

Logger::ILogger * ThreadBase::Logger() {
  return logger_;
}

void ThreadBase::Runner(ThreadBase *thread) {
  try {
    while (thread->run_ && thread->Run()) {}
  } catch (...) {
    std::exception_ptr p = std::current_exception();
    thread->logger_->Error(std::string(thread->name_).append(": ").append(p.__cxa_exception_type()->name()));
  }
  try {
    while (thread->DeInitialize()) {}
  } catch (...) {
    std::exception_ptr p = std::current_exception();
    thread->logger_->Error(std::string(thread->name_).append(": ").append(p.__cxa_exception_type()->name()));
  }
  thread->isRunning_ = false;

  std::unique_lock<std::mutex> lock(thread->mutex_);
  thread->cv_.wait(lock, [thread]{return !thread->run_;});
}

}  // namespace Utils
