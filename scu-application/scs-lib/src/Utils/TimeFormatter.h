/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <string>
#include <chrono>

namespace Utils {
class TimeFormatter {
 public:
  [[nodiscard]] static std::string ToString(const std::chrono::system_clock::time_point &timePoint);
};
}  // namespace Utils
