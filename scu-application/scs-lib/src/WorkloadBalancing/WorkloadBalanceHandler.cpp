/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "ItemHandler.h"
#include "WorkloadBalanceHandler.h"
#include "Messages/Visitor/VisitorBase.h"

namespace WorkloadBalancing {

const char WorkloadBalanceHandler::kName[] = "WorkloadBalanceHandler";
const char WorkloadBalanceHandler::kDescription[] = "";
const char WorkloadBalanceHandler::kComponentType[] = "WorkloadBalanceHandler";

WorkloadBalanceHandler::WorkloadBalanceHandler(
  Logger::ILogger *logger)
  : Utils::ThreadBase(
    logger,
    kComponentType)
  , lockHandler_(nullptr)
  , frontHandlePosition_(true)
  , frontHandlePositionOld_(true)
  , updateInterval_(std::chrono::milliseconds(100)) {
}

void WorkloadBalanceHandler::Accept(Messages::Visitor::VisitorBase *visitor) {
  visitor->Visit(this);
}

std::string WorkloadBalanceHandler::GetName() const {
  return kName;
}

std::string WorkloadBalanceHandler::GetComponentType() const {
  return kName;
}

void WorkloadBalanceHandler::SetLockHandler(ResourceSharing::LockHandler *lockHandler) {
  lockHandler_ = lockHandler;
}

bool WorkloadBalanceHandler::Run() {
  auto timeNext = std::chrono::steady_clock::now() + updateInterval_;
  auto timePointNow = std::chrono::steady_clock::now();
  // Check if all item handlers are running
  for (auto &itemHandler : itemHandlers_) {
    if (!itemHandler.second->IsRunning()) {
      return false;
    }
  }
  // Pause item handler if front handle is open
  frontHandlePositionOld_ = frontHandlePosition_;
  if (frontHandle_->Read(&frontHandlePosition_) &&
      frontHandlePosition_ != frontHandlePositionOld_) {
    if (frontHandlePosition_) {
      Logger()->Info("front-handle open");
      for (auto &itemHandler : itemHandlers_) {
        itemHandler.second->Pause();
      }
    } else {
      Logger()->Info("front-handle close");
      for (auto &itemHandler : itemHandlers_) {
        itemHandler.second->Resume();
      }
    }
  }
  std::this_thread::sleep_until(timeNext);
  return true;
}

bool WorkloadBalanceHandler::Initialize() {
  // Initialize front handle switch
  frontHandle_ = std::make_unique<SystemModules::Framework::Accessors::AccessorGPIO>(
    Logger(),
    232,
    Types::Direction::Enum::IN,
    false);
  frontHandle_->Initialize();

  for (auto &itemHandler : itemHandlers_) {
    if (!itemHandler.second->Start()) {
      return false;
    }
  }
  return true;
}

bool WorkloadBalanceHandler::DeInitialize() {
  for (auto &itemHandler : itemHandlers_) {
    itemHandler.second->Stop();
  }
  return true;
}

bool WorkloadBalanceHandler::AddUpdateHandler(
  const Types::Affiliation::Enum &affiliation,
  const std::chrono::milliseconds &updateInterval) {
  auto itemHandler = std::make_unique<ItemHandler>(
    Utils::ThreadBase::Logger(),
    lockHandler_,
    affiliation,
    updateInterval);

  itemHandlers_.insert(std::make_pair(affiliation, std::move(itemHandler)));

  return true;
}

bool WorkloadBalanceHandler::AddItem(SystemModules::Framework::Components::ItemBase * item) {
  auto itemHandler = itemHandlers_.find(item->GetAffiliation());
  if (itemHandler == itemHandlers_.end()) {
    Utils::ThreadBase::Logger()->Error("Object not found");
    return false;
  }
  return itemHandler->second->AddItem(item);
}

bool WorkloadBalanceHandler::RemoveItem(SystemModules::Framework::Components::ItemBase * item) {
  auto itemHandler = itemHandlers_.find(item->GetAffiliation());
  if (itemHandler == itemHandlers_.end()) {
    Utils::ThreadBase::Logger()->Error("Object not found");
    return false;
  }
  return itemHandler->second->RemoveItem(item);
}

}  // namespace WorkloadBalancing
