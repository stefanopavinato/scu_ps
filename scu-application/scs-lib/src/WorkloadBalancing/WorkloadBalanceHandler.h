/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <unordered_map>
#include <memory>
#include "SystemModules/Framework/Components/ItemBase.h"
#include "ItemHandler.h"
#include "Messages/Visitor/IVisitor.h"

#include "SystemModules/Framework/Accessors/AccessorGPIO.h"

namespace WorkloadBalancing {
class WorkloadBalanceHandler : public Utils::ThreadBase, public Messages::Visitor::IVisitor {
 public:
  explicit WorkloadBalanceHandler(
    Logger::ILogger *logger);

  ~WorkloadBalanceHandler() override = default;

  void Accept(Messages::Visitor::VisitorBase *visitor) override;

  [[nodiscard]] std::string GetName() const;

  [[nodiscard]] std::string GetComponentType() const;

  void SetLockHandler(ResourceSharing::LockHandler *lockHandler);

  [[nodiscard]] bool AddUpdateHandler(
    const Types::Affiliation::Enum &affiliation,
    const std::chrono::milliseconds &updateInterval);

  [[nodiscard]] bool AddItem(SystemModules::Framework::Components::ItemBase * item);

  [[nodiscard]] bool RemoveItem(SystemModules::Framework::Components::ItemBase * item);

 protected:
  [[nodiscard]] bool Run() override;

  [[nodiscard]] bool Initialize() override;

  [[nodiscard]] bool DeInitialize() override;

 private:
  ResourceSharing::LockHandler *lockHandler_;

  std::unordered_map<Types::Affiliation::Enum, std::unique_ptr<ItemHandler>> itemHandlers_;

  std::unique_ptr<SystemModules::Framework::Accessors::AccessorGPIO> frontHandle_;

  bool frontHandlePosition_;
  bool frontHandlePositionOld_;

  std::chrono::milliseconds updateInterval_;

  static const char kName[];
  static const char kDescription[];
  static const char kComponentType[];
};
}  // namespace WorkloadBalancing
