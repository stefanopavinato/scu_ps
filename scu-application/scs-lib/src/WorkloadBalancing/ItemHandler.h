/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <chrono>
#include <list>
#include "Utils/ThreadBase.h"
#include "SystemModules/Framework/Components/ItemBase.h"

namespace ResourceSharing {
class Lock;
class LockHandler;
}  // namespace ResourceSharing
namespace WorkloadBalancing {
class ItemHandler : public Utils::ThreadBase, Messages::Visitor::IVisitor {
 public:
  ItemHandler(
    Logger::ILogger *logger,
    ResourceSharing::LockHandler *lockHandler,
    const Types::Affiliation::Enum &affiliation,
    const std::chrono::milliseconds &updateInterval);

  void Accept(Messages::Visitor::VisitorBase *visitor) override;

  [[nodiscard]] std::string GetName() const;

  [[nodiscard]] std::string GetComponentType() const;

  void Pause();

  void Resume();

  bool IsPaused() const;

  [[nodiscard]] bool AddItem(SystemModules::Framework::Components::ItemBase * item);

  [[nodiscard]] bool RemoveItem(SystemModules::Framework::Components::ItemBase * item);

 protected:
  [[nodiscard]] bool Run() override;

  [[nodiscard]] bool Initialize() override;

  [[nodiscard]] bool DeInitialize() override;

 private:
  Logger::ILogger *logger_;

  const std::chrono::milliseconds updateInterval_;

  std::list<SystemModules::Framework::Components::ItemBase *> items_;

  const Types::Affiliation affiliation_;

  ResourceSharing::Lock *lock_;

  std::atomic<bool> pause_;

  mutable std::mutex mutex_;

  static const char kName[];
};
}  // namespace WorkloadBalancing
