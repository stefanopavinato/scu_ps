/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "ResourceSharing/Lock.h"
#include "ResourceSharing/LockHandler.h"
#include "Messages/Visitor/VisitorBase.h"
#include "ItemHandler.h"

namespace WorkloadBalancing {

const char ItemHandler::kName[] = "ItemHandler";

ItemHandler::ItemHandler(
  Logger::ILogger *logger,
  ResourceSharing::LockHandler *lockHandler,
  const Types::Affiliation::Enum &affiliation,
  const std::chrono::milliseconds &updateInterval)
  : Utils::ThreadBase(
  logger,
  std::move(Types::Affiliation(affiliation).ToString()))
  , logger_(logger)
  , updateInterval_(updateInterval)
  , affiliation_(affiliation)
  , lock_(nullptr)
  , pause_(false) {
  if (lockHandler == nullptr) {
    logger_->Error("Lock handler not set");
    return;
  }
  // Set lock
  auto lock = std::make_unique<ResourceSharing::Lock>(
    logger_,
    affiliation_.ToString(),
    static_cast<uint32_t>(affiliation_.GetValue()));
  lock_ = lock.get();
  lockHandler->AddLock(std::move(lock));
}

void ItemHandler::Accept(Messages::Visitor::VisitorBase *visitor) {
  visitor->Visit(this);
}

std::string ItemHandler::GetName() const {
  return affiliation_.ToString();
}

std::string ItemHandler::GetComponentType() const {
  return kName;
}

bool ItemHandler::AddItem(SystemModules::Framework::Components::ItemBase * item) {
  std::lock_guard<std::mutex> lock{mutex_};
  items_.push_back(item);
  return true;
}

bool ItemHandler::RemoveItem(SystemModules::Framework::Components::ItemBase * item) {
  std::lock_guard<std::mutex> lock{mutex_};
  for (const auto i : items_) {
    if (i == item) {
      items_.remove(i);
      return true;
    }
  }
  return false;
}

void ItemHandler::Pause() {
  if (affiliation_.GetValue() == Types::Affiliation::Enum::SHARED) {
    Logger()->Info(affiliation_.ToString());
    pause_ = true;
  }
}

void ItemHandler::Resume() {
  if (affiliation_.GetValue() == Types::Affiliation::Enum::SHARED) {
    Logger()->Info(affiliation_.ToString());
    pause_ = false;
  }
}

bool ItemHandler::IsPaused() const {
  return pause_;
}

bool ItemHandler::Run() {
  // Get time
  auto timeNow = std::chrono::steady_clock::now();
  auto timeNext = timeNow + updateInterval_;
  // Test if pausing
  if (pause_) {
    std::this_thread::sleep_until(timeNext);
    return true;
  }
  // Acquire lock
  if (affiliation_.GetValue() != Types::Affiliation::Enum::EXCLUSIVE) {
    while (!lock_->Acquire()) {
      std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }
  }
  // Update items
  for (const auto &item : items_) {
    // Process inputs of child components
    (void) item->Update(timeNow);
  }
  if (affiliation_.GetValue() != Types::Affiliation::Enum::EXCLUSIVE) {
    // Delay release of lock (i2c-mux-idle-disconnect delay)
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    // Release lock
    lock_->Release();
  }
  // Calc duration
  /*
  auto duration = std::chrono::steady_clock::now() - timeNow;
  if (duration > updateInterval_) {
    logger_->Warning(std::string(affiliation_.ToString()).append(": update duration (")
        .append(std::to_string(duration.count()/1000000)).append(" ms) > interval time (")
        .append(std::to_string(updateInterval_.count())).append(" ms)"));
  }
  */
  std::this_thread::sleep_until(timeNext);
  return true;
}

bool ItemHandler::Initialize() {
  return true;
}

bool ItemHandler::DeInitialize() {
  return true;
}

}  // namespace WorkloadBalancing
