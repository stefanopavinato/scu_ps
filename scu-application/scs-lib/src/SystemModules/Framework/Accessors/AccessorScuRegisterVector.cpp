/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <string>
#include <vector>
#include <sys/ioctl.h>
#include <unistd.h>
#include <fcntl.h>
#include "Logger/ILogger.h"
#include "AccessorScuRegisterVector.h"

#define WRITE_IOCTL         _IOW('a', 'a', uint64_t*)   // ioctl macro
#define READ_ADDRESS_IOCTL  _IOWR('a', 'c', uint64_t*)  // ioctl macro

namespace SystemModules::Framework::Accessors {

template<typename T>
struct is_vector : public std::false_type {
};

template<typename T, typename A>
struct is_vector<std::vector<T, A>> : public std::true_type {
};

template<typename VALUE>
const char AccessorScuRegisterVector<VALUE>::kName[] = "AccessorScuRegisterVector";

template<typename VALUE>
const char AccessorScuRegisterVector<VALUE>::kDescription[] = "";

template<typename VALUE>
const char AccessorScuRegisterVector<VALUE>::kPath[] = "/dev/scu_register";

template<typename VALUE>
AccessorScuRegisterVector<VALUE>::AccessorScuRegisterVector(
  Logger::ILogger *logger,
  const uint64_t &address)
  : AccessorTemplate<VALUE>(logger, kName, kDescription), address_(address) {
}

template<typename VALUE>
bool AccessorScuRegisterVector<VALUE>::Initialize() {
  // No further initialization needed
  this->SetInitialized(true);
  return true;
}

template<typename VALUE>
bool AccessorScuRegisterVector<VALUE>::DeInitialize() {
  // No further de-initialization needed
  this->SetInitialized(false);
  return true;
}

template<typename VALUE>
bool AccessorScuRegisterVector<VALUE>::Read(VALUE *value) const {
  // Open the register
  auto fd = open(kPath, O_RDWR);
  if (fd < 0) {
    this->Logger()->Error(std::string("Failed to open path: ").append(kPath));
    return false;
  }

  if constexpr (is_vector<VALUE>::value) {
    for (auto i = 0; i < value->size(); i++) {
      uint64_t tempValue = address_ + i * 4;
      // Read the value
      if (ioctl(fd, READ_ADDRESS_IOCTL, reinterpret_cast<uint64_t *>(&tempValue)) < 0) {
        this->Logger()->Error(
          std::string("Failed to read from address [").append(std::to_string(address_)).append("]"));
        close(fd);
        return false;
      }
      value->at(i) = static_cast<uint32_t>(tempValue);
    }
  }
  // Close the register
  close(fd);
  return true;
}

template<typename VALUE>
bool AccessorScuRegisterVector<VALUE>::Write(const VALUE &value) {
  (void) value;
  return false;
}

template<typename VALUE>
std::experimental::optional<VALUE> AccessorScuRegisterVector<VALUE>::Read(
  Logger::ILogger *logger,
  const uint64_t &address,
  const uint32_t &size) {
  auto retVal = VALUE();
  retVal.resize(size);
  auto accessor = AccessorScuRegisterVector<VALUE>(
    logger,
    address);
  if (!accessor.Initialize()) {
    logger->Error("Failed to initialize scu register");
    return std::experimental::nullopt;
  }
  if (!accessor.Read(&retVal)) {
    logger->Error("Failed to read scu register");
    return std::experimental::nullopt;
  }
  if (!accessor.DeInitialize()) {
    logger->Error("Failed to de-initialize scu register");
    return std::experimental::nullopt;
  }
  return retVal;
}
}  // namespace SystemModules::Framework::Accessors
