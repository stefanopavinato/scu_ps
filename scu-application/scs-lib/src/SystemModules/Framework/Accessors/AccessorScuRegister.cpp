/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <string>
#include <sys/ioctl.h>
#include <unistd.h>
#include <fcntl.h>
#include "Logger/ILogger.h"
#include "Utils/TypeConverter.h"
#include "AccessorScuRegister.h"

#define WRITE_IOCTL         _IOW('a', 'a', uint64_t*)   // ioctl macro
#define READ_ADDRESS_IOCTL  _IOWR('a', 'c', uint64_t*)  // ioctl macro

namespace SystemModules::Framework::Accessors {

template<typename VALUE>
const char AccessorScuRegister<VALUE>::kName[] = "AccessorScuRegister";

template<typename VALUE>
const char AccessorScuRegister<VALUE>::kDescription[] = "";

template<typename VALUE>
const char AccessorScuRegister<VALUE>::kPath[] = "/dev/scu_register";

template<typename VALUE>
const uint64_t AccessorScuRegister<VALUE>::kMaskBase = 0x0000000000000001;

#ifdef PETALINUX_BUILD
template <typename VALUE>
const char AccessorScuRegister<VALUE>::kDevmem[] = "devmem";
#else
template<typename VALUE>
const char AccessorScuRegister<VALUE>::kDevmem[] = "/sbin/devmem";
#endif

template<typename VALUE>
AccessorScuRegister<VALUE>::AccessorScuRegister(
  Logger::ILogger *logger,
  const uint64_t &address,
  const uint32_t &position,
  const uint32_t &size)
  : AccessorTemplate<VALUE>(logger, kName, kDescription), address_(address), position_(position) {
  // Calc mask
  mask_ = 0;
  for (uint32_t i = 0; i < size; i++) {
    mask_ |= (kMaskBase << i);
  }
}

template<typename VALUE>
bool AccessorScuRegister<VALUE>::Initialize() {
  // No further initialization needed
  this->SetInitialized(true);
  return true;
}

template<typename VALUE>
bool AccessorScuRegister<VALUE>::DeInitialize() {
  // No further de-initialization needed
  this->SetInitialized(false);
  return true;
}

template<typename VALUE>
bool AccessorScuRegister<VALUE>::Read(VALUE *value) const {
  // Open the register
  auto fd = open(kPath, O_RDWR);
  if (fd < 0) {
    this->Logger()->Error(std::string("Failed to open path: ").append(kPath));
    return false;
  }
  uint64_t tempValue = address_;
  // Read the value
  if (ioctl(fd, READ_ADDRESS_IOCTL, reinterpret_cast<uint64_t *>(&tempValue)) < 0) {
    this->Logger()->Error(
      std::string("Failed to read from address [").append(std::to_string(address_)).append("]"));
    close(fd);
    return false;
  }
  // Close the register
  close(fd);
  // Shift and mask the value
  auto valueShift = (tempValue >> position_) & mask_;
  // Convert and scale the read value
  if (!Utils::TypeConverter::Convert(valueShift, value)) {
    this->Logger()->Error("Failed to convert value");
    return false;
  }
  return true;
}

template<typename VALUE>
bool AccessorScuRegister<VALUE>::Write(const VALUE &value) {
  uint64_t valueWrite;
  // Convert and scale the write value
  if (!Utils::TypeConverter::Convert(value, &valueWrite)) {
    this->Logger()->Error("Failed to convert value");
    return false;
  }
  // Shift and mask the value
  valueWrite = (valueWrite & mask_) << position_;
  // Open the register
  auto fd = open(kPath, O_RDWR);
  if (fd < 0) {
    this->Logger()->Error(std::string("Failed to open path: ").append(kPath));
    return false;
  }
  uint64_t tempValue = address_;
  // Read old value (set address)
  if (ioctl(fd, READ_ADDRESS_IOCTL, reinterpret_cast<uint64_t *>(&tempValue)) < 0) {
    this->Logger()->Error(
      std::string("Failed to read from address [").append(std::to_string(address_)).append("]"));
    close(fd);
    return false;
  }
  // Clear old value
  tempValue &= ~(static_cast<uint64_t>(mask_) << position_);
  // Set new value
  tempValue |= valueWrite;
  // Write new value
  if (ioctl(fd, WRITE_IOCTL, reinterpret_cast<uint64_t *>(&tempValue)) < 0) {
    this->Logger()->Error(
      std::string("Failed to write to address [").append(std::to_string(address_)).append("]"));
    close(fd);
    return false;
  }
  close(fd);
  return true;
}

template<typename VALUE>
std::experimental::optional<VALUE> AccessorScuRegister<VALUE>::Read(
  Logger::ILogger *logger,
  const uint64_t &address,
  const uint32_t &position,
  const uint32_t &size) {
  VALUE retVal;
  auto accessor = AccessorScuRegister<VALUE>(
    logger,
    address,
    position,
    size);
  if (!accessor.Initialize()) {
    logger->Error("Failed to initialize scu register");
    return std::experimental::nullopt;
  }
  if (!accessor.Read(&retVal)) {
    logger->Error("Failed to read scu register");
    return std::experimental::nullopt;
  }
  if (!accessor.DeInitialize()) {
    logger->Error("Failed to de-initialize scu register");
    return std::experimental::nullopt;
  }
  return retVal;
}

template<typename VALUE>
bool AccessorScuRegister<VALUE>::ResetHW(
  Logger::ILogger *logger) {
  logger->Info("Assert pma_init");
  system(std::string(kDevmem).append(" 0x80010080 8 0x2").c_str());
  sleep(1);
  logger->Info("Assert reset");
  system(std::string(kDevmem).append(" 0x80010080 8 0x3").c_str());
  sleep(1);
  logger->Info("De-assert reset");
  system(std::string(kDevmem).append(" 0x80010080 8 0x2").c_str());
  sleep(1);
  logger->Info("De-assert pma_init");
  system(std::string(kDevmem).append(" 0x80010080 8 0x0").c_str());
  sleep(1);
}

}  // namespace SystemModules::Framework::Accessors
