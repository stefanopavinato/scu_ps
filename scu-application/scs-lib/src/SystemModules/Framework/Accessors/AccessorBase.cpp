/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Logger/ILogger.h"
#include "Messages/Visitor/VisitorBase.h"
#include "AccessorBase.h"

#include <utility>

namespace SystemModules::Framework::Accessors {

AccessorBase::AccessorBase(
  Logger::ILogger *logger,
  std::string typeName,
  std::string description)
  : logger_(logger)
  , typeName_(std::move(typeName))
  , description_(std::move(description))
  , isInitialized_(false) {
}

void AccessorBase::Accept(Messages::Visitor::VisitorBase *visitor) {
  visitor->Visit(this);
}

Logger::ILogger *AccessorBase::Logger() const {
  return logger_;
}

std::string AccessorBase::GetTypeName() const {
  return typeName_;
}

std::string AccessorBase::GetDescription() const {
  return description_;
}

bool AccessorBase::IsInitialized() const {
  return isInitialized_;
}

void AccessorBase::SetInitialized(const bool &value) {
  isInitialized_ = value;
}

}  // namespace SystemModules::Framework::Accessors

