/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <chrono>
#include <experimental/optional>
#include "Types/Value.h"
#include "Types/ModuleType.h"
#include "Types/ModuleState.h"
#include "AccessorTemplate.h"

namespace Logger {
class ILogger;
}  // namespace Logger
namespace SystemModules::Framework::Accessors {
template<typename VALUE, typename SCALE, typename IO>
class AccessorFile : public AccessorTemplate<VALUE> {
 public:
  AccessorFile(
    Logger::ILogger *logger,
    std::string path,
    const SCALE &scaleMultiply,
    const SCALE &scaleDivide,
    const SCALE &scaleOffset = 0);

  ~AccessorFile() override = default;

  bool Initialize() override;

  bool DeInitialize() override;

  bool Read(VALUE *value) const override;

  bool Write(const VALUE &value) override;

  static std::experimental::optional<VALUE> Read(
    Logger::ILogger *logger,
    const std::string &path,
    const SCALE &scaleMultiply,
    const SCALE &scaleDivide,
    const SCALE &scaleOffset = 0);

 private:
  const std::string path_;

  const SCALE scaleMultiply_;

  const SCALE scaleDivide_;

  const SCALE scaleOffset_;

  static const char kName[];

  static const char kDescription[];
};

}  // namespace SystemModules::Framework::Accessors

#include "AccessorFile.tpp"
