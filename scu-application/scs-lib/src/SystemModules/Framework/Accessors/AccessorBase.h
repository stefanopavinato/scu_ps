/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <string>
#include "Messages/Visitor/IVisitor.h"

namespace Logger {
class ILogger;
}  // namespace Logger
namespace SystemModules::Framework::Accessors {
class AccessorBase : public Messages::Visitor::IVisitor {
 public:
  AccessorBase(
    Logger::ILogger *logger,
    std::string typeName,
    std::string description);

  ~AccessorBase() override = default;

  void Accept(Messages::Visitor::VisitorBase *visitor) override;

  [[nodiscard]] Logger::ILogger *Logger() const;

  [[nodiscard]] std::string GetTypeName() const;

  [[nodiscard]] std::string GetDescription() const;

  [[nodiscard]] bool IsInitialized() const;

  [[nodiscard]] virtual bool Initialize() = 0;

  [[nodiscard]] virtual bool DeInitialize() = 0;

 protected:
  void SetInitialized(const bool &value);

 private:
  Logger::ILogger *const logger_;

  const std::string typeName_;

  const std::string description_;

  bool isInitialized_;
};
}  // namespace SystemModules::Framework::Accessors
