/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <fstream>
#include <sstream>
#include "Logger/ILogger.h"
#include "AccessorGPIO.h"

namespace SystemModules::Framework::Accessors {

const char AccessorGPIO::kName[] = "General purpose output";

const char AccessorGPIO::kDescription[] = "General purpose output";

const char AccessorGPIO::kPathExport[] = "/sys/class/gpio/export";
const char AccessorGPIO::kPathUnExport[] = "/sys/class/gpio/unexport";
const char AccessorGPIO::kPathGpio[] = "/sys/class/gpio/gpio";
const char AccessorGPIO::kDirection[] = "/direction";
const char AccessorGPIO::kDirectionIn[] = "in";
const char AccessorGPIO::kDirectionOut[] = "out";
const char AccessorGPIO::kValue[] = "/value";

AccessorGPIO::AccessorGPIO(
  Logger::ILogger *logger,
  const uint32_t &gpio,
  Types::Direction::Enum direction,
  const bool &isInverted)
  : AccessorTemplate<bool>(
  logger,
  kName,
  kDescription)
  , gpio_(gpio)
  , direction_(direction)
  , isInverted_(isInverted)
  , pathDirection_(std::string(kPathGpio).append(std::to_string(gpio)).append(kDirection))
  , pathValue_(std::string(kPathGpio).append(std::to_string(gpio)).append(kValue)) {
}

bool AccessorGPIO::Initialize() {
  // Check if gpio is already initialized
  if (IsInitialized()) {
    std::stringstream stream;
    stream << "GPIO is already initialized (Id: " << gpio_ << ")";
    Logger()->Info(stream.str());
    return true;
  }
  // Export gpio
  auto fileExport = std::ofstream(kPathExport);
  if (!fileExport.is_open()) {
    std::stringstream stream;
    stream << "Failed to open export path (" << kPathExport << ")";
    Logger()->Error(stream.str());
    return false;
  }
  fileExport << gpio_;
  fileExport.close();
  // Set gpio direction
  bool retVal;
  switch (direction_.GetEnum()) {
    case Types::Direction::Enum::IN: {
      retVal = SetDirection(true);
      break;
    }
    case Types::Direction::Enum::OUT: {
      retVal = SetValue(false);
      retVal &= SetDirection(false);
      break;
    }
    case Types::Direction::Enum::INOUT: {
      // Initialize as input (default)
      retVal = SetDirection(true);
      break;
    }
    case Types::Direction::Enum::NONE: {
      Logger()->Error("Direction not set");
      retVal = false;
      break;
    }
  }
  SetInitialized(retVal);
  return true;
}

bool AccessorGPIO::DeInitialize() {
  // Check if gpio is already initialized
  if (!IsInitialized()) {
    std::stringstream stream;
    stream << "GPIO is already de-initialized (Id: " << gpio_ << ")";
    Logger()->Info(stream.str());
    return true;
  }
  SetInitialized(false);
  // Un-export gpio
  auto fileUnExport = std::ofstream(kPathUnExport);
  if (!fileUnExport.is_open()) {
    std::stringstream stream;
    stream << "Failed to open un-export path (" << kPathUnExport << ")";
    Logger()->Error(stream.str());
    return false;
  }
  fileUnExport << gpio_;
  fileUnExport.close();
  return true;
}

bool AccessorGPIO::Read(bool *value) const {
  // Check direction
  if (direction_.GetEnum() == Types::Direction::Enum::NONE) {
    Logger()->Error("Direction not set");
    return false;
  }
  // Read gpio
  bool tempVal;
  if (!GetValue(&tempVal)) {
    return false;
  }
  // Invert value if needed (XOR)
  *value = tempVal ^ isInverted_;
  return true;
}

bool AccessorGPIO::Write(const bool &value) {
  switch (direction_.GetEnum()) {
    case Types::Direction::Enum::IN: {
      Logger()->Error("Direction not valid");
      return false;
    }
    case Types::Direction::Enum::OUT: {
      // Write gpio
      return SetValue(value ^ isInverted_);
    }
    case Types::Direction::Enum::INOUT: {
      if (value ^ isInverted_) {
        // Set as input
        return SetDirection(true);
      } else {
        // Set as output
        if (!SetDirection(false)) {
          return false;
        }
        // Set value to false
        return SetValue(false);
      }
    }
    case Types::Direction::Enum::NONE: {
      Logger()->Error("Direction not set");
      return false;
    }
  }
  return false;
}

bool AccessorGPIO::SetDirection(const bool &isInput) {
  auto fileDirection = std::ofstream(pathDirection_);
  if (!fileDirection.is_open()) {
    std::stringstream stream;
    stream << "Failed to open direction path (" << pathDirection_ << ")";
    Logger()->Error(stream.str());
    return false;
  }
  if (isInput) {
    fileDirection << kDirectionIn;
  } else {
    fileDirection << kDirectionOut;
  }
  fileDirection.close();
  return true;
}

bool AccessorGPIO::SetValue(const bool &value) {
  auto fileWrite = std::ofstream(pathValue_);
  if (!fileWrite.is_open()) {
    std::stringstream stream;
    stream << "Failed to open value path (" << pathValue_ << ")";
    Logger()->Error(stream.str());
    return false;
  }
  fileWrite << std::noboolalpha << value;
  fileWrite.close();
  return true;
}

bool AccessorGPIO::GetValue(bool *value) const {
  // Read gpio
  auto fileRead = std::ifstream(pathValue_);
  if (!fileRead.is_open()) {
    std::stringstream stream;
    stream << "Failed to open value path (" << pathValue_ << ")";
    Logger()->Error(stream.str());
    return false;
  }
  if (fileRead.peek() == std::ifstream::traits_type::eof()) {
    std::stringstream stream;
    stream << "Failed to read value  (" << pathValue_ << ")";
    Logger()->Error(stream.str());
    fileRead.close();
    return false;
  }
  fileRead >> std::noboolalpha >> *value;
  fileRead.close();
  return true;
}

std::experimental::optional<bool> AccessorGPIO::Read(
  Logger::ILogger *logger,
  const uint32_t &gpio,
  const bool &isInverted) {
  bool retVal;
  auto accessor = AccessorGPIO(
    logger,
    gpio,
    Types::Direction::Enum::IN,
    isInverted);
  if (!accessor.Initialize()) {
    logger->Error("Failed to initialize gpio");
    return std::experimental::nullopt;
  }
  if (!accessor.Read(&retVal)) {
    logger->Error("Failed to read gpio");
    return std::experimental::nullopt;
  }
  if (!accessor.DeInitialize()) {
    logger->Error("Failed to de-initialize gpio");
    return std::experimental::nullopt;
  }
  return retVal;
}


}  // namespace SystemModules::Framework::Accessors
