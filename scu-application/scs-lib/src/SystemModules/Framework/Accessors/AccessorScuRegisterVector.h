/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <experimental/optional>
#include <cstdint>
#include <vector>
#include "AccessorTemplate.h"

namespace SystemModules::Framework::Accessors {
template<typename VALUE>
class AccessorScuRegisterVector : public AccessorTemplate<VALUE> {
 public:
  AccessorScuRegisterVector(
    Logger::ILogger *logger,
    const uint64_t &address);

  ~AccessorScuRegisterVector() override = default;

  bool Initialize() override;

  bool DeInitialize() override;

  bool Read(VALUE *value) const override;

  bool Write(const VALUE &value) override;

  static std::experimental::optional<VALUE> Read(
    Logger::ILogger *logger,
    const uint64_t &address,
    const uint32_t &size);

 private:
  const uint64_t address_;

  static const char kName[];

  static const char kDescription[];

  static const char kPath[];
};

template
class AccessorScuRegisterVector<std::vector<uint32_t>>;

}  // namespace SystemModules::Framework::Accessors
