/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <experimental/optional>
#include "Types/Direction.h"
#include "AccessorTemplate.h"

namespace SystemModules::Framework::Accessors {
class AccessorGPIO : public AccessorTemplate<bool> {
 public:
  AccessorGPIO(
    Logger::ILogger *logger,
    const uint32_t &gpio,
    Types::Direction::Enum direction,
    const bool &isInverted);

  ~AccessorGPIO() override = default;

  bool Initialize() override;

  bool DeInitialize() override;

  bool Read(bool *value) const override;

  bool Write(const bool &value) override;

  static std::experimental::optional<bool> Read(
    Logger::ILogger *logger,
    const uint32_t &gpio,
    const bool &isInverted);

 private:
  bool SetDirection(const bool &isInput);

  bool SetValue(const bool &value);

  bool GetValue(bool *value) const;

  const uint32_t gpio_;

  const Types::Direction direction_;

  const bool isInverted_;

  const std::string pathDirection_;

  const std::string pathValue_;

  static const char kName[];
  static const char kDescription[];

  static const char kPathExport[];
  static const char kPathUnExport[];
  static const char kPathGpio[];
  static const char kDirection[];
  static const char kDirectionIn[];
  static const char kDirectionOut[];
  static const char kValue[];
};
}  // namespace SystemModules::Framework::Accessors
