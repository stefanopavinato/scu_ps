/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <cstring>
#include <fcntl.h>
#include <unistd.h>
#include <cstdint>
#include <iostream>
#include "Utils/TypeConverter.h"
#include "Logger/ILogger.h"
#include "SystemModules/Framework/Components/I2CDevice24C02.h"
#include "AccessorEEProm.h"

namespace SystemModules::Framework::Accessors {

template<typename VALUE, typename VALUE_IN_OUT>
const char AccessorEEProm<VALUE, VALUE_IN_OUT>::kName[] = "EEPROM value";

template<typename VALUE, typename VALUE_IN_OUT>
const char AccessorEEProm<VALUE, VALUE_IN_OUT>::kDescription[] = "Accessor for a ee-prom value";

template<typename VALUE, typename VALUE_IN_OUT>
AccessorEEProm<VALUE, VALUE_IN_OUT>::AccessorEEProm(
  Logger::ILogger *logger,
  const std::string &path,
  const uint32_t &address,
  const uint32_t &length,
  const VALUE_IN_OUT &scaleMultiply,
  const VALUE_IN_OUT &scaleDivide)
  : AccessorTemplate<VALUE>(logger, kName, kDescription)
    , path_(path)
    , address_(address)
    , size_(length)
    , scaleMultiply_(scaleMultiply)
    , scaleDivide_(scaleDivide) {
  this->SetInitialized(true);
}

template<typename VALUE, typename VALUE_IN_OUT>
bool AccessorEEProm<VALUE, VALUE_IN_OUT>::Initialize() {
  // Note: This class does not need to be initialization
  return true;
}

template<typename VALUE, typename VALUE_IN_OUT>
bool AccessorEEProm<VALUE, VALUE_IN_OUT>::DeInitialize() {
  // Note: This class does not need to be de-initialization
  return true;
}

template<typename VALUE, typename VALUE_IN_OUT>
bool AccessorEEProm<VALUE, VALUE_IN_OUT>::Read(VALUE *value) const {
  // Open file
  auto fd = open(path_.c_str(), O_RDWR);
  if (fd < 0) {
    this->Logger()->Error(
      std::string("Failed to access eeprom: ").append(this->GetTypeName()).append(")"));
    return false;
  }
  std::vector<char> vector(size_, 0);
  // Read value
  auto retVal = pread(fd, vector.data(), size_, address_);
  // Close file
  close(fd);
  if (retVal < 0) {
    this->Logger()->Error(std::string("Failed to read eeprom: ").append(std::strerror(errno)));
    return false;
  }
  // Convert and scale value
  VALUE_IN_OUT valueScaled;
  if (!ConvertAndScaleReadValue(vector, &valueScaled)) {
    this->Logger()->Error("Failed to convert value");
    return false;
  }
  // Convert and scale the read value
  if (!Utils::TypeConverter::Convert(valueScaled, value)) {
    this->Logger()->Error("Failed to convert value");
    return false;
  }
  return true;
}

template<typename VALUE, typename VALUE_IN_OUT>
bool AccessorEEProm<VALUE, VALUE_IN_OUT>::Write(const VALUE &value) {
  VALUE_IN_OUT valueWrite;
  // Convert and scale the write value
  if (!Utils::TypeConverter::Convert(value, &valueWrite)) {
    this->Logger()->Error("Failed to convert value");
    return false;
  }
  // Scale and convert value
  std::vector<char> vector;
  if (!ScaleAndConvertWriteValue(valueWrite, &vector)) {
    this->Logger()->Error("Failed to convert value");
    return false;
  }
  // Open file
  auto fd = open(path_.c_str(), O_RDWR);
  if (fd < 0) {
    this->Logger()->Error(
      std::string("Failed to access eeprom: ").append(this->GetTypeName()).append(")"));
    return false;
  }
  // Write value
  auto retVal = pwrite(fd, vector.data(), size_, address_);
  // Close file
  close(fd);
  if (retVal < 0) {
    this->Logger()->Error(std::string("Failed to write eeprom: ").append(std::strerror(errno)));
    return false;
  }
  return true;
}

template<typename VALUE, typename VALUE_IN_OUT>
bool AccessorEEProm<VALUE, VALUE_IN_OUT>::ConvertAndScaleReadValue(
  const std::vector<char> &vectorRead,
  VALUE_IN_OUT *value) const {
  uint32_t tempVal = 0;
  for (uint32_t i = 0; i < sizeof(VALUE_IN_OUT); i++) {
    tempVal += (static_cast<uint32_t>(vectorRead[i]) << (i * 8));
  }
  auto castVal = static_cast<VALUE_IN_OUT>(tempVal);
  auto multVal = static_cast<VALUE_IN_OUT>(castVal * scaleMultiply_);
  *value = static_cast<VALUE_IN_OUT>(multVal / scaleDivide_);
  return true;
}

template<>
bool AccessorEEProm<bool>::ConvertAndScaleReadValue(
  const std::vector<char> &vectorRead,
  bool *value) const {
  *value = static_cast<bool>(vectorRead[0] & 0x01);
  return true;
}

template<>
bool AccessorEEProm<std::string>::ConvertAndScaleReadValue(
  const std::vector<char> &vector,
  std::string *value) const {
  std::string tempStr(vector.begin(), vector.end());
  size_t end = tempStr.find(' ');
  *value = tempStr.substr(0, end);
  return true;
  // Get all chars use:
  // return std::string(vector.begin(), vector.end());
}

template<>
bool AccessorEEProm<Types::ModuleType, std::string>::ConvertAndScaleReadValue(
  const std::vector<char> &vector,
  std::string *value) const {
  std::string tempStr(vector.begin(), vector.end());
  size_t end = tempStr.find(' ');
  *value = tempStr.substr(0, end);
  return true;
  // Get all chars use:
  // return std::string(vector.begin(), vector.end());
}

template<typename VALUE, typename VALUE_IN_OUT>
bool AccessorEEProm<VALUE, VALUE_IN_OUT>::ScaleAndConvertWriteValue(
  const VALUE_IN_OUT &value,
  std::vector<char> *vectorWrite) const {
  // Scale value
  auto multVal = static_cast<VALUE_IN_OUT>(value * scaleDivide_);
  auto divVal = static_cast<uint32_t>(multVal / scaleMultiply_);

  for (uint32_t i = 0; i < sizeof(VALUE_IN_OUT); i++) {
    vectorWrite->push_back(static_cast<char>((divVal >> (i * 8)) & 0xFF));
  }
  return true;
}

template<>
bool AccessorEEProm<bool>::ScaleAndConvertWriteValue(
  const bool &value,
  std::vector<char> *vectorWrite) const {
  vectorWrite->resize(this->size_);
  (*vectorWrite)[0] = value;
  return true;
}

template<>
bool AccessorEEProm<std::string>::ScaleAndConvertWriteValue(
  const std::string &value,
  std::vector<char> *vectorWrite) const {
  vectorWrite->clear();
  std::copy(value.begin(), value.end(), std::back_inserter(*vectorWrite));
  vectorWrite->resize(this->size_);
  return true;
}

template<>
bool AccessorEEProm<Types::ModuleType, std::string>::ScaleAndConvertWriteValue(
  const std::string &value,
  std::vector<char> *vectorWrite) const {
  vectorWrite->clear();
  std::copy(value.begin(), value.end(), std::back_inserter(*vectorWrite));
  vectorWrite->resize(this->size_);
  return true;
}

}  // namespace SystemModules::Framework::Accessors
