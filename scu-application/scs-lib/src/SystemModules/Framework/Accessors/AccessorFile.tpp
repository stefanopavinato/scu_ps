/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <string>
#include <iostream>
#include <fstream>
#include <utility>
#include "Utils/TypeConverter.h"
#include "Utils/TypeScaler.h"
#include "Logger/ILogger.h"

namespace SystemModules::Framework::Accessors {

template<typename VALUE, typename SCALE, typename IO>
const char AccessorFile<VALUE, SCALE, IO>::kName[] = "FileAccessor";

template<typename VALUE, typename SCALE, typename IO>
const char AccessorFile<VALUE, SCALE, IO>::kDescription[] = "File input of type: ";

template<typename VALUE, typename SCALE, typename IO>
AccessorFile<VALUE, SCALE, IO>::AccessorFile(
  Logger::ILogger *logger,
  std::string path,
  const SCALE &scaleMultiply,
  const SCALE &scaleDivide,
  const SCALE &scaleOffset)
  : AccessorTemplate<VALUE>(
  logger,
  kName,
  std::string(kDescription).append(typeid(VALUE).name()))
  , path_(std::move(path))
  , scaleMultiply_(scaleMultiply)
  , scaleDivide_(scaleDivide)
  , scaleOffset_(scaleOffset) {
}

template<typename VALUE, typename SCALE, typename IO>
bool AccessorFile<VALUE, SCALE, IO>::Initialize() {
  this->SetInitialized(true);
  // Note: This type of file does not need initialization
  return true;
}

template<typename VALUE, typename SCALE, typename IO>
bool AccessorFile<VALUE, SCALE, IO>::DeInitialize() {
  // Note: This type of file does not need de-initialization
  return true;
}

template<typename VALUE, typename SCALE, typename IO>
bool AccessorFile<VALUE, SCALE, IO>::Read(VALUE *value) const {
  // Open input file
  auto fileRead = std::ifstream(path_);
  if (!fileRead.is_open()) {
    this->Logger()->Error(std::string("Failed to open file path: ").append(path_));
    return false;
  }
  // Read the value
  IO valueRead;
  fileRead >> valueRead;
  // Close the file
  fileRead.close();
  // Convert to scale value
  SCALE valueConverted;
  if (!Utils::TypeConverter::Convert(
    valueRead,
    &valueConverted)) {
    this->Logger()->Error(
      std::string("Failed to convert value \"").append(valueRead).append("\" @path: ").append(path_));
    return false;
  }
  // Scale value and pack into value type
  if (!Utils::TypeScaler::ScaleRead<VALUE, SCALE>(
    valueConverted,
    scaleMultiply_,
    scaleDivide_,
    scaleOffset_,
    value)) {
    this->Logger()->Error("Failed to scale value");
    return false;
  }
  return true;
}

template<typename VALUE, typename SCALE, typename IO>
bool AccessorFile<VALUE, SCALE, IO>::Write(const VALUE &value) {
  // Convert to scalable value and scale
  SCALE valueScaled;
  if (!Utils::TypeScaler::ScaleWrite<VALUE, SCALE>(
    value,
    scaleMultiply_,
    scaleDivide_,
    scaleOffset_,
    &valueScaled)) {
    this->Logger()->Error("Failed to scale value");
    return false;
  }
  // Convert to write value type
  IO valueConverted;
  if (!Utils::TypeConverter::Convert(
    valueScaled,
    &valueConverted)) {
    this->Logger()->Error("Failed to convert value");
    return false;
  }
  // open the file
  auto fileWrite = std::ofstream(path_);
  if (!fileWrite.is_open()) {
    this->Logger()->Error(
      std::string("Failed to write to file: ").append(path_));
    return false;
  }
  // Write new value
  fileWrite << valueConverted;
  // Close the file
  fileWrite.close();
  return true;
}

template<typename VALUE, typename SCALE, typename IO>
std::experimental::optional<VALUE> AccessorFile<VALUE, SCALE, IO>::Read(
  Logger::ILogger *logger,
  const std::string &path,
  const SCALE &scaleMultiply,
  const SCALE &scaleDivide,
  const SCALE &scaleOffset) {
  VALUE retVal;
    auto accessor = AccessorFile<VALUE, SCALE, IO>(
      logger,
      path,
      scaleMultiply,
      scaleDivide,
      scaleOffset);
    if (!accessor.Initialize()) {
      logger->Error("Failed to initialize gpio");
      return std::experimental::nullopt;
    }
    if (!accessor.Read(&retVal)) {
      logger->Error("Failed to read gpio");
      return std::experimental::nullopt;
    }
    if (!accessor.DeInitialize()) {
      logger->Error("Failed to de-initialize gpio");
      return std::experimental::nullopt;
    }
    return retVal;
}

}  // namespace SystemModules::Framework::Accessors
