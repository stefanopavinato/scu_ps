/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <experimental/optional>
#include "Types/ModuleType.h"
#include "Types/ModuleState.h"
#include "Types/ScuStatus.h"
#include "Types/OplStatus.h"
#include "AccessorTemplate.h"

namespace Logger {
class ILogger;
}  // namespace Logger
namespace SystemModules::Framework::Accessors {
template<typename VALUE>
class AccessorScuRegister : public AccessorTemplate<VALUE> {
 public:
  AccessorScuRegister(
    Logger::ILogger *logger,
    const uint64_t &address,
    const uint32_t &position,
    const uint32_t &size);

  ~AccessorScuRegister() override = default;

  bool Initialize() override;

  bool DeInitialize() override;

  bool Read(VALUE *value) const override;

  bool Write(const VALUE &value) override;

  static std::experimental::optional<VALUE> Read(
    Logger::ILogger *logger,
    const uint64_t &address,
    const uint32_t &position,
    const uint32_t &size);

  static bool ResetHW(
    Logger::ILogger *logger);

 private:
  const uint64_t address_;

  const uint32_t position_;

  uint64_t mask_;

  static const char kName[];

  static const char kDescription[];

  static const char kPath[];

  static const uint64_t kMaskBase;

  static const char kDevmem[];
};

template
class AccessorScuRegister<int32_t>;

template
class AccessorScuRegister<int16_t>;

template
class AccessorScuRegister<int8_t>;

template
class AccessorScuRegister<uint32_t>;

template
class AccessorScuRegister<uint16_t>;

template
class AccessorScuRegister<uint8_t>;

template
class AccessorScuRegister<bool>;

template
class AccessorScuRegister<Types::ModuleType>;

template
class AccessorScuRegister<Types::OplStatus>;

template
class AccessorScuRegister<Types::ScuStatus>;
}  // namespace SystemModules::Framework::Accessors
