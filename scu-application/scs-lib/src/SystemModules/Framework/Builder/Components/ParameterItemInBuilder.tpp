/************************************************************
* @Description  :
* @Author       : ZHAW-IAMP-SKS, Philippe Hindermann
* @Copyright    :
************************************************************/

namespace SystemModules::Framework::Builder::Components {

template<typename VALUE>
ParameterItemInBuilder<VALUE>::ParameterItemInBuilder(
  Framework::Components::Container *parent,
  const std::string &name,
  const std::string &description,
  const Types::Affiliation::Enum &affiliation)
  : ComponentBuilder(parent), value_(std::make_unique<Framework::Components::ParameterItemIn<VALUE>>(
  GetParent()->Logger(),
  name,
  description)) {
}

template<typename VALUE>
void ParameterItemInBuilder<VALUE>::AndAdd() {
  this->GetParent()->AddComponent(std::move(value_));
}

template<typename VALUE>
Framework::Components::ParameterItemIn<VALUE> *ParameterItemInBuilder<VALUE>::AddAndReturnPointer() {
  auto retVal_ptr = value_.get();
  this->GetParent()->AddComponent(std::move(value_));
  return retVal_ptr;
}

}  // namespace SystemModules::Framework::Builder::Components
