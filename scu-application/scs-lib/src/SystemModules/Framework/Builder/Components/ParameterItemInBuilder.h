/************************************************************
* @Description  :
* @Author       : ZHAW-IAMP-SKS, Philippe Hindermann
* @Copyright    :
************************************************************/

#pragma once

#include "SystemModules/Framework/Components/ParameterItemIn.h"
#include "ComponentBuilder.h"

namespace SystemModules::Framework::Builder::Components {
template<typename VALUE>
class ParameterItemInBuilder : public ComponentBuilder {
 public:
  ParameterItemInBuilder(
    Framework::Components::Container *parent,
    const std::string &name,
    const std::string &description,
    const Types::Affiliation::Enum &affiliation);

  void AndAdd();

  [[nodiscard]] Framework::Components::ParameterItemIn<VALUE> *AddAndReturnPointer();

 private:
  std::unique_ptr<Framework::Components::ParameterItemIn<VALUE>> value_;
};
}  // namespace SystemModules::Framework::Builder::Components

#include "ParameterItemInBuilder.tpp"
