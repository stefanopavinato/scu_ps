/************************************************************
* @Description  :
* @Author       : ZHAW-IAMP-SKS, Philippe Hindermann
* @Copyright    :
************************************************************/

namespace SystemModules::Framework::Builder::Components {

template<typename VALUE>
ItemOutBuilder<VALUE>::ItemOutBuilder(
  Framework::Components::Container *parent,
  const std::string &name,
  const std::string &description,
  const std::chrono::milliseconds &updateInterval,
  const Types::Affiliation::Enum &affiliation,
  const Types::WriteStrategy::Enum &writeStrategy,
  const bool &checkWrittenValue,
  const VALUE &initValue,
  const bool &debug)
  : ComponentBuilder(parent), value_(std::make_unique<Framework::Components::ItemOut<VALUE>>(
  GetParent()->Logger(),
  name,
  description,
  updateInterval,
  affiliation,
  writeStrategy,
  checkWrittenValue,
  initValue,
  debug)) {
}

template<typename VALUE>
ItemOutBuilder<VALUE>::ItemOutBuilder(
  Framework::Components::Container *parent,
  const std::string &name,
  const std::string &description,
  const std::chrono::milliseconds &updateInterval,
  const Types::Affiliation::Enum &affiliation,
  const Types::WriteStrategy::Enum &writeStrategy,
  const VALUE &initValue,
  const Types::Unit::Enum &unit,
  const Types::UnitPrefix::Enum &prefix,
  const bool &debug)
  : ComponentBuilder(parent), value_(std::make_unique<Framework::Components::ItemOut<VALUE>>(
  GetParent()->Logger(),
  name,
  description,
  updateInterval,
  affiliation,
  writeStrategy,
  VALUE(initValue, unit, prefix),
  debug)) {
}

template<typename VALUE>
void ItemOutBuilder<VALUE>::AndAdd() {
  this->GetParent()->AddComponent(std::move(value_));
}

template<typename VALUE>
Framework::Components::ItemOut<VALUE> *ItemOutBuilder<VALUE>::AddAndReturnPointer() {
  auto retVal_ptr = value_.get();
  this->GetParent()->AddComponent(std::move(value_));
  return retVal_ptr;
}

}  // namespace SystemModules::Framework::Builder::Components
