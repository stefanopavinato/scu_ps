/************************************************************
* @Description  :
* @Author       : ZHAW-IAMP-SKS, Philippe Hindermann
* @Copyright    :
************************************************************/

#pragma once

#include <memory>
#include "ComponentBuilder.h"

namespace SystemModules::Framework {
namespace Components {
class I2CDevice24C02;
}  // namespace Components
namespace Builder::Components {
class I2CEEPromBuilderAdd : public ComponentBuilder {
 public:
  explicit I2CEEPromBuilderAdd(
    Framework::Components::Container *parent,
    std::unique_ptr<Framework::Components::I2CDevice24C02> container);

  void AndAdd();

  [[nodiscard]] Framework::Components::I2CDevice24C02 *AddAndReturnPointer();

 private:
  std::unique_ptr<Framework::Components::I2CDevice24C02> container_;
};
}  // namespace Builder::Components
}  // namespace SystemModules::Framework
