/************************************************************
* @Description  :
* @Author       : ZHAW-IAMP-SKS, Philippe Hindermann
* @Copyright    :
************************************************************/

namespace SystemModules::Framework::Builder::Components {

template<typename VALUE>
ItemInOutBuilder<VALUE>::ItemInOutBuilder(
  Framework::Components::Container *parent,
  const std::string &name,
  const std::string &description,
  const Types::ValueGroup::Enum &group,
  const std::chrono::milliseconds &updateInterval,
  const Types::Affiliation::Enum &affiliation,
  const Types::WriteStrategy::Enum &writeStrategy,
  const bool &checkWrittenValue,
  const VALUE &initValue,
  const bool &debug)
  : ComponentBuilder(parent), value_(std::make_unique<Framework::Components::ItemInOut<VALUE>>(
  GetParent()->Logger(),
  name,
  description,
  group,
  updateInterval,
  affiliation,
  writeStrategy,
  checkWrittenValue,
  initValue,
  debug)) {
}

template<typename VALUE>
void ItemInOutBuilder<VALUE>::AndAdd() {
  this->GetParent()->AddComponent(std::move(value_));
}

template<typename VALUE>
Framework::Components::ItemInOut<VALUE> *ItemInOutBuilder<VALUE>::AddAndReturnPointer() {
  auto retVal_ptr = value_.get();
  this->GetParent()->AddComponent(std::move(value_));
  return retVal_ptr;
}

}  // namespace SystemModules::Framework::Builder::Components
