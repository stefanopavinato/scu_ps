/************************************************************
* @Description  :
* @Author       : ZHAW-IAMP-SKS, Philippe Hindermann
* @Copyright    :
************************************************************/

namespace SystemModules::Framework::Builder::Components {

template<typename VALUE>
ItemInBuilder<VALUE>::ItemInBuilder(
  Framework::Components::Container *parent,
  const std::string &name,
  const std::string &description,
  const Types::ValueGroup::Enum &group,
  const std::chrono::milliseconds &updateInterval,
  const Types::Affiliation::Enum &affiliation,
  const size_t &size,
  const bool &debug)
  : ComponentBuilder(parent), value_(std::make_unique<Framework::Components::ItemIn<VALUE>>(
  GetParent()->Logger(),
  name,
  description,
  group,
  updateInterval,
  affiliation,
  size,
  debug)) {
  (void) debug;
}

template<typename VALUE>
ItemInBuilder<VALUE>::ItemInBuilder(
  Framework::Components::Container *parent,
  const std::string &name,
  const std::string &description,
  const Types::ValueGroup::Enum &group,
  const std::chrono::milliseconds &updateInterval,
  const Types::Affiliation::Enum &affiliation,
  const Types::Unit::Enum &unit,
  const Types::UnitPrefix::Enum &prefix,
  const size_t &size,
  const bool &debug)
  : ComponentBuilder(parent), value_(std::make_unique<Framework::Components::ItemIn<VALUE>>(
  GetParent()->Logger(),
  name,
  description,
  group,
  updateInterval,
  affiliation,
  VALUE(unit, prefix),
  size,
  debug)) {
}

template<typename VALUE>
void ItemInBuilder<VALUE>::AndAdd() {
  this->GetParent()->AddComponent(std::move(value_));
}

template<typename VALUE>
Framework::Components::ItemIn<VALUE> *ItemInBuilder<VALUE>::AddAndReturnPointer() {
  auto retVal_ptr = value_.get();
  this->GetParent()->AddComponent(std::move(value_));
  return retVal_ptr;
}

}  // namespace SystemModules::Framework::Builder::Components
