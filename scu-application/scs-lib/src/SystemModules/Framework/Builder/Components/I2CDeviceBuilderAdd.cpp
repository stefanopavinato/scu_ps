/************************************************************
* @Description  :
* @Author       : ZHAW-IAMP-SKS, Philippe Hindermann
* @Copyright    :
************************************************************/

#include "SystemModules/Framework/Components/I2CDevice.h"
#include "I2CDeviceBuilderAdd.h"

namespace SystemModules::Framework::Builder::Components {

I2CDeviceBuilderAdd::I2CDeviceBuilderAdd(
  Framework::Components::Container *parent,
  std::unique_ptr<Framework::Components::I2CDevice> container)
  : ComponentBuilder(parent), container_(std::move(container)) {
}

void I2CDeviceBuilderAdd::AndAdd() {
  this->GetParent()->AddComponent(std::move(container_));
}

Framework::Components::I2CDevice *I2CDeviceBuilderAdd::AddAndReturnPointer() {
  auto retVal_ptr = container_.get();
  this->GetParent()->AddComponent(std::move(container_));
  return retVal_ptr;
}

}  // namespace SystemModules::Framework::Builder::Components
