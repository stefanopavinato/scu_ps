/************************************************************
* @Description  :
* @Author       : ZHAW-IAMP-SKS, Philippe Hindermann
* @Copyright    :
************************************************************/

namespace SystemModules::Framework::Builder::Components {

template<typename VALUE>
ParameterItemOutBuilder<VALUE>::ParameterItemOutBuilder(
  Framework::Components::Container *parent,
  const std::string &name,
  const std::string &description,
  const Types::Affiliation::Enum &affiliation,
  const bool &checkWrittenValue,
  const VALUE &value)
  : ComponentBuilder(parent), value_(std::make_unique<Framework::Components::ParameterItemOut<VALUE>>(
  GetParent()->Logger(),
  name,
  description,
  checkWrittenValue,
  value)) {
}

template<typename VALUE>
void ParameterItemOutBuilder<VALUE>::AndAdd() {
  this->GetParent()->AddComponent(std::move(value_));
}

template<typename VALUE>
Framework::Components::ParameterItemOut<VALUE> *ParameterItemOutBuilder<VALUE>::AddAndReturnPointer() {
  auto retVal_ptr = value_.get();
  this->GetParent()->AddComponent(std::move(value_));
  return retVal_ptr;
}

}  // namespace SystemModules::Framework::Builder::Components
