/************************************************************
* @Description  :
* @Author       : ZHAW-IAMP-SKS, Philippe Hindermann
* @Copyright    :
************************************************************/

#pragma once

#include "SystemModules/Framework/Components/ParameterItemOut.h"
#include "ComponentBuilder.h"

namespace SystemModules::Framework::Builder::Components {
template<typename VALUE>
class ParameterItemOutBuilder : public ComponentBuilder {
 public:
  ParameterItemOutBuilder(
    Framework::Components::Container *parent,
    const std::string &name,
    const std::string &description,
    const Types::Affiliation::Enum &affiliation,
    const bool &checkWrittenValue,
    const VALUE &value);

  void AndAdd();

  [[nodiscard]] Framework::Components::ParameterItemOut<VALUE> *AddAndReturnPointer();

 private:
  std::unique_ptr<Framework::Components::ParameterItemOut<VALUE>> value_;
};
}  // namespace SystemModules::Framework::Builder::Components

#include "ParameterItemOutBuilder.tpp"
