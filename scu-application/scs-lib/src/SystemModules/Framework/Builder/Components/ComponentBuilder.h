/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <string>
#include <memory>
#include "Types/Value.h"
#include "Types/ValueGroup.h"
#include "Types/Affiliation.h"
#include "Types/WriteStrategy.h"
#include "SystemModules/Framework/Components/Container.h"

namespace SystemModules::Framework::Builder::Components {
template<typename VALUE>
class ItemInBuilder;

template<typename VALUE>
class ItemOutBuilder;

template<typename VALUE>
class ItemInOutBuilder;

template<typename VALUE>
class ParameterItemInBuilder;

template<typename VALUE>
class ParameterItemOutBuilder;

class ComponentBuilderAdd;

class I2CDeviceBuilderAdd;

class I2CEEPromBuilderAdd;

class ComponentBuilder {
  friend class Framework::Components::Container;

 public:
  template<typename VALUE>
  [[nodiscard]] ItemInBuilder<VALUE> ItemIn(
    const std::string &name,
    const std::string &description,
    const Types::ValueGroup::Enum &group,
    const std::chrono::milliseconds &updateInterval,
    const Types::Affiliation::Enum &affiliation,
    const size_t &size = 1,
    const bool &debug = false);

  template<typename VALUE>
  [[nodiscard]] ItemInBuilder<VALUE> ItemIn(
    const std::string &name,
    const std::string &description,
    const Types::ValueGroup::Enum &group,
    const std::chrono::milliseconds &updateInterval,
    const Types::Affiliation::Enum &affiliation,
    const Types::Unit::Enum &unit,
    const Types::UnitPrefix::Enum &prefix,
    const size_t &size = 1,
    const bool &debug = false);

  template<typename VALUE>
  [[nodiscard]] ItemOutBuilder<VALUE> ItemOut(
    const std::string &name,
    const std::string &description,
    const std::chrono::milliseconds &updateInterval,
    const Types::Affiliation::Enum &affiliation,
    const Types::WriteStrategy::Enum &writeStrategy,
    const bool &checkWrittenValue,
    const VALUE &initValue,
    const bool &debug = false);

  template<typename VALUE>
  [[nodiscard]] ItemOutBuilder<VALUE> ItemOut(
    const std::string &name,
    const std::string &description,
    const std::chrono::milliseconds &updateInterval,
    const Types::Affiliation::Enum &affiliation,
    const Types::WriteStrategy::Enum &writeStrategy,
    const VALUE &initValue,
    const Types::Unit::Enum &unit,
    const Types::UnitPrefix::Enum &prefix,
    const bool &debug = false);

  template<typename VALUE>
  [[nodiscard]] ItemInOutBuilder<VALUE> ItemInOut(
    const std::string &name,
    const std::string &description,
    const Types::ValueGroup::Enum &group,
    const std::chrono::milliseconds &updateInterval,
    const Types::Affiliation::Enum &affiliation,
    const Types::WriteStrategy::Enum &writeStrategy,
    const bool &checkWrittenValue,
    const VALUE &initValue = {},
    const bool &debug = false);

  template<typename VALUE>
  [[nodiscard]] ParameterItemInBuilder<VALUE> ParameterIn(
    const std::string &name,
    const std::string &description,
    const Types::Affiliation::Enum &affiliation);

  template<typename VALUE>
  [[nodiscard]] ParameterItemOutBuilder<VALUE> ParameterOut(
    const std::string &name,
    const std::string &description,
    const Types::Affiliation::Enum &affiliation,
    const bool &checkWrittenValue,
    const VALUE &value);

  [[nodiscard]] ComponentBuilderAdd Container(
    const std::string &name,
    const std::string &description);

  [[nodiscard]] I2CEEPromBuilderAdd EEProm(
    const std::string &name,
    const std::string &description,
    const uint16_t &address,
    const std::string &path);

 protected:
  explicit ComponentBuilder(Framework::Components::Container *parent);

  Framework::Components::Container *GetParent();

  static ComponentBuilder Create(Framework::Components::Container *container);

 private:
  Framework::Components::Container *parent_;
};

}  // namespace SystemModules::Framework::Builder::Components

#include "ComponentBuilder.tpp"
