/************************************************************
* @Description  :
* @Author       : ZHAW-IAMP-SKS, Philippe Hindermann
* @Copyright    :
************************************************************/

namespace SystemModules::Framework::Builder::Components {
template<typename VALUE>
ItemInBuilder <VALUE> ComponentBuilder::ItemIn(
  const std::string &name,
  const std::string &description,
  const Types::ValueGroup::Enum &group,
  const std::chrono::milliseconds &updateInterval,
  const Types::Affiliation::Enum &affiliation,
  const size_t &size,
  const bool &debug) {
  return ItemInBuilder<VALUE>(
    GetParent(),
    name,
    description,
    group,
    updateInterval,
    affiliation,
    size,
    debug);
}

template<typename VALUE>
ItemInBuilder <VALUE> ComponentBuilder::ItemIn(
  const std::string &name,
  const std::string &description,
  const Types::ValueGroup::Enum &group,
  const std::chrono::milliseconds &updateInterval,
  const Types::Affiliation::Enum &affiliation,
  const Types::Unit::Enum &unit,
  const Types::UnitPrefix::Enum &prefix,
  const size_t &size,
  const bool &debug) {
  return ItemInBuilder<VALUE>(
    GetParent(),
    name,
    description,
    group,
    updateInterval,
    affiliation,
    unit,
    prefix,
    size,
    debug);
}

template<typename VALUE>
ItemOutBuilder <VALUE> ComponentBuilder::ItemOut(
  const std::string &name,
  const std::string &description,
  const std::chrono::milliseconds &updateInterval,
  const Types::Affiliation::Enum &affiliation,
  const Types::WriteStrategy::Enum &writeStrategy,
  const bool &checkWrittenValue,
  const VALUE &initValue,
  const bool &debug) {
  return ItemOutBuilder<VALUE>(
    GetParent(),
    name,
    description,
    updateInterval,
    affiliation,
    writeStrategy,
    checkWrittenValue,
    initValue,
    debug);
}

template<typename VALUE>
ItemOutBuilder <VALUE> ComponentBuilder::ItemOut(
  const std::string &name,
  const std::string &description,
  const std::chrono::milliseconds &updateInterval,
  const Types::Affiliation::Enum &affiliation,
  const Types::WriteStrategy::Enum &writeStrategy,
  const VALUE &initValue,
  const Types::Unit::Enum &unit,
  const Types::UnitPrefix::Enum &prefix,
  const bool &debug) {
  return ItemOutBuilder<VALUE>(
    GetParent(),
    name,
    description,
    updateInterval,
    affiliation,
    writeStrategy,
    initValue,
    unit,
    prefix,
    debug);
}

template<typename VALUE>
ItemInOutBuilder <VALUE> ComponentBuilder::ItemInOut(
  const std::string &name,
  const std::string &description,
  const Types::ValueGroup::Enum &group,
  const std::chrono::milliseconds &updateInterval,
  const Types::Affiliation::Enum &affiliation,
  const Types::WriteStrategy::Enum &writeStrategy,
  const bool &checkWrittenValue,
  const VALUE &initValue,
  const bool &debug) {
  return ItemInOutBuilder<VALUE>(
    GetParent(),
    name,
    description,
    group,
    updateInterval,
    affiliation,
    writeStrategy,
    checkWrittenValue,
    initValue,
    debug);
}

template<typename VALUE>
[[nodiscard]] ParameterItemInBuilder <VALUE> ComponentBuilder::ParameterIn(
  const std::string &name,
  const std::string &description,
  const Types::Affiliation::Enum &affiliation) {
  return ParameterItemInBuilder<VALUE>(
    GetParent(),
    name,
    description,
    affiliation);
}

template<typename VALUE>
[[nodiscard]] ParameterItemOutBuilder <VALUE> ComponentBuilder::ParameterOut(
  const std::string &name,
  const std::string &description,
  const Types::Affiliation::Enum &affiliation,
  const bool &checkWrittenValue,
  const VALUE &value) {
  return ParameterItemOutBuilder<VALUE>(
    GetParent(),
    name,
    description,
    affiliation,
    checkWrittenValue,
    value);
}

}  // namespace SystemModules::Framework::Builder::Components