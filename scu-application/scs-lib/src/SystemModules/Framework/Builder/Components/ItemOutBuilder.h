/************************************************************
* @Description  :
* @Author       : ZHAW-IAMP-SKS, Philippe Hindermann
* @Copyright    :
************************************************************/

#pragma once

#include "SystemModules/Framework/Components/ItemOut.h"
#include "ComponentBuilder.h"

namespace SystemModules::Framework::Builder::Components {
template<typename VALUE>
class ItemOutBuilder : public ComponentBuilder {
 public:
  ItemOutBuilder(
    Framework::Components::Container *parent,
    const std::string &name,
    const std::string &description,
    const std::chrono::milliseconds &updateInterval,
    const Types::Affiliation::Enum &affiliation,
    const Types::WriteStrategy::Enum &writeStrategy,
    const bool &checkWrittenValue,
    const VALUE &initValue,
    const bool &debug);

  ItemOutBuilder(
    Framework::Components::Container *parent,
    const std::string &name,
    const std::string &description,
    const std::chrono::milliseconds &updateInterval,
    const Types::Affiliation::Enum &affiliation,
    const Types::WriteStrategy::Enum &writeStrategy,
    const VALUE &initValue,
    const Types::Unit::Enum &unit,
    const Types::UnitPrefix::Enum &prefix,
    const bool &debug);

  void AndAdd();

  [[nodiscard]] Framework::Components::ItemOut<VALUE> *AddAndReturnPointer();

 private:
  std::unique_ptr<Framework::Components::ItemOut<VALUE>> value_;
};
}  // namespace SystemModules::Framework::Builder::Components

#include "ItemOutBuilder.tpp"
