/************************************************************
* @Description  :
* @Author       : ZHAW-IAMP-SKS, Philippe Hindermann
* @Copyright    :
************************************************************/

#pragma once

#include "SystemModules/Framework/Components/ItemIn.h"
#include "ComponentBuilder.h"

namespace SystemModules::Framework::Builder::Components {
template<typename VALUE>
class ItemInBuilder : public ComponentBuilder {
 public:
  ItemInBuilder(
    Framework::Components::Container *parent,
    const std::string &name,
    const std::string &description,
    const Types::ValueGroup::Enum &group,
    const std::chrono::milliseconds &updateInterval,
    const Types::Affiliation::Enum &affiliation,
    const size_t &size,
    const bool &debug);

  ItemInBuilder(
    Framework::Components::Container *parent,
    const std::string &name,
    const std::string &description,
    const Types::ValueGroup::Enum &group,
    const std::chrono::milliseconds &updateInterval,
    const Types::Affiliation::Enum &affiliation,
    const Types::Unit::Enum &unit,
    const Types::UnitPrefix::Enum &prefix,
    const size_t &size,
    const bool &debug);

  void AndAdd();

  [[nodiscard]] Framework::Components::ItemIn<VALUE> *AddAndReturnPointer();

 private:
  std::unique_ptr<Framework::Components::ItemIn<VALUE>> value_;
};
}  // namespace SystemModules::Framework::Builder::Components

#include "ItemInBuilder.tpp"
