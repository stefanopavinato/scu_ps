/************************************************************
* @Description  :
* @Author       : ZHAW-IAMP-SKS, Philippe Hindermann
* @Copyright    :
************************************************************/

#pragma once

#include "SystemModules/Framework/Components/ItemInOut.h"
#include "ComponentBuilder.h"

namespace SystemModules::Framework::Builder::Components {
template<typename VALUE>
class ItemInOutBuilder : public ComponentBuilder {
 public:
  ItemInOutBuilder(
    Framework::Components::Container *parent,
    const std::string &name,
    const std::string &description,
    const Types::ValueGroup::Enum &group,
    const std::chrono::milliseconds &updateInterval,
    const Types::Affiliation::Enum &affiliation,
    const Types::WriteStrategy::Enum &writeStrategy,
    const bool &checkWrittenValue,
    const VALUE &initValue,
    const bool &debug);

  void AndAdd();

  [[nodiscard]] Framework::Components::ItemInOut<VALUE> *AddAndReturnPointer();

 private:
  std::unique_ptr<Framework::Components::ItemInOut<VALUE>> value_;
};
}  // namespace SystemModules::Framework::Builder::Components

#include "ItemInOutBuilder.tpp"
