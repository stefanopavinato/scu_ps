/************************************************************
* @Description  :
* @Author       : ZHAW-IAMP-SKS, Philippe Hindermann
* @Copyright    :
************************************************************/

#include "SystemModules/Framework/Components/I2CDevice24C02.h"
#include "I2CEEPRomBuilderAdd.h"

namespace SystemModules::Framework::Builder::Components {

I2CEEPromBuilderAdd::I2CEEPromBuilderAdd(
  Framework::Components::Container *parent,
  std::unique_ptr<Framework::Components::I2CDevice24C02> container)
  : ComponentBuilder(parent), container_(std::move(container)) {
}

void I2CEEPromBuilderAdd::AndAdd() {
  this->GetParent()->AddComponent(std::move(container_));
}

Framework::Components::I2CDevice24C02 *I2CEEPromBuilderAdd::AddAndReturnPointer() {
  auto retVal_ptr = container_.get();
  this->GetParent()->AddComponent(std::move(container_));
  return retVal_ptr;
}

}  // namespace SystemModules::Framework::Builder::Components
