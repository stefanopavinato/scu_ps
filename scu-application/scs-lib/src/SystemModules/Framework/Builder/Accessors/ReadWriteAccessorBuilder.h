/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <cstdint>
#include <string>
#include "SystemModules/Framework/Components/AccessorItem.h"

namespace SystemModules::Framework::Builder::Accessors {
template<typename VALUE>
class ReadWriteAccessorBuilder {
 public:
  explicit ReadWriteAccessorBuilder(
    SystemModules::Framework::Components::AccessorItem<VALUE> *value);

  void DigitalInputOutput(
    const uint32_t &gpio,
    const bool &isInverted);

  template<typename SCALE = VALUE, typename IO>
  void File(
    const std::string &path,
    const SCALE &scaleMultiply,
    const SCALE &scaleDivide);

  template<typename SCALE = VALUE>
  void I2CEEProm(
    const std::string& path,
    const uint32_t &address,
    const uint32_t &length,
    const SCALE &scaleMultiply,
    const SCALE &scaleDivide);

  void ScuRegister(
    const uint64_t &address,
    const uint32_t &position,
    const uint32_t &size);

 private:
  Framework::Components::AccessorItem<VALUE> *value_;
};
}  // namespace SystemModules::Framework::Builder::Accessors

#include "ReadWriteAccessorBuilder.tpp"
