/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <cstdint>
#include <string>
#include <vector>
#include "SystemModules/Framework/Components/AccessorItem.h"

namespace SystemModules::Framework::Builder::Accessors {
template<class VALUE>
class ReadAccessorBuilder {
 public:
  explicit ReadAccessorBuilder(
    Framework::Components::AccessorItem<VALUE> *accessorItem);

  void DigitalInput(
    const uint32_t &gpio,
    const bool &isInverted);

  template<class SCALE = VALUE, class IO>
  void File(
    const std::string &path,
    const SCALE &scaleMultiply,
    const SCALE &scaleDivide,
    const SCALE &scaleOffset = 0);

  template<class SCALE = VALUE>
  void I2CEEProm(
    const std::string &path,
    const uint32_t &address,
    const uint32_t &length,
    const SCALE &scaleMultiply,
    const SCALE &scaleDivide);

  void ScuRegister(
    const uint64_t &address,
    const uint32_t &position,
    const uint32_t &size);

  void ScuRegisterVector(
    const uint64_t &address);

  void IniFileValue(
    const std::string &path,
    const std::string &section,
    const std::string &key);

  void InternalStaticLocation(
    const VALUE &value);

 private:
  Framework::Components::AccessorItem<VALUE> *accessorItem_;
};

}  // namespace SystemModules::Framework::Builder::Accessors

#include "ReadAccessorBuilder.tpp"
