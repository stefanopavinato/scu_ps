/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <cstdint>
#include <string>
#include "SystemModules/Framework/Components/AccessorItem.h"

namespace SystemModules::Framework {
namespace Components {
class I2CDevice24C02;
}  // namespace Components
namespace Builder::Accessors {
template<class VALUE>
class WriteAccessorBuilder {
 public:
  explicit WriteAccessorBuilder(
    Framework::Components::AccessorItem<VALUE> *accessorItem);

  void DigitalOutput(
    const uint32_t &gpio,
    const bool &isInverted);

  template<class SCALE = VALUE, class IO>
  void File(
    const std::string &path,
    const SCALE &scaleMultiply,
    const SCALE &scaleDivide);

  template<class SCALE = VALUE>
  void I2CEEProm(
    Framework::Components::I2CDevice24C02 *eeprom,
    const uint32_t &address,
    const uint32_t &length,
    const SCALE &scaleMultiply,
    const SCALE &scaleDivide);

  void ScuRegister(
    const uint64_t &address,
    const uint32_t &position,
    const uint32_t &size);

 private:
  Framework::Components::AccessorItem<VALUE> *accessorItem_;
};

}  // namespace Builder::Accessors
}  // namespace SystemModules::Framework

#include "WriteAccessorBuilder.tpp"
