/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Accessors/AccessorGPIO.h"
#include "SystemModules/Framework/Accessors/AccessorFile.h"
#include "SystemModules/Framework/Accessors/AccessorEEProm.h"
#include "SystemModules/Framework/Accessors/AccessorScuRegister.h"

namespace SystemModules::Framework::Builder::Accessors {

template<class VALUE>
WriteAccessorBuilder<VALUE>::WriteAccessorBuilder(
  Framework::Components::AccessorItem<VALUE> *accessorItem)
  : accessorItem_(accessorItem) {
}

template<class VALUE>
void WriteAccessorBuilder<VALUE>::DigitalOutput(
  const uint32_t &gpio,
  const bool &isInverted) {
  accessorItem_->SetAccessor(
    std::make_unique<Framework::Accessors::AccessorGPIO>(
      accessorItem_->GetLogger(),
      gpio,
      Types::Direction::Enum::OUT,
      isInverted));
}

template<class VALUE>
void WriteAccessorBuilder<VALUE>::ScuRegister(
  const uint64_t &address,
  const uint32_t &position,
  const uint32_t &size) {
  accessorItem_->SetAccessor(
    std::make_unique<Framework::Accessors::AccessorScuRegister<VALUE>>(
      accessorItem_->GetLogger(),
      address,
      position,
      size));
}

template<class VALUE>
template<class SCALE, class IO>
void WriteAccessorBuilder<VALUE>::File(
  const std::string &path,
  const SCALE &scaleMultiply,
  const SCALE &scaleDivide) {
  accessorItem_->SetAccessor(
    std::make_unique<Framework::Accessors::AccessorFile<VALUE, SCALE, IO>>(
      accessorItem_->GetLogger(),
      path,
      scaleMultiply,
      scaleDivide));
}


template<class VALUE>
template<class SCALE>
void WriteAccessorBuilder<VALUE>::I2CEEProm(
  Framework::Components::I2CDevice24C02 *eeprom,
  const uint32_t &address,
  const uint32_t &length,
  const SCALE &scaleMultiply,
  const SCALE &scaleDivide) {
  accessorItem_->SetAccessor(
    std::make_unique<Framework::Accessors::AccessorEEProm<VALUE, SCALE>>(
      accessorItem_->Logger(),
      eeprom,
      address,
      length,
      scaleMultiply,
      scaleDivide));
}

}  // namespace SystemModules::Framework::Builder::Accessors
