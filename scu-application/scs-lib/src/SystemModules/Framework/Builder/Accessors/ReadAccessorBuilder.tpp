/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Accessors/AccessorGPIO.h"
#include "SystemModules/Framework/Accessors/AccessorFile.h"
#include "SystemModules/Framework/Accessors/AccessorEEProm.h"
#include "SystemModules/Framework/Accessors/AccessorScuRegister.h"
#include "SystemModules/Framework/Accessors/AccessorScuRegisterVector.h"
#include "SystemModules/Framework/Accessors/AccessorIniFile.h"
#include "SystemModules/Framework/Accessors/AccessorStaticValue.h"

namespace SystemModules::Framework::Builder::Accessors {

template<class VALUE>
ReadAccessorBuilder<VALUE>::ReadAccessorBuilder(
  Components::AccessorItem<VALUE> *accessorItem)
  : accessorItem_(accessorItem) {
}

template<class VALUE>
void ReadAccessorBuilder<VALUE>::DigitalInput(
  const uint32_t &gpio,
  const bool &isInverted) {
  accessorItem_->SetAccessor(
    std::make_unique<Framework::Accessors::AccessorGPIO>(
      accessorItem_->GetLogger(),
      gpio,
      Types::Direction::Enum::IN,
      isInverted));
}

template<class VALUE>
void ReadAccessorBuilder<VALUE>::ScuRegister(
  const uint64_t &address,
  const uint32_t &position,
  const uint32_t &size) {
  accessorItem_->SetAccessor(
    std::make_unique<Framework::Accessors::AccessorScuRegister<VALUE>>(
      accessorItem_->GetLogger(),
      address,
      position,
      size));
}

template<class VALUE>
void ReadAccessorBuilder<VALUE>::ScuRegisterVector(
  const uint64_t &address) {
  accessorItem_->SetAccessor(
    std::make_unique<Framework::Accessors::AccessorScuRegisterVector<VALUE>>(
      accessorItem_->GetLogger(),
      address));
}

template<class VALUE>
void ReadAccessorBuilder<VALUE>::IniFileValue(
  const std::string &path,
  const std::string &section,
  const std::string &key) {
  accessorItem_->SetAccessor(
    std::make_unique<Framework::Accessors::AccessorIniFile<VALUE>>(
      accessorItem_->GetLogger(),
      path,
      section,
      key));
}

template<class VALUE>
template<class SCALE, class IO>
void ReadAccessorBuilder<VALUE>::File(
  const std::string &path,
  const SCALE &scaleMultiply,
  const SCALE &scaleDivide,
  const SCALE &scaleOffset) {
  accessorItem_->SetAccessor(
    std::make_unique<Framework::Accessors::AccessorFile<VALUE, SCALE, IO>>(
      accessorItem_->GetLogger(),
      path,
      scaleMultiply,
      scaleDivide,
      scaleOffset));
}

template<class VALUE>
template<class SCALE>
void ReadAccessorBuilder<VALUE>::I2CEEProm(
  const std::string &path,
  const uint32_t &address,
  const uint32_t &length,
  const SCALE &scaleMultiply,
  const SCALE &scaleDivide) {
  accessorItem_->SetAccessor(
    std::make_unique<Framework::Accessors::AccessorEEProm<VALUE, SCALE>>(
      accessorItem_->GetLogger(),
      path,
      address,
      length,
      scaleMultiply,
      scaleDivide));
}

template<class VALUE>
void ReadAccessorBuilder<VALUE>::InternalStaticLocation(
  const VALUE &value) {
  accessorItem_->SetAccessor(
    std::make_unique<Framework::Accessors::AccessorStaticValue<VALUE>>(
      accessorItem_->GetLogger(),
      value));
}
}  // namespace SystemModules::Framework::Builder::Accessors
