/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Components/ItemInOut.h"
#include "SystemModules/Framework/Actions/ActionTemplate.h"
#include "SystemModules/Framework/Accessors/AccessorFile.h"
#include "SystemModules/Framework/Accessors/AccessorEEProm.h"
#include "SystemModules/Framework/Accessors/AccessorGPIO.h"

namespace SystemModules::Framework::Builder::Accessors {

template<typename VALUE>
ReadWriteAccessorBuilder<VALUE>::ReadWriteAccessorBuilder(
  Components::AccessorItem <VALUE> *value)
  : value_(value) {
}

template<typename VALUE>
void ReadWriteAccessorBuilder<VALUE>::DigitalInputOutput(
  const uint32_t &gpio,
  const bool &isInverted) {
  value_->SetAccessor(
    std::make_unique<Framework::Accessors::AccessorGPIO>(
      value_->GetLogger(),
      gpio,
      Types::Direction::Enum::INOUT,
      isInverted));
}

template<typename VALUE>
template<typename SCALE, typename IO>
void ReadWriteAccessorBuilder<VALUE>::File(
  const std::string &path,
  const SCALE &scaleMultiply,
  const SCALE &scaleDivide) {
  value_->SetAccessor(
    std::make_unique<Framework::Accessors::AccessorFile<VALUE, SCALE, IO>>(
      value_->GetLogger(),
      path,
      scaleMultiply,
      scaleDivide));
}

template<typename VALUE>
template<typename SCALE>
void ReadWriteAccessorBuilder<VALUE>::I2CEEProm(
  const std::string &path,
  const uint32_t &address,
  const uint32_t &length,
  const SCALE &scaleMultiply,
  const SCALE &scaleDivide) {
  value_->SetAccessor(
    std::make_unique<Framework::Accessors::AccessorEEProm<VALUE, SCALE>>(
      value_->GetLogger(),
      path,
      address,
      length,
      scaleMultiply,
      scaleDivide));
}

template<class VALUE>
void ReadWriteAccessorBuilder<VALUE>::ScuRegister(
  const uint64_t &address,
  const uint32_t &position,
  const uint32_t &size) {
  value_->SetAccessor(
    std::make_unique<Framework::Accessors::AccessorScuRegister<VALUE>>(
      value_->GetLogger(),
      address,
      position,
      size));
}

}  // namespace SystemModules::Framework::Builder::Accessors
