/************************************************************
* @Description  :
* @Author       : ZHAW-IAMP-SKS, Philippe Hindermann
* @Copyright    :
************************************************************/

#include "SystemModules/Framework/MonitoringFunctions/MFIsValueEqual.h"
#include "SystemModules/Framework/MonitoringFunctions/MFIsValueNotEqual.h"
#include "SystemModules/Framework/MonitoringFunctions/MFIsValueLower.h"
#include "SystemModules/Framework/MonitoringFunctions/MFIsValueHigher.h"
#include "SystemModules/Framework/MonitoringFunctions/MFIsValueEqualOrLower.h"
#include "SystemModules/Framework/MonitoringFunctions/MFIsValueEqualOrHigher.h"
#include "SystemModules/Framework/MonitoringFunctions/MFIsValueInside.h"
#include "SystemModules/Framework/MonitoringFunctions/MFIsValueOutside.h"

namespace SystemModules::Framework::Builder::MonitoringFunctions {

template<typename VALUE>
MFBuilder<VALUE>::MFBuilder(
  Framework::Components::MonitoringFunctionItem<VALUE> *value)
  : value_(value) {
}

template<typename VALUE>
void MFBuilder<VALUE>::IsEqualTo(
  const Types::AlertSeverity::Enum &severity,
  const Types::MonitoringFunctionId::Enum &id,
  const std::chrono::milliseconds &onDelay,
  const std::chrono::milliseconds &offDelay,
  const VALUE &compareValue) {
  value_->AddMonitoringFunction(
    std::make_unique<Framework::MonitoringFunctions::MFIsValueEqual<VALUE>>(
      severity,
      id,
      value_->GetGroup().GetValue(),
      onDelay,
      offDelay,
      compareValue));
}

template<typename VALUE>
void MFBuilder<VALUE>::IsNotEqualTo(
  const Types::AlertSeverity::Enum &severity,
  const Types::MonitoringFunctionId::Enum &id,
  const std::chrono::milliseconds &onDelay,
  const std::chrono::milliseconds &offDelay,
  const VALUE &compareValue) {
  value_->AddMonitoringFunction(
    std::make_unique<Framework::MonitoringFunctions::MFIsValueNotEqual<VALUE>>(
      severity,
      id,
      value_->GetGroup().GetValue(),
      onDelay,
      offDelay,
      compareValue));
}

template<typename VALUE>
void MFBuilder<VALUE>::IsLowerThan(
  const Types::AlertSeverity::Enum &severity,
  const Types::MonitoringFunctionId::Enum &id,
  const std::chrono::milliseconds &onDelay,
  const std::chrono::milliseconds &offDelay,
  const VALUE &compareValue) {
  value_->AddMonitoringFunction(
    std::make_unique<Framework::MonitoringFunctions::MFIsValueLower<VALUE>>(
      severity,
      id,
      Types::ValueGroup::Enum::NONE,
      onDelay,
      offDelay,
      compareValue));
}

template<typename VALUE>
void MFBuilder<VALUE>::IsEqualOrLowerThan(
  const Types::AlertSeverity::Enum &severity,
  const Types::MonitoringFunctionId::Enum &id,
  const std::chrono::milliseconds &onDelay,
  const std::chrono::milliseconds &offDelay,
  const VALUE &compareValue) {
  value_->AddMonitoringFunction(
    std::make_unique<Framework::MonitoringFunctions::MFIsValueEqualOrLower<VALUE>>(
      severity,
      id,
      value_->GetGroup().GetValue(),
      onDelay,
      offDelay,
      compareValue));
}

template<typename VALUE>
void MFBuilder<VALUE>::IsHigherThan(
  const Types::AlertSeverity::Enum &severity,
  const Types::MonitoringFunctionId::Enum &id,
  const std::chrono::milliseconds &onDelay,
  const std::chrono::milliseconds &offDelay,
  const VALUE &compareValue) {
  value_->AddMonitoringFunction(
    std::make_unique<Framework::MonitoringFunctions::MFIsValueHigher<VALUE>>(
      severity,
      id,
      Types::ValueGroup::Enum::NONE,
      onDelay,
      offDelay,
      compareValue));
}

template<typename VALUE>
void MFBuilder<VALUE>::IsEqualOrHigherThan(
  const Types::AlertSeverity::Enum &severity,
  const Types::MonitoringFunctionId::Enum &id,
  const std::chrono::milliseconds &onDelay,
  const std::chrono::milliseconds &offDelay,
  const VALUE &compareValue) {
  value_->AddMonitoringFunction(
    std::make_unique<Framework::MonitoringFunctions::MFIsValueEqualOrHigher<VALUE>>(
      severity,
      id,
      Types::ValueGroup::Enum::NONE,
      onDelay,
      offDelay,
      compareValue));
}

template<typename VALUE>
void MFBuilder<VALUE>::IsInside(
  const Types::AlertSeverity::Enum &severity,
  const Types::MonitoringFunctionId::Enum &id,
  const std::chrono::milliseconds &onDelay,
  const std::chrono::milliseconds &offDelay,
  const VALUE &lowerLimit,
  const VALUE &upperLimit) {
  value_->AddMonitoringFunction(
    std::make_unique<Framework::MonitoringFunctions::MFIsValueInside<VALUE>>(
      severity,
      id,
      Types::ValueGroup::Enum::NONE,
      onDelay,
      offDelay,
      lowerLimit,
      upperLimit));
}

template<typename VALUE>
void MFBuilder<VALUE>::IsOutside(
  const Types::AlertSeverity::Enum &severity,
  const Types::MonitoringFunctionId::Enum &id,
  const std::chrono::milliseconds &onDelay,
  const std::chrono::milliseconds &offDelay,
  const VALUE &lowerLimit,
  const VALUE &upperLimit) {
  value_->AddMonitoringFunction(
    std::make_unique<Framework::MonitoringFunctions::MFIsValueOutside<VALUE>>(
      severity,
      id,
      Types::ValueGroup::Enum::NONE,
      onDelay,
      offDelay,
      lowerLimit,
      upperLimit));
}

}  // namespace SystemModules::Framework::Builder::MonitoringFunctions
