/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

namespace SystemModules::Framework::Builder::Actions {

template<typename T>
ActionBuilderBase<T>::ActionBuilderBase(
  Framework::Components::ActionItem<T> *item)
  : item_(item) {
}

template<typename T>
Framework::Components::ActionItem<T> *ActionBuilderBase<T>::GetItem() {
  return item_;
}

template<typename T>
void ActionBuilderBase<T>::SetAction(
  std::unique_ptr<Framework::Actions::ActionTemplate<T>> action) {
  item_->SetAction(std::move(action));
}

}  // namespace SystemModules::Framework::Builder::Actions
