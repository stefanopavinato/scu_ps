/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "SystemModules/Framework/Actions/ActionGenericAlertSeverityEqualOrHigher.h"
#include "SystemModules/Framework/Actions/ActionGenericBooleanEquation.h"
#include "SystemModules/Framework/Actions/ActionCollectData.h"
#include "ActionBuilderBase.h"

namespace SystemModules::Framework::Builder::Actions {
template<typename VALUE>
class ActionBuilderGeneric : public ActionBuilderBase<VALUE> {
 public:
  explicit ActionBuilderGeneric(
    Framework::Components::ActionItem<VALUE> *value);

  [[nodiscard]] Framework::Actions::ActionGenericAlertSeverityEqualOrHigher *AlertSeverityEqualOrHigher(
    const Types::AlertSeverity::Enum &alertSeverity);

  [[nodiscard]] Framework::Actions::ActionGenericBooleanEquation *BooleanEquation(
    const std::string &equation,
    const Types::AlertSeverity::Enum &maximumAlertSeverity);

  [[nodiscard]] Framework::Actions::ActionCollectData *CollectData();

 private:
  std::unique_ptr<Framework::Actions::ActionGenericBooleanEquation> actionDynamic_;
};
}  // namespace SystemModules::Framework::Builder::Actions

#include "ActionBuilderGeneric.tpp"
