/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <memory>
#include "SystemModules/Framework/Actions/ActionGetValue.h"
#include "SystemModules/Framework/Actions/ActionStaticValue.h"
#include "SystemModules/Framework/Actions/ActionStatusOK.h"

namespace SystemModules::Framework::Builder::Actions {

template<typename T>
ActionBuilder<T>::ActionBuilder(
  Framework::Components::ActionItem<T> *item)
  : ActionBuilderBase<T>(item) {
}

template<typename VALUE>
ActionBuilder <VALUE> ActionBuilder<VALUE>::Create(
  Framework::Components::ActionItem<VALUE> *item) {
  return ActionBuilder(item);
}

template<typename T>
void ActionBuilder<T>::StaticValue(const T &value) {
  ActionBuilderBase<T>::SetAction(
    std::make_unique<Framework::Actions::ActionStaticValue<T>>(
      value));
}

template<typename T>
void ActionBuilder<T>::ReadValue(T *value) {
  ActionBuilderBase<T>::SetAction(
    std::make_unique<Framework::Actions::ActionGetValue<T>>(
      value));
}

template<typename T>
void ActionBuilder<T>::ItemStatus(
  Types::ComponentStatus const *itemStatus) {
  ActionBuilderBase<T>::SetAction(
    std::make_unique<Framework::Actions::ActionStatusOK>(
      itemStatus));
}

template<typename T>
ActionBuilderGeneric <T> ActionBuilder<T>::Generic() {
  return ActionBuilderGeneric<T>(
    ActionBuilderBase<T>::GetItem());
}

template<typename T>
ActionBuilderFanUnit <T> ActionBuilder<T>::FanUnit() {
  return ActionBuilderFanUnit(
    ActionBuilderBase<T>::GetItem());
}

template<typename T>
ActionBuilderSerializer <T> ActionBuilder<T>::Serializer() {
  return ActionBuilderSerializer<T>(
    ActionBuilderBase<T>::GetItem());
}

template<typename T>
ActionBuilderMiscellaneous <T> ActionBuilder<T>::Miscellaneous() {
  return ActionBuilderMiscellaneous(
    ActionBuilderBase<T>::GetItem());
}

}  // namespace SystemModules::Framework::Builder::Actions
