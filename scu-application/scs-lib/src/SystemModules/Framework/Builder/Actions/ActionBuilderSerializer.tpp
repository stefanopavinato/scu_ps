/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Actions/ActionSerializerStatus.h"

namespace SystemModules::Framework::Builder::Actions {

template<class T>
ActionBuilderSerializer<T>::ActionBuilderSerializer(
  Framework::Components::ActionItem<T> *item)
  : ActionBuilderBase<T>(item) {
}

template<class T>
void ActionBuilderSerializer<T>::Status(
  Framework::Components::ModuleHandler const *moduleHandler) {
  ActionBuilderBase<T>::SetAction(
    std::make_unique<Framework::Actions::ActionSerializerStatus>(
      moduleHandler));
}

}  // namespace SystemModules::Framework::Builder::Actions
