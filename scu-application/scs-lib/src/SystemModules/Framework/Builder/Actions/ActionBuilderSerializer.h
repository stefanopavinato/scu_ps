/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "ActionBuilderBase.h"

namespace SystemModules::Framework::Builder::Actions {
template<class T>
class ActionBuilderSerializer : public ActionBuilderBase<T> {
 public:
  explicit ActionBuilderSerializer(
    Framework::Components::ActionItem<T> *item);

  void Status(
    Framework::Components::ModuleHandler const *moduleHandler);
};
}  // namespace SystemModules::Framework::Builder::Actions

#include "ActionBuilderSerializer.tpp"
