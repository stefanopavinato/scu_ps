/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Types/VariantValue.h"

namespace SystemModules::Framework::MonitoringFunctions {

template<class T>
const char MFIsValueLower<T>::kComponentType[] = "MFIsValueLower";

template<class T>
const char MFIsValueLower<T>::kDescription[] = "Value low";

template<class T>
MFIsValueLower<T>::MFIsValueLower(
  const Types::AlertSeverity::Enum &alertSeverity,
  const Types::MonitoringFunctionId::Enum &id,
  const Types::ValueGroup::Enum &group,
  const std::chrono::milliseconds &onDelay,
  const std::chrono::milliseconds &offDelay,
  const T &compareValue)
  : MFTemplate<T>(
  kComponentType,
  alertSeverity,
  id,
  kDescription,
  group,
  onDelay,
  offDelay), compareValue_(compareValue) {
}

template<class T>
std::string MFIsValueLower<T>::GetCompareValues() const {
  auto val = Types::TestVariantValue();
  val.template SetValue(compareValue_);
  return std::string("value < ").append(val.ToString());
}

template<class T>
bool MFIsValueLower<T>::CompareValue(const T &value) {
  return (value < compareValue_);
}
}  // namespace SystemModules::Framework::MonitoringFunctions
