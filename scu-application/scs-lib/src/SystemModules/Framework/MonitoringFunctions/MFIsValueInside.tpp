/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Types/VariantValue.h"

namespace SystemModules::Framework::MonitoringFunctions {

template<class T>
const char MFIsValueInside<T>::kComponentType[] = "MFIsValueInside";

template<class T>
const char MFIsValueInside<T>::kDescription[] = "Value inside";

template<class T>
MFIsValueInside<T>::MFIsValueInside(
  const Types::AlertSeverity::Enum &alertSeverity,
  const Types::MonitoringFunctionId::Enum &id,
  const Types::ValueGroup::Enum &group,
  const std::chrono::milliseconds &onDelay,
  const std::chrono::milliseconds &offDelay,
  const T &lowerLimit,
  const T &upperLimit)
  : MFTemplate<T>(
  kComponentType,
  alertSeverity,
  id,
  kDescription,
  group,
  onDelay,
  offDelay), lowerLimit_(lowerLimit), upperLimit_(upperLimit) {
}


template<class T>
std::string MFIsValueInside<T>::GetCompareValues() const {
  auto lowerLimit = Types::TestVariantValue();
  lowerLimit.template SetValue(lowerLimit_);
  auto upperLimit = Types::TestVariantValue();
  upperLimit.template SetValue(upperLimit_);
  return std::string("value > ").append(lowerLimit.ToString()).append(" && value < ").append(upperLimit.ToString());
}

template<class T>
bool MFIsValueInside<T>::CompareValue(const T &value) {
  return ((value > lowerLimit_) && (value < upperLimit_));
}
}  // namespace SystemModules::Framework::MonitoringFunctions
