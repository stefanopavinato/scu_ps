/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <string>
#include <chrono>
#include "Messages/Visitor/IVisitor.h"
#include "Types/AlertSeverity.h"
#include "Types/MonitoringFunctionId.h"
#include "Types/MonitoringFunctionStatus.h"
#include "Types/ValueGroup.h"
#include "Types/ComponentStatus.h"

namespace SystemModules::Framework::MonitoringFunctions {
class MFBase : public Messages::Visitor::IVisitor {
 public:
  MFBase(
    const std::string &componentType,
    const Types::AlertSeverity::Enum &alertSeverity,
    const Types::MonitoringFunctionId::Enum &id,
    const std::string &description,
    const Types::ValueGroup::Enum &group,
    const std::chrono::milliseconds &onDelay,
    const std::chrono::milliseconds &offDelay);

  ~MFBase() override = default;

  void Accept(Messages::Visitor::VisitorBase *visitor) override;

  [[nodiscard]] std::string GetComponentType() const;

  [[nodiscard]] virtual bool IsActive() const = 0;

  [[nodiscard]] Types::MonitoringFunctionStatus Status() const;

  virtual void RequestReset() = 0;

  [[nodiscard]] std::chrono::milliseconds GetOnDelay() const;

  void SetOnDelay(const std::chrono::milliseconds &onDelay);

  [[nodiscard]] std::chrono::milliseconds GetOffDelay() const;

  void SetOffDelay(const std::chrono::milliseconds &offDelay);

  [[nodiscard]] std::chrono::system_clock::time_point GetOnEventTime() const;

  [[nodiscard]] std::chrono::system_clock::time_point GetOffEventTime() const;

  [[nodiscard]] virtual std::string GetCompareValues() const = 0;

 protected:
  [[nodiscard]] std::chrono::steady_clock::time_point GetOnEvent() const;

  void SetOnEvent(const std::chrono::steady_clock::time_point &onEvent);

  [[nodiscard]] std::chrono::steady_clock::time_point GetOffEvent() const;

  void SetOffEvent(const std::chrono::steady_clock::time_point &offEvent);

  void SetOnEventTime(const std::chrono::system_clock::time_point &eventTime);

  void SetOffEventTime(const std::chrono::system_clock::time_point &eventTime);

 private:
  Types::MonitoringFunctionStatus status_;

  std::chrono::system_clock::time_point onEventTime_;

  std::chrono::system_clock::time_point offEventTime_;

  std::chrono::steady_clock::time_point onEvent_;

  std::chrono::steady_clock::time_point offEvent_;

  std::chrono::milliseconds onDelay_ms_;

  std::chrono::milliseconds offDelay_ms_;

  const std::string componentType_;
};
}  // namespace SystemModules::Framework::MonitoringFunctions
