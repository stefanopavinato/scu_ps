/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Messages/Visitor/VisitorBase.h"
#include "MFBase.h"

namespace SystemModules::Framework::MonitoringFunctions {
MFBase::MFBase(
  const std::string &componentType,
  const Types::AlertSeverity::Enum &alertSeverity,
  const Types::MonitoringFunctionId::Enum &id,
  const std::string &description,
  const Types::ValueGroup::Enum &group,
  const std::chrono::milliseconds &onDelay,
  const std::chrono::milliseconds &offDelay)
  : status_(
  alertSeverity,
  id,
  group,
  description), onEventTime_(std::chrono::system_clock::time_point::min()),
    offEventTime_(std::chrono::system_clock::time_point::min()),
    onEvent_(std::chrono::steady_clock::time_point::min()), offEvent_(std::chrono::steady_clock::time_point::min()),
    onDelay_ms_(onDelay), offDelay_ms_(offDelay), componentType_(componentType) {
}

void MFBase::Accept(Messages::Visitor::VisitorBase *visitor) {
  visitor->Visit(this);
}

std::string MFBase::GetComponentType() const {
  return componentType_;
}

Types::MonitoringFunctionStatus MFBase::Status() const {
  return status_;
}

std::chrono::milliseconds MFBase::GetOnDelay() const {
  return onDelay_ms_;
}

void MFBase::SetOnDelay(const std::chrono::milliseconds &onDelay) {
  onDelay_ms_ = onDelay;
}

std::chrono::milliseconds MFBase::GetOffDelay() const {
  return offDelay_ms_;
}

void MFBase::SetOffDelay(const std::chrono::milliseconds &offDelay) {
  offDelay_ms_ = offDelay;
}

std::chrono::system_clock::time_point MFBase::GetOnEventTime() const {
  return onEventTime_;
}

void MFBase::SetOnEventTime(const std::chrono::system_clock::time_point &eventTime) {
  onEventTime_ = eventTime;
}

std::chrono::system_clock::time_point MFBase::GetOffEventTime() const {
  return offEventTime_;
}

void MFBase::SetOffEventTime(const std::chrono::system_clock::time_point &eventTime) {
  offEventTime_ = eventTime;
}

std::chrono::steady_clock::time_point MFBase::GetOnEvent() const {
  return onEvent_;
}

void MFBase::SetOnEvent(const std::chrono::steady_clock::time_point &onEvent) {
  onEvent_ = onEvent;
}

std::chrono::steady_clock::time_point MFBase::GetOffEvent() const {
  return offEvent_;
}

void MFBase::SetOffEvent(const std::chrono::steady_clock::time_point &offEvent) {
  offEvent_ = offEvent;
}

}  // namespace SystemModules::Framework::MonitoringFunctions
