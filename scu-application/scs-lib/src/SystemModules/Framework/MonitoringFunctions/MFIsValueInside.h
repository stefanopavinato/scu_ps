/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "MFTemplate.h"

namespace SystemModules::Framework::MonitoringFunctions {
template<class T>
class MFIsValueInside : public MFTemplate<T> {
 public:
  MFIsValueInside(
    const Types::AlertSeverity::Enum &alertSeverity,
    const Types::MonitoringFunctionId::Enum &id,
    const Types::ValueGroup::Enum &group,
    const std::chrono::milliseconds &onDelay,
    const std::chrono::milliseconds &offDelay,
    const T &lowerLimit,
    const T &upperLimit);

  ~MFIsValueInside() override = default;

  [[nodiscard]] std::string GetCompareValues() const override;

 protected:
  bool CompareValue(const T &value) override;

 private:
  const T lowerLimit_;

  const T upperLimit_;

  static const char kComponentType[];

  static const char kDescription[];
};
}  // namespace SystemModules::Framework::MonitoringFunctions

#include "MFIsValueInside.tpp"
