/************************************************************
* @Description  :
* @Author       : ZHAW-IAMP-SKS, Philippe Hindermann
* @Copyright    :
************************************************************/

#include "Messages/Visitor/VisitorBase.h"
#include "ActionBase.h"

namespace SystemModules {
namespace Framework {
namespace Actions {
ActionBase::ActionBase(const std::string &description)
  : description_(description) {
}

void ActionBase::Accept(
  Messages::Visitor::VisitorBase *visitor) {
  visitor->Visit(this);
}

std::string ActionBase::GetDescription() const {
  return description_;
}

}  // namespace Actions
}  // namespace Framework
}  // namespace SystemModules
