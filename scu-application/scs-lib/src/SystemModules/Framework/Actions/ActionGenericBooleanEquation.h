/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <memory>
#include <list>
#include <unordered_map>
#include "Types/AlertSeverity.h"
#include "Types/ComponentStatus.h"
#include "Utils/Parser/ParserNode.h"
#include <SystemModules/Framework/Components/ItemIn.h>
#include "SystemModules/Framework/Actions/ActionTemplate.h"

namespace SystemModules::Framework::Actions {

class ActionGenericBooleanEquation : public ActionTemplate<bool> {
 public:
  ActionGenericBooleanEquation(
    const std::string &equation,
    const Types::AlertSeverity::Enum &maximumAlertSeverity);

  ~ActionGenericBooleanEquation() override = default;

  void AddVariable(
    const std::string &name,
    Types::ComponentStatus const *status,
    bool const *variable);

  void AddVariable(
    const std::string &name,
    SystemModules::Framework::Components::ItemIn<bool> *itemIn);

  void Setup();

  bool Update(
    const std::chrono::steady_clock::time_point &now,
    bool *valueOut) override;

 private:
  const std::string equation_;

  const Types::AlertSeverity::Enum maximumAlertSeverity_;

  std::list<Types::ComponentStatus const *> status_;

  std::unordered_map<std::string, bool const *> variables_;

  std::unique_ptr<Utils::Parser::ParserNode> nodes_;

  bool isSetup_;

  static const char kDescription[];
};
}  // namespace SystemModules::Framework::Actions
