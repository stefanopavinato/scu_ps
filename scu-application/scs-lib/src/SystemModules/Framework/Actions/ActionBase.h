/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <string>
#include "Messages/Visitor/IVisitor.h"

namespace SystemModules::Framework::Actions {
class ActionBase : public Messages::Visitor::IVisitor {
 public:
  explicit ActionBase(const std::string &description);

  ~ActionBase() override = default;

  void Accept(Messages::Visitor::VisitorBase *visitor) final;

  [[nodiscard]] std::string GetDescription() const;

 private:
  const std::string description_;
};
}  // namespace SystemModules::Framework::Actions
