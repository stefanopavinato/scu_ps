/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

namespace SystemModules::Framework::Actions {

template <typename T>
const char ActionIncrement<T>::kDescription[] = "Increment";

template <typename T>
ActionIncrement<T>::ActionIncrement(
  T const *valueIn,
  const T &step)
  : ActionTemplate<T>(kDescription)
  , valueIn_(valueIn)
  , step_(step) {
}

template <typename T>
bool ActionIncrement<T>::Update(
  const std::chrono::steady_clock::time_point &now,
  T *valueOut) {
  // Check pointers
  if (valueIn_ == nullptr || valueOut == nullptr) {
    return false;
  }
  // Increment value
  *valueOut = *valueIn_ + step_;
  return true;
}

}  // namespace SystemModules::Framework::Actions
