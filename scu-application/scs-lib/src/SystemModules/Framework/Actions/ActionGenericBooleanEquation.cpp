/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <Utils/Parser/TokensParser.h>
#include <Utils/Parser/BooleanEquationParser.h>
#include "ActionGenericBooleanEquation.h"

namespace SystemModules::Framework::Actions {

const char ActionGenericBooleanEquation::kDescription[] = "Generic Action";

ActionGenericBooleanEquation::ActionGenericBooleanEquation(
  const std::string &equation,
  const Types::AlertSeverity::Enum &maximumAlertSeverity)
  : ActionTemplate<bool>(
  kDescription), equation_(equation), maximumAlertSeverity_(maximumAlertSeverity), isSetup_(false) {
}

void ActionGenericBooleanEquation::AddVariable(
  const std::string &name,
  Types::ComponentStatus const *status,
  bool const *variable) {
  // Check item for nullptr
  if (!variable || !status) {
    return;
  }
  // Check if item is already in map
  if (variables_.find(name) != variables_.end()) {
    return;
  }
  // Add status
  status_.push_back(status);
  // Add variable
  variables_.emplace(name, variable);
}

void ActionGenericBooleanEquation::AddVariable(
  const std::string &name,
  SystemModules::Framework::Components::ItemIn<bool> *itemIn) {
  AddVariable(name, nullptr, itemIn->GetValue());
}

void ActionGenericBooleanEquation::Setup() {
  // Parse string
  auto tokens = Utils::Parser::TokensParser::Parse(equation_);
  // Parse boolean equation
  auto retVal = Utils::Parser::BooleanEquationParser::Parse(*tokens.get(), variables_);
  // Check if parsing was successful
  if (!retVal) {
    return;
  }
  // Set nodes
  nodes_ = std::move(retVal.value());
  isSetup_ = true;
}

bool ActionGenericBooleanEquation::Update(
  const std::chrono::steady_clock::time_point &now,
  bool *valueOut) {
  (void) now;
  if (!isSetup_) {
    return false;
  }
  // Validate status of added items
  for (const auto &status : status_) {
    if (status->AlertSeverity().GetEnum() >= maximumAlertSeverity_) {
      return false;
    }
  }
  // Update action value
  *valueOut = nodes_->Evaluate();
  return true;
}

}  // namespace SystemModules::Framework::Actions
