/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "ActionTemplate.h"

namespace SystemModules::Framework::Actions {
template<class T>
class ActionGetValue : public ActionTemplate<T> {
 public:
  explicit ActionGetValue(
    T const *valueIn);

  ~ActionGetValue() override = default;

  bool Update(
    const std::chrono::steady_clock::time_point &now,
    T *valueOut) override;

 private:
  T const *const valueIn_;

  static const char kDescription[];
};
}  // namespace SystemModules::Framework::Actions

#include "ActionGetValue.tpp"
