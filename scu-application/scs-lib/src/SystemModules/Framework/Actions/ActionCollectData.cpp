/************************************************************
* @Description  :
* @Author       : ZHAW-IAMP-SKS, Philippe Hindermann
* @Copyright    :
************************************************************/

#include "ActionCollectData.h"

namespace SystemModules::Framework::Actions {

const char ActionCollectData::kDescription[] = "CollectData";

ActionCollectData::ActionCollectData()
  : ActionTemplate<std::unordered_map<std::string, std::string>>(
  kDescription) {
}

void ActionCollectData::AddData(
  const std::string &name) {
  (void) name;
}

void ActionCollectData::Setup() {
}

bool ActionCollectData::Update(
  const std::chrono::steady_clock::time_point &now,
  std::unordered_map<std::string, std::string> *valueOut) {
  (void) now;
  (void) valueOut;
  return false;
}

}  // namespace SystemModules::Framework::Actions
