/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

namespace SystemModules::Framework::Actions {
template<class T>
ActionTemplate<T>::ActionTemplate(const std::string &description)
  : ActionBase(description) {
}

}  // namespace SystemModules::Framework::Actions
