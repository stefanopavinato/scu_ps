/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <list>
#include "SystemModules/Framework/Components/ItemBase.h"
#include "Types/ComponentStatus.h"
#include "SystemModules/Framework/Actions/ActionTemplate.h"

namespace SystemModules::Framework::Actions {

class ActionGenericAlertSeverityEqualOrHigher : public ActionTemplate<bool> {
 public:
  explicit ActionGenericAlertSeverityEqualOrHigher(
    const Types::AlertSeverity::Enum &maximumAlertSeverity);

  ~ActionGenericAlertSeverityEqualOrHigher() override = default;

  void AddStatus(
    SystemModules::Framework::Components::Component const *component);

  bool Update(
    const std::chrono::steady_clock::time_point &now,
    bool *valueOut) override;

 private:
  Types::AlertSeverity maximumAlertSeverity_;

  std::list<SystemModules::Framework::Components::Component const *> components_;

  bool isSetup_;

  static const char kDescription[];
};

}  // namespace SystemModules::Framework::Actions
