/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <unordered_map>
#include "ActionTemplate.h"

namespace SystemModules::Framework::Actions {

class ActionCollectData : public ActionTemplate<std::unordered_map<std::string, std::string>> {
 public:
  ActionCollectData();

  ~ActionCollectData() override = default;

  void AddData(
    const std::string &name);

  void Setup();

  bool Update(
    const std::chrono::steady_clock::time_point &now,
    std::unordered_map<std::string, std::string> *valueOut) override;

 private:
  static const char kDescription[];
};
}  // namespace SystemModules::Framework::Actions
