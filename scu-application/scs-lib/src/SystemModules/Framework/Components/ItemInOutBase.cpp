/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Messages/Visitor/VisitorBase.h"
#include "ItemInOutBase.h"

namespace SystemModules::Framework::Components {

ItemInOutBase::ItemInOutBase(
  Logger::ILogger *logger,
  const std::string &name,
  const std::string &description,
  const std::string &componentType,
  const Types::ValueType::Enum &valueType,
  const Types::ValueGroup::Enum &group,
  const std::chrono::milliseconds &updateInterval,
  const Types::Affiliation::Enum &affiliation,
  const Types::WriteStrategy::Enum &writeStrategy,
  const bool &checkWrittenValue,
  const bool &debug)
  : ItemBase(
  logger,
  name,
  description,
  componentType,
  valueType,
  group,
  updateInterval,
  affiliation,
  debug)
  , isValueInForced_(false)
  , isValueOutForced_(false)
  , forceStatusIn_(Types::ForceStatus::Enum::NONE)
  , forceStatusOut_(Types::ForceStatus::Enum::NONE)
  , writeStrategy_(writeStrategy)
  , checkWrittenValue_(checkWrittenValue) {
}

void ItemInOutBase::Accept(Messages::Visitor::VisitorBase *visitor) {
  visitor->Visit(this);
}

Types::ForceStatus ItemInOutBase::GetForceStatusIn() const {
  std::lock_guard<std::mutex> lock{mutex_};
  return forceStatusIn_;
}

Types::ForceStatus ItemInOutBase::GetForceStatusOut() const {
  std::lock_guard<std::mutex> lock{mutex_};
  return forceStatusOut_;
}

bool ItemInOutBase::IsValueInForced() const {
  return isValueInForced_;
}

bool ItemInOutBase::IsValueOutForced() const {
  return isValueOutForced_;
}

void ItemInOutBase::SetForceStatusIn(
  const Types::ForceStatus::Enum &forceStatus) {
  std::lock_guard<std::mutex> lock{mutex_};
  forceStatusIn_.SetValue(forceStatus);
}

void ItemInOutBase::SetForceStatusOut(
  const Types::ForceStatus::Enum &forceStatus) {
  std::lock_guard<std::mutex> lock{mutex_};
  forceStatusOut_.SetValue(forceStatus);
}

Types::WriteStrategy ItemInOutBase::WriteStrategy() const {
  return writeStrategy_;
}

bool ItemInOutBase::CheckWrittenValue() const {
  return checkWrittenValue_;
}

}  // namespace SystemModules::Framework::Components
