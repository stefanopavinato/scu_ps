/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Types/VariantValue.h"
#include "ParameterItemBase.h"

namespace SystemModules::Framework::Components {
class ParameterItemOutBase : public ParameterItemBase {
 public:
  ParameterItemOutBase(
    Logger::ILogger *logger,
    const std::string &name,
    const std::string &description,
    const std::string &componentType);

  ~ParameterItemOutBase() override = default;

  void Accept(Messages::Visitor::VisitorBase *visitor) final;

  [[nodiscard]] virtual Types::TestVariantValue GetVariantValue() const = 0;

  [[nodiscard]] virtual Accessors::AccessorBase *GetAccessor() const = 0;
};

}  // namespace SystemModules::Framework::Components
