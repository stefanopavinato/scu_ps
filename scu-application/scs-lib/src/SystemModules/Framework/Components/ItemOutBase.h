/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <atomic>
#include <memory>
#include <list>
#include "Types/ForceStatus.h"
#include "Types/WriteStrategy.h"
#include "ItemBase.h"

namespace SystemModules::Framework {
namespace Actions {
class ActionBase;
}  // namespace Actions
namespace Components {
class ItemOutBase : public ItemBase {
 public:
  ItemOutBase(
    Logger::ILogger *logger,
    const std::string &name,
    const std::string &description,
    const std::string &componentType,
    const Types::ValueType::Enum &valueType,
    const std::chrono::milliseconds &updateInterval,
    const Types::Affiliation::Enum &affiliation,
    const Types::WriteStrategy::Enum &writeStrategy,
    const bool &checkWrittenValue,
    const bool &debug);

  ~ItemOutBase() override = default;

  void Accept(Messages::Visitor::VisitorBase *visitor) final;

  [[nodiscard]] virtual Types::TestVariantValue GetVariantValueWrite() const = 0;

  [[nodiscard]] virtual Types::TestVariantValue GetVariantValue() const = 0;

  virtual bool ForceValue(
    const Types::VariantValue &value,
    const Types::ForceStatus::Enum &forceType) = 0;

  virtual bool ReleaseValueForced() = 0;

  [[nodiscard]] Types::ForceStatus GetForceStatus() const;

  [[nodiscard]] bool IsValueOutForced() const;

  [[nodiscard]] virtual Accessors::AccessorBase *GetAccessor() const = 0;

  [[nodiscard]] virtual Actions::ActionBase *GetAction() const = 0;

 protected:
  void SetForcedStatus(
    const Types::ForceStatus::Enum &forceStatus);

  [[nodiscard]] Types::WriteStrategy WriteStrategy() const;

  [[nodiscard]] bool CheckWrittenValue() const;

  std::atomic<bool> isValueOutForced_;

 private:
  Types::ForceStatus forceStatus_;

  Types::WriteStrategy writeStrategy_;

  const bool checkWrittenValue_;
};
}  // namespace Components
}  // namespace SystemModules::Framework
