/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Builder/Accessors/ReadAccessorBuilder.h"

namespace SystemModules::Framework::Components {

template<typename VALUE>
const char ParameterItemIn<VALUE>::kComponentType[] = "ParameterItemIn";

template<typename VALUE>
ParameterItemIn<VALUE>::ParameterItemIn(
  Logger::ILogger *logger,
  const std::string &name,
  const std::string &description)
  :ParameterItemInBase(
  logger,
  name,
  description,
  kComponentType), AccessorItem<VALUE>(
  logger) {
}

template<typename VALUE>
Types::TestVariantValue ParameterItemIn<VALUE>::GetVariantValue() const {
  auto retVal = Types::TestVariantValue();
  retVal.SetValue(value_);
  return retVal;
}

template<typename VALUE>
Accessors::AccessorBase *ParameterItemIn<VALUE>::GetAccessor() const {
  return AccessorItem<VALUE>::Accessor();
}

template<typename VALUE>
VALUE const *ParameterItemIn<VALUE>::GetValue() const {
  return &value_;
}

template<typename VALUE>
Builder::Accessors::ReadAccessorBuilder<VALUE> ParameterItemIn<VALUE>::CreateAccessor() {
  return Builder::Accessors::ReadAccessorBuilder<VALUE>(this);
}

template<typename VALUE>
bool ParameterItemIn<VALUE>::Initialize(WorkloadBalancing::WorkloadBalanceHandler *itemHandler) {
  // Check if accessor is set
  if (!AccessorItem<VALUE>::Accessor()) {
    SetAlertSeverity(Types::AlertSeverity::Enum::ERROR,
                     std::string(GetPath()).append(" Accessor not set"));
    return false;
  }
  // Initialize
  if (!AccessorItem<VALUE>::Accessor()->Initialize()) {
    SetAlertSeverity(Types::AlertSeverity::Enum::WARNING,
                     std::string(GetPath()).append(" Accessor initialization failed"));
    return false;
  }
  // Read value
  if (!AccessorItem<VALUE>::Accessor()->Read(&value_)) {
    SetAlertSeverity(Types::AlertSeverity::Enum::WARNING,
                     std::string(GetPath()).append(": Item read failure"));
    return false;
  }
  Logger()->Debug(std::string("Done: ").append(GetPath()));
  return true;
}

template<typename VALUE>
bool ParameterItemIn<VALUE>::DeInitialize(WorkloadBalancing::WorkloadBalanceHandler *itemHandler) {
  // Check if accessor is set
  if (!AccessorItem<VALUE>::Accessor()) {
    SetAlertSeverity(Types::AlertSeverity::Enum::WARNING,
                     std::string(GetPath()).append(" Accessor not set"));
    return false;
  }
  // De-Initialize
  if (!AccessorItem<VALUE>::Accessor()->DeInitialize()) {
    SetAlertSeverity(Types::AlertSeverity::Enum::WARNING,
                     std::string(GetPath()).append(" Accessor de-initialization failed"));
    return false;
  }
  return true;
}

template<typename VALUE>
Types::AlertSeverity::Enum ParameterItemIn<VALUE>::Update(
  const std::chrono::steady_clock::time_point &timePointNow) {
  (void) timePointNow;
  return Types::AlertSeverity::Enum::OK;
}

}  // namespace SystemModules::Framework::Components

