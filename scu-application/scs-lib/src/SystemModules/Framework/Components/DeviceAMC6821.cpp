/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <iomanip>
#include <iostream>
#include <experimental/filesystem>
#include <utility>
#include "DeviceAMC6821.h"

namespace SystemModules::Framework::Components {

const char DeviceAMC6821::kComponentType[] = "I2CDevice_AMC6821";

const char DeviceAMC6821::kSubPath[] = "hwmon";

DeviceAMC6821::DeviceAMC6821(
  Logger::ILogger *logger,
  const std::string &name,
  const std::string &description,
  const uint16_t &address,
  std::string path)
  : Container(
  logger,
  name,
  description)
  , address_(address)
  , path_(std::move(path)) {
  // Get address string stream
  std::stringstream addressStream;
  addressStream << std::setfill('0') << std::setw(sizeof(address_) * 2) << std::hex << address_;
  // Get device path
  pathDevice_.clear();
  try {
    for (auto &entry : std::experimental::filesystem::directory_iterator(path_)) {
      if (entry.path().filename().string().find(addressStream.str()) != std::string::npos) {
        pathDevice_ = entry.path();
        break;
      }
    }
  } catch (...) {
    Logger()->Error(std::string("Directory access failure: ").append(path_));
    throw std::invalid_argument(std::string("Directory access failure: ").append(path_));
  }
  if (pathDevice_.empty()) {
    Logger()->Error(std::string("Device path not found: ").append(path_));
    throw std::invalid_argument(std::string("Device path not found: ").append(path_));
  }
  // Construct amc6821 path
  try {
    for (auto &entry : std::experimental::filesystem::directory_iterator(
      pathDevice_.append("/").append(kSubPath))) {
      if (entry.path().filename().string().find(kSubPath) != std::string::npos) {
        pathAMC6821_ = entry.path();
      }
    }
  } catch (...) {
    Logger()->Error(std::string("Directory access failure: ").append(pathDevice_).append("/").append(kSubPath));
    throw std::invalid_argument(std::string("Directory access failure: ")
                                  .append(pathDevice_).append("/").append(kSubPath));
  }
  if (pathAMC6821_.empty()) {
    Logger()->Error(std::string("Path not found: ").append(pathDevice_).append("/").append(kSubPath));
    throw std::invalid_argument(std::string("Path not found: ")
                                  .append(pathDevice_).append("/").append(kSubPath));
  }
}

std::string DeviceAMC6821::Get() const {
  return pathAMC6821_;
}

}  // namespace SystemModules::Framework::Components
