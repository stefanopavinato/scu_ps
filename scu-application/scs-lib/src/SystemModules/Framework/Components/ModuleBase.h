/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <atomic>
#include "Types/ModuleType.h"
#include "Types/ModuleState.h"
#include "SystemModules/Framework/Components/Container.h"

namespace SystemModules::Framework::Components {

class ModuleBase : public Container {
  friend class ModuleHandler;

 public:
  ModuleBase(
    Logger::ILogger *logger,
    const std::string &name,
    const std::string &description,
    const std::string &componentType,
    WorkloadBalancing::WorkloadBalanceHandler *workloadBalanceHandler);

  ~ModuleBase() override = default;

  void Accept(
    Messages::Visitor::VisitorBase *visitor) override;

  [[nodiscard]] virtual Types::ModuleState GetStateId() const = 0;

  void Stop();

  void StopRelease();

  [[nodiscard]] bool GetStop() const;

  void SetShutDown();

  [[nodiscard]] bool GetShutDown() const;

  void Reset();

  void ClearReset();

  [[nodiscard]] bool GetReset() const;

  [[nodiscard]] bool IsRunning() const;

 protected:
  [[nodiscard]] ModuleHandler *GetModuleHandler() const override;

  bool SetModuleHandler(ModuleHandler *moduleHandler);

  [[nodiscard]] Framework::Components::ModuleBase const *GetModule() const override;

  WorkloadBalancing::WorkloadBalanceHandler *workloadBalanceHandler_;

 private:
  ModuleHandler *moduleHandler_;

  std::atomic<bool> stop_;

  std::atomic<bool> shutDown_;

  std::atomic<bool> reset_;
};
}  // namespace SystemModules::Framework::Components
