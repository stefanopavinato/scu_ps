/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "AccessorItem.h"
#include "ParameterItemOutBase.h"

namespace SystemModules::Framework::Components {
template<typename VALUE>
class ParameterItemOut : public ParameterItemOutBase, public AccessorItem<VALUE> {
 public:
  ParameterItemOut(
    Logger::ILogger *logger,
    const std::string &name,
    const std::string &description,
    const bool &checkWrittenValue,
    const VALUE &value);

  ~ParameterItemOut() override = default;

  [[nodiscard]] Types::TestVariantValue GetVariantValue() const final;

  [[nodiscard]] Accessors::AccessorBase *GetAccessor() const override;

  Builder::Accessors::WriteAccessorBuilder<VALUE> CreateAccessor();

  [[nodiscard]] VALUE const *GetValue() const;

 protected:
  [[nodiscard]] bool Initialize(WorkloadBalancing::WorkloadBalanceHandler *itemHandler) override;

  [[nodiscard]] bool DeInitialize(WorkloadBalancing::WorkloadBalanceHandler *itemHandler) override;

  Types::AlertSeverity::Enum Update(
    const std::chrono::steady_clock::time_point &timePointNow) override;

 private:
  const VALUE value_;

  const bool checkWrittenValue_;

  static const char kComponentType[];
};
}  // namespace SystemModules::Framework::Components

#include "ParameterItemOut.tpp"
