/************************************************************
* @Description  :
* @Author       : ZHAW-IAMP-SKS, Philippe Hindermann
* @Copyright    :
************************************************************/

#include "Component.h"

namespace SystemModules::Framework::Components {

Component::Component(
  Logger::ILogger *logger,
  const std::string &name,
  const std::string &description,
  const std::string &componentType)
  : ToBeNamed(
  logger,
  name,
  description,
  componentType)
  , parent_(nullptr)
  , alertSeverity_(Types::AlertSeverity::Enum::OK) {
}

Component *Component::GetParent() const {
  return parent_;
}

bool Component::SetParent(Component *parent) {
  if (parent == nullptr) {
    return false;
  }
  parent_ = parent;
  return true;
}

void Component::SetAlertSeverity(
  const Types::AlertSeverity::Enum &alertSeverity,
  const std::string &message) {
  // Log message if alert severity is higher than current alert severity
  if (alertSeverity > alertSeverity_ && !message.empty()) {
    switch (alertSeverity) {
      case Types::AlertSeverity::Enum::ERROR: {
        Logger()->Error(message);
        break;
      }
      case Types::AlertSeverity::Enum::WARNING: {
        Logger()->Warning(message);
        break;
      }
      case Types::AlertSeverity::Enum::INFORMATIONAL:
      case Types::AlertSeverity::Enum::NOTICE: {
        Logger()->Info(message);
        break;
      }
      case Types::AlertSeverity::Enum::OK:
      case Types::AlertSeverity::Enum::DEBUG: {
        Logger()->Debug(message);
        break;
      }
    }
  }
  if (alertSeverity_ >= Types::AlertSeverity::Enum::ERROR) {
    return;
  }
  alertSeverity_ = alertSeverity;
}

void Component::ResetAlertSeverity() {
  Logger()->Info(GetPath());
  alertSeverity_ = Types::AlertSeverity::Enum::OK;
}

Types::AlertSeverity::Enum Component::GetAlertSeverity() const {
  return alertSeverity_;
}

Framework::Components::ModuleBase const *Component::GetModule() const {
  if (parent_ == nullptr) {
    return nullptr;
  }
  return parent_->GetModule();
}

ModuleHandler const *Component::GetModuleHandler() const {
  if (parent_ == nullptr) {
    return nullptr;
  }
  return parent_->GetModuleHandler();
}

std::string Component::GetPath() const {
  if (parent_ != nullptr) {
    return parent_->GetPath().append("/").append(GetName());
  }
  return GetName();
}

}  // namespace SystemModules::Framework::Components
