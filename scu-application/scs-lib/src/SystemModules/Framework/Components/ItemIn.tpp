/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "WorkloadBalancing/WorkloadBalanceHandler.h"
#include "Messages/Visitor/VisitorPropertyGet.h"
#include "SystemModules/Framework/Builder/MonitoringFunctions/MFBuilder.h"

namespace SystemModules::Framework::Components {

template<typename T>
struct is_vector : public std::false_type {
};

template<typename T, typename A>
struct is_vector<std::vector<T, A>> : public std::true_type {
};

template<typename VALUE>
const char ItemIn<VALUE>::kComponentType[] = "ItemIn";

template<typename VALUE>
ItemIn<VALUE>::ItemIn(
  Logger::ILogger *logger,
  const std::string &name,
  const std::string &description,
  const Types::ValueGroup::Enum &group,
  const std::chrono::milliseconds &updateInterval,
  const Types::Affiliation::Enum &affiliation,
  const size_t &size,
  const bool &debug)
  : ItemInBase(
  logger,
  name,
  description,
  kComponentType,
  Types::ValueType::GetEnum<VALUE>(),
  group,
  updateInterval,
  affiliation,
  debug)
  , AccessorItem<VALUE>(logger)
  , MonitoringFunctionItem<VALUE>(logger)
  , size_(size) {
  if constexpr (is_vector<VALUE>::value) {
    valueInRead_.resize(size_);
    valueInForced_.resize(size_);
    valueInOld_.resize(size_);
  }
}

template<typename VALUE>
ItemIn<VALUE>::ItemIn(
  Logger::ILogger *logger,
  const std::string &name,
  const std::string &description,
  const Types::ValueGroup::Enum &group,
  const std::chrono::milliseconds &updateInterval,
  const Types::Affiliation::Enum &affiliation,
  const VALUE &initValue,
  const size_t &size,
  const bool &debug)
  : ItemInBase(
  logger,
  name,
  description,
  kComponentType,
  Types::ValueType::GetEnum<VALUE>(),
  group,
  updateInterval,
  affiliation,
  debug)
  , AccessorItem<VALUE>(logger)
  , MonitoringFunctionItem<VALUE>(logger)
  , valueInRead_(initValue)
  , valueInForced_(initValue)
  , valueIn_(initValue)
  , size_(size) {
  if constexpr (is_vector<VALUE>::value) {
    valueInRead_.resize(size_);
    valueInForced_.resize(size_);
    valueInOld_.resize(size_);
  }
}

template<typename VALUE>
Types::TestVariantValue ItemIn<VALUE>::GetVariantValueRead() const {
  // ToDo: clean handling of mutex
//  std::lock_guard<std::mutex> lock{mutex_};
  auto retVal = Types::TestVariantValue();
  retVal.SetValue(valueInRead_);
  return retVal;
}

template<typename VALUE>
Types::TestVariantValue ItemIn<VALUE>::GetVariantValue() const {
  // ToDo: clean handling of mutex
//  std::lock_guard<std::mutex> lock{mutex_};
  auto retVal = Types::TestVariantValue();
  retVal.SetValue(valueIn_);
  return retVal;
}

template<typename VALUE>
bool ItemIn<VALUE>::ForceValue(
  const Types::VariantValue &value,
  const Types::ForceStatus::Enum &forceType) {
  std::lock_guard<std::mutex> lock{mutex_};
  auto success = std::visit([this](const auto &arg) -> bool {
    using K = std::decay_t<decltype(arg)>;
    // Set forced value if types match
    if constexpr (std::is_same_v<K, VALUE>) {
      valueInForced_ = arg;
      return true;
    }
    // Else disable forced value
    valueInForced_ = {};
    return false;
  }, value);
  if (success) {
    SetForcedStatus(forceType);
    Logger()->Info(std::string(GetPath()).append(": Forced value"));
    return true;
  } else {
    this->SetForcedStatus(Types::ForceStatus::Enum::NONE);
    return false;
  }
}

template<typename VALUE>
bool ItemIn<VALUE>::ReleaseValueForced() {
  std::lock_guard<std::mutex> lock{mutex_};
  SetForcedStatus(Types::ForceStatus::Enum::NONE);
  valueInForced_ = {};
  Logger()->Info(std::string(GetPath()).append(": Released forced value."));
  return true;
}

template<typename VALUE>
Accessors::AccessorBase *ItemIn<VALUE>::GetAccessor() const {
  return AccessorItem<VALUE>::Accessor();
}

template<typename VALUE>
Builder::MonitoringFunctions::MFBuilder<VALUE> ItemIn<VALUE>::CreateMonitoringFunction() {
  return Builder::MonitoringFunctions::MFBuilder<VALUE>(this);
}

template<typename VALUE>
std::list<MonitoringFunctions::MFBase *> ItemIn<VALUE>::GetMonitoringFunctions() const {
  return MonitoringFunctionItem<VALUE>::MonitoringFunctionsBase();
}


template<typename VALUE>
Types::MonitoringFunctionStatus const *ItemIn<VALUE>::GetMonitoringFunctionStatus() const {
  std::lock_guard<std::mutex> lock{mutex_};
  return &monitoringFunctionStatus_;
}

template<typename VALUE>
void ItemIn<VALUE>::ResetMonitoringFunctions() {
  std::lock_guard<std::mutex> lock{mutex_};
  for (auto &monitoringFunction : *MonitoringFunctionItem<VALUE>::MonitoringFunctions()) {
    monitoringFunction->RequestReset();
  }
}

template<typename VALUE>
Builder::Accessors::ReadAccessorBuilder<VALUE> ItemIn<VALUE>::CreateAccessor() {
  return Builder::Accessors::ReadAccessorBuilder<VALUE>(this);
}

template<typename VALUE>
bool ItemIn<VALUE>::Initialize(WorkloadBalancing::WorkloadBalanceHandler *itemHandler) {
  // Check if accessor is set
  if (!AccessorItem<VALUE>::Accessor()) {
    SetAlertSeverity(Types::AlertSeverity::Enum::ERROR,
                     std::string(GetPath()).append(" Accessor not set"));
    return false;
  }
  // Initialize
  if (!AccessorItem<VALUE>::Accessor()->Initialize()) {
    SetAlertSeverity(Types::AlertSeverity::Enum::WARNING,
                     std::string(GetPath()).append(" Accessor initialization failed"));
    return false;
  }
  // Add item to handler
  if (!itemHandler->AddItem(this)) {
    SetAlertSeverity(Types::AlertSeverity::Enum::WARNING,
                     std::string(GetPath()).append(" Failed adding item to handler"));
    return false;
  }
  Logger()->Debug(std::string("Done: ").append(GetPath()));
  return true;
}

template<typename VALUE>
bool ItemIn<VALUE>::DeInitialize(WorkloadBalancing::WorkloadBalanceHandler *itemHandler) {
  // Remove item from handler
  (void) itemHandler->RemoveItem(this);
  // Check if accessor is set
  if (!AccessorItem<VALUE>::Accessor()) {
    SetAlertSeverity(Types::AlertSeverity::Enum::WARNING,
                     std::string(GetPath()).append(" Accessor not set"));
    return false;
  }
  // De-Initialize
  if (!AccessorItem<VALUE>::Accessor()->DeInitialize()) {
    SetAlertSeverity(Types::AlertSeverity::Enum::WARNING,
                     std::string(GetPath()).append(" Accessor de-initialization failed"));
    return false;
  }
  Logger()->Debug(std::string("Done: ").append(GetPath()));
  return true;
}

template<typename VALUE>
Types::AlertSeverity::Enum ItemIn<VALUE>::Update(
  const std::chrono::steady_clock::time_point &timePointNow) {
  std::lock_guard<std::mutex> lock{mutex_};
  // Check if value needs to be updated
  if (!TimerHasElapsed(timePointNow)) {
    return GetAlertSeverity();
  }
  // Reset timer
  ResetTimer(timePointNow);
  // Clear validity
  SetIsValid(false);
  // Store value in
  valueInOld_ = valueIn_;
  // Check if read accessor is set
  if (AccessorItem<VALUE>::Accessor()) {
    // Read value
    if (!AccessorItem<VALUE>::Accessor()->Read(&valueInRead_) &&
      GetForcedStatus().GetValue() == Types::ForceStatus::Enum::NONE) {  // Set status only if value is not forced
      SetAlertSeverity(Types::AlertSeverity::Enum::WARNING,
                       std::string(GetPath()).append(": Item read failure"));
      return GetAlertSeverity();
    }
  } else {
    if (GetForcedStatus().GetValue() == Types::ForceStatus::Enum::NONE) {
      // Set status only if value is not forced
      SetAlertSeverity(Types::AlertSeverity::Enum::ERROR,
                       std::string(GetPath()).append(": Accessor not set"));
      return GetAlertSeverity();
    }
  }
  // Set item value
  if (GetForcedStatus().GetValue() != Types::ForceStatus::Enum::NONE) {
    valueIn_ = valueInForced_;
    isValueInForced_ = true;
    if (GetForcedStatus().GetValue() == Types::ForceStatus::Enum::ONCE) {
      SetForcedStatus(Types::ForceStatus::Enum::NONE);
    }
  } else {
    valueIn_ = valueInRead_;
    isValueInForced_ = false;
  }
  // Log debug info
  if (GetDebugEnable() && (valueIn_ != valueInOld_)) {
    Logger()->Info(GetPath().append(":ValueIn = ").append(GetVariantValue().ToString()));
  }
  // Update active monitoring function
  auto parentAlertSeverity = Types::AlertSeverity(Types::AlertSeverity::Enum::OK);
  auto tempMonitoringFunctionStatus = Types::MonitoringFunctionStatus();
  for (auto &monitoringFunction : *MonitoringFunctionItem<VALUE>::MonitoringFunctions()) {
    // ToDo: use external status for update of monitoring function
    monitoringFunction->Update(
      valueIn_,
      parentAlertSeverity,
      timePointNow);
    if (monitoringFunction->IsActive() &&
        monitoringFunction->Status().AlertSeverity() > tempMonitoringFunctionStatus.AlertSeverity()) {
      tempMonitoringFunctionStatus = monitoringFunction->Status();
    }
  }
  monitoringFunctionStatus_ = tempMonitoringFunctionStatus;
  // Set validity
  SetIsValid(true);
  // Resets alert severity if no error
  SetAlertSeverity(monitoringFunctionStatus_.AlertSeverity().GetEnum(),
                   GetPath().append(": ").append(monitoringFunctionStatus_.Id().ToString()));
  return GetAlertSeverity();
}

template<typename VALUE>
VALUE const *ItemIn<VALUE>::GetValue() const {
  return &valueIn_;
}

template<typename VALUE>
Types::AlertSeverity ItemIn<VALUE>::GetGroupAlertSeverity(
  const Types::ValueGroup::Enum &group) const {
  std::lock_guard<std::mutex> lock{mutex_};
  if (group == Types::ValueGroup::Enum::ANY) {
    return Types::AlertSeverity(GetAlertSeverity());
  }
  if (GetGroup().GetValue() == group) {
    return Types::AlertSeverity(GetAlertSeverity());
  }
  return Types::AlertSeverity(Types::AlertSeverity::Enum::OK);
  /*
  for (const auto &monitoringFunction : GetMonitoringFunctions()) {
    if (monitoringFunction->Status().Group().GetValue() == group) {
         auto tempAlertSeverity = monitoringFunction->Status().AlertSeverity();
      if (alertSeverity < tempAlertSeverity) {
        alertSeverity = tempAlertSeverity;
      }
    }
  }
  return alertSeverity;
  */
}

}  // namespace SystemModules::Framework::Components
