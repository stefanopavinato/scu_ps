/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Container.h"

namespace SystemModules::Framework::Components {
class DeviceAMC6821 : public Container {
 public:
  DeviceAMC6821(
    Logger::ILogger *logger,
    const std::string &name,
    const std::string &description,
    const uint16_t &address,
    std::string path);

  ~DeviceAMC6821() override = default;

  [[nodiscard]] std::string Get() const;

 private:
  const uint16_t address_;

  const std::string path_;

  std::string pathDevice_;

  std::string pathAMC6821_;

  static const char kComponentType[];

  static const char kSubPath[];
};
}  // namespace SystemModules::Framework::Components
