/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Builder/Accessors/WriteAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Actions/ActionBuilder.h"

namespace SystemModules::Framework::Components {

template<typename VALUE>
const char ItemOut<VALUE>::kComponentType[] = "ItemOut";

template<typename VALUE>
ItemOut<VALUE>::ItemOut(
  Logger::ILogger *logger,
  const std::string &name,
  const std::string &description,
  const std::chrono::milliseconds &updateInterval,
  const Types::Affiliation::Enum &affiliation,
  const Types::WriteStrategy::Enum &writeStrategy,
  const bool &checkWrittenValue,
  const VALUE &initValue,
  const bool &debug)
  : ItemOutBase(
  logger,
  name,
  description,
  kComponentType,
  Types::ValueType::GetEnum<VALUE>(),
  updateInterval,
  affiliation,
  writeStrategy,
  checkWrittenValue,
  debug), AccessorItem<VALUE>(
  logger), ActionItem<VALUE>(
  logger)
  , valueOutForced_(initValue)
  , valueOut_(initValue)
  , valueOutWrite_(initValue)
  , valueWrittenOnce_(false) {
}

template<typename VALUE>
Types::TestVariantValue ItemOut<VALUE>::GetVariantValueWrite() const {
  // ToDo: clean handling of mutex
//  std::lock_guard<std::mutex> lock{mutex_};
  auto retVal = Types::TestVariantValue();
  retVal.SetValue(valueOutWrite_);
  return retVal;
}

template<typename VALUE>
Types::TestVariantValue ItemOut<VALUE>::GetVariantValue() const {
  // ToDo: clean handling of mutex
//  std::lock_guard<std::mutex> lock{mutex_};
  auto retVal = Types::TestVariantValue();
  retVal.SetValue(valueOut_);
  return retVal;
}

template<typename VALUE>
bool ItemOut<VALUE>::ForceValue(
  const Types::VariantValue &value,
  const Types::ForceStatus::Enum &forceType) {
  std::lock_guard<std::mutex> lock{mutex_};
  auto success = std::visit([this](const auto &arg) -> bool {
    using K = std::decay_t<decltype(arg)>;
    // Set forced value if types match
    if constexpr (std::is_same_v<K, VALUE>) {
      valueOutForced_ = arg;
      return true;
    }
    // Else disable forced value
    valueOutForced_ = {};
    return false;
  }, value);
  if (success) {
    SetForcedStatus(forceType);
    Logger()->Info(std::string(GetPath()).append(": Forced value"));
    return true;
  } else {
    this->SetForcedStatus(Types::ForceStatus::Enum::NONE);
    return false;
  }
}

template<typename VALUE>
bool ItemOut<VALUE>::ReleaseValueForced() {
  std::lock_guard<std::mutex> lock{mutex_};
  SetForcedStatus(Types::ForceStatus::Enum::NONE);
  valueOutForced_ = {};
  Logger()->Info(std::string(GetPath()));
  return true;
}


template<typename VALUE>
Builder::Accessors::WriteAccessorBuilder<VALUE> ItemOut<VALUE>::CreateAccessor() {
  return Builder::Accessors::WriteAccessorBuilder<VALUE>(this);
}

template<typename VALUE>
Builder::Actions::ActionBuilder<VALUE> ItemOut<VALUE>::CreateAction() {
  return Builder::Actions::ActionBuilder<VALUE>(this);
}

template<typename VALUE>
Accessors::AccessorBase *ItemOut<VALUE>::GetAccessor() const {
  return AccessorItem<VALUE>::Accessor();
}

template<typename VALUE>
Actions::ActionBase *ItemOut<VALUE>::GetAction() const {
  return ActionItem<VALUE>::Action();
}

template<typename VALUE>
bool ItemOut<VALUE>::Initialize(WorkloadBalancing::WorkloadBalanceHandler *itemHandler) {
  // Check if accessor is set
  if (!AccessorItem<VALUE>::Accessor()) {
    SetAlertSeverity(Types::AlertSeverity::Enum::ERROR,
                     std::string(GetPath()).append(" Accessor not set"));
    return false;
  }
  // Initialize
  if (!AccessorItem<VALUE>::Accessor()->Initialize()) {
    SetAlertSeverity(Types::AlertSeverity::Enum::WARNING,
                     std::string(GetPath()).append(" Accessor initialization failed"));
    return false;
  }
  // Add item to handler
  if (!itemHandler->AddItem(this)) {
    SetAlertSeverity(Types::AlertSeverity::Enum::WARNING,
                     std::string(GetPath()).append(" Failed adding item to handler"));
    return false;
  }
  Logger()->Debug(std::string("Done: ").append(GetPath()));
  return true;
}

template<typename VALUE>
bool ItemOut<VALUE>::DeInitialize(WorkloadBalancing::WorkloadBalanceHandler *itemHandler) {
  // Remove item from handler
  (void) itemHandler->RemoveItem(this);
  // Check if accessor is set
  if (!AccessorItem<VALUE>::Accessor()) {
    SetAlertSeverity(Types::AlertSeverity::Enum::WARNING,
                     std::string(GetPath()).append(" Accessor not set"));
    return false;
  }
  // De-Initialize
  if (!AccessorItem<VALUE>::Accessor()->DeInitialize()) {
    SetAlertSeverity(Types::AlertSeverity::Enum::WARNING,
                     std::string(GetPath()).append(" Accessor de-initialization failed"));
    return false;
  }
  Logger()->Debug(std::string("Done: ").append(GetPath()));
  return true;
}

template<typename VALUE>
Types::AlertSeverity::Enum ItemOut<VALUE>::Update(
  const std::chrono::steady_clock::time_point &timePointNow) {
  std::lock_guard<std::mutex> lock{mutex_};
  // Check if value needs to be updated
  if (!TimerHasElapsed(timePointNow)) {
    return GetAlertSeverity();
  }
  // Reset timer
  ResetTimer(timePointNow);
  // Clear validity
  SetIsValid(false);
  // store value out
  valueOutWriteOld_ = valueOutWrite_;
  // Check if action is set
  if (ActionItem<VALUE>::Action()) {
    if (!ActionItem<VALUE>::Action()->Update(timePointNow, &valueOut_) &&
      GetForceStatus().GetValue() == Types::ForceStatus::Enum::NONE) { // Set status only if value is not forced
      SetAlertSeverity(Types::AlertSeverity::Enum::WARNING,
                       std::string(GetPath()).append(": Action failure"));
      return GetAlertSeverity();
    }
  } else {
    if (GetForceStatus().GetValue() == Types::ForceStatus::Enum::NONE) {
      // Set status only if value is not forced
      SetAlertSeverity(Types::AlertSeverity::Enum::ERROR,
                       std::string(GetPath()).append(": Action not set"));
    }
  }
  // Set item value
  if (GetForceStatus().GetValue() != Types::ForceStatus::Enum::NONE) {
    valueOutWrite_ = valueOutForced_;
    isValueOutForced_ = true;
  } else {
    valueOutWrite_ = valueOut_;
    isValueOutForced_ = false;
  }
  // Check if value changed
  if (valueOutWriteOld_ != valueOutWrite_) {
    valueWrittenOnce_ = false;
  }
  // Check write strategy
  if (WriteStrategy().GetEnum() == Types::WriteStrategy::Enum::ON_CHANGE &&
      valueWrittenOnce_) {
    // Set validity
    SetIsValid(true);
    // Resets alert severity if no error
    SetAlertSeverity(Types::AlertSeverity::Enum::OK,
                     GetPath().append(": Done"));
    // Reset force status if on status once
    if (GetForceStatus().GetValue() == Types::ForceStatus::Enum::ONCE) {
      SetForcedStatus(Types::ForceStatus::Enum::NONE);
    }
    return GetAlertSeverity();
  }
  // Log debug info
  if (GetDebugEnable()) {
    Logger()->Info(GetPath().append(":ValueOutWrite = ").append(GetVariantValueWrite().ToString()));
  }
  // Check if write accessor is set
  if (!AccessorItem<VALUE>::Accessor()) {
    SetAlertSeverity(Types::AlertSeverity::Enum::ERROR,
                     std::string(GetPath()).append(": Accessor not set"));
    return GetAlertSeverity();
  }
  // Write value
  if (!AccessorItem<VALUE>::Accessor()->Write(valueOutWrite_)) {
    SetAlertSeverity(Types::AlertSeverity::Enum::WARNING,
                     std::string(GetPath()).append(": Item write failure"));
    return GetAlertSeverity();
  }
  // Check if read back is active
  if (!CheckWrittenValue()) {
    valueWrittenOnce_ = true;
    // Set validity
    SetIsValid(true);
    // Resets alert severity if no error
    SetAlertSeverity(Types::AlertSeverity::Enum::OK,
                     GetPath().append(": Done"));
    // Reset force status if on status once
    if (GetForceStatus().GetValue() == Types::ForceStatus::Enum::ONCE) {
      SetForcedStatus(Types::ForceStatus::Enum::NONE);
    }
    return GetAlertSeverity();
  }
  // Read value
  VALUE valueIn;
  if (!AccessorItem<VALUE>::Accessor()->Read(&valueIn)) {
    SetAlertSeverity(Types::AlertSeverity::Enum::WARNING,
                     std::string(GetPath()).append(": Item read failure"));
    return GetAlertSeverity();
  }
  // Compare valueOut and valueIn
  if (valueOutWrite_ != valueIn) {
    SetAlertSeverity(Types::AlertSeverity::Enum::WARNING,
                     std::string(GetPath()).append(": Item read back compare failure"));
    return GetAlertSeverity();
  }
  valueWrittenOnce_ = true;
  // Set validity
  SetIsValid(true);
  // Resets alert severity if no error
  SetAlertSeverity(Types::AlertSeverity::Enum::OK,
                   GetPath().append(": Done"));
  // Reset force status if on status once
  if (GetForceStatus().GetValue() == Types::ForceStatus::Enum::ONCE) {
    SetForcedStatus(Types::ForceStatus::Enum::NONE);
  }
  return GetAlertSeverity();
}

template<typename VALUE>
VALUE const *ItemOut<VALUE>::GetValue() const {
  return &valueOutWrite_;
}

template<typename VALUE>
Types::AlertSeverity ItemOut<VALUE>::GetGroupAlertSeverity(
  const Types::ValueGroup::Enum &group) const {
  std::lock_guard<std::mutex> lock{mutex_};
  if (group == Types::ValueGroup::Enum::ANY) {
    return Types::AlertSeverity(GetAlertSeverity());
  }
  return Types::AlertSeverity(Types::AlertSeverity::Enum::OK);
}


}  // namespace SystemModules::Framework::Components
