/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Messages/Visitor/VisitorBase.h"
#include "ModuleBase.h"

namespace SystemModules::Framework::Components {

ModuleBase::ModuleBase(
  Logger::ILogger *logger,
  const std::string &name,
  const std::string &description,
  const std::string &componentType,
  WorkloadBalancing::WorkloadBalanceHandler *workloadBalanceHandler)
  : SystemModules::Framework::Components::Container(
  logger,
  name,
  description,
  componentType)
  , moduleHandler_(nullptr)
  , workloadBalanceHandler_(workloadBalanceHandler)
  , stop_(false)
  , shutDown_(false)
  , reset_(false) {
}

void ModuleBase::Accept(Messages::Visitor::VisitorBase *visitor) {
  visitor->Visit(this);
}

void ModuleBase::Stop() {
  Logger()->Info(GetPath());
  stop_ = true;
}

void ModuleBase::StopRelease() {
  Logger()->Info(GetPath());
  stop_ = false;
}

bool ModuleBase::GetStop() const {
  return stop_;
}

void ModuleBase::SetShutDown() {
  shutDown_ = true;
}

bool ModuleBase::GetShutDown() const {
  return shutDown_;
}

void ModuleBase::Reset() {
  Logger()->Info(GetPath());
  reset_ = true;
}

bool ModuleBase::GetReset() const {
  return reset_;
}

void ModuleBase::ClearReset() {
  Logger()->Info(GetPath());
  reset_ = false;
}

bool ModuleBase::IsRunning() const {
  return GetStateId().GetEnum() != Types::ModuleState::Enum::DE_INITIALIZE;
}

Framework::Components::ModuleBase const *ModuleBase::GetModule() const {
  return this;
}

bool ModuleBase::SetModuleHandler(ModuleHandler *moduleHandler) {
  if (moduleHandler == nullptr) {
    return false;
  }
  this->moduleHandler_ = moduleHandler;
  return true;
}

ModuleHandler *ModuleBase::GetModuleHandler() const {
  return moduleHandler_;
}

}  // namespace SystemModules::Framework::Components
