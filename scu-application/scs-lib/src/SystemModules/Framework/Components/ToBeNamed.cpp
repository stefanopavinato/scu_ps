/************************************************************
* @Description  :
* @Author       : ZHAW-IAMP-SKS, Philippe Hindermann
* @Copyright    :
************************************************************/

#include "ToBeNamed.h"

namespace SystemModules::Framework::Components {

ToBeNamed::ToBeNamed(
  Logger::ILogger *logger,
  std::string name,
  std::string description,
  std::string componentType)
  : logger_(logger)
  , componentType_(std::move(componentType))
  , name_(std::move(name))
  , description_(std::move(description)) {
}

Logger::ILogger *ToBeNamed::Logger() const {
  return logger_;
}

std::string ToBeNamed::GetComponentType() const {
  return componentType_;
}

std::string ToBeNamed::GetName() const {
  return name_;
}

std::string ToBeNamed::GetDescription() const {
  return description_;
}

}  // namespace SystemModules::Framework::Components
