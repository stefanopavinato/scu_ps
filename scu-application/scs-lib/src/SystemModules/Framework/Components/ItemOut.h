/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "AccessorItem.h"
#include "ActionItem.h"
#include "ItemOutBase.h"

namespace SystemModules::Framework::Components {
template<typename VALUE>
class ItemOut : public ItemOutBase, public AccessorItem<VALUE>, public ActionItem<VALUE> {
 public:
  ItemOut(
    Logger::ILogger *logger,
    const std::string &name,
    const std::string &description,
    const std::chrono::milliseconds &updateInterval,
    const Types::Affiliation::Enum &affiliation,
    const Types::WriteStrategy::Enum &writeStrategy,
    const bool &checkWrittenValue,
    const VALUE &initValue,
    const bool &debug = false);

  ~ItemOut() override = default;

  [[nodiscard]] Types::AlertSeverity GetGroupAlertSeverity(
    const Types::ValueGroup::Enum &group) const override;

  [[nodiscard]] Types::TestVariantValue GetVariantValueWrite() const final;

  [[nodiscard]] Types::TestVariantValue GetVariantValue() const final;

  bool ForceValue(
    const Types::VariantValue &value,
    const Types::ForceStatus::Enum &forceType) override;

  bool ReleaseValueForced() override;

  [[nodiscard]] VALUE const *GetValue() const;

  // Accessors
  Builder::Accessors::WriteAccessorBuilder<VALUE> CreateAccessor();

  [[nodiscard]] Accessors::AccessorBase *GetAccessor() const override;

  // Actions
  Builder::Actions::ActionBuilder<VALUE> CreateAction();

  [[nodiscard]] Actions::ActionBase *GetAction() const override;

 protected:
  [[nodiscard]] bool Initialize(WorkloadBalancing::WorkloadBalanceHandler *itemHandler) override;

  [[nodiscard]] bool DeInitialize(WorkloadBalancing::WorkloadBalanceHandler *itemHandler) override;

  [[nodiscard]] Types::AlertSeverity::Enum Update(
    const std::chrono::steady_clock::time_point &timePointNow) override;

 private:
  VALUE valueOutWrite_;

  VALUE valueOutWriteOld_ {};

  VALUE valueOutForced_ {};

  VALUE valueOut_ {};

  bool valueWrittenOnce_;

  mutable std::mutex mutex_;

  static const char kComponentType[];
};

}  // namespace SystemModules::Framework::Components

#include "ItemOut.tpp"
