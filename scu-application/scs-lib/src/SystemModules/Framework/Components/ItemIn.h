/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Messages/Visitor/Types/AttributeType.h"
#include "AccessorItem.h"
#include "MonitoringFunctionItem.h"
#include "ItemInBase.h"

namespace SystemModules::Framework::Components {
template<typename VALUE>
class ItemIn :
  public ItemInBase,
  public AccessorItem<VALUE>,
  public MonitoringFunctionItem<VALUE> {
  friend class Builder::MonitoringFunctions::MFBuilder<VALUE>;

 public:
  ItemIn(
    Logger::ILogger *logger,
    const std::string &name,
    const std::string &description,
    const Types::ValueGroup::Enum &group,
    const std::chrono::milliseconds &updateInterval,
    const Types::Affiliation::Enum &affiliation,
    const size_t &size = 1,
    const bool &debug = false);

  ItemIn(
    Logger::ILogger *logger,
    const std::string &name,
    const std::string &description,
    const Types::ValueGroup::Enum &group,
    const std::chrono::milliseconds &updateInterval,
    const Types::Affiliation::Enum &affiliation,
    const VALUE &initValue,
    const size_t &size = 1,
    const bool &debug = false);

  ~ItemIn() override = default;

  [[nodiscard]] Types::AlertSeverity GetGroupAlertSeverity(
    const Types::ValueGroup::Enum &group) const override;

  [[nodiscard]] Types::TestVariantValue GetVariantValueRead() const final;

  [[nodiscard]] Types::TestVariantValue GetVariantValue() const final;

  bool ForceValue(
    const Types::VariantValue &value,
    const Types::ForceStatus::Enum &forceType) override;

  bool ReleaseValueForced() override;

  [[nodiscard]] VALUE const *GetValue() const;

  // Accessors
  Builder::Accessors::ReadAccessorBuilder<VALUE> CreateAccessor();

  [[nodiscard]] Accessors::AccessorBase *GetAccessor() const override;

  // Monitoring functions
  Builder::MonitoringFunctions::MFBuilder<VALUE> CreateMonitoringFunction();

  [[nodiscard]] std::list<MonitoringFunctions::MFBase *> GetMonitoringFunctions() const override;

  [[nodiscard]] Types::MonitoringFunctionStatus const *GetMonitoringFunctionStatus() const override;

  void ResetMonitoringFunctions() override;

 protected:
  [[nodiscard]] bool Initialize(WorkloadBalancing::WorkloadBalanceHandler *itemHandler) override;

  [[nodiscard]] bool DeInitialize(WorkloadBalancing::WorkloadBalanceHandler *itemHandler) override;

  [[nodiscard]] Types::AlertSeverity::Enum Update(
    const std::chrono::steady_clock::time_point &timePointNow) override;

 private:
  VALUE valueInRead_ {};

  VALUE valueInForced_ {};

  VALUE valueIn_ {};

  VALUE valueInOld_ {};

  size_t size_ {};

  Types::MonitoringFunctionStatus monitoringFunctionStatus_;

  mutable std::mutex mutex_;

  static const char kComponentType[];
};

}  // namespace SystemModules::Framework::Components

#include "ItemIn.tpp"
