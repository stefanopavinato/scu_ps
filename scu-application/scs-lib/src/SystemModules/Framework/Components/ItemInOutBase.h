/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <atomic>
#include <memory>
#include <list>
#include "Types/ForceStatus.h"
#include "Types/WriteStrategy.h"
#include "ItemBase.h"

namespace SystemModules::Framework {
namespace Actions {
class ActionBase;
}  // namespace Actions
namespace Components {
class ItemInOutBase : public ItemBase {
 public:
  ItemInOutBase(
    Logger::ILogger *logger,
    const std::string &name,
    const std::string &description,
    const std::string &componentType,
    const Types::ValueType::Enum &valueType,
    const Types::ValueGroup::Enum &group,
    const std::chrono::milliseconds &updateInterval,
    const Types::Affiliation::Enum &affiliation,
    const Types::WriteStrategy::Enum &writeStrategy,
    const bool &checkWrittenValue,
    const bool &debug);

  ~ItemInOutBase() override = default;

  void Accept(Messages::Visitor::VisitorBase *visitor) final;

  [[nodiscard]] virtual Types::TestVariantValue GetVariantValueInRead() const = 0;

  [[nodiscard]] virtual Types::TestVariantValue GetVariantValueIn() const = 0;

  [[nodiscard]] virtual Types::TestVariantValue GetVariantValueOutWrite() const = 0;

  [[nodiscard]] virtual Types::TestVariantValue GetVariantValueOut() const = 0;

  virtual bool ForceValueIn(
    const Types::VariantValue &value,
    const Types::ForceStatus::Enum &forceType) = 0;

  virtual bool ForceValueOut(
    const Types::VariantValue &value,
    const Types::ForceStatus::Enum &forceType) = 0;

  virtual bool ReleaseValueInForced() = 0;

  virtual bool ReleaseValueOutForced() = 0;

  [[nodiscard]] Types::ForceStatus GetForceStatusIn() const;

  [[nodiscard]] Types::ForceStatus GetForceStatusOut() const;

  [[nodiscard]] bool IsValueInForced() const;

  [[nodiscard]] bool IsValueOutForced() const;

  [[nodiscard]] virtual Accessors::AccessorBase *GetAccessor() const = 0;

  [[nodiscard]] virtual Actions::ActionBase *GetAction() const = 0;

  [[nodiscard]] virtual std::list<MonitoringFunctions::MFBase *> GetMonitoringFunctions() const = 0;

  [[nodiscard]] virtual Types::MonitoringFunctionStatus const *GetMonitoringFunctionStatus() const = 0;

 protected:
  void SetForceStatusIn(
    const Types::ForceStatus::Enum &forceStatus);

  void SetForceStatusOut(
    const Types::ForceStatus::Enum &forceStatus);

  [[nodiscard]] Types::WriteStrategy WriteStrategy() const;

  [[nodiscard]] bool CheckWrittenValue() const;

  std::atomic<bool> isValueInForced_;

  std::atomic<bool> isValueOutForced_;

 private:
  Types::ForceStatus forceStatusIn_;

  Types::ForceStatus forceStatusOut_;

  Types::WriteStrategy writeStrategy_;

  const bool checkWrittenValue_;

  mutable std::mutex mutex_;
};
}  // namespace Components
}  // namespace SystemModules::Framework
