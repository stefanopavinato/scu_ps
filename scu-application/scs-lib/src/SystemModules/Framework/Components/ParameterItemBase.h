/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Component.h"

namespace SystemModules::Framework::Components {
class ParameterItemBase : public Component {
 public:
  ParameterItemBase(
    Logger::ILogger *logger,
    const std::string &name,
    const std::string &description,
    const std::string &componentType);

  ~ParameterItemBase() override = default;

  [[nodiscard]] Types::AlertSeverity GetGroupAlertSeverity(
    const Types::ValueGroup::Enum &group) const final;

  Component *GetComponent(
    std::queue<std::string> *path) final;

  void ResetMonitoringFunctions() override;
};

}  // namespace SystemModules::Framework::Components
