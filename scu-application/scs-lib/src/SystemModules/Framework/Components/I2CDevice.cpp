/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <iomanip>
#include <iostream>
#include <fstream>
#include <experimental/filesystem>
#include <utility>
#include "I2CDevice.h"

namespace SystemModules::Framework::Components {

const char I2CDevice::kComponentType[] = "I2CDevice";

const char I2CDevice::kSubPathExport[] = "/new_device";
const char I2CDevice::kSubPathUnExport[] = "/delete_device";

I2CDevice::I2CDevice(
  Logger::ILogger *logger,
  const std::string &name,
  const std::string &description,
  const std::string &driver,
  const uint16_t &address,
  const std::string &path)
  : I2CDevice(
  logger,
  name,
  description,
  kComponentType,
  driver,
  address,
  path) {
}

I2CDevice::I2CDevice(
  Logger::ILogger *logger,
  const std::string &name,
  const std::string &description,
  const std::string &componentType,
  std::string driver,
  const uint16_t &address,
  std::string path)
  : Container(
  logger,
  name,
  description,
  componentType)
  , driver_(std::move(driver))
  , address_(address)
  , path_(std::move(path)) {
  // Un-export device
  auto fileUnExport = std::ofstream(std::string(path_).append(kSubPathUnExport));
  if (fileUnExport.is_open()) {
    fileUnExport << address_ << std::ends;
    fileUnExport.close();
  }
  // Export device
  auto fileExport = std::ofstream(std::string(path_).append(kSubPathExport));
  if (!fileExport.is_open()) {
    Logger()->Error(std::string("Export path not found: ").append(path_).append(kSubPathExport));
    throw std::invalid_argument(
      std::string("Export path not found: ").append(path_).append(kSubPathExport));
  }
  fileExport << driver_ << " " << address_ << std::ends;
  fileExport.close();
  // Get address string stream
  std::stringstream addressStream;
  addressStream << std::setfill('0') << std::setw(sizeof(address_) * 2) << std::hex << address_;
  // Get device path
  pathDevice_.clear();
  try {
    for (auto &entry : std::experimental::filesystem::directory_iterator(path_)) {
      if (entry.path().filename().string().find(addressStream.str()) != std::string::npos) {
        pathDevice_ = entry.path();
        break;
      }
    }
  } catch (...) {
    Logger()->Error(std::string("Directory access failure: ").append(path_));
    throw std::invalid_argument(std::string("Directory access failure: ").append(path_));
  }
  if (pathDevice_.empty()) {
    Logger()->Error(std::string("Device path not found: ").append(path_));
    throw std::invalid_argument(std::string("Device path not found: ").append(path_));
  }
  Logger()->Info(std::string("Initialized ").append(GetName())
                 .append(" (driver = ").append(driver_)
                 .append(", address = ").append(std::to_string(address_))
                 .append(", path = ").append(pathDevice_)
                 .append(")"));
}


I2CDevice::~I2CDevice() {
  // Un-export device
  auto fileUnExport = std::ofstream(std::string(path_).append(kSubPathUnExport));
  if (fileUnExport.is_open()) {
    fileUnExport << address_ << std::ends;
    fileUnExport.close();
    Logger()->Info(std::string("De-initialized ").append(GetName())
                   .append(" (driver = ").append(driver_)
                   .append(", address = ").append(std::to_string(address_))
                   .append(", path = ").append(pathDevice_)
                   .append(")"));
  }
}

std::string I2CDevice::GetDeviceLocationPath() const {
  return pathDevice_;
}

}  // namespace SystemModules::Framework::Components
