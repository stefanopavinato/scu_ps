/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <experimental/filesystem>
#include "I2CDeviceLM75.h"

namespace SystemModules::Framework::Components {

const char I2CDeviceLM75::kComponentType[] = "I2CDevice_LM75";

const char I2CDeviceLM75::kDriverName[] = "lm75";

const char I2CDeviceLM75::kSubPath[] = "hwmon";

I2CDeviceLM75::I2CDeviceLM75(
  Logger::ILogger *logger,
  const std::string &name,
  const std::string &description,
  const uint16_t &address,
  const std::string &path)
  : I2CDevice(
  logger,
  name,
  description,
  kComponentType,
  kDriverName,
  address,
  path) {
  // Construct device path
  try {
    for (auto &entry : std::experimental::filesystem::directory_iterator(
      GetDeviceLocationPath().append("/").append(kSubPath))) {
      if (entry.path().filename().string().find(kSubPath) != std::string::npos) {
        pathLM75_ = entry.path();
      }
    }
  } catch (...) {
    Logger()->Error(std::string("Path not found: ").append(GetDeviceLocationPath()));
    throw std::invalid_argument(std::string("Path not found: ").append(GetDeviceLocationPath()));
  }
  if (pathLM75_.empty()) {
    Logger()->Error(std::string("Path not found: ").append(GetDeviceLocationPath()));
    throw std::invalid_argument(std::string("Path not found: ").append(GetDeviceLocationPath()));
  }
}

std::string I2CDeviceLM75::GetDevicePath() const {
  return pathLM75_;
}

}  // namespace SystemModules::Framework::Components
