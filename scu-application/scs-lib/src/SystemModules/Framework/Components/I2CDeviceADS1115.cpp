/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <experimental/filesystem>
#include "I2CDeviceADS1115.h"

namespace SystemModules::Framework::Components {

const char I2CDeviceADS1115::kComponentType[] = "I2CDevice_ADS1115";

const char I2CDeviceADS1115::kDriverName[] = "ads1115";

const char I2CDeviceADS1115::kSubPath[] = "iio:device";

I2CDeviceADS1115::I2CDeviceADS1115(
  Logger::ILogger *logger,
  const std::string &name,
  const std::string &description,
  const uint16_t &address,
  const std::string &path)
  : I2CDevice(
  logger,
  name,
  description,
  kComponentType,
  kDriverName,
  address,
  path) {
  // Construct device path
  try {
    for (auto &entry : std::experimental::filesystem::directory_iterator(GetDeviceLocationPath())) {
      if (entry.path().filename().string().find(kSubPath) != std::string::npos) {
        pathADS1115_ = entry.path();
      }
    }
  } catch (...) {
    Logger()->Error(std::string("Path not found: ").append(GetDeviceLocationPath()));
    throw std::invalid_argument(std::string("Path not found: ").append(GetDeviceLocationPath()));
  }
  if (pathADS1115_.empty()) {
    Logger()->Error(std::string("Path not found: ").append(GetDeviceLocationPath()));
    throw std::invalid_argument(std::string("Path not found: ").append(GetDeviceLocationPath()));
  }
}

std::string I2CDeviceADS1115::GetDevicePath() const {
  return pathADS1115_;
}

}  // namespace SystemModules::Framework::Components
