/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <iomanip>
#include <iostream>
#include <experimental/filesystem>
#include "I2CDeviceINA220.h"

namespace SystemModules::Framework::Components {

const char I2CDeviceINA220::kComponentType[] = "I2CDevice_INA220";

const char I2CDeviceINA220::kDriverName[] = "ina220";

const char I2CDeviceINA220::kSubPath[] = "hwmon";

I2CDeviceINA220::I2CDeviceINA220(
  Logger::ILogger *logger,
  const std::string &name,
  const std::string &description,
  const uint32_t &i2cBus,
  const uint16_t &address,
  const std::string &path,
  const std::string &devicePath)
  : I2CDevice(
  logger,
  name,
  description,
  kComponentType,
  kDriverName,
  address,
  path) {
  // Set device path
  std::stringstream addressStream;
  addressStream << devicePath;
  addressStream << std::to_string(i2cBus);
  addressStream << "-";
  addressStream << std::setfill('0') << std::setw(sizeof(address) * 2) << std::hex << address;
  devicePath_ = addressStream.str();
  // Wait
  // sleep(1);
  // Construct device path
  try {
    for (auto &entry : std::experimental::filesystem::directory_iterator(
      devicePath_.append("/").append(kSubPath))) {
      if (entry.path().filename().string().find(kSubPath) != std::string::npos) {
        pathINA220_ = entry.path();
      }
    }
  } catch (...) {
    Logger()->Error(std::string("Path not found: ").append(GetDeviceLocationPath()));
    throw std::invalid_argument(std::string("Path not found: ").append(GetDeviceLocationPath()));
  }
  if (pathINA220_.empty()) {
    Logger()->Error(std::string("Path not found: ").append(GetDeviceLocationPath()));
    throw std::invalid_argument(std::string("Path not found: ").append(GetDeviceLocationPath()));
  }
}

std::string I2CDeviceINA220::GetDevicePath() const {
  return pathINA220_;
}

}  // namespace SystemModules::Framework::Components
