/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <memory>
#include <unordered_map>
#include "Component.h"

namespace SystemModules::Framework {
namespace Builder::Components {
class ComponentBuilder;
}  // namespace Builder::Components
namespace Components {
class Container : public Component {
 public:
  Container(
    Logger::ILogger *logger,
    const std::string &name,
    const std::string &description);

  ~Container() override = default;

  void Accept(Messages::Visitor::VisitorBase *visitor) override;

  [[nodiscard]] Types::AlertSeverity GetGroupAlertSeverity(
    const Types::ValueGroup::Enum &group) const override;

  ToBeNamed *GetComponent(std::queue<std::string> *path) final;

  Builder::Components::ComponentBuilder Create();

  bool AddComponent(std::unique_ptr<Component> component);

  void RemoveComponent(const std::string &name);

  std::unordered_map<std::string, std::unique_ptr<Component>> *GetComponents();

  [[nodiscard]] bool Initialize(WorkloadBalancing::WorkloadBalanceHandler *updateHandler) override;

  [[nodiscard]] bool DeInitialize(WorkloadBalancing::WorkloadBalanceHandler *updateHandler) override;

  [[nodiscard]] Types::AlertSeverity::Enum Update(
    const std::chrono::steady_clock::time_point &timePointNow) override;

  void ResetMonitoringFunctions() override;

 protected:
  Container(
    Logger::ILogger *logger,
    const std::string &name,
    const std::string &description,
    const std::string &componentType);

 private:
  std::unordered_map<std::string, std::unique_ptr<Component>> components_;

  static const char kComponentType[];
};
}  // namespace Components
}  // namespace SystemModules::Framework
