/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Messages/Visitor/VisitorBase.h"
#include "ParameterItemOutBase.h"

namespace SystemModules::Framework::Components {

ParameterItemOutBase::ParameterItemOutBase(
  Logger::ILogger *logger,
  const std::string &name,
  const std::string &description,
  const std::string &componentType)
  : ParameterItemBase(
  logger,
  name,
  description,
  componentType) {
}

void ParameterItemOutBase::Accept(Messages::Visitor::VisitorBase *visitor) {
  visitor->Visit(this);
}

}  // namespace SystemModules::Framework::Components
