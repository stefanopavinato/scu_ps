/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

namespace SystemModules::Framework::Components {

template<class T>
const char Item<T>::kComponentType[] = "Item";

template<class T>
const Types::ValueType::Enum Item<T>::kValueType = Types::ValueType::Enum::NONE;

template<class T>
Item<T>::Item(
  Logger::ILogger *logger,
  const std::string &name,
  const std::string &description,
  const std::string &componentType,
  const Types::ValueGroup::Enum &group,
  const std::chrono::milliseconds &updateInterval,
  const T &initValue)
  : ItemBase(
  logger,
  name,
  description,
  componentType,
  kValueType,
  group,
  updateInterval), value_(initValue) {
}

template<class T>
void Item<T>::Initialize() {
}

template<class T>
void Item<T>::DeInitialize() {
}

template<class T>
void Item<T>::Update(
  const std::chrono::steady_clock::time_point &timePointNow) {
  (void) timePointNow;
}

template<class T>
void Item<T>::ProcessInputs(
  const Types::AlertSeverity &parentAlertSeverity,
  const std::chrono::steady_clock::time_point &timePointNow) {
  (void) parentAlertSeverity;
  (void) timePointNow;
}

template<class T>
void Item<T>::ProcessOutputs(
  const std::chrono::steady_clock::time_point &timePointNow) {
  (void) timePointNow;
}

template<class T>
Types::TestVariantValue Item<T>::GetVariantValue() const {
  auto retVal = Types::TestVariantValue();
  retVal.SetValue(value_);
  return retVal;
}

template<class T>
T Item<T>::GetValue() const {
  return value_;
}

template<class T>
void Item<T>::SetValue(const T &value) {
  // ToDo: clean solution!
  value_ = value;
}

}  // namespace SystemModules::Framework::Components
