/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <memory>
#include "Logger/ILogger.h"
#include "SystemModules/Framework/Accessors/AccessorTemplate.h"

namespace SystemModules::Framework {
namespace Builder::Accessors {
template<typename VALUE>
class ReadAccessorBuilder;
template<typename VALUE>
class WriteAccessorBuilder;
template<typename VALUE>
class ReadWriteAccessorBuilder;
}  // namespace Builder::Accessors
namespace Components {
template<typename VALUE>
class AccessorItem {
  friend class Builder::Accessors::ReadAccessorBuilder<VALUE>;

  friend class Builder::Accessors::WriteAccessorBuilder<VALUE>;

  friend class Builder::Accessors::ReadWriteAccessorBuilder<VALUE>;

 public:
  explicit AccessorItem(
    Logger::ILogger *logger);

  virtual ~AccessorItem() = default;

 protected:
  Framework::Accessors::AccessorTemplate<VALUE> *Accessor() const;

  bool SetAccessor(
    std::unique_ptr<Framework::Accessors::AccessorTemplate<VALUE>> accessorItem);

 private:
  Logger::ILogger *GetLogger();

  Logger::ILogger *logger_;

  std::unique_ptr<Accessors::AccessorTemplate<VALUE>> accessor_;
};
}  // namespace Components
}  // namespace SystemModules::Framework

#include "AccessorItem.tpp"
