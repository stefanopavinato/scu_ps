/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "AccessorItem.h"
#include "MonitoringFunctionItem.h"
#include "ActionItem.h"
#include "ItemInOutBase.h"

namespace SystemModules::Framework::Components {
template<typename VALUE>
class ItemInOut :
  public ItemInOutBase,
  public AccessorItem<VALUE>,
  public MonitoringFunctionItem<VALUE>,
  public ActionItem<VALUE> {
  friend class Builder::MonitoringFunctions::MFBuilder<VALUE>;

 public:
  ItemInOut(
    Logger::ILogger *logger,
    const std::string &name,
    const std::string &description,
    const Types::ValueGroup::Enum &group,
    const std::chrono::milliseconds &updateInterval,
    const Types::Affiliation::Enum &affiliation,
    const Types::WriteStrategy::Enum &writeStrategy,
    const bool &checkWrittenValue,
    const VALUE &initValue,
    const bool &debug = false);

  ~ItemInOut() override = default;

  [[nodiscard]] Types::AlertSeverity GetGroupAlertSeverity(
    const Types::ValueGroup::Enum &group) const override;

  [[nodiscard]] Types::TestVariantValue GetVariantValueInRead() const final;

  [[nodiscard]] Types::TestVariantValue GetVariantValueIn() const final;

  bool ForceValueIn(
    const Types::VariantValue &value,
    const Types::ForceStatus::Enum &forceType) override;

  bool ReleaseValueInForced() override;

  [[nodiscard]] Types::TestVariantValue GetVariantValueOutWrite() const final;

  [[nodiscard]] Types::TestVariantValue GetVariantValueOut() const final;

  bool ForceValueOut(
    const Types::VariantValue &value,
    const Types::ForceStatus::Enum &forceType) override;

  bool ReleaseValueOutForced() override;

  [[nodiscard]] VALUE const *GetValueIn() const;

  [[nodiscard]] VALUE const *GetValueOut() const;

  // Accessors
  Builder::Accessors::ReadWriteAccessorBuilder<VALUE> CreateAccessor();

  [[nodiscard]] Accessors::AccessorBase *GetAccessor() const override;

  // Actions
  Builder::Actions::ActionBuilder<VALUE> CreateAction();

  [[nodiscard]] Actions::ActionBase *GetAction() const override;

  // Monitoring functions
  Builder::MonitoringFunctions::MFBuilder<VALUE> CreateMonitoringFunction();

  [[nodiscard]] std::list<MonitoringFunctions::MFBase *> GetMonitoringFunctions() const override;

  [[nodiscard]] Types::MonitoringFunctionStatus const *GetMonitoringFunctionStatus() const override;

  void ResetMonitoringFunctions() override;

 protected:
  [[nodiscard]] bool Initialize(WorkloadBalancing::WorkloadBalanceHandler *itemHandler) override;

  [[nodiscard]] bool DeInitialize(WorkloadBalancing::WorkloadBalanceHandler *itemHandler) override;

  [[nodiscard]] Types::AlertSeverity::Enum Update(
    const std::chrono::steady_clock::time_point &timePointNow) override;

 private:
  VALUE valueInRead_;

  VALUE valueInForced_;

  VALUE valueIn_;

  VALUE valueInOld_;

  VALUE valueOutWrite_;

  VALUE valueOutWriteOld_;

  VALUE valueOutForced_;

  VALUE valueOut_;

  bool valueWrittenOnce_;

  Types::MonitoringFunctionStatus monitoringFunctionStatus_;

  mutable std::mutex mutex_;

  static const char kComponentType[];
};
}  // namespace SystemModules::Framework::Components

#include "ItemInOut.tpp"
