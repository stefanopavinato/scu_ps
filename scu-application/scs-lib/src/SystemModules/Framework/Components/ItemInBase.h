/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <atomic>
#include <memory>
#include <list>
#include "Types/ForceStatus.h"
#include "Types/MonitoringFunctionStatus.h"
#include "ItemBase.h"

namespace SystemModules::Framework {
namespace MonitoringFunctions {
class MFBase;
}  // namespace MonitoringFunctions
namespace Accessors {
class AccessorBase;
}  // namespace Accessors
namespace Components {
class ItemInBase : public ItemBase {
 public:
  ItemInBase(
    Logger::ILogger *logger,
    const std::string &name,
    const std::string &description,
    const std::string &componentType,
    const Types::ValueType::Enum &valueType,
    const Types::ValueGroup::Enum &group,
    const std::chrono::milliseconds &updateInterval,
    const Types::Affiliation::Enum &affiliation,
    const bool &debug);

  ~ItemInBase() override = default;

  void Accept(Messages::Visitor::VisitorBase *visitor) final;

  [[nodiscard]] virtual Types::TestVariantValue GetVariantValueRead() const = 0;

  [[nodiscard]] virtual Types::TestVariantValue GetVariantValue() const = 0;

  virtual bool ForceValue(
    const Types::VariantValue &value,
    const Types::ForceStatus::Enum &forceType) = 0;

  virtual bool ReleaseValueForced() = 0;

  [[nodiscard]] bool IsValueInForced() const;

  [[nodiscard]] Types::ForceStatus GetForcedStatus() const;

  [[nodiscard]] virtual Accessors::AccessorBase *GetAccessor() const = 0;

  [[nodiscard]] virtual std::list<MonitoringFunctions::MFBase *> GetMonitoringFunctions() const = 0;

  [[nodiscard]] virtual Types::MonitoringFunctionStatus const *GetMonitoringFunctionStatus() const = 0;

 protected:
  void SetForcedStatus(const Types::ForceStatus::Enum &forceStatus);

  std::atomic<bool> isValueInForced_;

 private:
  Types::ForceStatus forceStatus_;

  mutable std::mutex mutex_;
};
}  // namespace Components
}  // namespace SystemModules::Framework
