/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Builder/MonitoringFunctions/MFBuilder.h"

namespace SystemModules::Framework::Components {

template<typename VALUE>
const char ItemInOut<VALUE>::kComponentType[] = "ItemInOut";

template<typename VALUE>
ItemInOut<VALUE>::ItemInOut(
  Logger::ILogger *logger,
  const std::string &name,
  const std::string &description,
  const Types::ValueGroup::Enum &group,
  const std::chrono::milliseconds &updateInterval,
  const Types::Affiliation::Enum &affiliation,
  const Types::WriteStrategy::Enum &writeStrategy,
  const bool &checkWrittenValue,
  const VALUE &initValue,
  const bool &debug)
  : ItemInOutBase(
  logger,
  name,
  description,
  kComponentType,
  Types::ValueType::GetEnum<VALUE>(),
  group,
  updateInterval,
  affiliation,
  writeStrategy,
  checkWrittenValue,
  debug)
  , AccessorItem<VALUE>(logger)
  , MonitoringFunctionItem<VALUE>(logger)
  , ActionItem<VALUE>(logger)
  , valueInRead_(initValue)
  , valueInForced_(initValue)
  , valueIn_(initValue)
  , valueInOld_(initValue)
  , valueOutWrite_(initValue)
  , valueOutWriteOld_(initValue)
  , valueOutForced_(initValue)
  , valueOut_(initValue)
  , valueWrittenOnce_(false) {
}

template<typename VALUE>
Types::TestVariantValue ItemInOut<VALUE>::GetVariantValueInRead() const {
  // ToDo: clean handling of mutex
//  std::lock_guard<std::mutex> lock{mutex_};
  auto retVal = Types::TestVariantValue();
  retVal.SetValue(valueInRead_);
  return retVal;
}

template<typename VALUE>
Types::TestVariantValue ItemInOut<VALUE>::GetVariantValueIn() const {
  // ToDo: clean handling of mutex
//  std::lock_guard<std::mutex> lock{mutex_};
  auto retVal = Types::TestVariantValue();
  retVal.SetValue(valueIn_);
  return retVal;
}

template<typename VALUE>
Types::TestVariantValue ItemInOut<VALUE>::GetVariantValueOutWrite() const {
  // ToDo: clean handling of mutex
//  std::lock_guard<std::mutex> lock{mutex_};
  auto retVal = Types::TestVariantValue();
  retVal.SetValue(valueOutWrite_);
  return retVal;
}

template<typename VALUE>
Types::TestVariantValue ItemInOut<VALUE>::GetVariantValueOut() const {
  // ToDo: clean handling of mutex
//  std::lock_guard<std::mutex> lock{mutex_};
  auto retVal = Types::TestVariantValue();
  retVal.SetValue(valueOut_);
  return retVal;
}


template<typename VALUE>
bool ItemInOut<VALUE>::ForceValueIn(
  const Types::VariantValue &value,
  const Types::ForceStatus::Enum &forceType) {
  std::lock_guard<std::mutex> lock{mutex_};
  auto success = std::visit([this](const auto &arg) -> bool {
    using K = std::decay_t<decltype(arg)>;
    // Set forced value if types match
    if constexpr (std::is_same_v<K, VALUE>) {
      valueInForced_ = arg;
      return true;
    }
    // Else disable forced value
    valueInForced_ = {};
    return false;
  }, value);
  if (success) {
    SetForceStatusIn(forceType);
    Logger()->Info(std::string(GetPath()).append(": Forced value"));
    return true;
  } else {
    this->SetForceStatusIn(Types::ForceStatus::Enum::NONE);
    return false;
  }
}

template<typename VALUE>
bool ItemInOut<VALUE>::ForceValueOut(
  const Types::VariantValue &value,
  const Types::ForceStatus::Enum &forceType) {
  std::lock_guard<std::mutex> lock{mutex_};
  auto success = std::visit([this](const auto &arg) -> bool {
    using K = std::decay_t<decltype(arg)>;
    // Set forced value if types match
    if constexpr (std::is_same_v<K, VALUE>) {
      valueOutForced_ = arg;
      return true;
    }
    // Else disable forced value
    valueOutForced_ = {};
    return false;
  }, value);
  if (success) {
    SetForceStatusOut(forceType);
    Logger()->Info(std::string(GetPath()).append(": Forced value"));
    return true;
  } else {
    this->SetForceStatusOut(Types::ForceStatus::Enum::NONE);
    return false;
  }
}

template<typename VALUE>
bool ItemInOut<VALUE>::ReleaseValueInForced() {
  std::lock_guard<std::mutex> lock{mutex_};
  SetForceStatusIn(Types::ForceStatus::Enum::NONE);
  valueInForced_ = {};
  Logger()->Info(std::string(GetPath()).append(": Released forced value in."));
  return true;
}

template<typename VALUE>
bool ItemInOut<VALUE>::ReleaseValueOutForced() {
  std::lock_guard<std::mutex> lock{mutex_};
  SetForceStatusOut(Types::ForceStatus::Enum::NONE);
  valueOutForced_ = {};
  Logger()->Info(std::string(GetPath()).append(": Released forced value out."));
  return true;
}

template<typename VALUE>
Accessors::AccessorBase *ItemInOut<VALUE>::GetAccessor() const {
  return AccessorItem<VALUE>::Accessor();
}

template<typename VALUE>
Actions::ActionBase *ItemInOut<VALUE>::GetAction() const {
  return ActionItem<VALUE>::Action();
}

template<typename VALUE>
Builder::MonitoringFunctions::MFBuilder <VALUE> ItemInOut<VALUE>::CreateMonitoringFunction() {
  return Builder::MonitoringFunctions::MFBuilder<VALUE>(this);
}

template<typename VALUE>
std::list<MonitoringFunctions::MFBase *> ItemInOut<VALUE>::GetMonitoringFunctions() const {
  return MonitoringFunctionItem<VALUE>::MonitoringFunctionsBase();
}


template<typename VALUE>
Types::MonitoringFunctionStatus const *ItemInOut<VALUE>::GetMonitoringFunctionStatus() const {
  return &monitoringFunctionStatus_;
}

template<typename VALUE>
void ItemInOut<VALUE>::ResetMonitoringFunctions() {
  std::lock_guard<std::mutex> lock{mutex_};
  for (auto &monitoringFunction : *MonitoringFunctionItem<VALUE>::MonitoringFunctions()) {
    monitoringFunction->RequestReset();
  }
}

template<typename VALUE>
Builder::Accessors::ReadWriteAccessorBuilder <VALUE> ItemInOut<VALUE>::CreateAccessor() {
  return Builder::Accessors::ReadWriteAccessorBuilder<VALUE>(this);
}

template<typename VALUE>
Builder::Actions::ActionBuilder <VALUE> ItemInOut<VALUE>::CreateAction() {
  return Builder::Actions::ActionBuilder<VALUE>(this);
}

template<typename VALUE>
bool ItemInOut<VALUE>::Initialize(WorkloadBalancing::WorkloadBalanceHandler *itemHandler) {
  // Check if accessor is set
  if (!AccessorItem<VALUE>::Accessor()) {
    SetAlertSeverity(Types::AlertSeverity::Enum::ERROR,
                     std::string(GetPath()).append(" Accessor not set"));
    return false;
  }
  // Initialize
  if (!AccessorItem<VALUE>::Accessor()->Initialize()) {
    SetAlertSeverity(Types::AlertSeverity::Enum::WARNING,
                     std::string(GetPath()).append(" Accessor initialization failed"));
    return false;
  }
  // Add item to handler
  if (!itemHandler->AddItem(this)) {
    SetAlertSeverity(Types::AlertSeverity::Enum::WARNING,
                     std::string(GetPath()).append(" Failed adding item to handler"));
    return false;
  }
  Logger()->Debug(std::string("Done: ").append(GetPath()));
  return true;
}

template<typename VALUE>
bool ItemInOut<VALUE>::DeInitialize(WorkloadBalancing::WorkloadBalanceHandler *itemHandler) {
  // Remove item from handler
  (void) itemHandler->RemoveItem(this);
  // Check if accessor is set
  if (!AccessorItem<VALUE>::Accessor()) {
    SetAlertSeverity(Types::AlertSeverity::Enum::WARNING,
                     std::string(GetPath()).append(" Accessor not set"));
    return false;
  }
  // De-Initialize
  if (!AccessorItem<VALUE>::Accessor()->DeInitialize()) {
    SetAlertSeverity(Types::AlertSeverity::Enum::WARNING,
                     std::string(GetPath()).append(" Accessor de-initialization failed"));
    return false;
  }
  Logger()->Debug(std::string("Done: ").append(GetPath()));
  return true;
}

template<typename VALUE>
Types::AlertSeverity::Enum ItemInOut<VALUE>::Update(
  const std::chrono::steady_clock::time_point &timePointNow) {
  std::lock_guard<std::mutex> lock{mutex_};
  // Check if value needs to be updated
  if (!TimerHasElapsed(timePointNow)) {
    return GetAlertSeverity();
  }
  // Reset timer
  ResetTimer(timePointNow);
  // Clear validity
  SetIsValid(false);
  // Store value in
  valueInOld_ = valueIn_;
  // Check if read accessor is set
  if (AccessorItem<VALUE>::Accessor()) {
    // Read value
    if (!AccessorItem<VALUE>::Accessor()->Read(&valueInRead_) &&
      GetForceStatusIn().GetValue() == Types::ForceStatus::Enum::NONE) {  // Set status only if value is not forced
      SetAlertSeverity(Types::AlertSeverity::Enum::WARNING,
                       std::string(GetPath()).append(": Item read failure"));
      return GetAlertSeverity();
    }
  } else {
    if (GetForceStatusIn().GetValue() == Types::ForceStatus::Enum::NONE) {
      // Set status only if value is not forced
      SetAlertSeverity(Types::AlertSeverity::Enum::ERROR,
                       std::string(GetPath()).append(": Accessor not set"));
      return GetAlertSeverity();
    }
  }
  // Set item value
  if (GetForceStatusIn().GetValue() != Types::ForceStatus::Enum::NONE) {
    valueIn_ = valueInForced_;
    isValueInForced_ = true;
    if (GetForceStatusIn().GetValue() == Types::ForceStatus::Enum::ONCE) {
      SetForceStatusIn(Types::ForceStatus::Enum::NONE);
    }
  } else {
    valueIn_ = valueInRead_;
    isValueInForced_ = false;
  }
  // Log debug info
  if (GetDebugEnable() && (valueIn_ != valueInOld_)) {
    Logger()->Info(GetPath().append(":ValueIn = ").append(GetVariantValueIn().ToString()));
  }
  // Update active monitoring function
  auto parentAlertSeverity = Types::AlertSeverity(Types::AlertSeverity::Enum::OK);
  auto tempMonitoringFunctionStatus = Types::MonitoringFunctionStatus();
  for (auto &monitoringFunction : *MonitoringFunctionItem<VALUE>::MonitoringFunctions()) {
    // ToDo: use external status for update of monitoring function
    monitoringFunction->Update(
      valueIn_,
      parentAlertSeverity,
      timePointNow);
    if (monitoringFunction->IsActive() &&
        monitoringFunction->Status().AlertSeverity() > tempMonitoringFunctionStatus.AlertSeverity()) {
      tempMonitoringFunctionStatus = monitoringFunction->Status();
    }
  }
  monitoringFunctionStatus_ = tempMonitoringFunctionStatus;
  // Update inner value
  valueOutWriteOld_ = valueOutWrite_;
  // Check if action is set
  if (ActionItem<VALUE>::Action()) {
    if (!ActionItem<VALUE>::Action()->Update(timePointNow, &valueOut_) &&
      GetForceStatusOut().GetValue() == Types::ForceStatus::Enum::NONE) {  // Set status only if value is not forced
      SetAlertSeverity(Types::AlertSeverity::Enum::WARNING,
                       std::string(GetPath()).append(": Action failure"));
      return GetAlertSeverity();
    }
  } else {
    if (GetForceStatusOut().GetValue() == Types::ForceStatus::Enum::NONE) {
      // Set status only if value is not forced
      SetAlertSeverity(Types::AlertSeverity::Enum::ERROR,
                       std::string(GetPath()).append(": Action not set"));
      return GetAlertSeverity();
    }
  }
  // Set item value
  if (GetForceStatusOut().GetValue() != Types::ForceStatus::Enum::NONE) {
    valueOutWrite_ = valueOutForced_;
    isValueOutForced_ = true;
  } else {
    valueOutWrite_ = valueOut_;
    isValueOutForced_ = false;
  }
  // Check if value changed
  if (valueOutWriteOld_ != valueOutWrite_) {
    valueWrittenOnce_ = false;
  }
  // Check write strategy
  if (WriteStrategy().GetEnum() == Types::WriteStrategy::Enum::ON_CHANGE &&
      valueWrittenOnce_) {
    // Set validity
    SetIsValid(true);
    // Resets alert severity if no error
    SetAlertSeverity(Types::AlertSeverity::Enum::OK,
                     GetPath().append(": Done"));
    // Reset force status if on status once
    if (GetForceStatusOut().GetValue() == Types::ForceStatus::Enum::ONCE) {
      SetForceStatusOut(Types::ForceStatus::Enum::NONE);
    }
    return GetAlertSeverity();
  }
  // Log debug info
  if (GetDebugEnable()) {
    Logger()->Info(GetPath().append(":ValueOutWrite = ").append(GetVariantValueOutWrite().ToString()));
  }
  // Check if write accessor is set
  if (!AccessorItem<VALUE>::Accessor()) {
    SetAlertSeverity(Types::AlertSeverity::Enum::ERROR,
                     std::string(GetPath()).append(": Accessor not set"));
    return GetAlertSeverity();
  }
  // Write value
  if (!AccessorItem<VALUE>::Accessor()->Write(valueOutWrite_)) {
    SetAlertSeverity(Types::AlertSeverity::Enum::WARNING,
                     std::string(GetPath()).append(": Item write failure"));
    return GetAlertSeverity();
  }
  // Check if read back is active
  if (!CheckWrittenValue()) {
    valueWrittenOnce_ = true;
    // Set validity
    SetIsValid(true);
    // Resets alert severity if no error
    SetAlertSeverity(Types::AlertSeverity::Enum::OK,
                     GetPath().append(": Done"));
    // Reset force status if on status once
    if (GetForceStatusOut().GetValue() == Types::ForceStatus::Enum::ONCE) {
      SetForceStatusOut(Types::ForceStatus::Enum::NONE);
    }
    return GetAlertSeverity();
  }
  // Read value
  VALUE valueIn = {};
  if (!AccessorItem<VALUE>::Accessor()->Read(&valueIn)) {
    SetAlertSeverity(Types::AlertSeverity::Enum::WARNING,
                     std::string(GetPath()).append(": Item read failure"));
    return GetAlertSeverity();
  }
  // Compare valueOut and valueIn
  if (valueOutWrite_ != valueIn) {
    SetAlertSeverity(Types::AlertSeverity::Enum::WARNING,
                     std::string(GetPath()).append(": Item read  back compare failure"));
    return GetAlertSeverity();
  }
  valueWrittenOnce_ = true;
  // Set validity
  SetIsValid(true);
  // Resets alert severity if no error
  SetAlertSeverity(monitoringFunctionStatus_.AlertSeverity().GetEnum(),
                   GetPath().append(": ").append(monitoringFunctionStatus_.Id().ToString()));
  // Reset force status if on status once
  if (GetForceStatusOut().GetValue() == Types::ForceStatus::Enum::ONCE) {
    SetForceStatusOut(Types::ForceStatus::Enum::NONE);
  }
  return GetAlertSeverity();
}

template<typename VALUE>
VALUE const *ItemInOut<VALUE>::GetValueIn() const {
  return &valueIn_;
}

template<typename VALUE>
VALUE const *ItemInOut<VALUE>::GetValueOut() const {
  return valueOutWrite_;
}

template<typename VALUE>
Types::AlertSeverity ItemInOut<VALUE>::GetGroupAlertSeverity(
  const Types::ValueGroup::Enum &group) const {
  std::lock_guard<std::mutex> lock{mutex_};
  if (group == Types::ValueGroup::Enum::ANY) {
    return Types::AlertSeverity(GetAlertSeverity());
  }
  if (GetGroup().GetValue() == group) {
    return Types::AlertSeverity(GetAlertSeverity());
  }
  return Types::AlertSeverity(Types::AlertSeverity::Enum::OK);
}

}  // namespace SystemModules::Framework::Components
