/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <chrono>
#include "Types/ComponentStatus.h"
#include "ToBeNamed.h"

namespace WorkloadBalancing {
class WorkloadBalanceHandler;
}  // namespace WorkloadBalancing
namespace SystemModules::Framework::Components {
class ModuleHandler;
class ModuleBase;
class Component : public ToBeNamed {
  friend class Container;
 public:
  Component(
    Logger::ILogger *logger,
    const std::string &name,
    const std::string &description,
    const std::string &componentType);

  ~Component() override = default;

  [[nodiscard]] std::string GetPath() const override;

  /// \brief function returning the alert severity level of the component and the underlying components
  [[nodiscard]] Types::AlertSeverity::Enum GetAlertSeverity() const;

  void ResetAlertSeverity();

  [[nodiscard]] virtual bool Initialize(WorkloadBalancing::WorkloadBalanceHandler *updateHandler) = 0;

  [[nodiscard]] virtual bool DeInitialize(WorkloadBalancing::WorkloadBalanceHandler *updateHandler) = 0;

  [[nodiscard]] virtual Types::AlertSeverity::Enum Update(
    const std::chrono::steady_clock::time_point &timePointNow) = 0;

  virtual void ResetMonitoringFunctions() = 0;

 protected:
  [[nodiscard]] ModuleHandler const *GetModuleHandler() const override;

  [[nodiscard]] virtual Framework::Components::ModuleBase const *GetModule() const;

  [[nodiscard]] Component *GetParent() const;

  bool SetParent(Component *parent);

  void SetAlertSeverity(
    const Types::AlertSeverity::Enum &alertSeverity,
    const std::string &message);

 private:
  Component *parent_;

  // Alert severity of the component
  Types::AlertSeverity::Enum alertSeverity_;
};
}  // namespace SystemModules::Framework::Components
