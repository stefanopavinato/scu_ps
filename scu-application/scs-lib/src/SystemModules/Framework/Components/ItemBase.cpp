/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "ItemBase.h"

namespace SystemModules::Framework::Components {

ItemBase::ItemBase(
  Logger::ILogger *logger,
  const std::string &name,
  const std::string &description,
  const std::string &componentType,
  const Types::ValueType::Enum &valueType,
  const Types::ValueGroup::Enum &group,
  const std::chrono::milliseconds &updateInterval,
  const Types::Affiliation::Enum &affiliation,
  const bool &debug)
  : Component(
  logger,
  name,
  description,
  componentType),
  group_(Types::ValueGroup(group))
  , valueType_(valueType)
  , updateInterval_(updateInterval)
  , affiliation_(affiliation)
  , updateNext_(std::chrono::steady_clock::time_point::min())
  , isValid_(false)
  , debug_(debug) {
}

Component *ItemBase::GetComponent(
  std::queue<std::string> *path) {
  // Check if path is empty
  if (path->empty()) {
    return this;
  }
  return nullptr;
}

void ItemBase::ResetMonitoringFunctions() {}

Types::ValueGroup ItemBase::GetGroup() const {
  return group_;
}

Types::ValueType ItemBase::GetValueType() const {
  return valueType_;
}

std::chrono::milliseconds ItemBase::GetUpdateInterval() const {
  std::lock_guard<std::mutex> lock{mutex_};
  return updateInterval_;
}

Types::Affiliation::Enum ItemBase::GetAffiliation() const {
  return affiliation_;
}

bool ItemBase::SetUpdateInterval(
  const Types::VariantValue &value) {
  std::lock_guard<std::mutex> lock{mutex_};
  return std::visit([this](const auto &arg) -> bool {
    using K = std::decay_t<decltype(arg)>;
    // Set update interval if types match
    if constexpr (std::is_same_v<K, std::chrono::milliseconds>) {
      updateInterval_ = arg;
      return true;
    }
    return false;
  }, value);
}

bool ItemBase::TimerHasElapsed(
  const std::chrono::steady_clock::time_point &timePointNow) {
  return (updateNext_ < timePointNow);
}

void ItemBase::ResetTimer(
  const std::chrono::steady_clock::time_point &timePointNow) {
  std::lock_guard<std::mutex> lock{mutex_};
  updateNext_ = timePointNow + updateInterval_;
}

bool ItemBase::IsValid() const {
  std::lock_guard<std::mutex> lock{mutex_};
  return isValid_;
}

void ItemBase::SetIsValid(const bool &isValid) {
  isValid_ = isValid;
}

bool ItemBase::GetDebugEnable() const {
  std::lock_guard<std::mutex> lock{mutex_};
  return debug_;
}

void ItemBase::SetDebugEnable(const bool &enable) {
  std::lock_guard<std::mutex> lock{mutex_};
  debug_ = enable;
}

}  // namespace SystemModules::Framework::Components
