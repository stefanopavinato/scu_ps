/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Messages/Visitor/VisitorBase.h"
#include "ItemOutBase.h"

namespace SystemModules::Framework::Components {

ItemOutBase::ItemOutBase(
  Logger::ILogger *logger,
  const std::string &name,
  const std::string &description,
  const std::string &componentType,
  const Types::ValueType::Enum &valueType,
  const std::chrono::milliseconds &updateInterval,
  const Types::Affiliation::Enum &affiliation,
  const Types::WriteStrategy::Enum &writeStrategy,
  const bool &checkWrittenValue,
  const bool &debug)
  : ItemBase(
  logger,
  name,
  description,
  componentType,
  valueType,
  Types::ValueGroup::Enum::NONE,
  updateInterval,
  affiliation,
  debug)
  , isValueOutForced_(false)
  , forceStatus_(Types::ForceStatus::Enum::NONE)
  , writeStrategy_(writeStrategy)
  , checkWrittenValue_(checkWrittenValue) {
}

void ItemOutBase::Accept(Messages::Visitor::VisitorBase *visitor) {
  visitor->Visit(this);
}

Types::ForceStatus ItemOutBase::GetForceStatus() const {
  return forceStatus_;
}

void ItemOutBase::SetForcedStatus(const Types::ForceStatus::Enum &forceStatus) {
  forceStatus_.SetValue(forceStatus);
}

bool ItemOutBase::IsValueOutForced() const {
  return isValueOutForced_;
}

Types::WriteStrategy ItemOutBase::WriteStrategy() const {
  return writeStrategy_;
}

bool ItemOutBase::CheckWrittenValue() const {
  return checkWrittenValue_;
}

}  // namespace SystemModules::Framework::Components
