/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "AccessorItem.h"
#include "ParameterItemInBase.h"

namespace SystemModules::Framework::Components {
template<typename VALUE>
class ParameterItemIn : public ParameterItemInBase, public AccessorItem<VALUE> {
 public:
  ParameterItemIn(
    Logger::ILogger *logger,
    const std::string &name,
    const std::string &description);

  ~ParameterItemIn() override = default;

  [[nodiscard]] Types::TestVariantValue GetVariantValue() const final;

  [[nodiscard]] Accessors::AccessorBase *GetAccessor() const override;

  [[nodiscard]] VALUE const *GetValue() const;

  Builder::Accessors::ReadAccessorBuilder<VALUE> CreateAccessor();

 protected:
  [[nodiscard]] bool Initialize(WorkloadBalancing::WorkloadBalanceHandler *itemHandler) override;

  [[nodiscard]] bool DeInitialize(WorkloadBalancing::WorkloadBalanceHandler *itemHandler) override;

  Types::AlertSeverity::Enum Update(
    const std::chrono::steady_clock::time_point &timePointNow) override;

 private:
  VALUE value_;

  static const char kComponentType[];
};
}  // namespace SystemModules::Framework::Components

#include "ParameterItemIn.tpp"
