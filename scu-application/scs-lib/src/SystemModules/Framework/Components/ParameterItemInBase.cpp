/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Messages/Visitor/VisitorBase.h"
#include "ParameterItemInBase.h"

namespace SystemModules::Framework::Components {

ParameterItemInBase::ParameterItemInBase(
  Logger::ILogger *logger,
  const std::string &name,
  const std::string &description,
  const std::string &componentType)
  : ParameterItemBase(
  logger,
  name,
  description,
  componentType) {
}

void ParameterItemInBase::Accept(Messages::Visitor::VisitorBase *visitor) {
  visitor->Visit(this);
}

}  // namespace SystemModules::Framework::Components
