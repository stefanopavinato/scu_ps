/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <chrono>
#include <memory>
#include <unordered_map>
#include "Utils/ThreadBase.h"
#include "Types/ValueGroup.h"
#include "Types/AlertSeverity.h"
#include "Messages/Visitor/IVisitor.h"
#include "SystemModules/Framework/Components/ToBeNamed.h"

namespace ResourceSharing {
class Lock;
class LockHandler;
}  // namespace ResourceSharing
namespace SystemModules::Framework::Components {
class ModuleBase;
class ModuleHandler : public Utils::ThreadBase, public ToBeNamed {
 public:
  ModuleHandler(
    Logger::ILogger *logger,
    const std::string &name,
    const std::chrono::microseconds &updateInterval);

  ~ModuleHandler() override = default;

  void Accept(Messages::Visitor::VisitorBase *visitor) override;

  ToBeNamed *GetComponent(std::queue<std::string> *path) override;

  [[nodiscard]] std::string GetPath() const override;

  [[nodiscard]] Types::AlertSeverity GetGroupAlertSeverity(
    const Types::ValueGroup::Enum &group) const override;

  void ResetAllModules();

  void StopAllModules();

  void ReleaseStopAllModules();

  void SetLockHandler(ResourceSharing::LockHandler *lockHandler);

  bool AddModule(std::unique_ptr<Framework::Components::ModuleBase> module);

  bool AddGroupAlertSeverity(const Types::ValueGroup::Enum &group);

  std::unordered_map<std::string, std::unique_ptr<Framework::Components::ModuleBase>> *GetModules();

  std::unordered_map<Types::ValueGroup::Enum, Types::AlertSeverity> *GetGroupAlertSeverities();

 protected:
  [[nodiscard]] ModuleHandler const *GetModuleHandler() const override;

 private:
  [[nodiscard]] bool Run() override;

  [[nodiscard]] bool Initialize() override;

  [[nodiscard]] bool DeInitialize() override;

  void UpdateGroupAlertSeverities();

  std::chrono::microseconds updateInterval_;

  std::unordered_map<std::string, std::unique_ptr<Framework::Components::ModuleBase>> modules_;

  std::unordered_map<Types::ValueGroup::Enum, Types::AlertSeverity> groupAlertSeverities_;

  ResourceSharing::Lock *lock_;

  static const char kDescription[];

  static const char kComponentType[];
};
}  // namespace SystemModules::Framework::Components
