/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Builder/Accessors/WriteAccessorBuilder.h"

namespace SystemModules::Framework::Components {

template<typename VALUE>
const char ParameterItemOut<VALUE>::kComponentType[] = "ParameterItemOut";

template<typename VALUE>
ParameterItemOut<VALUE>::ParameterItemOut(
  Logger::ILogger *logger,
  const std::string &name,
  const std::string &description,
  const bool &checkWrittenValue,
  const VALUE &value)
  :ParameterItemOutBase(
  logger,
  name,
  description,
  kComponentType), AccessorItem<VALUE>(
  logger), value_(value), checkWrittenValue_(checkWrittenValue) {
}

template<typename VALUE>
Types::TestVariantValue ParameterItemOut<VALUE>::GetVariantValue() const {
  auto retVal = Types::TestVariantValue();
  retVal.SetValue(value_);
  return retVal;
}

template<typename VALUE>
Accessors::AccessorBase *ParameterItemOut<VALUE>::GetAccessor() const {
  return AccessorItem<VALUE>::Accessor();
}

template<typename VALUE>
Builder::Accessors::WriteAccessorBuilder <VALUE> ParameterItemOut<VALUE>::CreateAccessor() {
  return Builder::Accessors::WriteAccessorBuilder<VALUE>(this);
}


template<typename VALUE>
VALUE const *ParameterItemOut<VALUE>::GetValue() const {
  return &value_;
}

template<typename VALUE>
bool ParameterItemOut<VALUE>::Initialize(WorkloadBalancing::WorkloadBalanceHandler *itemHandler) {
  // Check if accessor is set
  if (!AccessorItem<VALUE>::Accessor()) {
    SetAlertSeverity(Types::AlertSeverity::Enum::ERROR,
                     std::string(GetPath()).append(" Accessor not set"));
    return false;
  }
  // Initialize
  if (!AccessorItem<VALUE>::Accessor()->Initialize()) {
    SetAlertSeverity(Types::AlertSeverity::Enum::WARNING,
                     std::string(GetPath()).append(" Accessor initialization failed"));
    return false;
  }
  // Write value
  if (!AccessorItem<VALUE>::Accessor()->Write(value_)) {
    SetAlertSeverity(Types::AlertSeverity::Enum::WARNING,
                     std::string(GetPath()).append(": Item write failure"));
  }
  // Check if read back is active
  if (!checkWrittenValue_) {
    Logger()->Debug(std::string("Done: ").append(GetPath()));
    return false;
  }
  // Read value
  VALUE valueIn;
  if (!AccessorItem<VALUE>::Accessor()->Read(&valueIn)) {
    SetAlertSeverity(Types::AlertSeverity::Enum::WARNING,
                     std::string(GetPath()).append(": Item read failure"));
    return false;
  }
  // Compare valueOut and valueIn
  if (value_ != valueIn) {
    SetAlertSeverity(Types::AlertSeverity::Enum::WARNING,
                     std::string(GetPath()).append(": Item read back compare failure"));
    return false;
  }
  Logger()->Debug(std::string("Done: ").append(GetPath()));
  return true;
}

template<typename VALUE>
bool ParameterItemOut<VALUE>::DeInitialize(WorkloadBalancing::WorkloadBalanceHandler *itemHandler) {
  // Check if accessor is set
  if (!AccessorItem<VALUE>::Accessor()) {
    SetAlertSeverity(Types::AlertSeverity::Enum::WARNING,
                     std::string(GetPath()).append(" Accessor not set"));
    return false;
  }
  // De-Initialize
  if (!AccessorItem<VALUE>::Accessor()->DeInitialize()) {
    SetAlertSeverity(Types::AlertSeverity::Enum::WARNING,
                     std::string(GetPath()).append(" Accessor de-initialization failed"));
    return false;
  }
  return true;
}

template<typename VALUE>
Types::AlertSeverity::Enum ParameterItemOut<VALUE>::Update(
  const std::chrono::steady_clock::time_point &timePointNow) {
  (void) timePointNow;
  return Types::AlertSeverity::Enum::OK;
}

}  // namespace SystemModules::Framework::Components
