/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Messages/Visitor/VisitorBase.h"
#include "SystemModules/Framework/Builder/Components/ComponentBuilder.h"
#include "Container.h"

namespace SystemModules::Framework::Components {

const char Container::kComponentType[] = "Container";

Container::Container(
  Logger::ILogger *logger,
  const std::string &name,
  const std::string &description)
  : Component(
  logger,
  name,
  description,
  kComponentType) {
}

Container::Container(
  Logger::ILogger *logger,
  const std::string &name,
  const std::string &description,
  const std::string &componentType)
  : Component(
  logger,
  name,
  description,
  componentType) {
}

Builder::Components::ComponentBuilder Container::Create() {
  return Builder::Components::ComponentBuilder::Create(this);
}

ToBeNamed *Container::GetComponent(
  std::queue<std::string> *path) {
  // Check if path is fully walked
  if (path->empty()) {
    return this;
  }
  // Find next component
  auto component = components_.find(path->front());
  if (component == components_.end()) {
    Logger()->Warning("Component not found");
    return nullptr;
  }
  // Remove entry from path
  path->pop();
  // Jump to next component
  return component->second->GetComponent(path);
}

bool Container::Initialize(WorkloadBalancing::WorkloadBalanceHandler *updateHandler) {
  bool retVal = true;
  // Initialize hardware
  for (const auto &component : components_) {
    retVal &= component.second->Initialize(updateHandler);
  }
  return retVal;
}

bool Container::DeInitialize(WorkloadBalancing::WorkloadBalanceHandler *updateHandler) {
  bool retVal = true;
  // De-initialize hardware
  for (const auto &component : components_) {
    retVal &= component.second->DeInitialize(updateHandler);
  }
  return retVal;
}

Types::AlertSeverity::Enum Container::Update(
  const std::chrono::steady_clock::time_point &timePointNow) {
  // Resets alert severity if no error
  auto alertSeverity = Types::AlertSeverity::Enum::OK;
  // Update components
  for (const auto &component : components_) {
    // Process inputs of child components
    auto tempAlertSeverity = component.second->Update(timePointNow);
    if (alertSeverity < tempAlertSeverity) {
      alertSeverity = tempAlertSeverity;
    }
  }
  SetAlertSeverity(alertSeverity,
                   "");
  return GetAlertSeverity();
}

bool Container::AddComponent(std::unique_ptr<Component> component) {
  if (!component) {
    Logger()->Error("No component passed");
    return false;
  }
  if (!component->SetParent(this)) {
    Logger()->Warning("Failed to set parent");
    return false;
  }
  components_.insert(std::make_pair(component->GetName(), std::move(component)));
  return true;
}

void Container::RemoveComponent(const std::string &name) {
  auto component = components_.find(name);
  if (component != components_.end()) {
    components_.erase(component);
  }
}

std::unordered_map<std::string, std::unique_ptr<Component>> *Container::GetComponents() {
  return &components_;
}

Types::AlertSeverity Container::GetGroupAlertSeverity(
  const Types::ValueGroup::Enum &group) const {
  Types::AlertSeverity alertSeverity;
  for (const auto &component : components_) {
    auto childAlertSeverity = component.second->GetGroupAlertSeverity(group);
    if (alertSeverity < childAlertSeverity) {
      alertSeverity = childAlertSeverity;
    }
  }
  return alertSeverity;
}

void Container::ResetMonitoringFunctions() {
  // Request child status reset
  for (const auto &component : components_) {
    component.second->ResetMonitoringFunctions();
  }
}

void Container::Accept(
  Messages::Visitor::VisitorBase *visitor) {
  visitor->Visit(this);
}

}  // namespace SystemModules::Framework::Components
