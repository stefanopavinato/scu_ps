/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <mutex>
#include "Types/VariantValue.h"
#include "Types/Unit.h"
#include "Types/UnitPrefix.h"
#include "Types/Affiliation.h"
#include "Component.h"

namespace SystemModules::Framework::Components {
class ItemBase : public Component {
 public:
  ItemBase(
    Logger::ILogger *logger,
    const std::string &name,
    const std::string &description,
    const std::string &componentType,
    const Types::ValueType::Enum &valueType,
    const Types::ValueGroup::Enum &group,
    const std::chrono::milliseconds &updateInterval,
    const Types::Affiliation::Enum &affiliation,
    const bool &debug);

  ~ItemBase() override = default;

  Component *GetComponent(
    std::queue<std::string> *path) final;

  void ResetMonitoringFunctions() override;

  [[nodiscard]] Types::ValueGroup GetGroup() const;

  [[nodiscard]] Types::ValueType GetValueType() const;

  [[nodiscard]] std::chrono::milliseconds GetUpdateInterval() const;

  [[nodiscard]] Types::Affiliation::Enum GetAffiliation() const;

  bool SetUpdateInterval(const Types::VariantValue &value);

  [[nodiscard]] bool IsValid() const;

  [[nodiscard]] bool GetDebugEnable() const;

  void SetDebugEnable(const bool &enable);

 protected:
  [[nodiscard]] bool TimerHasElapsed(
    const std::chrono::steady_clock::time_point &timePointNow);

  void ResetTimer(
    const std::chrono::steady_clock::time_point &timePointNow);

  void SetIsValid(const bool &isValid);

 private:
  const Types::ValueGroup group_;

  const Types::ValueType valueType_;

  std::chrono::milliseconds updateInterval_;

  Types::Affiliation::Enum affiliation_;

  std::chrono::steady_clock::time_point updateNext_;

  bool isValid_;

  bool debug_;

  mutable std::mutex mutex_;

  static const char kDevmem[];
};
}  // namespace SystemModules::Framework::Components
