/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Messages/Visitor/VisitorBase.h"
#include "ItemInBase.h"

namespace SystemModules::Framework::Components {

ItemInBase::ItemInBase(
  Logger::ILogger *logger,
  const std::string &name,
  const std::string &description,
  const std::string &componentType,
  const Types::ValueType::Enum &valueType,
  const Types::ValueGroup::Enum &group,
  const std::chrono::milliseconds &updateInterval,
  const Types::Affiliation::Enum &affiliation,
  const bool &debug)
  : ItemBase(
  logger,
  name,
  description,
  componentType,
  valueType,
  group,
  updateInterval,
  affiliation,
  debug)
  , isValueInForced_(false)
  , forceStatus_(Types::ForceStatus::Enum::NONE) {
}

void ItemInBase::Accept(Messages::Visitor::VisitorBase *visitor) {
  visitor->Visit(this);
}

bool ItemInBase::IsValueInForced() const {
  return isValueInForced_;
}

Types::ForceStatus ItemInBase::GetForcedStatus() const {
  std::lock_guard<std::mutex> lock{mutex_};
  return forceStatus_;
}

void ItemInBase::SetForcedStatus(const Types::ForceStatus::Enum &forceStatus) {
  std::lock_guard<std::mutex> lock{mutex_};
  forceStatus_.SetValue(forceStatus);
}

}  // namespace SystemModules::Framework::Components
