/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "SystemModules/Framework/Components/Container.h"

namespace SystemModules::Framework::Components {
class I2CDevice : public Container {
 public:
  I2CDevice(
    Logger::ILogger *logger,
    const std::string &name,
    const std::string &description,
    const std::string &driver,
    const uint16_t &address,
    const std::string &path);

  ~I2CDevice() override;

 protected:
  I2CDevice(
    Logger::ILogger *logger,
    const std::string &name,
    const std::string &description,
    const std::string &componentType,
    std::string driver,
    const uint16_t &address,
    std::string path);

  [[nodiscard]] std::string GetDeviceLocationPath() const;

 private:
  const std::string driver_;

  const uint16_t address_;

  const std::string path_;

  std::string pathDevice_;

  static const char kSubPathExport[];

  static const char kSubPathUnExport[];

  static const char kComponentType[];
};
}  // namespace SystemModules::Framework::Components
