/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <sstream>
#include <chrono>
#include "Messages/Visitor/VisitorBase.h"
#include "ResourceSharing/Lock.h"
#include "ResourceSharing/LockHandler.h"
#include "SystemModules/Framework/Actions/ActionBase.h"
#include "SystemModules/Framework/Components/ModuleBase.h"
#include "ModuleHandler.h"

namespace SystemModules::Framework::Components {

const char ModuleHandler::kDescription[] = "";
const char ModuleHandler::kComponentType[] = "ModuleHandler";

ModuleHandler::ModuleHandler(
  Logger::ILogger *logger,
  const std::string &name,
  const std::chrono::microseconds &updateInterval)
  : Utils::ThreadBase(logger, kComponentType)
  , SystemModules::Framework::Components::ToBeNamed(
  logger,
  name,
  kDescription,
  kComponentType)
  , updateInterval_(updateInterval)
  , lock_(nullptr) {
}

void ModuleHandler::Accept(Messages::Visitor::VisitorBase *visitor) {
  visitor->Visit(this);
}

std::string ModuleHandler::GetPath() const {
  return GetName();
}

ModuleHandler const *ModuleHandler::GetModuleHandler() const {
  return this;
}

bool ModuleHandler::Initialize() {
  return true;
}

bool ModuleHandler::DeInitialize() {
  auto timeNext = std::chrono::steady_clock::now() + updateInterval_;
  // Acquire lock
//  while (!lock_->Acquire()) {
//    std::this_thread::sleep_for(std::chrono::milliseconds(1));
//  }
  // Get current time
  auto timePointNow = std::chrono::steady_clock::now();
  // Notify all components to shut down
  for (const auto &module : modules_) {
    module.second->SetShutDown();
  }
  // Update components
  bool isRunningTemp = false;
  for (auto &module : modules_) {
    (void) module.second->Update(timePointNow);
    isRunningTemp |= module.second->IsRunning();
  }
  // Update group alert severity
  UpdateGroupAlertSeverities();
  // Release lock
//  lock_->Release();
  std::this_thread::sleep_until(timeNext);
  return isRunningTemp;
}

bool ModuleHandler::Run() {
  auto timeNext = std::chrono::steady_clock::now() + updateInterval_;
  auto timePointNow = std::chrono::steady_clock::now();
  // Acquire lock
//  while (!lock_->Acquire()) {
//    std::this_thread::sleep_for(std::chrono::milliseconds(1));
//  }
//  Logger()->Info("Lock granted");
  // Get current time
  // Update components
  for (auto &module : modules_) {
    (void) module.second->Update(timePointNow);
  }
  // Update group alert severity
  UpdateGroupAlertSeverities();
  // Delay release of lock (i2c-mux-idle-disconnect delay)
  std::this_thread::sleep_for(std::chrono::milliseconds(10));
  // Release lock
//  Logger()->Info("Lock release");
//  lock_->Release();
  std::this_thread::sleep_until(timeNext);
  return true;
}

void ModuleHandler::ResetAllModules() {
  ToBeNamed::Logger()->Info("");
  for (const auto &module : modules_) {
    module.second->Reset();
  }
}

void ModuleHandler::StopAllModules() {
  ToBeNamed::Logger()->Info("");
  for (const auto &module : modules_) {
    module.second->Stop();
  }
}

void ModuleHandler::ReleaseStopAllModules() {
  ToBeNamed::Logger()->Info("");
  for (const auto &module : modules_) {
    module.second->StopRelease();
  }
}

bool ModuleHandler::AddModule(std::unique_ptr<Framework::Components::ModuleBase> module) {
  if (!module->SetModuleHandler(this)) {
    ToBeNamed::Logger()->Error(GetPath().append(": Failed to set parent"));
    return false;
  }
  modules_.insert(std::make_pair(module->GetName(), std::move(module)));
  return true;
}

void ModuleHandler::SetLockHandler(ResourceSharing::LockHandler *lockHandler) {
  if (lockHandler == nullptr) {
    ToBeNamed::Logger()->Error("Lock handler not set");
    return;
  }
  // Set lock
//  auto lock = std::make_unique<ResourceSharing::Lock>(Logger(), "MH", 1);
//  lock_ = lock.get();
//  lockHandler->AddLock(std::move(lock));
}

bool ModuleHandler::AddGroupAlertSeverity(const Types::ValueGroup::Enum &group) {
  if (groupAlertSeverities_.find(group) != groupAlertSeverities_.end()) {
    return false;
  }
  groupAlertSeverities_.insert(std::make_pair(group, Types::AlertSeverity()));
  return true;
}

Types::AlertSeverity ModuleHandler::GetGroupAlertSeverity(
  const Types::ValueGroup::Enum &group) const {
  auto severity = groupAlertSeverities_.find(group);
  if (severity == groupAlertSeverities_.end()) {
    std::stringstream message;
    message << "Group alert severity not found (Name: " << Types::ValueGroup(group).ToString() << ")" << std::endl;
    ToBeNamed::Logger()->Error(message.str());
    return Types::AlertSeverity(Types::AlertSeverity::Enum::OK);
  }
  return severity->second;
}

ToBeNamed *ModuleHandler::GetComponent(std::queue<std::string> *path) {
  // Check if path is fully walked
  if (path->empty()) {
    return this;
  }
  // Find next component
  auto component = modules_.find(path->front());
  if (component == modules_.end()) {
    return nullptr;
  }
  // Remove entry from path
  path->pop();
  // Jump to next component
  return component->second->GetComponent(path);
}

std::unordered_map<std::string, std::unique_ptr<Framework::Components::ModuleBase>> *ModuleHandler::GetModules() {
  return &modules_;
}

std::unordered_map<Types::ValueGroup::Enum, Types::AlertSeverity> *ModuleHandler::GetGroupAlertSeverities() {
  return &groupAlertSeverities_;
}

void ModuleHandler::UpdateGroupAlertSeverities() {
  for (auto &groupState : groupAlertSeverities_) {
    groupState.second.SetEnum(Types::AlertSeverity::Enum::OK);
    for (auto &systemModule : modules_) {
      (void) systemModule;
      auto valueGroup = Types::ValueGroup(groupState.first);
      auto tempState = systemModule.second->GetGroupAlertSeverity(valueGroup.GetValue());
      if (groupState.second < tempState) {
        groupState.second = tempState;
      }
    }
  }
}

}  // namespace SystemModules::Framework::Components
