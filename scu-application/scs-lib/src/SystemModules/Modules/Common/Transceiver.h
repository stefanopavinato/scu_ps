/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "SystemModules/Framework/Components/ItemIn.h"
#include "SystemModules/Framework/Components/Container.h"
#include "SystemModules/Modules/Configuration/ConfigurationCommon.h"

namespace SystemModules::Modules::Common {
class Transceiver : public Framework::Components::Container {
 public:
  Transceiver(
    Logger::ILogger *logger,
    const Modules::Configuration::ConfigurationCommon &configCommon,
    std::string const &name,
    uint32_t const &address);

  [[nodiscard]] Framework::Components::ItemIn<uint32_t> *Status() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *LaneUp() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *ChannelUp() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *HardError() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *SoftError() const;

  [[nodiscard]] Framework::Components::ItemIn<uint32_t> *LaneDownCounter() const;

  [[nodiscard]] Framework::Components::ItemIn<uint32_t> *ChannelDownCounter() const;

  [[nodiscard]] Framework::Components::ItemIn<uint32_t> *HardErrorCounter() const;

  [[nodiscard]] Framework::Components::ItemIn<uint32_t> *SoftErrorCounter() const;

 private:
  Framework::Components::ItemIn<uint32_t> *status_;

  Framework::Components::ItemIn<bool> *laneUp_;

  Framework::Components::ItemIn<bool> *channelUp_;

  Framework::Components::ItemIn<bool> *hardError_;

  Framework::Components::ItemIn<bool> *softError_;

  Framework::Components::ItemIn<uint32_t> *laneDownCounter_;

  Framework::Components::ItemIn<uint32_t> *channelDownCounter_;

  Framework::Components::ItemIn<uint32_t> *hardErrorCounter_;

  Framework::Components::ItemIn<uint32_t> *softErrorCounter_;

  static const char kDescription[];

  static const char kStatusName[];
  static const char kStatusDescription[];
  static const uint32_t kStatusPosition;
  static const uint32_t kStatusSize;

  static const char kLaneUpName[];
  static const char kLaneUpDescription[];
  static const uint32_t kLaneUpPosition;
  static const uint32_t kLaneUpSize;

  static const char kChannelUpName[];
  static const char kChannelUpDescription[];
  static const uint32_t kChannelUpPosition;
  static const uint32_t kChannelUpSize;

  static const char kHardErrorName[];
  static const char kHardErrorDescription[];
  static const uint32_t kHardErrorPosition;
  static const uint32_t kHardErrorSize;

  static const char kSoftErrorName[];
  static const char kSoftErrorDescription[];
  static const uint32_t kSoftErrorPosition;
  static const uint32_t kSoftErrorSize;

  static const char kLaneDownCounterName[];
  static const char kLaneDownCounterDescription[];
  static const uint32_t kLaneDownCounterAddressOffset;
  // Relative to register with {soft error, hard error, channel up, lane up}
  static const uint32_t kLaneDownCounterPosition;
  static const uint32_t kLaneDownCounterSize;

  static const char kChannelDownCounterName[];
  static const char kChannelDownCounterDescription[];
  static const uint32_t kChannelDownCounterAddressOffset;
  // Relative to register with {soft error, hard error, channel up, lane up}
  static const uint32_t kChannelDownCounterPosition;
  static const uint32_t kChannelDownCounterSize;

  static const char kHardErrorCounterName[];
  static const char kHardErrorCounterDescription[];
  static const uint32_t kHardErrorCounterAddressOffset;
  // Relative to register with {soft error, hard error, channel up, lane up}
  static const uint32_t kHardErrorCounterPosition;
  static const uint32_t kHardErrorCounterSize;

  static const char kSoftErrorCounterName[];
  static const char kSoftErrorCounterDescription[];
  static const uint32_t kSoftErrorCounterAddressOffset;
  // Relative to register with {soft error, hard error, channel up, lane up}
  static const uint32_t kSoftErrorCounterPosition;
  static const uint32_t kSoftErrorCounterSize;
};
}  // namespace SystemModules::Modules::Common
