/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Builder/Accessors/ReadAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemInBuilder.h"
#include "Transceiver.h"

namespace SystemModules::Modules::Common {

const char Transceiver::kDescription[] = "";

const char Transceiver::kStatusName[] = "Status";
const char Transceiver::kStatusDescription[] = "Status of LaneUp ChannelUp HardError SoftError";
const uint32_t Transceiver::kStatusPosition = 0;
const uint32_t Transceiver::kStatusSize = 4;

const char Transceiver::kLaneUpName[] = "LaneUp";
const char Transceiver::kLaneUpDescription[] = "";
const uint32_t Transceiver::kLaneUpPosition = 0;
const uint32_t Transceiver::kLaneUpSize = 1;

const char Transceiver::kChannelUpName[] = "ChannelUp";
const char Transceiver::kChannelUpDescription[] = "";
const uint32_t Transceiver::kChannelUpPosition = 1;
const uint32_t Transceiver::kChannelUpSize = 1;

const char Transceiver::kHardErrorName[] = "HardError";
const char Transceiver::kHardErrorDescription[] = "";
const uint32_t Transceiver::kHardErrorPosition = 2;
const uint32_t Transceiver::kHardErrorSize = 1;

const char Transceiver::kSoftErrorName[] = "SoftError";
const char Transceiver::kSoftErrorDescription[] = "";
const uint32_t Transceiver::kSoftErrorPosition = 3;
const uint32_t Transceiver::kSoftErrorSize = 1;

const char Transceiver::kLaneDownCounterName[] = "LaneDownCounter";
const char Transceiver::kLaneDownCounterDescription[] = "";
const uint32_t Transceiver::kLaneDownCounterAddressOffset = 16;
// Relative to register with {soft error, hard error, channel up, lane up}
const uint32_t Transceiver::kLaneDownCounterPosition = 0;
const uint32_t Transceiver::kLaneDownCounterSize = 32;

const char Transceiver::kChannelDownCounterName[] = "ChannelDownCounter";
const char Transceiver::kChannelDownCounterDescription[] = "";
const uint32_t Transceiver::kChannelDownCounterAddressOffset = 12;
// Relative to register with {soft error, hard error, channel up, lane up}
const uint32_t Transceiver::kChannelDownCounterPosition = 0;
const uint32_t Transceiver::kChannelDownCounterSize = 32;

const char Transceiver::kHardErrorCounterName[] = "HardErrorCounter";
const char Transceiver::kHardErrorCounterDescription[] = "";
const uint32_t Transceiver::kHardErrorCounterAddressOffset = 8;
// Relative to register with {soft error, hard error, channel up, lane up}
const uint32_t Transceiver::kHardErrorCounterPosition = 0;
const uint32_t Transceiver::kHardErrorCounterSize = 32;

const char Transceiver::kSoftErrorCounterName[] = "SoftErrorCounter";
const char Transceiver::kSoftErrorCounterDescription[] = "";
const uint32_t Transceiver::kSoftErrorCounterAddressOffset = 16;
// Relative to register with {soft error, hard error, channel up, lane up}
const uint32_t Transceiver::kSoftErrorCounterPosition = 0;
const uint32_t Transceiver::kSoftErrorCounterSize = 32;

Transceiver::Transceiver(
  Logger::ILogger *logger,
  const Modules::Configuration::ConfigurationCommon &configCommon,
  std::string const &name,
  uint32_t const &address)
  : Framework::Components::Container(
  logger,
  name,
  kDescription) {
  // Get update intervaval
  auto updateInterval = configCommon.UpdateInterval()->Medium()->Value();

  status_ = Create().ItemIn<uint32_t>(
      kStatusName,
      kStatusDescription,
      Types::ValueGroup::Enum::NONE,
      std::chrono::milliseconds(updateInterval),
      Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  status_->CreateAccessor().ScuRegister(
      address,
      kStatusPosition,
      kStatusSize);

  laneUp_ = Create().ItemIn<bool>(
      kLaneUpName,
      kLaneUpDescription,
      Types::ValueGroup::Enum::NONE,
      std::chrono::milliseconds(updateInterval),
      Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  laneUp_->CreateAccessor().ScuRegister(
      address,
      kLaneUpPosition,
      kLaneUpSize);

  channelUp_ = Create().ItemIn<bool>(
    kChannelUpName,
    kChannelUpDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  channelUp_->CreateAccessor().ScuRegister(
    address,
    kChannelUpPosition,
    kChannelUpSize);

  hardError_ = Create().ItemIn<bool>(
    kHardErrorName,
    kHardErrorDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  hardError_->CreateAccessor().ScuRegister(
    address,
    kHardErrorPosition,
    kHardErrorSize);

  softError_ = Create().ItemIn<bool>(
    kSoftErrorName,
    kSoftErrorDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  softError_->CreateAccessor().ScuRegister(
    address,
    kSoftErrorPosition,
    kSoftErrorSize);

  laneDownCounter_ = Create().ItemIn<uint32_t>(
      kLaneDownCounterName,
      kLaneDownCounterDescription,
      Types::ValueGroup::Enum::NONE,
      std::chrono::milliseconds(updateInterval),
      Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  laneDownCounter_->CreateAccessor().ScuRegister(
      address+kLaneDownCounterAddressOffset,
      kLaneDownCounterPosition,
      kLaneDownCounterSize);

  channelDownCounter_ = Create().ItemIn<uint32_t>(
      kChannelDownCounterName,
      kChannelDownCounterDescription,
      Types::ValueGroup::Enum::NONE,
      std::chrono::milliseconds(updateInterval),
      Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  channelDownCounter_->CreateAccessor().ScuRegister(
      address+kChannelDownCounterAddressOffset,
      kChannelDownCounterPosition,
      kChannelDownCounterSize);

  hardErrorCounter_ = Create().ItemIn<uint32_t>(
      kHardErrorCounterName,
      kHardErrorCounterDescription,
      Types::ValueGroup::Enum::NONE,
      std::chrono::milliseconds(updateInterval),
      Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  hardErrorCounter_->CreateAccessor().ScuRegister(
      address+kHardErrorCounterAddressOffset,
      kHardErrorCounterPosition,
      kHardErrorCounterSize);

  softErrorCounter_ = Create().ItemIn<uint32_t>(
      kSoftErrorCounterName,
      kSoftErrorCounterDescription,
      Types::ValueGroup::Enum::NONE,
      std::chrono::milliseconds(updateInterval),
      Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  softErrorCounter_->CreateAccessor().ScuRegister(
      address+kSoftErrorCounterAddressOffset,
      kSoftErrorCounterPosition,
      kSoftErrorCounterSize);
}

Framework::Components::ItemIn<uint32_t> *Transceiver::Status() const {
  return status_;
}

Framework::Components::ItemIn<bool> *Transceiver::LaneUp() const {
  return laneUp_;
}

Framework::Components::ItemIn<bool> *Transceiver::ChannelUp() const {
  return channelUp_;
}

Framework::Components::ItemIn<bool> *Transceiver::HardError() const {
  return hardError_;
}

Framework::Components::ItemIn<bool> *Transceiver::SoftError() const {
  return softError_;
}

Framework::Components::ItemIn<uint32_t> *Transceiver::LaneDownCounter() const {
  return laneDownCounter_;
}

Framework::Components::ItemIn<uint32_t> *Transceiver::ChannelDownCounter() const {
  return channelDownCounter_;
}

Framework::Components::ItemIn<uint32_t> *Transceiver::HardErrorCounter() const {
  return hardErrorCounter_;
}

Framework::Components::ItemIn<uint32_t> *Transceiver::SoftErrorCounter() const {
  return softErrorCounter_;
}

}  // namespace SystemModules::Modules::Common
