/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "SystemModules/Framework/Components/Container.h"
#include "SystemModules/Modules/Configuration/ConfigurationCommon.h"
#include "SystemModules/Modules/Configuration/Modules/Configuration_RS485MC.h"
#include "SystemModules/Modules/Configuration/Slots/Configuration_Slots.h"

namespace SystemModules::Modules {
namespace Common {
class VoltageMonitor_3V3_2V5_1V8_1V2;
class HotSwapController;
}  // namespace Common
namespace RS485MC {
class RS485MCGpios;

class RS485MCAnalogIn1;

class RS485MC_MCDriver;

class RS485MC : public Framework::Components::Container {
 public:
  RS485MC(
    Logger::ILogger *logger,
    const Modules::Configuration::Modules::Configuration_RS485MC &configRS485MC,
    const Modules::Configuration::ConfigurationCommon &configCommon,
    const Modules::Configuration::Configuration_Slots &config);

  [[nodiscard]] RS485MCGpios *Gpios() const;

  [[nodiscard]] RS485MCAnalogIn1 *AnalogIn1() const;

  [[nodiscard]] RS485MC_MCDriver *MCDriver() const;

  [[nodiscard]] Common::VoltageMonitor_3V3_2V5_1V8_1V2 *VoltageMonitor() const;

  [[nodiscard]] Modules::Common::HotSwapController *HotSwapController() const;

 private:
  RS485MCGpios *gpios_;

  RS485MCAnalogIn1 *analogIn1_;

  RS485MC_MCDriver *mcdriver_;

  Common::VoltageMonitor_3V3_2V5_1V8_1V2 *voltageMonitor_;

  Modules::Common::HotSwapController *hotSwapController_;
};
}  // namespace RS485MC
}  // namespace SystemModules::Modules
