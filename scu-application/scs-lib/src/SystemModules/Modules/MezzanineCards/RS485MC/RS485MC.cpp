/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Builder/Actions/ActionBuilder.h"
#include "SystemModules/Framework/Builder/Actions/ActionBuilderGeneric.h"
#include "SystemModules/Framework/Builder/Accessors/ReadAccessorBuilder.h"
#include "SystemModules/Modules/MezzanineCards/Common/VoltageMonitor_3V3_2V5_1V8_1V2.h"
#include "SystemModules/Modules/MezzanineCards/Common/HotSwapController.h"
#include "RS485MCGpios.h"
#include "RS485MCAnalogIn1.h"
#include "RS485MC_MCDriverOverride.h"
#include "RS485MC_MCDriverStatus.h"
#include "RS485MC_MCDriver.h"
#include "RS485MC.h"

namespace SystemModules::Modules::RS485MC {

RS485MC::RS485MC(
  Logger::ILogger *logger,
  const Modules::Configuration::Modules::Configuration_RS485MC &configRS485MC,
  const Modules::Configuration::ConfigurationCommon &configCommon,
  const Modules::Configuration::Configuration_Slots &config)
  : Container(
  logger,
  "Application",
  "RS485MC specific application") {
  // Create gpio
  auto gpios = std::make_unique<RS485MCGpios>(
      logger,
      configCommon,
      config.PathI2CApplication()->GetValue(),
      config.PathI2CApplicationDevice()->GetValue(),
      config.Slot()->GetValue());
  gpios_ = gpios.get();
  AddComponent(std::move(gpios));
  auto analogIn1 = std::make_unique<RS485MCAnalogIn1>(
      logger,
      config.PathI2CApplication()->GetValue(),
      config.PathI2CApplicationDevice()->GetValue(),
      config.Slot()->GetValue());
  analogIn1_ = analogIn1.get();
  AddComponent(std::move(analogIn1));

  // Create RS485MC MC_Driver
  auto mcdriver = std::make_unique<RS485MC_MCDriver>(
    logger,
    configCommon,
    config);
  mcdriver_ = mcdriver.get();
  AddComponent(std::move(mcdriver));

  // Create voltage monitor
  auto voltageMonitor = std::make_unique<Common::VoltageMonitor_3V3_2V5_1V8_1V2>(
    logger,
    configCommon,
    config.PathI2CManagement()->GetValue());
  voltageMonitor_ = voltageMonitor.get();
  AddComponent(std::move(voltageMonitor));
  // Create hot swap controller
  auto hotSwapController = std::make_unique<Modules::Common::HotSwapController>(
    logger,
    *configRS485MC.HotSwapController(),
    configCommon,
    config.PathI2CManagement()->GetValue(),
    0x4b);
  hotSwapController_ = hotSwapController.get();
  AddComponent(std::move(hotSwapController));

  // ToDo: Note: these actions dont use the right inputs!!!!!

/*  MCDriver()->Override()->MagnetronPSOutput()->CreateAction().TriggerOutput(
      MCDriver()->Status()->TriggerInput(),
      MCDriver()->Status()->TriggerOutput(),
      MCDriver()->Status()->MagnetronPSEnable(),
      MCDriver()->Status()->WatchdogAExpired(),
      MCDriver()->Status()->WatchdogBExpired());

  MCDriver()->Override()->FSUPowerOn()->CreateAction().FsuOutput(
      MCDriver()->Status()->FSUPowerOn(),
      MCDriver()->Status()->FSUOutputSignal());
*/
  MCDriver()->Override()->Port1_Discrete_Signal0()->CreateAction().StaticValue(false);
  MCDriver()->Override()->Port1_Discrete_Signal1()->CreateAction().StaticValue(false);
  MCDriver()->Override()->Port1_Discrete_Signal2()->CreateAction().StaticValue(false);
  MCDriver()->Override()->Port1_Discrete_Signal3()->CreateAction().StaticValue(false);
  MCDriver()->Override()->Port2_Discrete_Signal0()->CreateAction().StaticValue(false);
  MCDriver()->Override()->Port2_Discrete_Signal1()->CreateAction().StaticValue(false);
  MCDriver()->Override()->Port2_Discrete_Signal2()->CreateAction().StaticValue(false);
  MCDriver()->Override()->Port2_Discrete_Signal3()->CreateAction().StaticValue(false);
}

RS485MCGpios *RS485MC::Gpios() const {
  return gpios_;
}

RS485MCAnalogIn1 *RS485MC::AnalogIn1() const {
  return analogIn1_;
}

RS485MC_MCDriver *RS485MC::MCDriver() const {
  return mcdriver_;
}

Modules::Common::VoltageMonitor_3V3_2V5_1V8_1V2 *RS485MC::VoltageMonitor() const {
  return voltageMonitor_;
}

Modules::Common::HotSwapController *RS485MC::HotSwapController() const {
  return hotSwapController_;
}

}  // namespace SystemModules::Modules::RS485MC
