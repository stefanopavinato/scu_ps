/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <cstdint>
#include "SystemModules/Framework/Components/ItemIn.h"
#include "SystemModules/Framework/Components/Container.h"
#include "SystemModules/Modules/Configuration/ConfigurationCommon.h"
#include "SystemModules/Modules/Configuration/Slots/Configuration_Slots.h"

namespace SystemModules::Modules::RS485MC {
class RS485MC_MCDriverOverride : public Framework::Components::Container {
 public:
  RS485MC_MCDriverOverride(
    Logger::ILogger *logger,
    const Modules::Configuration::ConfigurationCommon &configCommon,
    const Configuration::Configuration_Slots &config);

  [[nodiscard]] Framework::Components::ItemOut<bool> *Port1_Discrete_Signal0() const;

  [[nodiscard]] Framework::Components::ItemOut<bool> *Port1_Discrete_Signal1() const;

  [[nodiscard]] Framework::Components::ItemOut<bool> *Port1_Discrete_Signal2() const;

  [[nodiscard]] Framework::Components::ItemOut<bool> *Port1_Discrete_Signal3() const;

  [[nodiscard]] Framework::Components::ItemOut<bool> *Port2_Discrete_Signal0() const;

  [[nodiscard]] Framework::Components::ItemOut<bool> *Port2_Discrete_Signal1() const;

  [[nodiscard]] Framework::Components::ItemOut<bool> *Port2_Discrete_Signal2() const;

  [[nodiscard]] Framework::Components::ItemOut<bool> *Port2_Discrete_Signal3() const;

 private:
  Framework::Components::ItemOut<bool> *port1_Discrete_Signal0_;

  Framework::Components::ItemOut<bool> *port1_Discrete_Signal1_Discrete_;

  Framework::Components::ItemOut<bool> *port1_Discrete_Signal2_Discrete_;

  Framework::Components::ItemOut<bool> *port1_Discrete_Signal3_;

  Framework::Components::ItemOut<bool> *port2_Discrete_Signal0_;

  Framework::Components::ItemOut<bool> *port2_Discrete_Signal1_Discrete_;

  Framework::Components::ItemOut<bool> *port2_Discrete_Signal2_Discrete_;

  Framework::Components::ItemOut<bool> *port2_Discrete_Signal3_;

  static const char kName[];
  static const char kDescription[];

  static const char kPort1_Discrete_Signal0Name[];
  static const char kPort1_Discrete_Signal0Description[];
  static const uint32_t kPort1_Discrete_Signal0Position;

  static const char kPort1_Discrete_Signal1Name[];
  static const char kPort1_Discrete_Signal1Description[];
  static const uint32_t kPort1_Discrete_Signal1Position;

  static const char kPort1_Discrete_Signal2Name[];
  static const char kPort1_Discrete_Signal2Description[];
  static const uint32_t kPort1_Discrete_Signal2Position;

  static const char kPort1_Discrete_Signal3Name[];
  static const char kPort1_Discrete_Signal3Description[];
  static const uint32_t kPort1_Discrete_Signal3Position;

  static const char kPort2_Discrete_Signal0Name[];
  static const char kPort2_Discrete_Signal0Description[];
  static const uint32_t kPort2_Discrete_Signal0Position;

  static const char kPort2_Discrete_Signal1Name[];
  static const char kPort2_Discrete_Signal1Description[];
  static const uint32_t kPort2_Discrete_Signal1Position;

  static const char kPort2_Discrete_Signal2Name[];
  static const char kPort2_Discrete_Signal2Description[];
  static const uint32_t kPort2_Discrete_Signal2Position;

  static const char kPort2_Discrete_Signal3Name[];
  static const char kPort2_Discrete_Signal3Description[];
  static const uint32_t kPort2_Discrete_Signal3Position;
};

}  // namespace SystemModules::Modules::RS485MC
