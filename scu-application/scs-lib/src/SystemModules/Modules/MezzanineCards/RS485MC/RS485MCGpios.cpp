/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Actions/ActionTemplate.h"
#include "SystemModules/Framework/Builder/Actions/ActionBuilder.h"
#include "SystemModules/Framework/MonitoringFunctions/MFTemplate.h"
#include "SystemModules/Framework/Accessors/AccessorBase.h"
#include "SystemModules/Framework/Builder/Accessors/ReadAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Accessors/WriteAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemInBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemOutBuilder.h"
#include "SystemModules/Framework/Builder/Components/ParameterItemInBuilder.h"
#include "SystemModules/Framework/Builder/Components/ComponentBuilder.h"
#include "RS485MCGpios.h"

namespace SystemModules::Modules::RS485MC {

const char RS485MCGpios::kName[] = "Gpios";
const char RS485MCGpios::kDescription[] = "Additional gpios on MC";
const uint16_t RS485MCGpios::kAddress = 0x74;

const char RS485MCGpios::kEthSoftResetName[] = "EthSoftReset";
const char RS485MCGpios::kEthSoftResetDescription[] = "";
const uint32_t RS485MCGpios::kEthSoftResetAddressOffset = 2;

const char RS485MCGpios::kPowerGoodName[] = "PowerGood";
const char RS485MCGpios::kPowerGoodDescription[] = "";
const uint32_t RS485MCGpios::kPowerGoodAddressOffset = 3;

const char RS485MCGpios::kPort1LedGreenAName[] = "Port1LedGreen";
const char RS485MCGpios::kPort1LedGreenADescription[] = "";
const uint32_t RS485MCGpios::kPort1LedGreenAAddressOffset = 6;

const char RS485MCGpios::kPort1LedRedAName[] = "Port1LedRed";
const char RS485MCGpios::kPort1LedRedADescription[] = "";
const uint32_t RS485MCGpios::kPort1LedRedAAddressOffset = 7;

const char RS485MCGpios::kPort1Signal0LossName[] = "Port1Signal0Loss";
const char RS485MCGpios::kPort1Signal0LossDescription[] = "";
const uint32_t RS485MCGpios::kPort1Signal0LossAddressOffset = 9;

const char RS485MCGpios::kPort1Signal1LossName[] = "Port1Signal1Loss";
const char RS485MCGpios::kPort1Signal1LossDescription[] = "";
const uint32_t RS485MCGpios::kPort1Signal1LossAddressOffset = 8;

const char RS485MCGpios::kPort1Signal2LossName[] = "Port1Signal2Loss";
const char RS485MCGpios::kPort1Signal2LossDescription[] = "";
const uint32_t RS485MCGpios::kPort1Signal2LossAddressOffset = 11;

const char RS485MCGpios::kPort1Signal3LossName[] = "Port1Signal3Loss";
const char RS485MCGpios::kPort1Signal3LossDescription[] = "";
const uint32_t RS485MCGpios::kPort1Signal3LossAddressOffset = 10;

const char RS485MCGpios::kPort2LedGreenAName[] = "Port2LedGreen";
const char RS485MCGpios::kPort2LedGreenADescription[] = "";
const uint32_t RS485MCGpios::kPort2LedGreenAAddressOffset = 1;

const char RS485MCGpios::kPort2LedRedAName[] = "Port2LedRed";
const char RS485MCGpios::kPort2LedRedADescription[] = "";
const uint32_t RS485MCGpios::kPort2LedRedAAddressOffset = 0;

const char RS485MCGpios::kPort2Signal0LossName[] = "Port2Signal0Loss";
const char RS485MCGpios::kPort2Signal0LossDescription[] = "";
const uint32_t RS485MCGpios::kPort2Signal0LossAddressOffset = 12;

const char RS485MCGpios::kPort2Signal1LossName[] = "Port2Signal1Loss";
const char RS485MCGpios::kPort2Signal1LossDescription[] = "";
const uint32_t RS485MCGpios::kPort2Signal1LossAddressOffset = 13;

const char RS485MCGpios::kPort2Signal2LossName[] = "Port2Signal2Loss";
const char RS485MCGpios::kPort2Signal2LossDescription[] = "";
const uint32_t RS485MCGpios::kPort2Signal2LossAddressOffset = 15;

const char RS485MCGpios::kPort2Signal3LossName[] = "Port2Signal3Loss";
const char RS485MCGpios::kPort2Signal3LossDescription[] = "";
const uint32_t RS485MCGpios::kPort2Signal3LossAddressOffset = 14;

const char RS485MCGpios::kDbgLedGreenAName[] = "DbgLedGreenA";
const char RS485MCGpios::kDbgLedGreenADescription[] = "";
const uint32_t RS485MCGpios::kDbgLedGreenAAddressOffset = 5;

const char RS485MCGpios::kDbgLedRedAName[] = "DbgLedRedA";
const char RS485MCGpios::kDbgLedRedADescription[] = "";
const uint32_t RS485MCGpios::kDbgLedRedAAddressOffset = 4;

RS485MCGpios::RS485MCGpios(
  Logger::ILogger *logger,
  const Modules::Configuration::ConfigurationCommon &configCommon,
  const std::string &path,
  const std::string &devicePath,
  const uint32_t &slot)
  : Framework::Components::I2CDevicePCA9539(
  logger,
  kName,
  kDescription,
  kAddress,
  path) {
  (void) devicePath;
  (void) slot;
  auto updateInterval = configCommon.UpdateInterval()->Medium()->Value();

  auto baseAddr = GetBaseAddress();

  powerGood_ = Create().ItemIn<bool>(
    kPowerGoodName,
    kPowerGoodDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::SHARED,
    1,
    false).AddAndReturnPointer();
  powerGood_->CreateAccessor().DigitalInput(
    baseAddr + kPowerGoodAddressOffset,
    false);

  ethSoftReset_ = Create().ItemOut<bool>(
    kEthSoftResetName,
    kEthSoftResetDescription,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::SHARED,
    Types::WriteStrategy::Enum::ON_CHANGE,
    false,
    false,
    false).AddAndReturnPointer();
  ethSoftReset_->CreateAccessor().DigitalOutput(
    baseAddr + kEthSoftResetAddressOffset,
    true);
  ethSoftReset_->CreateAction().StaticValue(false);

  port1LedGreenA_ = Create().ItemOut<bool>(
    kPort1LedGreenAName,
    kPort1LedGreenADescription,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::SHARED,
    Types::WriteStrategy::Enum::ON_CHANGE,
    false,
    false,
    false).AddAndReturnPointer();
  port1LedGreenA_->CreateAccessor().DigitalOutput(
    baseAddr + kPort1LedGreenAAddressOffset,
    false);
  port1LedGreenA_->CreateAction().StaticValue(false);

  port1LedRedA_ = Create().ItemOut<bool>(
    kPort1LedRedAName,
    kPort1LedRedADescription,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::SHARED,
    Types::WriteStrategy::Enum::ON_CHANGE,
    false,
    false,
    false).AddAndReturnPointer();
  port1LedRedA_->CreateAccessor().DigitalOutput(
    baseAddr + kPort1LedRedAAddressOffset,
    false);
  port1LedRedA_->CreateAction().StaticValue(false);

  Port1Signal0Loss_ = Create().ItemIn<bool>(
    kPort1Signal0LossName,
    kPort1Signal0LossDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::SHARED,
    1,
    false).AddAndReturnPointer();
  Port1Signal0Loss_->CreateAccessor().DigitalInput(
    baseAddr + kPort1Signal0LossAddressOffset,
    false);

  Port1Signal1Loss_ = Create().ItemIn<bool>(
    kPort1Signal1LossName,
    kPort1Signal1LossDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::SHARED,
    1,
    false).AddAndReturnPointer();
  Port1Signal1Loss_->CreateAccessor().DigitalInput(
    baseAddr + kPort1Signal1LossAddressOffset,
    false);

  Port1Signal2Loss_ = Create().ItemIn<bool>(
    kPort1Signal2LossName,
    kPort1Signal2LossDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::SHARED,
    1,
    false).AddAndReturnPointer();
  Port1Signal2Loss_->CreateAccessor().DigitalInput(
    baseAddr + kPort1Signal2LossAddressOffset,
    false);

  Port1Signal3Loss_ = Create().ItemIn<bool>(
    kPort1Signal3LossName,
    kPort1Signal3LossDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::SHARED,
    1,
    false).AddAndReturnPointer();
  Port1Signal3Loss_->CreateAccessor().DigitalInput(
    baseAddr + kPort1Signal3LossAddressOffset,
    false);

  port2LedGreenA_ = Create().ItemOut<bool>(
    kPort2LedGreenAName,
    kPort2LedGreenADescription,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::SHARED,
    Types::WriteStrategy::Enum::ON_CHANGE,
    false,
    false,
    false).AddAndReturnPointer();
  port2LedGreenA_->CreateAccessor().DigitalOutput(
    baseAddr + kPort2LedGreenAAddressOffset,
    false);
  port2LedGreenA_->CreateAction().StaticValue(false);

  port2LedRedA_ = Create().ItemOut<bool>(
    kPort2LedRedAName,
    kPort2LedRedADescription,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::SHARED,
    Types::WriteStrategy::Enum::ON_CHANGE,
    false,
    false,
    false).AddAndReturnPointer();
  port2LedRedA_->CreateAccessor().DigitalOutput(
    baseAddr + kPort2LedRedAAddressOffset,
    false);
  port2LedRedA_->CreateAction().StaticValue(false);

  Port2Signal0Loss_ = Create().ItemIn<bool>(
    kPort2Signal0LossName,
    kPort2Signal0LossDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::SHARED,
    1,
    false).AddAndReturnPointer();
  Port2Signal0Loss_->CreateAccessor().DigitalInput(
    baseAddr + kPort2Signal0LossAddressOffset,
    false);

  Port2Signal1Loss_ = Create().ItemIn<bool>(
    kPort2Signal1LossName,
    kPort2Signal1LossDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::SHARED,
    1,
    false).AddAndReturnPointer();
  Port2Signal1Loss_->CreateAccessor().DigitalInput(
    baseAddr + kPort2Signal1LossAddressOffset,
    false);

  Port2Signal2Loss_ = Create().ItemIn<bool>(
    kPort2Signal2LossName,
    kPort2Signal2LossDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::SHARED,
    1,
    false).AddAndReturnPointer();
  Port2Signal2Loss_->CreateAccessor().DigitalInput(
    baseAddr + kPort2Signal2LossAddressOffset,
    false);

  Port2Signal3Loss_ = Create().ItemIn<bool>(
    kPort2Signal3LossName,
    kPort2Signal3LossDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::SHARED,
    1,
    false).AddAndReturnPointer();
  Port2Signal3Loss_->CreateAccessor().DigitalInput(
    baseAddr + kPort2Signal3LossAddressOffset,
    false);

  dbgLedGreenA_ = Create().ItemOut<bool>(
    kDbgLedGreenAName,
    kDbgLedGreenADescription,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::SHARED,
    Types::WriteStrategy::Enum::ON_CHANGE,
    false,
    false,
    false).AddAndReturnPointer();
  dbgLedGreenA_->CreateAccessor().DigitalOutput(
    baseAddr + kDbgLedGreenAAddressOffset,
    false);
  dbgLedGreenA_->CreateAction().StaticValue(false);

  dbgLedRedA_ = Create().ItemOut<bool>(
    kDbgLedRedAName,
    kDbgLedRedADescription,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::SHARED,
    Types::WriteStrategy::Enum::ON_CHANGE,
    true,
    false,
    false).AddAndReturnPointer();
  dbgLedRedA_->CreateAccessor().DigitalOutput(
    baseAddr + kDbgLedRedAAddressOffset,
    false);
  dbgLedRedA_->CreateAction().StaticValue(false);
}

Framework::Components::ItemIn<bool> *RS485MCGpios::PowerGood() const {
  return powerGood_;
}

Framework::Components::ItemOut<bool> *RS485MCGpios::EthSoftReset() const {
  return ethSoftReset_;
}

Framework::Components::ItemOut<bool> *RS485MCGpios::Port1LedGreenA() const {
  return port1LedGreenA_;
}
Framework::Components::ItemOut<bool> *RS485MCGpios::Port1LedRedA() const {
  return port1LedRedA_;
}

Framework::Components::ItemIn<bool> *RS485MCGpios::Port1Signal0Loss() const {
  return Port1Signal0Loss_;
}

Framework::Components::ItemIn<bool> *RS485MCGpios::Port1Signal1Loss() const {
  return Port1Signal1Loss_;
}

Framework::Components::ItemIn<bool> *RS485MCGpios::Port1Signal2Loss() const {
  return Port1Signal2Loss_;
}

Framework::Components::ItemIn<bool> *RS485MCGpios::Port1Signal3Loss() const {
  return Port1Signal3Loss_;
}

Framework::Components::ItemOut<bool> *RS485MCGpios::Port2LedGreenA() const {
  return port2LedGreenA_;
}
Framework::Components::ItemOut<bool> *RS485MCGpios::Port2LedRedA() const {
  return port2LedRedA_;
}

Framework::Components::ItemIn<bool> *RS485MCGpios::Port2Signal0Loss() const {
  return Port2Signal0Loss_;
}

Framework::Components::ItemIn<bool> *RS485MCGpios::Port2Signal1Loss() const {
  return Port2Signal1Loss_;
}

Framework::Components::ItemIn<bool> *RS485MCGpios::Port2Signal2Loss() const {
  return Port2Signal2Loss_;
}

Framework::Components::ItemIn<bool> *RS485MCGpios::Port2Signal3Loss() const {
  return Port2Signal3Loss_;
}

Framework::Components::ItemOut<bool> *RS485MCGpios::DbgLedGreenA() const {
  return dbgLedGreenA_;
}

Framework::Components::ItemOut<bool> *RS485MCGpios::DbgLedRedA() const {
  return dbgLedRedA_;
}

}  // namespace SystemModules::Modules::RS485MC
