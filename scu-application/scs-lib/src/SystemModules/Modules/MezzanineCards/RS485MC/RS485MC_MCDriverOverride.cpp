/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Logger/ILogger.h"
#include "SystemModules/Framework/Accessors/AccessorTemplate.h"
#include "SystemModules/Framework/Components/ItemOut.h"
#include "SystemModules/Framework/Builder/Components/ItemOutBuilder.h"
#include "SystemModules/Framework/Builder/Components/ComponentBuilder.h"
#include "RS485MC_MCDriverOverride.h"

namespace SystemModules::Modules::RS485MC {

const char RS485MC_MCDriverOverride::kName[] = "SignalOverride";
const char RS485MC_MCDriverOverride::kDescription[] =
  "Allows to override individual discrete input and output signals by means of an error-flag";

const char RS485MC_MCDriverOverride::kPort1_Discrete_Signal0Name[] = "Port1_Discrete_Signal0";
const char RS485MC_MCDriverOverride::kPort1_Discrete_Signal0Description[] =
  "Override Port 1 signal 0 (discrete). False==Do not override; True==Override with 'err-flag'";
const uint32_t RS485MC_MCDriverOverride::kPort1_Discrete_Signal0Position = 0;

const char RS485MC_MCDriverOverride::kPort1_Discrete_Signal1Name[] = "Port1_Discrete_Signal1";
const char RS485MC_MCDriverOverride::kPort1_Discrete_Signal1Description[] =
  "Override Port 1 signal 1 (discrete). False==Do not override; True==Override with 'err-flag'";
const uint32_t RS485MC_MCDriverOverride::kPort1_Discrete_Signal1Position = 1;

const char RS485MC_MCDriverOverride::kPort1_Discrete_Signal2Name[] = "Port1_Discrete_Signal2";
const char RS485MC_MCDriverOverride::kPort1_Discrete_Signal2Description[] =
  "Override Port 1 signal 2 (discrete). False==Do not override; True==Override with 'err-flag'";
const uint32_t RS485MC_MCDriverOverride::kPort1_Discrete_Signal2Position = 2;

const char RS485MC_MCDriverOverride::kPort1_Discrete_Signal3Name[] = "Port1_Discrete_Signal3";
const char RS485MC_MCDriverOverride::kPort1_Discrete_Signal3Description[] =
  "Override Port 1 signal 3 (discrete). False==Do not override; True==Override with 'err-flag'";
const uint32_t RS485MC_MCDriverOverride::kPort1_Discrete_Signal3Position = 3;

const char RS485MC_MCDriverOverride::kPort2_Discrete_Signal0Name[] = "Port2_Discrete_Signal0";
const char RS485MC_MCDriverOverride::kPort2_Discrete_Signal0Description[] =
  "Override Port 2 signal 0 (discrete). False==Do not override; True==Override with 'err-flag'";
const uint32_t RS485MC_MCDriverOverride::kPort2_Discrete_Signal0Position = 4;

const char RS485MC_MCDriverOverride::kPort2_Discrete_Signal1Name[] = "Port2_Discrete_Signal1";
const char RS485MC_MCDriverOverride::kPort2_Discrete_Signal1Description[] =
  "Override Port 2 signal 1 (discrete). False==Do not override; True==Override with 'err-flag'";
const uint32_t RS485MC_MCDriverOverride::kPort2_Discrete_Signal1Position = 5;

const char RS485MC_MCDriverOverride::kPort2_Discrete_Signal2Name[] = "Port2_Discrete_Signal2";
const char RS485MC_MCDriverOverride::kPort2_Discrete_Signal2Description[] =
  "Override Port 2 signal 2 (discrete). False==Do not override; True==Override with 'err-flag'";
const uint32_t RS485MC_MCDriverOverride::kPort2_Discrete_Signal2Position = 6;

const char RS485MC_MCDriverOverride::kPort2_Discrete_Signal3Name[] = "Port2_Discrete_Signal3";
const char RS485MC_MCDriverOverride::kPort2_Discrete_Signal3Description[] =
  "Override Port 2 signal 3 (discrete). False==Do not override; True==Override with 'err-flag'";
const uint32_t RS485MC_MCDriverOverride::kPort2_Discrete_Signal3Position = 7;

RS485MC_MCDriverOverride::RS485MC_MCDriverOverride(
  Logger::ILogger *logger,
  const Modules::Configuration::ConfigurationCommon &configCommon,
  const Configuration::Configuration_Slots &config)
  : Framework::Components::Container(
  logger,
  kName,
  kDescription) {
  // Get update intervaval
  auto updateInterval = configCommon.UpdateInterval()->Fast()->Value();
  // Setup Port 1 signal 0 override input
  port1_Discrete_Signal0_ = Create().ItemOut<bool>(
    kPort1_Discrete_Signal0Name,
    kPort1_Discrete_Signal0Description,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    true,
    false).AddAndReturnPointer();
  port1_Discrete_Signal0_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ReadWriteRegister1Address()->Value(),
    kPort1_Discrete_Signal0Position,
    1);
  // Setup Port 1 signal 1 override input
  port1_Discrete_Signal1_Discrete_ = Create().ItemOut<bool>(
    kPort1_Discrete_Signal1Name,
    kPort1_Discrete_Signal1Description,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    true,
    false).AddAndReturnPointer();
  port1_Discrete_Signal1_Discrete_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ReadWriteRegister1Address()->Value(),
    kPort1_Discrete_Signal1Position,
    1);
  // Setup Port 1 signal 2 override input
  port1_Discrete_Signal2_Discrete_ = Create().ItemOut<bool>(
    kPort1_Discrete_Signal2Name,
    kPort1_Discrete_Signal2Description,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    true,
    false).AddAndReturnPointer();
  port1_Discrete_Signal2_Discrete_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ReadWriteRegister1Address()->Value(),
    kPort1_Discrete_Signal2Position,
    1);
  // Setup Port 1 signal 3 override input
  port1_Discrete_Signal3_ = Create().ItemOut<bool>(
    kPort1_Discrete_Signal3Name,
    kPort1_Discrete_Signal3Description,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    true,
    false).AddAndReturnPointer();
  port1_Discrete_Signal3_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ReadWriteRegister1Address()->Value(),
    kPort1_Discrete_Signal3Position,
    1);
  // Setup Port 2 signal 0 override input
  port2_Discrete_Signal0_ = Create().ItemOut<bool>(
    kPort2_Discrete_Signal0Name,
    kPort2_Discrete_Signal0Description,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    true,
    false).AddAndReturnPointer();
  port2_Discrete_Signal0_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ReadWriteRegister1Address()->Value(),
    kPort2_Discrete_Signal0Position,
    1);
  // Setup Port 2 signal 1 override input
  port2_Discrete_Signal1_Discrete_ = Create().ItemOut<bool>(
    kPort2_Discrete_Signal1Name,
    kPort2_Discrete_Signal1Description,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    true,
    false).AddAndReturnPointer();
  port2_Discrete_Signal1_Discrete_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ReadWriteRegister1Address()->Value(),
    kPort2_Discrete_Signal1Position,
    1);
  // Setup Port 2 signal 2 override input
  port2_Discrete_Signal2_Discrete_ = Create().ItemOut<bool>(
    kPort2_Discrete_Signal2Name,
    kPort2_Discrete_Signal2Description,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    true,
    false).AddAndReturnPointer();
  port2_Discrete_Signal2_Discrete_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ReadWriteRegister1Address()->Value(),
    kPort2_Discrete_Signal2Position,
    1);
  // Setup Port 2 signal 3 override input
  port2_Discrete_Signal3_ = Create().ItemOut<bool>(
    kPort2_Discrete_Signal3Name,
    kPort2_Discrete_Signal3Description,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    true,
    false).AddAndReturnPointer();
  port2_Discrete_Signal3_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ReadWriteRegister1Address()->Value(),
    kPort2_Discrete_Signal3Position,
    1);

  (void) config;
}

Framework::Components::ItemOut<bool> *RS485MC_MCDriverOverride::Port1_Discrete_Signal0() const {
  return port1_Discrete_Signal0_;
}

Framework::Components::ItemOut<bool> *RS485MC_MCDriverOverride::Port1_Discrete_Signal1() const {
  return port1_Discrete_Signal1_Discrete_;
}

Framework::Components::ItemOut<bool> *RS485MC_MCDriverOverride::Port1_Discrete_Signal2() const {
  return port1_Discrete_Signal2_Discrete_;
}

Framework::Components::ItemOut<bool> *RS485MC_MCDriverOverride::Port1_Discrete_Signal3() const {
  return port1_Discrete_Signal3_;
}

Framework::Components::ItemOut<bool> *RS485MC_MCDriverOverride::Port2_Discrete_Signal0() const {
  return port2_Discrete_Signal0_;
}

Framework::Components::ItemOut<bool> *RS485MC_MCDriverOverride::Port2_Discrete_Signal1() const {
  return port2_Discrete_Signal1_Discrete_;
}

Framework::Components::ItemOut<bool> *RS485MC_MCDriverOverride::Port2_Discrete_Signal2() const {
  return port2_Discrete_Signal2_Discrete_;
}

Framework::Components::ItemOut<bool> *RS485MC_MCDriverOverride::Port2_Discrete_Signal3() const {
  return port2_Discrete_Signal3_;
}

}  // namespace SystemModules::Modules::RS485MC
