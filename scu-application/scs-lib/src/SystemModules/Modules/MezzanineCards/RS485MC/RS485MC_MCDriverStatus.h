/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "SystemModules/Framework/Components/ItemIn.h"
#include "SystemModules/Framework/Components/Container.h"
#include "SystemModules/Modules/Configuration/ConfigurationCommon.h"
#include "SystemModules/Modules/Configuration/Slots/Configuration_Slots.h"

namespace SystemModules::Modules::RS485MC {
class RS485MC_MCDriverStatus : public Framework::Components::Container {
 public:
  RS485MC_MCDriverStatus(
    Logger::ILogger *logger,
    const Modules::Configuration::ConfigurationCommon &configCommon,
    const Configuration::Configuration_Slots &config);

  [[nodiscard]] Framework::Components::ItemIn<uint32_t> *CollectionOverride() const;

  [[nodiscard]] Framework::Components::ItemIn<uint32_t> *CollectionStatus1() const;

  [[nodiscard]] Framework::Components::ItemIn<uint32_t> *CollectionStatus2() const;

  [[nodiscard]] Framework::Components::ItemIn<uint32_t> *CollectionStatus3() const;

  [[nodiscard]] Framework::Components::ItemIn<uint32_t> *CollectionStatus4() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *Port1_Datalink_Signal0() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *Port1_Datalink_Signal1() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *Port1_Datalink_Signal2() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *Port1_Datalink_Signal3() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *Port2_Datalink_Signal0() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *Port2_Datalink_Signal1() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *Port2_Datalink_Signal2() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *Port2_Datalink_Signal3() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *Port1_Datalink_PBD() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *Port1_Datalink_PBM() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *Port2_Datalink_PBD() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *Port2_Datalink_PBM() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *Port1_Signal0() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *Port1_Signal1() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *Port1_Signal2() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *Port1_Signal3() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *Port2_Signal0() const;

  // ADC ready not used in software and therefore not supported.

  [[nodiscard]] Framework::Components::ItemIn<bool> *Port2_Signal1() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *Port2_Signal2() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *Port2_Signal3() const;


 private:
  Framework::Components::ItemIn<uint32_t> *collectionOverride_;

  Framework::Components::ItemIn<uint32_t> *collectionStatus1_;

  Framework::Components::ItemIn<uint32_t> *collectionStatus2_;

  Framework::Components::ItemIn<uint32_t> *collectionStatus3_;

  Framework::Components::ItemIn<uint32_t> *collectionStatus4_;

  Framework::Components::ItemIn<bool> *port1_Datalink_Signal0_;

  Framework::Components::ItemIn<bool> *port1_Datalink_Signal1_;

  Framework::Components::ItemIn<bool> *port1_Datalink_Signal2_;

  Framework::Components::ItemIn<bool> *port1_Datalink_Signal3_;

  Framework::Components::ItemIn<bool> *port2_Datalink_Signal0_;

  Framework::Components::ItemIn<bool> *port2_Datalink_Signal1_;

  Framework::Components::ItemIn<bool> *port2_Datalink_Signal2_;

  Framework::Components::ItemIn<bool> *port2_Datalink_Signal3_;

  Framework::Components::ItemIn<bool> *port1_Datalink_PBD_;

  Framework::Components::ItemIn<bool> *port1_Datalink_PBM_;

  Framework::Components::ItemIn<bool> *port2_Datalink_PBD_;

  Framework::Components::ItemIn<bool> *port2_Datalink_PBM_;

  Framework::Components::ItemIn<bool> *port1_Signal0_;

  Framework::Components::ItemIn<bool> *port1_Signal1_;

  Framework::Components::ItemIn<bool> *port1_Signal2_;

  Framework::Components::ItemIn<bool> *port1_Signal3_;

  Framework::Components::ItemIn<bool> *port2_Signal0_;

  Framework::Components::ItemIn<bool> *port2_Signal1_;

  Framework::Components::ItemIn<bool> *port2_Signal2_;

  Framework::Components::ItemIn<bool> *port2_Signal3_;

  static const char kName[];
  static const char kDescription[];

  static const char kCollectionOverrideName[];
  static const char kCollectionOverrideDescription[];
  static const uint32_t kCollectionOverridePosition;

  static const char kCollectionStatus1Name[];
  static const char kCollectionStatus1Description[];
  static const uint32_t kCollectionStatus1Position;

  static const char kCollectionStatus2Name[];
  static const char kCollectionStatus2Description[];
  static const uint32_t kCollectionStatus2Position;

  static const char kCollectionStatus3Name[];
  static const char kCollectionStatus3Description[];
  static const uint32_t kCollectionStatus3Position;

  static const char kCollectionStatus4Name[];
  static const char kCollectionStatus4Description[];
  static const uint32_t kCollectionStatus4Position;

  static const char kPort1_Datalink_Signal0Name[];
  static const char kPort1_Datalink_Signal0Description[];
  static const uint32_t kPort1_Datalink_Signal0Position;

  static const char kPort1_Datalink_Signal1Name[];
  static const char kPort1_Datalink_Signal1Description[];
  static const uint32_t kPort1_Datalink_Signal1Position;

  static const char kPort1_Datalink_Signal2Name[];
  static const char kPort1_Datalink_Signal2Description[];
  static const uint32_t kPort1_Datalink_Signal2Position;

  static const char kPort1_Datalink_Signal3Name[];
  static const char kPort1_Datalink_Signal3Description[];
  static const uint32_t kPort1_Datalink_Signal3Position;

  static const char kPort2_Datalink_Signal0Name[];
  static const char kPort2_Datalink_Signal0Description[];
  static const uint32_t kPort2_Datalink_Signal0Position;

  static const char kPort2_Datalink_Signal1Name[];
  static const char kPort2_Datalink_Signal1Description[];
  static const uint32_t kPort2_Datalink_Signal1Position;

  static const char kPort2_Datalink_Signal2Name[];
  static const char kPort2_Datalink_Signal2Description[];
  static const uint32_t kPort2_Datalink_Signal2Position;

  static const char kPort2_Datalink_Signal3Name[];
  static const char kPort2_Datalink_Signal3Description[];
  static const uint32_t kPort2_Datalink_Signal3Position;

  static const char kPort1_Datalink_PBDName[];
  static const char kPort1_Datalink_PBDDescription[];
  static const uint32_t kPort1_Datalink_PBDPosition;

  static const char kPort1_Datalink_PBMName[];
  static const char kPort1_Datalink_PBMDescription[];
  static const uint32_t kPort1_Datalink_PBMPosition;

  static const char kPort2_Datalink_PBDName[];
  static const char kPort2_Datalink_PBDDescription[];
  static const uint32_t kPort2_Datalink_PBDPosition;

  static const char kPort2_Datalink_PBMName[];
  static const char kPort2_Datalink_PBMDescription[];
  static const uint32_t kPort2_Datalink_PBMPosition;

  static const char kPort1_Signal0Name[];
  static const char kPort1_Signal0Description[];
  static const uint32_t kPort1_Signal0Position;

  static const char kPort1_Signal1Name[];
  static const char kPort1_Signal1Description[];
  static const uint32_t kPort1_Signal1Position;

  static const char kPort1_Signal2Name[];
  static const char kPort1_Signal2Description[];
  static const uint32_t kPort1_Signal2Position;

  static const char kPort1_Signal3Name[];
  static const char kPort1_Signal3Description[];
  static const uint32_t kPort1_Signal3Position;

  static const char kPort2_Signal0Name[];
  static const char kPort2_Signal0Description[];
  static const uint32_t kPort2_Signal0Position;

  static const char kPort2_Signal1Name[];
  static const char kPort2_Signal1Description[];
  static const uint32_t kPort2_Signal1Position;

  static const char kPort2_Signal2Name[];
  static const char kPort2_Signal2Description[];
  static const uint32_t kPort2_Signal2Position;

  static const char kPort2_Signal3Name[];
  static const char kPort2_Signal3Description[];
  static const uint32_t kPort2_Signal3Position;
};
}  // namespace SystemModules::Modules::RS485MC
