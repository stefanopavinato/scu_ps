/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Logger/ILogger.h"
#include "SystemModules/Framework/Components/ItemIn.h"
#include "SystemModules/Framework/Builder/Accessors/ReadAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemInBuilder.h"
#include "SystemModules/Framework/Builder/Components/ComponentBuilder.h"
#include "RS485MC_MCDriverStatus.h"

namespace SystemModules::Modules::RS485MC {

const char RS485MC_MCDriverStatus::kName[] = "Status";
const char RS485MC_MCDriverStatus::kDescription[] =
  "Status of signals the Serializer receives/generates from/towards the mezzanine card. (Raw values)";

const char RS485MC_MCDriverStatus::kCollectionOverrideName[] = "CollectionOverride";
const char RS485MC_MCDriverStatus::kCollectionOverrideDescription[] =
  "CollectionOverride of RS485MC status items related to signals received from the MC.";
const uint32_t RS485MC_MCDriverStatus::kCollectionOverridePosition = 0;

const char RS485MC_MCDriverStatus::kCollectionStatus1Name[] = "CollectionStatus1";
const char RS485MC_MCDriverStatus::kCollectionStatus1Description[] =
  "Collection1 of RS485 status items related to signal driven towards the MC.";
const uint32_t RS485MC_MCDriverStatus::kCollectionStatus1Position = 0;

const char RS485MC_MCDriverStatus::kCollectionStatus2Name[] = "CollectionStatus2";
const char RS485MC_MCDriverStatus::kCollectionStatus2Description[] =
  "Collection2 of RS485 status items related to signal driven towards the MC.";
const uint32_t RS485MC_MCDriverStatus::kCollectionStatus2Position = 0;

const char RS485MC_MCDriverStatus::kCollectionStatus3Name[] = "CollectionStatus3";
const char RS485MC_MCDriverStatus::kCollectionStatus3Description[] =
  "Collection3 of RS485 status items related to signal driven towards the MC.";
const uint32_t RS485MC_MCDriverStatus::kCollectionStatus3Position = 0;

const char RS485MC_MCDriverStatus::kCollectionStatus4Name[] = "CollectionStatus4";
const char RS485MC_MCDriverStatus::kCollectionStatus4Description[] =
  "Collection4 of RS485 status items related to signal driven towards the MC.";
const uint32_t RS485MC_MCDriverStatus::kCollectionStatus4Position = 0;

const char RS485MC_MCDriverStatus::kPort1_Datalink_Signal0Name[] = "Port1_Datalink_Signal0";
const char RS485MC_MCDriverStatus::kPort1_Datalink_Signal0Description[] =
  "Override Port 1 signal 0 (Datalink). False==Do not override; True==Override with 'err-flag'";
const uint32_t RS485MC_MCDriverStatus::kPort1_Datalink_Signal0Position = 8;

const char RS485MC_MCDriverStatus::kPort1_Datalink_Signal1Name[] = "Port1_Datalink_Signal1";
const char RS485MC_MCDriverStatus::kPort1_Datalink_Signal1Description[] =
  "Override Port 1 signal 1 (Datalink). False==Do not override; True==Override with 'err-flag'";
const uint32_t RS485MC_MCDriverStatus::kPort1_Datalink_Signal1Position = 9;

const char RS485MC_MCDriverStatus::kPort1_Datalink_Signal2Name[] = "Port1_Datalink_Signal2";
const char RS485MC_MCDriverStatus::kPort1_Datalink_Signal2Description[] =
  "Override Port 1 signal 2 (Datalink). False==Do not override; True==Override with 'err-flag'";
const uint32_t RS485MC_MCDriverStatus::kPort1_Datalink_Signal2Position = 10;

const char RS485MC_MCDriverStatus::kPort1_Datalink_Signal3Name[] = "Port1_Datalink_Signal3";
const char RS485MC_MCDriverStatus::kPort1_Datalink_Signal3Description[] =
  "Override Port 1 signal 3 (Datalink). False==Do not override; True==Override with 'err-flag'";
const uint32_t RS485MC_MCDriverStatus::kPort1_Datalink_Signal3Position = 11;

const char RS485MC_MCDriverStatus::kPort2_Datalink_Signal0Name[] = "Port2_Datalink_Signal0";
const char RS485MC_MCDriverStatus::kPort2_Datalink_Signal0Description[] =
  "Override Port 2 signal 0 (Datalink). False==Do not override; True==Override with 'err-flag'";
const uint32_t RS485MC_MCDriverStatus::kPort2_Datalink_Signal0Position = 12;

const char RS485MC_MCDriverStatus::kPort2_Datalink_Signal1Name[] = "Port2_Datalink_Signal1";
const char RS485MC_MCDriverStatus::kPort2_Datalink_Signal1Description[] =
  "Override Port 2 signal 1 (Datalink). False==Do not override; True==Override with 'err-flag'";
const uint32_t RS485MC_MCDriverStatus::kPort2_Datalink_Signal1Position = 13;

const char RS485MC_MCDriverStatus::kPort2_Datalink_Signal2Name[] = "Port2_Datalink_Signal2";
const char RS485MC_MCDriverStatus::kPort2_Datalink_Signal2Description[] =
  "Override Port 2 signal 2 (Datalink). False==Do not override; True==Override with 'err-flag'";
const uint32_t RS485MC_MCDriverStatus::kPort2_Datalink_Signal2Position = 14;

const char RS485MC_MCDriverStatus::kPort2_Datalink_Signal3Name[] = "Port2_Datalink_Signal3";
const char RS485MC_MCDriverStatus::kPort2_Datalink_Signal3Description[] =
  "Override Port 2 signal 3 (Datalink). False==Do not override; True==Override with 'err-flag'";
const uint32_t RS485MC_MCDriverStatus::kPort2_Datalink_Signal3Position = 15;

const char RS485MC_MCDriverStatus::kPort1_Datalink_PBDName[] = "Port1_Datalink_PBD";
const char RS485MC_MCDriverStatus::kPort1_Datalink_PBDDescription[] =
  "Override Port 1 PBD (Datalink). False==Do not override; True==Override with 'err-flag'";
const uint32_t RS485MC_MCDriverStatus::kPort1_Datalink_PBDPosition = 16;

const char RS485MC_MCDriverStatus::kPort1_Datalink_PBMName[] = "Port1_Datalink_PBM";
const char RS485MC_MCDriverStatus::kPort1_Datalink_PBMDescription[] =
  "Override Port 1 PBM (Datalink). False==Do not override; True==Override with 'err-flag'";
const uint32_t RS485MC_MCDriverStatus::kPort1_Datalink_PBMPosition = 17;

const char RS485MC_MCDriverStatus::kPort2_Datalink_PBDName[] = "Port2_Datalink_PBD";
const char RS485MC_MCDriverStatus::kPort2_Datalink_PBDDescription[] =
  "Override Port 2 PBD (Datalink). False==Do not override; True==Override with 'err-flag'";
const uint32_t RS485MC_MCDriverStatus::kPort2_Datalink_PBDPosition = 16;

const char RS485MC_MCDriverStatus::kPort2_Datalink_PBMName[] = "Port2_Datalink_PBM";
const char RS485MC_MCDriverStatus::kPort2_Datalink_PBMDescription[] =
  "Override Port 2 PBM (Datalink). False==Do not override; True==Override with 'err-flag'";
const uint32_t RS485MC_MCDriverStatus::kPort2_Datalink_PBMPosition = 17;

const char RS485MC_MCDriverStatus::kPort1_Signal0Name[] = "Port1_Signal0";
const char RS485MC_MCDriverStatus::kPort1_Signal0Description[] =
  "Status of the discrete Port1_Signal0 as received from the MC";
const uint32_t RS485MC_MCDriverStatus::kPort1_Signal0Position = 0;

const char RS485MC_MCDriverStatus::kPort1_Signal1Name[] = "Port1_Signal1";
const char RS485MC_MCDriverStatus::kPort1_Signal1Description[] =
  "Status of the discrete Port1_Signal1 as received from the MC";
const uint32_t RS485MC_MCDriverStatus::kPort1_Signal1Position = 1;

const char RS485MC_MCDriverStatus::kPort1_Signal2Name[] = "Port1_Signal2";
const char RS485MC_MCDriverStatus::kPort1_Signal2Description[] =
  "Status of the discrete Port1_Signal2 as received from the MC";
const uint32_t RS485MC_MCDriverStatus::kPort1_Signal2Position = 2;

const char RS485MC_MCDriverStatus::kPort1_Signal3Name[] = "Port1_Signal3";
const char RS485MC_MCDriverStatus::kPort1_Signal3Description[] =
  "Status of the discrete Port1_Signal3 as received from the MC";
const uint32_t RS485MC_MCDriverStatus::kPort1_Signal3Position = 3;

const char RS485MC_MCDriverStatus::kPort2_Signal0Name[] = "Port2_Signal0";
const char RS485MC_MCDriverStatus::kPort2_Signal0Description[] =
  "Status of the discrete Port2_Signal0 as received from the MC";
const uint32_t RS485MC_MCDriverStatus::kPort2_Signal0Position = 0;

const char RS485MC_MCDriverStatus::kPort2_Signal1Name[] = "Port2_Signal1";
const char RS485MC_MCDriverStatus::kPort2_Signal1Description[] =
  "Status of the discrete Port2_Signal1 as received from the MC";
const uint32_t RS485MC_MCDriverStatus::kPort2_Signal1Position = 1;

const char RS485MC_MCDriverStatus::kPort2_Signal2Name[] = "Port2_Signal2";
const char RS485MC_MCDriverStatus::kPort2_Signal2Description[] =
  "Status of the discrete Port2_Signal2 as received from the MC";
const uint32_t RS485MC_MCDriverStatus::kPort2_Signal2Position = 2;

const char RS485MC_MCDriverStatus::kPort2_Signal3Name[] = "Port2_Signal3";
const char RS485MC_MCDriverStatus::kPort2_Signal3Description[] =
  "Status of the discrete Port2_Signal3 as received from the MC";
const uint32_t RS485MC_MCDriverStatus::kPort2_Signal3Position = 3;

RS485MC_MCDriverStatus::RS485MC_MCDriverStatus(
  Logger::ILogger *logger,
  const Modules::Configuration::ConfigurationCommon &configCommon,
  const Configuration::Configuration_Slots &config)
  : Framework::Components::Container(
  logger,
  kName,
  kDescription) {
  // Get update intervaval
  auto updateIntervalDiscreteSignals = configCommon.UpdateInterval()->Medium()->Value();
  // auto updateIntervalDatalink = configCommon.UpdateInterval()->Medium()->Value();
  // todo: datalink items yet to be implemented (future extension)
  // Setup item collection
  collectionOverride_ = Create().ItemIn<uint32_t>(
    kCollectionOverrideName,
    kCollectionOverrideDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDiscreteSignals),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  collectionOverride_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ReadWriteRegister1Address()->Value(),
    kCollectionOverridePosition,
    32);
  collectionStatus1_ = Create().ItemIn<uint32_t>(
    kCollectionStatus1Name,
    kCollectionStatus1Description,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDiscreteSignals),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  collectionStatus1_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister1Address()->Value(),
    kCollectionStatus1Position,
    32);
  collectionStatus2_ = Create().ItemIn<uint32_t>(
    kCollectionStatus2Name,
    kCollectionStatus2Description,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDiscreteSignals),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  collectionStatus2_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister2Address()->Value(),
    kCollectionStatus2Position,
    32);
  collectionStatus3_ = Create().ItemIn<uint32_t>(
    kCollectionStatus3Name,
    kCollectionStatus3Description,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDiscreteSignals),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  collectionStatus3_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister3Address()->Value(),
    kCollectionStatus3Position,
    32);
  collectionStatus4_ = Create().ItemIn<uint32_t>(
    kCollectionStatus4Name,
    kCollectionStatus4Description,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDiscreteSignals),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  collectionStatus4_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister4Address()->Value(),
    kCollectionStatus4Position,
    32);
  // Setup Port 1 signal 0 override input
/*  port1_Datalink_Signal0_ = Create().ItemIn<bool>(
      kPort1_Datalink_Signal0Name,
      kPort1_Datalink_Signal0Description,
      Types::ValueGroup::Enum::NONE,
      std::chrono::milliseconds(kStatusUpdateRate)).AddAndReturnPointer();
  port1_Datalink_Signal0_->CreateAccessor().ScuRegister(
      config.Scu()->ReadWriteRegister1Address()->GetValue(),
      kPort1_Datalink_Signal0Position,
      1);
  // Setup Port 1 signal 1 override input
  port1_Datalink_Signal1_ = Create().ItemIn<bool>(
      kPort1_Datalink_Signal1Name,
      kPort1_Datalink_Signal1Description,
      Types::ValueGroup::Enum::NONE,
      std::chrono::milliseconds(kStatusUpdateRate)).AddAndReturnPointer();
  port1_Datalink_Signal1_->CreateAccessor().ScuRegister(
      config.Scu()->ReadWriteRegister1Address()->GetValue(),
      kPort1_Datalink_Signal1Position,
      1);
  // Setup Port 1 signal 2 override input
  port1_Datalink_Signal2_ = Create().ItemIn<bool>(
      kPort1_Datalink_Signal2Name,
      kPort1_Datalink_Signal2Description,
      Types::ValueGroup::Enum::NONE,
      std::chrono::milliseconds(kStatusUpdateRate)).AddAndReturnPointer();
  port1_Datalink_Signal2_->CreateAccessor().ScuRegister(
      config.Scu()->ReadWriteRegister1Address()->GetValue(),
      kPort1_Datalink_Signal2Position,
      1);
  // Setup Port 1 signal 3 override input
  port1_Datalink_Signal3_ = Create().ItemIn<bool>(
      kPort1_Datalink_Signal3Name,
      kPort1_Datalink_Signal3Description,
      Types::ValueGroup::Enum::NONE,
      std::chrono::milliseconds(kStatusUpdateRate)).AddAndReturnPointer();
  port1_Datalink_Signal3_->CreateAccessor().ScuRegister(
      config.Scu()->ReadWriteRegister1Address()->GetValue(),
      kPort1_Datalink_Signal3Position,
      1);
  // Setup Port 2 signal 0 override input
  port2_Datalink_Signal0_ = Create().ItemIn<bool>(
      kPort2_Datalink_Signal0Name,
      kPort2_Datalink_Signal0Description,
      Types::ValueGroup::Enum::NONE,
      std::chrono::milliseconds(kStatusUpdateRate)).AddAndReturnPointer();
  port2_Datalink_Signal0_->CreateAccessor().ScuRegister(
      config.Scu()->ReadWriteRegister1Address()->GetValue(),
      kPort2_Datalink_Signal0Position,
      1);
  // Setup Port 2 signal 1 override input
  port2_Datalink_Signal1_ = Create().ItemIn<bool>(
      kPort2_Datalink_Signal1Name,
      kPort2_Datalink_Signal1Description,
      Types::ValueGroup::Enum::NONE,
      std::chrono::milliseconds(kStatusUpdateRate)).AddAndReturnPointer();
  port2_Datalink_Signal1_->CreateAccessor().ScuRegister(
      config.Scu()->ReadWriteRegister1Address()->GetValue(),
      kPort2_Datalink_Signal1Position,
      1);
  // Setup Port 2 signal 2 override input
  port2_Datalink_Signal2_ = Create().ItemIn<bool>(
      kPort2_Datalink_Signal2Name,
      kPort2_Datalink_Signal2Description,
      Types::ValueGroup::Enum::NONE,
      std::chrono::milliseconds(kStatusUpdateRate)).AddAndReturnPointer();
  port2_Datalink_Signal2_->CreateAccessor().ScuRegister(
      config.Scu()->ReadWriteRegister1Address()->GetValue(),
      kPort2_Datalink_Signal2Position,
      1);
  // Setup Port 2 signal 3 override input
  port2_Datalink_Signal3_ = Create().ItemIn<bool>(
      kPort2_Datalink_Signal3Name,
      kPort2_Datalink_Signal3Description,
      Types::ValueGroup::Enum::NONE,
      std::chrono::milliseconds(kStatusUpdateRate)).AddAndReturnPointer();
  port2_Datalink_Signal3_->CreateAccessor().ScuRegister(
      config.Scu()->ReadWriteRegister1Address()->GetValue(),
      kPort2_Datalink_Signal3Position,
      1);
  // Setup Port 1 PBD (Datalink) override input
  port1_Datalink_PBD_ = Create().ItemIn<bool>(
      kPort1_Datalink_PBDName,
      kPort1_Datalink_PBDDescription,
      Types::ValueGroup::Enum::NONE,
      std::chrono::milliseconds(kStatusUpdateRate)).AddAndReturnPointer();
  port1_Datalink_PBD_->CreateAccessor().ScuRegister(
      config.Scu()->ReadWriteRegister1Address()->GetValue(),
      kPort1_Datalink_PBDPosition,
      1);
  // Setup Port 1 PBM (Datalink) override input
  port1_Datalink_PBM_ = Create().ItemIn<bool>(
      kPort1_Datalink_PBMName,
      kPort1_Datalink_PBMDescription,
      Types::ValueGroup::Enum::NONE,
      std::chrono::milliseconds(kStatusUpdateRate)).AddAndReturnPointer();
  port1_Datalink_PBM_->CreateAccessor().ScuRegister(
      config.Scu()->ReadWriteRegister1Address()->GetValue(),
      kPort1_Datalink_PBMPosition,
      1);
  // Setup Port 2 PBD (Datalink) override input
  port2_Datalink_PBD_ = Create().ItemIn<bool>(
      kPort2_Datalink_PBDName,
      kPort2_Datalink_PBDDescription,
      Types::ValueGroup::Enum::NONE,
      std::chrono::milliseconds(kStatusUpdateRate)).AddAndReturnPointer();
  port2_Datalink_PBD_->CreateAccessor().ScuRegister(
      config.Scu()->ReadWriteRegister1Address()->GetValue(),
      kPort2_Datalink_PBDPosition,
      1);
  // Setup Port 2 PBM (Datalink) override input
  port2_Datalink_PBM_ = Create().ItemIn<bool>(
      kPort2_Datalink_PBMName,
      kPort2_Datalink_PBMDescription,
      Types::ValueGroup::Enum::NONE,
      std::chrono::milliseconds(kStatusUpdateRate)).AddAndReturnPointer();
  port2_Datalink_PBM_->CreateAccessor().ScuRegister(
      config.Scu()->ReadWriteRegister1Address()->GetValue(),
      kPort2_Datalink_PBMPosition,
      1);
  // Setup Port1_Signal0 status input
  port1_Signal0_ = Create().ItemIn<bool>(
      kPort1_Signal0Name,
      kPort1_Signal0Description,
      Types::ValueGroup::Enum::NONE,
      std::chrono::milliseconds(kStatusUpdateRate)).AddAndReturnPointer();
  port1_Signal0_->CreateAccessor().ScuRegister(
      config.Scu()->StatusRegister1Address()->GetValue(),
      kPort1_Signal0Position,
      1);
  // Setup Port1_Signal1 status input
  port1_Signal1_ = Create().ItemIn<bool>(
      kPort1_Signal1Name,
      kPort1_Signal1Description,
      Types::ValueGroup::Enum::NONE,
      std::chrono::milliseconds(kStatusUpdateRate)).AddAndReturnPointer();
  port1_Signal1_->CreateAccessor().ScuRegister(
      config.Scu()->StatusRegister1Address()->GetValue(),
      kPort1_Signal1Position,
      1);
  // Setup Port1_Signal2 status input
  port1_Signal2_ = Create().ItemIn<bool>(
      kPort1_Signal2Name,
      kPort1_Signal2Description,
      Types::ValueGroup::Enum::NONE,
      std::chrono::milliseconds(kStatusUpdateRate)).AddAndReturnPointer();
  port1_Signal2_->CreateAccessor().ScuRegister(
      config.Scu()->StatusRegister1Address()->GetValue(),
      kPort1_Signal2Position,
      1);
  // Setup Port1_Signal3 status input
  port1_Signal3_ = Create().ItemIn<bool>(
      kPort1_Signal3Name,
      kPort1_Signal3Description,
      Types::ValueGroup::Enum::NONE,
      std::chrono::milliseconds(kStatusUpdateRate)).AddAndReturnPointer();
  port1_Signal3_->CreateAccessor().ScuRegister(
      config.Scu()->StatusRegister1Address()->GetValue(),
      kPort1_Signal3Position,
      1);
  // Setup Port2_Signal0 status input
  port2_Signal0_ = Create().ItemIn<bool>(
      kPort2_Signal0Name,
      kPort2_Signal0Description,
      Types::ValueGroup::Enum::NONE,
      std::chrono::milliseconds(kStatusUpdateRate)).AddAndReturnPointer();
  port2_Signal0_->CreateAccessor().ScuRegister(
      config.Scu()->StatusRegister2Address()->GetValue(),
      kPort2_Signal0Position,
      1);
  // Setup Port2_Signal1 status input
  port2_Signal1_ = Create().ItemIn<bool>(
      kPort2_Signal1Name,
      kPort2_Signal1Description,
      Types::ValueGroup::Enum::NONE,
      std::chrono::milliseconds(kStatusUpdateRate)).AddAndReturnPointer();
  port2_Signal1_->CreateAccessor().ScuRegister(
      config.Scu()->StatusRegister2Address()->GetValue(),
      kPort2_Signal1Position,
      1);
  // Setup Port2_Signal2 status input
  port2_Signal2_ = Create().ItemIn<bool>(
      kPort2_Signal2Name,
      kPort2_Signal2Description,
      Types::ValueGroup::Enum::NONE,
      std::chrono::milliseconds(kStatusUpdateRate)).AddAndReturnPointer();
  port2_Signal2_->CreateAccessor().ScuRegister(
      config.Scu()->StatusRegister2Address()->GetValue(),
      kPort2_Signal2Position,
      1);
  // Setup Port2_Signal3 status input
  port2_Signal3_ = Create().ItemIn<bool>(
      kPort2_Signal3Name,
      kPort2_Signal3Description,
      Types::ValueGroup::Enum::NONE,
      std::chrono::milliseconds(kStatusUpdateRate)).AddAndReturnPointer();
  port2_Signal3_->CreateAccessor().ScuRegister(
      config.Scu()->StatusRegister2Address()->GetValue(),
      kPort2_Signal3Position,
      1);
      */
}

Framework::Components::ItemIn<uint32_t> *RS485MC_MCDriverStatus::CollectionOverride() const {
  return collectionOverride_;
}

Framework::Components::ItemIn<uint32_t> *RS485MC_MCDriverStatus::CollectionStatus1() const {
  return collectionStatus1_;
}

Framework::Components::ItemIn<uint32_t> *RS485MC_MCDriverStatus::CollectionStatus2() const {
  return collectionStatus2_;
}

Framework::Components::ItemIn<uint32_t> *RS485MC_MCDriverStatus::CollectionStatus3() const {
  return collectionStatus3_;
}

Framework::Components::ItemIn<uint32_t> *RS485MC_MCDriverStatus::CollectionStatus4() const {
  return collectionStatus4_;
}

Framework::Components::ItemIn<bool> *RS485MC_MCDriverStatus::Port1_Datalink_Signal0() const {
  return port1_Datalink_Signal0_;
}

Framework::Components::ItemIn<bool> *RS485MC_MCDriverStatus::Port1_Datalink_Signal1() const {
  return port1_Datalink_Signal1_;
}

Framework::Components::ItemIn<bool> *RS485MC_MCDriverStatus::Port1_Datalink_Signal2() const {
  return port1_Datalink_Signal2_;
}

Framework::Components::ItemIn<bool> *RS485MC_MCDriverStatus::Port1_Datalink_Signal3() const {
  return port1_Datalink_Signal3_;
}

Framework::Components::ItemIn<bool> *RS485MC_MCDriverStatus::Port2_Datalink_Signal0() const {
  return port2_Datalink_Signal0_;
}

Framework::Components::ItemIn<bool> *RS485MC_MCDriverStatus::Port2_Datalink_Signal1() const {
  return port2_Datalink_Signal1_;
}

Framework::Components::ItemIn<bool> *RS485MC_MCDriverStatus::Port2_Datalink_Signal2() const {
  return port2_Datalink_Signal2_;
}

Framework::Components::ItemIn<bool> *RS485MC_MCDriverStatus::Port2_Datalink_Signal3() const {
  return port2_Datalink_Signal3_;
}

Framework::Components::ItemIn<bool> *RS485MC_MCDriverStatus::Port1_Datalink_PBD() const {
  return port1_Datalink_PBD_;
}

Framework::Components::ItemIn<bool> *RS485MC_MCDriverStatus::Port1_Datalink_PBM() const {
  return port1_Datalink_PBM_;
}

Framework::Components::ItemIn<bool> *RS485MC_MCDriverStatus::Port2_Datalink_PBD() const {
  return port2_Datalink_PBD_;
}

Framework::Components::ItemIn<bool> *RS485MC_MCDriverStatus::Port2_Datalink_PBM() const {
  return port2_Datalink_PBM_;
}

Framework::Components::ItemIn<bool> *RS485MC_MCDriverStatus::Port1_Signal0() const {
  return port1_Signal0_;
}

Framework::Components::ItemIn<bool> *RS485MC_MCDriverStatus::Port1_Signal1() const {
  return port1_Signal1_;
}

Framework::Components::ItemIn<bool> *RS485MC_MCDriverStatus::Port1_Signal2() const {
  return port1_Signal2_;
}

Framework::Components::ItemIn<bool> *RS485MC_MCDriverStatus::Port1_Signal3() const {
  return port1_Signal3_;
}

Framework::Components::ItemIn<bool> *RS485MC_MCDriverStatus::Port2_Signal0() const {
  return port2_Signal0_;
}

Framework::Components::ItemIn<bool> *RS485MC_MCDriverStatus::Port2_Signal1() const {
  return port2_Signal1_;
}

Framework::Components::ItemIn<bool> *RS485MC_MCDriverStatus::Port2_Signal2() const {
  return port2_Signal2_;
}

Framework::Components::ItemIn<bool> *RS485MC_MCDriverStatus::Port2_Signal3() const {
  return port2_Signal3_;
}

}  // namespace SystemModules::Modules::RS485MC
