/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <cstdint>
#include "Types/Value.h"
#include "SystemModules/Framework/Components/ItemIn.h"
#include "SystemModules/Framework/Components/ParameterItemIn.h"
#include "SystemModules/Framework/Components/I2CDevicePCA9539.h"
#include "SystemModules/Framework/Components/Container.h"
#include "SystemModules/Modules/Configuration/ConfigurationCommon.h"

namespace SystemModules::Modules::RS485MC {
class RS485MCGpios : public Framework::Components::I2CDevicePCA9539 {
 public:
  RS485MCGpios(
    Logger::ILogger *logger,
    const Modules::Configuration::ConfigurationCommon &configCommon,
    const std::string &path,
    const std::string &devicePath,
    const uint32_t &slot);

  [[nodiscard]] Framework::Components::ItemIn<bool> *PowerGood() const;

  [[nodiscard]] Framework::Components::ItemOut<bool> *EthSoftReset() const;

  [[nodiscard]] Framework::Components::ItemOut<bool> *Port1LedGreenA() const;
  [[nodiscard]] Framework::Components::ItemOut<bool> *Port1LedRedA() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *Port1Signal0Loss() const;
  [[nodiscard]] Framework::Components::ItemIn<bool> *Port1Signal1Loss() const;
  [[nodiscard]] Framework::Components::ItemIn<bool> *Port1Signal2Loss() const;
  [[nodiscard]] Framework::Components::ItemIn<bool> *Port1Signal3Loss() const;

  [[nodiscard]] Framework::Components::ItemOut<bool> *Port2LedGreenA() const;
  [[nodiscard]] Framework::Components::ItemOut<bool> *Port2LedRedA() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *Port2Signal0Loss() const;
  [[nodiscard]] Framework::Components::ItemIn<bool> *Port2Signal1Loss() const;
  [[nodiscard]] Framework::Components::ItemIn<bool> *Port2Signal2Loss() const;
  [[nodiscard]] Framework::Components::ItemIn<bool> *Port2Signal3Loss() const;

  [[nodiscard]] Framework::Components::ItemOut<bool> *DbgLedGreenA() const;
  [[nodiscard]] Framework::Components::ItemOut<bool> *DbgLedRedA() const;

 private:
  Framework::Components::ItemIn<bool> *powerGood_;

  Framework::Components::ItemOut<bool> *ethSoftReset_;

  Framework::Components::ItemOut<bool> *port1LedGreenA_;
  Framework::Components::ItemOut<bool> *port1LedRedA_;

  Framework::Components::ItemIn<bool> *Port1Signal0Loss_;
  Framework::Components::ItemIn<bool> *Port1Signal1Loss_;
  Framework::Components::ItemIn<bool> *Port1Signal2Loss_;
  Framework::Components::ItemIn<bool> *Port1Signal3Loss_;

  Framework::Components::ItemOut<bool> *port2LedGreenA_;
  Framework::Components::ItemOut<bool> *port2LedRedA_;

  Framework::Components::ItemIn<bool> *Port2Signal0Loss_;
  Framework::Components::ItemIn<bool> *Port2Signal1Loss_;
  Framework::Components::ItemIn<bool> *Port2Signal2Loss_;
  Framework::Components::ItemIn<bool> *Port2Signal3Loss_;

  Framework::Components::ItemOut<bool> *dbgLedGreenA_;
  Framework::Components::ItemOut<bool> *dbgLedRedA_;

  static const char kName[];
  static const char kDescription[];
  static const uint16_t kAddress;

  static const char kEthSoftResetName[];
  static const char kEthSoftResetDescription[];
  static const uint32_t kEthSoftResetAddressOffset;

  static const char kPowerGoodName[];
  static const char kPowerGoodDescription[];
  static const uint32_t kPowerGoodAddressOffset;

  static const char kPort1LedGreenAName[];
  static const char kPort1LedGreenADescription[];
  static const uint32_t kPort1LedGreenAAddressOffset;

  static const char kPort1LedRedAName[];
  static const char kPort1LedRedADescription[];
  static const uint32_t kPort1LedRedAAddressOffset;

  static const char kPort1Signal0LossName[];
  static const char kPort1Signal0LossDescription[];
  static const uint32_t kPort1Signal0LossAddressOffset;

  static const char kPort1Signal1LossName[];
  static const char kPort1Signal1LossDescription[];
  static const uint32_t kPort1Signal1LossAddressOffset;

  static const char kPort1Signal2LossName[];
  static const char kPort1Signal2LossDescription[];
  static const uint32_t kPort1Signal2LossAddressOffset;

  static const char kPort1Signal3LossName[];
  static const char kPort1Signal3LossDescription[];
  static const uint32_t kPort1Signal3LossAddressOffset;

  static const char kPort2LedGreenAName[];
  static const char kPort2LedGreenADescription[];
  static const uint32_t kPort2LedGreenAAddressOffset;

  static const char kPort2LedRedAName[];
  static const char kPort2LedRedADescription[];
  static const uint32_t kPort2LedRedAAddressOffset;

  static const char kPort2Signal0LossName[];
  static const char kPort2Signal0LossDescription[];
  static const uint32_t kPort2Signal0LossAddressOffset;

  static const char kPort2Signal1LossName[];
  static const char kPort2Signal1LossDescription[];
  static const uint32_t kPort2Signal1LossAddressOffset;

  static const char kPort2Signal2LossName[];
  static const char kPort2Signal2LossDescription[];
  static const uint32_t kPort2Signal2LossAddressOffset;

  static const char kPort2Signal3LossName[];
  static const char kPort2Signal3LossDescription[];
  static const uint32_t kPort2Signal3LossAddressOffset;

  static const char kDbgLedGreenAName[];
  static const char kDbgLedGreenADescription[];
  static const uint32_t kDbgLedGreenAAddressOffset;

  static const char kDbgLedRedAName[];
  static const char kDbgLedRedADescription[];
  static const uint32_t kDbgLedRedAAddressOffset;
};

}  // namespace SystemModules::Modules::RS485MC
