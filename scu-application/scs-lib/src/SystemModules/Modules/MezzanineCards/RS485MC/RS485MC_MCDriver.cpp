/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Logger/ILogger.h"
#include "SystemModules/Framework/Components/ItemOut.h"
#include "SystemModules/Framework/Builder/Components/ComponentBuilder.h"
#include "RS485MC_MCDriverOverride.h"
#include "RS485MC_MCDriverStatus.h"
#include "RS485MC_MCDriver.h"

namespace SystemModules::Modules::RS485MC {

RS485MC_MCDriver::RS485MC_MCDriver(
  Logger::ILogger *logger,
  const Modules::Configuration::ConfigurationCommon &configCommon,
  const Configuration::Configuration_Slots &config)
  : Framework::Components::Container(
  logger,
  "MCDriver",
  "RS485MC MC_Driver") {
  // Set override container
  auto override = std::make_unique<RS485MC_MCDriverOverride>(
    logger,
    configCommon,
    config);
  override_ = override.get();
  AddComponent(std::move(override));
  // Set status container
  auto status = std::make_unique<RS485MC_MCDriverStatus>(
    logger,
    configCommon,
    config);
  status_ = status.get();
  AddComponent(std::move(status));

  (void) config;
}

RS485MC_MCDriverStatus *RS485MC_MCDriver::Status() const {
  return status_;
}

RS485MC_MCDriverOverride *RS485MC_MCDriver::Override() const {
  return override_;
}

}  // namespace SystemModules::Modules::RS485MC
