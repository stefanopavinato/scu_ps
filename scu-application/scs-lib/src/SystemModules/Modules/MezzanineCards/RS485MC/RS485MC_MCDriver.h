/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <cstdint>
#include "SystemModules/Modules/Configuration/ConfigurationCommon.h"
#include "SystemModules/Modules/Configuration/Slots/Configuration_Slots.h"

namespace SystemModules::Modules::RS485MC {
class RS485MC_MCDriverStatus;

class RS485MC_MCDriverOverride;

class RS485MC_MCDriver : public Framework::Components::Container {
 public:
  RS485MC_MCDriver(
    Logger::ILogger *logger,
    const Modules::Configuration::ConfigurationCommon &configCommon,
    const Configuration::Configuration_Slots &config);

  [[nodiscard]] RS485MC_MCDriverStatus *Status() const;

  [[nodiscard]] RS485MC_MCDriverOverride *Override() const;

 private:
  RS485MC_MCDriverStatus *status_;

  RS485MC_MCDriverOverride *override_;
};

}  // namespace SystemModules::Modules::RS485MC
