/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Actions/ActionTemplate.h"
#include "SystemModules/Framework/MonitoringFunctions/MFTemplate.h"
#include "SystemModules/Framework/Accessors/AccessorBase.h"
#include "SystemModules/Framework/Builder/Accessors/ReadAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemInBuilder.h"
#include "SystemModules/Framework/Builder/Components/ComponentBuilder.h"
#include "RS485MCAnalogIn1.h"

namespace SystemModules::Modules::RS485MC {

const char RS485MCAnalogIn1::kName[] = "Temperature";
const char RS485MCAnalogIn1::kDescription[] = "Temperature Sensor close to the Ethernet Switch";
const uint16_t RS485MCAnalogIn1::kAddress = 0x48;

const char RS485MCAnalogIn1::kTemperatureEthernetSwitchName[] = "EthernetSwitch";
const char RS485MCAnalogIn1::kTemperatureEthernetSwitchDescription[] = "Temperature at EthernetSwitch";
const char RS485MCAnalogIn1::kTemperatureEthernetSwitchSubPath[] = "/temp1_input";

const int32_t RS485MCAnalogIn1::kTemperatureEthernetSwitchUpperLimit = 45000;
const int32_t RS485MCAnalogIn1::kTemperatureEthernetSwitchLowerLimit = -1000;

RS485MCAnalogIn1::RS485MCAnalogIn1(
  Logger::ILogger *logger,
  const std::string &path,
  const std::string &devicePath,
  const uint32_t &slot)
  : Framework::Components::I2CDeviceLM75(
  logger,
  kName,
  kDescription,
  kAddress,
  path) {
  (void) devicePath;
  (void) slot;
  temperatureEthernetSwitch_ = Create().ItemIn<Types::Value<int32_t>>(
    kTemperatureEthernetSwitchName,
    kTemperatureEthernetSwitchDescription,
    Types::ValueGroup::Enum::TEMPERATURE,
    std::chrono::milliseconds(1000),
    Types::Affiliation::Enum::SHARED,
    Types::Unit::Enum::DEGREE_CELSIUS,
    Types::UnitPrefix::Enum::MILLI).AddAndReturnPointer();
  temperatureEthernetSwitch_->CreateAccessor().File<int32_t, std::string>(
    GetDevicePath().append(kTemperatureEthernetSwitchSubPath),
    1,
    1);
//  temperatureEthernetSwitch_->CreateMonitoringFunction().IsHigherThan(
//      Types::AlertSeverity::Enum::ERROR,
//      Types::MonitoringFunctionId::Enum::NONE,
//      std::chrono::milliseconds(1000),
//      std::chrono::milliseconds(1000),
//      Types::Value<int32_t> (
//          kTemperatureEthernetSwitchUpperLimit,
//          Types::Unit::Enum::DEGREE_CELSIUS,
//          Types::UnitPrefix::Enum::MILLI));
//  temperatureEthernetSwitch_->CreateMonitoringFunction().IsLowerThan(
//      Types::AlertSeverity::Enum::ERROR,
//      Types::MonitoringFunctionId::Enum::NONE,
//      std::chrono::milliseconds(1000),
//      std::chrono::milliseconds(1000),
//      Types::Value<int32_t> (
//          kTemperatureEthernetSwitchLowerLimit,
//          Types::Unit::Enum::DEGREE_CELSIUS,
//          Types::UnitPrefix::Enum::MILLI));
  (void) path;
}

Framework::Components::ItemIn<Types::Value<int32_t>> *RS485MCAnalogIn1::TemperatureEthernetSwitch() const {
  return temperatureEthernetSwitch_;
}

}  // namespace SystemModules::Modules::RS485MC
