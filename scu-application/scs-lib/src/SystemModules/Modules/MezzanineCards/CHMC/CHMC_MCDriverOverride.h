/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "SystemModules/Framework/Actions/ActionTemplate.h"
#include "SystemModules/Framework/Builder/Actions/ActionBuilder.h"
#include "SystemModules/Framework/Components/ItemIn.h"
#include "SystemModules/Framework/Components/ItemOut.h"
#include "SystemModules/Framework/Components/Container.h"
#include "SystemModules/Modules/Configuration/ConfigurationCommon.h"
#include "SystemModules/Modules/Configuration/Slots/Configuration_Slots.h"

namespace SystemModules::Modules::CHMC {
class CHMC_MCDriverOverride : public Framework::Components::Container {
 public:
  CHMC_MCDriverOverride(
    Logger::ILogger *logger,
    const Modules::Configuration::ConfigurationCommon &configCommon,
    const Configuration::Configuration_Slots &config);

  [[nodiscard]] Framework::Components::ItemIn<uint32_t> *InputSignalCollection() const;

  [[nodiscard]] Framework::Components::ItemIn<uint32_t> *OutputSignalCollection() const;

  [[nodiscard]] Framework::Components::ItemOut<bool> *ChopperFeedback() const;

  [[nodiscard]] Framework::Components::ItemOut<bool> *EnableOutput() const;

  [[nodiscard]] Framework::Components::ItemOut<bool> *WatchdogKeepAlive() const;

 private:
  Framework::Components::ItemIn<uint32_t> *inputSignalCollection_;

  Framework::Components::ItemIn<uint32_t> *outputSignalCollection_;

  Framework::Components::ItemOut<bool> *chopperFeedback_;

  Framework::Components::ItemOut<bool> *enableOutput_;

  Framework::Components::ItemOut<bool> *watchdogKeepAlive_;

  static const char kName[];
  static const char kDescription[];

  static const char kInputSignalCollectionName[];
  static const char kInputSignalCollectionDescription[];
  static const uint32_t kInputSignalCollectionPosition;

  static const char kOutputSignalCollectionName[];
  static const char kOutputSignalCollectionDescription[];
  static const uint32_t kOutputSignalCollectionPosition;

  static const char kChopperFeedbackName[];
  static const char kChopperFeedbackDescription[];
  static const uint32_t kChopperFeedbackPosition;

  static const char kEnableOutputName[];
  static const char kEnableOutputDescription[];
  static const uint32_t kEnableOutputPosition;

  static const char kWatchdogKeepAliveName[];
  static const char kWatchdogKeepAliveDescription[];
  static const uint32_t kWatchdogKeepAlivePosition;
};
}  // namespace SystemModules::Modules::CHMC
