/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "SystemModules/Framework/Components/ParameterItemOut.h"
#include "SystemModules/Framework/Components/Container.h"
#include "SystemModules/Modules/Configuration/Slots/Configuration_Slots.h"

namespace SystemModules::Modules::CHMC {
class CHMC_MCDriverStatus;

class CHMC_MCDriverOverride;

class CHMC_MCDriver : public Framework::Components::Container {
 public:
  CHMC_MCDriver(
    Logger::ILogger *logger,
    const Modules::Configuration::ConfigurationCommon &configCommon,
    const Configuration::Configuration_Slots &config);

  [[nodiscard]] CHMC_MCDriverStatus *Status() const;

  [[nodiscard]] CHMC_MCDriverOverride *Override() const;

  [[nodiscard]] Framework::Components::ParameterItemOut<bool> *DoNotActOnBI() const;

 private:
  CHMC_MCDriverStatus *status_;

  CHMC_MCDriverOverride *override_;

  Framework::Components::ParameterItemOut<bool> *doNotActOnBI_;

  static const char kName[];
  static const char kDescription[];

  static const char kDoNotActOnBIName[];
  static const char kDoNotActOnBIDescription[];
  static const uint32_t kDoNotActOnBIPosition;
};

}  // namespace SystemModules::Modules::CHMC
