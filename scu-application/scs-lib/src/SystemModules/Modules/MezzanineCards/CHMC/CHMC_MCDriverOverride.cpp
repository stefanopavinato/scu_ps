/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Builder/Actions/ActionBuilder.h"
#include "SystemModules/Framework/Builder/Accessors/ReadAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemInBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemOutBuilder.h"
#include "SystemModules/Framework/Builder/Components/ComponentBuilder.h"
#include "CHMC_MCDriverOverride.h"

namespace SystemModules::Modules::CHMC {

const char CHMC_MCDriverOverride::kName[] = "CHMCSignalOverride";
const char CHMC_MCDriverOverride::kDescription[] =
  "Allows to override individual input and output signals by means of an error-flag.";

const char CHMC_MCDriverOverride::kInputSignalCollectionName[] = "InputSignalCollection";
const char CHMC_MCDriverOverride::kInputSignalCollectionDescription[] =
  "Collection of CHMC input signal override items.";
const uint32_t CHMC_MCDriverOverride::kInputSignalCollectionPosition = 0;

const char CHMC_MCDriverOverride::kOutputSignalCollectionName[] = "OutputSignalCollection";
const char CHMC_MCDriverOverride::kOutputSignalCollectionDescription[] =
  "Collection of CHMC output signal override items.";
const uint32_t CHMC_MCDriverOverride::kOutputSignalCollectionPosition = 1;

const char CHMC_MCDriverOverride::kChopperFeedbackName[] = "ChopperFeedback";
const char CHMC_MCDriverOverride::kChopperFeedbackDescription[] =
  "Override Chopper Feedback signal. False==Do not override; True==Override with 'err-flag'.";
const uint32_t CHMC_MCDriverOverride::kChopperFeedbackPosition = 0;

const char CHMC_MCDriverOverride::kEnableOutputName[] = "EnableOutput";
const char CHMC_MCDriverOverride::kEnableOutputDescription[] =
  "Override Enable Output status. False==Do not override; True==Override with 'disable'.";
const uint32_t CHMC_MCDriverOverride::kEnableOutputPosition = 0;

const char CHMC_MCDriverOverride::kWatchdogKeepAliveName[] = "WDKeepAlive";
const char CHMC_MCDriverOverride::kWatchdogKeepAliveDescription[] =
  "Disable watchdog keep alive output. False==Do not disable; True==Disable.";
const uint32_t CHMC_MCDriverOverride::kWatchdogKeepAlivePosition = 1;

CHMC_MCDriverOverride::CHMC_MCDriverOverride(
  Logger::ILogger *logger,
  const Modules::Configuration::ConfigurationCommon &configCommon,
  const Configuration::Configuration_Slots &config)
  : Framework::Components::Container(
  logger,
  kName,
  kDescription) {
  // Get update intervaval
  auto updateInterval = configCommon.UpdateInterval()->Fast()->Value();
  // Setup Input Signal item collection
  inputSignalCollection_ = Create().ItemIn<uint32_t>(
    kInputSignalCollectionName,
    kInputSignalCollectionDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  inputSignalCollection_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ReadWriteRegister1Address()->Value(),
    kInputSignalCollectionPosition,
    1);
  // Setup Output Signal item collection
  outputSignalCollection_ = Create().ItemIn<uint32_t>(
    kOutputSignalCollectionName,
    kOutputSignalCollectionDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  outputSignalCollection_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ReadWriteRegister2Address()->Value(),
    kInputSignalCollectionPosition,
    2);
  // Setup chopper feedback override output
  chopperFeedback_ = Create().ItemOut<bool>(
    kChopperFeedbackName,
    kChopperFeedbackDescription,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    true,
    false).AddAndReturnPointer();
  chopperFeedback_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ReadWriteRegister1Address()->Value(),
    kChopperFeedbackPosition,
    1);
  // Setup enable output override output
  enableOutput_ = Create().ItemOut<bool>(
    kEnableOutputName,
    kEnableOutputDescription,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    true,
    false).AddAndReturnPointer();
  enableOutput_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ReadWriteRegister2Address()->Value(),
    kEnableOutputPosition,
    1);
  // Setup watchdog keep alive override output
  watchdogKeepAlive_ = Create().ItemOut<bool>(
    kWatchdogKeepAliveName,
    kWatchdogKeepAliveDescription,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    true,
    false).AddAndReturnPointer();
  watchdogKeepAlive_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ReadWriteRegister2Address()->Value(),
    kWatchdogKeepAlivePosition,
    1);

  (void) config;
}

Framework::Components::ItemIn<uint32_t> *CHMC_MCDriverOverride::InputSignalCollection() const {
  return inputSignalCollection_;
}

Framework::Components::ItemIn<uint32_t> *CHMC_MCDriverOverride::OutputSignalCollection() const {
  return outputSignalCollection_;
}


Framework::Components::ItemOut<bool> *CHMC_MCDriverOverride::ChopperFeedback() const {
  return chopperFeedback_;
}

Framework::Components::ItemOut<bool> *CHMC_MCDriverOverride::EnableOutput() const {
  return enableOutput_;
}

Framework::Components::ItemOut<bool> *CHMC_MCDriverOverride::WatchdogKeepAlive() const {
  return watchdogKeepAlive_;
}

}  // namespace SystemModules::Modules::CHMC
