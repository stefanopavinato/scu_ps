/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Builder/Accessors/ReadAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemInBuilder.h"
#include "SystemModules/Framework/Builder/Components/ComponentBuilder.h"
#include "CHMC_MCDriverStatus.h"

namespace SystemModules::Modules::CHMC {

const char CHMC_MCDriverStatus::kName[] = "Status";
const char CHMC_MCDriverStatus::kDescription[] =
  "Status of signals the Serializer receives/generates from/towards the mezzanine card.";

const char CHMC_MCDriverStatus::kCollection1Name[] = "Collection1";
const char CHMC_MCDriverStatus::kCollection1Description[] =
  "Collection of CHMC status items related to signals received from the MC.";
const uint32_t CHMC_MCDriverStatus::kCollection1Position = 0;

const char CHMC_MCDriverStatus::kCollection2Name[] = "Collection2";
const char CHMC_MCDriverStatus::kCollection2Description[] =
  "Collection of CHMC status items related to signal driven towards the MC.";
const uint32_t CHMC_MCDriverStatus::kCollection2Position = 0;

const char CHMC_MCDriverStatus::kWatchdogAExpiredName[] = "WDAExpired";
const char CHMC_MCDriverStatus::kWatchdogAExpiredDescription[] =
  "Watchdog A expired. False==not expired; True==expired.";
const uint32_t CHMC_MCDriverStatus::kWatchdogAExpiredPosition = 0;

const char CHMC_MCDriverStatus::kWatchdogBExpiredName[] = "WDBExpired";
const char CHMC_MCDriverStatus::kWatchdogBExpiredDescription[] =
  "Watchdog B expired. False==not expired; True==expired.";
const uint32_t CHMC_MCDriverStatus::kWatchdogBExpiredPosition = 1;

const char CHMC_MCDriverStatus::kChopperFeedbackStatusName[] = "ChopperFeedback";
const char CHMC_MCDriverStatus::kChopperFeedbackStatusDescription[] =
  "Status of the Chopper Feedback signal as received from the MC (raw value). "
  "False==proton beam deflected; True==proton beam not deflected.";
const uint32_t CHMC_MCDriverStatus::kChopperFeedbackStatusPosition = 2;

const char CHMC_MCDriverStatus::kEnable1Name[] = "Enable1";
const char CHMC_MCDriverStatus::kEnable1Description[] =
  "Status of the Enable 1 signal readback as received from the MC (raw value).";
const uint32_t CHMC_MCDriverStatus::kEnable1Position = 3;

const char CHMC_MCDriverStatus::kEnable2Name[] = "Enable2";
const char CHMC_MCDriverStatus::kEnable2Description[] =
  "Status of the Enable 2 signal readback as received from the MC (raw value).";
const uint32_t CHMC_MCDriverStatus::kEnable2Position = 4;

const char CHMC_MCDriverStatus::kEnable3Name[] = "Enable3";
const char CHMC_MCDriverStatus::kEnable3Description[] =
  "Status of the Enable 3 signal readback as received from the MC (raw value).";
const uint32_t CHMC_MCDriverStatus::kEnable3Address = 5;

const char CHMC_MCDriverStatus::kChopperFeedbackName[] = "ChopperFeedback";
const char CHMC_MCDriverStatus::kChopperFeedbackDescription[] =
  "Override Chopper Feedback signal. False==Not overridden; True==Overridden with 'err-flag'.";
const uint32_t CHMC_MCDriverStatus::kChopperFeedbackPosition = 0;

const char CHMC_MCDriverStatus::kEnableOutputName[] = "EnableOutput";
const char CHMC_MCDriverStatus::kEnableOutputDescription[] =
  "Override Enable Output status. False==Not overridden; True==Overridden with 'disable'.";
const uint32_t CHMC_MCDriverStatus::kEnableOutputPosition = 0;

const char CHMC_MCDriverStatus::kWatchdogKeepAliveName[] = "WDKeepAlive";
const char CHMC_MCDriverStatus::kWatchdogKeepAliveDescription[] =
  "Disable watchdog keep alive output. False==Not disabled; True==Disabled.";
const uint32_t CHMC_MCDriverStatus::kWatchdogKeepAlivePosition = 1;

CHMC_MCDriverStatus::CHMC_MCDriverStatus(
  Logger::ILogger *logger,
  const Modules::Configuration::ConfigurationCommon &configCommon,
  const Configuration::Configuration_Slots &config)
  : Framework::Components::Container(
  logger,
  kName,
  kDescription) {
  // Get update intervaval
  auto updateIntervalDiscreteSignals = configCommon.UpdateInterval()->Medium()->Value();
  // Setup item collection
  collection1_ = Create().ItemIn<uint32_t>(
    kCollection1Name,
    kCollection1Description,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDiscreteSignals),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  collection1_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister1Address()->Value(),
    kCollection1Position,
    6);
  collection2_ = Create().ItemIn<uint32_t>(
    kCollection2Name,
    kCollection2Description,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDiscreteSignals),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  collection2_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister2Address()->Value(),
    kCollection2Position,
    2);
  // Setup watchdog A expired status input
  watchdogAExpired_ = Create().ItemIn<bool>(
    kWatchdogAExpiredName,
    kWatchdogAExpiredDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDiscreteSignals),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  watchdogAExpired_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister1Address()->Value(),
    kWatchdogAExpiredPosition,
    1);
  // Setup watchdog B expired status input
  watchdogBExpired_ = Create().ItemIn<bool>(
    kWatchdogBExpiredName,
    kWatchdogBExpiredDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDiscreteSignals),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  watchdogBExpired_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister1Address()->Value(),
    kWatchdogBExpiredPosition,
    1);
  // Setup choper feedback signal status input
  chopperFeedbackStatus_ = Create().ItemIn<bool>(
    kChopperFeedbackStatusName,
    kChopperFeedbackStatusDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDiscreteSignals),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  chopperFeedbackStatus_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister1Address()->Value(),
    kChopperFeedbackStatusPosition,
    1);
  // Setup enable 1 signal readback status
  enable1_ = Create().ItemIn<bool>(
    kEnable1Name,
    kEnable1Description,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDiscreteSignals),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  enable1_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister1Address()->Value(),
    kEnable1Position,
    1);
  // Setup enable 2 signal readback status
  enable2_ = Create().ItemIn<bool>(
    kEnable2Name,
    kEnable2Description,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDiscreteSignals),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  enable2_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister1Address()->Value(),
    kEnable2Position,
    1);
  // Setup enable 3 signal readback status
  enable3_ = Create().ItemIn<bool>(
    kEnable3Name,
    kEnable3Description,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDiscreteSignals),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  enable3_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister1Address()->Value(),
    kEnable3Address,
    1);
  // Setup chopper feedback override output
  chopperFeedback_ = Create().ItemIn<bool>(
    kChopperFeedbackName,
    kChopperFeedbackDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDiscreteSignals),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  chopperFeedback_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ReadWriteRegister1Address()->Value(),
    kChopperFeedbackPosition,
    1);
  // Setup enable output override output
  enableOutput_ = Create().ItemIn<bool>(
    kEnableOutputName,
    kEnableOutputDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDiscreteSignals),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  enableOutput_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ReadWriteRegister2Address()->Value(),
    kEnableOutputPosition,
    1);
  // Setup watchdog keep alive override output
  watchdogKeepAlive_ = Create().ItemIn<bool>(
    kWatchdogKeepAliveName,
    kWatchdogKeepAliveDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDiscreteSignals),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  watchdogKeepAlive_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ReadWriteRegister2Address()->Value(),
    kWatchdogKeepAlivePosition,
    1);

  // ADC ready status is not used and therefore currently not supported by this software

  (void) config;
}

Framework::Components::ItemIn<uint32_t> *CHMC_MCDriverStatus::Collection1() const {
  return collection1_;
}

Framework::Components::ItemIn<uint32_t> *CHMC_MCDriverStatus::Collection2() const {
  return collection2_;
}

Framework::Components::ItemIn<bool> *CHMC_MCDriverStatus::WatchdogAExpired() const {
  return watchdogAExpired_;
}

Framework::Components::ItemIn<bool> *CHMC_MCDriverStatus::WatchdogBExpired() const {
  return watchdogBExpired_;
}

Framework::Components::ItemIn<bool> *CHMC_MCDriverStatus::ChopperFeedbackStatus() const {
  return chopperFeedbackStatus_;
}

Framework::Components::ItemIn<bool> *CHMC_MCDriverStatus::Enable1() const {
  return enable1_;
}

Framework::Components::ItemIn<bool> *CHMC_MCDriverStatus::Enable2() const {
  return enable2_;
}

Framework::Components::ItemIn<bool> *CHMC_MCDriverStatus::Enable3() const {
  return enable3_;
}

Framework::Components::ItemIn<bool> *CHMC_MCDriverStatus::ChopperFeedback() const {
  return chopperFeedback_;
}

Framework::Components::ItemIn<bool> *CHMC_MCDriverStatus::EnableOutput() const {
  return enableOutput_;
}

Framework::Components::ItemIn<bool> *CHMC_MCDriverStatus::WatchdogKeepAlive() const {
  return watchdogKeepAlive_;
}

}  // namespace SystemModules::Modules::CHMC
