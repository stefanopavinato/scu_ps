/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Builder/Actions/ActionBuilder.h"
#include "SystemModules/Framework/Builder/Accessors/WriteAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Components/ParameterItemOutBuilder.h"
#include "SystemModules/Framework/Builder/Components/ComponentBuilder.h"
#include "CHMC_MCDriverOverride.h"
#include "CHMC_MCDriverStatus.h"
#include "CHMC_MCDriver.h"

namespace SystemModules::Modules::CHMC {

const char CHMC_MCDriver::kName[] = "MCDriver";
const char CHMC_MCDriver::kDescription[] = "CHMC MC_Driver";

const char CHMC_MCDriver::kDoNotActOnBIName[] = "DoNotActOnBI";
const char CHMC_MCDriver::kDoNotActOnBIDescription[] =
  "0: Act on BI/RBI/EBI, 1: Act on BI/EBI (but not on BI). "
  "Temporarily feature for NCL commissioning. "
  "Needs to be removed for regular operation";
const uint32_t CHMC_MCDriver::kDoNotActOnBIPosition = 0;

CHMC_MCDriver::CHMC_MCDriver(
  Logger::ILogger *logger,
  const Modules::Configuration::ConfigurationCommon &configCommon,
  const Configuration::Configuration_Slots &config)
  : Framework::Components::Container(
  logger,
  kName,
  kDescription) {
  // Set override container
  auto override = std::make_unique<CHMC_MCDriverOverride>(
    logger,
    configCommon,
    config);
  override_ = override.get();
  AddComponent(std::move(override));
  // Set status container
  auto status = std::make_unique<CHMC_MCDriverStatus>(
    logger,
    configCommon,
    config);
  status_ = status.get();
  AddComponent(std::move(status));

  doNotActOnBI_ = Create().ParameterOut<bool>(
    kDoNotActOnBIName,
    kDoNotActOnBIDescription,
    Types::Affiliation::Enum::EXCLUSIVE,
    true,
    config.Application()->DoNotActOnBI()->Value()).AddAndReturnPointer();
  doNotActOnBI_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ReadWriteRegister3Address()->Value(),
    kDoNotActOnBIPosition,
    1);
}

CHMC_MCDriverStatus *CHMC_MCDriver::Status() const {
  return status_;
}

CHMC_MCDriverOverride *CHMC_MCDriver::Override() const {
  return override_;
}

Framework::Components::ParameterItemOut<bool> *CHMC_MCDriver::DoNotActOnBI() const {
  return doNotActOnBI_;
}

}  // namespace SystemModules::Modules::CHMC
