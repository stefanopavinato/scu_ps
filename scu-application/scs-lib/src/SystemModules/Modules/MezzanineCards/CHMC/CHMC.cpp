/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Builder/Actions/ActionBuilder.h"
#include "SystemModules/Framework/Builder/Actions/ActionBuilderGeneric.h"
#include "SystemModules/Framework/Components/ItemOut.h"
#include "SystemModules/Modules/MezzanineCards/Common/VoltageMonitor_3V3_1V8.h"
#include "SystemModules/Modules/MezzanineCards/Common/HotSwapController.h"
#include "CHMC_MCDriverStatus.h"
#include "CHMC_MCDriverOverride.h"
#include "CHMC_MCDriver.h"
#include "CHMC.h"

namespace SystemModules::Modules::CHMC {

const char CHMC::kName[] = "Application";
const char CHMC::kDescription[] = "CHMC specific application";

CHMC::CHMC(
  Logger::ILogger *logger,
  const Modules::Configuration::Modules::Configuration_CHMC &configCHMC,
  const Modules::Configuration::ConfigurationCommon &configCommon,
  const Configuration::Configuration_Slots &config)
  : Container(
  logger,
  kName,
  kDescription) {
  // Crate chmc MC_Driver
  auto mcDriver = std::make_unique<CHMC_MCDriver>(
    logger,
    configCommon,
    config);
  mcDriver_ = mcDriver.get();
  AddComponent(std::move(mcDriver));
  // Create voltage monitor
  auto voltageMonitor = std::make_unique<Common::VoltageMonitor_3V3_1V8>(
    logger,
    configCommon,
    config.PathI2CManagement()->GetValue());
  voltageMonitor_ = voltageMonitor.get();
  AddComponent(std::move(voltageMonitor));
  // Create hot swap controller
  auto hotSwapController = std::make_unique<Modules::Common::HotSwapController>(
    logger,
    *configCHMC.HotSwapController(),
    configCommon,
    config.PathI2CManagement()->GetValue(),
    0x4b);
  hotSwapController_ = hotSwapController.get();
  AddComponent(std::move(hotSwapController));
  // Enable read-back A
  /*
  {
    auto action = MCDriver()->Override()->EnableOutput()->CreateAction().Generic().BooleanEquation(
      "A || B",
      Types::AlertSeverity::Enum::WARNING);
    action->AddVariable("A", MCDriver()->Status()->WatchdogAExpired());
    action->AddVariable("B", MCDriver()->Status()->Enable1());
    action->Setup();
  }
  // Enable read-back B
  {
    auto action = MCDriver()->Override()->EnableOutput()->CreateAction().Generic().BooleanEquation(
      "A || B",
      Types::AlertSeverity::Enum::WARNING);
    action->AddVariable("A", MCDriver()->Status()->WatchdogBExpired());
    action->AddVariable("B", MCDriver()->Status()->Enable2());
    action->Setup();
  }
  // Enable output 3
  {
    auto action = MCDriver()->Override()->EnableOutput()->CreateAction().Generic().BooleanEquation(
      "A && B",
      Types::AlertSeverity::Enum::WARNING);
    action->AddVariable("A", MCDriver()->Status()->Enable1());
    action->AddVariable("B", MCDriver()->Status()->Enable2());
    action->Setup();
  }
  // Watchdog A expired
  {
    auto action = MCDriver()->Override()->WatchdogKeepAlive()->CreateAction().Generic().BooleanEquation(
      "A && B",
      Types::AlertSeverity::Enum::WARNING);
    action->AddVariable("A", MCDriver()->Status()->Enable1());
    action->AddVariable("B", MCDriver()->Status()->Enable2());
    action->Setup();
  }
   */
  MCDriver()->Override()->EnableOutput()->CreateAction().StaticValue(false);
  MCDriver()->Override()->WatchdogKeepAlive()->CreateAction().StaticValue(false);
  MCDriver()->Override()->ChopperFeedback()->CreateAction().StaticValue(false);
}

CHMC_MCDriver *CHMC::MCDriver() const {
  return mcDriver_;
}

Modules::Common::VoltageMonitor_3V3_1V8 *CHMC::VoltageMonitor() const {
  return voltageMonitor_;
}

Modules::Common::HotSwapController *CHMC::HotSwapController() const {
  return hotSwapController_;
}

}  // namespace SystemModules::Modules::CHMC
