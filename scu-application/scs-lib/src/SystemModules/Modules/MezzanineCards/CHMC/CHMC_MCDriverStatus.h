/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "SystemModules/Framework/Components/ItemIn.h"
#include "SystemModules/Framework/Components/Container.h"
#include "SystemModules/Modules/Configuration/ConfigurationCommon.h"
#include "SystemModules/Modules/Configuration/Slots/Configuration_Slots.h"

namespace SystemModules::Modules::CHMC {
class CHMC_MCDriverStatus : public Framework::Components::Container {
 public:
  CHMC_MCDriverStatus(
    Logger::ILogger *logger,
    const Modules::Configuration::ConfigurationCommon &configCommon,
    const Configuration::Configuration_Slots &config);

  [[nodiscard]] Framework::Components::ItemIn<uint32_t> *Collection1() const;

  [[nodiscard]] Framework::Components::ItemIn<uint32_t> *Collection2() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *WatchdogAExpired() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *WatchdogBExpired() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *ChopperFeedbackStatus() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *Enable1() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *Enable2() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *Enable3() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *ChopperFeedback() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *EnableOutput() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *WatchdogKeepAlive() const;

 private:
  Framework::Components::ItemIn<uint32_t> *collection1_;

  Framework::Components::ItemIn<uint32_t> *collection2_;

  Framework::Components::ItemIn<bool> *watchdogAExpired_;

  Framework::Components::ItemIn<bool> *watchdogBExpired_;

  Framework::Components::ItemIn<bool> *chopperFeedbackStatus_;

  Framework::Components::ItemIn<bool> *enable1_;

  Framework::Components::ItemIn<bool> *enable2_;

  Framework::Components::ItemIn<bool> *enable3_;

  Framework::Components::ItemIn<bool> *chopperFeedback_;

  Framework::Components::ItemIn<bool> *enableOutput_;

  Framework::Components::ItemIn<bool> *watchdogKeepAlive_;

  static const char kName[];
  static const char kDescription[];

  static const char kCollection1Name[];
  static const char kCollection1Description[];
  static const uint32_t kCollection1Position;

  static const char kCollection2Name[];
  static const char kCollection2Description[];
  static const uint32_t kCollection2Position;

  static const char kWatchdogAExpiredName[];
  static const char kWatchdogAExpiredDescription[];
  static const uint32_t kWatchdogAExpiredPosition;

  static const char kWatchdogBExpiredName[];
  static const char kWatchdogBExpiredDescription[];
  static const uint32_t kWatchdogBExpiredPosition;

  static const char kChopperFeedbackStatusName[];
  static const char kChopperFeedbackStatusDescription[];
  static const uint32_t kChopperFeedbackStatusPosition;

  static const char kEnable1Name[];
  static const char kEnable1Description[];
  static const uint32_t kEnable1Position;

  static const char kEnable2Name[];
  static const char kEnable2Description[];
  static const uint32_t kEnable2Position;

  static const char kEnable3Name[];
  static const char kEnable3Description[];
  static const uint32_t kEnable3Address;

  static const char kChopperFeedbackName[];
  static const char kChopperFeedbackDescription[];
  static const uint32_t kChopperFeedbackPosition;

  static const char kEnableOutputName[];
  static const char kEnableOutputDescription[];
  static const uint32_t kEnableOutputPosition;

  static const char kWatchdogKeepAliveName[];
  static const char kWatchdogKeepAliveDescription[];
  static const uint32_t kWatchdogKeepAlivePosition;
};
}  // namespace SystemModules::Modules::CHMC
