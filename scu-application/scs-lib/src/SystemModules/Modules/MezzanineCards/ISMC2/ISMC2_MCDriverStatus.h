/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "SystemModules/Framework/Components/ItemIn.h"
#include "SystemModules/Framework/Components/Container.h"
#include "SystemModules/Modules/Configuration/ConfigurationCommon.h"
#include "SystemModules/Modules/Configuration/Slots/Configuration_Slots.h"

namespace SystemModules::Modules::ISMC2 {
class ISMC2_MCDriverStatus : public Framework::Components::Container {
 public:
  ISMC2_MCDriverStatus(
    Logger::ILogger *logger,
    const Modules::Configuration::ConfigurationCommon &configCommon,
    const Configuration::Configuration_Slots &config);

  [[nodiscard]] Framework::Components::ItemIn<uint32_t> *Collection1() const;

  [[nodiscard]] Framework::Components::ItemIn<uint32_t> *Collection2() const;

  [[nodiscard]] Framework::Components::ItemIn<uint32_t> *RelaisSwitchingCount() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *WatchdogAExpired() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *WatchdogBExpired() const;

  [[nodiscard]] Framework::Components::ItemIn<uint8_t> *RelaisAStatus() const;

  [[nodiscard]] Framework::Components::ItemIn<uint8_t> *RelaisBStatus() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *HVEnable() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *WDKeepAlive() const;

 private:
  Framework::Components::ItemIn<uint32_t> *collection1_;

  Framework::Components::ItemIn<uint32_t> *collection2_;

  Framework::Components::ItemIn<uint32_t> *relaisSwitchingCount_;

  Framework::Components::ItemIn<bool> *watchdogAExpired_;

  Framework::Components::ItemIn<bool> *watchdogBExpired_;

  Framework::Components::ItemIn<uint8_t> *relaisAstatus_;

  Framework::Components::ItemIn<uint8_t> *relaisBstatus_;

  Framework::Components::ItemIn<bool> *hvEnable_;

  Framework::Components::ItemIn<bool> *wdKeepAlive_;

  static const char kName[];
  static const char kDescription[];

  static const char kCollection1Name[];
  static const char kCollection1Description[];
  static const uint32_t kCollection1Position;

  static const char kCollection2Name[];
  static const char kCollection2Description[];
  static const uint32_t kCollection2Position;

  static const char kRelaisSwitchingCountName[];
  static const char kRelaisSwitchingCountDescription[];
  static const uint32_t kRelaisSwitchingCountPosition;

  static const char kWatchdogAExpiredName[];
  static const char kWatchdogAExpiredDescription[];
  static const uint32_t kWatchdogAExpiredPosition;

  static const char kWatchdogBExpiredName[];
  static const char kWatchdogBExpiredDescription[];
  static const uint32_t kWatchdogBExpiredPosition;

  static const char kRelaisAStatusName[];
  static const char kRelaisAStatusDescription[];
  static const uint32_t kRelaisAStatusPosition;

  static const char kRelaisBStatusName[];
  static const char kRelaisBStatusDescription[];
  static const uint32_t kRelaisBStatusPosition;

  static const char kHVEnableName[];
  static const char kHVEnableDescription[];
  static const uint32_t kHVEnablePosition;

  static const char kWDKeepAliveName[];
  static const char kWDKeepAliveDescription[];
  static const uint32_t kWDKeepAlivePosition;
};
}  // namespace SystemModules::Modules::ISMC2
