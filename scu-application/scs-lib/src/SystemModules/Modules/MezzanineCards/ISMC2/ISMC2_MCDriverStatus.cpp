/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Actions/ActionTemplate.h"
#include "SystemModules/Framework/MonitoringFunctions/MFTemplate.h"
#include "SystemModules/Framework/Accessors/AccessorBase.h"
#include "SystemModules/Framework/Builder/Accessors/ReadAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemInBuilder.h"
#include "SystemModules/Framework/Builder/Components/ComponentBuilder.h"
#include "ISMC2_MCDriverStatus.h"

namespace SystemModules::Modules::ISMC2 {

const char ISMC2_MCDriverStatus::kName[] = "Status";
const char ISMC2_MCDriverStatus::kDescription[] =
  "Status of signals the Serializer receives/generates from/towards the mezzanine card.";

const char ISMC2_MCDriverStatus::kCollection1Name[] = "Collection1";
const char ISMC2_MCDriverStatus::kCollection1Description[] =
  "Collection of ISMC2 status items related to signals received from the MC.";
const uint32_t ISMC2_MCDriverStatus::kCollection1Position = 0;

const char ISMC2_MCDriverStatus::kCollection2Name[] = "Collection2";
const char ISMC2_MCDriverStatus::kCollection2Description[] =
  "Collection of ISMC2 status items related to signal driven towards the MC.";
const uint32_t ISMC2_MCDriverStatus::kCollection2Position = 0;

const char ISMC2_MCDriverStatus::kRelaisSwitchingCountName[] = "RelaisSwitchingCount";
const char ISMC2_MCDriverStatus::kRelaisSwitchingCountDescription[] = "Total number of relay switching.";
const uint32_t ISMC2_MCDriverStatus::kRelaisSwitchingCountPosition = 0;

const char ISMC2_MCDriverStatus::kWatchdogAExpiredName[] = "WDAExpired";
const char ISMC2_MCDriverStatus::kWatchdogAExpiredDescription[] =
  "Watchdog A expired. False==not expired; True==expired.";
const uint32_t ISMC2_MCDriverStatus::kWatchdogAExpiredPosition = 0;

const char ISMC2_MCDriverStatus::kWatchdogBExpiredName[] = "WDBExpired";
const char ISMC2_MCDriverStatus::kWatchdogBExpiredDescription[] =
  "Watchdog B expired. False==not expired; True==expired.";
const uint32_t ISMC2_MCDriverStatus::kWatchdogBExpiredPosition = 1;

const char ISMC2_MCDriverStatus::kRelaisAStatusName[] = "RelaisAStatus";
const char ISMC2_MCDriverStatus::kRelaisAStatusDescription[] =
  "Relay A status: ‘00’: Undefined, ‘01’: Open, ‘10’: Closed, ‘11’: Undefined.";
const uint32_t ISMC2_MCDriverStatus::kRelaisAStatusPosition = 2;

const char ISMC2_MCDriverStatus::kRelaisBStatusName[] = "RelaisBStatus";
const char ISMC2_MCDriverStatus::kRelaisBStatusDescription[] =
  "Relay B status: ‘00’: Undefined, ‘01’: Open, ‘10’: Closed, ‘11’: Undefined.";
const uint32_t ISMC2_MCDriverStatus::kRelaisBStatusPosition = 4;

const char ISMC2_MCDriverStatus::kHVEnableName[] = "HVEnable";
const char ISMC2_MCDriverStatus::kHVEnableDescription[] =
  "HV Enable output as driven towards the MC.";
const uint32_t ISMC2_MCDriverStatus::kHVEnablePosition = 0;

const char ISMC2_MCDriverStatus::kWDKeepAliveName[] = "WDKeepAlive";
const char ISMC2_MCDriverStatus::kWDKeepAliveDescription[] =
  "WD Keep Alive output as driven towards the MC (without frequency overlay).";
const uint32_t ISMC2_MCDriverStatus::kWDKeepAlivePosition = 1;

ISMC2_MCDriverStatus::ISMC2_MCDriverStatus(
  Logger::ILogger *logger,
  const Modules::Configuration::ConfigurationCommon &configCommon,
  const Configuration::Configuration_Slots &config)
  : Framework::Components::Container(
  logger,
  kName,
  kDescription) {
  // Get update intervaval
  auto updateIntervalDiscreteSignals = configCommon.UpdateInterval()->Medium()->Value();
  // Setup item collection
  collection1_ = Create().ItemIn<uint32_t>(
    kCollection1Name,
    kCollection1Description,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDiscreteSignals),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  collection1_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister1Address()->Value(),
    kCollection1Position,
    32);
  collection2_ = Create().ItemIn<uint32_t>(
    kCollection2Name,
    kCollection2Description,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDiscreteSignals),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  collection2_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister2Address()->Value(),
    kCollection2Position,
    32);
  relaisSwitchingCount_ = Create().ItemIn<uint32_t>(
    kRelaisSwitchingCountName,
    kRelaisSwitchingCountDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDiscreteSignals),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  relaisSwitchingCount_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister3Address()->Value(),
    kRelaisSwitchingCountPosition,
    32);
  // Setup watchdog A expired status input
  watchdogAExpired_ = Create().ItemIn<bool>(
    kWatchdogAExpiredName,
    kWatchdogAExpiredDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDiscreteSignals),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  watchdogAExpired_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister1Address()->Value(),
    kWatchdogAExpiredPosition,
    1);
  // Setup watchdog B expired status input
  watchdogBExpired_ = Create().ItemIn<bool>(
    kWatchdogBExpiredName,
    kWatchdogBExpiredDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDiscreteSignals),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  watchdogBExpired_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister1Address()->Value(),
    kWatchdogBExpiredPosition,
    1);
  // Setup trigger input signal status input
  relaisAstatus_ = Create().ItemIn<uint8_t>(
    kRelaisAStatusName,
    kRelaisAStatusDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDiscreteSignals),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  relaisAstatus_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister1Address()->Value(),
    kRelaisAStatusPosition,
    2);
//  relaisAstatus_->CreateMonitoringFunction().IsOutside(
//      Types::AlertSeverity::Enum::WARNING,
//      Types::MonitoringFunctionId::Enum::NONE,
//      std::chrono::milliseconds(1000),
//      std::chrono::milliseconds(1000),
//      1,
//      2);

  // Setup trigger output signal status input

  relaisBstatus_ = Create().ItemIn<uint8_t>(
    kRelaisBStatusName,
    kRelaisBStatusDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDiscreteSignals),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  relaisBstatus_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister1Address()->Value(),
    kRelaisBStatusPosition,
    2);
//  relaisBstatus_->CreateMonitoringFunction().IsOutside(
//      Types::AlertSeverity::Enum::WARNING,
//      Types::MonitoringFunctionId::Enum::NONE,
//      std::chrono::milliseconds(1000),
//      std::chrono::milliseconds(1000),
//      1,
//      2);

  // HV Enable output as driven towards the MC
  hvEnable_ = Create().ItemIn<bool>(
    kHVEnableName,
    kHVEnableDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDiscreteSignals),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  hvEnable_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister2Address()->Value(),
    kHVEnablePosition,
    1);
  // WD Keep Alive output as driven towards the MC (without frequency overlay)
  wdKeepAlive_ = Create().ItemIn<bool>(
    kWDKeepAliveName,
    kWDKeepAliveDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDiscreteSignals),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  wdKeepAlive_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister2Address()->Value(),
    kWDKeepAlivePosition,
    1);
}

Framework::Components::ItemIn<uint32_t> *ISMC2_MCDriverStatus::Collection1() const {
  return collection1_;
}

Framework::Components::ItemIn<uint32_t> *ISMC2_MCDriverStatus::Collection2() const {
  return collection2_;
}

Framework::Components::ItemIn<uint32_t> *ISMC2_MCDriverStatus::RelaisSwitchingCount() const {
  return relaisSwitchingCount_;
}

Framework::Components::ItemIn<bool> *ISMC2_MCDriverStatus::WatchdogAExpired() const {
  return watchdogAExpired_;
}

Framework::Components::ItemIn<bool> *ISMC2_MCDriverStatus::WatchdogBExpired() const {
  return watchdogBExpired_;
}

Framework::Components::ItemIn<uint8_t> *ISMC2_MCDriverStatus::RelaisAStatus() const {
  return relaisAstatus_;
}

Framework::Components::ItemIn<uint8_t> *ISMC2_MCDriverStatus::RelaisBStatus() const {
  return relaisBstatus_;
}

Framework::Components::ItemIn<bool> *ISMC2_MCDriverStatus::HVEnable() const {
  return hvEnable_;
}

Framework::Components::ItemIn<bool> *ISMC2_MCDriverStatus::WDKeepAlive() const {
  return wdKeepAlive_;
}

}  // namespace SystemModules::Modules::ISMC2
