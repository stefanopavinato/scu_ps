/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Builder/Actions/ActionBuilder.h"
#include "SystemModules/Framework/Builder/Accessors/ReadAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemInBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemOutBuilder.h"
#include "SystemModules/Framework/Builder/Components/ComponentBuilder.h"
#include "ISMC2_MCDriverOverride.h"

namespace SystemModules::Modules::ISMC2 {

const char ISMC2_MCDriverOverride::kName[] = "ISMC2Override";
const char ISMC2_MCDriverOverride::kDescription[] =
  "Allows to override individual input and output signals by means of an error-flag.";

const char ISMC2_MCDriverOverride::kOutputSignalCollectionName[] = "OutputSignalCollection";
const char ISMC2_MCDriverOverride::kOutputSignalCollectionDescription[] =
  "Collection of ISMC1 output signal override items.";
const uint32_t ISMC2_MCDriverOverride::kOutputSignalCollectionPosition = 0;

const char ISMC2_MCDriverOverride::kHighVoltageOutputName[] = "HVEnableOutput";
const char ISMC2_MCDriverOverride::kHighVoltageOutputDescription[] =
  "Override HighVoltageOutput signal. False==Do not override; True==Override with 'err-flag'.";
const uint32_t ISMC2_MCDriverOverride::kHighVoltageOutputPosition = 0;

const char ISMC2_MCDriverOverride::kWatchdogName[] = "WDKeepAliveOutput";
const char ISMC2_MCDriverOverride::kWatchdogDescription[] =
  "Override Watchdog signal. False==Do not override; True==Override with 'err-flag'.";
const uint32_t ISMC2_MCDriverOverride::kWatchdogPosition = 1;

ISMC2_MCDriverOverride::ISMC2_MCDriverOverride(
  Logger::ILogger *logger,
  const Modules::Configuration::ConfigurationCommon &configCommon,
  const Configuration::Configuration_Slots &config)
  : Framework::Components::Container(
  logger,
  kName,
  kDescription) {
  // Get update intervaval
  auto updateInterval = configCommon.UpdateInterval()->Fast()->Value();
  // Setup Output Signal item collection
  outputSignalCollection_ = Create().ItemIn<uint32_t>(
    kOutputSignalCollectionName,
    kOutputSignalCollectionDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  outputSignalCollection_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ReadWriteRegister2Address()->Value(),
    kOutputSignalCollectionPosition,
    32);
  // Setup HighVoltageOutput override input
  highVoltageOutput_ = Create().ItemOut<bool>(
    kHighVoltageOutputName,
    kHighVoltageOutputDescription,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    true,
    false).AddAndReturnPointer();
  highVoltageOutput_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ReadWriteRegister2Address()->Value(),
    kHighVoltageOutputPosition,
    1);
  // Setup Watchdog override input
  watchdog_ = Create().ItemOut<bool>(
    kWatchdogName,
    kWatchdogDescription,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    true,
    false).AddAndReturnPointer();
  watchdog_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ReadWriteRegister2Address()->Value(),
    kWatchdogPosition,
    1);
  (void) config;
}

Framework::Components::ItemOut<bool> *ISMC2_MCDriverOverride::HighVoltageOutput() const {
  return highVoltageOutput_;
}

Framework::Components::ItemOut<bool> *ISMC2_MCDriverOverride::Watchdog() const {
  return watchdog_;
}

}  // namespace SystemModules::Modules::ISMC2
