/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Builder/Actions/ActionBuilder.h"
#include "SystemModules/Framework/Components/ItemOut.h"
#include "SystemModules/Modules/MezzanineCards/Common/VoltageMonitor_3V3_1V8.h"
#include "SystemModules/Modules/MezzanineCards/Common/HotSwapController.h"
#include "ISMC2_MCDriverStatus.h"
#include "ISMC2_MCDriverOverride.h"
#include "ISMC2_MCDriver.h"
#include "ISMC2.h"

namespace SystemModules::Modules::ISMC2 {

ISMC2::ISMC2(
  Logger::ILogger *logger,
  const Modules::Configuration::Modules::Configuration_ISMC2 &configISMC2,
  const Modules::Configuration::ConfigurationCommon &configCommon,
  const Modules::Configuration::Configuration_Slots &config)
  : Container(
  logger,
  "Application",
  "ISMC2 specific application") {
  // Crate ISMC2 MC_Driver
  auto mcdriver = std::make_unique<ISMC2_MCDriver>(
    logger,
    configCommon,
    config);
  mcdriver_ = mcdriver.get();
  AddComponent(std::move(mcdriver));
  // Create voltage monitor
  auto voltageMonitor = std::make_unique<Common::VoltageMonitor_3V3_1V8>(
    logger,
    configCommon,
    config.PathI2CManagement()->GetValue());
  voltageMonitor_ = voltageMonitor.get();
  AddComponent(std::move(voltageMonitor));
  // Create hot swap controller
  auto hotSwapController = std::make_unique<Modules::Common::HotSwapController>(
    logger,
    *configISMC2.HotSwapController(),
    configCommon,
    config.PathI2CManagement()->GetValue(),
    0x4b);
  hotSwapController_ = hotSwapController.get();
  AddComponent(std::move(hotSwapController));
  // Create and add the actions
  MCDriver()->Override()->HighVoltageOutput()->CreateAction().StaticValue(false);
  MCDriver()->Override()->Watchdog()->CreateAction().StaticValue(false);
}

Modules::ISMC2::ISMC2_MCDriver *ISMC2::MCDriver() const {
  return mcdriver_;
}

Modules::Common::VoltageMonitor_3V3_1V8 *ISMC2::VoltageMonitor() const {
  return voltageMonitor_;
}

Modules::Common::HotSwapController *ISMC2::HotSwapController() const {
  return hotSwapController_;
}

}  // namespace SystemModules::Modules::ISMC2
