/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <cstdint>
#include "SystemModules/Framework/Components/ItemIn.h"
#include "SystemModules/Framework/Components/Container.h"
#include "SystemModules/Modules/Configuration/ConfigurationCommon.h"
#include "SystemModules/Modules/Configuration/Slots/Configuration_Slots.h"

namespace SystemModules::Modules::ISMC2 {
class ISMC2_MCDriverOverride : public Framework::Components::Container {
 public:
  ISMC2_MCDriverOverride(
    Logger::ILogger *logger,
    const Modules::Configuration::ConfigurationCommon &configCommon,
    const Configuration::Configuration_Slots &config);

  [[nodiscard]] Framework::Components::ItemIn<uint32_t> *OutputSignalCollection() const;

  [[nodiscard]] Framework::Components::ItemOut<bool> *HighVoltageOutput() const;

  [[nodiscard]] Framework::Components::ItemOut<bool> *Watchdog() const;

 private:
  Framework::Components::ItemIn<uint32_t> *outputSignalCollection_;

  Framework::Components::ItemOut<bool> *highVoltageOutput_;

  Framework::Components::ItemOut<bool> *watchdog_;

  static const char kName[];
  static const char kDescription[];

  static const char kOutputSignalCollectionName[];
  static const char kOutputSignalCollectionDescription[];
  static const uint32_t kOutputSignalCollectionPosition;

  static const char kHighVoltageOutputName[];
  static const char kHighVoltageOutputDescription[];
  static const uint32_t kHighVoltageOutputPosition;

  static const char kWatchdogName[];
  static const char kWatchdogDescription[];
  static const uint32_t kWatchdogPosition;
};

}  // namespace SystemModules::Modules::ISMC2
