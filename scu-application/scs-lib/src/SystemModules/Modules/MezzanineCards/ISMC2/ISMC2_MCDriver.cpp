/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Logger/ILogger.h"
#include "SystemModules/Framework/Components/ItemOut.h"
#include "SystemModules/Framework/Builder/Components/ComponentBuilder.h"
#include "ISMC2_MCDriverOverride.h"
#include "ISMC2_MCDriverStatus.h"
#include "ISMC2_MCDriver.h"

namespace SystemModules::Modules::ISMC2 {

ISMC2_MCDriver::ISMC2_MCDriver(
  Logger::ILogger *logger,
  const Modules::Configuration::ConfigurationCommon &configCommon,
  const Configuration::Configuration_Slots &config)
  : Framework::Components::Container(
  logger,
  "MCDriver",
  "ISMC2 MC_Driver") {
  // Set override container
  auto override = std::make_unique<ISMC2_MCDriverOverride>(
    logger,
    configCommon,
    config);
  override_ = override.get();
  AddComponent(std::move(override));
  // Set status container
  auto status = std::make_unique<ISMC2_MCDriverStatus>(
    logger,
    configCommon,
    config);
  status_ = status.get();
  AddComponent(std::move(status));

  (void) config;
}

ISMC2_MCDriverStatus *ISMC2_MCDriver::Status() const {
  return status_;
}

ISMC2_MCDriverOverride *ISMC2_MCDriver::Override() const {
  return override_;
}

}  // namespace SystemModules::Modules::ISMC2
