/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "SystemModules/Framework/Components/Container.h"
#include "SystemModules/Modules/Configuration/ConfigurationCommon.h"
#include "SystemModules/Modules/Configuration/Modules/Configuration_ISMC2.h"
#include "SystemModules/Modules/Configuration/Slots/Configuration_Slots.h"

namespace SystemModules::Modules {
namespace Common {
class VoltageMonitor_3V3_1V8;
class HotSwapController;
}  // namespace Common
namespace ISMC2 {
class ISMC2_MCDriver;

class ISMC2 : public Framework::Components::Container {
 public:
  ISMC2(
    Logger::ILogger *logger,
    const Modules::Configuration::Modules::Configuration_ISMC2 &configISMC2,
    const Modules::Configuration::ConfigurationCommon &configCommon,
    const Modules::Configuration::Configuration_Slots &config);

  [[nodiscard]] ISMC2_MCDriver *MCDriver() const;

  [[nodiscard]] Common::VoltageMonitor_3V3_1V8 *VoltageMonitor() const;

  [[nodiscard]] Modules::Common::HotSwapController *HotSwapController() const;

 private:
  ISMC2_MCDriver *mcdriver_;

  Common::VoltageMonitor_3V3_1V8 *voltageMonitor_;

  Modules::Common::HotSwapController *hotSwapController_;
};
}  // namespace ISMC2
}  // namespace SystemModules::Modules
