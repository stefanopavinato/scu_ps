/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "SystemModules/Framework/Components/Container.h"
#include "SystemModules/Modules/Configuration/ConfigurationCommon.h"
#include "SystemModules/Modules/Configuration/Modules/Configuration_BUMC.h"
#include "SystemModules/Modules/Configuration/Slots/Configuration_Slots.h"

namespace SystemModules::Modules::BUMC {
class BUMC_MCDriver;

class BUMC : public Framework::Components::Container {
 public:
  BUMC(
    Logger::ILogger *logger,
    const Modules::Configuration::Modules::Configuration_BUMC &configBUMC,
    const Modules::Configuration::ConfigurationCommon &configCommon,
    const Modules::Configuration::Configuration_Slots &config);

  [[nodiscard]] BUMC_MCDriver *MCDriver() const;

 private:
  BUMC_MCDriver *mcDriver_;

  static const char kName[];
  static const char kDescription[];
};
}  // namespace SystemModules::Modules::BUMC
