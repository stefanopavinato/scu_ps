/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "SystemModules/Framework/Components/ItemIn.h"
#include "SystemModules/Framework/Components/ItemInOut.h"
#include "SystemModules/Framework/Components/Container.h"
#include "SystemModules/Modules/Configuration/ConfigurationCommon.h"
#include "SystemModules/Modules/Configuration/Slots/Configuration_Slots.h"

namespace SystemModules::Modules::BUMC {
class BUMC_MCDriverStatus;

class BUMC_MCDriverOverride;

class BUMC_MCDriver : public Framework::Components::Container {
 public:
  BUMC_MCDriver(
    Logger::ILogger *logger,
    const Modules::Configuration::ConfigurationCommon &configCommon,
    const Configuration::Configuration_Slots &config);

  [[nodiscard]] Framework::Components::ItemInOut<uint32_t> *RwReg1() const;

  [[nodiscard]] Framework::Components::ItemInOut<uint32_t> *RwReg2() const;

  [[nodiscard]] Framework::Components::ItemInOut<uint32_t> *RwReg3() const;

  [[nodiscard]] Framework::Components::ItemIn<uint32_t> *StatReg1() const;

  [[nodiscard]] Framework::Components::ItemIn<uint32_t> *StatReg2() const;

  [[nodiscard]] Framework::Components::ItemIn<uint32_t> *StatReg3() const;

  [[nodiscard]] Framework::Components::ItemIn<uint32_t> *StatReg4() const;

  [[nodiscard]] Framework::Components::ItemIn<uint32_t> *StatReg5() const;

  [[nodiscard]] Framework::Components::ItemIn<uint32_t> *StatReg6() const;

 private:
  Framework::Components::ItemInOut<uint32_t> *rwReg1_;

  Framework::Components::ItemInOut<uint32_t> *rwReg2_;

  Framework::Components::ItemInOut<uint32_t> *rwReg3_;

  Framework::Components::ItemIn<uint32_t> *statReg1_;

  Framework::Components::ItemIn<uint32_t> *statReg2_;

  Framework::Components::ItemIn<uint32_t> *statReg3_;

  Framework::Components::ItemIn<uint32_t> *statReg4_;

  Framework::Components::ItemIn<uint32_t> *statReg5_;

  Framework::Components::ItemIn<uint32_t> *statReg6_;

  static const char kName[];
  static const char kDescription[];

  static const char kRwReg1Name[];
  static const char kRwReg1Description[];

  static const char kRwReg2Name[];
  static const char kRwReg2Description[];

  static const char kRwReg3Name[];
  static const char kRwReg3Description[];

  static const char kStatReg1Name[];
  static const char kStatReg1Description[];

  static const char kStatReg2Name[];
  static const char kStatReg2Description[];

  static const char kStatReg3Name[];
  static const char kStatReg3Description[];

  static const char kStatReg4Name[];
  static const char kStatReg4Description[];

  static const char kStatReg5Name[];
  static const char kStatReg5Description[];

  static const char kStatReg6Name[];
  static const char kStatReg6Description[];
};

}  // namespace SystemModules::Modules::BUMC
