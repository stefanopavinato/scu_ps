/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Builder/Actions/ActionBuilder.h"
#include "BUMC_MCDriver.h"
#include "BUMC.h"

namespace SystemModules::Modules::BUMC {

const char BUMC::kName[] = "Application";
const char BUMC::kDescription[] = "BUMC specific application";

BUMC::BUMC(
  Logger::ILogger *logger,
  const Modules::Configuration::Modules::Configuration_BUMC &configBUMC,
  const Modules::Configuration::ConfigurationCommon &configCommon,
  const Configuration::Configuration_Slots &config)
  : Container(
  logger,
  kName,
  kDescription) {
  // Crate BUMC MC_Driver
  auto mcDriver = std::make_unique<BUMC_MCDriver>(
    logger,
    configCommon,
    config);
  mcDriver_ = mcDriver.get();
  AddComponent(std::move(mcDriver));
}

BUMC_MCDriver *BUMC::MCDriver() const {
  return mcDriver_;
}

}  // namespace SystemModules::Modules::BUMC
