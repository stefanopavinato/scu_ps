/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Builder/Actions/ActionBuilder.h"
#include "SystemModules/Framework/Builder/Accessors/ReadAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Accessors/ReadWriteAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemInBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemInOutBuilder.h"
#include "SystemModules/Framework/Builder/Components/ComponentBuilder.h"
#include "BUMC_MCDriver.h"

namespace SystemModules::Modules::BUMC {

const char BUMC_MCDriver::kName[] = "MCDriver";
const char BUMC_MCDriver::kDescription[] = "BUMC MC_Driver";

const char BUMC_MCDriver::kRwReg1Name[] = "RwReg1";
const char BUMC_MCDriver::kRwReg1Description[] = "Read/Write register 1 of BUMC";

const char BUMC_MCDriver::kRwReg2Name[] = "RwReg2";
const char BUMC_MCDriver::kRwReg2Description[] = "Read/Write register 2 of BUMC";

const char BUMC_MCDriver::kRwReg3Name[] = "RwReg3";
const char BUMC_MCDriver::kRwReg3Description[] = "Read/Write register 3 of BUMC";

const char BUMC_MCDriver::kStatReg1Name[] = "StatReg1";
const char BUMC_MCDriver::kStatReg1Description[] = "Status register 1 of BUMC";

const char BUMC_MCDriver::kStatReg2Name[] = "StatReg2";
const char BUMC_MCDriver::kStatReg2Description[] = "Status register 2 of BUMC";

const char BUMC_MCDriver::kStatReg3Name[] = "StatReg3";
const char BUMC_MCDriver::kStatReg3Description[] = "Status register 3 of BUMC";

const char BUMC_MCDriver::kStatReg4Name[] = "StatReg4";
const char BUMC_MCDriver::kStatReg4Description[] = "Status register 4 of BUMC";

const char BUMC_MCDriver::kStatReg5Name[] = "StatReg5";
const char BUMC_MCDriver::kStatReg5Description[] = "Status register 5 of BUMC";

const char BUMC_MCDriver::kStatReg6Name[] = "StatReg6";
const char BUMC_MCDriver::kStatReg6Description[] = "Status register 6 of BUMC";


BUMC_MCDriver::BUMC_MCDriver(
  Logger::ILogger *logger,
  const Modules::Configuration::ConfigurationCommon &configCommon,
  const Configuration::Configuration_Slots &config)
  : Framework::Components::Container(
  logger,
  kName,
  kDescription) {
  // Get update intervaval
  auto updateInterval = configCommon.UpdateInterval()->Fast()->Value();
  // Setup RwReg1
  rwReg1_ = Create().ItemInOut<uint32_t>(
    kRwReg1Name,
    kRwReg1Description,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    false).AddAndReturnPointer();
  rwReg1_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ReadWriteRegister1Address()->Value(),
    0,
    32);
  rwReg1_->CreateAction().StaticValue(false);
  // Setup RwReg2
  rwReg2_ = Create().ItemInOut<uint32_t>(
    kRwReg2Name,
    kRwReg2Description,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    false).AddAndReturnPointer();
  rwReg2_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ReadWriteRegister2Address()->Value(),
    0,
    32);
  rwReg2_->CreateAction().StaticValue(false);
  // Setup RwReg3
  rwReg3_ = Create().ItemInOut<uint32_t>(
    kRwReg3Name,
    kRwReg3Description,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    false).AddAndReturnPointer();
  rwReg3_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ReadWriteRegister3Address()->Value(),
    0,
    32);
  rwReg3_->CreateAction().StaticValue(false);
  // Setup StatReg1
  statReg1_ = Create().ItemIn<uint32_t>(
    kStatReg1Name,
    kStatReg1Description,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  statReg1_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister1Address()->Value(),
    0,
    32);
  // Setup StatReg2
  statReg2_ = Create().ItemIn<uint32_t>(
    kStatReg2Name,
    kStatReg2Description,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  statReg2_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister2Address()->Value(),
    0,
    32);
  // Setup StatReg3
  statReg3_ = Create().ItemIn<uint32_t>(
    kStatReg3Name,
    kStatReg3Description,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  statReg3_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister3Address()->Value(),
    0,
    32);
  // Setup StatReg4
  statReg4_ = Create().ItemIn<uint32_t>(
    kStatReg4Name,
    kStatReg4Description,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  statReg4_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister4Address()->Value(),
    0,
    32);
  // Setup StatReg5
  statReg5_ = Create().ItemIn<uint32_t>(
    kStatReg5Name,
    kStatReg5Description,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  statReg5_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister5Address()->Value(),
    0,
    32);
  // Setup StatReg6
  statReg6_ = Create().ItemIn<uint32_t>(
    kStatReg6Name,
    kStatReg6Description,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  statReg6_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister6Address()->Value(),
    0,
    32);

  (void) config;
}

Framework::Components::ItemInOut<uint32_t> *BUMC_MCDriver::RwReg1() const {
  return rwReg1_;
}

Framework::Components::ItemInOut<uint32_t> *BUMC_MCDriver::RwReg2() const {
  return rwReg2_;
}

Framework::Components::ItemInOut<uint32_t> *BUMC_MCDriver::RwReg3() const {
  return rwReg3_;
}

Framework::Components::ItemIn<uint32_t> *BUMC_MCDriver::StatReg1() const {
  return statReg1_;
}

Framework::Components::ItemIn<uint32_t> *BUMC_MCDriver::StatReg2() const {
  return statReg2_;
}

Framework::Components::ItemIn<uint32_t> *BUMC_MCDriver::StatReg3() const {
  return statReg3_;
}

Framework::Components::ItemIn<uint32_t> *BUMC_MCDriver::StatReg4() const {
  return statReg4_;
}

Framework::Components::ItemIn<uint32_t> *BUMC_MCDriver::StatReg5() const {
  return statReg5_;
}

Framework::Components::ItemIn<uint32_t> *BUMC_MCDriver::StatReg6() const {
  return statReg6_;
}

}  // namespace SystemModules::Modules::BUMC
