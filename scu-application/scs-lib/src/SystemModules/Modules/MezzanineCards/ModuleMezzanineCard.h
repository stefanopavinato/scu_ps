/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "SystemModules/Framework/Components/ModuleBase.h"
#include "SystemModules/Modules/Configuration/Configuration.h"
#include "SystemModules/Modules/Configuration/ConfigurationCommon.h"
#include "SystemModules/Modules/Configuration/Slots/Configuration_Slots.h"

namespace SystemModules {
namespace Modules {
class Management;

class CardDetectionItems;
}  // namespace Modules
namespace Framework {
namespace States {
class MCStateBase;
}  // namespace States
namespace Components {
class ModuleMezzanineCard : public ModuleBase {
  friend class States::MCStateBase;

 public:
  ModuleMezzanineCard(
    Logger::ILogger *logger,
    Modules::Configuration::Configuration *configuration,
    Modules::Configuration::ConfigurationCommon *configCommon,
    Modules::Configuration::Configuration_Slots *config,
    WorkloadBalancing::WorkloadBalanceHandler *workloadBalanceHandler);

  [[nodiscard]] Types::ModuleState GetStateId() const override;

  [[nodiscard]] Modules::CardDetectionItems *CardDetectionItems() const;

  [[nodiscard]] bool InstallCardDetectionItems();

  [[nodiscard]] bool UninstallCardDetectionItems();

  [[nodiscard]] Modules::Management *Management() const;

  [[nodiscard]] bool InstallManagement();

  [[nodiscard]] bool UninstallManagement();

  [[nodiscard]] SystemModules::Framework::Components::Container *Application() const;

  [[nodiscard]] bool InstallApplication();

  [[nodiscard]] bool UninstallApplication();

  bool *MezzanineCardEnable();

  bool *MezzanineCardReset();

  bool *DriverEnable();

  bool *DriverReset();

  [[nodiscard]] Types::AlertSeverity::Enum Update(
    const std::chrono::steady_clock::time_point &timePointNow) override;

 protected:
  void SetState(std::unique_ptr<States::MCStateBase> stateNew);

 private:
  std::unique_ptr<States::MCStateBase> state_;

  bool mezzanineCardEnable_;
  bool mezzanineCardReset_;
  bool driverEnable_;
  bool driverReset_;

  Modules::Configuration::Configuration *configuration_;

  Modules::Configuration::ConfigurationCommon const *configCommon_;

  Modules::Configuration::Configuration_Slots const *config_;

  Modules::CardDetectionItems *cardDetectionItems_;

  Modules::Management *management_;

  SystemModules::Framework::Components::Container *application_;

  static const char kDescription[];
  static const char kComponentType[];
};
}  // namespace Components
}  // namespace Framework
}  // namespace SystemModules
