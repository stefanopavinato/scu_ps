/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Types/AlertSeverity.h"
#include "SystemModules/Framework/Builder/Actions/ActionBuilderGeneric.h"
#include "SystemModules/Framework/Components/ItemOut.h"
#include "SystemModules/Modules/MezzanineCards/Common/VoltageMonitor_3V3_1V8.h"
#include "SystemModules/Modules/MezzanineCards/Common/HotSwapController.h"
#include "LVDSMC_MCDriverOverride.h"
#include "LVDSMC_MCDriverStatus.h"
#include "LVDSMC_MCDriverPort.h"
#include "LVDSMC_MCDriver.h"
#include "LVDSMC.h"

namespace SystemModules::Modules::LVDSMC {

LVDSMC::LVDSMC(
  Logger::ILogger *logger,
  const Modules::Configuration::Modules::Configuration_LVDSMC &configLVDSMC,
  const Modules::Configuration::ConfigurationCommon &configCommon,
  const Modules::Configuration::Configuration_Slots &config)
  : Container(
  logger,
  "Application",
  "LVDS specific application") {
  // Crate lvdsmc MC_Driver
  auto mcdriver = std::make_unique<LVDSMC_MCDriver>(
    logger,
    configCommon,
    config);
  mcDriver_ = mcdriver.get();
  AddComponent(std::move(mcdriver));
  // Create voltage monitor
  auto voltageMonitor = std::make_unique<Common::VoltageMonitor_3V3_1V8>(
    logger,
    configCommon,
    config.PathI2CManagement()->GetValue());
  voltageMonitor_ = voltageMonitor.get();
  AddComponent(std::move(voltageMonitor));
  // Create hot swap controller
  auto hotSwapController = std::make_unique<Modules::Common::HotSwapController>(
    logger,
    *configLVDSMC.HotSwapController(),
    configCommon,
    config.PathI2CManagement()->GetValue(),
    0x4b);
  hotSwapController_ = hotSwapController.get();
  AddComponent(std::move(hotSwapController));

  MCDriver()->Port1()->Override()->BeamAbsence()->CreateAction().StaticValue(false);
  MCDriver()->Port1()->Override()->BeamPermit()->CreateAction().StaticValue(false);
  MCDriver()->Port1()->Override()->Ready()->CreateAction().StaticValue(false);
  MCDriver()->Port1()->Override()->DatalinkBeamAbsence()->CreateAction().StaticValue(false);
  MCDriver()->Port1()->Override()->DatalinkBeamPermit()->CreateAction().StaticValue(false);
  MCDriver()->Port1()->Override()->DatalinkReady()->CreateAction().StaticValue(false);
  MCDriver()->Port1()->Override()->DatalinkHVAbsence()->CreateAction().StaticValue(false);
  MCDriver()->Port1()->Override()->DatalinkHVExtraction()->CreateAction().StaticValue(false);
  MCDriver()->Port1()->Override()->DatalinkPBD()->CreateAction().StaticValue(false);
  MCDriver()->Port1()->Override()->DatalinkPBM()->CreateAction().StaticValue(false);

  MCDriver()->Port2()->Override()->BeamAbsence()->CreateAction().StaticValue(false);
  MCDriver()->Port2()->Override()->BeamPermit()->CreateAction().StaticValue(false);
  MCDriver()->Port2()->Override()->Ready()->CreateAction().StaticValue(false);
  MCDriver()->Port2()->Override()->DatalinkBeamAbsence()->CreateAction().StaticValue(false);
  MCDriver()->Port2()->Override()->DatalinkBeamPermit()->CreateAction().StaticValue(false);
  MCDriver()->Port2()->Override()->DatalinkReady()->CreateAction().StaticValue(false);
  MCDriver()->Port2()->Override()->DatalinkHVAbsence()->CreateAction().StaticValue(false);
  MCDriver()->Port2()->Override()->DatalinkHVExtraction()->CreateAction().StaticValue(false);
  MCDriver()->Port2()->Override()->DatalinkPBD()->CreateAction().StaticValue(false);
  MCDriver()->Port2()->Override()->DatalinkPBM()->CreateAction().StaticValue(false);

  MCDriver()->Port3()->Override()->BeamAbsence()->CreateAction().StaticValue(false);
  MCDriver()->Port3()->Override()->BeamPermit()->CreateAction().StaticValue(false);
  MCDriver()->Port3()->Override()->Ready()->CreateAction().StaticValue(false);
  MCDriver()->Port3()->Override()->DatalinkBeamAbsence()->CreateAction().StaticValue(false);
  MCDriver()->Port3()->Override()->DatalinkBeamPermit()->CreateAction().StaticValue(false);
  MCDriver()->Port3()->Override()->DatalinkReady()->CreateAction().StaticValue(false);
  MCDriver()->Port3()->Override()->DatalinkHVAbsence()->CreateAction().StaticValue(false);
  MCDriver()->Port3()->Override()->DatalinkHVExtraction()->CreateAction().StaticValue(false);
  MCDriver()->Port3()->Override()->DatalinkPBD()->CreateAction().StaticValue(false);
  MCDriver()->Port3()->Override()->DatalinkPBM()->CreateAction().StaticValue(false);
}

Modules::LVDSMC::LVDSMC_MCDriver *LVDSMC::MCDriver() const {
  return mcDriver_;
}

Modules::Common::VoltageMonitor_3V3_1V8 *LVDSMC::VoltageMonitor() const {
  return voltageMonitor_;
}

Modules::Common::HotSwapController *LVDSMC::HotSwapController() const {
  return hotSwapController_;
}

}  // namespace SystemModules::Modules::LVDSMC
