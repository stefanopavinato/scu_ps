/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Logger/ILogger.h"
#include "SystemModules/Framework/Components/ItemOut.h"
#include "SystemModules/Framework/Builder/Components/ComponentBuilder.h"
#include "LVDSMC_MCDriverOverride.h"
#include "LVDSMC_MCDriverStatus.h"
#include "LVDSMC_MCDriverPort.h"

namespace SystemModules::Modules::LVDSMC {

const uint32_t LVDSMC_MCDriverPort::kClearCounterDelay = 400;  // milli-seconds tbd:rejz?

const char LVDSMC_MCDriverPort::kName[] = "MCDriverPort";
const char LVDSMC_MCDriverPort::kDescription[] = "LVDSMC MC_Driver Port";

const char LVDSMC_MCDriverPort::kClearDecoderErrorCounterName[] = "ClearDecoderErrorCounter";
const char LVDSMC_MCDriverPort::kClearDecoderErrorCounterDescription[] =
  "Clear the Decoder-error counter of this port.";

const char LVDSMC_MCDriverPort::kClearLiveSignErrorCounterName[] = "ClearLiveSignErrorCounter";
const char LVDSMC_MCDriverPort::kClearLiveSignErrorCounterDescription[] =
  "Clear the LiveSign-error counter of this port.";

LVDSMC_MCDriverPort::LVDSMC_MCDriverPort(
  Logger::ILogger *logger,
  const Modules::Configuration::ConfigurationCommon &configCommon,
  const Configuration::Configuration_Slots &config,
  const uint8_t &port)
  : Framework::Components::Container(
  logger,
  std::string(kName).append(std::to_string(port)),
  kDescription) {
// Set override container
  auto override = std::make_unique<LVDSMC_MCDriverOverride>(
    logger,
    configCommon,
    config,
    port);
  override_ = override.get();
  AddComponent(std::move(override));
// Set status container
  auto status = std::make_unique<LVDSMC_MCDriverStatus>(
    logger,
    configCommon,
    config,
    port);
  status_ = status.get();
  AddComponent(std::move(status));
// Create Clear DecoderError counter output
// clearDecoderErrorCounter_ = Create().ItemOut(
//    kClearDecoderErrorCounterName,
//    kClearDecoderErrorCounterDescription,
//    std::chrono::milliseconds(kClearCounterDelay),
//    true,
//    false).AddAndReturnPointer();
// clearDecoderErrorCounter_->CreateAccessor().ScuRegister(
//    config.Scu().MezzanineCardClearRegister1Address(),  // diese Art von Register gibt es aktuell noch nicht.
// Weiss nicht wie Martin Gassner das handhabt.
// 0);  // tbd: rejz: bit 1 für port2, bit 2 für port3
// Create Clear LiveSign counter output
// clearLiveSignErrorCounter_ = Create().ItemOut(
//    kClearLiveSignErrorCounterName,
//    kClearLiveSignErrorCounterDescription,
//    std::chrono::milliseconds(kClearCounterDelay),
//    true,
//    false).AddAndReturnPointer();
// clearLiveSignErrorCounter_->CreateAccessor().ScuRegister(
//    config.Scu().MezzanineCardClearRegister1Address(),  // diese Art von Register gibt es aktuell noch nicht.
// 3);  // tbd: rejz: bit 4 für port2, bit 5 für port3

  (void) config;
}

LVDSMC_MCDriverStatus *LVDSMC_MCDriverPort::Status() const {
  return status_;
}

LVDSMC_MCDriverOverride *LVDSMC_MCDriverPort::Override() const {
  return override_;
}

Framework::Components::ItemOut<bool> *LVDSMC_MCDriverPort::ClearDecoderErrorCounter() const {
  return clearDecoderErrorCounter_;
}

Framework::Components::ItemOut<bool> *LVDSMC_MCDriverPort::ClearLiveSignErrorCounter() const {
  return clearLiveSignErrorCounter_;
}

}  // namespace SystemModules::Modules::LVDSMC
