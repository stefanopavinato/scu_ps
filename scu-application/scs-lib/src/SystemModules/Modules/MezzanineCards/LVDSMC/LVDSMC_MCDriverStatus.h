/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "SystemModules/Framework/Components/ItemIn.h"
#include "SystemModules/Framework/Components/ItemOut.h"
#include "SystemModules/Framework/Components/Container.h"
#include "SystemModules/Modules/Configuration/ConfigurationCommon.h"
#include "SystemModules/Modules/Configuration/Slots/Configuration_Slots.h"

namespace SystemModules::Modules::LVDSMC {
class LVDSMC_MCDriverStatus : public Framework::Components::Container {
 public:
  LVDSMC_MCDriverStatus(
    Logger::ILogger *logger,
    const Modules::Configuration::ConfigurationCommon &configCommon,
    const Configuration::Configuration_Slots &config,
    const uint32_t &port);

  [[nodiscard]] Framework::Components::ItemIn<uint32_t> *MCDriverStatus() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *BeamAbsence() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *BeamPermit() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *Ready() const;

  [[nodiscard]] Framework::Components::ItemIn<uint8_t> *DatalinkBeamAbsence() const;

  [[nodiscard]] Framework::Components::ItemIn<uint8_t> *DatalinkBeamPermit() const;

  [[nodiscard]] Framework::Components::ItemIn<uint8_t> *DatalinkReady() const;

  [[nodiscard]] Framework::Components::ItemIn<uint8_t> *DatalinkHVAbsence() const;

  [[nodiscard]] Framework::Components::ItemIn<uint8_t> *DatalinkHVExtraction() const;

  [[nodiscard]] Framework::Components::ItemIn<uint8_t> *DatalinkPBD() const;

  [[nodiscard]] Framework::Components::ItemIn<uint8_t> *DatalinkPBM() const;

  [[nodiscard]] Framework::Components::ItemIn<uint32_t> *DatalinkDecoderErrorCounter() const;

  [[nodiscard]] Framework::Components::ItemIn<uint32_t> *DatalinkLifeSignErrorCounter() const;

  [[nodiscard]] Framework::Components::ItemIn<uint32_t> *DatalinkTimeExpectationTimeoutCounter() const;

  [[nodiscard]] Framework::Components::ItemOut<bool> *datalinkDecoderErrorCounterReset() const;

  [[nodiscard]] Framework::Components::ItemOut<bool> *datalinkLifeSignErrorCounterReset() const;

  [[nodiscard]] Framework::Components::ItemOut<bool> *datalinkTimeExpectationTimeoutCounterReset() const;

  [[nodiscard]] Framework::Components::ItemIn<uint16_t> *DatalinkSenderID() const;

  [[nodiscard]] Framework::Components::ItemIn<uint8_t> *LiveSignCounter() const;

 private:
  Framework::Components::ItemIn<uint32_t> *mcDriverStatus_;

  Framework::Components::ItemIn<bool> *beamAbsence_;

  Framework::Components::ItemIn<bool> *beamPermit_;

  Framework::Components::ItemIn<bool> *ready_;

  Framework::Components::ItemIn<uint8_t> *datalinkBeamAbsence_;

  Framework::Components::ItemIn<uint8_t> *datalinkBeamPermit_;

  Framework::Components::ItemIn<uint8_t> *datalinkReady_;

  Framework::Components::ItemIn<uint8_t> *datalinkHVAbsence_;

  Framework::Components::ItemIn<uint8_t> *datalinkHVExtraction_;

  Framework::Components::ItemIn<uint8_t> *datalinkPBD_;

  Framework::Components::ItemIn<uint8_t> *datalinkPBM_;

  Framework::Components::ItemIn<uint32_t> *datalinkDecoderErrorCounter_;

  Framework::Components::ItemIn<uint32_t> *datalinkLifeSignErrorCounter_;

  Framework::Components::ItemIn<uint32_t> *datalinkTimeExpectationTimeoutCounter_;

  Framework::Components::ItemOut<bool> *datalinkDecoderErrorCounterReset_;

  Framework::Components::ItemOut<bool> *datalinkLifeSignErrorCounterReset_;

  Framework::Components::ItemOut<bool> *datalinkTimeExpectationTimeoutCounterReset_;

  Framework::Components::ItemIn<uint16_t> *senderID_;

  Framework::Components::ItemIn<uint8_t> *liveSignCounter_;

  static const char kName[];
  static const char kDescription[];

  static const char kMCDriverStatusName[];
  static const char kMCDriverStatusDescription[];
  static const uint32_t kMCDriverStatusPosition;

  static const char kBeamAbsenceName[];
  static const char kBeamAbsenceDescription[];
  static const uint32_t kBeamAbsencePosition;
  static const uint32_t kBeamAbsenceSize;

  static const char kBeamPermitName[];
  static const char kBeamPermitDescription[];
  static const uint32_t kBeamPermitPosition;
  static const uint32_t kBeamPermitSize;

  static const char kReadyName[];
  static const char kReadyDescription[];
  static const uint32_t kReadyPosition;
  static const uint32_t kReadySize;

  static const char kDatalinkBeamAbsenceName[];
  static const char kDatalinkBeamAbsenceDescription[];
  static const uint32_t kDatalinkBeamAbsencePosition;
  static const uint32_t kDatalinkBeamAbsenceSize;

  static const char kDatalinkBeamPermitName[];
  static const char kDatalinkBeamPermitDescription[];
  static const uint32_t kDatalinkBeamPermitPosition;
  static const uint32_t kDatalinkBeamPermitSize;

  static const char kDatalinkReadyName[];
  static const char kDatalinkReadyDescription[];
  static const uint32_t kDatalinkReadyPosition;
  static const uint32_t kDatalinkReadySize;

  static const char kDatalinkHVAbsenceName[];
  static const char kDatalinkHVAbsenceDescription[];
  static const uint32_t kDatalinkHVAbsencePosition;
  static const uint32_t kDatalinkHVAbsenceSize;

  static const char kDatalinkHVExtractionName[];
  static const char kDatalinkHVExtractionDescription[];
  static const uint32_t kDatalinkHVExtractionPosition;
  static const uint32_t kDatalinkHVExtractionSize;

  static const char kDatalinkPBDName[];
  static const char kDatalinkPBDDescription[];
  static const uint32_t kDatalinkPBDPosition;
  static const uint32_t kDatalinkPBDSize;

  static const char kDatalinkPBMName[];
  static const char kDatalinkPBMDescription[];
  static const uint32_t kDatalinkPBMPosition;
  static const uint32_t kDatalinkPBMSize;

  static const char kDatalinkDecoderErrorCounterName[];
  static const char kDatalinkDecoderErrorCounterDescription[];
  static const uint32_t kDatalinkDecoderErrorCounterPosition;
  static const uint32_t kDatalinkDecoderErrorCounterSize;

  static const char kDatalinkLifeSignErrorCounterName[];
  static const char kDatalinkLifeSignErrorCounterDescription[];
  static const uint32_t kDatalinkLifeSignErrorCounterPosition;
  static const uint32_t kDatalinkLifeSignErrorCounterSize;

  static const char kDatalinkTimeExpectationTimeoutCounterName[];
  static const char kDatalinkTimeExpectationTimeoutCounterDescription[];
  static const uint32_t kDatalinkTimeExpectationTimeoutCounterPosition;
  static const uint32_t kDatalinkTimeExpectationTimeoutCounterSize;

  static const char kDatalinkDecoderErrorCounterResetName[];
  static const char kDatalinkDecoderErrorCounterResetDescription[];
  static const uint32_t kDatalinkDecoderErrorCounterResetSize;

  static const char kDatalinkLifeSignErrorCounterResetName[];
  static const char kDatalinkLifeSignErrorCounterResetDescription[];
  static const uint32_t kDatalinkLifeSignErrorCounterResetSize;

  static const char kDatalinkTimeExpectationTimeoutCounterResetName[];
  static const char kDatalinkTimeExpectationTimeoutCounterResetDescription[];
  static const uint32_t kDatalinkTimeExpectationTimeoutCounterResetSize;

  static const char kSenderIDName[];
  static const char kSenderIDDescription[];
  static const uint32_t kSenderIDPosition;
  static const uint32_t kSenderIDSize;

  static const char kLiveSignCounterName[];
  static const char kLiveSignCounterDescription[];
  static const uint32_t kLiveSignCounterPosition;
  static const uint32_t kLiveSignCounterSize;
};
}  // namespace SystemModules::Modules::LVDSMC
