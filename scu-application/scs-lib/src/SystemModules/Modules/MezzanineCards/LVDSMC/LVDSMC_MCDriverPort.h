/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <cstdint>
#include "SystemModules/Framework/Components/ItemIn.h"
#include "SystemModules/Framework/Components/Container.h"
#include "SystemModules/Modules/Configuration/ConfigurationCommon.h"
#include "SystemModules/Modules/Configuration/Slots/Configuration_Slots.h"

namespace SystemModules::Modules::LVDSMC {
class LVDSMC_MCDriverPort : public Framework::Components::Container {
 public:
  LVDSMC_MCDriverPort(
    Logger::ILogger *logger,
    const Modules::Configuration::ConfigurationCommon &configCommon,
    const Configuration::Configuration_Slots &config,
    const uint8_t &port);

  [[nodiscard]] LVDSMC_MCDriverStatus *Status() const;

  [[nodiscard]] LVDSMC_MCDriverOverride *Override() const;

  [[nodiscard]] Framework::Components::ItemOut<bool> *ClearDecoderErrorCounter() const;

  [[nodiscard]] Framework::Components::ItemOut<bool> *ClearLiveSignErrorCounter() const;


 private:
  LVDSMC_MCDriverStatus *status_;

  LVDSMC_MCDriverOverride *override_;

  Framework::Components::ItemOut<bool> *clearDecoderErrorCounter_;

  Framework::Components::ItemOut<bool> *clearLiveSignErrorCounter_;

  static const uint32_t kClearCounterDelay;

  static const char kName[];
  static const char kDescription[];

  static const char kClearDecoderErrorCounterName[];
  static const char kClearDecoderErrorCounterDescription[];

  static const char kClearLiveSignErrorCounterName[];
  static const char kClearLiveSignErrorCounterDescription[];
};

}  // namespace SystemModules::Modules::LVDSMC
