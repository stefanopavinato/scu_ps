/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "SystemModules/Framework/Components/Container.h"
#include "SystemModules/Modules/Configuration/ConfigurationCommon.h"
#include "SystemModules/Modules/Configuration/Modules/Configuration_LVDSMC.h"
#include "SystemModules/Modules/Configuration/Slots/Configuration_Slots.h"

namespace SystemModules::Modules {
namespace Common {
class VoltageMonitor_3V3_1V8;
class HotSwapController;
}  // namespace Common
namespace LVDSMC {
class LVDSMC_MCDriver;

class LVDSMC : public Framework::Components::Container {
 public:
  LVDSMC(
    Logger::ILogger *logger,
    const Modules::Configuration::Modules::Configuration_LVDSMC &configLVDSMC,
    const Modules::Configuration::ConfigurationCommon &configCommon,
    const Modules::Configuration::Configuration_Slots &config);

  [[nodiscard]] LVDSMC_MCDriver *MCDriver() const;

  [[nodiscard]] Common::VoltageMonitor_3V3_1V8 *VoltageMonitor() const;

  [[nodiscard]] Modules::Common::HotSwapController *HotSwapController() const;

 private:
  LVDSMC_MCDriver *mcDriver_;

  Common::VoltageMonitor_3V3_1V8 *voltageMonitor_;

  Modules::Common::HotSwapController *hotSwapController_;
};
}  // namespace LVDSMC
}  // namespace SystemModules::Modules
