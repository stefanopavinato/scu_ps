/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Logger/ILogger.h"
#include "SystemModules/Framework/Actions/ActionTemplate.h"
#include "SystemModules/Framework/Builder/Actions/ActionBuilder.h"
#include "SystemModules/Framework/MonitoringFunctions/MFTemplate.h"
#include "SystemModules/Framework/Builder/Accessors/WriteAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Accessors/ReadAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemInBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemOutBuilder.h"
#include "SystemModules/Framework/Builder/Components/ComponentBuilder.h"
#include "LVDSMC_MCDriverStatus.h"

namespace SystemModules::Modules::LVDSMC {

const char LVDSMC_MCDriverStatus::kName[] = "Status";
const char LVDSMC_MCDriverStatus::kDescription[] =
  "Status of signals the MCDriver receives from one single port of the MC.";

const char LVDSMC_MCDriverStatus::kMCDriverStatusName[] = "StatusItemCollection";
const char LVDSMC_MCDriverStatus::kMCDriverStatusDescription[] = "Collection of LVDSMC status items.";
const uint32_t LVDSMC_MCDriverStatus::kMCDriverStatusPosition = 0;

const char LVDSMC_MCDriverStatus::kBeamAbsenceName[] = "BeamAbsence";
const char LVDSMC_MCDriverStatus::kBeamAbsenceDescription[] =
  "Status of the discrete Beam Absence signal as received from the MC (raw value).";
const uint32_t LVDSMC_MCDriverStatus::kBeamAbsencePosition = 0;
const uint32_t LVDSMC_MCDriverStatus::kBeamAbsenceSize = 1;

const char LVDSMC_MCDriverStatus::kBeamPermitName[] = "BeamPermit";
const char LVDSMC_MCDriverStatus::kBeamPermitDescription[] =
  "Status of the discrete Beam Permit signal as received from the MC (raw value).";
const uint32_t LVDSMC_MCDriverStatus::kBeamPermitPosition = 1;
const uint32_t LVDSMC_MCDriverStatus::kBeamPermitSize = 1;

const char LVDSMC_MCDriverStatus::kReadyName[] = "Ready";
const char LVDSMC_MCDriverStatus::kReadyDescription[] =
  "Status of the discrete Ready signal as received from the MC (raw value).";
const uint32_t LVDSMC_MCDriverStatus::kReadyPosition = 2;
const uint32_t LVDSMC_MCDriverStatus::kReadySize = 1;

const char LVDSMC_MCDriverStatus::kDatalinkBeamAbsenceName[] = "DatalinkBeamAbsence";
const char LVDSMC_MCDriverStatus::kDatalinkBeamAbsenceDescription[] =
  "Status of the Datalink Beam Absence signal as received from the MC (raw value).";
const uint32_t LVDSMC_MCDriverStatus::kDatalinkBeamAbsencePosition = 3;
const uint32_t LVDSMC_MCDriverStatus::kDatalinkBeamAbsenceSize = 2;

const char LVDSMC_MCDriverStatus::kDatalinkBeamPermitName[] = "DatalinkBeamPermit";
const char LVDSMC_MCDriverStatus::kDatalinkBeamPermitDescription[] =
  "Status of the Datalink Beam Permit signal as received from the MC (raw value).";
const uint32_t LVDSMC_MCDriverStatus::kDatalinkBeamPermitPosition = 5;
const uint32_t LVDSMC_MCDriverStatus::kDatalinkBeamPermitSize = 2;

const char LVDSMC_MCDriverStatus::kDatalinkReadyName[] = "DatalinkReady";
const char LVDSMC_MCDriverStatus::kDatalinkReadyDescription[] =
  "Status of the Datalink Ready signal as received from the MC (raw value).";
const uint32_t LVDSMC_MCDriverStatus::kDatalinkReadyPosition = 8;
const uint32_t LVDSMC_MCDriverStatus::kDatalinkReadySize = 2;

const char LVDSMC_MCDriverStatus::kDatalinkHVAbsenceName[] = "DatalinkHVAbsence";
const char LVDSMC_MCDriverStatus::kDatalinkHVAbsenceDescription[] =
  "Status of the Datalink HV Absence signal as received from the MC (raw value).";
const uint32_t LVDSMC_MCDriverStatus::kDatalinkHVAbsencePosition = 10;
const uint32_t LVDSMC_MCDriverStatus::kDatalinkHVAbsenceSize = 2;

const char LVDSMC_MCDriverStatus::kDatalinkHVExtractionName[] = "DatalinkHVExtraction";
const char LVDSMC_MCDriverStatus::kDatalinkHVExtractionDescription[] =
  "Status of the Datalink HV Extraction signal as received from the MC (raw value).";
const uint32_t LVDSMC_MCDriverStatus::kDatalinkHVExtractionPosition = 12;
const uint32_t LVDSMC_MCDriverStatus::kDatalinkHVExtractionSize = 2;

const char LVDSMC_MCDriverStatus::kDatalinkPBDName[] = "DatalinkPBD";
const char LVDSMC_MCDriverStatus::kDatalinkPBDDescription[] =
  "Status of the Datalink PBD signal as received from the MC (raw value).";
const uint32_t LVDSMC_MCDriverStatus::kDatalinkPBDPosition = 16;
const uint32_t LVDSMC_MCDriverStatus::kDatalinkPBDSize = 8;

const char LVDSMC_MCDriverStatus::kDatalinkPBMName[] = "DatalinkPBM";
const char LVDSMC_MCDriverStatus::kDatalinkPBMDescription[] =
  "Status of the Datalink PBM signal as received from the MC (raw value).";
const uint32_t LVDSMC_MCDriverStatus::kDatalinkPBMPosition = 24;
const uint32_t LVDSMC_MCDriverStatus::kDatalinkPBMSize = 8;

const char LVDSMC_MCDriverStatus::kDatalinkDecoderErrorCounterName[] = "DatalinkDecoderErrorCounter";
const char LVDSMC_MCDriverStatus::kDatalinkDecoderErrorCounterDescription[] =
  "Decoder error counter value received via the Datalink.";
const uint32_t LVDSMC_MCDriverStatus::kDatalinkDecoderErrorCounterPosition = 0;
const uint32_t LVDSMC_MCDriverStatus::kDatalinkDecoderErrorCounterSize = 32;

const char LVDSMC_MCDriverStatus::kDatalinkLifeSignErrorCounterName[] = "DatalinkLifeSignErrorCounter";
const char LVDSMC_MCDriverStatus::kDatalinkLifeSignErrorCounterDescription[] =
  "Life sign error counter value received via the Datalink.";
const uint32_t LVDSMC_MCDriverStatus::kDatalinkLifeSignErrorCounterPosition = 0;
const uint32_t LVDSMC_MCDriverStatus::kDatalinkLifeSignErrorCounterSize = 32;

const char LVDSMC_MCDriverStatus::kDatalinkTimeExpectationTimeoutCounterName[] =
  "DatalinkTimeExpectationTimeoutCounter";
const char LVDSMC_MCDriverStatus::kDatalinkTimeExpectationTimeoutCounterDescription[] =
  "Time expectation timeout counter via the Datalink";
const uint32_t LVDSMC_MCDriverStatus::kDatalinkTimeExpectationTimeoutCounterPosition = 0;
const uint32_t LVDSMC_MCDriverStatus::kDatalinkTimeExpectationTimeoutCounterSize = 32;

const char LVDSMC_MCDriverStatus::kDatalinkDecoderErrorCounterResetName[] = "DatalinkDecoderErrorCounterReset";
const char LVDSMC_MCDriverStatus::kDatalinkDecoderErrorCounterResetDescription[] =
  "Decoder error counter reset.";
const uint32_t LVDSMC_MCDriverStatus::kDatalinkDecoderErrorCounterResetSize = 1;

const char LVDSMC_MCDriverStatus::kDatalinkLifeSignErrorCounterResetName[] = "DatalinkLifeSignErrorCounterReset";
const char LVDSMC_MCDriverStatus::kDatalinkLifeSignErrorCounterResetDescription[] =
  "Life sign error counter reset.";
const uint32_t LVDSMC_MCDriverStatus::kDatalinkLifeSignErrorCounterResetSize = 1;

const char LVDSMC_MCDriverStatus::kDatalinkTimeExpectationTimeoutCounterResetName[] =
  "DatalinkTimeExpectationTimeoutCounterReset";
const char LVDSMC_MCDriverStatus::kDatalinkTimeExpectationTimeoutCounterResetDescription[] =
  "Time expectation timeout counter reset";
const uint32_t LVDSMC_MCDriverStatus::kDatalinkTimeExpectationTimeoutCounterResetSize = 1;

const char LVDSMC_MCDriverStatus::kSenderIDName[] = "SenderID";
const char LVDSMC_MCDriverStatus::kSenderIDDescription[] =
  "Sender ID (System ID and System Index) received via the Datalink.";
const uint32_t LVDSMC_MCDriverStatus::kSenderIDPosition = 0;
const uint32_t LVDSMC_MCDriverStatus::kSenderIDSize = 16;

const char LVDSMC_MCDriverStatus::kLiveSignCounterName[] = "LiveSignCounter";
const char LVDSMC_MCDriverStatus::kLiveSignCounterDescription[] =
    "LiveSign Counter value received via the Datalink.";
const uint32_t LVDSMC_MCDriverStatus::kLiveSignCounterPosition = 16;
const uint32_t LVDSMC_MCDriverStatus::kLiveSignCounterSize = 8;

LVDSMC_MCDriverStatus::LVDSMC_MCDriverStatus(
  Logger::ILogger *logger,
  const Modules::Configuration::ConfigurationCommon &configCommon,
  const Configuration::Configuration_Slots &config,
  const uint32_t &port)
  : Framework::Components::Container(
  logger,
  kName,
  kDescription) {
  uint64_t statusRegisterAddress;
  uint64_t datalinkSenderIDSystemAddress;
  uint64_t datalinkDecoderErrorCounterAddress;
  uint64_t datalinkLifeSignErrorCounterAddress;
  uint64_t datalinkTimeExpectationTimeoutAddress;
  uint32_t datalinkDecoderErrorCounterResetPosition;
  uint32_t datalinkLifeSignErrorCounterResetPosition;
  uint32_t datalinkTimeExpectationTimeoutCounterResetPosition;
  // Get update intervaval
  auto updateIntervalDiscreteSignals = configCommon.UpdateInterval()->Medium()->Value();
  auto updateIntervalDatalink = configCommon.UpdateInterval()->Medium()->Value();
  // Setup ports
  if (port == 1) {
    statusRegisterAddress = config.Scu()->Address()->StatusRegister1Address()->Value();
    datalinkSenderIDSystemAddress = config.Scu()->Address()->StatusRegister4Address()->Value();
    datalinkDecoderErrorCounterAddress = config.Scu()->Address()->StatusRegister7Address()->Value();
    datalinkLifeSignErrorCounterAddress = config.Scu()->Address()->StatusRegister10Address()->Value();
    datalinkTimeExpectationTimeoutAddress = config.Scu()->Address()->StatusRegister13Address()->Value();
    datalinkDecoderErrorCounterResetPosition = 0;
    datalinkLifeSignErrorCounterResetPosition = 3;
    datalinkTimeExpectationTimeoutCounterResetPosition = 6;
  } else if (port == 2) {
    statusRegisterAddress = config.Scu()->Address()->StatusRegister2Address()->Value();
    datalinkSenderIDSystemAddress = config.Scu()->Address()->StatusRegister5Address()->Value();
    datalinkDecoderErrorCounterAddress = config.Scu()->Address()->StatusRegister8Address()->Value();
    datalinkLifeSignErrorCounterAddress = config.Scu()->Address()->StatusRegister11Address()->Value();
    datalinkTimeExpectationTimeoutAddress = config.Scu()->Address()->StatusRegister14Address()->Value();
    datalinkDecoderErrorCounterResetPosition = 1;
    datalinkLifeSignErrorCounterResetPosition = 4;
    datalinkTimeExpectationTimeoutCounterResetPosition = 7;
  } else if (port == 3) {
    statusRegisterAddress = config.Scu()->Address()->StatusRegister3Address()->Value();
    datalinkSenderIDSystemAddress = config.Scu()->Address()->StatusRegister6Address()->Value();
    datalinkDecoderErrorCounterAddress = config.Scu()->Address()->StatusRegister9Address()->Value();
    datalinkLifeSignErrorCounterAddress = config.Scu()->Address()->StatusRegister12Address()->Value();
    datalinkTimeExpectationTimeoutAddress = config.Scu()->Address()->StatusRegister15Address()->Value();
    datalinkDecoderErrorCounterResetPosition = 2;
    datalinkLifeSignErrorCounterResetPosition = 5;
    datalinkTimeExpectationTimeoutCounterResetPosition = 8;
  } else {
    Logger()->Error("Port number not valid.");
    return;
  }
  // Setup item MCDriverStatus
  mcDriverStatus_ = Create().ItemIn<uint32_t>(
    kMCDriverStatusName,
    kMCDriverStatusDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDiscreteSignals),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  mcDriverStatus_->CreateAccessor().ScuRegister(
    statusRegisterAddress,
    kMCDriverStatusPosition,
    32);
  // Setup Beam Absence status input
  beamAbsence_ = Create().ItemIn<bool>(
    kBeamAbsenceName,
    kBeamAbsenceDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDiscreteSignals),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  beamAbsence_->CreateAccessor().ScuRegister(
    statusRegisterAddress,
    kBeamAbsencePosition,
    kBeamAbsenceSize);
  // Setup Beam Permit status input
  beamPermit_ = Create().ItemIn<bool>(
    kBeamPermitName,
    kBeamPermitDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDiscreteSignals),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  beamPermit_->CreateAccessor().ScuRegister(
    statusRegisterAddress,
    kBeamPermitPosition,
    kBeamPermitSize);
  // Setup Ready status input
  ready_ = Create().ItemIn<bool>(
    kReadyName,
    kReadyDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDiscreteSignals),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  ready_->CreateAccessor().ScuRegister(
    statusRegisterAddress,
    kReadyPosition,
    kReadySize);
  // Setup Datalink Beam Absence status input
  datalinkBeamAbsence_ = Create().ItemIn<uint8_t>(
    kDatalinkBeamAbsenceName,
    kDatalinkBeamAbsenceDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDatalink),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  datalinkBeamAbsence_->CreateAccessor().ScuRegister(
    statusRegisterAddress,
    kDatalinkBeamAbsencePosition,
    kDatalinkBeamAbsenceSize);
  // Setup Datalink Beam Permit status input
  datalinkBeamPermit_ = Create().ItemIn<uint8_t>(
    kDatalinkBeamPermitName,
    kDatalinkBeamPermitDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDatalink),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  datalinkBeamPermit_->CreateAccessor().ScuRegister(
    statusRegisterAddress,
    kDatalinkBeamPermitPosition,
    kDatalinkBeamPermitSize);
  // Setup Datalink Ready status input
  datalinkReady_ = Create().ItemIn<uint8_t>(
    kDatalinkReadyName,
    kDatalinkReadyDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDatalink),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  datalinkReady_->CreateAccessor().ScuRegister(
    statusRegisterAddress,
    kDatalinkReadyPosition,
    kDatalinkReadySize);
  // Setup Datalink HV Absence status input
  datalinkHVAbsence_ = Create().ItemIn<uint8_t>(
    kDatalinkHVAbsenceName,
    kDatalinkHVAbsenceDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDatalink),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  datalinkHVAbsence_->CreateAccessor().ScuRegister(
    statusRegisterAddress,
    kDatalinkHVAbsencePosition,
    kDatalinkHVAbsenceSize);
  // Setup Datalink HV Extraction status input
  datalinkHVExtraction_ = Create().ItemIn<uint8_t>(
    kDatalinkHVExtractionName,
    kDatalinkHVExtractionDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDatalink),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  datalinkHVExtraction_->CreateAccessor().ScuRegister(
    statusRegisterAddress,
    kDatalinkHVExtractionPosition,
    kDatalinkHVExtractionSize);
  // Setup Datalink PBD status input
  datalinkPBD_ = Create().ItemIn<uint8_t>(
    kDatalinkPBDName,
    kDatalinkPBDDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDatalink),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  datalinkPBD_->CreateAccessor().ScuRegister(
    statusRegisterAddress,
    kDatalinkPBDPosition,
    kDatalinkPBDSize);
  // Setup Datalink PBM status input
  datalinkPBM_ = Create().ItemIn<uint8_t>(
    kDatalinkPBMName,
    kDatalinkPBMDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDatalink),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  datalinkPBM_->CreateAccessor().ScuRegister(
    statusRegisterAddress,
    kDatalinkPBMPosition,
    kDatalinkPBMSize);
  // Setup datalink decoder error counter
  datalinkDecoderErrorCounter_ = Create().ItemIn<uint32_t>(
    kDatalinkDecoderErrorCounterName,
    kDatalinkDecoderErrorCounterDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDatalink),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  datalinkDecoderErrorCounter_->CreateAccessor().ScuRegister(
    datalinkDecoderErrorCounterAddress,
    kDatalinkDecoderErrorCounterPosition,
    kDatalinkDecoderErrorCounterSize);
  // Setup datalink decoder error counter
  datalinkLifeSignErrorCounter_ = Create().ItemIn<uint32_t>(
    kDatalinkLifeSignErrorCounterName,
    kDatalinkLifeSignErrorCounterDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDatalink),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  datalinkLifeSignErrorCounter_->CreateAccessor().ScuRegister(
    datalinkLifeSignErrorCounterAddress,
    kDatalinkLifeSignErrorCounterPosition,
    kDatalinkLifeSignErrorCounterSize);
  // Setup datalink time expectation timeout counter
  datalinkTimeExpectationTimeoutCounter_ = Create().ItemIn<uint32_t>(
    kDatalinkTimeExpectationTimeoutCounterName,
    kDatalinkTimeExpectationTimeoutCounterDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDatalink),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  datalinkTimeExpectationTimeoutCounter_->CreateAccessor().ScuRegister(
    datalinkTimeExpectationTimeoutAddress,
    kDatalinkTimeExpectationTimeoutCounterPosition,
    kDatalinkTimeExpectationTimeoutCounterSize);

  // Setup datalink decoder error counter reset
  datalinkDecoderErrorCounterReset_ = Create().ItemOut<bool>(
    kDatalinkDecoderErrorCounterResetName,
    kDatalinkDecoderErrorCounterResetDescription,
    std::chrono::milliseconds(updateIntervalDatalink),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    false,
    false).AddAndReturnPointer();
  datalinkDecoderErrorCounterReset_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ClearRegister1Address()->Value(),
    kDatalinkDecoderErrorCounterPosition,
    kDatalinkLifeSignErrorCounterResetSize);
  datalinkDecoderErrorCounterReset_->CreateAction().StaticValue(false);
  // Setup datalink decoder error counter reset
  datalinkLifeSignErrorCounterReset_ = Create().ItemOut<bool>(
    kDatalinkLifeSignErrorCounterResetName,
    kDatalinkLifeSignErrorCounterResetDescription,
    std::chrono::milliseconds(updateIntervalDatalink),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    false,
    false).AddAndReturnPointer();
  datalinkLifeSignErrorCounterReset_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ClearRegister1Address()->Value(),
    datalinkLifeSignErrorCounterResetPosition,
    kDatalinkLifeSignErrorCounterResetSize);
  datalinkLifeSignErrorCounterReset_->CreateAction().StaticValue(false);
  // Setup datalink time expectation timeout counter reset
  datalinkTimeExpectationTimeoutCounterReset_ = Create().ItemOut<bool>(
    kDatalinkTimeExpectationTimeoutCounterResetName,
    kDatalinkTimeExpectationTimeoutCounterResetDescription,
    std::chrono::milliseconds(updateIntervalDatalink),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    false,
    false).AddAndReturnPointer();
  datalinkTimeExpectationTimeoutCounterReset_->CreateAction().StaticValue(false);
  datalinkTimeExpectationTimeoutCounterReset_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ClearRegister1Address()->Value(),
    datalinkTimeExpectationTimeoutCounterResetPosition,
    kDatalinkTimeExpectationTimeoutCounterResetSize);
  // Setup Sender ID
  senderID_ = Create().ItemIn<uint16_t>(
    kSenderIDName,
    kSenderIDDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDatalink),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  senderID_->CreateAccessor().ScuRegister(
    datalinkSenderIDSystemAddress,
    kSenderIDPosition,
    kSenderIDSize);
  // Setup LiveSign counter of the port
  liveSignCounter_ = Create().ItemIn<uint8_t>(
      kLiveSignCounterName,
      kLiveSignCounterDescription,
      Types::ValueGroup::Enum::NONE,
      std::chrono::milliseconds(updateIntervalDatalink),
      Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  liveSignCounter_->CreateAccessor().ScuRegister(
      datalinkSenderIDSystemAddress,
      kLiveSignCounterPosition,
      kLiveSignCounterSize);
}

Framework::Components::ItemIn<uint32_t> *LVDSMC_MCDriverStatus::MCDriverStatus() const {
  return mcDriverStatus_;
}

Framework::Components::ItemIn<bool> *LVDSMC_MCDriverStatus::BeamAbsence() const {
  return beamAbsence_;
}

Framework::Components::ItemIn<bool> *LVDSMC_MCDriverStatus::BeamPermit() const {
  return beamPermit_;
}

Framework::Components::ItemIn<bool> *LVDSMC_MCDriverStatus::Ready() const {
  return ready_;
}

Framework::Components::ItemIn<uint8_t> *LVDSMC_MCDriverStatus::DatalinkBeamAbsence() const {
  return datalinkBeamAbsence_;
}

Framework::Components::ItemIn<uint8_t> *LVDSMC_MCDriverStatus::DatalinkBeamPermit() const {
  return datalinkBeamPermit_;
}

Framework::Components::ItemIn<uint8_t> *LVDSMC_MCDriverStatus::DatalinkReady() const {
  return datalinkReady_;
}

Framework::Components::ItemIn<uint8_t> *LVDSMC_MCDriverStatus::DatalinkHVAbsence() const {
  return datalinkHVAbsence_;
}

Framework::Components::ItemIn<uint8_t> *LVDSMC_MCDriverStatus::DatalinkHVExtraction() const {
  return datalinkHVExtraction_;
}

Framework::Components::ItemIn<uint8_t> *LVDSMC_MCDriverStatus::DatalinkPBD() const {
  return datalinkPBD_;
}

Framework::Components::ItemIn<uint8_t> *LVDSMC_MCDriverStatus::DatalinkPBM() const {
  return datalinkPBM_;
}


Framework::Components::ItemIn<uint32_t> *LVDSMC_MCDriverStatus::DatalinkDecoderErrorCounter() const {
  return datalinkDecoderErrorCounter_;
}

Framework::Components::ItemIn<uint32_t> *LVDSMC_MCDriverStatus::DatalinkLifeSignErrorCounter() const {
  return datalinkLifeSignErrorCounter_;
}

Framework::Components::ItemIn<uint32_t> *LVDSMC_MCDriverStatus::DatalinkTimeExpectationTimeoutCounter() const {
  return datalinkTimeExpectationTimeoutCounter_;
}

Framework::Components::ItemOut<bool> *LVDSMC_MCDriverStatus::datalinkDecoderErrorCounterReset() const {
  return datalinkLifeSignErrorCounterReset_;
}

Framework::Components::ItemOut<bool> *LVDSMC_MCDriverStatus::datalinkLifeSignErrorCounterReset() const {
  return datalinkLifeSignErrorCounterReset_;
}

Framework::Components::ItemOut<bool> *LVDSMC_MCDriverStatus::datalinkTimeExpectationTimeoutCounterReset() const {
  return datalinkTimeExpectationTimeoutCounterReset_;
}

Framework::Components::ItemIn<uint16_t> *LVDSMC_MCDriverStatus::DatalinkSenderID() const {
  return senderID_;
}

Framework::Components::ItemIn<uint8_t> *LVDSMC_MCDriverStatus::LiveSignCounter() const {
  return liveSignCounter_;
}

}  // namespace SystemModules::Modules::LVDSMC
