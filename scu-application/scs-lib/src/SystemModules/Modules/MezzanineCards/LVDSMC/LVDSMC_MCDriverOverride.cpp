/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Logger/ILogger.h"
#include "SystemModules/Framework/Components/ItemIn.h"
#include "SystemModules/Framework/Components/ItemOut.h"
#include "SystemModules/Framework/Builder/Accessors/ReadAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemInBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemOutBuilder.h"
#include "SystemModules/Framework/Builder/Components/ComponentBuilder.h"
#include "LVDSMC_MCDriverOverride.h"

namespace SystemModules::Modules::LVDSMC {

const char LVDSMC_MCDriverOverride::kName[] = "SignalOverride";
const char LVDSMC_MCDriverOverride::kDescription[] =
  "Allows to override individual LVDSMC input signals by means of an error-flag";

const char LVDSMC_MCDriverOverride::kDiscreteSignalOverrideCollectionName[] = "DiscreteSignalOverrideCollection";
const char LVDSMC_MCDriverOverride::kDiscreteSignalOverrideCollectionDescription[] =
  "Collection of items related to the Overriding of discrete signals.";
const uint32_t LVDSMC_MCDriverOverride::kDiscreteSignalOverrideCollectionPosition = 0;

const char LVDSMC_MCDriverOverride::kDatalinkSignalOverrideCollectionName[] = "DatalinkSignalOverrideCollection";
const char LVDSMC_MCDriverOverride::kDatalinkSignalOverrideCollectionDescription[] =
  "Collection of items related to the Overriding of datalink signals.";
const uint32_t LVDSMC_MCDriverOverride::kDatalinkSignalOverrideCollectionPosition = 0;

const char LVDSMC_MCDriverOverride::kBeamAbsenceName[] = "BeamAbsence";
const char LVDSMC_MCDriverOverride::kBeamAbsenceDescription[] =
  "Override discrete Beam Absence signal. False==Do not override; True==Override with 'err-flag'.";
const uint32_t LVDSMC_MCDriverOverride::kBeamAbsencePosition = 0;

const char LVDSMC_MCDriverOverride::kBeamPermitName[] = "BeamPermit";
const char LVDSMC_MCDriverOverride::kBeamPermitDescription[] =
  "Override discrete Beam Permit signal. False==Do not override; True==Override with 'err-flag'.";
const uint32_t LVDSMC_MCDriverOverride::kBeamPermitPosition = 1;

const char LVDSMC_MCDriverOverride::kReadyName[] = "Ready";
const char LVDSMC_MCDriverOverride::kReadyDescription[] =
  "Override discrete Ready signal. False==Do not override; True==Override with 'err-flag'.";
const uint32_t LVDSMC_MCDriverOverride::kReadyPosition = 2;

const char LVDSMC_MCDriverOverride::kDatalinkBeamAbsenceName[] = "DatalinkBeamAbsence";
const char LVDSMC_MCDriverOverride::kDatalinkBeamAbsenceDescription[] =
  "Override Datalink Beam Absence signal. False==Do not override; True==Override with 'err-flag'.";
const uint32_t LVDSMC_MCDriverOverride::kDatalinkBeamAbsencePosition = 0;

const char LVDSMC_MCDriverOverride::kDatalinkBeamPermitName[] = "DatalinkBeamPermit";
const char LVDSMC_MCDriverOverride::kDatalinkBeamPermitDescription[] =
  "Override Datalink Beam Permit signal. False==Do not override; True==Override with 'err-flag'.";
const uint32_t LVDSMC_MCDriverOverride::kDatalinkBeamPermitPosition = 1;

const char LVDSMC_MCDriverOverride::kDatalinkReadyName[] = "DatalinkReady";
const char LVDSMC_MCDriverOverride::kDatalinkReadyDescription[] =
  "Override Datalink Ready signal. False==Do not override; True==Override with 'err-flag'.";
const uint32_t LVDSMC_MCDriverOverride::kDatalinkReadyPosition = 2;

const char LVDSMC_MCDriverOverride::kDatalinkHVAbsenceName[] = "DatalinkHVAbsence";
const char LVDSMC_MCDriverOverride::kDatalinkHVAbsenceDescription[] =
  "Override Datalink HV Absence signal. False==Do not override; True==Override with 'err-flag'.";
const uint32_t LVDSMC_MCDriverOverride::kDatalinkHVAbsencePosition = 3;

const char LVDSMC_MCDriverOverride::kDatalinkHVExtractionName[] = "DatalinkHVExtraction";
const char LVDSMC_MCDriverOverride::kDatalinkHVExtractionDescription[] =
  "Override Datalink HV Extraction signal. False==Do not override; True==Override with 'err-flag'.";
const uint32_t LVDSMC_MCDriverOverride::kDatalinkHVExtractionPosition = 4;

const char LVDSMC_MCDriverOverride::kDatalinkPBDName[] = "DatalinkPBD";
const char LVDSMC_MCDriverOverride::kDatalinkPBDDescription[] =
  "Override Datalink PBD signal. False==Do not override; True==Override with 'err-flag'.";
const uint32_t LVDSMC_MCDriverOverride::kDatalinkPBDPosition = 5;

const char LVDSMC_MCDriverOverride::kDatalinkPBMName[] = "DatalinkPBM";
const char LVDSMC_MCDriverOverride::kDatalinkPBMDescription[] =
  "Override Datalink PBM signal. False==Do not override; True==Override with 'err-flag'.";
const uint32_t LVDSMC_MCDriverOverride::kDatalinkPBMPosition = 6;

LVDSMC_MCDriverOverride::LVDSMC_MCDriverOverride(
  Logger::ILogger *logger,
  const Modules::Configuration::ConfigurationCommon &configCommon,
  const Configuration::Configuration_Slots &config,
  const uint8_t &port)
  : Framework::Components::Container(
  logger,
  kName,
  kDescription) {
  // Get update intervaval
  auto updateInterval = configCommon.UpdateInterval()->Fast()->Value();
  // Setup Input Signal item collection
  discreteSignalOverrideCollection_ = Create().ItemIn<uint8_t>(
    kDiscreteSignalOverrideCollectionName,
    kDiscreteSignalOverrideCollectionDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  discreteSignalOverrideCollection_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ReadWriteRegister1Address()->Value(),
    kDiscreteSignalOverrideCollectionPosition + (port - 1) * 8,
    8);
  datalinkSignalOverrideCollection_ = Create().ItemIn<uint8_t>(
    kDatalinkSignalOverrideCollectionName,
    kDatalinkSignalOverrideCollectionDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  datalinkSignalOverrideCollection_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ReadWriteRegister2Address()->Value(),
    kDatalinkSignalOverrideCollectionPosition + (port - 1) * 8,
    8);
  // Setup beam absence override output
  beamAbsence_ = Create().ItemOut<bool>(
    kBeamAbsenceName,
    kBeamAbsenceDescription,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    true,
    false).AddAndReturnPointer();
  beamAbsence_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ReadWriteRegister1Address()->Value(),
    kBeamAbsencePosition + (port - 1) * 8,
    1);
  // Setup beam permit override output
  beamPermit_ = Create().ItemOut<bool>(
    kBeamPermitName,
    kBeamPermitDescription,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    true,
    false).AddAndReturnPointer();
  beamPermit_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ReadWriteRegister1Address()->Value(),
    kBeamPermitPosition + (port - 1) * 8,
    1);
  // Setup beam absence override output
  ready_ = Create().ItemOut<bool>(
    kReadyName,
    kReadyDescription,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    true,
    false).AddAndReturnPointer();
  ready_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ReadWriteRegister1Address()->Value(),
    kReadyPosition + (port - 1) * 8,
    1);
  // Setup datalink beam absence override output
  datalinkBeamAbsence_ = Create().ItemOut<bool>(
    kDatalinkBeamAbsenceName,
    kDatalinkBeamAbsenceDescription,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    true,
    false).AddAndReturnPointer();
  datalinkBeamAbsence_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ReadWriteRegister2Address()->Value(),
    kDatalinkBeamAbsencePosition + (port - 1) * 8,
    1);
  // Setup datalink beam permit override output
  datalinkBeamPermit_ = Create().ItemOut<bool>(
    kDatalinkBeamPermitName,
    kDatalinkBeamPermitDescription,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    true,
    false).AddAndReturnPointer();
  datalinkBeamPermit_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ReadWriteRegister2Address()->Value(),
    kDatalinkBeamPermitPosition + (port - 1) * 8,
    1);
  // Setup datalink ready override output
  datalinkReady_ = Create().ItemOut<bool>(
    kDatalinkReadyName,
    kDatalinkReadyDescription,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    true,
    false).AddAndReturnPointer();
  datalinkReady_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ReadWriteRegister2Address()->Value(),
    kDatalinkReadyPosition + (port - 1) * 8,
    1);
  // Setup datalink HV absence override output
  datalinkHVAbsence_ = Create().ItemOut<bool>(
    kDatalinkHVAbsenceName,
    kDatalinkHVAbsenceDescription,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    true,
    false).AddAndReturnPointer();
  datalinkHVAbsence_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ReadWriteRegister2Address()->Value(),
    kDatalinkHVAbsencePosition + (port - 1) * 8,
    1);
  // Setup datalink HV extraction override output
  datalinkHVExtraction_ = Create().ItemOut<bool>(
    kDatalinkHVExtractionName,
    kDatalinkHVExtractionDescription,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    true,
    false).AddAndReturnPointer();
  datalinkHVExtraction_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ReadWriteRegister2Address()->Value(),
    kDatalinkHVExtractionPosition + (port - 1) * 8,
    1);
  // Setup datalink PBD override output
  datalinkPBD_ = Create().ItemOut<bool>(
    kDatalinkPBDName,
    kDatalinkPBDDescription,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    true,
    false).AddAndReturnPointer();
  datalinkPBD_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ReadWriteRegister2Address()->Value(),
    kDatalinkPBDPosition + (port - 1) * 8,
    1);
  // Setup datalink PBM override output
  datalinkPBM_ = Create().ItemOut<bool>(
    kDatalinkPBMName,
    kDatalinkPBMDescription,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    true,
    false).AddAndReturnPointer();
  datalinkPBM_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ReadWriteRegister2Address()->Value(),
    kDatalinkPBMPosition + (port - 1) * 8,
    1);

  (void) config;
  (void) port;
}

Framework::Components::ItemIn<uint8_t> *LVDSMC_MCDriverOverride::DiscreteSignalOverrideCollection() const {
  return discreteSignalOverrideCollection_;
}

Framework::Components::ItemIn<uint8_t> *LVDSMC_MCDriverOverride::DatalinkSignalOverrideCollection() const {
  return datalinkSignalOverrideCollection_;
}

Framework::Components::ItemOut<bool> *LVDSMC_MCDriverOverride::BeamAbsence() const {
  return beamAbsence_;
}

Framework::Components::ItemOut<bool> *LVDSMC_MCDriverOverride::BeamPermit() const {
  return beamPermit_;
}

Framework::Components::ItemOut<bool> *LVDSMC_MCDriverOverride::Ready() const {
  return ready_;
}

Framework::Components::ItemOut<bool> *LVDSMC_MCDriverOverride::DatalinkBeamPermit() const {
  return datalinkBeamPermit_;
}

Framework::Components::ItemOut<bool> *LVDSMC_MCDriverOverride::DatalinkReady() const {
  return datalinkReady_;
}

Framework::Components::ItemOut<bool> *LVDSMC_MCDriverOverride::DatalinkBeamAbsence() const {
  return datalinkBeamAbsence_;
}

Framework::Components::ItemOut<bool> *LVDSMC_MCDriverOverride::DatalinkHVAbsence() const {
  return datalinkHVAbsence_;
}

Framework::Components::ItemOut<bool> *LVDSMC_MCDriverOverride::DatalinkHVExtraction() const {
  return datalinkHVExtraction_;
}

Framework::Components::ItemOut<bool> *LVDSMC_MCDriverOverride::DatalinkPBD() const {
  return datalinkPBD_;
}

Framework::Components::ItemOut<bool> *LVDSMC_MCDriverOverride::DatalinkPBM() const {
  return datalinkPBM_;
}

}  // namespace SystemModules::Modules::LVDSMC
