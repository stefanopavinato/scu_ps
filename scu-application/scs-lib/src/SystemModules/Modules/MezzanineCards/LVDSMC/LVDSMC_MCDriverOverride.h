/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <cstdint>
#include "SystemModules/Framework/Components/ItemIn.h"
#include "SystemModules/Framework/Components/Container.h"
#include "SystemModules/Modules/Configuration/ConfigurationCommon.h"
#include "SystemModules/Modules/Configuration/Slots/Configuration_Slots.h"

namespace SystemModules::Modules::LVDSMC {
class LVDSMC_MCDriverOverride : public Framework::Components::Container {
 public:
  LVDSMC_MCDriverOverride(
    Logger::ILogger *logger,
    const Modules::Configuration::ConfigurationCommon &configCommon,
    const Configuration::Configuration_Slots &config,
    const uint8_t &port);
  // Check ob nicht erlaubter Wert, also weder 1, 2, noch 3?

  [[nodiscard]] Framework::Components::ItemIn<uint8_t> *DiscreteSignalOverrideCollection() const;

  [[nodiscard]] Framework::Components::ItemIn<uint8_t> *DatalinkSignalOverrideCollection() const;

  [[nodiscard]] Framework::Components::ItemOut<bool> *BeamAbsence() const;

  [[nodiscard]] Framework::Components::ItemOut<bool> *BeamPermit() const;

  [[nodiscard]] Framework::Components::ItemOut<bool> *Ready() const;

  [[nodiscard]] Framework::Components::ItemOut<bool> *DatalinkBeamPermit() const;

  [[nodiscard]] Framework::Components::ItemOut<bool> *DatalinkReady() const;

  [[nodiscard]] Framework::Components::ItemOut<bool> *DatalinkBeamAbsence() const;

  [[nodiscard]] Framework::Components::ItemOut<bool> *DatalinkHVAbsence() const;

  [[nodiscard]] Framework::Components::ItemOut<bool> *DatalinkHVExtraction() const;

  [[nodiscard]] Framework::Components::ItemOut<bool> *DatalinkPBD() const;

  [[nodiscard]] Framework::Components::ItemOut<bool> *DatalinkPBM() const;

 private:
  Framework::Components::ItemIn<uint8_t> *discreteSignalOverrideCollection_;

  Framework::Components::ItemIn<uint8_t> *datalinkSignalOverrideCollection_;

  Framework::Components::ItemOut<bool> *beamAbsence_;

  Framework::Components::ItemOut<bool> *beamPermit_;

  Framework::Components::ItemOut<bool> *ready_;

  Framework::Components::ItemOut<bool> *datalinkBeamPermit_;

  Framework::Components::ItemOut<bool> *datalinkReady_;

  Framework::Components::ItemOut<bool> *datalinkBeamAbsence_;

  Framework::Components::ItemOut<bool> *datalinkHVAbsence_;

  Framework::Components::ItemOut<bool> *datalinkHVExtraction_;

  Framework::Components::ItemOut<bool> *datalinkPBD_;

  Framework::Components::ItemOut<bool> *datalinkPBM_;

  static const char kName[];
  static const char kDescription[];

  static const char kDiscreteSignalOverrideCollectionName[];
  static const char kDiscreteSignalOverrideCollectionDescription[];
  static const uint32_t kDiscreteSignalOverrideCollectionPosition;

  static const char kDatalinkSignalOverrideCollectionName[];
  static const char kDatalinkSignalOverrideCollectionDescription[];
  static const uint32_t kDatalinkSignalOverrideCollectionPosition;

  static const char kBeamAbsenceName[];
  static const char kBeamAbsenceDescription[];
  static const uint32_t kBeamAbsencePosition;

  static const char kBeamPermitName[];
  static const char kBeamPermitDescription[];
  static const uint32_t kBeamPermitPosition;

  static const char kReadyName[];
  static const char kReadyDescription[];
  static const uint32_t kReadyPosition;

  static const char kDatalinkBeamAbsenceName[];
  static const char kDatalinkBeamAbsenceDescription[];
  static const uint32_t kDatalinkBeamAbsencePosition;

  static const char kDatalinkBeamPermitName[];
  static const char kDatalinkBeamPermitDescription[];
  static const uint32_t kDatalinkBeamPermitPosition;

  static const char kDatalinkReadyName[];
  static const char kDatalinkReadyDescription[];
  static const uint32_t kDatalinkReadyPosition;

  static const char kDatalinkHVAbsenceName[];
  static const char kDatalinkHVAbsenceDescription[];
  static const uint32_t kDatalinkHVAbsencePosition;

  static const char kDatalinkHVExtractionName[];
  static const char kDatalinkHVExtractionDescription[];
  static const uint32_t kDatalinkHVExtractionPosition;

  static const char kDatalinkPBDName[];
  static const char kDatalinkPBDDescription[];
  static const uint32_t kDatalinkPBDPosition;

  static const char kDatalinkPBMName[];
  static const char kDatalinkPBMDescription[];
  static const uint32_t kDatalinkPBMPosition;
};
}  // namespace SystemModules::Modules::LVDSMC
