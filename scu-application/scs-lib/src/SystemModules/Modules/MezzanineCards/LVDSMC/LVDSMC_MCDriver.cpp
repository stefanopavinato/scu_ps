/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Logger/ILogger.h"
#include "SystemModules/Framework/Components/ItemOut.h"
#include "SystemModules/Framework/Builder/Components/ComponentBuilder.h"
#include "LVDSMC_MCDriverOverride.h"
#include "LVDSMC_MCDriverStatus.h"
#include "LVDSMC_MCDriverPort.h"
#include "LVDSMC_MCDriver.h"

namespace SystemModules::Modules::LVDSMC {

LVDSMC_MCDriver::LVDSMC_MCDriver(
  Logger::ILogger *logger,
  const Modules::Configuration::ConfigurationCommon &configCommon,
  const Configuration::Configuration_Slots &config)
  : Framework::Components::Container(
  logger,
  "MCDriver",
  "LVDSMC MC_Driver") {
  // Crate MC_Driver Port 1
  auto port1 = std::make_unique<LVDSMC_MCDriverPort>(
    logger,
    configCommon,
    config,
    1);
  port1_ = port1.get();
  AddComponent(std::move(port1));
  // Crate MC_Driver Port 2
  auto port2 = std::make_unique<LVDSMC_MCDriverPort>(
    logger,
    configCommon,
    config,
    2);
  port2_ = port2.get();
  AddComponent(std::move(port2));
  // Crate MC_Driver Port 3
  auto port3 = std::make_unique<LVDSMC_MCDriverPort>(
    logger,
    configCommon,
    config,
    3);
  port3_ = port3.get();
  AddComponent(std::move(port3));
  (void) config;
}

LVDSMC_MCDriverPort *LVDSMC_MCDriver::Port1() const {
  return port1_;
}

LVDSMC_MCDriverPort *LVDSMC_MCDriver::Port2() const {
  return port2_;
}

LVDSMC_MCDriverPort *LVDSMC_MCDriver::Port3() const {
  return port3_;
}

}  // namespace SystemModules::Modules::LVDSMC
