/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Modules/MezzanineCards/ModuleMezzanineCard.h"
#include "MCStateRemoved1.h"
#include "MCStateStopped.h"
#include "MCStateInserted1.h"

namespace SystemModules::Framework::States {

const std::chrono::milliseconds MCStateInserted1::delay_ = std::chrono::milliseconds(2000);

MCStateInserted1::MCStateInserted1(Components::ModuleMezzanineCard *module)
  : MCStateBase(Types::ModuleState::Enum::INSERTED1, module) {
}

void MCStateInserted1::OnEntry() {
  // Install management
  if (!Module()->InstallManagement()) {
    SetState(std::make_unique<MCStateRemoved1>(Module()));
    return;
  }
  // Set timer
  timerExpired_ = std::chrono::steady_clock::now() + delay_;
}

void MCStateInserted1::OnExit() {
}

void MCStateInserted1::OnUpdate() {
  // MCStateStopped else
  if (std::chrono::steady_clock::now() > timerExpired_) {
    SetState(std::make_unique<MCStateStopped>(Module()));
  }
}

}  // namespace SystemModules::Framework::States
