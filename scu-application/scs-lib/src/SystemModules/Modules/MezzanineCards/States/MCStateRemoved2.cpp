/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Modules/MezzanineCards/ModuleMezzanineCard.h"
#include "MCStateStopped.h"
#include "MCStateRemoved2.h"

namespace SystemModules::Framework::States {

const std::chrono::milliseconds MCStateRemoved2::delay_ = std::chrono::milliseconds(1000);

MCStateRemoved2::MCStateRemoved2(Components::ModuleMezzanineCard *module)
  : MCStateBase(Types::ModuleState::Enum::REMOVED2, module) {
}

void MCStateRemoved2::OnEntry() {
  // Check status
  if (Module()->GetGroupAlertSeverity(Types::ValueGroup::Enum::ANY).GetEnum() <= Types::AlertSeverity::Enum::WARNING) {
    // Clear status
    // Todo: REset status
    //    Module()->SetComponentStatus(Types::ComponentStatus::OK());
    // Delay
  }
  // set pwren to low
  *Module()->DriverEnable() = false;
  // set mc-driver-enable to false
  *Module()->MezzanineCardEnable() = false;
  // Set timer
  timerExpired_ = std::chrono::steady_clock::now() + delay_;
}

void MCStateRemoved2::OnExit() {
}

void MCStateRemoved2::OnUpdate() {
  // MCStateStopped if delay expired
  if (std::chrono::steady_clock::now() > timerExpired_) {
    SetState(std::make_unique<MCStateStopped>(Module()));
    return;
  }
}
}  // namespace SystemModules::Framework::States
