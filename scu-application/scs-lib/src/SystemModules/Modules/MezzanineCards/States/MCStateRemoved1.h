/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "MCStateBase.h"

namespace SystemModules::Framework::States {
class MCStateRemoved1 : public MCStateBase {
 public:
  explicit MCStateRemoved1(Components::ModuleMezzanineCard *module);

  ~MCStateRemoved1() override = default;

  void OnEntry() final;

  void OnExit() final;

  void OnUpdate() final;

 private:
  static const char kName[];
};
}  // namespace SystemModules::Framework::States
