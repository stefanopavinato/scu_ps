/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Logger/ILogger.h"
#include "SystemModules/Modules/MezzanineCards/MezzanineCardDetection/CardDetectionItems.h"
#include "SystemModules/Modules/MezzanineCards/ModuleMezzanineCard.h"
#include "MCStateRemoved3.h"
#include "MCStateInserted4.h"
#include "MCStateInserted3.h"

namespace SystemModules::Framework::States {

const std::chrono::milliseconds MCStateInserted3::delay_ = std::chrono::milliseconds(2000);

MCStateInserted3::MCStateInserted3(Components::ModuleMezzanineCard *module)
  : MCStateBase(Types::ModuleState::Enum::INSERTED3, module) {
}

void MCStateInserted3::OnEntry() {
  // Set RSTn to tri-state
  *Module()->DriverReset() = false;
  *Module()->MezzanineCardReset() = false;

  // Set timer
  timerExpired_ = std::chrono::steady_clock::now() + delay_;
}

void MCStateInserted3::OnExit() {
}

void MCStateInserted3::OnUpdate() {
  // CardRemoved3 if card removed or shutdown
  if (!*Module()->CardDetectionItems()->MezzanineCardPresent()->GetValue() ||
      Module()->GetGroupAlertSeverity(Types::ValueGroup::Enum::ANY).GetEnum() >= Types::AlertSeverity::Enum::WARNING ||
      Module()->GetStop() ||
      Module()->GetReset() ||
      Module()->GetShutDown()) {
    SetState(std::make_unique<MCStateRemoved3>(Module()));
    return;
  }
  // CardInserted4 otherwise if PWREN_n == 1 and RST_n == 1
  if ((std::chrono::steady_clock::now() > timerExpired_) &&
      *Module()->CardDetectionItems()->MezzanineCardEnable()->GetValueIn() &&
      !(*Module()->CardDetectionItems()->MezzanineCardReset()->GetValueIn())) {
    SetState(std::make_unique<MCStateInserted4>(Module()));
    return;
  }
}
}  // namespace SystemModules::Framework::States
