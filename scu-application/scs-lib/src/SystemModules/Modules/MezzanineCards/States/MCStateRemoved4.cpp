/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Modules/MezzanineCards/ModuleMezzanineCard.h"
#include "MCStateRemoved3.h"
#include "MCStateRemoved4.h"

namespace SystemModules::Framework::States {

const std::chrono::milliseconds MCStateRemoved4::delay_ = std::chrono::milliseconds(500);

MCStateRemoved4::MCStateRemoved4(Components::ModuleMezzanineCard *module)
  : MCStateBase(Types::ModuleState::Enum::REMOVED4, module) {
}

void MCStateRemoved4::OnEntry() {
  // Uninstall application
  (void) Module()->UninstallApplication();
  // Set timer
  timerExpired_ = std::chrono::steady_clock::now() + delay_;
}

void MCStateRemoved4::OnExit() {
}

void MCStateRemoved4::OnUpdate() {
  // CardRemoved3 if delay expired
  if (std::chrono::steady_clock::now() > timerExpired_) {
    SetState(std::make_unique<States::MCStateRemoved3>(Module()));
    return;
  }
}

}  // namespace SystemModules::Framework::States
