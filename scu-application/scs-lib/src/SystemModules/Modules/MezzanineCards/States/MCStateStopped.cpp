/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Builder/Actions/ActionBuilder.h"
#include "SystemModules/Modules/MezzanineCards/Management/MezzanineCardInfo.h"
#include "SystemModules/Modules/MezzanineCards/Management/Management.h"
#include "SystemModules/Modules/MezzanineCards/MezzanineCardDetection/CardDetectionItems.h"
#include "SystemModules/Modules/MezzanineCards/ModuleMezzanineCard.h"
#include "MCStateRemoved1.h"
#include "MCStateInserted2.h"
#include "MCStateStopped.h"

namespace SystemModules::Framework::States {

MCStateStopped::MCStateStopped(Components::ModuleMezzanineCard *module)
  : MCStateBase(Types::ModuleState::Enum::STOPPED, module) {
}

void MCStateStopped::OnEntry() {
  if (Module()->GetReset()) {
    Module()->ClearReset();
  }
}

void MCStateStopped::OnExit() {
}

void MCStateStopped::OnUpdate() {
  // CardRemoved1 if card removed or shutdown
  if (!*Module()->CardDetectionItems()->MezzanineCardPresent()->GetValue() ||
      Module()->GetShutDown()) {
    SetState(std::make_unique<MCStateRemoved1>(Module()));
    return;
  }
  // CardInserted2 if card types match or if start is forced and no error
  if (!Module()->GetShutDown() &&
      !Module()->GetStop() &&
      Module()->GetGroupAlertSeverity(Types::ValueGroup::Enum::ANY).GetEnum() < Types::AlertSeverity::Enum::ERROR &&
      *Module()->Management()->MezzanineCardInfo()->CardType()->GetValue() ==
      *Module()->CardDetectionItems()->ExpectedCardType()->GetValue()) {
    SetState(std::make_unique<MCStateInserted2>(Module()));
    return;
  }
}

}  // namespace SystemModules::Framework::States
