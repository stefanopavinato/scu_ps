/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <unordered_map>
#include <queue>
#include "SystemModules/Modules/MezzanineCards/ModuleMezzanineCard.h"
#include "MCStateBase.h"

namespace SystemModules::Framework::States {
MCStateBase::MCStateBase(
  const Types::ModuleState::Enum &id,
  Components::ModuleMezzanineCard *module)
  : id_(Types::ModuleState(id))
  , module_(module) {
}

Types::ModuleState MCStateBase::GetId() const {
  return id_;
}

Components::ModuleMezzanineCard *MCStateBase::Module() const {
  return module_;
}

void MCStateBase::SetState(std::unique_ptr<MCStateBase> state) {
  module_->SetState(std::move(state));
}

}  // namespace SystemModules::Framework::States
