/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Modules/MezzanineCards/MezzanineCardDetection/CardDetectionItems.h"
#include "SystemModules/Modules/MezzanineCards/ModuleMezzanineCard.h"
#include "MCStateRemoved4.h"
#include "MCStateRunning.h"
#include "MCStateInserted4.h"

namespace SystemModules::Framework::States {

const std::chrono::milliseconds MCStateInserted4::delay_ = std::chrono::milliseconds(500);

MCStateInserted4::MCStateInserted4(Components::ModuleMezzanineCard *module)
  : MCStateBase(Types::ModuleState::Enum::INSERTED4, module) {
}

void MCStateInserted4::OnEntry() {
  // Install application
  if (!Module()->InstallApplication()) {
    SetState(std::make_unique<MCStateRemoved4>(Module()));
    return;
  }
  // Set timer
  timerExpired_ = std::chrono::steady_clock::now() + delay_;
}

void MCStateInserted4::OnExit() {
}

void MCStateInserted4::OnUpdate() {
  // CardRemoved4 if card removed or shutdown
  if (!*Module()->CardDetectionItems()->MezzanineCardPresent()->GetValue() ||
      Module()->GetGroupAlertSeverity(Types::ValueGroup::Enum::ANY).GetEnum() >= Types::AlertSeverity::Enum::WARNING ||
      Module()->GetStop() ||
      Module()->GetReset() ||
      Module()->GetShutDown()) {
    SetState(std::make_unique<MCStateRemoved4>(Module()));
    return;
  }
  // Run if delay expired
  if (std::chrono::steady_clock::now() > timerExpired_) {
    SetState(std::make_unique<MCStateRunning>(Module()));
    return;
  }
}
}  // namespace SystemModules::Framework::States
