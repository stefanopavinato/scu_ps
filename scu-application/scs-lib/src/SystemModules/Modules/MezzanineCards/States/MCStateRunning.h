/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "MCStateBase.h"

namespace SystemModules::Framework::States {
class MCStateRunning : public MCStateBase {
 public:
  explicit MCStateRunning(Components::ModuleMezzanineCard *module);

  ~MCStateRunning() override = default;

  void OnEntry() final;

  void OnExit() final;

  void OnUpdate() final;
};
}  // namespace SystemModules::Framework::States
