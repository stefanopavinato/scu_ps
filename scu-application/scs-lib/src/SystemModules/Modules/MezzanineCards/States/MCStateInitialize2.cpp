/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Builder/Actions/ActionBuilder.h"
#include "SystemModules/Modules/MezzanineCards/ModuleMezzanineCard.h"
#include "SystemModules/Modules/MezzanineCards/MezzanineCardDetection/CardDetectionItems.h"
#include "MCStateInitialize3.h"
#include "MCStateInitialize2.h"

namespace SystemModules::Framework::States {

MCStateInitialize2::MCStateInitialize2(Components::ModuleMezzanineCard *module)
: MCStateBase(Types::ModuleState::Enum::INITIALIZE2, module) {
}

void MCStateInitialize2::OnEntry() {
}

void MCStateInitialize2::OnExit() {
}

void MCStateInitialize2::OnUpdate() {
  // Set initial values for state machine
  *Module()->MezzanineCardEnable() = *Module()->CardDetectionItems()->MezzanineCardEnable()->GetValueIn();
  *Module()->MezzanineCardReset() = *Module()->CardDetectionItems()->MezzanineCardReset()->GetValueIn();
  *Module()->DriverEnable() = false;
  *Module()->DriverReset() = true;
  // Empty if always
  SetState(std::make_unique<MCStateInitialize3>(Module()));
}
}  // namespace SystemModules::Framework::States
