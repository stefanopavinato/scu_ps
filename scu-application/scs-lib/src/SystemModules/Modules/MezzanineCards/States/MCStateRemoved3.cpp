/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Modules/MezzanineCards/ModuleMezzanineCard.h"
#include "MCStateRemoved2.h"
#include "MCStateRemoved3.h"

namespace SystemModules::Framework::States {

const std::chrono::milliseconds MCStateRemoved3::delay_ = std::chrono::milliseconds(500);

MCStateRemoved3::MCStateRemoved3(Components::ModuleMezzanineCard *module)
  : MCStateBase(Types::ModuleState::Enum::REMOVED3, module) {
}

void MCStateRemoved3::OnEntry() {
  // Set reset_n to low
  *Module()->MezzanineCardReset() = true;

  // Set timer
  timerExpired_ = std::chrono::steady_clock::now() + delay_;
}

void MCStateRemoved3::OnExit() {
}

void MCStateRemoved3::OnUpdate() {
  // CardRemoved2 if delay expired
  if (std::chrono::steady_clock::now() > timerExpired_) {
    SetState(std::make_unique<States::MCStateRemoved2>(Module()));
    return;
  }
}
}  // namespace SystemModules::Framework::States
