/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <memory>
#include <string>
#include "Types/ModuleState.h"

namespace SystemModules::Framework {
namespace Components {
class ModuleMezzanineCard;
}  // namespace Components
namespace States {
class MCStateBase {
 public:
  MCStateBase(
    const Types::ModuleState::Enum &id,
    Components::ModuleMezzanineCard *module);

  virtual ~MCStateBase() = default;

  virtual void OnEntry() = 0;

  virtual void OnExit() = 0;

  virtual void OnUpdate() = 0;

  [[nodiscard]] Types::ModuleState GetId() const;

  void SetState(
    std::unique_ptr<MCStateBase> stateNew);

  [[nodiscard]] Components::ModuleMezzanineCard *Module() const;

 private:
  Types::ModuleState id_;

  Components::ModuleMezzanineCard *module_;
};
}  // namespace States
}  // namespace SystemModules::Framework
