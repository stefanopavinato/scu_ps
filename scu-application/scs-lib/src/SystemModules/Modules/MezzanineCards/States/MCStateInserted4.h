/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "MCStateBase.h"

namespace SystemModules::Framework::States {
class MCStateInserted4 : public MCStateBase {
 public:
  explicit MCStateInserted4(Components::ModuleMezzanineCard *module);

  ~MCStateInserted4() override = default;

  void OnEntry() final;

  void OnExit() final;

  void OnUpdate() final;

 private:
  std::chrono::steady_clock::time_point timerExpired_;

  static const std::chrono::milliseconds delay_;
};
}  // namespace SystemModules::Framework::States
