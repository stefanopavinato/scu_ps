/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "MCStateBase.h"

namespace SystemModules::Framework::States {
class MCStateRemoved4 : public MCStateBase {
 public:
  explicit MCStateRemoved4(Components::ModuleMezzanineCard *module);

  ~MCStateRemoved4() override = default;

  void OnEntry() final;

  void OnExit() final;

  void OnUpdate() final;

 private:
  std::chrono::steady_clock::time_point timerExpired_;

  static const std::chrono::milliseconds delay_;
};
}  // namespace SystemModules::Framework::States
