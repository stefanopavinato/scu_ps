/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Modules/MezzanineCards/MezzanineCardDetection/CardDetectionItems.h"
#include "SystemModules/Modules/MezzanineCards/ModuleMezzanineCard.h"
#include "MCStateRemoved2.h"
#include "MCStateInserted3.h"
#include "MCStateInserted2.h"

namespace SystemModules::Framework::States {

const std::chrono::milliseconds MCStateInserted2::delay_ = std::chrono::milliseconds(2000);

MCStateInserted2::MCStateInserted2(Components::ModuleMezzanineCard *module)
  : MCStateBase(Types::ModuleState::Enum::INSERTED2, module) {
}

void MCStateInserted2::OnEntry() {
  // Set power enable to tri-state
  *Module()->MezzanineCardEnable() = true;
  // Set mc-driver-enable to true
  *Module()->DriverEnable() = true;
  // Set timer
  timerExpired_ = std::chrono::steady_clock::now() + delay_;
}

void MCStateInserted2::OnExit() {
}

void MCStateInserted2::OnUpdate() {
  // CardRemoved2 if card removed or shutdown
  if (!*Module()->CardDetectionItems()->MezzanineCardPresent()->GetValue() ||
      Module()->GetGroupAlertSeverity(Types::ValueGroup::Enum::ANY).GetEnum() >= Types::AlertSeverity::Enum::WARNING ||
      Module()->GetStop() ||
      Module()->GetReset() ||
      Module()->GetShutDown()) {
    SetState(std::make_unique<MCStateRemoved2>(Module()));
    return;
  }
  // CardInserted3 if delay expired
  if (std::chrono::steady_clock::now() > timerExpired_ &&
      *Module()->CardDetectionItems()->MezzanineCardEnable()->GetValueIn()) {
    SetState(std::make_unique<MCStateInserted3>(Module()));
    return;
  }
}
}  // namespace SystemModules::Framework::States
