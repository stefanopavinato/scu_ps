/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Types/Value.h"
#include "SystemModules/Framework/Components/ItemIn.h"
#include "SystemModules/Framework/Components/I2CDeviceINA220.h"
#include "SystemModules/Modules/Configuration/ConfigurationCommon.h"

namespace SystemModules::Modules::CLMC {
class CLMCAnalogIn3 : public Framework::Components::I2CDeviceINA220 {
 public:
  CLMCAnalogIn3(
    Logger::ILogger *logger,
    const Modules::Configuration::ConfigurationCommon &configCommon,
    const std::string &path,
    const std::string &devicePath,
    const uint32_t &slot);

  [[nodiscard]]
  Framework::Components::ItemIn<Types::Value<int32_t>> *RedundantBeamPermitCurrent() const;

  [[nodiscard]]
  Framework::Components::ItemIn<Types::Value<int32_t>> *RedundantBeamPermitAndReadyVoltage() const;

 private:
  Framework::Components::ItemIn<Types::Value<int32_t>> *beamPermitCurrentRedundant_;

  Framework::Components::ItemIn<Types::Value<int32_t>> *beamPermitReadyVoltageRedundant_;

  static const char kName[];
  static const char kDescription[];
  static const uint16_t kAddress;

  static const uint32_t kOnDelay_ms;
  static const uint32_t kOffDelay_ms;

  static const uint32_t kCurrentScaleMultiply;
  static const uint32_t kCurrentScaleDivide;

  static const char kRedundantBeamPermitCurrentName[];
  static const char kRedundantBeamPermitCurrentDescription[];
  static const char kRedundantBeamPermitCurrentSubPath[];

  static const char kRedundantBeamPermitAndReadyVoltageName[];
  static const char kRedundantBeamPermitAndReadyVoltageDescription[];
  static const char kRedundantBeamPermitAndReadyVoltageSubPath[];
};

}  // namespace SystemModules::Modules::CLMC
