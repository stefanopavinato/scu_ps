/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <cstdint>
#include "SystemModules/Framework/Components/ParameterItemOut.h"
#include "SystemModules/Framework/Components/Container.h"
#include "SystemModules/Modules/Configuration/ConfigurationCommon.h"
#include "SystemModules/Modules/Configuration/Slots/Configuration_Slots.h"

namespace SystemModules::Modules::CLMC {
class CLMC_MCDriverStatus;

class CLMC_MCDriverOverride;

class CLMC_MCDriver : public Framework::Components::Container {
 public:
  CLMC_MCDriver(
    Logger::ILogger *logger,
    const Modules::Configuration::ConfigurationCommon &configCommon,
    const Configuration::Configuration_Slots &config);

  [[nodiscard]] CLMC_MCDriverStatus *Status() const;

  [[nodiscard]] CLMC_MCDriverOverride *Override() const;

  [[nodiscard]] Framework::Components::ParameterItemOut <uint8_t> *UARTDirection() const;

 private:
  CLMC_MCDriverStatus *status_;

  CLMC_MCDriverOverride *override_;

  Framework::Components::ParameterItemOut <uint8_t> *uartDirection_;

  static const char kName[];
  static const char kDescription[];

  static const char kUARTDirectionName[];
  static const char kUARTDirectionDescription[];
  static const uint32_t kUARTDirectionPosition;
};

}  // namespace SystemModules::Modules::CLMC
