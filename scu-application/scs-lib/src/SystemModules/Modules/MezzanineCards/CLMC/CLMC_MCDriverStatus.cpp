/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Actions/ActionTemplate.h"
#include "SystemModules/Framework/Builder/Actions/ActionBuilder.h"
#include "SystemModules/Framework/MonitoringFunctions/MFTemplate.h"
#include "SystemModules/Framework/Builder/Accessors/WriteAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Accessors/ReadAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemInBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemOutBuilder.h"
#include "SystemModules/Framework/Builder/Components/ComponentBuilder.h"
#include "CLMC_MCDriverStatus.h"

namespace SystemModules::Modules::CLMC {

const char CLMC_MCDriverStatus::kName[] = "Status";
const char CLMC_MCDriverStatus::kDescription[] = "";

const char CLMC_MCDriverStatus::kCollectionName[] = "Collection";
const char CLMC_MCDriverStatus::kCollectionDescription[] = "Collection of CLMC status items";
const uint32_t CLMC_MCDriverStatus::kCollectionPosition = 0;

const char CLMC_MCDriverStatus::kBeamPermitName[] = "BeamPermit";
const char CLMC_MCDriverStatus::kBeamPermitDescription[] =
  "Status of the discrete BP signal as received from the MC (raw value)";
const uint32_t CLMC_MCDriverStatus::kBeamPermitPosition = 0;

const char CLMC_MCDriverStatus::kRedundantBeamPermitName[] = "RedundantBeamPermit";
const char CLMC_MCDriverStatus::kRedundantBeamPermitDescription[] =
  "Status of the discrete redBP signal as received from the MC (raw value)";
const uint32_t CLMC_MCDriverStatus::kRedundantBeamPermitPosition = 2;

const char CLMC_MCDriverStatus::kReadyName[] = "Ready";
const char CLMC_MCDriverStatus::kReadyDescription[] =
  "Status of the discrete RDY signal as received from the MC (raw value)";
const uint32_t CLMC_MCDriverStatus::kReadyPosition = 1;

const char CLMC_MCDriverStatus::kRedundantReadyName[] = "RedundantReady";
const char CLMC_MCDriverStatus::kRedundantReadyDescription[] =
  "Status of the discrete redRDY signal as received from the MC (raw value)";
const uint32_t CLMC_MCDriverStatus::kRedundantReadyPosition = 3;

const char CLMC_MCDriverStatus::kBeamPermitAName[] = "BeamPermitA";
const char CLMC_MCDriverStatus::kBeamPermitADescription[] =
  "Status of the datalink BP_A signal as received from the AnyBus Module on the MC (raw value)";
const uint32_t CLMC_MCDriverStatus::kBeamPermitAPosition = 4;

const char CLMC_MCDriverStatus::kBeamPermitBName[] = "BeamPermitB";
const char CLMC_MCDriverStatus::kBeamPermitBDescription[] =
  "Status of the datalink BP_B signal as received from the AnyBus Module on the MC (raw value)";
const uint32_t CLMC_MCDriverStatus::kBeamPermitBAddress = 5;

const char CLMC_MCDriverStatus::kReadyAName[] = "ReadyA";
const char CLMC_MCDriverStatus::kReadyADescription[] =
  "Status of the datalink RDY_A signal as received from the AnyBus Module on the MC (raw value)";
const uint32_t CLMC_MCDriverStatus::kReadyAAddress = 6;

const char CLMC_MCDriverStatus::kReadyBName[] = "ReadyB";
const char CLMC_MCDriverStatus::kReadyBDescription[] =
  "Status of the datalink RDY_B signal as received from the AnyBus Module on the MC (raw value)";
const uint32_t CLMC_MCDriverStatus::kReadyBAddress = 7;

const char CLMC_MCDriverStatus::kDl_BP_AB_ErrorName[] = "Dl_BP_AB_Error";
const char CLMC_MCDriverStatus::kDl_BP_AB_ErrorDescription[] =
  "Datalink BP_A / BP_B error <br><b>0x0</b>: No Error <br> <b>0x1</b>: Error";
const uint32_t CLMC_MCDriverStatus::kDl_BP_AB_ErrorAddress = 8;

const char CLMC_MCDriverStatus::kDl_RDY_AB_ErrorName[] = "Dl_RDY_AB_Error";
const char CLMC_MCDriverStatus::kDl_RDY_AB_ErrorDescription[] =
  "Datalink RDY_A / RDY_B error <br><b>0x0</b>: No Error <br> <b>0x1</b>: Error";
const uint32_t CLMC_MCDriverStatus::kDl_RDY_AB_ErrorAddress = 9;

const char CLMC_MCDriverStatus::kDl_PBD_AB_ErrorName[] = "Dl_PBD_AB_Error";
const char CLMC_MCDriverStatus::kDl_PBD_AB_ErrorDescription[] =
  "Datalink PBD_A / PBD_B error <br><b>0x0</b>: No Error <br> <b>0x1</b>: Error";
const uint32_t CLMC_MCDriverStatus::kDl_PBD_AB_ErrorAddress = 10;

const char CLMC_MCDriverStatus::kDl_PBM_AB_ErrorName[] = "Dl_PBM_AB_Error";
const char CLMC_MCDriverStatus::kDl_PBM_AB_ErrorDescription[] =
  "Datalink PBM_A / PBM_B error <br><b>0x0</b>: No Error <br> <b>0x1</b>: Error";
const uint32_t CLMC_MCDriverStatus::kDl_PBM_AB_ErrorAddress = 11;

const char CLMC_MCDriverStatus::kDl_AnyBus_errorName[] = "Dl_AnyBus_Error";
const char CLMC_MCDriverStatus::kDl_AnyBus_errorDescription[] =
  "Datalink AnyBus Module error <br><b>0x0</b>: No Error <br> <b>0x1</b>: Error";
const uint32_t CLMC_MCDriverStatus::kDl_AnyBus_errorAddress = 12;

const char CLMC_MCDriverStatus::kDl_AnyBus_readyName[] = "Dl_AnyBus_Ready";
const char CLMC_MCDriverStatus::kDl_AnyBus_readyDescription[] =
  "Datalink AnyBus Module ready <br><b>0x0</b>: Not Ready <br> <b>0x1</b>: Ready";
const uint32_t CLMC_MCDriverStatus::kDl_AnyBus_readyAddress = 13;

const char CLMC_MCDriverStatus::kDl_Decoder_readyName[] = "Dl_Decoder_Ready";
const char CLMC_MCDriverStatus::kDl_Decoder_readyDescription[] =
  "Datalink Decoder ready <br><b>0x0</b>: Not Ready <br> <b>0x1</b>: Ready";
const uint32_t CLMC_MCDriverStatus::kDl_Decoder_readyAddress = 14;

const char CLMC_MCDriverStatus::kPbdAName[] = "PbdA";
const char CLMC_MCDriverStatus::kPbdADescription[] =
  "Status of the PBD_A signal as received from the AnyBus Module on the MC (raw value)";
const uint32_t CLMC_MCDriverStatus::kPbdAAddress = 0;

const char CLMC_MCDriverStatus::kPbdBName[] = "PbdB";
const char CLMC_MCDriverStatus::kPbdBDescription[] =
  "Status of the PBD_B signal as received from the AnyBus Module on the MC (raw value)";
const uint32_t CLMC_MCDriverStatus::kPbdBAddress = 16;

const char CLMC_MCDriverStatus::kPbmAName[] = "PbmA";
const char CLMC_MCDriverStatus::kPbmADescription[] =
  "Status of the PBM_A signal as received from the AnyBus Module on the MC (raw value)";
const uint32_t CLMC_MCDriverStatus::kPbmAAddress = 0;

const char CLMC_MCDriverStatus::kPbmBName[] = "PbmB";
const char CLMC_MCDriverStatus::kPbmBDescription[] =
  "Status of the PBM_B signal as received from the AnyBus Module on the MC (raw value)";
const uint32_t CLMC_MCDriverStatus::kPbmBAddress = 16;

const char CLMC_MCDriverStatus::kLiveSignCounterName[] = "LiveSignCounter";
const char CLMC_MCDriverStatus::kLiveSignCounterDescription[] =
  "Datalink LiveSign Counter";
const uint32_t CLMC_MCDriverStatus::kLiveSignCounterAddress = 16;

const char CLMC_MCDriverStatus::kSenderIdName[] = "SenderId";
const char CLMC_MCDriverStatus::kSenderIdDescription[] =
  "Sender ID of the Sensor System received via the AnyBus Module on the MC (raw value)";
const uint32_t CLMC_MCDriverStatus::kSenderIdAddress = 0;

const char CLMC_MCDriverStatus::kDatalinkLifeSignErrorsName[] = "DatalinkLifeSignErrors";
const char CLMC_MCDriverStatus::kDatalinkLifeSignErrorsDescription[] =
  "Counter for Datalink LifeSign Errors";
const uint32_t CLMC_MCDriverStatus::kDatalinkLifeSignErrorsAddress = 0;

const char CLMC_MCDriverStatus::kDatalinkTimeExpectationTimeoutsName[] = "DatalinkTimeExpectationTimeouts";
const char CLMC_MCDriverStatus::kDatalinkTimeExpectationTimeoutsDescription[] =
  "Counter for Datalink TimeExpectation-timeouts";
const uint32_t CLMC_MCDriverStatus::kDatalinkTimeExpectationTimeoutsAddress = 0;

const char CLMC_MCDriverStatus::kDatalinkLifeSignErrorsResetName[] = "DatalinkLifeSignErrorsReset";
const char CLMC_MCDriverStatus::kDatalinkLifeSignErrorsResetDescription[] =
  "Reset for Datalink LifeSign Errors";
const uint32_t CLMC_MCDriverStatus::kDatalinkLifeSignErrorsResetAddress = 0;

const char CLMC_MCDriverStatus::kDatalinkTimeExpectationTimeoutsResetName[] = "DatalinkTimeExpectationTimeoutsReset";
const char CLMC_MCDriverStatus::kDatalinkTimeExpectationTimeoutsResetDescription[] =
  "Reset for Datalink TimeExpectation-timeouts";
const uint32_t CLMC_MCDriverStatus::kDatalinkTimeExpectationTimeoutsResetAddress = 1;

const char CLMC_MCDriverStatus::kDatalinkDebugInfoName[] = "DatalinkDebugInfo";
const char CLMC_MCDriverStatus::kDatalinkDebugInfoDescription[] =
  "Debug information of the CLMC datalink";
const uint32_t CLMC_MCDriverStatus::kDatalinkDebugInfoAddress = 0;


CLMC_MCDriverStatus::CLMC_MCDriverStatus(
  Logger::ILogger *logger,
  const Modules::Configuration::ConfigurationCommon &configCommon,
  const Configuration::Configuration_Slots &config)
  : Framework::Components::Container(
  logger,
  kName,
  kDescription) {
  // Get update intervaval
  auto updateIntervalDiscreteSignals = configCommon.UpdateInterval()->Medium()->Value();
  auto updateIntervalDatalink = configCommon.UpdateInterval()->Medium()->Value();
  // Setup item collection
  collection_ = Create().ItemIn<uint16_t>(
    kCollectionName,
    kCollectionDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDiscreteSignals),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  collection_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister1Address()->Value(),
    kCollectionPosition,
    16);
  // Setup beam permit input
  beamPermit_ = Create().ItemIn<bool>(
    kBeamPermitName,
    kBeamPermitDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDiscreteSignals),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  beamPermit_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister1Address()->Value(),
    kBeamPermitPosition,
    1);
  // Setup beam permit redundant input
  redundantBeamPermit_ = Create().ItemIn<bool>(
    kRedundantBeamPermitName,
    kRedundantBeamPermitDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDiscreteSignals),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  redundantBeamPermit_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister1Address()->Value(),
    kRedundantBeamPermitPosition,
    1);
  // Setup ready input
  ready_ = Create().ItemIn<bool>(
    kReadyName,
    kReadyDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDiscreteSignals),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  ready_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister1Address()->Value(),
    kReadyPosition,
    1);
  // Setup ready redundant input
  redundantReady_ = Create().ItemIn<bool>(
    kRedundantReadyName,
    kRedundantReadyDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDiscreteSignals),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  redundantReady_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister1Address()->Value(),
    kRedundantReadyPosition,
    1);
  //  Setup beam permit A input
  beamPermitA_ = Create().ItemIn<bool>(
    kBeamPermitAName,
    kBeamPermitADescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDatalink),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  beamPermitA_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister1Address()->Value(),
    kBeamPermitAPosition,
    1);
  //  Setup beam permit B input
  beamPermitB_ = Create().ItemIn<bool>(
    kBeamPermitBName,
    kBeamPermitBDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDatalink),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  beamPermitB_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister1Address()->Value(),
    kBeamPermitBAddress,
    1);
  //  Setup ready A input
  readyA_ = Create().ItemIn<bool>(
    kReadyAName,
    kReadyADescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDatalink),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  readyA_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister1Address()->Value(),
    kReadyAAddress,
    1);
  //  Setup ready B input
  readyB_ = Create().ItemIn<bool>(
    kReadyBName,
    kReadyBDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDatalink),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  readyB_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister1Address()->Value(),
    kReadyBAddress,
    1);
  //  Setup Dl_BP_AB_Error input
  Dl_BP_AB_Error_ = Create().ItemIn<bool>(
    kDl_BP_AB_ErrorName,
    kDl_BP_AB_ErrorDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDatalink),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  Dl_BP_AB_Error_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister1Address()->Value(),
    kDl_BP_AB_ErrorAddress,
    1);
  //  Setup Dl_RDY_AB_Error input
  Dl_RDY_AB_Error_ = Create().ItemIn<bool>(
    kDl_RDY_AB_ErrorName,
    kDl_RDY_AB_ErrorDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDatalink),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  Dl_RDY_AB_Error_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister1Address()->Value(),
    kDl_RDY_AB_ErrorAddress,
    1);
  //  Setup Dl_BPD_AB_Error input
  Dl_PBD_AB_Error_ = Create().ItemIn<bool>(
    kDl_PBD_AB_ErrorName,
    kDl_PBD_AB_ErrorDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDatalink),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  Dl_PBD_AB_Error_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister1Address()->Value(),
    kDl_PBD_AB_ErrorAddress,
    1);
  //  Setup Dl_PBM_AB_Error input
  Dl_PBM_AB_Error_ = Create().ItemIn<bool>(
    kDl_PBM_AB_ErrorName,
    kDl_PBM_AB_ErrorDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDatalink),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  Dl_PBM_AB_Error_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister1Address()->Value(),
    kDl_PBM_AB_ErrorAddress,
    1);
  //  Setup Dl_AnyBus_error input
  Dl_AnyBus_Error_ = Create().ItemIn<bool>(
    kDl_AnyBus_errorName,
    kDl_AnyBus_errorDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDatalink),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  Dl_AnyBus_Error_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister1Address()->Value(),
    kDl_AnyBus_errorAddress,
    1);
  //  Setup Dl_AnyBus_ready input
  Dl_AnyBus_Ready_ = Create().ItemIn<bool>(
    kDl_AnyBus_readyName,
    kDl_AnyBus_readyDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDatalink),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  Dl_AnyBus_Ready_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister1Address()->Value(),
    kDl_AnyBus_readyAddress,
    1);
  //  Setup Dl_Decoder_ready input
  Dl_Decoder_Ready_ = Create().ItemIn<bool>(
    kDl_Decoder_readyName,
    kDl_Decoder_readyDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDatalink),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  Dl_Decoder_Ready_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister1Address()->Value(),
    kDl_Decoder_readyAddress,
    1);
  // Setup live sign counter input
  liveSignCounter_ = Create().ItemIn<uint16_t>(
    kLiveSignCounterName,
    kLiveSignCounterDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDatalink),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  liveSignCounter_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister1Address()->Value(),
    kLiveSignCounterAddress,
    16);
  // Setup pbd A input
  pbdA_ = Create().ItemIn<uint16_t>(
    kPbdAName,
    kPbdADescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDatalink),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  pbdA_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister2Address()->Value(),
    kPbdAAddress,
    16);
  // Setup pbd B input
  pbdB_ = Create().ItemIn<uint16_t>(
    kPbdBName,
    kPbdBDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDatalink),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  pbdB_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister2Address()->Value(),
    kPbdBAddress,
    16);
  // Setup pbm A input
  pbmA_ = Create().ItemIn<uint16_t>(
    kPbmAName,
    kPbmADescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDatalink),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  pbmA_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister3Address()->Value(),
    kPbmAAddress,
    16);
  // Setup pbm B input
  pbmB_ = Create().ItemIn<uint16_t>(
    kPbmBName,
    kPbmBDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDatalink),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  pbmB_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister3Address()->Value(),
    kPbmBAddress,
    16);
  // Set up sender id input
  senderId_ = Create().ItemIn<uint32_t>(
    kSenderIdName,
    kSenderIdDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDatalink),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  senderId_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister4Address()->Value(),
    kSenderIdAddress,
    32);
  // Set up Datalink LifeSign Error Counter
  datalinkLifeSignErrors_ = Create().ItemIn<uint32_t>(
    kDatalinkLifeSignErrorsName,
    kDatalinkLifeSignErrorsDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDatalink),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  datalinkLifeSignErrors_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister5Address()->Value(),
    kDatalinkLifeSignErrorsAddress,
    32);
  // Set up Datalink TimeExpectation-timeout Counter
  datalinkTimeExpectationTimeouts_ = Create().ItemIn<uint32_t>(
    kDatalinkTimeExpectationTimeoutsName,
    kDatalinkTimeExpectationTimeoutsDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDatalink),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  datalinkTimeExpectationTimeouts_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister6Address()->Value(),
    kDatalinkTimeExpectationTimeoutsAddress,
    32);
  // Set up Datalink LifeSign Error Counter Reset
  datalinkLifeSignErrorsReset_ = Create().ItemOut<bool>(
    kDatalinkLifeSignErrorsResetName,
    kDatalinkLifeSignErrorsResetDescription,
    std::chrono::milliseconds(updateIntervalDatalink),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    false,
    false).AddAndReturnPointer();
  datalinkLifeSignErrorsReset_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ClearRegister1Address()->Value(),
    kDatalinkLifeSignErrorsResetAddress,
    1);
  datalinkLifeSignErrorsReset_->CreateAction().StaticValue(false);
  // Set up Datalink TimeExpectation-timeout Counter Reset
  datalinkTimeExpectationTimeoutsReset_ = Create().ItemOut<bool>(
    kDatalinkTimeExpectationTimeoutsResetName,
    kDatalinkTimeExpectationTimeoutsResetDescription,
    std::chrono::milliseconds(updateIntervalDatalink),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    false,
    false).AddAndReturnPointer();
  datalinkTimeExpectationTimeoutsReset_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ClearRegister1Address()->Value(),
    kDatalinkTimeExpectationTimeoutsResetAddress,
    1);
  datalinkTimeExpectationTimeoutsReset_->CreateAction().StaticValue(false);
  // Set up Datalink Debug Info
  datalinkDebugInfo_ = Create().ItemIn<uint32_t>(
    kDatalinkDebugInfoName,
    kDatalinkDebugInfoDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDatalink),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  datalinkDebugInfo_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister7Address()->Value(),
    kDatalinkDebugInfoAddress,
    32);
  (void) config;
}

Framework::Components::ItemIn<uint16_t> *CLMC_MCDriverStatus::Collection() const {
  return collection_;
}

Framework::Components::ItemIn<bool> *CLMC_MCDriverStatus::BeamPermit() const {
  return beamPermit_;
}

Framework::Components::ItemIn<bool> *CLMC_MCDriverStatus::RedundantBeamPermit() const {
  return redundantBeamPermit_;
}

Framework::Components::ItemIn<bool> *CLMC_MCDriverStatus::Ready() const {
  return ready_;
}

Framework::Components::ItemIn<bool> *CLMC_MCDriverStatus::RedundantReady() const {
  return redundantReady_;
}

Framework::Components::ItemIn<bool> *CLMC_MCDriverStatus::BeamPermitA() const {
  return beamPermitA_;
}

Framework::Components::ItemIn<bool> *CLMC_MCDriverStatus::BeamPermitB() const {
  return beamPermitB_;
}

Framework::Components::ItemIn<bool> *CLMC_MCDriverStatus::ReadyA() const {
  return readyA_;
}

Framework::Components::ItemIn<bool> *CLMC_MCDriverStatus::ReadyB() const {
  return readyB_;
}

Framework::Components::ItemIn<bool> *CLMC_MCDriverStatus::Dl_BP_AB_Error() const {
  return Dl_BP_AB_Error_;
}

Framework::Components::ItemIn<bool> *CLMC_MCDriverStatus::Dl_RDY_AB_Error() const {
  return Dl_RDY_AB_Error_;
}


Framework::Components::ItemIn<bool> *CLMC_MCDriverStatus::Dl_PBD_AB_Error() const {
  return Dl_PBD_AB_Error_;
}

Framework::Components::ItemIn<bool> *CLMC_MCDriverStatus::Dl_PBM_AB_Error() const {
  return Dl_PBM_AB_Error_;
}

Framework::Components::ItemIn<bool> *CLMC_MCDriverStatus::Dl_AnyBus_Error() const {
  return Dl_AnyBus_Error_;
}

Framework::Components::ItemIn<bool> *CLMC_MCDriverStatus::Dl_AnyBus_Ready() const {
  return Dl_AnyBus_Ready_;
}

Framework::Components::ItemIn<bool> *CLMC_MCDriverStatus::Dl_Decoder_Ready() const {
  return Dl_Decoder_Ready_;
}

Framework::Components::ItemIn<uint16_t> *CLMC_MCDriverStatus::PbdA() const {
  return pbdA_;
}

Framework::Components::ItemIn<uint16_t> *CLMC_MCDriverStatus::PbdB() const {
  return pbdB_;
}

Framework::Components::ItemIn<uint16_t> *CLMC_MCDriverStatus::PbmA() const {
  return pbmA_;
}

Framework::Components::ItemIn<uint16_t> *CLMC_MCDriverStatus::PbmB() const {
  return pbmB_;
}

Framework::Components::ItemIn<uint16_t> *CLMC_MCDriverStatus::LiveSignCounter() const {
  return liveSignCounter_;
}

Framework::Components::ItemIn<uint32_t> *CLMC_MCDriverStatus::SenderId() const {
  return senderId_;
}

Framework::Components::ItemIn<uint32_t> *CLMC_MCDriverStatus::DatalinkLifeSignErrors() const {
  return datalinkLifeSignErrors_;
}

Framework::Components::ItemIn<uint32_t> *CLMC_MCDriverStatus::DatalinkTimeExpectationTimeouts() const {
  return datalinkTimeExpectationTimeouts_;
}

Framework::Components::ItemOut<bool> *CLMC_MCDriverStatus::DatalinkLifeSignErrorsReset() const {
  return datalinkLifeSignErrorsReset_;
}

Framework::Components::ItemOut<bool> *CLMC_MCDriverStatus::DatalinkTimeExpectationTimeoutsReset() const {
  return datalinkTimeExpectationTimeoutsReset_;
}

Framework::Components::ItemIn<uint32_t> *CLMC_MCDriverStatus::DatalinkDebugInfo() const {
  return datalinkDebugInfo_;
}

}  // namespace SystemModules::Modules::CLMC
