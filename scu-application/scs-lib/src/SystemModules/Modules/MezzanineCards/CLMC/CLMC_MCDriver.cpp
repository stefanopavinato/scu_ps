/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Builder/Actions/ActionBuilder.h"
#include "SystemModules/Framework/Builder/Components/ParameterItemOutBuilder.h"
#include "SystemModules/Framework/Builder/Components/ComponentBuilder.h"
#include "CLMC_MCDriverOverride.h"
#include "CLMC_MCDriverStatus.h"
#include "CLMC_MCDriver.h"

namespace SystemModules::Modules::CLMC {

const char CLMC_MCDriver::kName[] = "MCDriver";
const char CLMC_MCDriver::kDescription[] = "CLMC MC_Driver";

const char CLMC_MCDriver::kUARTDirectionName[] = "UARTDirection";
const char CLMC_MCDriver::kUARTDirectionDescription[] = "Direction Control of the CLMC inter-serializer UART";
const uint32_t CLMC_MCDriver::kUARTDirectionPosition = 0;

CLMC_MCDriver::CLMC_MCDriver(
  Logger::ILogger *logger,
  const Modules::Configuration::ConfigurationCommon &configCommon,
  const Configuration::Configuration_Slots &config)
  : Framework::Components::Container(
  logger,
  kName,
  kDescription) {
  // Set override container
  auto override = std::make_unique<CLMC_MCDriverOverride>(
    logger,
    configCommon,
    config);
  override_ = override.get();
  AddComponent(std::move(override));
  // Set status container
  auto status = std::make_unique<CLMC_MCDriverStatus>(
    logger,
    configCommon,
    config);
  status_ = status.get();
  AddComponent(std::move(status));
  // Setup UART Direction output
  uint8_t direction;
  if (configCommon.Serializer()->Slot()->Value() == "A") {
    direction = 1;
  } else {
    direction = 2;
  }
  uartDirection_ = Create().ParameterOut(
    kUARTDirectionName,
    kUARTDirectionDescription,
    Types::Affiliation::Enum::EXCLUSIVE,
    true,
    direction).AddAndReturnPointer();
  uartDirection_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ReadWriteRegister3Address()->Value(),
    kUARTDirectionPosition,
    2);
}

CLMC_MCDriverStatus *CLMC_MCDriver::Status() const {
  return status_;
}

CLMC_MCDriverOverride *CLMC_MCDriver::Override() const {
  return override_;
}

Framework::Components::ParameterItemOut<uint8_t> *CLMC_MCDriver::UARTDirection() const {
  return uartDirection_;
}

}  // namespace SystemModules::Modules::CLMC
