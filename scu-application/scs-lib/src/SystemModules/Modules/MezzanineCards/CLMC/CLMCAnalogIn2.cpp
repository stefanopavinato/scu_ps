/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Builder/Accessors/ReadAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemInBuilder.h"
#include "SystemModules/Framework/Builder/Components/ComponentBuilder.h"
#include "CLMCAnalogIn2.h"

namespace SystemModules::Modules::CLMC {

const char CLMCAnalogIn2::kName[] = "AnalogIn2";
const char CLMCAnalogIn2::kDescription[] = "";
const uint16_t CLMCAnalogIn2::kAddress = 0x41;

const uint32_t CLMCAnalogIn2::kOnDelay_ms = 100;
const uint32_t CLMCAnalogIn2::kOffDelay_ms = 100;

const uint32_t CLMCAnalogIn2::kCurrentScaleMultiply = 1000000;
const uint32_t CLMCAnalogIn2::kCurrentScaleDivide = 8450;

const char CLMCAnalogIn2::kReadyCurrentName[] = "ReadyCurrent";
const char CLMCAnalogIn2::kReadyCurrentDescription[] = "";
const char CLMCAnalogIn2::kReadyCurrentSubPath[] = "/in0_input";

CLMCAnalogIn2::CLMCAnalogIn2(
  Logger::ILogger *logger,
  const Modules::Configuration::ConfigurationCommon &configCommon,
  const std::string &path,
  const std::string &devicePath,
  const uint32_t &slot)
  : Framework::Components::I2CDeviceINA220(
  logger,
  kName,
  kDescription,
  slot + 1,
  kAddress,
  path,
  devicePath) {
  auto updateInterval = configCommon.UpdateInterval()->Fast()->Value();
  readyCurrent_ = Create().ItemIn<Types::Value<int32_t>>(
    kReadyCurrentName,
    kReadyCurrentDescription,
    Types::ValueGroup::Enum::SUPPLY,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::SHARED,
    Types::Unit::Enum::AMPERE,
    Types::UnitPrefix::Enum::MICRO).AddAndReturnPointer();
  readyCurrent_->CreateAccessor().File<int32_t, std::string>(
    GetDevicePath().append(kReadyCurrentSubPath),
    kCurrentScaleMultiply,
    kCurrentScaleDivide);  // provides current as 10 uA
//  readyCurrent_->CreateMonitoringFunction().IsLowerThan(
//      Types::AlertSeverity::Enum::WARNING,
//      Types::MonitoringFunctionId::Enum::SCU_MON_W_FUNCTION_clmc_current_RDY,
//      std::chrono::milliseconds(kOnDelay_ms),
//      std::chrono::milliseconds(kOffDelay_ms),
//      Types::Value<int32_t>(
//          0,
//          Types::Unit::Enum::AMPERE,
//          Types::UnitPrefix::Enum::MICRO));
//  readyCurrent_->CreateMonitoringFunction().IsInside(
//      Types::AlertSeverity::Enum::WARNING,
//      Types::MonitoringFunctionId::Enum::SCU_MON_W_FUNCTION_clmc_current_RDY,
//      std::chrono::milliseconds(kOnDelay_ms),
//      std::chrono::milliseconds(kOffDelay_ms),
//      Types::Value<int32_t>(
//          4,
//          Types::Unit::Enum::AMPERE,
//          Types::UnitPrefix::Enum::MICRO),
//      Types::Value<int32_t>(
//          11,
//          Types::Unit::Enum::AMPERE,
//          Types::UnitPrefix::Enum::MICRO));
//  readyCurrent_->CreateMonitoringFunction().IsHigherThan(
//      Types::AlertSeverity::Enum::WARNING,
//      Types::MonitoringFunctionId::Enum::SCU_MON_W_FUNCTION_clmc_current_RDY,
//      std::chrono::milliseconds(kOnDelay_ms),
//      std::chrono::milliseconds(kOffDelay_ms),
//      Types::Value<int32_t>(
//          19,
//          Types::Unit::Enum::AMPERE,
//          Types::UnitPrefix::Enum::MICRO));
}

Framework::Components::ItemIn<Types::Value<int32_t>> *CLMCAnalogIn2::ReadyCurrent() const {
  return readyCurrent_;
}

}  // namespace SystemModules::Modules::CLMC
