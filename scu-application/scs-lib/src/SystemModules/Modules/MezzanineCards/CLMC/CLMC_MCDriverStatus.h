/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "SystemModules/Framework/Components/ItemIn.h"
#include "SystemModules/Framework/Components/ItemOut.h"
#include "SystemModules/Framework/Components/Container.h"
#include "SystemModules/Modules/Configuration/ConfigurationCommon.h"
#include "SystemModules/Modules/Configuration/Slots/Configuration_Slots.h"

namespace SystemModules::Modules::CLMC {
class CLMC_MCDriverStatus : public Framework::Components::Container {
 public:
  CLMC_MCDriverStatus(
    Logger::ILogger *logger,
    const Modules::Configuration::ConfigurationCommon &configCommon,
    const Configuration::Configuration_Slots &config);

  [[nodiscard]] Framework::Components::ItemIn<uint16_t> *Collection() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *BeamPermit() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *RedundantBeamPermit() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *Ready() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *RedundantReady() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *BeamPermitA() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *BeamPermitB() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *ReadyA() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *ReadyB() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *Dl_BP_AB_Error() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *Dl_RDY_AB_Error() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *Dl_PBD_AB_Error() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *Dl_PBM_AB_Error() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *Dl_AnyBus_Error() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *Dl_AnyBus_Ready() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *Dl_Decoder_Ready() const;

  [[nodiscard]] Framework::Components::ItemIn<uint16_t> *PbdA() const;

  [[nodiscard]] Framework::Components::ItemIn<uint16_t> *PbdB() const;

  [[nodiscard]] Framework::Components::ItemIn<uint16_t> *PbmA() const;

  [[nodiscard]] Framework::Components::ItemIn<uint16_t> *PbmB() const;

  [[nodiscard]] Framework::Components::ItemIn<uint16_t> *LiveSignCounter() const;

  [[nodiscard]] Framework::Components::ItemIn<uint32_t> *SenderId() const;

  [[nodiscard]] Framework::Components::ItemIn<uint32_t> *DatalinkLifeSignErrors() const;

  [[nodiscard]] Framework::Components::ItemIn<uint32_t> *DatalinkTimeExpectationTimeouts() const;

  [[nodiscard]] Framework::Components::ItemOut<bool> *DatalinkLifeSignErrorsReset() const;

  [[nodiscard]] Framework::Components::ItemOut<bool> *DatalinkTimeExpectationTimeoutsReset() const;

  [[nodiscard]] Framework::Components::ItemIn<uint32_t> *DatalinkDebugInfo() const;

 private:
  Framework::Components::ItemIn<uint16_t> *collection_;

  Framework::Components::ItemIn<bool> *beamPermit_;

  Framework::Components::ItemIn<bool> *redundantBeamPermit_;

  Framework::Components::ItemIn<bool> *ready_;

  Framework::Components::ItemIn<bool> *redundantReady_;

  Framework::Components::ItemIn<bool> *beamPermitA_;

  Framework::Components::ItemIn<bool> *beamPermitB_;

  Framework::Components::ItemIn<bool> *readyA_;

  Framework::Components::ItemIn<bool> *readyB_;

  Framework::Components::ItemIn<bool> *Dl_BP_AB_Error_;

  Framework::Components::ItemIn<bool> *Dl_RDY_AB_Error_;

  Framework::Components::ItemIn<bool> *Dl_PBD_AB_Error_;

  Framework::Components::ItemIn<bool> *Dl_PBM_AB_Error_;

  Framework::Components::ItemIn<bool> *Dl_AnyBus_Error_;

  Framework::Components::ItemIn<bool> *Dl_AnyBus_Ready_;

  Framework::Components::ItemIn<bool> *Dl_Decoder_Ready_;

  Framework::Components::ItemIn<uint16_t> *liveSignCounter_;

  Framework::Components::ItemIn<uint16_t> *pbdA_;

  Framework::Components::ItemIn<uint16_t> *pbdB_;

  Framework::Components::ItemIn<uint16_t> *pbmA_;

  Framework::Components::ItemIn<uint16_t> *pbmB_;

  Framework::Components::ItemIn<uint32_t> *senderId_;

  Framework::Components::ItemIn<uint32_t> *datalinkLifeSignErrors_;

  Framework::Components::ItemIn<uint32_t> *datalinkTimeExpectationTimeouts_;

  Framework::Components::ItemOut<bool> *datalinkLifeSignErrorsReset_;

  Framework::Components::ItemOut<bool> *datalinkTimeExpectationTimeoutsReset_;

  Framework::Components::ItemIn<uint32_t> *datalinkDebugInfo_;

  static const char kName[];
  static const char kDescription[];

  static const char kCollectionName[];
  static const char kCollectionDescription[];
  static const uint32_t kCollectionPosition;

  static const char kBeamPermitName[];
  static const char kBeamPermitDescription[];
  static const uint32_t kBeamPermitPosition;

  static const char kRedundantBeamPermitName[];
  static const char kRedundantBeamPermitDescription[];
  static const uint32_t kRedundantBeamPermitPosition;

  static const char kReadyName[];
  static const char kReadyDescription[];
  static const uint32_t kReadyPosition;

  static const char kRedundantReadyName[];
  static const char kRedundantReadyDescription[];
  static const uint32_t kRedundantReadyPosition;

  static const char kBeamPermitAName[];
  static const char kBeamPermitADescription[];
  static const uint32_t kBeamPermitAPosition;

  static const char kBeamPermitBName[];
  static const char kBeamPermitBDescription[];
  static const uint32_t kBeamPermitBAddress;

  static const char kReadyAName[];
  static const char kReadyADescription[];
  static const uint32_t kReadyAAddress;

  static const char kReadyBName[];
  static const char kReadyBDescription[];
  static const uint32_t kReadyBAddress;

  static const char kDl_BP_AB_ErrorName[];
  static const char kDl_BP_AB_ErrorDescription[];
  static const uint32_t kDl_BP_AB_ErrorAddress;

  static const char kDl_RDY_AB_ErrorName[];
  static const char kDl_RDY_AB_ErrorDescription[];
  static const uint32_t kDl_RDY_AB_ErrorAddress;

  static const char kDl_PBD_AB_ErrorName[];
  static const char kDl_PBD_AB_ErrorDescription[];
  static const uint32_t kDl_PBD_AB_ErrorAddress;

  static const char kDl_PBM_AB_ErrorName[];
  static const char kDl_PBM_AB_ErrorDescription[];
  static const uint32_t kDl_PBM_AB_ErrorAddress;

  static const char kDl_AnyBus_errorName[];
  static const char kDl_AnyBus_errorDescription[];
  static const uint32_t kDl_AnyBus_errorAddress;

  static const char kDl_AnyBus_readyName[];
  static const char kDl_AnyBus_readyDescription[];
  static const uint32_t kDl_AnyBus_readyAddress;

  static const char kDl_Decoder_readyName[];
  static const char kDl_Decoder_readyDescription[];
  static const uint32_t kDl_Decoder_readyAddress;

  static const char kPbdAName[];
  static const char kPbdADescription[];
  static const uint32_t kPbdAAddress;

  static const char kPbdBName[];
  static const char kPbdBDescription[];
  static const uint32_t kPbdBAddress;

  static const char kPbmAName[];
  static const char kPbmADescription[];
  static const uint32_t kPbmAAddress;

  static const char kPbmBName[];
  static const char kPbmBDescription[];
  static const uint32_t kPbmBAddress;

  static const char kLiveSignCounterName[];
  static const char kLiveSignCounterDescription[];
  static const uint32_t kLiveSignCounterAddress;

  static const char kSenderIdName[];
  static const char kSenderIdDescription[];
  static const uint32_t kSenderIdAddress;

  static const char kDatalinkLifeSignErrorsName[];
  static const char kDatalinkLifeSignErrorsDescription[];
  static const uint32_t kDatalinkLifeSignErrorsAddress;

  static const char kDatalinkTimeExpectationTimeoutsName[];
  static const char kDatalinkTimeExpectationTimeoutsDescription[];
  static const uint32_t kDatalinkTimeExpectationTimeoutsAddress;

  static const char kDatalinkLifeSignErrorsResetName[];
  static const char kDatalinkLifeSignErrorsResetDescription[];
  static const uint32_t kDatalinkLifeSignErrorsResetAddress;

  static const char kDatalinkTimeExpectationTimeoutsResetName[];
  static const char kDatalinkTimeExpectationTimeoutsResetDescription[];
  static const uint32_t kDatalinkTimeExpectationTimeoutsResetAddress;

  static const char kDatalinkDebugInfoName[];
  static const char kDatalinkDebugInfoDescription[];
  static const uint32_t kDatalinkDebugInfoAddress;
};
}  // namespace SystemModules::Modules::CLMC
