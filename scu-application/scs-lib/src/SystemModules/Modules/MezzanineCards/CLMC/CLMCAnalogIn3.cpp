/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Builder/Accessors/ReadAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemInBuilder.h"
#include "SystemModules/Framework/Builder/Components/ComponentBuilder.h"
#include "CLMCAnalogIn3.h"

namespace SystemModules::Modules::CLMC {

const char CLMCAnalogIn3::kName[] = "AnalogIn3";
const char CLMCAnalogIn3::kDescription[] = "";
const uint16_t CLMCAnalogIn3::kAddress = 0x44;

const uint32_t CLMCAnalogIn3::kOnDelay_ms = 100;
const uint32_t CLMCAnalogIn3::kOffDelay_ms = 100;

const uint32_t CLMCAnalogIn3::kCurrentScaleMultiply = 1000000;
const uint32_t CLMCAnalogIn3::kCurrentScaleDivide = 8450;

const char CLMCAnalogIn3::kRedundantBeamPermitCurrentName[] = "RedundantBeamPermitCurrent";
const char CLMCAnalogIn3::kRedundantBeamPermitCurrentDescription[] = "";
const char CLMCAnalogIn3::kRedundantBeamPermitCurrentSubPath[] = "/in0_input";

const char CLMCAnalogIn3::kRedundantBeamPermitAndReadyVoltageName[] = "RedundantBeamPermitAndReadyVoltage";
const char CLMCAnalogIn3::kRedundantBeamPermitAndReadyVoltageDescription[] = "";
const char CLMCAnalogIn3::kRedundantBeamPermitAndReadyVoltageSubPath[] = "/in1_input";

CLMCAnalogIn3::CLMCAnalogIn3(
  Logger::ILogger *logger,
  const Modules::Configuration::ConfigurationCommon &configCommon,
  const std::string &path,
  const std::string &devicePath,
  const uint32_t &slot)
  : Framework::Components::I2CDeviceINA220(
  logger,
  kName,
  kDescription,
  slot + 1,
  kAddress,
  path,
  devicePath) {
  auto updateInterval = configCommon.UpdateInterval()->Fast()->Value();

  beamPermitCurrentRedundant_ = Create().ItemIn<Types::Value<int32_t>>(
    kRedundantBeamPermitCurrentName,
    kRedundantBeamPermitCurrentDescription,
    Types::ValueGroup::Enum::SUPPLY,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::SHARED,
    Types::Unit::Enum::AMPERE,
    Types::UnitPrefix::Enum::MICRO).AddAndReturnPointer();
  beamPermitCurrentRedundant_->CreateAccessor().File<int32_t, std::string>(
    GetDevicePath().append(kRedundantBeamPermitCurrentSubPath),
    kCurrentScaleMultiply,
    kCurrentScaleDivide);  // provides current as 10 uA
//  beamPermitCurrentRedundant_->CreateMonitoringFunction().IsLowerThan(
//      Types::AlertSeverity::Enum::WARNING,
//      Types::MonitoringFunctionId::Enum::SCU_MON_W_FUNCTION_clmc_current_redBP,
//      std::chrono::milliseconds(kOnDelay_ms),
//      std::chrono::milliseconds(kOffDelay_ms),
//      Types::Value<int32_t>(
//          0,
//          Types::Unit::Enum::AMPERE,
//          Types::UnitPrefix::Enum::MICRO));
//  beamPermitCurrentRedundant_->CreateMonitoringFunction().IsInside(
//      Types::AlertSeverity::Enum::WARNING,
//      Types::MonitoringFunctionId::Enum::SCU_MON_W_FUNCTION_clmc_current_redBP,
//      std::chrono::milliseconds(kOnDelay_ms),
//      std::chrono::milliseconds(kOffDelay_ms),
//      Types::Value<int32_t>(
//          4,
//          Types::Unit::Enum::AMPERE,
//          Types::UnitPrefix::Enum::MICRO),
//      Types::Value<int32_t>(
//          11,
//          Types::Unit::Enum::AMPERE,
//          Types::UnitPrefix::Enum::MICRO));
//  beamPermitCurrentRedundant_->CreateMonitoringFunction().IsHigherThan(
//      Types::AlertSeverity::Enum::WARNING,
//      Types::MonitoringFunctionId::Enum::SCU_MON_W_FUNCTION_clmc_current_redBP,
//      std::chrono::milliseconds(kOnDelay_ms),
//      std::chrono::milliseconds(kOffDelay_ms),
//      Types::Value<int32_t>(
//          19,
//          Types::Unit::Enum::AMPERE,
//          Types::UnitPrefix::Enum::MICRO));

  beamPermitReadyVoltageRedundant_ = Create().ItemIn<Types::Value<int32_t>>(
    kRedundantBeamPermitAndReadyVoltageName,
    kRedundantBeamPermitAndReadyVoltageDescription,
    Types::ValueGroup::Enum::SUPPLY,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::SHARED,
    Types::Unit::Enum::VOLTAGE,
    Types::UnitPrefix::Enum::MILLI).AddAndReturnPointer();
  beamPermitReadyVoltageRedundant_->CreateAccessor().File<int32_t, std::string>(
    GetDevicePath().append(kRedundantBeamPermitAndReadyVoltageSubPath),
    1,
    1);
//  beamPermitReadyVoltageRedundant_->CreateMonitoringFunction().IsOutside(
//      Types::AlertSeverity::Enum::WARNING,
//      Types::MonitoringFunctionId::Enum::SCU_MON_W_FUNCTION_clmc_voltage_redBP_redRDY,
//      std::chrono::milliseconds(kOnDelay_ms),
//      std::chrono::milliseconds(kOffDelay_ms),
//      Types::Value<int32_t>(
//          22000,
//          Types::Unit::Enum::VOLTAGE,
//          Types::UnitPrefix::Enum::MILLI),
//      Types::Value<int32_t>(
//          26000,
//          Types::Unit::Enum::VOLTAGE,
//          Types::UnitPrefix::Enum::MILLI));
}

Framework::Components::ItemIn<Types::Value<int32_t>> *
CLMCAnalogIn3::RedundantBeamPermitCurrent() const {
  return beamPermitCurrentRedundant_;
}

Framework::Components::ItemIn<Types::Value<int32_t>> *
CLMCAnalogIn3::RedundantBeamPermitAndReadyVoltage() const {
  return beamPermitReadyVoltageRedundant_;
}

}  // namespace SystemModules::Modules::CLMC
