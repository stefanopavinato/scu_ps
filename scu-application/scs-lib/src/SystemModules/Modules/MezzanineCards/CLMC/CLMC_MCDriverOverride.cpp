/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Builder/Actions/ActionBuilder.h"
#include "SystemModules/Framework/Builder/Accessors/ReadAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemInBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemOutBuilder.h"
#include "SystemModules/Framework/Builder/Components/ComponentBuilder.h"
#include "CLMC_MCDriverOverride.h"

namespace SystemModules::Modules::CLMC {

const char CLMC_MCDriverOverride::kName[] = "Override";
const char CLMC_MCDriverOverride::kDescription[] =
  "Allows to override individual input signals by means of an error-flag";

const char CLMC_MCDriverOverride::kCollectionName[] = "Collection";
const char CLMC_MCDriverOverride::kCollectionDescription[] = "Collection of CLMC signal override items";
const uint32_t CLMC_MCDriverOverride::kCollectionPosition = 0;

const char CLMC_MCDriverOverride::kBeamPermitName[] = "BeamPermit";
const char CLMC_MCDriverOverride::kBeamPermitDescription[] =
  "Override discrete Beam Permit signal. False==Do not override; True==Override with 'err-flag'";
const uint32_t CLMC_MCDriverOverride::kBeamPermitPosition = 0;

const char CLMC_MCDriverOverride::kRedundantBeamPermitName[] = "RedundantBeamPermit";
const char CLMC_MCDriverOverride::kRedundantBeamPermitDescription[] =
  "Override discrete Redundant Beam Permit signal. False==Do not override; True==Override with 'err-flag'";
const uint32_t CLMC_MCDriverOverride::kRedundantBeamPermitPosition = 1;

const char CLMC_MCDriverOverride::kReadyName[] = "Ready";
const char CLMC_MCDriverOverride::kReadyDescription[] =
  "Override discrete Ready signal. False==Do not override; True==Override with 'err-flag'";
const uint32_t CLMC_MCDriverOverride::kReadyPosition = 2;

const char CLMC_MCDriverOverride::kRedundantReadyName[] = "RedundantReady";
const char CLMC_MCDriverOverride::kRedundantReadyDescription[] =
  "Override discrete Redundant Ready signal. False==Do not override; True==Override with 'err-flag'";
const uint32_t CLMC_MCDriverOverride::kRedundantReadyPosition = 3;

const char CLMC_MCDriverOverride::kDataLinkBeamPermitName[] = "DataLinkBeamPermit";
const char CLMC_MCDriverOverride::kDataLinkBeamPermitDescription[] =
  "Override datalink Beam Permit signal. False==Do not override; True==Override with 'err-flag'";
const uint32_t CLMC_MCDriverOverride::kDataLinkBeamPermitPosition = 4;

const char CLMC_MCDriverOverride::kDataLinkReadyName[] = "DataLinkReady";
const char CLMC_MCDriverOverride::kDataLinkReadyDescription[] =
  "Override datalink Ready signal. False==Do not override; True==Override with 'err-flag'";
const uint32_t CLMC_MCDriverOverride::kDataLinkReadyPosition = 5;

const char CLMC_MCDriverOverride::kDataLinkPBDName[] = "DataLinkPBD";
const char CLMC_MCDriverOverride::kDataLinkPBDDescription[] =
  "Override datalink Proton Beam Destination signal. False==Do not override; True==Override with 'ERROR'";
const uint32_t CLMC_MCDriverOverride::kDataLinkPBDPosition = 6;

const char CLMC_MCDriverOverride::kDataLinkPBMName[] = "DataLinkPBM";
const char CLMC_MCDriverOverride::kDataLinkPBMDescription[] =
  "Override datalink Proton Beam Mode signal. False==Do not override; True==Override with 'ERROR'";
const uint32_t CLMC_MCDriverOverride::kDataLinkPBMPosition = 7;

CLMC_MCDriverOverride::CLMC_MCDriverOverride(
  Logger::ILogger *logger,
  const Modules::Configuration::ConfigurationCommon &configCommon,
  const Configuration::Configuration_Slots &config)
  : Framework::Components::Container(
  logger,
  kName,
  kDescription) {
  // Get update intervaval
  auto updateInterval = configCommon.UpdateInterval()->Fast()->Value();
  // Setup item collection
  collection_ = Create().ItemIn<uint32_t>(
    kCollectionName,
    kCollectionDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  collection_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ReadWriteRegister1Address()->Value(),
    kCollectionPosition,
    32);
  // Setup beam permit override output
  beamPermit_ = Create().ItemOut<bool>(
    kBeamPermitName,
    kBeamPermitDescription,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    true,
    false).AddAndReturnPointer();
  beamPermit_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ReadWriteRegister1Address()->Value(),
    kBeamPermitPosition,
    1);
  // Setup beam permit redundant override output
  redundantBeamPermit_ = Create().ItemOut<bool>(
    kRedundantBeamPermitName,
    kRedundantBeamPermitDescription,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    true,
    false).AddAndReturnPointer();
  redundantBeamPermit_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ReadWriteRegister1Address()->Value(),
    kRedundantBeamPermitPosition,
    1);
  // Setup ready override output
  ready_ = Create().ItemOut<bool>(
    kReadyName,
    kReadyDescription,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    true,
    false).AddAndReturnPointer();
  ready_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ReadWriteRegister1Address()->Value(),
    kReadyPosition,
    1);
  // Setup ready redundant override output
  redundantReady_ = Create().ItemOut<bool>(
    kRedundantReadyName,
    kRedundantReadyDescription,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    true,
    false).AddAndReturnPointer();
  redundantReady_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ReadWriteRegister1Address()->Value(),
    kRedundantReadyPosition,
    1);
  // Setup beam permit data link override output
  dataLinkBeamPermit_ = Create().ItemOut<bool>(
    kDataLinkBeamPermitName,
    kDataLinkBeamPermitDescription,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    true,
    false).AddAndReturnPointer();
  dataLinkBeamPermit_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ReadWriteRegister1Address()->Value(),
    kDataLinkBeamPermitPosition,
    1);
  // Setup ready data link override output
  dataLinkReady_ = Create().ItemOut<bool>(
    kDataLinkReadyName,
    kDataLinkReadyDescription,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    true,
    false).AddAndReturnPointer();
  dataLinkReady_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ReadWriteRegister1Address()->Value(),
    kDataLinkReadyPosition,
    1);
  // Setup pdb override output
  dataLinkPBD_ = Create().ItemOut<bool>(
    kDataLinkPBDName,
    kDataLinkPBDDescription,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    true,
    false).AddAndReturnPointer();
  dataLinkPBD_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ReadWriteRegister1Address()->Value(),
    kDataLinkPBDPosition,
    1);
  // Setup pbm override output
  dataLinkPBM_ = Create().ItemOut<bool>(
    kDataLinkPBMName,
    kDataLinkPBMDescription,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    true,
    false).AddAndReturnPointer();
  dataLinkPBM_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ReadWriteRegister1Address()->Value(),
    kDataLinkPBMPosition,
    1);

  (void) config;
}

Framework::Components::ItemIn<uint32_t> *CLMC_MCDriverOverride::Collection() const {
  return collection_;
}


Framework::Components::ItemOut<bool> *CLMC_MCDriverOverride::BeamPermit() const {
  return beamPermit_;
}

Framework::Components::ItemOut<bool> *CLMC_MCDriverOverride::RedundantBeamPermit() const {
  return redundantBeamPermit_;
}

Framework::Components::ItemOut<bool> *CLMC_MCDriverOverride::Ready() const {
  return ready_;
}

Framework::Components::ItemOut<bool> *CLMC_MCDriverOverride::RedundantReady() const {
  return redundantReady_;
}

Framework::Components::ItemOut<bool> *CLMC_MCDriverOverride::DataLinkBeamPermit() const {
  return dataLinkBeamPermit_;
}

Framework::Components::ItemOut<bool> *CLMC_MCDriverOverride::DataLinkReady() const {
  return dataLinkReady_;
}

Framework::Components::ItemOut<bool> *CLMC_MCDriverOverride::DataLinkPBD() const {
  return dataLinkPBD_;
}

Framework::Components::ItemOut<bool> *CLMC_MCDriverOverride::DataLinkPBM() const {
  return dataLinkPBM_;
}

}  // namespace SystemModules::Modules::CLMC
