/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Builder/Accessors/ReadAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemInBuilder.h"
#include "SystemModules/Framework/Builder/Components/ComponentBuilder.h"
#include "CLMCAnalogIn1.h"

namespace SystemModules::Modules::CLMC {

const char CLMCAnalogIn1::kName[] = "AnalogIn1";
const char CLMCAnalogIn1::kDescription[] = "";
const uint16_t CLMCAnalogIn1::kAddress = 0x40;

const uint32_t CLMCAnalogIn1::kOnDelay_ms = 100;
const uint32_t CLMCAnalogIn1::kOffDelay_ms = 100;

const uint32_t CLMCAnalogIn1::kCurrentScaleMultiply = 1000000;
const uint32_t CLMCAnalogIn1::kCurrentScaleDivide = 8450;

const char CLMCAnalogIn1::kBeamPermitCurrentName[] = "BeamPermitCurrent";
const char CLMCAnalogIn1::kBeamPermitCurrentDescription[] = "";
const char CLMCAnalogIn1::kBeamPermitCurrentSubPath[] = "/in0_input";

const char CLMCAnalogIn1::kBeamPermitAndReadyVoltageName[] = "BeamPermitAndReadyVoltage";
const char CLMCAnalogIn1::kBeamPermitAndReadyVoltageDescription[] = "";
const char CLMCAnalogIn1::kBeamPermitAndReadyVoltageSubPath[] = "/in1_input";

CLMCAnalogIn1::CLMCAnalogIn1(
  Logger::ILogger *logger,
  const Modules::Configuration::ConfigurationCommon &configCommon,
  const std::string &path,
  const std::string &devicePath,
  const uint16_t &slot)
  : Framework::Components::I2CDeviceINA220(
  logger,
  kName,
  kDescription,
  slot + 1,
  kAddress,
  path,
  devicePath) {
  auto updateInterval = configCommon.UpdateInterval()->Fast()->Value();

  beamPermitCurrent_ = Create().ItemIn<Types::Value<int32_t>>(
    kBeamPermitCurrentName,
    kBeamPermitCurrentDescription,
    Types::ValueGroup::Enum::SUPPLY,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::SHARED,
    Types::Unit::Enum::AMPERE,
    Types::UnitPrefix::Enum::MICRO).AddAndReturnPointer();
  beamPermitCurrent_->CreateAccessor().File<int32_t, std::string>(
    GetDevicePath().append(kBeamPermitCurrentSubPath),
    kCurrentScaleMultiply,
    kCurrentScaleDivide);  // provides current as 1 uA
//  beamPermitCurrent_->CreateMonitoringFunction().IsLowerThan(
//      Types::AlertSeverity::Enum::WARNING,
//      Types::MonitoringFunctionId::Enum::SCU_MON_W_FUNCTION_clmc_current_BP,
//      std::chrono::milliseconds(kOnDelay_ms),
//      std::chrono::milliseconds(kOffDelay_ms),
//      Types::Value<int32_t>(
//          0,
//          Types::Unit::Enum::AMPERE,
//          Types::UnitPrefix::Enum::MICRO));
//
//  beamPermitCurrent_->CreateMonitoringFunction().IsInside(
//      Types::AlertSeverity::Enum::WARNING,
//      Types::MonitoringFunctionId::Enum::SCU_MON_W_FUNCTION_clmc_current_BP,
//      std::chrono::milliseconds(kOnDelay_ms),
//      std::chrono::milliseconds(kOffDelay_ms),
//      Types::Value<int32_t>(
//          4,
//          Types::Unit::Enum::AMPERE,
//          Types::UnitPrefix::Enum::MICRO),
//      Types::Value<int32_t>(
//          11,
//          Types::Unit::Enum::AMPERE,
//          Types::UnitPrefix::Enum::MICRO));
//
//  beamPermitCurrent_->CreateMonitoringFunction().IsHigherThan(
//      Types::AlertSeverity::Enum::WARNING,
//      Types::MonitoringFunctionId::Enum::SCU_MON_W_FUNCTION_clmc_current_BP,
//      std::chrono::milliseconds(kOnDelay_ms),
//      std::chrono::milliseconds(kOffDelay_ms),
//      Types::Value<int32_t>(
//          19,
//          Types::Unit::Enum::AMPERE,
//          Types::UnitPrefix::Enum::MICRO));
//
  beamPermitAndReadyVoltage_ = Create().ItemIn<Types::Value<int32_t>>(
    kBeamPermitAndReadyVoltageName,
    kBeamPermitAndReadyVoltageDescription,
    Types::ValueGroup::Enum::SUPPLY,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::SHARED,
    Types::Unit::Enum::VOLTAGE,
    Types::UnitPrefix::Enum::MILLI).AddAndReturnPointer();
  beamPermitAndReadyVoltage_->CreateAccessor().File<int32_t, std::string>(
    GetDevicePath().append(kBeamPermitAndReadyVoltageSubPath),
    1,
    1);
//  beamPermitAndReadyVoltage_->CreateMonitoringFunction().IsOutside(
//      Types::AlertSeverity::Enum::WARNING,
//      Types::MonitoringFunctionId::Enum::SCU_MON_W_FUNCTION_clmc_voltage_BP_RDY,
//      std::chrono::milliseconds(kOnDelay_ms),
//      std::chrono::milliseconds(kOffDelay_ms),
//      Types::Value<int32_t>(
//          22000,
//          Types::Unit::Enum::VOLTAGE,
//          Types::UnitPrefix::Enum::MILLI),
//      Types::Value<int32_t>(
//          26000,
//          Types::Unit::Enum::VOLTAGE,
//          Types::UnitPrefix::Enum::MILLI));
}

Framework::Components::ItemIn<Types::Value<int32_t>> *CLMCAnalogIn1::BeamPermitCurrent() const {
  return beamPermitCurrent_;
}

Framework::Components::ItemIn<Types::Value<int32_t>> *CLMCAnalogIn1::BeamPermitAndReadyVoltage() const {
  return beamPermitAndReadyVoltage_;
}

}  // namespace SystemModules::Modules::CLMC
