/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "SystemModules/Framework/Components/Container.h"
#include "SystemModules/Modules/Configuration/ConfigurationCommon.h"
#include "SystemModules/Modules/Configuration/Modules/Configuration_CLMC.h"
#include "SystemModules/Modules/Configuration/Slots/Configuration_Slots.h"

namespace SystemModules::Modules {
namespace Common {
class VoltageMonitor_3V3_1V8;
class HotSwapController;
}  // namespace Common
namespace CLMC {
class CLMCAnalogIn1;

class CLMCAnalogIn2;

class CLMCAnalogIn3;

class CLMCAnalogIn4;

class CLMC_MCDriver;

class CLMC : public Framework::Components::Container {
 public:
  CLMC(
    Logger::ILogger *logger,
    const Modules::Configuration::Modules::Configuration_CLMC &configCLMC,
    const Modules::Configuration::ConfigurationCommon &configCommon,
    const Modules::Configuration::Configuration_Slots &config);

  [[nodiscard]] CLMCAnalogIn1 *AnalogIn1() const;

  [[nodiscard]] CLMCAnalogIn2 *AnalogIn2() const;

  [[nodiscard]] CLMCAnalogIn3 *AnalogIn3() const;

  [[nodiscard]] CLMCAnalogIn4 *AnalogIn4() const;

  [[nodiscard]] CLMC_MCDriver *MCDriver() const;

  [[nodiscard]] Common::VoltageMonitor_3V3_1V8 *VoltageMonitor() const;

  [[nodiscard]] Modules::Common::HotSwapController *HotSwapController() const;

 private:
  CLMCAnalogIn1 *analogIn1_;

  CLMCAnalogIn2 *analogIn2_;

  CLMCAnalogIn3 *analogIn3_;

  CLMCAnalogIn4 *analogIn4_;

  CLMC_MCDriver *mcDriver_;

  Common::VoltageMonitor_3V3_1V8 *voltageMonitor_;

  Modules::Common::HotSwapController *hotSwapController_;

  static const char kName[];
  static const char kDescription[];
};
}  // namespace CLMC
}  // namespace SystemModules::Modules
