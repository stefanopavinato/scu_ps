/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Types/Value.h"
#include "SystemModules/Framework/Components/ItemIn.h"
#include "SystemModules/Framework/Components/I2CDeviceINA220.h"
#include "SystemModules/Modules/Configuration/ConfigurationCommon.h"

namespace SystemModules::Modules::CLMC {
class CLMCAnalogIn1 : public Framework::Components::I2CDeviceINA220 {
 public:
  CLMCAnalogIn1(
    Logger::ILogger *logger,
    const Modules::Configuration::ConfigurationCommon &configCommon,
    const std::string &path,
    const std::string &devicePath,
    const uint16_t &slot);

  [[nodiscard]] Framework::Components::ItemIn<Types::Value<int32_t>> *BeamPermitCurrent() const;

  [[nodiscard]] Framework::Components::ItemIn<Types::Value<int32_t>> *BeamPermitAndReadyVoltage() const;

 private:
  Framework::Components::ItemIn<Types::Value<int32_t>> *beamPermitCurrent_;

  Framework::Components::ItemIn<Types::Value<int32_t>> *beamPermitAndReadyVoltage_;

  static const char kName[];
  static const char kDescription[];
  static const uint16_t kAddress;

  static const uint32_t kOnDelay_ms;
  static const uint32_t kOffDelay_ms;

  static const uint32_t kCurrentScaleMultiply;
  static const uint32_t kCurrentScaleDivide;

  static const char kBeamPermitCurrentName[];
  static const char kBeamPermitCurrentDescription[];
  static const char kBeamPermitCurrentSubPath[];

  static const char kBeamPermitAndReadyVoltageName[];
  static const char kBeamPermitAndReadyVoltageDescription[];
  static const char kBeamPermitAndReadyVoltageSubPath[];
};

}  // namespace SystemModules::Modules::CLMC
