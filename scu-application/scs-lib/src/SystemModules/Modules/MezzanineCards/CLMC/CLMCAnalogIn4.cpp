/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Builder/Accessors/ReadAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemInBuilder.h"
#include "SystemModules/Framework/Builder/Components/ComponentBuilder.h"
#include "CLMCAnalogIn4.h"

namespace SystemModules::Modules::CLMC {

const char CLMCAnalogIn4::kName[] = "AnalogIn4";
const char CLMCAnalogIn4::kDescription[] = "";
const uint16_t CLMCAnalogIn4::kAddress = 0x45;

const uint32_t CLMCAnalogIn4::kOnDelay_ms = 100;
const uint32_t CLMCAnalogIn4::kOffDelay_ms = 100;

const uint32_t CLMCAnalogIn4::kCurrentScaleMultiply = 1000000;
const uint32_t CLMCAnalogIn4::kCurrentScaleDivide = 8450;

const char CLMCAnalogIn4::kRedundantReadyCurrentName[] = "RedundantReadyCurrent";
const char CLMCAnalogIn4::kRedundantReadyCurrentDescription[] = "";
const char CLMCAnalogIn4::kRedundantReadyCurrentSubPath[] = "/in0_input";

CLMCAnalogIn4::CLMCAnalogIn4(
  Logger::ILogger *logger,
  const Modules::Configuration::ConfigurationCommon &configCommon,
  const std::string &path,
  const std::string &devicePath,
  const uint32_t &slot)
  : Framework::Components::I2CDeviceINA220(
  logger,
  kName,
  kDescription,
  slot + 1,
  kAddress,
  path,
  devicePath) {
  auto updateInterval = configCommon.UpdateInterval()->Fast()->Value();

  redundantReadyCurrent_ = Create().ItemIn<Types::Value<int32_t>>(
    kRedundantReadyCurrentName,
    kRedundantReadyCurrentDescription,
    Types::ValueGroup::Enum::SUPPLY,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::SHARED,
    Types::Unit::Enum::AMPERE,
    Types::UnitPrefix::Enum::MICRO).AddAndReturnPointer();
  redundantReadyCurrent_->CreateAccessor().File<int32_t, std::string>(
    GetDevicePath().append(kRedundantReadyCurrentSubPath),
    kCurrentScaleMultiply,
    kCurrentScaleDivide);  // provides current as 1 uA
//  redundantReadyCurrent_->CreateMonitoringFunction().IsLowerThan(
//      Types::AlertSeverity::Enum::WARNING,
//      Types::MonitoringFunctionId::Enum::SCU_MON_W_FUNCTION_clmc_current_redRDY,
//      std::chrono::milliseconds(kOnDelay_ms),
//      std::chrono::milliseconds(kOffDelay_ms),
//      Types::Value<int32_t>(
//          0,
//          Types::Unit::Enum::AMPERE,
//          Types::UnitPrefix::Enum::MICRO));
//  redundantReadyCurrent_->CreateMonitoringFunction().IsInside(
//      Types::AlertSeverity::Enum::WARNING,
//      Types::MonitoringFunctionId::Enum::SCU_MON_W_FUNCTION_clmc_current_redRDY,
//      std::chrono::milliseconds(kOnDelay_ms),
//      std::chrono::milliseconds(kOffDelay_ms),
//      Types::Value<int32_t>(
//          4,
//          Types::Unit::Enum::AMPERE,
//          Types::UnitPrefix::Enum::MICRO),
//      Types::Value<int32_t>(
//          11,
//          Types::Unit::Enum::AMPERE,
//          Types::UnitPrefix::Enum::MICRO));
//  redundantReadyCurrent_->CreateMonitoringFunction().IsHigherThan(
//      Types::AlertSeverity::Enum::WARNING,
//      Types::MonitoringFunctionId::Enum::SCU_MON_W_FUNCTION_clmc_current_redRDY,
//      std::chrono::milliseconds(kOnDelay_ms),
//      std::chrono::milliseconds(kOffDelay_ms),
//      Types::Value<int32_t>(
//          19,
//          Types::Unit::Enum::AMPERE,
//          Types::UnitPrefix::Enum::MILLI));
}

Framework::Components::ItemIn<Types::Value<int32_t>> *CLMCAnalogIn4::RedundantReadyCurrent() const {
  return redundantReadyCurrent_;
}

}  // namespace SystemModules::Modules::CLMC
