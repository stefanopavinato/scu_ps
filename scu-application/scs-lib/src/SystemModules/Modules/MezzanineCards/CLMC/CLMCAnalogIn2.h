/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Types/Value.h"
#include "SystemModules/Framework/Components/ItemIn.h"
#include "SystemModules/Framework/Components/I2CDeviceINA220.h"
#include "SystemModules/Modules/Configuration/ConfigurationCommon.h"

namespace SystemModules::Modules::CLMC {
class CLMCAnalogIn2 : public Framework::Components::I2CDeviceINA220 {
 public:
  CLMCAnalogIn2(
    Logger::ILogger *logger,
    const Modules::Configuration::ConfigurationCommon &configCommon,
    const std::string &path,
    const std::string &devicePath,
    const uint32_t &slot);

  [[nodiscard]] Framework::Components::ItemIn<Types::Value<int32_t>> *ReadyCurrent() const;

 private:
  Framework::Components::ItemIn<Types::Value<int32_t>> *readyCurrent_;
  static const char kName[];
  static const char kDescription[];
  static const uint16_t kAddress;

  static const uint32_t kOnDelay_ms;
  static const uint32_t kOffDelay_ms;

  static const uint32_t kCurrentScaleMultiply;
  static const uint32_t kCurrentScaleDivide;

  static const char kReadyCurrentName[];
  static const char kReadyCurrentDescription[];
  static const char kReadyCurrentSubPath[];
};

}  // namespace SystemModules::Modules::CLMC
