/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Builder/Actions/ActionBuilder.h"
#include "SystemModules/Framework/Builder/Actions/ActionBuilderGeneric.h"
#include "SystemModules/Modules/MezzanineCards/Common/VoltageMonitor_3V3_1V8.h"
#include "SystemModules/Modules/MezzanineCards/Common/HotSwapController.h"
#include "CLMC_MCDriverOverride.h"
#include "CLMC_MCDriverStatus.h"
#include "CLMCAnalogIn1.h"
#include "CLMCAnalogIn2.h"
#include "CLMCAnalogIn3.h"
#include "CLMCAnalogIn4.h"
#include "CLMC_MCDriver.h"
#include "CLMC.h"

namespace SystemModules::Modules::CLMC {

const char CLMC::kName[] = "Application";
const char CLMC::kDescription[] = "CLMC specific application";

CLMC::CLMC(
  Logger::ILogger *logger,
  const Modules::Configuration::Modules::Configuration_CLMC &configCLMC,
  const Modules::Configuration::ConfigurationCommon &configCommon,
  const Configuration::Configuration_Slots &config)
  : Container(
  logger,
  kName,
  kDescription) {
  // Create permit device
  auto analogIn1 = std::make_unique<CLMCAnalogIn1>(
    logger,
    configCommon,
    config.PathI2CApplication()->GetValue(),
    config.PathI2CApplicationDevice()->GetValue(),
    config.Slot()->GetValue());
  analogIn1_ = analogIn1.get();
  AddComponent(std::move(analogIn1));
  // Create permit redundant device
  auto analogIn2 = std::make_unique<CLMCAnalogIn2>(
    logger,
    configCommon,
    config.PathI2CApplication()->GetValue(),
    config.PathI2CApplicationDevice()->GetValue(),
    config.Slot()->GetValue());
  analogIn2_ = analogIn2.get();
  AddComponent(std::move(analogIn2));
  // Create ready device
  auto analogIn3 = std::make_unique<CLMCAnalogIn3>(
    logger,
    configCommon,
    config.PathI2CApplication()->GetValue(),
    config.PathI2CApplicationDevice()->GetValue(),
    config.Slot()->GetValue());
  analogIn3_ = analogIn3.get();
  AddComponent(std::move(analogIn3));
  // Create ready redundant device
  auto analogIn4 = std::make_unique<CLMCAnalogIn4>(
    logger,
    configCommon,
    config.PathI2CApplication()->GetValue(),
    config.PathI2CApplicationDevice()->GetValue(),
    config.Slot()->GetValue());
  analogIn4_ = analogIn4.get();
  AddComponent(std::move(analogIn4));
  // Crate clmc serializer
  auto serializer = std::make_unique<CLMC_MCDriver>(
    logger,
    configCommon,
    config);
  mcDriver_ = serializer.get();
  AddComponent(std::move(serializer));
  // Create voltage monitor
  auto voltageMonitor = std::make_unique<Common::VoltageMonitor_3V3_1V8>(
    logger,
    configCommon,
    config.PathI2CManagement()->GetValue());
  voltageMonitor_ = voltageMonitor.get();
  AddComponent(std::move(voltageMonitor));
  // Create hot swap controller
  auto hotSwapController = std::make_unique<Modules::Common::HotSwapController>(
    logger,
    *configCLMC.HotSwapController(),
    configCommon,
    config.PathI2CManagement()->GetValue(),
    0x4b);
  hotSwapController_ = hotSwapController.get();
  AddComponent(std::move(hotSwapController));
  // Create and add the actions
  {
    auto action = MCDriver()->Override()->BeamPermit()->CreateAction().Generic().AlertSeverityEqualOrHigher(
      Types::AlertSeverity::Enum::WARNING);
    action->AddStatus(AnalogIn1()->BeamPermitCurrent());
    action->AddStatus(AnalogIn1()->BeamPermitAndReadyVoltage());
  }
  {
    auto action = MCDriver()->Override()->Ready()->CreateAction().Generic().AlertSeverityEqualOrHigher(
      Types::AlertSeverity::Enum::WARNING);
    action->AddStatus(AnalogIn2()->ReadyCurrent());
    action->AddStatus(AnalogIn1()->BeamPermitAndReadyVoltage());
  }
  {
    auto action = MCDriver()->Override()->RedundantBeamPermit()->CreateAction().Generic().AlertSeverityEqualOrHigher(
      Types::AlertSeverity::Enum::WARNING);
    action->AddStatus(AnalogIn3()->RedundantBeamPermitCurrent());
    action->AddStatus(AnalogIn3()->RedundantBeamPermitAndReadyVoltage());
  }
  {
    auto action = MCDriver()->Override()->RedundantReady()->CreateAction().Generic().AlertSeverityEqualOrHigher(
      Types::AlertSeverity::Enum::WARNING);
    action->AddStatus(AnalogIn4()->RedundantReadyCurrent());
    action->AddStatus(AnalogIn3()->RedundantBeamPermitAndReadyVoltage());
  }
  MCDriver()->Override()->DataLinkPBM()->CreateAction().StaticValue(false);
  MCDriver()->Override()->DataLinkPBD()->CreateAction().StaticValue(false);
  MCDriver()->Override()->DataLinkReady()->CreateAction().StaticValue(false);
  MCDriver()->Override()->DataLinkBeamPermit()->CreateAction().StaticValue(false);
}

CLMCAnalogIn1 *CLMC::AnalogIn1() const {
  return analogIn1_;
}

CLMCAnalogIn2 *CLMC::AnalogIn2() const {
  return analogIn2_;
}

CLMCAnalogIn3 *CLMC::AnalogIn3() const {
  return analogIn3_;
}

CLMCAnalogIn4 *CLMC::AnalogIn4() const {
  return analogIn4_;
}

CLMC_MCDriver *CLMC::MCDriver() const {
  return mcDriver_;
}

Modules::Common::VoltageMonitor_3V3_1V8 *CLMC::VoltageMonitor() const {
  return voltageMonitor_;
}

Modules::Common::HotSwapController *CLMC::HotSwapController() const {
  return hotSwapController_;
}

}  // namespace SystemModules::Modules::CLMC
