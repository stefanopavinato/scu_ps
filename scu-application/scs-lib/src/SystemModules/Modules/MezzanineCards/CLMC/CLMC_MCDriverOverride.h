/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "SystemModules/Framework/Components/ItemIn.h"
#include "SystemModules/Framework/Components/ItemOut.h"
#include "SystemModules/Framework/Components/Container.h"
#include "SystemModules/Modules/Configuration/ConfigurationCommon.h"
#include "SystemModules/Modules/Configuration/Slots/Configuration_Slots.h"

namespace SystemModules::Modules::CLMC {
class CLMC_MCDriverOverride : public Framework::Components::Container {
 public:
  CLMC_MCDriverOverride(
    Logger::ILogger *logger,
    const Modules::Configuration::ConfigurationCommon &configCommon,
    const Configuration::Configuration_Slots &config);

  [[nodiscard]] Framework::Components::ItemIn<uint32_t> *Collection() const;

  [[nodiscard]] Framework::Components::ItemOut<bool> *BeamPermit() const;

  [[nodiscard]] Framework::Components::ItemOut<bool> *RedundantBeamPermit() const;

  [[nodiscard]] Framework::Components::ItemOut<bool> *Ready() const;

  [[nodiscard]] Framework::Components::ItemOut<bool> *RedundantReady() const;

  [[nodiscard]] Framework::Components::ItemOut<bool> *DataLinkBeamPermit() const;

  [[nodiscard]] Framework::Components::ItemOut<bool> *DataLinkReady() const;

  [[nodiscard]] Framework::Components::ItemOut<bool> *DataLinkPBD() const;

  [[nodiscard]] Framework::Components::ItemOut<bool> *DataLinkPBM() const;

 private:
  Framework::Components::ItemIn<uint32_t> *collection_;

  Framework::Components::ItemOut<bool> *beamPermit_;

  Framework::Components::ItemOut<bool> *redundantBeamPermit_;

  Framework::Components::ItemOut<bool> *ready_;

  Framework::Components::ItemOut<bool> *redundantReady_;

  Framework::Components::ItemOut<bool> *dataLinkBeamPermit_;

  Framework::Components::ItemOut<bool> *dataLinkReady_;

  Framework::Components::ItemOut<bool> *dataLinkPBD_;

  Framework::Components::ItemOut<bool> *dataLinkPBM_;

  static const char kName[];
  static const char kDescription[];

  static const char kCollectionName[];
  static const char kCollectionDescription[];
  static const uint32_t kCollectionPosition;

  static const char kBeamPermitName[];
  static const char kBeamPermitDescription[];
  static const uint32_t kBeamPermitPosition;

  static const char kRedundantBeamPermitName[];
  static const char kRedundantBeamPermitDescription[];
  static const uint32_t kRedundantBeamPermitPosition;

  static const char kReadyName[];
  static const char kReadyDescription[];
  static const uint32_t kReadyPosition;

  static const char kRedundantReadyName[];
  static const char kRedundantReadyDescription[];
  static const uint32_t kRedundantReadyPosition;

  static const char kDataLinkBeamPermitName[];
  static const char kDataLinkBeamPermitDescription[];
  static const uint32_t kDataLinkBeamPermitPosition;

  static const char kDataLinkReadyName[];
  static const char kDataLinkReadyDescription[];
  static const uint32_t kDataLinkReadyPosition;

  static const char kDataLinkPBDName[];
  static const char kDataLinkPBDDescription[];
  static const uint32_t kDataLinkPBDPosition;

  static const char kDataLinkPBMName[];
  static const char kDataLinkPBMDescription[];
  static const uint32_t kDataLinkPBMPosition;
};
}  // namespace SystemModules::Modules::CLMC
