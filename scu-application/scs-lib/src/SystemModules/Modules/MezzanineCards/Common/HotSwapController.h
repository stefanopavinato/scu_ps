/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "SystemModules/Framework/Components/ItemIn.h"
#include "SystemModules/Framework/Components/I2CDeviceLTC4215.h"
#include "SystemModules/Modules/Configuration/ConfigurationCommon.h"
#include "SystemModules/Modules/Configuration/Devices/Configuration_HotSwapController.h"

namespace SystemModules::Modules::Common {
class HotSwapController : public Framework::Components::I2CDeviceLTC4215 {
 public:
  HotSwapController(
    Logger::ILogger *logger,
    const Configuration::Devices::Configuration_HotSwapController &configHotSwapController,
    const Configuration::ConfigurationCommon &configCommon,
    const std::string &path,
    const std::uint16_t &i2caddr);

  [[nodiscard]] Framework::Components::ItemIn<Types::Value<int32_t>> *MainCurrent() const;

  [[nodiscard]] Framework::Components::ItemIn<Types::Value<int32_t>> *MainVoltage() const;

  [[nodiscard]] Framework::Components::ItemIn<Types::Value<int32_t>> *StandbyVoltage() const;

 private:
  Framework::Components::ItemIn<Types::Value<int32_t>> *mainCurrent_;

  Framework::Components::ItemIn<Types::Value<int32_t>> *mainVoltage_;

  Framework::Components::ItemIn<Types::Value<int32_t>> *standbyVoltage_;

  static const char kName[];
  static const char kDescription[];
  static const uint16_t kAddress;

  static const uint32_t kOnDelay_ms;
  static const uint32_t kOffDelay_ms;

  static const char kMainCurrentName[];
  static const char kMainCurrentDescription[];
  static const char kMainCurrentSubPath[];

  static const char kMainVoltageName[];
  static const char kMainVoltageDescription[];
  static const char kMainVoltageSubPath[];

  static const char kStandbyVoltageName[];
  static const char kStandbyVoltageDescription[];
  static const char kStandbyVoltageSubPath[];
};
}  // namespace SystemModules::Modules::Common
