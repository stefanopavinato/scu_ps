/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "SystemModules/Framework/Components/ItemIn.h"
#include "SystemModules/Framework/Components/ParameterItemOut.h"
#include "SystemModules/Framework/Components/I2CDeviceADS1115.h"
#include "SystemModules/Modules/Configuration/ConfigurationCommon.h"

namespace SystemModules::Modules::Common {
class VoltageMonitor_3V3_2V5_1V8_1V2 : public Framework::Components::I2CDeviceADS1115 {
 public:
  VoltageMonitor_3V3_2V5_1V8_1V2(
    Logger::ILogger *logger,
    const Modules::Configuration::ConfigurationCommon &configCommon,
    const std::string &path);

  [[nodiscard]] Framework::Components::ItemIn<Types::Value<int32_t>> *Voltage1V2() const;

  [[nodiscard]] Framework::Components::ItemIn<Types::Value<int32_t>> *Voltage1V8() const;

  [[nodiscard]] Framework::Components::ItemIn<Types::Value<int32_t>> *Voltage2V5() const;

  [[nodiscard]] Framework::Components::ItemIn<Types::Value<int32_t>> *Voltage3V3() const;

  [[nodiscard]] Framework::Components::ParameterItemOut<double> *VoltageScale1V2() const;

  [[nodiscard]] Framework::Components::ParameterItemOut<double> *VoltageScale1V8() const;

  [[nodiscard]] Framework::Components::ParameterItemOut<double> *VoltageScale2V5() const;

  [[nodiscard]] Framework::Components::ParameterItemOut<double> *VoltageScale3V3() const;

 private:
  Framework::Components::ItemIn<Types::Value<int32_t>> *voltage1V2_;

  Framework::Components::ItemIn<Types::Value<int32_t>> *voltage1V8_;

  Framework::Components::ItemIn<Types::Value<int32_t>> *voltage2V5_;

  Framework::Components::ItemIn<Types::Value<int32_t>> *voltage3V3_;

  Framework::Components::ParameterItemOut<double> *voltageScale1V2_;

  Framework::Components::ParameterItemOut<double> *voltageScale1V8_;

  Framework::Components::ParameterItemOut<double> *voltageScale2V5_;

  Framework::Components::ParameterItemOut<double> *voltageScale3V3_;

  static const char kName[];
  static const char kDescription[];
  static const uint16_t kAddress;

  static const uint32_t kOnDelay_ms;
  static const uint32_t kOffDelay_ms;

  static const char kVoltage1V2Name[];
  static const char kVoltage1V2Description[];
  static const char kVoltage1V2SubPath[];

  static const char kVoltage1V8Name[];
  static const char kVoltage1V8Description[];
  static const char kVoltage1V8SubPath[];

  static const char kVoltage2V5Name[];
  static const char kVoltage2V5Description[];
  static const char kVoltage2V5SubPath[];

  static const char kVoltage3V3Name[];
  static const char kVoltage3V3Description[];
  static const char kVoltage3V3SubPath[];

  static const char kVoltageScale1V2Name[];
  static const char kVoltageScale1V2Description[];
  static const char kVoltageScale1V2SubPath[];
  static const double kVoltageScale1V2Value;

  static const char kVoltageScale1V8Name[];
  static const char kVoltageScale1V8Description[];
  static const char kVoltageScale1V8SubPath[];
  static const double kVoltageScale1V8Value;

  static const char kVoltageScale2V5Name[];
  static const char kVoltageScale2V5Description[];
  static const char kVoltageScale2V5SubPath[];
  static const double kVoltageScale2V5Value;

  static const char kVoltageScale3V3Name[];
  static const char kVoltageScale3V3Description[];
  static const char kVoltageScale3V3SubPath[];
  static const double kVoltageScale3V3Value;
};
}  // namespace SystemModules::Modules::Common
