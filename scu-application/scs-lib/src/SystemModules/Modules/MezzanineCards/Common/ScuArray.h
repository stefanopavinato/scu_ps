/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "SystemModules/Framework/Components/ItemIn.h"
#include "SystemModules/Framework/Components/Container.h"
#include "SystemModules/Modules/Configuration/ConfigurationCommon.h"
#include "SystemModules/Modules/Configuration/Slots/Configuration_Slots.h"

namespace SystemModules::Modules::Common {
class ScuArray : public Framework::Components::Container {
 public:
  ScuArray(
    Logger::ILogger *logger,
    const Modules::Configuration::ConfigurationCommon &configCommon,
    const Configuration::Configuration_Slots &config);

  [[nodiscard]] Framework::Components::ItemIn<std::vector<uint32_t>> *OkNok() const;

  [[nodiscard]] Framework::Components::ItemIn<std::vector<uint32_t>> *Nds() const;

  [[nodiscard]] Framework::Components::ItemIn<std::vector<uint32_t>> *UserData() const;

 private:
  Framework::Components::ItemIn<std::vector<uint32_t>> *okNok_;

  Framework::Components::ItemIn<std::vector<uint32_t>> *nds_;

  Framework::Components::ItemIn<std::vector<uint32_t>> *userData_;

  static const char kName[];
  static const char kDescription[];

  static const char kOkNokName[];
  static const char kOkNokDescription[];

  static const char kNdsName[];
  static const char kNdsDescription[];

  static const char kUserDataName[];
  static const char kUserDataDescription[];
};

}  // namespace SystemModules::Modules::Common
