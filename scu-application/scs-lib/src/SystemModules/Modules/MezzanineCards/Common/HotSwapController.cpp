/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Builder/Accessors/ReadAccessorBuilder.h"
#include "SystemModules/Framework/Builder/MonitoringFunctions/MFBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemInBuilder.h"
#include "SystemModules/Framework/Builder/Components/ComponentBuilder.h"
#include "HotSwapController.h"

namespace SystemModules::Modules::Common {

const char HotSwapController::kName[] = "HotSwapController";
const char HotSwapController::kDescription[] = "";
const uint16_t HotSwapController::kAddress = 0x4b;

const uint32_t HotSwapController::kOnDelay_ms = 100;
const uint32_t HotSwapController::kOffDelay_ms = 100;

const char HotSwapController::kMainCurrentName[] = "MainCurrent";
const char HotSwapController::kMainCurrentDescription[] = "Current of the MAIN Supply";
const char HotSwapController::kMainCurrentSubPath[] = "/curr1_input";

const char HotSwapController::kMainVoltageName[] = "MainVoltage";
const char HotSwapController::kMainVoltageDescription[] = "Voltage of the MAIN Supply";
const char HotSwapController::kMainVoltageSubPath[] = "/in2_input";

const char HotSwapController::kStandbyVoltageName[] = "StandbyVoltage";
const char HotSwapController::kStandbyVoltageDescription[] = "Voltage of the STANDBY Supply";
const char HotSwapController::kStandbyVoltageSubPath[] = "/in1_input";

HotSwapController::HotSwapController(
  Logger::ILogger *logger,
  const Configuration::Devices::Configuration_HotSwapController &configHotSwapController,
  const Configuration::ConfigurationCommon &configCommon,
  const std::string &path,
  const std::uint16_t &i2caddress)
  : I2CDeviceLTC4215(
  logger,
  kName,
  kDescription,
  i2caddress,
  path) {
  // Get update intervaval
  auto updateInterval = configCommon.UpdateInterval()->Medium()->Value();
  // Setup items
  mainCurrent_ = Create().ItemIn<Types::Value<int32_t>>(
    kMainCurrentName,
    kMainCurrentDescription,
    Types::ValueGroup::Enum::SUPPLY,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::SHARED,
    Types::Unit::Enum::AMPERE,
    Types::UnitPrefix::Enum::MILLI).AddAndReturnPointer();
  mainCurrent_->CreateAccessor().File<int32_t, std::string>(
    GetDevicePath().append(kMainCurrentSubPath),
    configHotSwapController.MainCurrentScaleMultiply()->Value(),
    configHotSwapController.MainCurrentScaleDivide()->Value());

//  mainCurrent_->CreateMonitoringFunction().IsHigherThan(
//      Types::AlertSeverity::Enum::WARNING,
//      Types::MonitoringFunctionId::Enum::SCU_MON_W_FUNCTION_main_current,
//      std::chrono::milliseconds(kOnDelay_ms),
//      std::chrono::milliseconds(kOffDelay_ms),
//      Types::Value<int32_t>(
//          1000,
//          Types::Unit::Enum::AMPERE,
//          Types::UnitPrefix::Enum::MILLI));
//  mainCurrent_->CreateMonitoringFunction().IsHigherThan(
//      Types::AlertSeverity::Enum::ERROR,
//      Types::MonitoringFunctionId::Enum::SCU_MON_W_FUNCTION_main_current,
//      std::chrono::milliseconds(kOnDelay_ms),
//      std::chrono::milliseconds(kOffDelay_ms),
//      Types::Value<int32_t>(
//          1500,
//          Types::Unit::Enum::AMPERE,
//          Types::UnitPrefix::Enum::MILLI));

  mainVoltage_ = Create().ItemIn<Types::Value<int32_t>>(
    kMainVoltageName,
    kMainVoltageDescription,
    Types::ValueGroup::Enum::SUPPLY,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::SHARED,
    Types::Unit::Enum::VOLTAGE,
    Types::UnitPrefix::Enum::MILLI).AddAndReturnPointer();
  mainVoltage_->CreateAccessor().File<int32_t, std::string>(
    GetDevicePath().append(kMainVoltageSubPath),
    configHotSwapController.MainVoltageScaleMultiply()->Value(),
    configHotSwapController.MainVoltageScaleDivide()->Value());
//  mainVoltage_->CreateMonitoringFunction().IsOutside(
//      Types::AlertSeverity::Enum::WARNING,
//      Types::MonitoringFunctionId::Enum::SCU_MON_W_FUNCTION_internal_main_voltage,
//      std::chrono::milliseconds(kOnDelay_ms),
//      std::chrono::milliseconds(kOffDelay_ms),
//      Types::Value<int32_t>(
//          11400,
//          Types::Unit::Enum::VOLTAGE,
//          Types::UnitPrefix::Enum::MILLI),
//      Types::Value<int32_t>(
//          12600,
//          Types::Unit::Enum::VOLTAGE,
//          Types::UnitPrefix::Enum::MILLI));
  /*
  mainVoltage_->CreateMonitoringFunction().IsOutside(
      Types::AlertSeverity::Enum::ERROR,
      Types::MonitoringFunctionId::Enum::SCU_MON_E_FUNCTION_internal_main_voltage,
      std::chrono::milliseconds(kOnDelay_ms),
      std::chrono::milliseconds(kOffDelay_ms),
      Types::Value<int32_t>(
          10800,
          Types::Unit::Enum::VOLTAGE,
          Types::UnitPrefix::Enum::MILLI),
      Types::Value<int32_t>(
          13200,
          Types::Unit::Enum::VOLTAGE,
          Types::UnitPrefix::Enum::MILLI)).Add();
  */
  standbyVoltage_ = Create().ItemIn<Types::Value<int32_t>>(
    kStandbyVoltageName,
    kStandbyVoltageDescription,
    Types::ValueGroup::Enum::SUPPLY,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::SHARED,
    Types::Unit::Enum::VOLTAGE,
    Types::UnitPrefix::Enum::MILLI).AddAndReturnPointer();
  standbyVoltage_->CreateAccessor().File<int32_t, std::string>(
    GetDevicePath().append(kStandbyVoltageSubPath),
    configHotSwapController.StandbyVoltageScaleMultiply()->Value(),
    configHotSwapController.StandbyVoltageScaleDivide()->Value());

//  standbyVoltage_->CreateMonitoringFunction().IsOutside(
//      Types::AlertSeverity::Enum::WARNING,
//      Types::MonitoringFunctionId::Enum::SCU_MON_W_FUNCTION_internal_standby_voltage,
//      std::chrono::milliseconds(kOnDelay_ms),
//      std::chrono::milliseconds(kOffDelay_ms),
//      Types::Value<int32_t>(
//          4750,
//          Types::Unit::Enum::VOLTAGE,
//          Types::UnitPrefix::Enum::MILLI),
//      Types::Value<int32_t>(
//          5250,
//          Types::Unit::Enum::VOLTAGE,
//          Types::UnitPrefix::Enum::MILLI));
//  standbyVoltage_->CreateMonitoringFunction().IsOutside(
//      Types::AlertSeverity::Enum::ERROR,
//      Types::MonitoringFunctionId::Enum::SCU_MON_E_FUNCTION_internal_standby_voltage,
//      std::chrono::milliseconds(kOnDelay_ms),
//      std::chrono::milliseconds(kOffDelay_ms),
//      Types::Value<int32_t>(
//          4500,
//          Types::Unit::Enum::VOLTAGE,
//          Types::UnitPrefix::Enum::MILLI),
//      Types::Value<int32_t>(
//          5500,
//          Types::Unit::Enum::VOLTAGE,
//          Types::UnitPrefix::Enum::MILLI));
}

Framework::Components::ItemIn<Types::Value<int32_t>> *HotSwapController::MainCurrent() const {
  return mainCurrent_;
}

Framework::Components::ItemIn<Types::Value<int32_t>> *HotSwapController::MainVoltage() const {
  return mainVoltage_;
}

Framework::Components::ItemIn<Types::Value<int32_t>> *HotSwapController::StandbyVoltage() const {
  return standbyVoltage_;
}

}  // namespace SystemModules::Modules::Common
