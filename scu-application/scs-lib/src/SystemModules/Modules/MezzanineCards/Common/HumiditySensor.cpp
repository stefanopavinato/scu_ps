/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Builder/Accessors/ReadAccessorBuilder.h"
#include "SystemModules/Framework/Builder/MonitoringFunctions/MFBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemInBuilder.h"
#include "SystemModules/Framework/Builder/Components/ComponentBuilder.h"
#include "HumiditySensor.h"

namespace SystemModules::Modules::Common {

const char HumiditySensor::kName[] = "HumiditySensor";
const char HumiditySensor::kDescription[] = "";
const uint16_t HumiditySensor::kAddress = 0x41;

const uint32_t HumiditySensor::kOnDelay_ms = 100;
const uint32_t HumiditySensor::kOffDelay_ms = 100;

const char HumiditySensor::kHumidityName[] = "Humidity";
const char HumiditySensor::kHumidityDescription[] = "Humidity at the MAIN Supply";
const char HumiditySensor::kHumiditySubPath[] = "/in_humidityrelative_raw";

const char HumiditySensor::kTemperatureName[] = "Temperature";
const char HumiditySensor::kTemperatureDescription[] = "Temperature of the humidity sensor on the MAIN Supply";
const char HumiditySensor::kTemperatureSubPath[] = "/in_temp_raw";

HumiditySensor::HumiditySensor(
  Logger::ILogger *logger,
  const Modules::Configuration::ConfigurationCommon &configCommon,
  const std::string &path,
  const std::uint16_t &i2caddress)
  : I2CDeviceHDC2080(
  logger,
  kName,
  kDescription,
  i2caddress,
  path) {
  // Get update intervaval
  auto updateInterval = configCommon.UpdateInterval()->Slow()->Value();
  // Setup items
  humidity_ = Create().ItemIn<Types::Value<int32_t>>(
    kHumidityName,
    kHumidityDescription,
    Types::ValueGroup::Enum::SUPPLY,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::Unit::Enum::PERCENT,
    Types::UnitPrefix::Enum::DECI).AddAndReturnPointer();
  humidity_->CreateAccessor().File<int32_t, std::string>(
    GetDevicePath().append(kHumiditySubPath),
    125,  // %RH = (value_raw * 100) / 2**16, see data sheet hdc2080 table 14 on page 18
    0x2000);

  temperature_ = Create().ItemIn<Types::Value<int32_t>>(
    kTemperatureName,
    kTemperatureDescription,
    Types::ValueGroup::Enum::SUPPLY,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::Unit::Enum::DEGREE_CELSIUS,
    Types::UnitPrefix::Enum::MILLI).AddAndReturnPointer();
  temperature_->CreateAccessor().File<int32_t, std::string>(
    GetDevicePath().append(kTemperatureSubPath),
    20625,
    0x2000,
    -40000);
}

Framework::Components::ItemIn<Types::Value<int32_t>> *HumiditySensor::Humidity() const {
  return humidity_;
}

Framework::Components::ItemIn<Types::Value<int32_t>> *HumiditySensor::Temperature() const {
  return temperature_;
}

}  // namespace SystemModules::Modules::Common
