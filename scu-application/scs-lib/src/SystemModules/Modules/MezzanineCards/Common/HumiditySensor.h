/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "SystemModules/Framework/Components/ItemIn.h"
#include "SystemModules/Framework/Components/I2CDeviceHDC2080.h"
#include "SystemModules/Modules/Configuration/ConfigurationCommon.h"

namespace SystemModules::Modules::Common {
class HumiditySensor : public Framework::Components::I2CDeviceHDC2080 {
 public:
  HumiditySensor(
    Logger::ILogger *logger,
    const Modules::Configuration::ConfigurationCommon &configCommon,
    const std::string &path,
    const std::uint16_t &i2caddr);

  [[nodiscard]] Framework::Components::ItemIn<Types::Value<int32_t>> *Humidity() const;

  [[nodiscard]] Framework::Components::ItemIn<Types::Value<int32_t>> *Temperature() const;

 private:
  Framework::Components::ItemIn<Types::Value<int32_t>> *humidity_;

  Framework::Components::ItemIn<Types::Value<int32_t>> *temperature_;

  static const char kName[];
  static const char kDescription[];
  static const uint16_t kAddress;

  static const uint32_t kOnDelay_ms;
  static const uint32_t kOffDelay_ms;

  static const char kHumidityName[];
  static const char kHumidityDescription[];
  static const char kHumiditySubPath[];

  static const char kTemperatureName[];
  static const char kTemperatureDescription[];
  static const char kTemperatureSubPath[];
};
}  // namespace SystemModules::Modules::Common
