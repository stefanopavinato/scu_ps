/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Builder/Accessors/ReadAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemInBuilder.h"
#include "SystemModules/Framework/Builder/Components/ComponentBuilder.h"
#include "ScuArray.h"

namespace SystemModules::Modules::Common {

const char ScuArray::kName[] = "ScuArray";
const char ScuArray::kDescription[] = "";

const char ScuArray::kOkNokName[] = "OkNok";
const char ScuArray::kOkNokDescription[] = "";

const char ScuArray::kNdsName[] = "Nds";
const char ScuArray::kNdsDescription[] = "";

const char ScuArray::kUserDataName[] = "UserData";
const char ScuArray::kUserDataDescription[] = "";

ScuArray::ScuArray(
  Logger::ILogger *logger,
  const Modules::Configuration::ConfigurationCommon &configCommon,
  const Configuration::Configuration_Slots &config)
  : Framework::Components::Container(
  logger,
  kName,
  kDescription) {
  auto updateInterval = configCommon.UpdateInterval()->Medium()->Value();
  // Setup ok nok signal
  okNok_ = Create().ItemIn<std::vector<uint32_t>>(
    kOkNokName,
    kOkNokDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    config.Scu()->Arrays()->OkNok()->Size()).AddAndReturnPointer();
  okNok_->CreateAccessor().ScuRegisterVector(
    config.Scu()->Arrays()->OkNok()->Address());

  // Setup nds signal
  nds_ = Create().ItemIn<std::vector<uint32_t>>(
    kNdsName,
    kNdsDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    config.Scu()->Arrays()->Nds()->Size()).AddAndReturnPointer();
  nds_->CreateAccessor().ScuRegisterVector(
    config.Scu()->Arrays()->Nds()->Address());

  // Setup user data signal
  userData_ = Create().ItemIn<std::vector<uint32_t>>(
    kUserDataName,
    kUserDataDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    config.Scu()->Arrays()->UserData()->Size()).AddAndReturnPointer();
  userData_->CreateAccessor().ScuRegisterVector(
    config.Scu()->Arrays()->UserData()->Address());
}


Framework::Components::ItemIn<std::vector<uint32_t>> *ScuArray::OkNok() const {
  return okNok_;
}

Framework::Components::ItemIn<std::vector<uint32_t>> *ScuArray::Nds() const {
  return nds_;
}

Framework::Components::ItemIn<std::vector<uint32_t>> *ScuArray::UserData() const {
  return userData_;
}

}  // namespace SystemModules::Modules::Common
