/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Builder/MonitoringFunctions/MFBuilder.h"
#include "SystemModules/Framework/Builder/Accessors/ReadAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Accessors/WriteAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemInBuilder.h"
#include "SystemModules/Framework/Builder/Components/ParameterItemOutBuilder.h"
#include "SystemModules/Framework/Builder/Components/ComponentBuilder.h"
#include "VoltageMonitor_3V3_2V5_1V8_1V2.h"

namespace SystemModules::Modules::Common {

const char VoltageMonitor_3V3_2V5_1V8_1V2::kName[] = "VoltageMonitor";
const char VoltageMonitor_3V3_2V5_1V8_1V2::kDescription[] = "";
const uint16_t VoltageMonitor_3V3_2V5_1V8_1V2::kAddress = 0x4a;

const uint32_t VoltageMonitor_3V3_2V5_1V8_1V2::kOnDelay_ms = 100;
const uint32_t VoltageMonitor_3V3_2V5_1V8_1V2::kOffDelay_ms = 100;

const char VoltageMonitor_3V3_2V5_1V8_1V2::kVoltage1V2Name[] = "Voltage1V2";
const char VoltageMonitor_3V3_2V5_1V8_1V2::kVoltage1V2Description[] = "Internal 1V2 Voltage";
const char VoltageMonitor_3V3_2V5_1V8_1V2::kVoltage1V2SubPath[] = "/in_voltage0_raw";

const char VoltageMonitor_3V3_2V5_1V8_1V2::kVoltage1V8Name[] = "Voltage1V8";
const char VoltageMonitor_3V3_2V5_1V8_1V2::kVoltage1V8Description[] = "Internal 1V8 Voltage";
const char VoltageMonitor_3V3_2V5_1V8_1V2::kVoltage1V8SubPath[] = "/in_voltage1_raw";

const char VoltageMonitor_3V3_2V5_1V8_1V2::kVoltage2V5Name[] = "Voltage2V5";
const char VoltageMonitor_3V3_2V5_1V8_1V2::kVoltage2V5Description[] = "Internal 2V5 Voltage";
const char VoltageMonitor_3V3_2V5_1V8_1V2::kVoltage2V5SubPath[] = "/in_voltage2_raw";

const char VoltageMonitor_3V3_2V5_1V8_1V2::kVoltage3V3Name[] = "Voltage3V3";
const char VoltageMonitor_3V3_2V5_1V8_1V2::kVoltage3V3Description[] = "Internal 3V3 Voltage";
const char VoltageMonitor_3V3_2V5_1V8_1V2::kVoltage3V3SubPath[] = "/in_voltage3_raw";

const char VoltageMonitor_3V3_2V5_1V8_1V2::kVoltageScale1V2Name[] = "VoltageScale1V2";
const char VoltageMonitor_3V3_2V5_1V8_1V2::kVoltageScale1V2Description[] = "Scaling value for 1V2";
const char VoltageMonitor_3V3_2V5_1V8_1V2::kVoltageScale1V2SubPath[] = "/in_voltage0_scale";
const double VoltageMonitor_3V3_2V5_1V8_1V2::kVoltageScale1V2Value = 0.0625;

const char VoltageMonitor_3V3_2V5_1V8_1V2::kVoltageScale1V8Name[] = "VoltageScale1V8";
const char VoltageMonitor_3V3_2V5_1V8_1V2::kVoltageScale1V8Description[] = "Scaling value for 1V8";
const char VoltageMonitor_3V3_2V5_1V8_1V2::kVoltageScale1V8SubPath[] = "/in_voltage1_scale";
const double VoltageMonitor_3V3_2V5_1V8_1V2::kVoltageScale1V8Value = 0.0625;

const char VoltageMonitor_3V3_2V5_1V8_1V2::kVoltageScale2V5Name[] = "VoltageScale2V5";
const char VoltageMonitor_3V3_2V5_1V8_1V2::kVoltageScale2V5Description[] = "Scaling value for 2V5";
const char VoltageMonitor_3V3_2V5_1V8_1V2::kVoltageScale2V5SubPath[] = "/in_voltage2_scale";
const double VoltageMonitor_3V3_2V5_1V8_1V2::kVoltageScale2V5Value = 0.0625;

const char VoltageMonitor_3V3_2V5_1V8_1V2::kVoltageScale3V3Name[] = "VoltageScale3V3";
const char VoltageMonitor_3V3_2V5_1V8_1V2::kVoltageScale3V3Description[] = "Scaling value for 3V3";
const char VoltageMonitor_3V3_2V5_1V8_1V2::kVoltageScale3V3SubPath[] = "/in_voltage3_scale";
const double VoltageMonitor_3V3_2V5_1V8_1V2::kVoltageScale3V3Value = 0.125;

VoltageMonitor_3V3_2V5_1V8_1V2::VoltageMonitor_3V3_2V5_1V8_1V2(
  Logger::ILogger *logger,
  const Modules::Configuration::ConfigurationCommon &configCommon,
  const std::string &path)
  : I2CDeviceADS1115(
  logger,
  kName,
  kDescription,
  kAddress,
  path) {
  // Get update intervaval
  auto updateInterval = configCommon.UpdateInterval()->Medium()->Value();
  // Setup items
  voltage1V2_ = Create().ItemIn<Types::Value<int32_t>>(
    kVoltage1V2Name,
    kVoltage1V2Description,
    Types::ValueGroup::Enum::SUPPLY,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::SHARED,
    Types::Unit::Enum::VOLTAGE,
    Types::UnitPrefix::Enum::MILLI).AddAndReturnPointer();
  voltage1V2_->CreateAccessor().File<int32_t, std::string>(
    GetDevicePath().append(kVoltage1V2SubPath),
    1,
    static_cast<int32_t>(1 / kVoltageScale1V2Value));
//  voltage1V2_->CreateMonitoringFunction().IsOutside(
//      Types::AlertSeverity::Enum::WARNING,
//      Types::MonitoringFunctionId::Enum::SCU_MON_W_FUNCTION_mc_voltage_1_2,
//      std::chrono::milliseconds(kOnDelay_ms),
//      std::chrono::milliseconds(kOffDelay_ms),
//      Types::Value<int32_t>(
//          1140,
//          Types::Unit::Enum::AMPERE,
//          Types::UnitPrefix::Enum::MILLI),
//      Types::Value<int32_t>(
//          1260,
//          Types::Unit::Enum::AMPERE,
//          Types::UnitPrefix::Enum::MILLI));
//  voltage1V2_->CreateMonitoringFunction().IsOutside(
//      Types::AlertSeverity::Enum::ERROR,
//      Types::MonitoringFunctionId::Enum::SCU_MON_E_FUNCTION_mc_voltage_1_2,
//      std::chrono::milliseconds(kOnDelay_ms),
//      std::chrono::milliseconds(kOffDelay_ms),
//      Types::Value<int32_t>(
//          1080,
//          Types::Unit::Enum::AMPERE,
//          Types::UnitPrefix::Enum::MILLI),
//      Types::Value<int32_t>(
//          1260,
//          Types::Unit::Enum::AMPERE,
//          Types::UnitPrefix::Enum::MILLI));

  voltage1V8_ = Create().ItemIn<Types::Value<int32_t>>(
    kVoltage1V8Name,
    kVoltage1V8Description,
    Types::ValueGroup::Enum::SUPPLY,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::SHARED,
    Types::Unit::Enum::VOLTAGE,
    Types::UnitPrefix::Enum::MILLI).AddAndReturnPointer();
  voltage1V8_->CreateAccessor().File<int32_t, std::string>(
    GetDevicePath().append(kVoltage1V8SubPath),
    1,
    static_cast<uint32_t>(1 / kVoltageScale1V8Value));
//  voltage1V8_->CreateMonitoringFunction().IsOutside(
//      Types::AlertSeverity::Enum::WARNING,
//      Types::MonitoringFunctionId::Enum::SCU_MON_W_FUNCTION_mc_voltage_1_8,
//      std::chrono::milliseconds(kOnDelay_ms),
//      std::chrono::milliseconds(kOffDelay_ms),
//      Types::Value<int32_t>(
//          1710,
//          Types::Unit::Enum::AMPERE,
//          Types::UnitPrefix::Enum::MILLI),
//      Types::Value<int32_t>(
//          1890,
//          Types::Unit::Enum::AMPERE,
//          Types::UnitPrefix::Enum::MILLI));
//  voltage1V8_->CreateMonitoringFunction().IsOutside(
//      Types::AlertSeverity::Enum::ERROR,
//      Types::MonitoringFunctionId::Enum::SCU_MON_E_FUNCTION_mc_voltage_1_8,
//      std::chrono::milliseconds(kOnDelay_ms),
//      std::chrono::milliseconds(kOffDelay_ms),
//      Types::Value<int32_t>(
//          1620,
//          Types::Unit::Enum::AMPERE,
//          Types::UnitPrefix::Enum::MILLI),
//      Types::Value<int32_t>(
//          1980,
//          Types::Unit::Enum::AMPERE,
//          Types::UnitPrefix::Enum::MILLI));

  voltage2V5_ = Create().ItemIn<Types::Value<int32_t>>(
    kVoltage2V5Name,
    kVoltage2V5Description,
    Types::ValueGroup::Enum::SUPPLY,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::SHARED,
    Types::Unit::Enum::VOLTAGE,
    Types::UnitPrefix::Enum::MILLI).AddAndReturnPointer();
  voltage2V5_->CreateAccessor().File<int32_t, std::string>(
    GetDevicePath().append(kVoltage2V5SubPath),
    1,
    static_cast<uint32_t>(1 / kVoltageScale2V5Value));

//  voltage2V5_->CreateMonitoringFunction().IsOutside(
//      Types::AlertSeverity::Enum::WARNING,
//      Types::MonitoringFunctionId::Enum::SCU_MON_W_FUNCTION_mc_voltage_2_5,
//      std::chrono::milliseconds(kOnDelay_ms),
//      std::chrono::milliseconds(kOffDelay_ms),
//      Types::Value<int32_t>(
//          2375,
//          Types::Unit::Enum::AMPERE,
//          Types::UnitPrefix::Enum::MILLI),
//      Types::Value<int32_t>(
//          2625,
//          Types::Unit::Enum::AMPERE,
//          Types::UnitPrefix::Enum::MILLI));
//  voltage2V5_->CreateMonitoringFunction().IsOutside(
//      Types::AlertSeverity::Enum::ERROR,
//      Types::MonitoringFunctionId::Enum::SCU_MON_E_FUNCTION_mc_voltage_2_5,
//      std::chrono::milliseconds(kOnDelay_ms),
//      std::chrono::milliseconds(kOffDelay_ms),
//      Types::Value<int32_t>(
//          2250,
//          Types::Unit::Enum::AMPERE,
//          Types::UnitPrefix::Enum::MILLI),
//      Types::Value<int32_t>(
//          2750,
//          Types::Unit::Enum::AMPERE,
//          Types::UnitPrefix::Enum::MILLI));

  voltage3V3_ = Create().ItemIn<Types::Value<int32_t>>(
    kVoltage3V3Name,
    kVoltage3V3Description,
    Types::ValueGroup::Enum::SUPPLY,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::SHARED,
    Types::Unit::Enum::VOLTAGE,
    Types::UnitPrefix::Enum::MILLI).AddAndReturnPointer();
  voltage3V3_->CreateAccessor().File<int32_t, std::string>(
    GetDevicePath().append(kVoltage3V3SubPath),
    1,
    static_cast<uint32_t>(1 / kVoltageScale3V3Value));
//  voltage3V3_->CreateMonitoringFunction().IsOutside(
//      Types::AlertSeverity::Enum::WARNING,
//      Types::MonitoringFunctionId::Enum::SCU_MON_W_FUNCTION_mc_voltage_3_3,
//      std::chrono::milliseconds(kOnDelay_ms),
//      std::chrono::milliseconds(kOffDelay_ms),
//      Types::Value<int32_t>(
//          3135,
//          Types::Unit::Enum::AMPERE,
//          Types::UnitPrefix::Enum::MILLI),
//      Types::Value<int32_t>(
//          3465,
//          Types::Unit::Enum::AMPERE,
//          Types::UnitPrefix::Enum::MILLI));
//  voltage3V3_->CreateMonitoringFunction().IsOutside(
//      Types::AlertSeverity::Enum::ERROR,
//      Types::MonitoringFunctionId::Enum::SCU_MON_E_FUNCTION_mc_voltage_3_3,
//      std::chrono::milliseconds(kOnDelay_ms),
//      std::chrono::milliseconds(kOffDelay_ms),
//      Types::Value<int32_t>(
//          2970,
//          Types::Unit::Enum::AMPERE,
//          Types::UnitPrefix::Enum::MILLI),
//      Types::Value<int32_t>(
//          3630,
//          Types::Unit::Enum::AMPERE,
//          Types::UnitPrefix::Enum::MILLI));

  voltageScale1V2_ = Create().ParameterOut(
    kVoltageScale1V2Name,
    kVoltageScale1V2Description,
    Types::Affiliation::Enum::SHARED,
    true,
    kVoltageScale1V2Value).AddAndReturnPointer();
  voltageScale1V2_->CreateAccessor().File<double, std::string>(
    GetDevicePath().append(kVoltageScale1V2SubPath),
    1,
    1);

  voltageScale1V8_ = Create().ParameterOut(
    kVoltageScale1V8Name,
    kVoltageScale1V8Description,
    Types::Affiliation::Enum::SHARED,
    true,
    kVoltageScale1V8Value).AddAndReturnPointer();
  voltageScale1V8_->CreateAccessor().File<double, std::string>(
    GetDevicePath().append(kVoltageScale1V8SubPath),
    1,
    1);

  voltageScale2V5_ = Create().ParameterOut(
    kVoltageScale2V5Name,
    kVoltageScale2V5Description,
    Types::Affiliation::Enum::SHARED,
    true,
    kVoltageScale2V5Value).AddAndReturnPointer();
  voltageScale2V5_->CreateAccessor().File<double, std::string>(
    GetDevicePath().append(kVoltageScale2V5SubPath),
    1,
    1);

  voltageScale3V3_ = Create().ParameterOut(
    kVoltageScale3V3Name,
    kVoltageScale3V3Description,
    Types::Affiliation::Enum::SHARED,
    true,
    kVoltageScale3V3Value).AddAndReturnPointer();
  voltageScale3V3_->CreateAccessor().File<double, std::string>(
    GetDevicePath().append(kVoltageScale3V3SubPath),
    1,
    1);
}

Framework::Components::ItemIn<Types::Value<int32_t>> *VoltageMonitor_3V3_2V5_1V8_1V2::Voltage1V2() const {
  return voltage1V2_;
}

Framework::Components::ItemIn<Types::Value<int32_t>> *VoltageMonitor_3V3_2V5_1V8_1V2::Voltage1V8() const {
  return voltage1V8_;
}

Framework::Components::ItemIn<Types::Value<int32_t>> *VoltageMonitor_3V3_2V5_1V8_1V2::Voltage2V5() const {
  return voltage2V5_;
}

Framework::Components::ItemIn<Types::Value<int32_t>> *VoltageMonitor_3V3_2V5_1V8_1V2::Voltage3V3() const {
  return voltage3V3_;
}

Framework::Components::ParameterItemOut<double> *VoltageMonitor_3V3_2V5_1V8_1V2::VoltageScale1V2() const {
  return voltageScale1V2_;
}

Framework::Components::ParameterItemOut<double> *VoltageMonitor_3V3_2V5_1V8_1V2::VoltageScale1V8() const {
  return voltageScale1V8_;
}

Framework::Components::ParameterItemOut<double> *VoltageMonitor_3V3_2V5_1V8_1V2::VoltageScale2V5() const {
  return voltageScale2V5_;
}

Framework::Components::ParameterItemOut<double> *VoltageMonitor_3V3_2V5_1V8_1V2::VoltageScale3V3() const {
  return voltageScale3V3_;
}

}  // namespace SystemModules::Modules::Common
