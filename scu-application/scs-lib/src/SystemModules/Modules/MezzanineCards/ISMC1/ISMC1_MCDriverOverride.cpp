/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Builder/Actions/ActionBuilder.h"
#include "SystemModules/Framework/Builder/Accessors/WriteAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Accessors/ReadAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemInBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemOutBuilder.h"
#include "SystemModules/Framework/Builder/Components/ComponentBuilder.h"
#include "ISMC1_MCDriverOverride.h"

namespace SystemModules::Modules::ISMC1 {

const char ISMC1_MCDriverOverride::kName[] = "ISMC1Override";
const char ISMC1_MCDriverOverride::kDescription[] =
  "Allows to override individual input and output signals by means of an error-flag.";

const char ISMC1_MCDriverOverride::kInputSignalCollectionName[] = "InputSignalCollection";
const char ISMC1_MCDriverOverride::kInputSignalCollectionDescription[] =
  "Collection of ISMC1 input signal override items.";
const uint32_t ISMC1_MCDriverOverride::kInputSignalCollectionPosition = 0;

const char ISMC1_MCDriverOverride::kOutputSignalCollectionName[] = "OutputSignalCollection";
const char ISMC1_MCDriverOverride::kOutputSignalCollectionDescription[] =
  "Collection of ISMC1 output signal override items.";
const uint32_t ISMC1_MCDriverOverride::kOutputSignalCollectionPosition = 0;

const char ISMC1_MCDriverOverride::kMagnetronPSFeedbackName[] = "MagnetronPSFeedback";
const char ISMC1_MCDriverOverride::kMagnetronPSFeedbackDescription[] =
  "Override Magnetron PS Feedback signal. False==Do not override; True==Override with 'err-flag'.";
const uint32_t ISMC1_MCDriverOverride::kMagnetronPSFeedbackPosition = 0;

const char ISMC1_MCDriverOverride::kRFFeedbackName[] = "RFFeedback";
const char ISMC1_MCDriverOverride::kRFFeedbackDescription[] =
  "Override RF Feedback signal. False==Do not override; True==Override with 'err-flag'.";
const uint32_t ISMC1_MCDriverOverride::kRFFeedbackPosition = 1;

const char ISMC1_MCDriverOverride::kMagnetronPSOutputName[] = "MagnetronPSOutput";
const char ISMC1_MCDriverOverride::kMagnetronPSOutputDescription[] =
  "Override Magnetron PS Output signal. False==Do not override; True==Override with 'err-flag'.";
const uint32_t ISMC1_MCDriverOverride::kMagnetronPSOutputPosition = 0;

const char ISMC1_MCDriverOverride::kWatchdogName[] = "Watchdog";
const char ISMC1_MCDriverOverride::kWatchdogDescription[] =
  "Override Watchdog signal. False==Do not override; True==Override with 'err-flag'.";
const uint32_t ISMC1_MCDriverOverride::kWatchdogPosition = 1;

const char ISMC1_MCDriverOverride::kFSUPowerOnName[] = "FSUPowerOn";
const char ISMC1_MCDriverOverride::kFSUPowerOnDescription[] =
  "Override Magnetron PS Feedback signal. False==Do not override; True==Override with 'err-flag'.";
const uint32_t ISMC1_MCDriverOverride::kFSUPowerOnPosition = 2;

ISMC1_MCDriverOverride::ISMC1_MCDriverOverride(
  Logger::ILogger *logger,
  const Modules::Configuration::ConfigurationCommon &configCommon,
  const Configuration::Configuration_Slots &config)
  : Framework::Components::Container(
  logger,
  kName,
  kDescription) {
  // Get update intervaval
  auto updateInterval = configCommon.UpdateInterval()->Fast()->Value();
  // Setup Input Signal item collection
  inputSignalCollection_ = Create().ItemIn<uint32_t>(
    kInputSignalCollectionName,
    kInputSignalCollectionDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  inputSignalCollection_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ReadWriteRegister1Address()->Value(),
    kInputSignalCollectionPosition,
    32);
  // Setup Output Signal item collection
  outputSignalCollection_ = Create().ItemIn<uint32_t>(
    kOutputSignalCollectionName,
    kOutputSignalCollectionDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  outputSignalCollection_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ReadWriteRegister2Address()->Value(),
    kOutputSignalCollectionPosition,
    32);
  // Setup Magnetron PS Feedback override input
  magnetronPSFeedback_ = Create().ItemOut<bool>(
    kMagnetronPSFeedbackName,
    kMagnetronPSFeedbackDescription,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    true,
    false).AddAndReturnPointer();
  magnetronPSFeedback_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ReadWriteRegister1Address()->Value(),
    kMagnetronPSFeedbackPosition,
    1);
  // Setup RF Feedback override input
  rfFeedback_ = Create().ItemOut<bool>(
    kRFFeedbackName,
    kRFFeedbackDescription,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    true,
    false).AddAndReturnPointer();
  rfFeedback_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ReadWriteRegister1Address()->Value(),
    kRFFeedbackPosition,
    1);
  // Setup Magnetron PS Output override input
  magnetronPSOutput_ = Create().ItemOut<bool>(
    kMagnetronPSOutputName,
    kMagnetronPSOutputDescription,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    true,
    false).AddAndReturnPointer();
  magnetronPSOutput_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ReadWriteRegister2Address()->Value(),
    kMagnetronPSOutputPosition,
    1);
  // Setup Watchdog override input
  watchdog_ = Create().ItemOut<bool>(
    kWatchdogName,
    kWatchdogDescription,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    true,
    false).AddAndReturnPointer();
  watchdog_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ReadWriteRegister2Address()->Value(),
    kWatchdogPosition,
    1);
  // Setup Magnetron PS Feedback override input
  fsuPowerOn_ = Create().ItemOut<bool>(
    kFSUPowerOnName,
    kFSUPowerOnDescription,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    true,
    false).AddAndReturnPointer();
  fsuPowerOn_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->ReadWriteRegister2Address()->Value(),
    kFSUPowerOnPosition,
    1);

  (void) config;
}

Framework::Components::ItemOut<bool> *ISMC1_MCDriverOverride::MagnetronPSFeedback() const {
  return magnetronPSFeedback_;
}

Framework::Components::ItemOut<bool> *ISMC1_MCDriverOverride::RFFeedback() const {
  return rfFeedback_;
}

Framework::Components::ItemOut<bool> *ISMC1_MCDriverOverride::MagnetronPSOutput() const {
  return magnetronPSOutput_;
}

Framework::Components::ItemOut<bool> *ISMC1_MCDriverOverride::Watchdog() const {
  return watchdog_;
}

Framework::Components::ItemOut<bool> *ISMC1_MCDriverOverride::FSUPowerOn() const {
  return fsuPowerOn_;
}

}  // namespace SystemModules::Modules::ISMC1
