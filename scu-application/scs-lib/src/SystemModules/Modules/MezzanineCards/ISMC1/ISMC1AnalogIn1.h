/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Types/Value.h"
#include "SystemModules/Framework/Components/ItemIn.h"
#include "SystemModules/Framework/Components/I2CDeviceADS1115.h"

namespace SystemModules::Modules::ISMC1 {
class ISMC1AnalogIn1 : public Framework::Components::I2CDeviceADS1115 {
 public:
  ISMC1AnalogIn1(
    Logger::ILogger *logger,
    const std::string &path,
    const std::string &devicePath,
    const uint32_t &slot);

  [[nodiscard]] Framework::Components::ItemIn<Types::Value<int32_t>> *TriggerInputRSSI() const;

  [[nodiscard]] Framework::Components::ItemIn<Types::Value<int32_t>> *MagnetronPSFeedbackRSSI() const;

  [[nodiscard]] Framework::Components::ItemIn<Types::Value<int32_t>> *RFFeedbackRSSI() const;

  [[nodiscard]] Framework::Components::ItemIn<Types::Value<int32_t>> *SupplyVoltageRSSI() const;

 private:
  Framework::Components::ItemIn<Types::Value<int32_t>> *triggerinputrssi_;

  Framework::Components::ItemIn<Types::Value<int32_t>> *magnetronpsfeedbackrssi_;

  Framework::Components::ItemIn<Types::Value<int32_t>> *rffeedbackrssi_;

  Framework::Components::ItemIn<Types::Value<int32_t>> *supplyvoltagerssi_;

  static const char kName[];
  static const char kDescription[];
  static const uint16_t kAddress;

  static const char kTriggerInputRSSIName[];
  static const char kTriggerInputRSSIDescription[];
  static const char kTriggerInputRSSISubPath[];

  static const char kMagnetronPSFeedbackRSSIName[];
  static const char kMagnetronPSFeedbackRSSIDescription[];
  static const char kMagnetronPSFeedbackRSSISubPath[];

  static const char kRFFeedbackRSSIName[];
  static const char kRFFeedbackRSSIDescription[];
  static const char kRFFeedbackRSSISubPath[];

  static const char kSupplyVoltageRSSIName[];
  static const char kSupplyVoltageRSSIDescription[];
  static const char kSupplyVoltageRSSISubPath[];
  static const uint32_t kSupplyVoltageRSSIUpperLimit;
  static const uint32_t kSupplyVoltageRSSILowerLimit;
};

}  // namespace SystemModules::Modules::ISMC1
