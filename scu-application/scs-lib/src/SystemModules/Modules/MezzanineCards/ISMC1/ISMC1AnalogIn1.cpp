/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Actions/ActionTemplate.h"
#include "SystemModules/Framework/MonitoringFunctions/MFTemplate.h"
#include "SystemModules/Framework/Accessors/AccessorBase.h"
#include "SystemModules/Framework/Builder/Accessors/ReadAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemInBuilder.h"
#include "SystemModules/Framework/Builder/Components/ComponentBuilder.h"
#include "ISMC1AnalogIn1.h"

namespace SystemModules::Modules::ISMC1 {

const char ISMC1AnalogIn1::kName[] = "ReceiverStrengthIndication";
const char ISMC1AnalogIn1::kDescription[] = "Optical receiver strength indication";
const uint16_t ISMC1AnalogIn1::kAddress = 0x49;

const char ISMC1AnalogIn1::kTriggerInputRSSIName[] = "TriggerInputRSSI";
const char ISMC1AnalogIn1::kTriggerInputRSSIDescription[] = "Trigger Input RSSI";
const char ISMC1AnalogIn1::kTriggerInputRSSISubPath[] = "/in_voltage0_raw";

const char ISMC1AnalogIn1::kMagnetronPSFeedbackRSSIName[] = "MagnetronPSFeedbackRSSI";
const char ISMC1AnalogIn1::kMagnetronPSFeedbackRSSIDescription[] = "Magnetron PS Feedback RSSI";
const char ISMC1AnalogIn1::kMagnetronPSFeedbackRSSISubPath[] = "/in_voltage1_raw";

const char ISMC1AnalogIn1::kRFFeedbackRSSIName[] = "RFFeedbackRSSI";
const char ISMC1AnalogIn1::kRFFeedbackRSSIDescription[] = "RF Feedback RSSI";
const char ISMC1AnalogIn1::kRFFeedbackRSSISubPath[] = "/in_voltage2_raw";

const char ISMC1AnalogIn1::kSupplyVoltageRSSIName[] = "SupplyVoltageRSSI";
const char ISMC1AnalogIn1::kSupplyVoltageRSSIDescription[] = "Supply Voltage RSSI";
const char ISMC1AnalogIn1::kSupplyVoltageRSSISubPath[] = "/in_voltage3_raw";
const uint32_t ISMC1AnalogIn1::kSupplyVoltageRSSIUpperLimit = 36000;
const uint32_t ISMC1AnalogIn1::kSupplyVoltageRSSILowerLimit = 30000;

ISMC1AnalogIn1::ISMC1AnalogIn1(
  Logger::ILogger *logger,
  const std::string &path,
  const std::string &devicePath,
  const uint32_t &slot)
  : Framework::Components::I2CDeviceADS1115(
  logger,
  kName,
  kDescription,
  kAddress,
  path) {
  (void) devicePath;
  (void) slot;
  triggerinputrssi_ = Create().ItemIn<Types::Value<int32_t>>(
    kTriggerInputRSSIName,
    kTriggerInputRSSIDescription,
    Types::ValueGroup::Enum::SUPPLY,
    std::chrono::milliseconds(1000),
    Types::Affiliation::Enum::SHARED,
    Types::Unit::Enum::VOLTAGE,
    Types::UnitPrefix::Enum::MILLI).AddAndReturnPointer();
  triggerinputrssi_->CreateAccessor().File<int32_t, std::string>(
    GetDevicePath().append(kTriggerInputRSSISubPath),
    1,
    16);
//  triggerinputrssi_->CreateMonitoringFunction().IsHigherThan(
//      Types::AlertSeverity::Enum::ERROR,
//      Types::MonitoringFunctionId::Enum::NONE,
//      std::chrono::milliseconds(1000),
//      std::chrono::milliseconds(1000),
//      Types::Value<int32_t> (
//          1000,
//          Types::Unit::Enum::VOLTAGE,
//          Types::UnitPrefix::Enum::MILLI)).Add();
//  triggerinputrssi_->CreateMonitoringFunction().IsInside(
//      Types::AlertSeverity::Enum::ERROR,
//      Types::MonitoringFunctionId::Enum::NONE,
//      std::chrono::milliseconds(1000),
//      std::chrono::milliseconds(1000),
//      Types::Value<int32_t> (
//          0,
//          Types::Unit::Enum::VOLTAGE,
//          Types::UnitPrefix::Enum::MILLI),
//      Types::Value<int32_t> (
//          0,
//          Types::Unit::Enum::VOLTAGE,
//          Types::UnitPrefix::Enum::MILLI)).Add();
//  triggerinputrssi_->CreateMonitoringFunction().IsLowerThan(
//      Types::AlertSeverity::Enum::ERROR,
//      Types::MonitoringFunctionId::Enum::NONE,
//      std::chrono::milliseconds(1000),
//      std::chrono::milliseconds(1000),
//      Types::Value<int32_t> (
//          0,
//          Types::Unit::Enum::VOLTAGE,
//          Types::UnitPrefix::Enum::MILLI)).Add();
  magnetronpsfeedbackrssi_ = Create().ItemIn<Types::Value<int32_t>>(
    kMagnetronPSFeedbackRSSIName,
    kMagnetronPSFeedbackRSSIDescription,
    Types::ValueGroup::Enum::SUPPLY,
    std::chrono::milliseconds(1000),
    Types::Affiliation::Enum::SHARED,
    Types::Unit::Enum::VOLTAGE,
    Types::UnitPrefix::Enum::MILLI).AddAndReturnPointer();
  magnetronpsfeedbackrssi_->CreateAccessor().File<int32_t, std::string>(
    GetDevicePath().append(kMagnetronPSFeedbackRSSISubPath),
    1,
    16);
//  magnetronpsfeedbackrssi_->CreateMonitoringFunction().IsHigherThan(
//      Types::AlertSeverity::Enum::ERROR,
//      Types::MonitoringFunctionId::Enum::NONE,
//      std::chrono::milliseconds(1000),
//      std::chrono::milliseconds(1000),
//      Types::Value<int32_t> (
//          1000,
//          Types::Unit::Enum::VOLTAGE,
//          Types::UnitPrefix::Enum::MILLI)).Add();
//  magnetronpsfeedbackrssi_->CreateMonitoringFunction().IsInside(
//      Types::AlertSeverity::Enum::ERROR,
//      Types::MonitoringFunctionId::Enum::NONE,
//      std::chrono::milliseconds(1000),
//      std::chrono::milliseconds(1000),
//      Types::Value<int32_t> (
//          0,
//          Types::Unit::Enum::VOLTAGE,
//          Types::UnitPrefix::Enum::MILLI),
//      Types::Value<int32_t> (
//          0,
//          Types::Unit::Enum::VOLTAGE,
//          Types::UnitPrefix::Enum::MILLI)).Add();
//  magnetronpsfeedbackrssi_->CreateMonitoringFunction().IsLowerThan(
//      Types::AlertSeverity::Enum::ERROR,
//      Types::MonitoringFunctionId::Enum::NONE,
//      std::chrono::milliseconds(1000),
//      std::chrono::milliseconds(1000),
//      Types::Value<int32_t> (
//          0,
//          Types::Unit::Enum::VOLTAGE,
//          Types::UnitPrefix::Enum::MILLI)).Add();
  rffeedbackrssi_ = Create().ItemIn<Types::Value<int32_t>>(
    kRFFeedbackRSSIName,
    kRFFeedbackRSSIDescription,
    Types::ValueGroup::Enum::SUPPLY,
    std::chrono::milliseconds(1000),
    Types::Affiliation::Enum::SHARED,
    Types::Unit::Enum::VOLTAGE,
    Types::UnitPrefix::Enum::MILLI).AddAndReturnPointer();
  rffeedbackrssi_->CreateAccessor().File<int32_t, std::string>(
    GetDevicePath().append(kRFFeedbackRSSISubPath),
    1,
    16);
//  rffeedbackrssi_->CreateMonitoringFunction().IsHigherThan(
//      Types::AlertSeverity::Enum::ERROR,
//      Types::MonitoringFunctionId::Enum::NONE,
//      std::chrono::milliseconds(1000),
//      std::chrono::milliseconds(1000),
//      Types::Value<int32_t> (
//          1000,
//          Types::Unit::Enum::VOLTAGE,
//          Types::UnitPrefix::Enum::MILLI)).Add();
//  rffeedbackrssi_->CreateMonitoringFunction().IsInside(
//      Types::AlertSeverity::Enum::ERROR,
//      Types::MonitoringFunctionId::Enum::NONE,
//      std::chrono::milliseconds(1000),
//      std::chrono::milliseconds(1000),
//      Types::Value<int32_t> (
//          0,
//          Types::Unit::Enum::VOLTAGE,
//          Types::UnitPrefix::Enum::MILLI),
//      Types::Value<int32_t> (
//          0,
//          Types::Unit::Enum::VOLTAGE,
//          Types::UnitPrefix::Enum::MILLI)).Add();
//  rffeedbackrssi_->CreateMonitoringFunction().IsLowerThan(
//      Types::AlertSeverity::Enum::ERROR,
//      Types::MonitoringFunctionId::Enum::NONE,
//      std::chrono::milliseconds(1000),
//      std::chrono::milliseconds(1000),
//      Types::Value<int32_t> (
//          0,
//          Types::Unit::Enum::VOLTAGE,
//          Types::UnitPrefix::Enum::MILLI)).Add();
  supplyvoltagerssi_ = Create().ItemIn<Types::Value<int32_t>>(
    kSupplyVoltageRSSIName,
    kSupplyVoltageRSSIDescription,
    Types::ValueGroup::Enum::SUPPLY,
    std::chrono::milliseconds(1000),
    Types::Affiliation::Enum::SHARED,
    Types::Unit::Enum::VOLTAGE,
    Types::UnitPrefix::Enum::MILLI).AddAndReturnPointer();
  supplyvoltagerssi_->CreateAccessor().File<int32_t, std::string>(
    GetDevicePath().append(kSupplyVoltageRSSISubPath),
    1,
    8);
//  supplyvoltagerssi_->CreateMonitoringFunction().IsHigherThan(
//      Types::AlertSeverity::Enum::ERROR,
//      Types::MonitoringFunctionId::Enum::NONE,
//      std::chrono::milliseconds(1000),
//      std::chrono::milliseconds(1000),
//      Types::Value<int32_t> (
//          kSupplyVoltageRSSIUpperLimit,
//          Types::Unit::Enum::VOLTAGE,
//          Types::UnitPrefix::Enum::MILLI)).Add();
//  supplyvoltagerssi_->CreateMonitoringFunction().IsLowerThan(
//      Types::AlertSeverity::Enum::ERROR,
//      Types::MonitoringFunctionId::Enum::NONE,
//      std::chrono::milliseconds(1000),
//      std::chrono::milliseconds(1000),
//      Types::Value<int32_t> (
//          kSupplyVoltageRSSILowerLimit,
//          Types::Unit::Enum::VOLTAGE,
//          Types::UnitPrefix::Enum::MILLI)).Add();
  (void) path;
}

Framework::Components::ItemIn<Types::Value<int32_t>> *ISMC1AnalogIn1::TriggerInputRSSI() const {
  return triggerinputrssi_;
}

Framework::Components::ItemIn<Types::Value<int32_t>> *ISMC1AnalogIn1::MagnetronPSFeedbackRSSI() const {
  return magnetronpsfeedbackrssi_;
}

Framework::Components::ItemIn<Types::Value<int32_t>> *ISMC1AnalogIn1::RFFeedbackRSSI() const {
  return rffeedbackrssi_;
}

Framework::Components::ItemIn<Types::Value<int32_t>> *ISMC1AnalogIn1::SupplyVoltageRSSI() const {
  return supplyvoltagerssi_;
}

}  // namespace SystemModules::Modules::ISMC1
