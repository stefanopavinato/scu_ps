/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Logger/ILogger.h"
#include "SystemModules/Framework/Components/ItemIn.h"
#include "SystemModules/Framework/Builder/Accessors/ReadAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemInBuilder.h"
#include "SystemModules/Framework/Builder/Components/ComponentBuilder.h"
#include "ISMC1_MCDriverStatus.h"

namespace SystemModules::Modules::ISMC1 {

const char ISMC1_MCDriverStatus::kName[] = "Status";
const char ISMC1_MCDriverStatus::kDescription[] =
  "Status of signals the Serializer receives/generates from/towards the mezzanine card.";

const char ISMC1_MCDriverStatus::kCollection1Name[] = "Collection1";
const char ISMC1_MCDriverStatus::kCollection1Description[] =
  "Collection of ISMC1 status items related to signals received from the MC.";
const uint32_t ISMC1_MCDriverStatus::kCollection1Position = 0;

const char ISMC1_MCDriverStatus::kCollection2Name[] = "Collection2";
const char ISMC1_MCDriverStatus::kCollection2Description[] =
  "Collection of ISMC1 status items related to signal driven towards the MC.";
const uint32_t ISMC1_MCDriverStatus::kCollection2Position = 0;

const char ISMC1_MCDriverStatus::kWatchdogAExpiredName[] = "WDAExpired";
const char ISMC1_MCDriverStatus::kWatchdogAExpiredDescription[] =
  "Watchdog A expired. False==not expired; True==expired.";
const uint32_t ISMC1_MCDriverStatus::kWatchdogAExpiredPosition = 0;

const char ISMC1_MCDriverStatus::kWatchdogBExpiredName[] = "WDBExpired";
const char ISMC1_MCDriverStatus::kWatchdogBExpiredDescription[] =
  "Watchdog B expired. False==not expired; True==expired.";
const uint32_t ISMC1_MCDriverStatus::kWatchdogBExpiredPosition = 1;

const char ISMC1_MCDriverStatus::kTriggerInputName[] = "TriggerInput";
const char ISMC1_MCDriverStatus::kTriggerInputDescription[] =
  "Status of the Trigger Input signal. False==proton beam off; True==proton beam on.";
const uint32_t ISMC1_MCDriverStatus::kTriggerInputPosition = 2;

const char ISMC1_MCDriverStatus::kTriggerOutputName[] = "TriggerOutput";
const char ISMC1_MCDriverStatus::kTriggerOutputDescription[] =
  "Status of the Trigger Output signal (toward Magnetron PS). False==proton beam off; True==proton beam on.";
const uint32_t ISMC1_MCDriverStatus::kTriggerOutputPosition = 3;

const char ISMC1_MCDriverStatus::kFSUPowerOnReadbackName[] = "FSUPowerOnReadback";
const char ISMC1_MCDriverStatus::kFSUPowerOnReadbackDescription[] =
  "Status of the FSU Power On readback as received from the MC. False==power Off; True==power on.";
const uint32_t ISMC1_MCDriverStatus::kFSUPowerOnReadbackPosition = 4;

const char ISMC1_MCDriverStatus::kMagnetronPSFeedbackName[] = "MagnetronPSFeedback";
const char ISMC1_MCDriverStatus::kMagnetronPSFeedbackDescription[] =
  "Status of the Magnetron PS Feedback signal (raw value).";
const uint32_t ISMC1_MCDriverStatus::kMagnetronPSFeedbackPosition = 6;

const char ISMC1_MCDriverStatus::kRFFeedbackName[] = "RFFeedback";
const char ISMC1_MCDriverStatus::kRFFeedbackDescription[] =
  "Status of the RF Feedback signal (raw value).";
const uint32_t ISMC1_MCDriverStatus::kRFFeedbackPosition = 7;

const char ISMC1_MCDriverStatus::kMagnetronPSEnableName[] = "MagnetronPSEnable";
const char ISMC1_MCDriverStatus::kMagnetronPSEnableDescription[] =
  "Status of Magnetron PS Enable output (raw value).";
const uint32_t ISMC1_MCDriverStatus::kMagnetronPSEnablePosition = 0;

const char ISMC1_MCDriverStatus::kWDKeepAliveName[] = "WDKeepAlive";
const char ISMC1_MCDriverStatus::kWDKeepAliveDescription[] =
  "Status of WD Keep Alive output (raw value).";
const uint32_t ISMC1_MCDriverStatus::kWDKeepAlivePosition = 1;

const char ISMC1_MCDriverStatus::kFSUOutputName[] = "FSUOutput";
const char ISMC1_MCDriverStatus::kFSUOutputDescription[] =
  "Status of FSU Output (raw value).";
const uint32_t ISMC1_MCDriverStatus::kFSUOutputPosition = 2;

ISMC1_MCDriverStatus::ISMC1_MCDriverStatus(
  Logger::ILogger *logger,
  const Modules::Configuration::ConfigurationCommon &configCommon,
  const Configuration::Configuration_Slots &config)
  : Framework::Components::Container(
  logger,
  kName,
  kDescription) {
  // Get update intervaval
  auto updateIntervalDiscreteSignals = configCommon.UpdateInterval()->Medium()->Value();
  // Setup item collection
  collection1_ = Create().ItemIn<uint32_t>(
    kCollection1Name,
    kCollection1Description,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDiscreteSignals),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  collection1_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister1Address()->Value(),
    kCollection1Position,
    32);
  collection2_ = Create().ItemIn<uint32_t>(
    kCollection2Name,
    kCollection2Description,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDiscreteSignals),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  collection2_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister2Address()->Value(),
    kCollection2Position,
    32);
  // Setup watchdog A expired status input
  watchdogAExpired_ = Create().ItemIn<bool>(
    kWatchdogAExpiredName,
    kWatchdogAExpiredDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDiscreteSignals),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  watchdogAExpired_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister1Address()->Value(),
    kWatchdogAExpiredPosition,
    1);
  // Setup watchdog B expired status input
  watchdogBExpired_ = Create().ItemIn<bool>(
    kWatchdogBExpiredName,
    kWatchdogBExpiredDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDiscreteSignals),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  watchdogBExpired_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister1Address()->Value(),
    kWatchdogBExpiredPosition,
    1);
  // Setup trigger input signal status input
  triggerinput_ = Create().ItemIn<bool>(
    kTriggerInputName,
    kTriggerInputDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDiscreteSignals),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  triggerinput_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister1Address()->Value(),
    kTriggerInputPosition,
    1);
  // Setup trigger output signal status input
  triggeroutput_ = Create().ItemIn<bool>(
    kTriggerOutputName,
    kTriggerOutputDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDiscreteSignals),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  triggeroutput_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister1Address()->Value(),
    kTriggerOutputPosition,
    1);
  // Setup FSU Power On Readback input
  fsupoweron_ = Create().ItemIn<bool>(
    kFSUPowerOnReadbackName,
    kFSUPowerOnReadbackDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDiscreteSignals),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  fsupoweron_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister1Address()->Value(),
    kFSUPowerOnReadbackPosition,
    1);
// ADC ready status is not used and therefore currently not supported by this software

  // Setup Magnetron PS Feedback signal status
  magnetronpsfeedback_ = Create().ItemIn<bool>(
    kMagnetronPSFeedbackName,
    kMagnetronPSFeedbackDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDiscreteSignals),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  magnetronpsfeedback_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister1Address()->Value(),
    kMagnetronPSFeedbackPosition,
    1);
  // Setup RF Feedback signal status
  rffeedback_ = Create().ItemIn<bool>(
    kRFFeedbackName,
    kRFFeedbackDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDiscreteSignals),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  rffeedback_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister1Address()->Value(),
    kRFFeedbackPosition,
    1);
  // Setup Magnetron PS Enable output
  magnetronpsenable_ = Create().ItemIn<bool>(
    kMagnetronPSEnableName,
    kMagnetronPSEnableDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDiscreteSignals),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  magnetronpsenable_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister2Address()->Value(),
    kMagnetronPSEnablePosition,
    1);
  // Setup WD Keep Alive output
  wdkeepalive_ = Create().ItemIn<bool>(
    kWDKeepAliveName,
    kWDKeepAliveDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDiscreteSignals),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  wdkeepalive_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister2Address()->Value(),
    kWDKeepAlivePosition,
    1);
  // Setup FSU Output output
  fsuoutputsignal_ = Create().ItemIn<bool>(
    kFSUOutputName,
    kFSUOutputDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateIntervalDiscreteSignals),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  fsuoutputsignal_->CreateAccessor().ScuRegister(
    config.Scu()->Address()->StatusRegister2Address()->Value(),
    kFSUOutputPosition,
    1);
}

Framework::Components::ItemIn<uint32_t> *ISMC1_MCDriverStatus::Collection1() const {
  return collection1_;
}

Framework::Components::ItemIn<uint32_t> *ISMC1_MCDriverStatus::Collection2() const {
  return collection2_;
}

Framework::Components::ItemIn<bool> *ISMC1_MCDriverStatus::WatchdogAExpired() const {
  return watchdogAExpired_;
}

Framework::Components::ItemIn<bool> *ISMC1_MCDriverStatus::WatchdogBExpired() const {
  return watchdogBExpired_;
}

Framework::Components::ItemIn<bool> *ISMC1_MCDriverStatus::TriggerInput() const {
  return triggerinput_;
}

Framework::Components::ItemIn<bool> *ISMC1_MCDriverStatus::TriggerOutput() const {
  return triggeroutput_;
}

Framework::Components::ItemIn<bool> *ISMC1_MCDriverStatus::FSUPowerOn() const {
  return fsupoweron_;
}

Framework::Components::ItemIn<bool> *ISMC1_MCDriverStatus::MagnetronPSFeedback() const {
  return magnetronpsfeedback_;
}

Framework::Components::ItemIn<bool> *ISMC1_MCDriverStatus::RFFeedback() const {
  return rffeedback_;
}

Framework::Components::ItemIn<bool> *ISMC1_MCDriverStatus::MagnetronPSEnable() const {
  return magnetronpsenable_;
}

Framework::Components::ItemIn<bool> *ISMC1_MCDriverStatus::WDKeepAlive() const {
  return wdkeepalive_;
}

Framework::Components::ItemIn<bool> *ISMC1_MCDriverStatus::FSUOutputSignal() const {
  return fsuoutputsignal_;
}

}  // namespace SystemModules::Modules::ISMC1
