/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Logger/ILogger.h"
#include "SystemModules/Framework/Components/ItemOut.h"
#include "SystemModules/Framework/Builder/Components/ComponentBuilder.h"
#include "ISMC1_MCDriverOverride.h"
#include "ISMC1_MCDriverStatus.h"
#include "ISMC1_MCDriver.h"

namespace SystemModules::Modules::ISMC1 {

ISMC1_MCDriver::ISMC1_MCDriver(
  Logger::ILogger *logger,
  const Modules::Configuration::ConfigurationCommon &configCommon,
  const Configuration::Configuration_Slots &config)
  : Framework::Components::Container(
  logger,
  "MCDriver",
  "ISMC1 MC_Driver") {
  // Set override container
  auto override = std::make_unique<ISMC1_MCDriverOverride>(
    logger,
    configCommon,
    config);
  override_ = override.get();
  AddComponent(std::move(override));
  // Set status container
  auto status = std::make_unique<ISMC1_MCDriverStatus>(
    logger,
    configCommon,
    config);
  status_ = status.get();
  AddComponent(std::move(status));

  (void) config;
}

ISMC1_MCDriverStatus *ISMC1_MCDriver::Status() const {
  return status_;
}

ISMC1_MCDriverOverride *ISMC1_MCDriver::Override() const {
  return override_;
}

}  // namespace SystemModules::Modules::ISMC1
