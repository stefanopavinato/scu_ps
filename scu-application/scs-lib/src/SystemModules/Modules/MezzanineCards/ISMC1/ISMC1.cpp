/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Builder/Actions/ActionBuilderGeneric.h"
#include "SystemModules/Framework/Components/ItemOut.h"
#include "SystemModules/Modules/MezzanineCards/Common/VoltageMonitor_3V3_1V8.h"
#include "SystemModules/Modules/MezzanineCards/Common/HotSwapController.h"
#include "ISMC1AnalogIn1.h"
#include "ISMC1_MCDriverOverride.h"
#include "ISMC1_MCDriverStatus.h"
#include "ISMC1_MCDriver.h"
#include "ISMC1.h"

namespace SystemModules::Modules::ISMC1 {

ISMC1::ISMC1(
  Logger::ILogger *logger,
  const Modules::Configuration::Modules::Configuration_ISMC1 &configISMC1,
  const Modules::Configuration::ConfigurationCommon &configCommon,
  const Modules::Configuration::Configuration_Slots &config)
  : Container(
  logger,
  "Application",
  "ISMC1 specific application") {
  // Create trigger input RSSI
  auto analogIn1 = std::make_unique<ISMC1AnalogIn1>(
    logger,
    config.PathI2CApplication()->GetValue(),
    config.PathI2CApplicationDevice()->GetValue(),
    config.Slot()->GetValue());
  analogIn1_ = analogIn1.get();
  AddComponent(std::move(analogIn1));
  // Crate ismc1 MC_Driver
  auto mcdriver = std::make_unique<ISMC1_MCDriver>(
    logger,
    configCommon,
    config);
  mcdriver_ = mcdriver.get();
  AddComponent(std::move(mcdriver));
  // Create voltage monitor
  auto voltageMonitor = std::make_unique<Common::VoltageMonitor_3V3_1V8>(
    logger,
    configCommon,
    config.PathI2CManagement()->GetValue());
  voltageMonitor_ = voltageMonitor.get();
  AddComponent(std::move(voltageMonitor));
  // Create hot swap controller
  auto hotSwapController = std::make_unique<Modules::Common::HotSwapController>(
    logger,
    *configISMC1.HotSwapController(),
    configCommon,
    config.PathI2CManagement()->GetValue(),
    0x4b);
  hotSwapController_ = hotSwapController.get();
  AddComponent(std::move(hotSwapController));
  // ToDo: Note: these actions dont use the right inputs!!!!!
//  {
//    auto action = MCDriver()->Override()->MagnetronPSOutput()->CreateAction().Generic().BooleanEquation(
//        "OUT && (!IN || !ENABLE || WDA || WDB)",
//        Types::AlertSeverity::Enum::WARNING);
//    action->AddVariable("OUT", MCDriver()->Status()->TriggerOutput());
//    action->AddVariable("IN", MCDriver()->Status()->TriggerInput());
//    action->AddVariable("ENABLE", MCDriver()->Status()->MagnetronPSEnable());
//    action->AddVariable("WDA", MCDriver()->Status()->WatchdogAExpired());
//    action->AddVariable("WDB", MCDriver()->Status()->WatchdogBExpired());
//    action->Setup();
//  }
//
//  {
//    auto action = MCDriver()->Override()->FSUPowerOn()->CreateAction().Generic().BooleanEquation(
//        "PON && !OS",
//    Types::AlertSeverity::Enum::WARNING);
//    action->AddVariable("PON", MCDriver()->Status()->FSUPowerOn());
//    action->AddVariable("OS", MCDriver()->Status()->FSUOutputSignal());
//    action->Setup();
//  }
  MCDriver()->Override()->MagnetronPSFeedback()->CreateAction().StaticValue(false);
  MCDriver()->Override()->RFFeedback()->CreateAction().StaticValue(false);
  MCDriver()->Override()->MagnetronPSOutput()->CreateAction().StaticValue(false);
  MCDriver()->Override()->Watchdog()->CreateAction().StaticValue(false);
  MCDriver()->Override()->FSUPowerOn()->CreateAction().StaticValue(false);
}

ISMC1AnalogIn1 *ISMC1::AnalogIn1() const {
  return analogIn1_;
}

ISMC1_MCDriver *ISMC1::MCDriver() const {
  return mcdriver_;
}

Modules::Common::VoltageMonitor_3V3_1V8 *ISMC1::VoltageMonitor() const {
  return voltageMonitor_;
}

Modules::Common::HotSwapController *ISMC1::HotSwapController() const {
  return hotSwapController_;
}

}  // namespace SystemModules::Modules::ISMC1
