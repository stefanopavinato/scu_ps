/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <cstdint>
#include "SystemModules/Modules/Configuration/Slots/Configuration_Slots.h"

namespace SystemModules::Modules::ISMC1 {
class ISMC1_MCDriverStatus;

class ISMC1_MCDriverOverride;

class ISMC1_MCDriver : public Framework::Components::Container {
 public:
  ISMC1_MCDriver(
    Logger::ILogger *logger,
    const Modules::Configuration::ConfigurationCommon &configCommon,
    const Configuration::Configuration_Slots &config);

  [[nodiscard]] ISMC1_MCDriverStatus *Status() const;

  [[nodiscard]] ISMC1_MCDriverOverride *Override() const;

 private:
  ISMC1_MCDriverStatus *status_;

  ISMC1_MCDriverOverride *override_;
};

}  // namespace SystemModules::Modules::ISMC1
