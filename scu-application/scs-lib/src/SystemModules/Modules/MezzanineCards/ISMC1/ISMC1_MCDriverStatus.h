/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "SystemModules/Framework/Components/ItemIn.h"
#include "SystemModules/Framework/Components/Container.h"
#include "SystemModules/Modules/Configuration/ConfigurationCommon.h"
#include "SystemModules/Modules/Configuration/Slots/Configuration_Slots.h"

namespace SystemModules::Modules::ISMC1 {
class ISMC1_MCDriverStatus : public Framework::Components::Container {
 public:
  ISMC1_MCDriverStatus(
    Logger::ILogger *logger,
    const Modules::Configuration::ConfigurationCommon &configCommon,
    const Configuration::Configuration_Slots &config);

  [[nodiscard]] Framework::Components::ItemIn<uint32_t> *Collection1() const;

  [[nodiscard]] Framework::Components::ItemIn<uint32_t> *Collection2() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *WatchdogAExpired() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *WatchdogBExpired() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *TriggerInput() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *TriggerOutput() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *FSUPowerOn() const;

  // ADC ready not used in software and therefore not supported.

  [[nodiscard]] Framework::Components::ItemIn<bool> *MagnetronPSFeedback() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *RFFeedback() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *MagnetronPSEnable() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *WDKeepAlive() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *FSUOutputSignal() const;


 private:
  Framework::Components::ItemIn<uint32_t> *collection1_;

  Framework::Components::ItemIn<uint32_t> *collection2_;

  Framework::Components::ItemIn<bool> *watchdogAExpired_;

  Framework::Components::ItemIn<bool> *watchdogBExpired_;

  Framework::Components::ItemIn<bool> *triggerinput_;

  Framework::Components::ItemIn<bool> *triggeroutput_;

  Framework::Components::ItemIn<bool> *fsupoweron_;

  Framework::Components::ItemIn<bool> *magnetronpsfeedback_;

  Framework::Components::ItemIn<bool> *rffeedback_;

  Framework::Components::ItemIn<bool> *magnetronpsenable_;

  Framework::Components::ItemIn<bool> *wdkeepalive_;

  Framework::Components::ItemIn<bool> *fsuoutputsignal_;

  static const char kName[];
  static const char kDescription[];

  static const char kCollection1Name[];
  static const char kCollection1Description[];
  static const uint32_t kCollection1Position;

  static const char kCollection2Name[];
  static const char kCollection2Description[];
  static const uint32_t kCollection2Position;

  static const char kWatchdogAExpiredName[];
  static const char kWatchdogAExpiredDescription[];
  static const uint32_t kWatchdogAExpiredPosition;

  static const char kWatchdogBExpiredName[];
  static const char kWatchdogBExpiredDescription[];
  static const uint32_t kWatchdogBExpiredPosition;

  static const char kTriggerInputName[];
  static const char kTriggerInputDescription[];
  static const uint32_t kTriggerInputPosition;

  static const char kTriggerOutputName[];
  static const char kTriggerOutputDescription[];
  static const uint32_t kTriggerOutputPosition;

  static const char kFSUPowerOnReadbackName[];
  static const char kFSUPowerOnReadbackDescription[];
  static const uint32_t kFSUPowerOnReadbackPosition;

  static const char kMagnetronPSFeedbackName[];
  static const char kMagnetronPSFeedbackDescription[];
  static const uint32_t kMagnetronPSFeedbackPosition;

  static const char kRFFeedbackName[];
  static const char kRFFeedbackDescription[];
  static const uint32_t kRFFeedbackPosition;

  static const char kMagnetronPSEnableName[];
  static const char kMagnetronPSEnableDescription[];
  static const uint32_t kMagnetronPSEnablePosition;

  static const char kWDKeepAliveName[];
  static const char kWDKeepAliveDescription[];
  static const uint32_t kWDKeepAlivePosition;

  static const char kFSUOutputName[];
  static const char kFSUOutputDescription[];
  static const uint32_t kFSUOutputPosition;
};
}  // namespace SystemModules::Modules::ISMC1
