/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <cstdint>
#include "SystemModules/Framework/Components/ItemIn.h"
#include "SystemModules/Framework/Components/ItemOut.h"
#include "SystemModules/Framework/Components/Container.h"
#include "SystemModules/Modules/Configuration/ConfigurationCommon.h"
#include "SystemModules/Modules/Configuration/Slots/Configuration_Slots.h"

namespace SystemModules::Modules::ISMC1 {
class ISMC1_MCDriverOverride : public Framework::Components::Container {
 public:
  ISMC1_MCDriverOverride(
    Logger::ILogger *logger,
    const Modules::Configuration::ConfigurationCommon &configCommon,
    const Configuration::Configuration_Slots &config);

  [[nodiscard]] Framework::Components::ItemIn<uint32_t> *InputSignalCollection() const;

  [[nodiscard]] Framework::Components::ItemIn<uint32_t> *OutputSignalCollection() const;

  [[nodiscard]] Framework::Components::ItemOut<bool> *MagnetronPSFeedback() const;

  [[nodiscard]] Framework::Components::ItemOut<bool> *RFFeedback() const;

  [[nodiscard]] Framework::Components::ItemOut<bool> *MagnetronPSOutput() const;

  [[nodiscard]] Framework::Components::ItemOut<bool> *Watchdog() const;

  [[nodiscard]] Framework::Components::ItemOut<bool> *FSUPowerOn() const;

 private:
  Framework::Components::ItemIn<uint32_t> *inputSignalCollection_;

  Framework::Components::ItemIn<uint32_t> *outputSignalCollection_;

  Framework::Components::ItemOut<bool> *magnetronPSFeedback_;

  Framework::Components::ItemOut<bool> *rfFeedback_;

  Framework::Components::ItemOut<bool> *magnetronPSOutput_;

  Framework::Components::ItemOut<bool> *watchdog_;

  Framework::Components::ItemOut<bool> *fsuPowerOn_;

  static const char kName[];
  static const char kDescription[];

  static const char kInputSignalCollectionName[];
  static const char kInputSignalCollectionDescription[];
  static const uint32_t kInputSignalCollectionPosition;

  static const char kOutputSignalCollectionName[];
  static const char kOutputSignalCollectionDescription[];
  static const uint32_t kOutputSignalCollectionPosition;

  static const char kMagnetronPSFeedbackName[];
  static const char kMagnetronPSFeedbackDescription[];
  static const uint32_t kMagnetronPSFeedbackPosition;

  static const char kRFFeedbackName[];
  static const char kRFFeedbackDescription[];
  static const uint32_t kRFFeedbackPosition;

  static const char kMagnetronPSOutputName[];
  static const char kMagnetronPSOutputDescription[];
  static const uint32_t kMagnetronPSOutputPosition;

  static const char kWatchdogName[];
  static const char kWatchdogDescription[];
  static const uint32_t kWatchdogPosition;

  static const char kFSUPowerOnName[];
  static const char kFSUPowerOnDescription[];
  static const uint32_t kFSUPowerOnPosition;
};

}  // namespace SystemModules::Modules::ISMC1
