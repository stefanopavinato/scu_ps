/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "SystemModules/Framework/Components/Container.h"
#include "SystemModules/Modules/Configuration/ConfigurationCommon.h"
#include "SystemModules/Modules/Configuration/Modules/Configuration_ISMC1.h"
#include "SystemModules/Modules/Configuration/Slots/Configuration_Slots.h"

namespace SystemModules::Modules {
namespace Common {
class VoltageMonitor_3V3_1V8;
class HotSwapController;
}  // namespace Common
namespace ISMC1 {
class ISMC1AnalogIn1;

class ISMC1_MCDriver;

class ISMC1 : public Framework::Components::Container {
 public:
  ISMC1(
    Logger::ILogger *logger,
    const Modules::Configuration::Modules::Configuration_ISMC1 &configISMC1,
    const Modules::Configuration::ConfigurationCommon &configCommon,
    const Modules::Configuration::Configuration_Slots &config);

  [[nodiscard]] ISMC1AnalogIn1 *AnalogIn1() const;

  [[nodiscard]] ISMC1_MCDriver *MCDriver() const;

  [[nodiscard]] Common::VoltageMonitor_3V3_1V8 *VoltageMonitor() const;

  [[nodiscard]] Modules::Common::HotSwapController *HotSwapController() const;

 private:
  ISMC1AnalogIn1 *analogIn1_;

  ISMC1_MCDriver *mcdriver_;

  Common::VoltageMonitor_3V3_1V8 *voltageMonitor_;

  Modules::Common::HotSwapController *hotSwapController_;
};
}  // namespace ISMC1
}  // namespace SystemModules::Modules
