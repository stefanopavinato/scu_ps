/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "SystemModules/Framework/Components/ItemIn.h"
#include "SystemModules/Framework/Components/ItemOut.h"
#include "SystemModules/Framework/Components/ItemInOut.h"
#include "SystemModules/Framework/Components/Container.h"
#include "SystemModules/Modules/Configuration/ConfigurationCommon.h"
#include "SystemModules/Modules/Configuration/Slots/Configuration_Slots.h"

namespace SystemModules::Modules {
class CardDetectionItems : public Framework::Components::Container {
 public:
  CardDetectionItems(
    Logger::ILogger *logger,
    const Modules::Configuration::ConfigurationCommon &configCommon,
    const Configuration::Configuration_Slots &config,
    bool *mezzanineCardEnable,
    bool *mezzanineCardReset,
    bool *driverEnable,
    bool *driverReset);

  [[nodiscard]] Framework::Components::ItemIn<bool> *MezzanineCardPresent() const;

  [[nodiscard]] Framework::Components::ItemInOut<bool> *MezzanineCardEnable() const;

  [[nodiscard]] Framework::Components::ItemInOut<bool> *MezzanineCardReset() const;

  [[nodiscard]] Framework::Components::ItemOut<bool> *DriverEnable() const;

  [[nodiscard]] Framework::Components::ItemOut<bool> *DriverReset() const;

  [[nodiscard]] Framework::Components::ItemIn<Types::ModuleType> *ExpectedCardType() const;

  [[nodiscard]] Framework::Components::ItemIn<Types::ModuleType> *ModuleState() const;

 private:
  Framework::Components::ItemIn<bool> *mezzanineCardPresent_;

  Framework::Components::ItemInOut<bool> *mezzanineCardEnable_;

  Framework::Components::ItemInOut<bool> *mezzanineCardReset_;

  Framework::Components::ItemOut<bool> *driverEnable_;

  Framework::Components::ItemOut<bool> *driverReset_;

  Framework::Components::ItemIn<Types::ModuleType> *cardType_;

  Framework::Components::ItemIn<Types::ModuleType> *moduleState_;

  static const char kName[];
  static const char kDescription[];

  static const char kMezzanineCardPresentName[];
  static const char kMezzanineCardPresentDescription[];

  static const char kMezzanineCardEnableName[];
  static const char kMezzanineCardEnableDescription[];

  static const char kMezzanineCardResetName[];
  static const char kMezzanineCardResetDescription[];

  static const char kDriverEnableName[];
  static const char kDriverEnableDescription[];

  static const char kDriverResetName[];
  static const char kDriverResetDescription[];

  static const char kDriverCardTypeName[];
  static const char kDriverCardTypeDescription[];

  static const char kModuleStateName[];
  static const char kModuleStateDescription[];
};
}  // namespace SystemModules::Modules
