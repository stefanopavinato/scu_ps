/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Logger/ILogger.h"
#include "SystemModules/Framework/MonitoringFunctions/MFTemplate.h"
#include "SystemModules/Framework/Builder/Accessors/ReadAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Accessors/ReadWriteAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Actions/ActionBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemInBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemOutBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemInOutBuilder.h"
#include "SystemModules/Framework/Builder/Components/ComponentBuilder.h"
#include "CardDetectionItems.h"

namespace SystemModules::Modules {

const char CardDetectionItems::kName[] = "CardDetectionItems";
const char CardDetectionItems::kDescription[] = "Items to control insertion/removal of mezzanine cards.";

const char CardDetectionItems::kMezzanineCardPresentName[] = "MezzanineCardPresent";
const char CardDetectionItems::kMezzanineCardPresentDescription[] = "Active, if mezzanine card is present.";

const char CardDetectionItems::kMezzanineCardEnableName[] = "MezzanineCardEnable";
const char CardDetectionItems::kMezzanineCardEnableDescription[] =
  "Enables the MAIN Supply of the mezzanine card via the Platform Control Signals.";

const char CardDetectionItems::kMezzanineCardResetName[] = "MezzanineCardReset";
const char CardDetectionItems::kMezzanineCardResetDescription[] =
  "Resets the mezzanine card via the Platform Control Signals.";

const char CardDetectionItems::kDriverEnableName[] = "DriverEnable";
const char CardDetectionItems::kDriverEnableDescription[] = "Enables the MC_Driver in the SCU Firmware.";

const char CardDetectionItems::kDriverResetName[] = "DriverReset";
const char CardDetectionItems::kDriverResetDescription[] = "Resets the MC_Driver in the SCU Firmware.";

const char CardDetectionItems::kDriverCardTypeName[] = "CardType";
const char CardDetectionItems::kDriverCardTypeDescription[] = "Type of the MC expected by the Firmware.";

const char CardDetectionItems::kModuleStateName[] = "ModuleState";
const char CardDetectionItems::kModuleStateDescription[] = "State of the MC.";

CardDetectionItems::CardDetectionItems(
  Logger::ILogger *logger,
  const Modules::Configuration::ConfigurationCommon &configCommon,
  const Configuration::Configuration_Slots &config,
  bool *mezzanineCardEnable,
  bool *mezzanineCardReset,
  bool *driverEnable,
  bool *driverReset)
  : Container(
  logger,
  kName,
  kDescription) {
  auto updateInterval = configCommon.UpdateInterval()->Medium()->Value();

  // Create in-value present
  mezzanineCardPresent_ = Create().ItemIn<bool>(
    kMezzanineCardPresentName,
    kMezzanineCardPresentDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::SHARED,
    1).AddAndReturnPointer();
  mezzanineCardPresent_->CreateAccessor().DigitalInput(
    config.Gpio()->Present()->Address(),
    config.Gpio()->Present()->IsInverted());

  // Create out-value power enable
  mezzanineCardEnable_ = Create().ItemInOut<bool>(
    kMezzanineCardEnableName,
    kMezzanineCardEnableDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::SHARED,
    Types::WriteStrategy::Enum::ON_UPDATE,
    false).AddAndReturnPointer();
  mezzanineCardEnable_->CreateAccessor().DigitalInputOutput(
    config.Gpio()->Enable()->Address(),
    config.Gpio()->Enable()->IsInverted());
  mezzanineCardEnable_->CreateAction().ReadValue(
    mezzanineCardEnable);

  // Create out-value reset
  mezzanineCardReset_ = Create().ItemInOut<bool>(
    kMezzanineCardResetName,
    kMezzanineCardResetDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::SHARED,
    Types::WriteStrategy::Enum::ON_UPDATE,
    false).AddAndReturnPointer();
  mezzanineCardReset_->CreateAccessor().DigitalInputOutput(
    config.Gpio()->Reset()->Address(),
    config.Gpio()->Reset()->IsInverted());
  mezzanineCardReset_->CreateAction().ReadValue(
    mezzanineCardReset);

  // Create out-value driver enable
  driverEnable_ = Create().ItemOut(
    kDriverEnableName,
    kDriverEnableDescription,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    true,
    false).AddAndReturnPointer();
  driverEnable_->CreateAccessor().ScuRegister(
    config.Scu()->Values()->DriverEnable()->Address(),
    config.Scu()->Values()->DriverEnable()->Bit(),
    config.Scu()->Values()->DriverEnable()->Size());
  driverEnable_->CreateAction().ReadValue(
    driverEnable);

  // Create out-value driver reset
  driverReset_ = Create().ItemOut(
    kDriverResetName,
    kDriverResetDescription,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    true,
    true).AddAndReturnPointer();
  driverReset_->CreateAccessor().ScuRegister(
    config.Scu()->Values()->DriverReset()->Address(),
    config.Scu()->Values()->DriverReset()->Bit(),
    config.Scu()->Values()->DriverReset()->Size());
  driverReset_->CreateAction().ReadValue(
    driverReset);

  cardType_ = Create().ItemIn<Types::ModuleType>(
      kDriverCardTypeName,
      kDriverCardTypeDescription,
      Types::ValueGroup::Enum::NONE,
      std::chrono::milliseconds(updateInterval),
      Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  cardType_->CreateAccessor().ScuRegister(
      config.Scu()->ExpectedTypeAddress()->GetValue(),
      0,
      32);
}

Framework::Components::ItemIn<bool> *CardDetectionItems::MezzanineCardPresent() const {
  return mezzanineCardPresent_;
}

Framework::Components::ItemInOut<bool> *CardDetectionItems::MezzanineCardEnable() const {
  return mezzanineCardEnable_;
}

Framework::Components::ItemInOut<bool> *CardDetectionItems::MezzanineCardReset() const {
  return mezzanineCardReset_;
}

Framework::Components::ItemOut<bool> *CardDetectionItems::DriverEnable() const {
  return driverEnable_;
}

Framework::Components::ItemOut<bool> *CardDetectionItems::DriverReset() const {
  return driverReset_;
}

Framework::Components::ItemIn<Types::ModuleType> *CardDetectionItems::ExpectedCardType() const {
  return cardType_;
}

Framework::Components::ItemIn<Types::ModuleType> *CardDetectionItems::ModuleState() const {
  return moduleState_;
}

}  // namespace SystemModules::Modules
