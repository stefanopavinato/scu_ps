/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Logger/ILogger.h"
#include "SystemModules/Framework/Builder/Accessors/ReadAccessorBuilder.h"
#include "SystemModules/Modules/ModuleFactory.h"
#include "SystemModules/Modules/MezzanineCards/MezzanineCardDetection/CardDetectionItems.h"
#include "SystemModules/Modules/MezzanineCards/Management/Management.h"
#include "SystemModules/Modules/MezzanineCards/Management/MezzanineCardInfo.h"
#include "SystemModules/Modules/MezzanineCards/States/MCStateInitialize1.h"
#include "ModuleMezzanineCard.h"

namespace SystemModules::Framework::Components {

const char ModuleMezzanineCard::kDescription[] = "";
const char ModuleMezzanineCard::kComponentType[] = "ModuleMezzanineCard";

ModuleMezzanineCard::ModuleMezzanineCard(
  Logger::ILogger *logger,
  Modules::Configuration::Configuration *configuration,
  Modules::Configuration::ConfigurationCommon *configCommon,
  Modules::Configuration::Configuration_Slots *config,
  WorkloadBalancing::WorkloadBalanceHandler *workloadBalanceHandler)
  : ModuleBase(
  logger,
  config->Name()->GetValue(),
  kDescription,
  kComponentType,
  workloadBalanceHandler)
  , state_(std::make_unique<States::MCStateInitialize1>(this))
  , mezzanineCardEnable_(false)
  , mezzanineCardReset_(true)
  , driverEnable_(false)
  , driverReset_(true)
  , configuration_(configuration)
  , configCommon_(configCommon)
  , config_(config)
  , cardDetectionItems_(nullptr)
  , management_(nullptr)
  , application_(nullptr) {
}

Types::ModuleState ModuleMezzanineCard::GetStateId() const {
  return state_->GetId();
}

Types::AlertSeverity::Enum ModuleMezzanineCard::Update(
  const std::chrono::steady_clock::time_point &timePointNow) {
  state_->OnUpdate();
  return Types::AlertSeverity::Enum::OK;
}

void ModuleMezzanineCard::SetState(std::unique_ptr<States::MCStateBase> stateNew) {
  Logger()->Info(GetName().append(" to ").append(stateNew->GetId().ToString()));
  state_->OnExit();
  state_ = std::move(stateNew);
  state_->OnEntry();
}

bool *ModuleMezzanineCard::MezzanineCardEnable() {
  return &mezzanineCardEnable_;
}

bool *ModuleMezzanineCard::MezzanineCardReset() {
  return &mezzanineCardReset_;
}

bool *ModuleMezzanineCard::DriverEnable() {
  return &driverEnable_;
}

bool *ModuleMezzanineCard::DriverReset() {
  return &driverReset_;
}

Modules::CardDetectionItems *ModuleMezzanineCard::CardDetectionItems() const {
  return cardDetectionItems_;
}

bool ModuleMezzanineCard::InstallCardDetectionItems() {
  // Create card detection items
  try {
    auto cardDetectionItems = std::make_unique<Modules::CardDetectionItems>(
      Logger(),
      *configCommon_,
      *config_,
      MezzanineCardEnable(),
      MezzanineCardReset(),
      DriverEnable(),
      DriverReset());
    // Add card detection items
    cardDetectionItems_ = cardDetectionItems.get();
    AddComponent(std::move(cardDetectionItems));
  } catch (const std::invalid_argument &ia) {
    cardDetectionItems_ = nullptr;
    SetAlertSeverity(Types::AlertSeverity::Enum::ERROR,
                     GetPath().append(": ia.what()"));
    return false;
  }
  // Initialize hardware
  if (!cardDetectionItems_->Initialize(workloadBalanceHandler_)) {
    SetAlertSeverity(Types::AlertSeverity::Enum::ERROR,
                     GetPath().append(": Failed to initialize card detection items"));
    return false;
  }
  return true;
}

bool ModuleMezzanineCard::UninstallCardDetectionItems() {
  // Check if card detection items holds an object
  if (cardDetectionItems_ == nullptr) {
    SetAlertSeverity(Types::AlertSeverity::Enum::ERROR,
                     GetPath().append(": Card detection items not found"));
    return false;
  }
  // De-Initialize card detection items
  if (!cardDetectionItems_->DeInitialize(workloadBalanceHandler_)) {
    SetAlertSeverity(Types::AlertSeverity::Enum::ERROR,
                     GetPath().append(": Failed to de initialize"));
    return false;
  }
  // Remove card detection items from components
  RemoveComponent(cardDetectionItems_->GetName());
  // Reset card detection items pointer
  cardDetectionItems_ = nullptr;
  return true;
}

Modules::Management *ModuleMezzanineCard::Management() const {
  return management_;
}

bool ModuleMezzanineCard::InstallManagement() {
  try {
    // Create management
    auto management = std::make_unique<SystemModules::Modules::Management>(
      Logger(),
      *configCommon_,
      *config_);
    // Add management
    management_ = management.get();
    AddComponent(std::move(management));
  } catch (const std::invalid_argument &ia) {
    management_ = nullptr;
    SetAlertSeverity(Types::AlertSeverity::Enum::ERROR,
                     GetPath().append(": ia.what()"));
    return false;
  }
  // Initialize hardware
  if (!management_->Initialize(workloadBalanceHandler_)) {
    SetAlertSeverity(Types::AlertSeverity::Enum::ERROR,
                     GetPath().append(": Failed to initialize management"));
    return false;
  }
  return true;
}

bool ModuleMezzanineCard::UninstallManagement() {
  // Check if management holds an object
  if (management_ == nullptr) {
    SetAlertSeverity(Types::AlertSeverity::Enum::ERROR,
                     GetPath().append(": Management not found"));
    return false;
  }
  // De-Initialize management
  if (!management_->DeInitialize(workloadBalanceHandler_)) {
    SetAlertSeverity(Types::AlertSeverity::Enum::ERROR,
                     GetPath().append(": De initialize failed"));
    return false;
  }
  // Remove application from components
  RemoveComponent(management_->GetName());
  // Reset application pointer
  management_ = nullptr;
  return true;
}

SystemModules::Framework::Components::Container *ModuleMezzanineCard::Application() const {
  return application_;
}

bool ModuleMezzanineCard::InstallApplication() {
  try {
    // Create application
    auto application = SystemModules::Modules::ModuleFactory::Create(
      Logger(),
      *configuration_,
      *configCommon_,
      *config_,
      management_->MezzanineCardInfo()->CardType()->GetValue()->GetEnum());
    // Check if application holds an object
    if (!application) {
      SetAlertSeverity(Types::AlertSeverity::Enum::ERROR,
                       GetPath().append(": Application not found"));
      return false;
    }
    // Add application
    application_ = application.value().get();
    AddComponent(std::move(application.value()));
  } catch (const std::invalid_argument &ia) {
    application_ = nullptr;
    Logger()->Error(ia.what());
    SetAlertSeverity(Types::AlertSeverity::Enum::ERROR,
                     GetPath().append(": ia.what()"));
    return false;
  }
  // Initialize hardware
  if (!application_->Initialize(workloadBalanceHandler_)) {
    SetAlertSeverity(Types::AlertSeverity::Enum::ERROR,
                     GetPath().append(": Failed to initialize application"));
    return false;
  }
  return true;
}

bool ModuleMezzanineCard::UninstallApplication() {
  // Check if application holds an object
  if (application_ == nullptr) {
    SetAlertSeverity(Types::AlertSeverity::Enum::ERROR,
                     GetPath().append(": Application not found"));
    return false;
  }
  // De-Initialize application
  if (!application_->DeInitialize(workloadBalanceHandler_)) {
    SetAlertSeverity(Types::AlertSeverity::Enum::ERROR,
                     GetPath().append(": Failed to de initialize application"));
    return false;
  }
  // Remove application from components
  RemoveComponent(application_->GetName());
  // Reset application pointer
  application_ = nullptr;
  return true;
}

}  // namespace SystemModules::Framework::Components
