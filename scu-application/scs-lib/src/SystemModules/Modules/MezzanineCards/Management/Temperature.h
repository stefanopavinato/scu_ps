/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "SystemModules/Framework/Components/ItemIn.h"
#include "SystemModules/Framework/Components/I2CDeviceLM75.h"
#include "SystemModules/Modules/Configuration/ConfigurationCommon.h"

namespace SystemModules::Modules {
class Temperature : public Framework::Components::I2CDeviceLM75 {
 public:
  Temperature(
    Logger::ILogger *logger,
    const Modules::Configuration::ConfigurationCommon &configCommon,
    const std::string &name,
    const std::string &description,
    const uint16_t &address,
    const std::string &path,
    const Types::MonitoringFunctionId::Enum &warningId,
    const Types::MonitoringFunctionId::Enum &errorId);

  [[nodiscard]] Framework::Components::ItemIn<Types::Value<int32_t>> *Value() const;

 private:
  Framework::Components::ItemIn<Types::Value<int32_t>> *value_;

  static const uint32_t kOnDelay_ms;
  static const uint32_t kOffDelay_ms;

  static const char kTemperatureName[];
  static const char kTemperatureDescription[];
  static const char kTemperatureSubPath[];
};
}  // namespace SystemModules::Modules
