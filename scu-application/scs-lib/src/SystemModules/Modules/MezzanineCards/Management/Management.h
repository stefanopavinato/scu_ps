/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "SystemModules/Modules/Configuration/Slots/Configuration_Slots.h"
#include "SystemModules/Framework/Components/Container.h"
#include "SystemModules/Modules/MezzanineCards/Common/ScuArray.h"
#include "SystemModules/Modules/Configuration/ConfigurationCommon.h"

namespace SystemModules::Modules {
class MezzanineCardInfo;
class Temperature;
class Management : public Framework::Components::Container {
 public:
  Management(
    Logger::ILogger *logger,
    const Modules::Configuration::ConfigurationCommon &configCommon,
    const Modules::Configuration::Configuration_Slots &config);

  [[nodiscard]] Modules::MezzanineCardInfo *MezzanineCardInfo() const;

  [[nodiscard]] Modules::Common::ScuArray *ScuArray() const;

  [[nodiscard]] Modules::Temperature *UpperTemperature() const;

  [[nodiscard]] Modules::Temperature *LowerTemperature() const;

 private:
  Modules::MezzanineCardInfo *mezzanineCardInfo_;

  Modules::Common::ScuArray *scuArray_;

  Modules::Temperature *upperTemperature_;

  Modules::Temperature *lowerTemperature_;

  static const char kName[];
  static const char kDescription[];

  static const char kUpperTemperatureName[];
  static const char kUpperTemperatureDescription[];
  static const uint16_t kUpperTemperatureAddress;

  static const char kLowerTemperatureName[];
  static const char kLowerTemperatureDescription[];
  static const uint16_t kLowerTemperatureAddress;
};
}  // namespace SystemModules::Modules
