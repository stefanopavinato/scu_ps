/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Actions/ActionTemplate.h"
#include "SystemModules/Framework/Accessors/AccessorTemplate.h"
#include "SystemModules/Framework/MonitoringFunctions/MFTemplate.h"
#include "SystemModules/Framework/Accessors/AccessorBase.h"
#include "SystemModules/Framework/Builder/Accessors/ReadAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemInBuilder.h"
#include "SystemModules/Framework/Builder/Components/ComponentBuilder.h"
#include "Temperature.h"

namespace SystemModules::Modules {

const uint32_t Temperature::kOnDelay_ms = 5000;
const uint32_t Temperature::kOffDelay_ms = 5000;

const char Temperature::kTemperatureName[] = "Value";
const char Temperature::kTemperatureDescription[] = "Temperature at the upper/lower pcb edge";
const char Temperature::kTemperatureSubPath[] = "/temp1_input";

Temperature::Temperature(
  Logger::ILogger *logger,
  const Modules::Configuration::ConfigurationCommon &configCommon,
  const std::string &name,
  const std::string &description,
  const uint16_t &address,
  const std::string &path,
  const Types::MonitoringFunctionId::Enum &warningId,
  const Types::MonitoringFunctionId::Enum &errorId)
  : I2CDeviceLM75(
  logger,
  name,
  description,
  address,
  path) {
  auto updateInterval = configCommon.UpdateInterval()->Slow()->Value();

  value_ = Create().ItemIn<Types::Value<int32_t>>(
    kTemperatureName,
    kTemperatureDescription,
    Types::ValueGroup::Enum::TEMPERATURE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::SHARED,
    Types::Unit::Enum::DEGREE_CELSIUS,
    Types::UnitPrefix::Enum::MILLI).AddAndReturnPointer();
  value_->CreateAccessor().File<int32_t, std::string>(
    GetDevicePath().append(kTemperatureSubPath),
    1,
    1);
  value_->CreateMonitoringFunction().IsOutside(
    Types::AlertSeverity::Enum::WARNING,
    warningId,
    std::chrono::milliseconds(kOnDelay_ms),
    std::chrono::milliseconds(kOffDelay_ms),
    Types::Value<int32_t>(
      0,
      Types::Unit::Enum::DEGREE_CELSIUS,
      Types::UnitPrefix::Enum::MILLI),
    Types::Value<int32_t>(
      88000,
      Types::Unit::Enum::DEGREE_CELSIUS,
      Types::UnitPrefix::Enum::MILLI));
  value_->CreateMonitoringFunction().IsOutside(
    Types::AlertSeverity::Enum::ERROR,
    errorId,
    std::chrono::milliseconds(kOnDelay_ms),
    std::chrono::milliseconds(kOffDelay_ms),
    Types::Value<int32_t>(
      -1000,
      Types::Unit::Enum::DEGREE_CELSIUS,
      Types::UnitPrefix::Enum::MILLI),
    Types::Value<int32_t>(
      95000,
      Types::Unit::Enum::DEGREE_CELSIUS,
      Types::UnitPrefix::Enum::MILLI));
}

Framework::Components::ItemIn<Types::Value<int32_t>> *Temperature::Value() const {
  return value_;
}

}  // namespace SystemModules::Modules
