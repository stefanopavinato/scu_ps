/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Builder/Accessors/ReadAccessorBuilder.h"
#include "SystemModules/Framework/Accessors/AccessorTemplate.h"
#include "MezzanineCardInfo.h"
#include "Temperature.h"
#include "Management.h"

namespace SystemModules::Modules {

const char Management::kName[] = "Management";
const char Management::kDescription[] = "Items for MC management";

const char Management::kUpperTemperatureName[] = "UpperTemperature";
const char Management::kUpperTemperatureDescription[] = "Temperature at the upper pcb edge.";
const uint16_t Management::kUpperTemperatureAddress = 0x48;

const char Management::kLowerTemperatureName[] = "LowerTemperature";
const char Management::kLowerTemperatureDescription[] = "Temperature at the lower pcb edge.";
const uint16_t Management::kLowerTemperatureAddress = 0x49;

Management::Management(
  Logger::ILogger *logger,
  const Modules::Configuration::ConfigurationCommon &configCommon,
  const Modules::Configuration::Configuration_Slots &config)
  : Container(
  logger,
  kName,
  kDescription) {
  (void) config;
  // Create card info
  auto cardInfo = std::make_unique<Modules::MezzanineCardInfo>(
    logger,
    config.PathI2CManagement()->GetValue());
  mezzanineCardInfo_ = cardInfo.get();
  AddComponent(std::move(cardInfo));
  // Set scu array
  auto scuArray = std::make_unique<Common::ScuArray>(
    logger,
    configCommon,
    config);
  scuArray_ = scuArray.get();
  AddComponent(std::move(scuArray));
  // Create upper temperature
  auto upperTemperature = std::make_unique<Modules::Temperature>(
    logger,
    configCommon,
    kUpperTemperatureName,
    kUpperTemperatureDescription,
    kUpperTemperatureAddress,
    config.PathI2CManagement()->GetValue(),
    Types::MonitoringFunctionId::Enum::SCU_MON_W_FUNCTION_temp_upper_PCB_Edge,
    Types::MonitoringFunctionId::Enum::SCU_MON_E_FUNCTION_temp_upper_PCB_Edge);
  upperTemperature_ = upperTemperature.get();
  AddComponent(std::move(upperTemperature));
  // Create lower temperature
  auto lowerTemperature = std::make_unique<Modules::Temperature>(
    logger,
    configCommon,
    kLowerTemperatureName,
    kLowerTemperatureDescription,
    kLowerTemperatureAddress,
    config.PathI2CManagement()->GetValue(),
    Types::MonitoringFunctionId::Enum::SCU_MON_W_FUNCTION_temp_lower_PCB_Edge,
    Types::MonitoringFunctionId::Enum::SCU_MON_E_FUNCTION_temp_lower_PCB_Edge);
  lowerTemperature_ = lowerTemperature.get();
  AddComponent(std::move(lowerTemperature));
}

Modules::MezzanineCardInfo *Management::MezzanineCardInfo() const {
  return mezzanineCardInfo_;
}

Modules::Common::ScuArray *Management::ScuArray() const {
  return scuArray_;
}

Modules::Temperature *Management::UpperTemperature() const {
  return upperTemperature_;
}

Modules::Temperature *Management::LowerTemperature() const {
  return lowerTemperature_;
}

}  // namespace SystemModules::Modules
