/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Builder/Actions/ActionBuilder.h"
#include "SystemModules/Framework/Builder/Accessors/ReadAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Accessors/WriteAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Accessors/ReadWriteAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemInOutBuilder.h"
#include "SystemModules/Framework/Builder/Components/ParameterItemInBuilder.h"
#include "SystemModules/Framework/Builder/Components/ComponentBuilder.h"
#include "MezzanineCardInfo.h"

namespace SystemModules::Modules {

const char MezzanineCardInfo::kName[] = "EEProm";
const char MezzanineCardInfo::kDescription[] = "Default management EEProm";
const uint16_t MezzanineCardInfo::kAddress = 0x50;

const char MezzanineCardInfo::kManufacturerName[] = "Manufacturer";
const char MezzanineCardInfo::kManufacturerDescription[] = "";
const uint32_t MezzanineCardInfo::kManufacturerAddress = 0x00;
const uint32_t MezzanineCardInfo::kManufacturerSize = 32;

const char MezzanineCardInfo::kBoardNameStringName[] = "BoardNameString";
const char MezzanineCardInfo::kBoardNameName[] = "BoardName";
const char MezzanineCardInfo::kBoardNameDescription[] = "";
const uint32_t MezzanineCardInfo::kBoardNameAddress = 0x20;
const uint32_t MezzanineCardInfo::kBoardNameSize = 16;

const char MezzanineCardInfo::kBoardRevisionName[] = "BoardRevision";
const char MezzanineCardInfo::kBoardRevisionDescription[] = "";
const uint32_t MezzanineCardInfo::kBoardRevisionAddress = 0x30;
const uint32_t MezzanineCardInfo::kBoardRevisionSize = 16;

const char MezzanineCardInfo::kSerialNumberName[] = "SerialNumber";
const char MezzanineCardInfo::kSerialNumberDescription[] = "";
const uint32_t MezzanineCardInfo::kSerialNumberAddress = 0x40;
const uint32_t MezzanineCardInfo::kSerialNumberSize = 16;

const char MezzanineCardInfo::kOperatingHoursName[] = "OperatingHours";
const char MezzanineCardInfo::kOperatingHoursDescription[] = "";
const uint32_t MezzanineCardInfo::kOperatingHoursAddress = 0x60;
const uint32_t MezzanineCardInfo::kOperatingHoursSize = 4;

const char MezzanineCardInfo::kMD5Name[] = "MD5";
const char MezzanineCardInfo::kMD5Description[] = "";
const uint32_t MezzanineCardInfo::kMD5Address = 0x80;
const uint32_t MezzanineCardInfo::kMD5Size = 32;

const char MezzanineCardInfo::kUUIDName[] = "UUID";
const char MezzanineCardInfo::kUUIDDescription[] = "";
const uint32_t MezzanineCardInfo::kUUIDAddress = 0xFC;
const uint32_t MezzanineCardInfo::kUUIDSize = 4;

MezzanineCardInfo::MezzanineCardInfo(
  Logger::ILogger *logger,
  const std::string &path)
  : I2CDevice24C02(
  logger,
  kName,
  kDescription,
  kAddress,
  path) {
  // Create value manufacturer
  manufacturer_ = Create().ParameterIn<std::string>(
    kManufacturerName,
    kManufacturerDescription,
    Types::Affiliation::Enum::SHARED).AddAndReturnPointer();
  manufacturer_->CreateAccessor().I2CEEProm<std::string>(
    GetDevicePath(),
    kManufacturerAddress,
    kManufacturerSize,
    "",
    "");
  boardNameString_ = Create().ParameterIn<std::string>(
    kBoardNameStringName,
    kBoardNameDescription,
    Types::Affiliation::Enum::SHARED).AddAndReturnPointer();
  boardNameString_->CreateAccessor().I2CEEProm<std::string>(
    GetDevicePath(),
    kBoardNameAddress,
    kBoardNameSize,
    "",
    "");
  boardName_ = Create().ParameterIn<Types::ModuleType>(
    kBoardNameName,
    kBoardNameDescription,
    Types::Affiliation::Enum::SHARED).AddAndReturnPointer();
  boardName_->CreateAccessor().I2CEEProm<std::string>(
    GetDevicePath(),
    kBoardNameAddress,
    kBoardNameSize,
    "",
    "");

  boardRevision_ = Create().ParameterIn<std::string>(
    kBoardRevisionName,
    kBoardRevisionDescription,
    Types::Affiliation::Enum::SHARED).AddAndReturnPointer();
  boardRevision_->CreateAccessor().I2CEEProm<std::string>(
    GetDevicePath(),
    kBoardRevisionAddress,
    kBoardRevisionSize,
    "",
    "");

  serialNumber_ = Create().ParameterIn<std::string>(
    kSerialNumberName,
    kSerialNumberDescription,
    Types::Affiliation::Enum::SHARED).AddAndReturnPointer();
  serialNumber_->CreateAccessor().I2CEEProm<std::string>(
    GetDevicePath(),
    kSerialNumberAddress,
    kSerialNumberSize,
    "",
    "");
  // ToDo: test io value
  operatingHours_ = Create().ItemInOut<std::chrono::minutes>(
    kOperatingHoursName,
    kOperatingHoursDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::minutes(15),
    Types::Affiliation::Enum::SHARED,
    Types::WriteStrategy::Enum::ON_CHANGE,
    false).AddAndReturnPointer();
  operatingHours_->CreateAccessor().I2CEEProm<uint32_t>(
    GetDevicePath(),
    kOperatingHoursAddress,
    kOperatingHoursSize,
    15,
    1);
  operatingHours_->CreateAction().Miscellaneous().Increment(
    operatingHours_->GetValueIn(),
    std::chrono::minutes(15));

  md5_ = Create().ParameterIn<std::string>(
    kMD5Name,
    kMD5Description,
    Types::Affiliation::Enum::SHARED).AddAndReturnPointer();
  md5_->CreateAccessor().I2CEEProm<std::string>(
    GetDevicePath(),
    kMD5Address,
    kMD5Size,
    "",
    "");

  uuid_ = Create().ParameterIn<std::string>(
    kUUIDName,
    kUUIDDescription,
    Types::Affiliation::Enum::SHARED).AddAndReturnPointer();
  uuid_->CreateAccessor().I2CEEProm<std::string>(
    GetDevicePath(),
    kUUIDAddress,
    kUUIDSize,
    "",
    "");
}

Framework::Components::ParameterItemIn<std::string> *MezzanineCardInfo::Manufacturer() const {
  return manufacturer_;
}

Framework::Components::ParameterItemIn<std::string> *MezzanineCardInfo::CardTypeString() const {
  return boardNameString_;
}

Framework::Components::ParameterItemIn<Types::ModuleType> *MezzanineCardInfo::CardType() const {
  return boardName_;
}

Framework::Components::ParameterItemIn<std::string> *MezzanineCardInfo::BoardRevision() const {
  return boardRevision_;
}

Framework::Components::ParameterItemIn<std::string> *MezzanineCardInfo::SerialNumber() const {
  return serialNumber_;
}

Framework::Components::ItemInOut<std::chrono::minutes> *MezzanineCardInfo::OperatingHours() const {
  return operatingHours_;
}

Framework::Components::ParameterItemIn<std::string> *MezzanineCardInfo::MD5() const {
  return md5_;
}

Framework::Components::ParameterItemIn<std::string> *MezzanineCardInfo::UUID() const {
  return uuid_;
}

}  // namespace SystemModules::Modules
