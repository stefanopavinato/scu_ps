/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <experimental/optional>
#include <memory>
#include "Types/ModuleType.h"
#include "Types/ModuleState.h"
#include "SystemModules/Modules/Configuration/Configuration.h"
#include "SystemModules/Modules/Configuration/ConfigurationCommon.h"
#include "SystemModules/Modules/Configuration/Slots/Configuration_Slots.h"

namespace Logger {
class ILogger;
}  // namespace Logger
namespace SystemModules {
namespace Framework::Components {
class ModuleHandler;

class Container;
}  // namespace Framework::Components
namespace Modules {
class ModuleFactory {
 public:
  static std::experimental::optional<std::unique_ptr<Framework::Components::Container>> Create(
    Logger::ILogger *logger,
    const  Modules::Configuration::Configuration &configuration,
    const Modules::Configuration::ConfigurationCommon &configCommon,
    const Configuration::Configuration_Slots &config,
    const Types::ModuleType::Enum &type);
};
}  // namespace Modules
}  // namespace SystemModules
