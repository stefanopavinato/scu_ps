/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <minini/minIni.h>
#include "Configuration/Builder/ConfigurationBuilder.h"
#include "ConfigurationMezzanineCardApplication.h"

namespace SystemModules::Modules::Configuration {

const char ConfigurationMezzanineCardApplication::kName[] = "Application";

const char ConfigurationMezzanineCardApplication::kDoNotActOnBIName[] = "DoNotActOnBI";
const char ConfigurationMezzanineCardApplication::kApplicationSection[] = "FBIS-SCU-MC";
const char ConfigurationMezzanineCardApplication::kDoNotActOnBIKey[] = "DO_NOT_ACT_ON_BI";

ConfigurationMezzanineCardApplication::ConfigurationMezzanineCardApplication(
  Logger::ILogger *logger,
  const uint32_t &slot,
  const std::string &iniFilePath)
  : ::Configuration::ConfigArray(
  kName) {
  // Open ini file
  auto iniFile = minIni(iniFilePath);

  // Do not act on BI
  auto doNotActOnBIValue = iniFile.getbool(
    std::string(kApplicationSection).append(std::to_string(slot)),
    kDoNotActOnBIKey);
  doNotActOnBI_ = Add().KeyValue(
    kDoNotActOnBIName,
    doNotActOnBIValue);
}


::Configuration::ConfigKeyValue<bool> *ConfigurationMezzanineCardApplication::DoNotActOnBI() const {
  return doNotActOnBI_;
}
}  // namespace SystemModules::Modules::Configuration
