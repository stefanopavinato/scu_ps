/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Configuration/Builder/ConfigurationBuilder.h"
#include "ConfigurationSerializerSvnInfo.h"

namespace SystemModules::Modules::Configuration {

const char ConfigurationSerializerSvnInfo::kVersionName[] = "Version";

const char ConfigurationSerializerSvnInfo::kBranchName[] = "Branch";

const char ConfigurationSerializerSvnInfo::kTimestampName[] = "Timestamp";

const char ConfigurationSerializerSvnInfo::kDateName[] = "Date";

const char ConfigurationSerializerSvnInfo::kTimeName[] = "Time";

const char ConfigurationSerializerSvnInfo::kIsDirtyName[] = "IsDirty";

ConfigurationSerializerSvnInfo::ConfigurationSerializerSvnInfo(
  Logger::ILogger *logger,
  const std::string &name,
  const uint32_t &version,
  const std::string &branch,
  const uint32_t &timestamp,
  const std::string &date,
  const std::string &time,
  const bool &isDirty)
  : ::Configuration::ConfigArray(
  name) {
  version_ = Add().KeyValue(
    kVersionName,
    version);

  branch_ = Add().KeyValue(
    kBranchName,
    branch);

  timestamp_ = Add().KeyValue(
    kTimestampName,
    timestamp);

  date_ = Add().KeyValue(
    kDateName,
    date);

  time_ = Add().KeyValue(
    kTimeName,
    time);

  isDirty_ = Add().KeyValue(
    kIsDirtyName,
    isDirty);
}

::Configuration::ConfigKeyValue<uint32_t> *ConfigurationSerializerSvnInfo::Version() const {
  return version_;
}

::Configuration::ConfigKeyValue<std::string> *ConfigurationSerializerSvnInfo::Branch() const {
  return branch_;
}

::Configuration::ConfigKeyValue<uint32_t> *ConfigurationSerializerSvnInfo::Timestamp() const {
  return timestamp_;
}

::Configuration::ConfigKeyValue<std::string> *ConfigurationSerializerSvnInfo::Date() const {
  return date_;
}

::Configuration::ConfigKeyValue<std::string> *ConfigurationSerializerSvnInfo::Time() const {
  return time_;
}

::Configuration::ConfigKeyValue<bool> *ConfigurationSerializerSvnInfo::IsDirty() const {
  return isDirty_;
}

}  // namespace SystemModules::Modules::Configuration
