/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Logger/ILogger.h"
#include "Configuration/ConfigArray.h"
#include "Configuration/ConfigKeyValue.h"

namespace SystemModules::Modules::Configuration {
class ConfigurationSerializerAddressOffset : public ::Configuration::ConfigArray {
 public:
  explicit ConfigurationSerializerAddressOffset(
    Logger::ILogger *logger);

  [[nodiscard]] ::Configuration::ConfigKeyValue<uint64_t> *MC() const;

  [[nodiscard]] ::Configuration::ConfigKeyValue<uint64_t> *SLINK() const;

 private:
  static const char kName[];

  ::Configuration::ConfigKeyValue<uint64_t> *mc_;

  ::Configuration::ConfigKeyValue<uint64_t> *slink_;

  static const char kMCName[];
  static const uint64_t kMCAddress;

  static const char kSLINKName[];
  static const uint64_t kSLINKAddress;
};
}  // namespace SystemModules::Modules::Configuration
