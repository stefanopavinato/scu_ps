/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Logger/ILogger.h"
#include "Configuration/ConfigContainer.h"
#include "Configuration/ConfigItem.h"
#include "Configuration/ConfigKeyValue.h"
#include "ConfigurationSerializerSlot.h"
#include "ConfigurationSerializerAddress.h"
#include "ConfigurationSerializerSoftware.h"

namespace SystemModules::Modules::Configuration {
class ConfigurationSerializer : public ::Configuration::ConfigContainer {
 public:
  ConfigurationSerializer(
    Logger::ILogger *logger,
    const std::string &iniFilePath);

  [[nodiscard]] ::Configuration::ConfigKeyValue<std::string> *DetectedSlot() const;

  [[nodiscard]] ConfigurationSerializerSlot *SlotA() const;

  [[nodiscard]] ConfigurationSerializerSlot *SlotB() const;

  [[nodiscard]] ConfigurationSerializerAddress *Address() const;

  [[nodiscard]] ConfigurationSerializerSoftware *Software() const;

 private:
  ::Configuration::ConfigKeyValue<std::string> *detectedSlot_;

  ConfigurationSerializerSlot *slotA_;

  ConfigurationSerializerSlot *slotB_;

  ConfigurationSerializerAddress *address_;

  ConfigurationSerializerSoftware *software_;

  static const char kName[];

  static const char kSlotAName[];
  static const char kSlotBName[];

  static const char kDetectedSlotName[];
  static const uint32_t kDetectedSlotId;
};
}  // namespace SystemModules::Modules::Configuration
