/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Configuration/Builder/ConfigurationBuilder.h"
#include "Configuration_HotSwapController.h"

namespace SystemModules::Modules::Configuration::Devices {

const char Configuration_HotSwapController::kName[] = "HotSwapController";

const char Configuration_HotSwapController::mainCurrentScaleMultiplyName[] = "MainCurrentScaleMultiply";
const char Configuration_HotSwapController::mainCurrentScaleDivideName[] = "MainCurrentScaleDivide";
const char Configuration_HotSwapController::mainVoltageScaleMultiplyName[] = "MainVoltageScaleMultiply";
const char Configuration_HotSwapController::mainVoltageScaleDivideName[] = "MainVoltageScaleDivide";
const char Configuration_HotSwapController::standbyVoltageScaleMultiplyName[] = "StandbyVoltageScaleMultiply";
const char Configuration_HotSwapController::standbyVoltageScaleDivideName[] = "StandbyVoltageScaleDivide";

Configuration_HotSwapController::Configuration_HotSwapController(
  const int32_t &mainCurrentScaleMultiply,
  const int32_t &mainCurrentScaleDivide,
  const int32_t &mainVoltageScaleMultiply,
  const int32_t &mainVoltageScaleDivide,
  const int32_t &standbyVoltageScaleMultiply,
  const int32_t &standbyVoltageScaleDivide)
  : ::Configuration::ConfigContainer(
    kName) {
  // Main current scale multiply
  mainCurrentScaleMultiply_ = Add().KeyValue<int32_t>(
    mainCurrentScaleMultiplyName,
    mainCurrentScaleMultiply);
  // Main current scale divide
  mainCurrentScaleDivide_ = Add().KeyValue<int32_t>(
    mainCurrentScaleDivideName,
    mainCurrentScaleDivide);
  // Main voltage scale multiply
  mainVoltageScaleMultiply_ = Add().KeyValue<int32_t>(
    mainVoltageScaleMultiplyName,
    mainVoltageScaleMultiply);
  // Main voltage scale divide
  mainVoltageScaleDivide_ = Add().KeyValue<int32_t>(
    mainVoltageScaleDivideName,
    mainVoltageScaleDivide);
  // Backup voltage scale multiply
  standbyVoltageScaleMultiply_ = Add().KeyValue<int32_t>(
    standbyVoltageScaleMultiplyName,
    standbyVoltageScaleMultiply);
  // Backup voltage scale divide
  standbyVoltageScaleDivide_ = Add().KeyValue<int32_t>(
    standbyVoltageScaleDivideName,
    standbyVoltageScaleDivide);
}

::Configuration::ConfigKeyValue<int32_t> *Configuration_HotSwapController::MainCurrentScaleMultiply() const {
  return mainCurrentScaleMultiply_;
}

::Configuration::ConfigKeyValue<int32_t> *Configuration_HotSwapController::MainCurrentScaleDivide() const {
  return mainCurrentScaleDivide_;
}

::Configuration::ConfigKeyValue<int32_t> *Configuration_HotSwapController::MainVoltageScaleMultiply() const {
  return mainVoltageScaleMultiply_;
}

::Configuration::ConfigKeyValue<int32_t> *Configuration_HotSwapController::MainVoltageScaleDivide() const {
  return mainVoltageScaleDivide_;
}

::Configuration::ConfigKeyValue<int32_t> *Configuration_HotSwapController::StandbyVoltageScaleMultiply() const {
  return standbyVoltageScaleMultiply_;
}

::Configuration::ConfigKeyValue<int32_t> *Configuration_HotSwapController::StandbyVoltageScaleDivide() const {
  return standbyVoltageScaleDivide_;
}

}  // namespace SystemModules::Modules::Configuration::Devices
