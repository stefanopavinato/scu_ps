/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Configuration/ConfigContainer.h"
#include "Configuration/ConfigKeyValue.h"

namespace SystemModules::Modules::Configuration::Devices {
class Configuration_HotSwapController: public ::Configuration::ConfigContainer {
 public:
  Configuration_HotSwapController(
    const int32_t &mainCurrentScaleMultiply,
    const int32_t &mainCurrentScaleDivide,
    const int32_t &mainVoltageScaleMultiply,
    const int32_t &mainVoltageScaleDivide,
    const int32_t &standbyVoltageScaleMultiply,
    const int32_t &standbyVoltageScaleDivide);

  [[nodiscard]] ::Configuration::ConfigKeyValue<int32_t> *MainCurrentScaleMultiply() const;

  [[nodiscard]] ::Configuration::ConfigKeyValue<int32_t> *MainCurrentScaleDivide() const;

  [[nodiscard]] ::Configuration::ConfigKeyValue<int32_t> *MainVoltageScaleMultiply() const;

  [[nodiscard]] ::Configuration::ConfigKeyValue<int32_t> *MainVoltageScaleDivide() const;

  [[nodiscard]] ::Configuration::ConfigKeyValue<int32_t> *StandbyVoltageScaleMultiply() const;

  [[nodiscard]] ::Configuration::ConfigKeyValue<int32_t> *StandbyVoltageScaleDivide() const;

 private:
  ::Configuration::ConfigKeyValue<int32_t> *mainCurrentScaleMultiply_;

  ::Configuration::ConfigKeyValue<int32_t> *mainCurrentScaleDivide_;

  ::Configuration::ConfigKeyValue<int32_t> *mainVoltageScaleMultiply_;

  ::Configuration::ConfigKeyValue<int32_t> *mainVoltageScaleDivide_;

  ::Configuration::ConfigKeyValue<int32_t> *standbyVoltageScaleMultiply_;

  ::Configuration::ConfigKeyValue<int32_t> *standbyVoltageScaleDivide_;

  static const char kName[];

  static const char mainCurrentScaleMultiplyName[];
  static const char mainCurrentScaleDivideName[];
  static const char mainVoltageScaleMultiplyName[];
  static const char mainVoltageScaleDivideName[];
  static const char standbyVoltageScaleMultiplyName[];
  static const char standbyVoltageScaleDivideName[];
};
}  // namespace SystemModules::Modules::Configuration::Devices
