/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Logger/ILogger.h"
#include "Configuration/ConfigContainer.h"
#include "Configuration/ConfigItem.h"

namespace SystemModules::Modules::Configuration {
class ConfigurationPowerSupply : public ::Configuration::ConfigContainer {
 public:
  ConfigurationPowerSupply(
    Logger::ILogger *logger,
    const std::string &iniFilePath);

  ::Configuration::ConfigItem<std::string> *PathI2CApplication() const;

 private:
  ::Configuration::ConfigItem<std::string> *pathI2CApplication_;

  static const char kPowerSupply[];
};

}  // namespace SystemModules::Modules::Configuration
