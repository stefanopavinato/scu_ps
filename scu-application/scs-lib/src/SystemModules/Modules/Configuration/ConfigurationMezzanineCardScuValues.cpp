/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Accessors/AccessorScuRegister.h"
#include "Configuration/Builder/ConfigurationBuilder.h"
#include "ConfigurationMezzanineCardScuValues.h"

namespace SystemModules::Modules::Configuration {

const char ConfigurationMezzanineCardScuValues::kName[] = "Value";

const char ConfigurationMezzanineCardScuValues::kDriverEnableKey[] = "DriverEnable";
const uint64_t ConfigurationMezzanineCardScuValues::kDriverEnableAddress = 0x14;

const char ConfigurationMezzanineCardScuValues::kDriverResetKey[] = "DriverReset";
const uint64_t ConfigurationMezzanineCardScuValues::kDriverResetAddress = 0x18;

ConfigurationMezzanineCardScuValues::ConfigurationMezzanineCardScuValues(
  Logger::ILogger *logger,
  const uint32_t &slot)
  : ::Configuration::ConfigArray(
  kName) {
  // Driver reset
  driverReset_ = this->Add().ScuRegister(
    kDriverResetKey,
    kDriverResetAddress,
    slot - 1,
    1);
  // Driver enable
  driverEnable_ = Add().ScuRegister(
    kDriverEnableKey,
    kDriverEnableAddress,
    slot - 1,
    1);
}

::Configuration::ConfigScuRegister *ConfigurationMezzanineCardScuValues::DriverReset() const {
  return driverReset_;
}

::Configuration::ConfigScuRegister *ConfigurationMezzanineCardScuValues::DriverEnable() const {
  return driverEnable_;
}

}  // namespace SystemModules::Modules::Configuration
