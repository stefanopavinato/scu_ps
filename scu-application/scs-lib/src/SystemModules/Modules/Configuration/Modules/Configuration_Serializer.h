/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Configuration/ConfigContainer.h"
#include "SystemModules/Modules/Configuration/Devices/Configuration_HotSwapController.h"

namespace SystemModules::Modules::Configuration::Modules {
class Configuration_Serializer : public ::Configuration::ConfigContainer {
 public:
  Configuration_Serializer();

  [[nodiscard]] Devices::Configuration_HotSwapController *HotSwapController() const;

 private:
  Devices::Configuration_HotSwapController *hotSwapController_;

  static const char kName[];
};
}  // namespace SystemModules::Modules::Configuration::Modules
