/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Configuration_BUMC.h"

namespace SystemModules::Modules::Configuration::Modules {

const char Configuration_BUMC::kName[] = "BUMC";

Configuration_BUMC::Configuration_BUMC()
  : ::Configuration::ConfigContainer(
  kName) {
}

}  // namespace SystemModules::Modules::Configuration::Modules
