/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Configuration_Serializer.h"

namespace SystemModules::Modules::Configuration::Modules {

const char Configuration_Serializer::kName[] = "Serializer";

Configuration_Serializer::Configuration_Serializer()
  : ::Configuration::ConfigContainer(
  kName) {
  // Hot swap controller
  auto hotSwapController = std::make_unique<Devices::Configuration_HotSwapController>(
    4,
    4,  // Resistor 4 mOhm
    1,
    1,
    490,  // voltage divider invers: 100*((R1+R2)/R2)
    1250);  // The SOURCE pin has a 1/12.5 resistive divider, multiply is 100 to high => 12.5*100
  hotSwapController_ = hotSwapController.get();
  AddComponent(std::move(hotSwapController));
}

Devices::Configuration_HotSwapController *Configuration_Serializer::HotSwapController() const {
  return hotSwapController_;
}

}  // namespace SystemModules::Modules::Configuration::Modules
