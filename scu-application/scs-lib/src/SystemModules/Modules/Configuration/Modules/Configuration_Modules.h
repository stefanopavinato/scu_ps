/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Configuration/ConfigContainer.h"
#include "Configuration_Serializer.h"
#include "Configuration_BUMC.h"
#include "Configuration_CHMC.h"
#include "Configuration_CLMC.h"
#include "Configuration_ISMC1.h"
#include "Configuration_ISMC2.h"
#include "Configuration_LVDSMC.h"
#include "Configuration_RS485MC.h"

namespace SystemModules::Modules::Configuration::Modules {
class Configuration_Modules : public ::Configuration::ConfigContainer {
 public:
  Configuration_Modules();

  [[nodiscard]] Configuration_Serializer *Serializer() const;

  [[nodiscard]] Configuration_BUMC *BUMC() const;

  [[nodiscard]] Configuration_CHMC *CHMC() const;

  [[nodiscard]] Configuration_CLMC *CLMC() const;

  [[nodiscard]] Configuration_ISMC1 *ISMC1() const;

  [[nodiscard]] Configuration_ISMC2 *ISMC2() const;

  [[nodiscard]] Configuration_LVDSMC *LVDSMC() const;

  [[nodiscard]] Configuration_RS485MC *RS485MC() const;

 private:
  Configuration_Serializer *serializer_;

  Configuration_BUMC *bumc_;

  Configuration_CHMC *chmc_;

  Configuration_CLMC *clmc_;

  Configuration_ISMC1 *ismc1_;

  Configuration_ISMC2 *ismc2_;

  Configuration_LVDSMC *lvdsmc_;

  Configuration_RS485MC *rs485mc_;

  static const char kName[];
};

}  // namespace SystemModules::Modules::Configuration::Modules
