/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Configuration_Modules.h"

namespace SystemModules::Modules::Configuration::Modules {

const char Configuration_Modules::kName[] = "Modules";

Configuration_Modules::Configuration_Modules()
  : ::Configuration::ConfigContainer(
    kName) {
  // Serializer
  auto serializer = std::make_unique<Configuration_Serializer>();
  serializer_ = serializer.get();
  AddComponent(std::move(serializer));
  // BUMC
  auto bumc = std::make_unique<Configuration_BUMC>();
  bumc_ = bumc.get();
  AddComponent(std::move(bumc));
  // CHMC
  auto chmc = std::make_unique<Configuration_CHMC>();
  chmc_ = chmc.get();
  AddComponent(std::move(chmc));
  // CLMC
  auto clmc = std::make_unique<Configuration_CLMC>();
  clmc_ = clmc.get();
  AddComponent(std::move(clmc));
  // ISMC1
  auto ismc1 = std::make_unique<Configuration_ISMC1>();
  ismc1_ = ismc1.get();
  AddComponent(std::move(ismc1));
  // ISMC2
  auto ismc2 = std::make_unique<Configuration_ISMC2>();
  ismc2_ = ismc2.get();
  AddComponent(std::move(ismc2));
  // LVDSMC
  auto lvdsmc = std::make_unique<Configuration_LVDSMC>();
  lvdsmc_ = lvdsmc.get();
  AddComponent(std::move(lvdsmc));
  // RS485MC
  auto rs485mc = std::make_unique<Configuration_RS485MC>();
  rs485mc_ = rs485mc.get();
  AddComponent(std::move(rs485mc));
}

Configuration_Serializer *Configuration_Modules::Serializer() const {
  return serializer_;
}


Configuration_BUMC *Configuration_Modules::BUMC() const {
  return bumc_;
}

Configuration_CHMC *Configuration_Modules::CHMC() const {
  return chmc_;
}

Configuration_CLMC *Configuration_Modules::CLMC() const {
  return clmc_;
}

Configuration_ISMC1 *Configuration_Modules::ISMC1() const {
  return ismc1_;
}

Configuration_ISMC2 *Configuration_Modules::ISMC2() const {
  return ismc2_;
}

Configuration_LVDSMC *Configuration_Modules::LVDSMC() const {
  return lvdsmc_;
}

Configuration_RS485MC *Configuration_Modules::RS485MC() const {
  return rs485mc_;
}

}  // namespace SystemModules::Modules::Configuration::Modules
