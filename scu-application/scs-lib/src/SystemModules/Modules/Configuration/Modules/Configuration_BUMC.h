/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Configuration/ConfigContainer.h"

namespace SystemModules::Modules::Configuration::Modules {
class Configuration_BUMC : public ::Configuration::ConfigContainer {
 public:
  Configuration_BUMC();

 private:
  static const char kName[];
};
}  // namespace SystemModules::Modules::Configuration::Modules
