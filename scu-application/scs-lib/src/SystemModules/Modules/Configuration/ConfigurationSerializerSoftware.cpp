/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <string>
#include <fstream>
#include <locale>
#include <iomanip>
#include "ConfigurationSerializerSoftware.h"

namespace SystemModules::Modules::Configuration {

const char ConfigurationSerializerSoftware::kName[] = "Software";

const char ConfigurationSerializerSoftware::kApplicationName[] = "Application";
const char ConfigurationSerializerSoftware::kApplicationPath[] = "/tmp/application";

const char ConfigurationSerializerSoftware::kPlatformName[] = "Platform";
const char ConfigurationSerializerSoftware::kPlatformPath[] = "/tmp/platform";

ConfigurationSerializerSoftware::ConfigurationSerializerSoftware(
  Logger::ILogger *logger)
  : ::Configuration::ConfigArray(
  kName) {
  // Set application
  {
    bool scuApplicationVersionRead = false;
    system(std::string("scu-application --version > ").append(kApplicationPath).c_str());
    auto file = std::ifstream(kApplicationPath);
    auto words = std::vector<std::string>();
    std::tm t = {};
    if (!file.is_open()) {
      logger->Error("Failed to read scu-application version");
    } else {
      while (!file.eof()) {
        std::string word;
        file >> word;
        words.push_back(word);
      }
      if (words.size() < 6) {
        logger->Error("Failed to read scu-application version");
      } else {
        // Get build timestamp
        std::istringstream ss(std::string(words.at(6)).append(" ").append(words.at(9)));
        if (ss >> std::get_time(&t, "%Y.%m.%d %H:%M:%S")) {
          scuApplicationVersionRead = true;
        } else {
          logger->Warning("Failed to parse application build timestamp");
        }
      }
      file.close();
    }
    if (scuApplicationVersionRead) {
      auto application = std::make_unique<ConfigurationSerializerSvnInfo>(
          logger,
          kApplicationName,
          std::stoul(words.at(1)),
          words.at(3),
          std::mktime(&t),
          words.at(6),
          words.at(9),
          words.size() > 11);
      application_ = application.get();
      AddComponent(std::move(application));
    } else {
      std::get_time(&t, "2020.01.01 00:00:00");
      auto application = std::make_unique<ConfigurationSerializerSvnInfo>(
          logger,
          kApplicationName,
          -1,
          "",
          std::mktime(&t),
          "2020.01.01",
          "00:00:00",
          true);
      application_ = application.get();
      AddComponent(std::move(application));
    }
  }
  // Set platform
  {
    bool scuPlatformVersionRead = false;
    system(std::string("platform-version > ").append(kPlatformPath).c_str());
    auto file = std::ifstream(kPlatformPath);
    auto words = std::vector<std::string>();
    std::tm t = {};
    if (!file.is_open()) {
      logger->Error("Failed to read platform version");
    } else {
      while (!file.eof()) {
        std::string word;
        file >> word;
        words.push_back(word);
      }
      if (words.size() < 6) {
        logger->Error("Failed to read platform version");
      } else {
        // Get build timestamp
        std::istringstream ss(std::string(words.at(6)).append(" ").append(words.at(9)));
        if (ss >> std::get_time(&t, "%Y.%m.%d %H:%M:%S")) {
          scuPlatformVersionRead = true;
        } else {
          logger->Warning("Failed to parse platform build timestamp");
        }
      }
      file.close();
    }
    if (scuPlatformVersionRead) {
      auto platform = std::make_unique<ConfigurationSerializerSvnInfo>(
          logger,
          kPlatformName,
          std::stoul(words.at(1)),
          words.at(3),
          std::mktime(&t),
          words.at(6),
          words.at(9),
          words.size() > 11);
      platform_ = platform.get();
      AddComponent(std::move(platform));
    } else {
      std::get_time(&t, "2020.01.01 00:00:00");
      auto platform = std::make_unique<ConfigurationSerializerSvnInfo>(
          logger,
          kPlatformName,
          -1,
          "",
          std::mktime(&t),
          "2020.01.01",
          "00:00:00",
          true);
      platform_ = platform.get();
      AddComponent(std::move(platform));
    }
  }
}

ConfigurationSerializerSvnInfo *ConfigurationSerializerSoftware::Application() const {
  return application_;
}

ConfigurationSerializerSvnInfo *ConfigurationSerializerSoftware::Platform() const {
  return platform_;
}

}  // namespace SystemModules::Modules::Configuration
