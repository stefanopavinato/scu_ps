/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Configuration/Builder/ConfigurationBuilder.h"
#include "ConfigurationSerializerAddressOffset.h"

namespace SystemModules::Modules::Configuration {

const char ConfigurationSerializerAddressOffset::kName[] = "Offset";

const char ConfigurationSerializerAddressOffset::kMCName[] = "MC";
const uint64_t ConfigurationSerializerAddressOffset::kMCAddress = 0x100;

const char ConfigurationSerializerAddressOffset::kSLINKName[] = "SLINK";
const uint64_t ConfigurationSerializerAddressOffset::kSLINKAddress = 0x100;

ConfigurationSerializerAddressOffset::ConfigurationSerializerAddressOffset(
  Logger::ILogger *logger)
  : ::Configuration::ConfigArray(
  kName) {
  mc_ = Add().KeyValue(
    kMCName,
    kMCAddress);
  slink_ = Add().KeyValue(
    kSLINKName,
    kSLINKAddress);
}

::Configuration::ConfigKeyValue<uint64_t> *ConfigurationSerializerAddressOffset::MC() const {
  return mc_;
}

::Configuration::ConfigKeyValue<uint64_t> *ConfigurationSerializerAddressOffset::SLINK() const {
  return slink_;
}

}  // namespace SystemModules::Modules::Configuration
