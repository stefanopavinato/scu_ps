/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Logger/ILogger.h"
#include "ConfigurationSerializerAddressBase.h"
#include "ConfigurationSerializerAddressOffset.h"
#include "Configuration/ConfigContainer.h"

namespace SystemModules::Modules::Configuration {
class ConfigurationSerializerAddress : public ::Configuration::ConfigContainer {
 public:
  explicit ConfigurationSerializerAddress(
    Logger::ILogger *logger);

  [[nodiscard]] ConfigurationSerializerAddressBase *Base() const;

  [[nodiscard]] ConfigurationSerializerAddressOffset *Offset() const;

 private:
  ConfigurationSerializerAddressBase *base_;

  ConfigurationSerializerAddressOffset *offset_;

  static const char kName[];
};
}  // namespace SystemModules::Modules::Configuration
