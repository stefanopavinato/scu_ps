/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Logger/ILogger.h"
#include "Configuration/ConfigContainer.h"
#include "ConfigurationSerializerNetwork.h"

namespace SystemModules::Modules::Configuration {
class ConfigurationSerializerSlot : public ::Configuration::ConfigContainer {
 public:
  ConfigurationSerializerSlot(
    Logger::ILogger *logger,
    const std::string &name,
    const std::string &iniFilePath);

  [[nodiscard]] ConfigurationSerializerNetwork *Network() const;

 private:
  ConfigurationSerializerNetwork *network_;
};
}  // namespace SystemModules::Modules::Configuration
