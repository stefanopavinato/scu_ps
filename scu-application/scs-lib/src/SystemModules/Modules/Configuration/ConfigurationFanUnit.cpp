/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <minini/minIni.h>
#include "Configuration/Builder/ConfigurationBuilder.h"
#include "ConfigurationFanUnit.h"

namespace SystemModules::Modules::Configuration {

const char ConfigurationFanUnit::kSourceIniFile[] = "IniFile";

const char ConfigurationFanUnit::kIniFileFanUnitSection[] = "FBIS-SCU-FU";
const char ConfigurationFanUnit::kIniFileFanSpeedKey[] = "STARTUP_SPEED_PWM";
const char ConfigurationFanUnit::kIniFileFanAEnableKey[] = "FAN_A_Enable";
const char ConfigurationFanUnit::kIniFileFanBEnableKey[] = "FAN_B_Enable";
const char ConfigurationFanUnit::kIniFileFanMEnableKey[] = "FAN_M_Enable";

const char ConfigurationFanUnit::kFanUnit[] = "FanUnit";
const char ConfigurationFanUnit::kFanSpeedKey[] = "FanSpeed";
const char ConfigurationFanUnit::kFanAEnableKey[] = "FanAEnable";
const char ConfigurationFanUnit::kFanBEnableKey[] = "FanBEnable";
const char ConfigurationFanUnit::kFanMEnableKey[] = "FanMEnable";

ConfigurationFanUnit::ConfigurationFanUnit(
  Logger::ILogger *logger,
  const std::string &iniFilePath)
  : ::Configuration::ConfigContainer(
  kFanUnit) {
  // Open ini file
  auto iniFile = minIni(iniFilePath);
  // Fan speed
  {
    auto value = iniFile.geti(
      kIniFileFanUnitSection,
      kIniFileFanSpeedKey);
    if (value == 0) {
      logger->Error("Failed to read value from ini file");
      throw std::exception();
    }
    fanSpeed_ = this->Add().Value(
      kFanSpeedKey,
      static_cast<uint8_t>(value),
      kSourceIniFile);
  }
  // Fan A enable
  {
    auto value = iniFile.getbool(
      kIniFileFanUnitSection,
      kIniFileFanAEnableKey);
    fanAEnable_ = this->Add().Value(
      kFanAEnableKey,
      value,
      kSourceIniFile);
  }
  // Fan B enable
  {
    auto value = iniFile.getbool(
      kIniFileFanUnitSection,
      kIniFileFanBEnableKey);
    fanBEnable_ = this->Add().Value(
      kFanBEnableKey,
      value,
      kSourceIniFile);
  }
  // Fan M enable
  {
    auto value = iniFile.getbool(
      kIniFileFanUnitSection,
      kIniFileFanMEnableKey);
    fanMEnable_ = this->Add().Value<bool>(
      kFanMEnableKey,
      value,
      kSourceIniFile);
  }
}

::Configuration::ConfigItem<uint8_t> *ConfigurationFanUnit::FanSpeed() const {
  return fanSpeed_;
}

::Configuration::ConfigItem<bool> *ConfigurationFanUnit::FanAEnable() const {
  return fanAEnable_;
}

::Configuration::ConfigItem<bool> *ConfigurationFanUnit::FanBEnable() const {
  return fanBEnable_;
}

::Configuration::ConfigItem<bool> *ConfigurationFanUnit::FanMEnable() const {
  return fanMEnable_;
}

}  // namespace SystemModules::Modules::Configuration
