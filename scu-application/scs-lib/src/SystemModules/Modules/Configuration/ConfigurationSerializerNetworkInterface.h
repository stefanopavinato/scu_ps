/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Logger/ILogger.h"
#include "Configuration/ConfigArray.h"
#include "Configuration/ConfigKeyValue.h"

namespace SystemModules::Modules::Configuration {
class ConfigurationSerializerNetworkInterface : public ::Configuration::ConfigArray {
 public:
  ConfigurationSerializerNetworkInterface(
    Logger::ILogger *logger,
    const std::string &name,
    const std::string &macAddress,
    const std::string &ipAddress,
    const std::string &netMask);

  [[nodiscard]] ::Configuration::ConfigKeyValue<std::string> *MacAddress() const;

  [[nodiscard]] ::Configuration::ConfigKeyValue<std::string> *IpAddress() const;

  [[nodiscard]] ::Configuration::ConfigKeyValue<std::string> *NetMask() const;

 private:
  ::Configuration::ConfigKeyValue<std::string> *macAddress_;

  ::Configuration::ConfigKeyValue<std::string> *ipAddress_;

  ::Configuration::ConfigKeyValue<std::string> *netMask_;

  static const char kMacAddress[];
  static const char kIpAddress[];
  static const char kNetMask[];
};
}  // namespace SystemModules::Modules::Configuration
