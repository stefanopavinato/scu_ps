/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "ConfigurationSerializerSlot.h"

namespace SystemModules::Modules::Configuration {

ConfigurationSerializerSlot::ConfigurationSerializerSlot(
  Logger::ILogger *logger,
  const std::string &name,
  const std::string &iniFilePath)
  : ConfigContainer(std::string("Slot").append(name)) {
  // Network
  auto network = std::make_unique<ConfigurationSerializerNetwork>(
    logger,
    name,
    iniFilePath);
  network_ = network.get();
  AddComponent(std::move(network));
}

ConfigurationSerializerNetwork *ConfigurationSerializerSlot::Network() const {
  return network_;
}

}  // namespace SystemModules::Modules::Configuration
