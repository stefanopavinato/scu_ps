/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Logger/ILogger.h"
#include "Configuration/ConfigContainer.h"
#include "Configuration/ConfigKeyValue.h"

namespace SystemModules::Modules::Configuration {
class ConfigurationCommonSerializer : public ::Configuration::ConfigContainer {
 public:
  explicit ConfigurationCommonSerializer(
    Logger::ILogger *logger);

  [[nodiscard]] ::Configuration::ConfigKeyValue<std::string> *Slot() const;

 private:
  ::Configuration::ConfigKeyValue<std::string> *slot_;

  static const char kName[];

  static const char kSlotName[];
  static const uint32_t kSlotId;
};
}  // namespace SystemModules::Modules::Configuration
