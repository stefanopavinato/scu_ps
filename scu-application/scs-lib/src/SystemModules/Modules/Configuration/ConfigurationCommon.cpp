/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "ConfigurationCommon.h"

namespace SystemModules::Modules::Configuration {

const char ConfigurationCommon::kName[] = "Common";

ConfigurationCommon::ConfigurationCommon(
  Logger::ILogger * logger)
  : ::Configuration::ConfigContainer(
  kName) {
  // Update interval
  auto updateinterval = std::make_unique<ConfigurationUpdateInterval>(
    logger);
  updateInterval_ = updateinterval.get();
  AddComponent(std::move(updateinterval));
  // Serializer
  auto serializer = std::make_unique<ConfigurationCommonSerializer>(
    logger);
  serializer_ = serializer.get();
  AddComponent(std::move(serializer));
}

ConfigurationUpdateInterval *ConfigurationCommon::UpdateInterval() const {
  return updateInterval_;
}

ConfigurationCommonSerializer *ConfigurationCommon::Serializer() const {
  return serializer_;
}

}  // namespace SystemModules::Modules::Configuration
