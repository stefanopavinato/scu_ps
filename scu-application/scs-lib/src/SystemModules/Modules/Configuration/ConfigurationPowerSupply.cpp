/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "ConfigurationPowerSupply.h"

namespace SystemModules::Modules::Configuration {

const char ConfigurationPowerSupply::kPowerSupply[] = "PowerSupply";

ConfigurationPowerSupply::ConfigurationPowerSupply(
  Logger::ILogger *logger,
  const std::string &iniFilePath)
  : ::Configuration::ConfigContainer(
  kPowerSupply) {
  (void) logger;
  (void) iniFilePath;
}

::Configuration::ConfigItem<std::string> *ConfigurationPowerSupply::PathI2CApplication() const {
  return pathI2CApplication_;
}
}  // namespace SystemModules::Modules::Configuration
