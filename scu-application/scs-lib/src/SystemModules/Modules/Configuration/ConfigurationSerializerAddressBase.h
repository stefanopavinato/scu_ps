/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Logger/ILogger.h"
#include "Configuration/ConfigArray.h"
#include "Configuration/ConfigKeyValue.h"

namespace SystemModules::Modules::Configuration {
class ConfigurationSerializerAddressBase : public ::Configuration::ConfigArray {
 public:
  explicit ConfigurationSerializerAddressBase(
    Logger::ILogger *logger);

  [[nodiscard]] ::Configuration::ConfigKeyValue<uint64_t> *CFG() const;

  [[nodiscard]] ::Configuration::ConfigKeyValue<uint64_t> *MC() const;

  [[nodiscard]] ::Configuration::ConfigKeyValue<uint64_t> *SLINK() const;

  [[nodiscard]] ::Configuration::ConfigKeyValue<uint64_t> *OKNOK() const;

  [[nodiscard]] ::Configuration::ConfigKeyValue<uint64_t> *SMF() const;

 private:
  ::Configuration::ConfigKeyValue<uint64_t> *cfg_;

  ::Configuration::ConfigKeyValue<uint64_t> *mc_;

  ::Configuration::ConfigKeyValue<uint64_t> *slink_;

  ::Configuration::ConfigKeyValue<uint64_t> *oknok_;

  ::Configuration::ConfigKeyValue<uint64_t> *smf_;

  static const char kName[];

  static const char kCFGName[];
  static const uint64_t kCFGAddress;

  static const char kMCName[];
  static const uint64_t kMCAddress;

  static const char kSLINKName[];
  static const uint64_t kSLINKAddress;

  static const char kOKNOKName[];
  static const uint64_t kOKNOKAddress;

  static const char kSMFName[];
  static const uint64_t kSMFAddress;
};
}  // namespace SystemModules::Modules::Configuration
