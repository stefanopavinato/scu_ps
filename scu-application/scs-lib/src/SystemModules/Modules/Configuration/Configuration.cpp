/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Configuration.h"

namespace SystemModules::Modules::Configuration {

const char Configuration::kConfiguration[] = "Scu";

Configuration::Configuration(
  Logger::ILogger *logger,
  const std::string &iniFilePath)
  : ::Configuration::ConfigContainer(
  kConfiguration) {
  (void) logger;
  (void) iniFilePath;
  // Common
  auto common = std::make_unique<ConfigurationCommon>(
    logger);
  common_ = common.get();
  AddComponent(std::move(common));
  // Serializer
  auto serializer = std::make_unique<ConfigurationSerializer>(
    logger,
    iniFilePath);
  serializer_ = serializer.get();
  AddComponent(std::move(serializer));
  // Power supply
  auto powerSupply = std::make_unique<ConfigurationPowerSupply>(
    logger,
    iniFilePath);
  powerSupply_ = powerSupply.get();
  AddComponent(std::move(powerSupply));
  // Fan unit
  auto fanUnit = std::make_unique<ConfigurationFanUnit>(
    logger,
    iniFilePath);
  fanUnit_ = fanUnit.get();
  AddComponent(std::move(fanUnit));
  // Slots
  auto okNokAddress = Serializer()->Address()->Base()->OKNOK()->Value();
  auto ndsAddress = Serializer()->Address()->Base()->SMF()->Value();
  for (uint32_t i = 0; i < 12; i++) {
    auto mezzanineCard = std::make_unique<Configuration_Slots>(
      logger,
      i + 1,
      iniFilePath,
      Serializer()->Address()->Base()->MC()->Value(),
      Serializer()->Address()->Offset()->MC()->Value(),
      &okNokAddress,
      &ndsAddress);
    slots_.at(i) = mezzanineCard.get();
    AddComponent(std::move(mezzanineCard));
  }
  // Mezzanine cards
  auto modules = std::make_unique<Modules::Configuration_Modules>();
  modules_ = modules.get();
  AddComponent(std::move(modules));
}

ConfigurationCommon * Configuration::Common() const {
  return common_;
}

ConfigurationSerializer *Configuration::Serializer() const {
  return serializer_;
}

ConfigurationPowerSupply *Configuration::PowerSupply() const {
  return powerSupply_;
}

ConfigurationFanUnit *Configuration::FanUnit() const {
  return fanUnit_;
}

std::array<Configuration_Slots *, 12> Configuration::Slots() const {
  return slots_;
}

Modules::Configuration_Modules *Configuration::Modules() const {
  return modules_;
}

}  // namespace SystemModules::Modules::Configuration
