/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Logger/ILogger.h"
#include "Configuration/ConfigArray.h"
#include "Configuration/ConfigKeyValue.h"

namespace SystemModules::Modules::Configuration {
class ConfigurationMezzanineCardScuAddress : public ::Configuration::ConfigArray {
 public:
  ConfigurationMezzanineCardScuAddress(
    Logger::ILogger *logger,
    const uint64_t &mcAddress);

  ::Configuration::ConfigKeyValue<uint64_t> *ReadWriteRegister1Address() const;

  ::Configuration::ConfigKeyValue<uint64_t> *ReadWriteRegister2Address() const;

  ::Configuration::ConfigKeyValue<uint64_t> *ReadWriteRegister3Address() const;

  ::Configuration::ConfigKeyValue<uint64_t> *StatusRegister1Address() const;

  ::Configuration::ConfigKeyValue<uint64_t> *StatusRegister2Address() const;

  ::Configuration::ConfigKeyValue<uint64_t> *StatusRegister3Address() const;

  ::Configuration::ConfigKeyValue<uint64_t> *StatusRegister4Address() const;

  ::Configuration::ConfigKeyValue<uint64_t> *StatusRegister5Address() const;

  ::Configuration::ConfigKeyValue<uint64_t> *StatusRegister6Address() const;

  ::Configuration::ConfigKeyValue<uint64_t> *StatusRegister7Address() const;

  ::Configuration::ConfigKeyValue<uint64_t> *StatusRegister8Address() const;

  ::Configuration::ConfigKeyValue<uint64_t> *StatusRegister9Address() const;

  ::Configuration::ConfigKeyValue<uint64_t> *StatusRegister10Address() const;

  ::Configuration::ConfigKeyValue<uint64_t> *StatusRegister11Address() const;

  ::Configuration::ConfigKeyValue<uint64_t> *StatusRegister12Address() const;

  ::Configuration::ConfigKeyValue<uint64_t> *StatusRegister13Address() const;

  ::Configuration::ConfigKeyValue<uint64_t> *StatusRegister14Address() const;

  ::Configuration::ConfigKeyValue<uint64_t> *StatusRegister15Address() const;

  ::Configuration::ConfigKeyValue<uint64_t> *ClearRegister1Address() const;

 private:
  ::Configuration::ConfigKeyValue<uint64_t> *readWriteRegisterAddress1_;

  ::Configuration::ConfigKeyValue<uint64_t> *readWriteRegisterAddress2_;

  ::Configuration::ConfigKeyValue<uint64_t> *readWriteRegisterAddress3_;

  ::Configuration::ConfigKeyValue<uint64_t> *statusRegisterAddress1_;

  ::Configuration::ConfigKeyValue<uint64_t> *statusRegisterAddress2_;

  ::Configuration::ConfigKeyValue<uint64_t> *statusRegisterAddress3_;

  ::Configuration::ConfigKeyValue<uint64_t> *statusRegisterAddress4_;

  ::Configuration::ConfigKeyValue<uint64_t> *statusRegisterAddress5_;

  ::Configuration::ConfigKeyValue<uint64_t> *statusRegisterAddress6_;

  ::Configuration::ConfigKeyValue<uint64_t> *statusRegisterAddress7_;

  ::Configuration::ConfigKeyValue<uint64_t> *statusRegisterAddress8_;

  ::Configuration::ConfigKeyValue<uint64_t> *statusRegisterAddress9_;

  ::Configuration::ConfigKeyValue<uint64_t> *statusRegisterAddress10_;

  ::Configuration::ConfigKeyValue<uint64_t> *statusRegisterAddress11_;

  ::Configuration::ConfigKeyValue<uint64_t> *statusRegisterAddress12_;

  ::Configuration::ConfigKeyValue<uint64_t> *statusRegisterAddress13_;

  ::Configuration::ConfigKeyValue<uint64_t> *statusRegisterAddress14_;

  ::Configuration::ConfigKeyValue<uint64_t> *statusRegisterAddress15_;

  ::Configuration::ConfigKeyValue<uint64_t> *clearRegisterAddress1_;

  static const char kName[];

  static const char kReadWriteRegisterAddressKey[];
  static const uint64_t kMezzanineCardReadWriteRegister1Address;
  static const uint64_t kMezzanineCardReadWriteRegister2Address;
  static const uint64_t kMezzanineCardReadWriteRegister3Address;

  static const char kStatusRegisterAddressKey[];
  static const uint64_t kMezzanineCardStatusRegister1Address;
  static const uint64_t kMezzanineCardStatusRegister2Address;
  static const uint64_t kMezzanineCardStatusRegister3Address;
  static const uint64_t kMezzanineCardStatusRegister4Address;
  static const uint64_t kMezzanineCardStatusRegister5Address;
  static const uint64_t kMezzanineCardStatusRegister6Address;
  static const uint64_t kMezzanineCardStatusRegister7Address;
  static const uint64_t kMezzanineCardStatusRegister8Address;
  static const uint64_t kMezzanineCardStatusRegister9Address;
  static const uint64_t kMezzanineCardStatusRegister10Address;
  static const uint64_t kMezzanineCardStatusRegister11Address;
  static const uint64_t kMezzanineCardStatusRegister12Address;
  static const uint64_t kMezzanineCardStatusRegister13Address;
  static const uint64_t kMezzanineCardStatusRegister14Address;
  static const uint64_t kMezzanineCardStatusRegister15Address;

  static const char kClearRegisterAddressKey[];
  static const uint64_t kMezzanineCardClearRegister1Address;
};
}  // namespace SystemModules::Modules::Configuration
