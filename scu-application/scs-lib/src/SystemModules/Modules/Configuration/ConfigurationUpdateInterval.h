/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Logger/ILogger.h"
#include "Configuration/ConfigArray.h"
#include "Configuration/ConfigKeyValue.h"

namespace SystemModules::Modules::Configuration {
class ConfigurationUpdateInterval : public ::Configuration::ConfigArray {
 public:
  explicit ConfigurationUpdateInterval(
    Logger::ILogger *logger);

  [[nodiscard]] ::Configuration::ConfigKeyValue<uint64_t> *VerySlow() const;

  [[nodiscard]] ::Configuration::ConfigKeyValue<uint64_t> *Slow() const;

  [[nodiscard]] ::Configuration::ConfigKeyValue<uint64_t> *Medium() const;

  [[nodiscard]] ::Configuration::ConfigKeyValue<uint64_t> *Fast() const;

 private:
  ::Configuration::ConfigKeyValue<uint64_t> *verySlow_;

  ::Configuration::ConfigKeyValue<uint64_t> *slow_;

  ::Configuration::ConfigKeyValue<uint64_t> *medium_;

  ::Configuration::ConfigKeyValue<uint64_t> *fast_;

  static const char kName[];

  static const char kVerySlowName[];
  static const uint64_t kVerySlowValue;

  static const char kSlowName[];
  static const uint64_t kSlowValue;

  static const char kMediumName[];
  static const uint64_t kMediumValue;

  static const char kFastName[];
  static const uint64_t kFastValue;
};
}  // namespace SystemModules::Modules::Configuration
