/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Configuration/Builder/ConfigurationBuilder.h"
#include "ConfigurationUpdateInterval.h"

namespace SystemModules::Modules::Configuration {

const char ConfigurationUpdateInterval::kName[] = "UpdateInterval";

const char ConfigurationUpdateInterval::kVerySlowName[] = "VerySlow";
const uint64_t ConfigurationUpdateInterval::kVerySlowValue = 10000;

const char ConfigurationUpdateInterval::kSlowName[] = "Slow";
const uint64_t ConfigurationUpdateInterval::kSlowValue = 1000;

const char ConfigurationUpdateInterval::kMediumName[] = "Medium";
const uint64_t ConfigurationUpdateInterval::kMediumValue = 200;

const char ConfigurationUpdateInterval::kFastName[] = "Fast";
const uint64_t ConfigurationUpdateInterval::kFastValue = 10;

ConfigurationUpdateInterval::ConfigurationUpdateInterval(
  Logger::ILogger *logger)
  : ::Configuration::ConfigArray(kName) {
  verySlow_ = Add().KeyValue(
    kVerySlowName,
    kVerySlowValue);
  slow_ = Add().KeyValue(
    kSlowName,
    kSlowValue);
  medium_ = Add().KeyValue(
    kMediumName,
    kMediumValue);
  fast_ = Add().KeyValue(
    kFastName,
    kFastValue);
}

::Configuration::ConfigKeyValue<uint64_t> *ConfigurationUpdateInterval::VerySlow() const {
  return verySlow_;
}

::Configuration::ConfigKeyValue<uint64_t> *ConfigurationUpdateInterval::Slow() const {
  return slow_;
}

::Configuration::ConfigKeyValue<uint64_t> *ConfigurationUpdateInterval::Medium() const {
  return medium_;
}

::Configuration::ConfigKeyValue<uint64_t> *ConfigurationUpdateInterval::Fast() const {
  return fast_;
}

}  // namespace SystemModules::Modules::Configuration
