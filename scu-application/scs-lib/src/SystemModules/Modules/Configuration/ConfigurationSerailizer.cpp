/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <SystemModules/Framework/Accessors/AccessorGPIO.h>
#include "Configuration/Builder/ConfigurationBuilder.h"
#include "ConfigurationSerailizer.h"

namespace SystemModules::Modules::Configuration {

const char ConfigurationSerializer::kName[] = "Serializer";

const char ConfigurationSerializer::kSlotAName[] = "A";
const char ConfigurationSerializer::kSlotBName[] = "B";

const char ConfigurationSerializer::kDetectedSlotName[] = "DetectedSlot";
const uint32_t ConfigurationSerializer::kDetectedSlotId = 499;

ConfigurationSerializer::ConfigurationSerializer(
  Logger::ILogger *logger,
  const std::string &iniFilePath)
  : ::Configuration::ConfigContainer(
  kName) {
  // Set slot
  auto reg = SystemModules::Framework::Accessors::AccessorGPIO::Read(
    logger,
    kDetectedSlotId,
    false);
  std::string slot;
  if (!reg) {
    logger->Error("Failed to read configuration");
    throw std::exception();
  }
  if (!reg.value()) {
    slot = "A";
  } else {
    slot = "B";
  }
  detectedSlot_ = Add().KeyValue(
    kDetectedSlotName,
    slot);
  // Slot A
  auto slotA = std::make_unique<ConfigurationSerializerSlot>(
    logger,
    kSlotAName,
    iniFilePath);
  slotA_ = slotA.get();
  AddComponent(std::move(slotA));
  // Slot B
  auto slotB = std::make_unique<ConfigurationSerializerSlot>(
    logger,
    kSlotBName,
    iniFilePath);
  slotB_ = slotB.get();
  AddComponent(std::move(slotB));
  // Address
  auto address = std::make_unique<ConfigurationSerializerAddress>(
    logger);
  address_ = address.get();
  AddComponent(std::move(address));
  // Software
  auto software = std::make_unique<ConfigurationSerializerSoftware>(
    logger);
  software_ = software.get();
  AddComponent(std::move(software));
}

::Configuration::ConfigKeyValue<std::string> *ConfigurationSerializer::DetectedSlot() const {
  return detectedSlot_;
}

ConfigurationSerializerSlot *ConfigurationSerializer::SlotA() const {
  return slotA_;
}

ConfigurationSerializerSlot *ConfigurationSerializer::SlotB() const {
  return slotB_;
}

ConfigurationSerializerAddress *ConfigurationSerializer::Address() const {
  return address_;
}

ConfigurationSerializerSoftware *ConfigurationSerializer::Software() const {
  return software_;
}

}  // namespace SystemModules::Modules::Configuration
