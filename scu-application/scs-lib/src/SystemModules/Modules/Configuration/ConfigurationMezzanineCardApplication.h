/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Logger/ILogger.h"
#include "Configuration/ConfigArray.h"

namespace SystemModules::Modules::Configuration {
class ConfigurationMezzanineCardApplication : public ::Configuration::ConfigArray {
 public:
  ConfigurationMezzanineCardApplication(
    Logger::ILogger *logger,
    const uint32_t &slot,
    const std::string &iniFilePath);

  [[nodiscard]] ::Configuration::ConfigKeyValue<bool> *DoNotActOnBI() const;

 private:
  ::Configuration::ConfigKeyValue<bool> *doNotActOnBI_;

  static const char kName[];

  static const char kDoNotActOnBIName[];

  static const char kApplicationSection[];
  static const char kDoNotActOnBIKey[];
};
}  // namespace SystemModules::Modules::Configuration
