/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Logger/ILogger.h"
#include "Configuration/ConfigContainer.h"
#include "ConfigurationUpdateInterval.h"
#include "ConfigurationCommonSerializer.h"

namespace SystemModules::Modules::Configuration {
class ConfigurationCommon : public ::Configuration::ConfigContainer {
 public:
  explicit ConfigurationCommon(
    Logger::ILogger * logger);

  [[nodiscard]] ConfigurationUpdateInterval *UpdateInterval() const;

  [[nodiscard]] ConfigurationCommonSerializer *Serializer() const;

 private:
  ConfigurationUpdateInterval *updateInterval_;

  ConfigurationCommonSerializer * serializer_;

  static const char kName[];
};
}  // namespace SystemModules::Modules::Configuration
