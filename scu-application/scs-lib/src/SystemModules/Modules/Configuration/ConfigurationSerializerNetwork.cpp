/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <minini/minIni.h>
#include <SystemModules/Framework/Accessors/AccessorGPIO.h>
#include "Configuration/Builder/ConfigurationBuilder.h"
#include "ConfigurationSerializerNetwork.h"

namespace SystemModules::Modules::Configuration {

const char ConfigurationSerializerNetwork::kInterface0Name[] = "Eth0";
const char ConfigurationSerializerNetwork::kInterface1Name[] = "Eth1";

const char ConfigurationSerializerNetwork::kNetworkName[] = "Network";

const char ConfigurationSerializerNetwork::kHostnameKey[] = "Hostname";

const char ConfigurationSerializerNetwork::kSerializerSection[] = "FBIS-SCU-SER";

const char ConfigurationSerializerNetwork::kHostnameAKey[] = "SER_A_NAME";
const char ConfigurationSerializerNetwork::kHostnameADefaultValue[] = "SCS1600_A";

const char ConfigurationSerializerNetwork::kHostnameBKey[] = "SER_B_NAME";
const char ConfigurationSerializerNetwork::kHostnameBDefaultValue[] = "SCS1600_B";

const char ConfigurationSerializerNetwork::kMacAddressA0Key[] = "MAC_ADDR_A";
const char ConfigurationSerializerNetwork::kIpAddressA0Key[] = "TCPIP_ADDR_A";

const char ConfigurationSerializerNetwork::kMacAddressB0Key[] = "MAC_ADDR_B";
const char ConfigurationSerializerNetwork::kIpAddressB0Key[] = "TCPIP_ADDR_B";

const char ConfigurationSerializerNetwork::kNetmaskKey[] = "SER_NETMASK";

const char ConfigurationSerializerNetwork::kMacAddressA1[] = "00:0A:35:00:23:11";
const char ConfigurationSerializerNetwork::kIpAddressA1[] = "192.168.1.200";

const char ConfigurationSerializerNetwork::kMacAddressB1[] = "00:0A:35:00:23:12";
const char ConfigurationSerializerNetwork::kIpAddressB1[] = "192.168.1.201";

const char ConfigurationSerializerNetwork::kNetMask1[] = "255.255.255.0";


ConfigurationSerializerNetwork::ConfigurationSerializerNetwork(
  Logger::ILogger *logger,
  const std::string &slotName,
  const std::string &iniFilePath)
  : ::Configuration::ConfigContainer(
  kNetworkName) {
  std::string hostname;
  std::string macAddress0;
  std::string ipAddress0;
  std::string netMask0;
  std::string macAddress1;
  std::string ipAddress1;
  std::string netMask1;

  // Open ini file
  auto iniFile = minIni(iniFilePath);
  if (slotName == "A") {
    hostname = iniFile.gets(
      kSerializerSection,
      kHostnameAKey,
      kHostnameADefaultValue);

    macAddress0 = iniFile.gets(
      kSerializerSection,
      kMacAddressA0Key);
    ipAddress0 = iniFile.gets(
      kSerializerSection,
      kIpAddressA0Key);
    netMask0 = iniFile.gets(
      kSerializerSection,
        kNetmaskKey);

    macAddress1 = kMacAddressA1;
    ipAddress1 = kIpAddressA1;
    netMask1 = kNetMask1;
  } else {
    hostname = iniFile.gets(
      kSerializerSection,
      kHostnameBKey,
      kHostnameBDefaultValue);

    macAddress0 = iniFile.gets(
      kSerializerSection,
      kMacAddressB0Key);
    ipAddress0 = iniFile.gets(
      kSerializerSection,
      kIpAddressB0Key);
    netMask0 = iniFile.gets(
      kSerializerSection,
      kNetmaskKey);

    macAddress1 = kMacAddressB1;
    ipAddress1 = kIpAddressB1;
    netMask1 = kNetMask1;
  }
  // Hostname
  hostname_ = Add().KeyValue(
    kHostnameKey,
    hostname);
  // Interface 0
  auto interface0 = std::make_unique<ConfigurationSerializerNetworkInterface>(
    logger,
    kInterface0Name,
    macAddress0,
    ipAddress0,
    netMask0);
  interface0_ = interface0.get();
  AddComponent(std::move(interface0));
  // Interface 1
  auto interface1 = std::make_unique<ConfigurationSerializerNetworkInterface>(
    logger,
    kInterface1Name,
    macAddress1,
    ipAddress1,
    netMask1);
  interface1_ = interface1.get();
  AddComponent(std::move(interface1));
}

::Configuration::ConfigKeyValue<std::string> *ConfigurationSerializerNetwork::Hostname() const {
  return hostname_;
}

ConfigurationSerializerNetworkInterface *ConfigurationSerializerNetwork::Interface0() const {
  return interface0_;
}

ConfigurationSerializerNetworkInterface *ConfigurationSerializerNetwork::Interface1() const {
  return interface1_;
}

}  // namespace SystemModules::Modules::Configuration
