/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Configuration/Builder/ConfigurationBuilder.h"
#include "ConfigurationMezzanineCardScuAddress.h"

namespace SystemModules::Modules::Configuration {

const char ConfigurationMezzanineCardScuAddress::kName[] = "Address";

const char ConfigurationMezzanineCardScuAddress::kReadWriteRegisterAddressKey[] = "ReadWriteRegisterAddress";
const uint64_t ConfigurationMezzanineCardScuAddress::kMezzanineCardReadWriteRegister1Address = 0x08;
const uint64_t ConfigurationMezzanineCardScuAddress::kMezzanineCardReadWriteRegister2Address = 0x0C;
const uint64_t ConfigurationMezzanineCardScuAddress::kMezzanineCardReadWriteRegister3Address = 0x10;

const char ConfigurationMezzanineCardScuAddress::kStatusRegisterAddressKey[] = "StatusRegisterAddress";
const uint64_t ConfigurationMezzanineCardScuAddress::kMezzanineCardStatusRegister1Address = 0x40;
const uint64_t ConfigurationMezzanineCardScuAddress::kMezzanineCardStatusRegister2Address = 0x44;
const uint64_t ConfigurationMezzanineCardScuAddress::kMezzanineCardStatusRegister3Address = 0x48;
const uint64_t ConfigurationMezzanineCardScuAddress::kMezzanineCardStatusRegister4Address = 0x4C;
const uint64_t ConfigurationMezzanineCardScuAddress::kMezzanineCardStatusRegister5Address = 0x50;
const uint64_t ConfigurationMezzanineCardScuAddress::kMezzanineCardStatusRegister6Address = 0x54;
const uint64_t ConfigurationMezzanineCardScuAddress::kMezzanineCardStatusRegister7Address = 0x58;
const uint64_t ConfigurationMezzanineCardScuAddress::kMezzanineCardStatusRegister8Address = 0x5C;
const uint64_t ConfigurationMezzanineCardScuAddress::kMezzanineCardStatusRegister9Address = 0x60;
const uint64_t ConfigurationMezzanineCardScuAddress::kMezzanineCardStatusRegister10Address = 0x64;
const uint64_t ConfigurationMezzanineCardScuAddress::kMezzanineCardStatusRegister11Address = 0x68;
const uint64_t ConfigurationMezzanineCardScuAddress::kMezzanineCardStatusRegister12Address = 0x6C;
const uint64_t ConfigurationMezzanineCardScuAddress::kMezzanineCardStatusRegister13Address = 0x70;
const uint64_t ConfigurationMezzanineCardScuAddress::kMezzanineCardStatusRegister14Address = 0x74;
const uint64_t ConfigurationMezzanineCardScuAddress::kMezzanineCardStatusRegister15Address = 0x78;

const char ConfigurationMezzanineCardScuAddress::kClearRegisterAddressKey[] = "ClearRegisterAddress";
const uint64_t ConfigurationMezzanineCardScuAddress::kMezzanineCardClearRegister1Address = 0x40;

ConfigurationMezzanineCardScuAddress::ConfigurationMezzanineCardScuAddress(
  Logger::ILogger *logger,
  const uint64_t &mcAddress)
  : ConfigArray(kName) {
  // Read write register address 1
  readWriteRegisterAddress1_ = Add().KeyValue(
    std::string(kReadWriteRegisterAddressKey).append("1"),
    mcAddress + kMezzanineCardReadWriteRegister1Address);

  // Read write register address 2
  readWriteRegisterAddress2_ = Add().KeyValue(
    std::string(kReadWriteRegisterAddressKey).append("2"),
    mcAddress + kMezzanineCardReadWriteRegister2Address);

  // Read write register address 3
  readWriteRegisterAddress3_ = Add().KeyValue(
    std::string(kReadWriteRegisterAddressKey).append("3"),
    mcAddress + kMezzanineCardReadWriteRegister3Address);

  // Status register address 1
  statusRegisterAddress1_ = Add().KeyValue(
    std::string(kStatusRegisterAddressKey).append("1"),
    mcAddress + kMezzanineCardStatusRegister1Address);

  // Status register address 2
  statusRegisterAddress2_ = Add().KeyValue(
    std::string(kStatusRegisterAddressKey).append("2"),
    mcAddress + kMezzanineCardStatusRegister2Address);

  // Status register address 3
  statusRegisterAddress3_ = Add().KeyValue(
    std::string(kStatusRegisterAddressKey).append("3"),
    mcAddress + kMezzanineCardStatusRegister3Address);

  // Status register address 4
  statusRegisterAddress4_ = Add().KeyValue(
    std::string(kStatusRegisterAddressKey).append("4"),
    mcAddress + kMezzanineCardStatusRegister4Address);

  // Status register address 5
  statusRegisterAddress5_ = Add().KeyValue(
    std::string(kStatusRegisterAddressKey).append("5"),
    mcAddress + kMezzanineCardStatusRegister5Address);

  // Status register address 6
  statusRegisterAddress6_ = Add().KeyValue(
    std::string(kStatusRegisterAddressKey).append("6"),
    mcAddress + kMezzanineCardStatusRegister6Address);

  // Status register address 7
  statusRegisterAddress7_ = Add().KeyValue(
    std::string(kStatusRegisterAddressKey).append("7"),
    mcAddress + kMezzanineCardStatusRegister7Address);

  // Status register address 8
  statusRegisterAddress8_ = Add().KeyValue(
    std::string(kStatusRegisterAddressKey).append("8"),
    mcAddress + kMezzanineCardStatusRegister8Address);

  // Status register address 9
  statusRegisterAddress9_ = Add().KeyValue(
    std::string(kStatusRegisterAddressKey).append("9"),
    mcAddress + kMezzanineCardStatusRegister9Address);

  // Status register address 10
  statusRegisterAddress10_ = Add().KeyValue(
    std::string(kStatusRegisterAddressKey).append("10"),
    mcAddress + kMezzanineCardStatusRegister10Address);

  // Status register address 11
  statusRegisterAddress11_ = Add().KeyValue(
    std::string(kStatusRegisterAddressKey).append("11"),
    mcAddress + kMezzanineCardStatusRegister11Address);

  // Status register address 12
  statusRegisterAddress12_ = Add().KeyValue(
    std::string(kStatusRegisterAddressKey).append("12"),
    mcAddress + kMezzanineCardStatusRegister12Address);

  // Status register address 13
  statusRegisterAddress13_ = Add().KeyValue(
    std::string(kStatusRegisterAddressKey).append("13"),
    mcAddress + kMezzanineCardStatusRegister13Address);

  // Status register address 14
  statusRegisterAddress14_ = Add().KeyValue(
    std::string(kStatusRegisterAddressKey).append("14"),
    mcAddress + kMezzanineCardStatusRegister14Address);

  // Status register address 15
  statusRegisterAddress15_ = Add().KeyValue(
    std::string(kStatusRegisterAddressKey).append("15"),
    mcAddress + kMezzanineCardStatusRegister15Address);

  // Clear register address 1
  clearRegisterAddress1_ = Add().KeyValue(
    std::string(kClearRegisterAddressKey).append("1"),
    mcAddress + kMezzanineCardClearRegister1Address);
}

::Configuration::ConfigKeyValue<uint64_t> *ConfigurationMezzanineCardScuAddress::ReadWriteRegister1Address() const {
  return readWriteRegisterAddress1_;
}

::Configuration::ConfigKeyValue<uint64_t> *ConfigurationMezzanineCardScuAddress::ReadWriteRegister2Address() const {
  return readWriteRegisterAddress2_;
}

::Configuration::ConfigKeyValue<uint64_t> *ConfigurationMezzanineCardScuAddress::ReadWriteRegister3Address() const {
  return readWriteRegisterAddress3_;
}

::Configuration::ConfigKeyValue<uint64_t> *ConfigurationMezzanineCardScuAddress::StatusRegister1Address() const {
  return statusRegisterAddress1_;
}

::Configuration::ConfigKeyValue<uint64_t> *ConfigurationMezzanineCardScuAddress::StatusRegister2Address() const {
  return statusRegisterAddress2_;
}

::Configuration::ConfigKeyValue<uint64_t> *ConfigurationMezzanineCardScuAddress::StatusRegister3Address() const {
  return statusRegisterAddress3_;
}

::Configuration::ConfigKeyValue<uint64_t> *ConfigurationMezzanineCardScuAddress::StatusRegister4Address() const {
  return statusRegisterAddress4_;
}

::Configuration::ConfigKeyValue<uint64_t> *ConfigurationMezzanineCardScuAddress::StatusRegister5Address() const {
  return statusRegisterAddress5_;
}

::Configuration::ConfigKeyValue<uint64_t> *ConfigurationMezzanineCardScuAddress::StatusRegister6Address() const {
  return statusRegisterAddress6_;
}

::Configuration::ConfigKeyValue<uint64_t> *ConfigurationMezzanineCardScuAddress::StatusRegister7Address() const {
  return statusRegisterAddress7_;
}

::Configuration::ConfigKeyValue<uint64_t> *ConfigurationMezzanineCardScuAddress::StatusRegister8Address() const {
  return statusRegisterAddress8_;
}

::Configuration::ConfigKeyValue<uint64_t> *ConfigurationMezzanineCardScuAddress::StatusRegister9Address() const {
  return statusRegisterAddress9_;
}

::Configuration::ConfigKeyValue<uint64_t> *ConfigurationMezzanineCardScuAddress::StatusRegister10Address() const {
  return statusRegisterAddress10_;
}

::Configuration::ConfigKeyValue<uint64_t> *ConfigurationMezzanineCardScuAddress::StatusRegister11Address() const {
  return statusRegisterAddress11_;
}

::Configuration::ConfigKeyValue<uint64_t> *ConfigurationMezzanineCardScuAddress::StatusRegister12Address() const {
  return statusRegisterAddress12_;
}

::Configuration::ConfigKeyValue<uint64_t> *ConfigurationMezzanineCardScuAddress::StatusRegister13Address() const {
  return statusRegisterAddress13_;
}

::Configuration::ConfigKeyValue<uint64_t> *ConfigurationMezzanineCardScuAddress::StatusRegister14Address() const {
  return statusRegisterAddress14_;
}

::Configuration::ConfigKeyValue<uint64_t> *ConfigurationMezzanineCardScuAddress::StatusRegister15Address() const {
  return statusRegisterAddress15_;
}

::Configuration::ConfigKeyValue<uint64_t> *ConfigurationMezzanineCardScuAddress::ClearRegister1Address() const {
  return clearRegisterAddress1_;
}

}  // namespace SystemModules::Modules::Configuration
