/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Logger/ILogger.h"
#include "Configuration/ConfigContainer.h"
#include "Configuration/ConfigItem.h"

namespace SystemModules::Modules::Configuration {
class ConfigurationFanUnit : public ::Configuration::ConfigContainer {
 public:
  ConfigurationFanUnit(
    Logger::ILogger *logger,
    const std::string &iniFilePath);

  [[nodiscard]] ::Configuration::ConfigItem<uint8_t> *FanSpeed() const;

  [[nodiscard]] ::Configuration::ConfigItem<bool> *FanAEnable() const;

  [[nodiscard]] ::Configuration::ConfigItem<bool> *FanBEnable() const;

  [[nodiscard]] ::Configuration::ConfigItem<bool> *FanMEnable() const;

 private:
  ::Configuration::ConfigItem<uint8_t> *fanSpeed_;

  ::Configuration::ConfigItem<bool> *fanAEnable_;

  ::Configuration::ConfigItem<bool> *fanBEnable_;

  ::Configuration::ConfigItem<bool> *fanMEnable_;

  static const char kSourceIniFile[];

  static const char kIniFileFanUnitSection[];
  static const char kIniFileFanSpeedKey[];
  static const char kIniFileFanAEnableKey[];
  static const char kIniFileFanBEnableKey[];
  static const char kIniFileFanMEnableKey[];

  static const char kFanUnit[];
  static const char kFanSpeedKey[];
  static const char kFanAEnableKey[];
  static const char kFanBEnableKey[];
  static const char kFanMEnableKey[];
};

}  // namespace SystemModules::Modules::Configuration
