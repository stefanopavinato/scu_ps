/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Logger/ILogger.h"
#include "Configuration/ConfigContainer.h"
#include "ConfigurationCommon.h"
#include "ConfigurationSerailizer.h"
#include "ConfigurationPowerSupply.h"
#include "ConfigurationFanUnit.h"
#include "Slots/Configuration_Slots.h"
#include "Modules/Configuration_Modules.h"


namespace SystemModules::Modules::Configuration {
class Configuration : public ::Configuration::ConfigContainer {
 public:
  Configuration(
    Logger::ILogger *logger,
    const std::string &iniFilePath);

  [[nodiscard]] ConfigurationCommon * Common() const;

  [[nodiscard]] ConfigurationSerializer *Serializer() const;

  [[nodiscard]] ConfigurationPowerSupply *PowerSupply() const;

  [[nodiscard]] ConfigurationFanUnit *FanUnit() const;

  [[nodiscard]] std::array<Configuration_Slots *, 12> Slots() const;

  [[nodiscard]] Modules::Configuration_Modules *Modules() const;

 private:
  ConfigurationCommon *common_;

  ConfigurationSerializer *serializer_;

  ConfigurationPowerSupply *powerSupply_;

  ConfigurationFanUnit *fanUnit_;

  std::array<Configuration_Slots *, 12> slots_;

  Modules::Configuration_Modules *modules_;

  static const char kConfiguration[];
};

}  // namespace SystemModules::Modules::Configuration
