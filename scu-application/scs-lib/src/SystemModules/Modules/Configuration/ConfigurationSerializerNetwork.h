/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Logger/ILogger.h"
#include "Configuration/ConfigContainer.h"
#include "Configuration/ConfigItem.h"
#include "ConfigurationSerializerNetworkInterface.h"

namespace SystemModules::Modules::Configuration {
class ConfigurationSerializerNetwork : public ::Configuration::ConfigContainer {
 public:
  ConfigurationSerializerNetwork(
    Logger::ILogger *logger,
    const std::string &slotName,
    const std::string &iniFilePath);

  [[nodiscard]] ::Configuration::ConfigKeyValue<std::string> *Hostname() const;

  [[nodiscard]] ConfigurationSerializerNetworkInterface *Interface0() const;

  [[nodiscard]] ConfigurationSerializerNetworkInterface *Interface1() const;

 private:
  ConfigurationSerializerNetworkInterface *interface0_;

  ConfigurationSerializerNetworkInterface *interface1_;

  ::Configuration::ConfigKeyValue<std::string> *hostname_;

  static const char kInterface0Name[];
  static const char kInterface1Name[];

  static const char kNetworkName[];

  static const char kHostnameKey[];

  static const char kSerializerSection[];

  static const char kHostnameAKey[];
  static const char kHostnameADefaultValue[];

  static const char kHostnameBKey[];
  static const char kHostnameBDefaultValue[];

  static const char kMacAddressA0Key[];
  static const char kIpAddressA0Key[];
  static const char kMacAddressB0Key[];
  static const char kIpAddressB0Key[];
  static const char kNetmaskKey[];

  static const char kMacAddressA1[];
  static const char kIpAddressA1[];

  static const char kMacAddressB1[];
  static const char kIpAddressB1[];

  static const char kNetMask1[];
};
}  // namespace SystemModules::Modules::Configuration
