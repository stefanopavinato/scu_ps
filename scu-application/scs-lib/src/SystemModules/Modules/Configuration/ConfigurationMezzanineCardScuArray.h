/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Logger/ILogger.h"
#include "Configuration/ConfigArray.h"
#include "Configuration/ConfigScuRegisterArray.h"

namespace SystemModules::Modules::Configuration {
class ConfigurationMezzanineCardScuArray : public ::Configuration::ConfigArray {
 public:
  ConfigurationMezzanineCardScuArray(
    Logger::ILogger *logger,
    const uint32_t &slot,
    const uint64_t &mcAddress,
    uint64_t *okNokAddress,
    uint64_t *ndsAddress);

  [[nodiscard]] ::Configuration::ConfigScuRegisterArray *OkNok() const;

  [[nodiscard]] ::Configuration::ConfigScuRegisterArray *Nds() const;

  [[nodiscard]] ::Configuration::ConfigScuRegisterArray *UserData() const;

 private:
  ::Configuration::ConfigScuRegisterArray *okNok_;

  ::Configuration::ConfigScuRegisterArray *nds_;

  ::Configuration::ConfigScuRegisterArray *userData_;

  static const char kName[];

  static const char kOkNokKey[];
  static const uint64_t kNumberOfOkNokAddressOffset;

  static const char kNdsKey[];
  static const uint64_t kNumberOfNdsAddressOffset;

  static const char kUserDataKey[];
  static const uint64_t kNumberOfUserDataAddressOffset;
  static const uint64_t kUserDataAddressOffset;
};
}  // namespace SystemModules::Modules::Configuration
