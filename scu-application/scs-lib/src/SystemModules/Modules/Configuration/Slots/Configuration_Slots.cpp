/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Accessors/AccessorScuRegister.h"
#include "Configuration/Builder/ConfigurationBuilder.h"
#include "Configuration_Slots.h"

namespace SystemModules::Modules::Configuration {

const char Configuration_Slots::kMC[] = "MC";

const char Configuration_Slots::kSourceInternal[] = "Internal";

const char Configuration_Slots::kNameKey[] = "Name";

const char Configuration_Slots::kSlotKey[] = "Slot";

const char Configuration_Slots::kI2CPathBase[] = "/sys/bus/i2c/devices/";
const char Configuration_Slots::kI2CChannelSlot1[] = "0-0070/channel-5/";
const char Configuration_Slots::kI2CChannelSlot2[] = "0-0070/channel-4/";
const char Configuration_Slots::kI2CChannelSlot3[] = "0-0070/channel-3/";
const char Configuration_Slots::kI2CChannelSlot4[] = "0-0070/channel-6/";
const char Configuration_Slots::kI2CChannelSlot5[] = "0-0070/channel-7/";
const char Configuration_Slots::kI2CChannelSlot6[] = "0-0070/channel-2/";
const char Configuration_Slots::kI2CChannelSlot7[] = "0-0070/channel-1/";
const char Configuration_Slots::kI2CChannelSlot8[] = "0-0070/channel-0/";
const char Configuration_Slots::kI2CChannelSlot9[] = "0-0071/channel-0/";
const char Configuration_Slots::kI2CChannelSlot10[] = "0-0071/channel-1/";
const char Configuration_Slots::kI2CChannelSlot11[] = "0-0071/channel-2/";
const char Configuration_Slots::kI2CChannelSlot12[] = "0-0071/channel-3/";
const char *Configuration_Slots::kI2CChannels[] = {
  Configuration_Slots::kI2CChannelSlot1,
  Configuration_Slots::kI2CChannelSlot2,
  Configuration_Slots::kI2CChannelSlot3,
  Configuration_Slots::kI2CChannelSlot4,
  Configuration_Slots::kI2CChannelSlot5,
  Configuration_Slots::kI2CChannelSlot6,
  Configuration_Slots::kI2CChannelSlot7,
  Configuration_Slots::kI2CChannelSlot8,
  Configuration_Slots::kI2CChannelSlot9,
  Configuration_Slots::kI2CChannelSlot10,
  Configuration_Slots::kI2CChannelSlot11,
  Configuration_Slots::kI2CChannelSlot12};

const char Configuration_Slots::kPathI2CManagementKey[] = "PathI2CManagement";
const char Configuration_Slots::kPathI2CApplicationKey[] = "PathI2CApplication";
const char Configuration_Slots::kPathI2cApplicationDeviceKey[] = "PathI2CApplicationDevice";


Configuration_Slots::Configuration_Slots(
  Logger::ILogger *logger,
  const uint32_t &slot,
  const std::string &iniFilePath,
  const uint64_t &mcAddressBase,
  const uint64_t &mcAddressOffset,
  uint64_t *okNokAddress,
  uint64_t *ndsAddress)
  : ::Configuration::ConfigContainer(
  std::string(kMC).append(std::to_string(slot))) {
  auto mcAddress = mcAddressBase + (slot - 1) * mcAddressOffset;
  // Scu
  auto scu = std::make_unique<ConfigurationMezzanineCardScu>(
    logger,
    slot,
    mcAddress,
    okNokAddress,
    ndsAddress);
  scu_ = scu.get();
  AddComponent(std::move(scu));
  // Gpio
  auto gpio = std::make_unique<ConfigurationMezzanineCardGpio>(
    logger,
    slot);
  gpio_ = gpio.get();
  AddComponent(std::move(gpio));
  // Application
  auto application = std::make_unique<ConfigurationMezzanineCardApplication>(
    logger,
    slot,
    iniFilePath);
  application_ = application.get();
  AddComponent(std::move(application));
  // Name
  name_ = this->Add().Value(
    kNameKey,
    std::string(kMC).append(std::to_string(slot)),
    kSourceInternal);

  // Slot
  slot_ = this->Add().Value(
    kSlotKey,
    slot,
    kSourceInternal);
  // Path i2c management
  pathI2CManagement_ = this->Add().Value<std::string>(
    kPathI2CManagementKey,
    std::string(kI2CPathBase).append(kI2CChannels[slot - 1]),
    kSourceInternal);
  // Path i2c application
  pathI2CApplication_ = this->Add().Value<std::string>(
    kPathI2CApplicationKey,
    std::string(kI2CPathBase).append("i2c-").append(std::to_string(slot + 1)),
    kSourceInternal);
  // Path i2c device
  pathI2CApplicationDevice_ = this->Add().Value(
    kPathI2cApplicationDeviceKey,
    std::string(kI2CPathBase),
    kSourceInternal);
}

ConfigurationMezzanineCardScu *Configuration_Slots::Scu() const {
  return scu_;
}

ConfigurationMezzanineCardGpio *Configuration_Slots::Gpio() const {
  return gpio_;
}

ConfigurationMezzanineCardApplication *Configuration_Slots::Application() const {
  return application_;
}

::Configuration::ConfigItem<std::string> *Configuration_Slots::Name() const {
  return name_;
}

::Configuration::ConfigItem<uint32_t> *Configuration_Slots::Slot() const {
  return slot_;
}

::Configuration::ConfigItem<std::string> *Configuration_Slots::PathI2CManagement() const {
  return pathI2CManagement_;
}

::Configuration::ConfigItem<std::string> *Configuration_Slots::PathI2CApplication() const {
  return pathI2CApplication_;
}

::Configuration::ConfigItem<std::string> *Configuration_Slots::PathI2CApplicationDevice() const {
  return pathI2CApplicationDevice_;
}

}  // namespace SystemModules::Modules::Configuration
