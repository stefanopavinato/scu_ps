/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <array>
#include "Logger/ILogger.h"
#include "Configuration/ConfigContainer.h"
#include "Configuration/ConfigScuRegister.h"
#include "Configuration/ConfigItem.h"
#include "SystemModules/Modules/Configuration/ConfigurationMezzanineCardScuArray.h"
#include "SystemModules/Modules/Configuration/ConfigurationMezzanineCardScuValues.h"
#include "SystemModules/Modules/Configuration/ConfigurationMezzanineCardScuAddress.h"

namespace SystemModules::Modules::Configuration {
class ConfigurationMezzanineCardScu : public ::Configuration::ConfigContainer {
 public:
  ConfigurationMezzanineCardScu(
    Logger::ILogger *logger,
    const uint32_t &slot,
    const uint64_t &mcAddress,
    uint64_t *okNokAddress,
    uint64_t *ndsAddress);

  [[nodiscard]] ConfigurationMezzanineCardScuArray *Arrays() const;

  [[nodiscard]] ConfigurationMezzanineCardScuValues *Values() const;

  [[nodiscard]] ConfigurationMezzanineCardScuAddress *Address() const;

  [[nodiscard]] ::Configuration::ConfigItem<uint64_t> *SlotNumberAddress() const;

  [[nodiscard]] ::Configuration::ConfigItem<uint64_t> *ExpectedTypeAddress() const;

 private:
  ConfigurationMezzanineCardScuArray *arrays_;

  ConfigurationMezzanineCardScuValues *values_;

  ConfigurationMezzanineCardScuAddress *address_;

  ::Configuration::ConfigItem<uint64_t> *slotNumberAddress_;

  ::Configuration::ConfigItem<uint64_t> *expectedTypeAddress_;

  static const char kScu[];

  static const char kSourceInternal[];

  static const char kSlotNumberAddressKey[];
  static const uint64_t kSlotNumberAddress;

  static const char kExpectedTypeAddressKey[];
  static const uint64_t kExpectedTypeAddress;
};
}  // namespace SystemModules::Modules::Configuration
