/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Configuration/Builder/ConfigurationBuilder.h"
#include "ConfigurationMezzanineCardScu.h"

namespace SystemModules::Modules::Configuration {

const char ConfigurationMezzanineCardScu::kScu[] = "Scu";

const char ConfigurationMezzanineCardScu::kSourceInternal[] = "Internal";

const char ConfigurationMezzanineCardScu::kSlotNumberAddressKey[] = "SlotNumberAddress";
const uint64_t ConfigurationMezzanineCardScu::kSlotNumberAddress = 0x00;

const char ConfigurationMezzanineCardScu::kExpectedTypeAddressKey[] = "ExpectedTypeAddress";
const uint64_t ConfigurationMezzanineCardScu::kExpectedTypeAddress = 0x04;

ConfigurationMezzanineCardScu::ConfigurationMezzanineCardScu(
  Logger::ILogger *logger,
  const uint32_t &slot,
  const uint64_t &mcAddress,
  uint64_t *okNokAddress,
  uint64_t *ndsAddress)
  : ::Configuration::ConfigContainer(
  std::string(kScu).append(std::to_string(slot))) {
  // Scu array
  auto arrays = std::make_unique<ConfigurationMezzanineCardScuArray>(
    logger,
    slot,
    mcAddress,
    okNokAddress,
    ndsAddress);
  arrays_ = arrays.get();
  AddComponent(std::move(arrays));

  // Scu values
  auto values = std::make_unique<ConfigurationMezzanineCardScuValues>(
    logger,
    slot);
  values_ = values.get();
  AddComponent(std::move(values));

  // Scu address
  auto address = std::make_unique<ConfigurationMezzanineCardScuAddress>(
    logger,
    mcAddress);
  address_ = address.get();
  AddComponent(std::move(address));

  // Slot number address
  slotNumberAddress_ = this->Add().Value(
    kSlotNumberAddressKey,
      mcAddress + kSlotNumberAddress,
    kSourceInternal);

  // Expected card type address
  expectedTypeAddress_ = this->Add().Value(
    kExpectedTypeAddressKey,
      mcAddress + kExpectedTypeAddress,
    kSourceInternal);
}


ConfigurationMezzanineCardScuArray *ConfigurationMezzanineCardScu::Arrays() const {
  return arrays_;
}

ConfigurationMezzanineCardScuValues *ConfigurationMezzanineCardScu::Values() const {
  return values_;
}

ConfigurationMezzanineCardScuAddress *ConfigurationMezzanineCardScu::Address() const {
  return address_;
}

::Configuration::ConfigItem<uint64_t> *ConfigurationMezzanineCardScu::SlotNumberAddress() const {
  return slotNumberAddress_;
}

::Configuration::ConfigItem<uint64_t> *ConfigurationMezzanineCardScu::ExpectedTypeAddress() const {
  return expectedTypeAddress_;
}

}  // namespace SystemModules::Modules::Configuration
