/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Logger/ILogger.h"
#include "Configuration/ConfigArray.h"
#include "Configuration/ConfigGpio.h"

namespace SystemModules::Modules::Configuration {
class ConfigurationMezzanineCardGpio : public ::Configuration::ConfigArray {
 public:
  ConfigurationMezzanineCardGpio(
    Logger::ILogger *logger,
    const uint32_t &slot);

  [[nodiscard]] ::Configuration::ConfigGpio *Present() const;

  [[nodiscard]] ::Configuration::ConfigGpio *Enable() const;

  [[nodiscard]] ::Configuration::ConfigGpio *Reset() const;

 private:
  ::Configuration::ConfigGpio *present_;

  ::Configuration::ConfigGpio *enable_;

  ::Configuration::ConfigGpio *reset_;

  static const char kName[];

  static const char kPresentKey[];
  static const uint32_t kPresentAddressArray[];

  static const char kEnableKey[];
  static const uint32_t kEnableAddressArray[];

  static const char kResetKey[];
  static const uint32_t kResetAddressArray[];
};
}  // namespace SystemModules::Modules::Configuration
