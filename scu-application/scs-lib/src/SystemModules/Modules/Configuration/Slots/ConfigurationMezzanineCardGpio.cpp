/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Configuration/Builder/ConfigurationBuilder.h"
#include "ConfigurationMezzanineCardGpio.h"

namespace SystemModules::Modules::Configuration {

const char ConfigurationMezzanineCardGpio::kName[] = "Gpio";

const char ConfigurationMezzanineCardGpio::kPresentKey[] = "Present";
const uint32_t ConfigurationMezzanineCardGpio::kPresentAddressArray[] =
  {498, 502, 506, 510, 466, 470, 474, 478, 482, 486, 490, 494};

const char ConfigurationMezzanineCardGpio::kEnableKey[] = "Enable";
const uint32_t ConfigurationMezzanineCardGpio::kEnableAddressArray[] =
  {497, 501, 505, 509, 465, 469, 473, 477, 481, 485, 489, 493};

const char ConfigurationMezzanineCardGpio::kResetKey[] = "Reset";
const uint32_t ConfigurationMezzanineCardGpio::kResetAddressArray[] =
  {496, 500, 504, 508, 464, 468, 472, 476, 480, 484, 488, 492};

ConfigurationMezzanineCardGpio::ConfigurationMezzanineCardGpio(
  Logger::ILogger *logger,
  const uint32_t &slot)
  : ::Configuration::ConfigArray(
  kName) {
  // Present address
  present_ = this->Add().Gpio(
    kPresentKey,
    kPresentAddressArray[slot - 1],
    true);
  // Enable address
  enable_ = this->Add().Gpio(
    kEnableKey,
    kEnableAddressArray[slot - 1],
    false);
  // Reset address
  reset_ = this->Add().Gpio(
    kResetKey,
    kResetAddressArray[slot - 1],
    true);
}

::Configuration::ConfigGpio *ConfigurationMezzanineCardGpio::Present() const {
  return present_;
}

::Configuration::ConfigGpio *ConfigurationMezzanineCardGpio::Enable() const {
  return enable_;
}

::Configuration::ConfigGpio *ConfigurationMezzanineCardGpio::Reset() const {
  return reset_;
}

}  // namespace SystemModules::Modules::Configuration
