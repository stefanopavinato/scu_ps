/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Accessors/AccessorScuRegister.h"
#include "Configuration/Builder/ConfigurationBuilder.h"
#include "ConfigurationMezzanineCardScuArray.h"

namespace SystemModules::Modules::Configuration {

const char ConfigurationMezzanineCardScuArray::kName[] = "Arrays";

const char ConfigurationMezzanineCardScuArray::kOkNokKey[] = "OkNok";
const uint64_t ConfigurationMezzanineCardScuArray::kNumberOfOkNokAddressOffset = 0xF0;

const char ConfigurationMezzanineCardScuArray::kNdsKey[] = "Nds";
const uint64_t ConfigurationMezzanineCardScuArray::kNumberOfNdsAddressOffset = 0xF4;

const char ConfigurationMezzanineCardScuArray::kUserDataKey[] = "UserData";
const uint64_t ConfigurationMezzanineCardScuArray::kNumberOfUserDataAddressOffset = 0xF8;
const uint64_t ConfigurationMezzanineCardScuArray::kUserDataAddressOffset = 0x80;

ConfigurationMezzanineCardScuArray::ConfigurationMezzanineCardScuArray(
  Logger::ILogger *logger,
  const uint32_t &slot,
  const uint64_t &mcAddress,
  uint64_t *okNokAddress,
  uint64_t *ndsAddress)
  : ::Configuration::ConfigArray(
  kName) {
  // OK/NOK Signals
  {
    auto reg = SystemModules::Framework::Accessors::AccessorScuRegister<uint32_t>::Read(
      logger,
      mcAddress + kNumberOfOkNokAddressOffset,
      0,
      32);
    if (!reg) {
      throw std::exception();
    }
    okNok_ = this->Add().ScuRegisterArray(
      kOkNokKey,
      *okNokAddress,
      reg.value());
    *okNokAddress += (reg.value() * 4);
  }

  // NDS structures
  {
    auto reg = SystemModules::Framework::Accessors::AccessorScuRegister<uint32_t>::Read(
      logger,
      mcAddress + kNumberOfNdsAddressOffset,
      0,
      32);
    if (!reg) {
      throw std::exception();
    }
    nds_ = this->Add().ScuRegisterArray(
      kNdsKey,
      *ndsAddress,
      reg.value());
    *ndsAddress += (reg.value() * 4);
  }

  // UserData
  {
    auto reg = SystemModules::Framework::Accessors::AccessorScuRegister<uint32_t>::Read(
      logger,
      mcAddress + kNumberOfUserDataAddressOffset,
      0,
      32);
    if (!reg) {
      throw std::exception();
    }
    userData_ = this->Add().ScuRegisterArray(
      kUserDataKey,
      mcAddress + kUserDataAddressOffset,
      reg.value());
  }
}


::Configuration::ConfigScuRegisterArray *ConfigurationMezzanineCardScuArray::OkNok() const {
  return okNok_;
}

::Configuration::ConfigScuRegisterArray *ConfigurationMezzanineCardScuArray::Nds() const {
  return nds_;
}

::Configuration::ConfigScuRegisterArray *ConfigurationMezzanineCardScuArray::UserData() const {
  return userData_;
}

}  // namespace SystemModules::Modules::Configuration
