/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Logger/ILogger.h"
#include "Configuration/ConfigArray.h"
#include "Configuration/ConfigScuRegisterArray.h"

namespace SystemModules::Modules::Configuration {
class ConfigurationMezzanineCardScuValues : public ::Configuration::ConfigArray {
 public:
  ConfigurationMezzanineCardScuValues(
    Logger::ILogger *logger,
    const uint32_t &slot);

  [[nodiscard]] ::Configuration::ConfigScuRegister *DriverReset() const;

  [[nodiscard]] ::Configuration::ConfigScuRegister *DriverEnable() const;

 private:
  ::Configuration::ConfigScuRegister *driverReset_;

  ::Configuration::ConfigScuRegister *driverEnable_;

  static const char kName[];

  static const char kDriverEnableKey[];
  static const uint64_t kDriverEnableAddress;

  static const char kDriverResetKey[];
  static const uint64_t kDriverResetAddress;
};
}  // namespace SystemModules::Modules::Configuration
