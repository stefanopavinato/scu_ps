/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "SystemModules/Modules/Configuration/ConfigurationCommon.h"
#include "SystemModules/Modules/Configuration/ConfigurationFanUnit.h"
#include "SystemModules/Framework/Components/Container.h"

namespace SystemModules::Modules::FanUnit {
class FanUnit_CardInfo;

class FanUnit_Fan;

class FanUnit : public Framework::Components::Container {
 public:
  FanUnit(
    Logger::ILogger *logger,
    const Modules::Configuration::ConfigurationCommon &configCommon,
    const Configuration::ConfigurationFanUnit &config);

  [[nodiscard]] FanUnit_CardInfo *CardInfo() const;

  [[nodiscard]] FanUnit_Fan *FanA() const;

//  [[nodiscard]] FanUnit_Fan *FanM() const;

  [[nodiscard]] FanUnit_Fan *FanB() const;

 private:
  FanUnit_CardInfo *cardInfo_;

  FanUnit_Fan *fanA_;

  FanUnit_Fan *fanM_;

  FanUnit_Fan *fanB_;

  static const char kName[];
  static const char kDescription[];
  static const char kPath[];

  static const char kFanAName[];
  static const uint16_t kFanAAddress;

//  static const char kFanMName[];
//  static const uint16_t kFanMAddress;

  static const char kFanBName[];
  static const uint16_t kFanBAddress;
};
}  // namespace SystemModules::Modules::FanUnit
