/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Builder/Actions/ActionBuilder.h"
#include "SystemModules/Framework/Builder/Accessors/ReadAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Accessors/ReadWriteAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemInOutBuilder.h"
#include "SystemModules/Framework/Builder/Components/ParameterItemInBuilder.h"
#include "FanUnit_CardInfo.h"

namespace SystemModules::Modules::FanUnit {

const char FanUnit_CardInfo::kName[] = "EEProm";
const char FanUnit_CardInfo::kDescription[] = "Default management EEProm";
const uint16_t FanUnit_CardInfo::kAddress = 0x50;

const char FanUnit_CardInfo::kManufacturerName[] = "Manufacturer";
const char FanUnit_CardInfo::kManufacturerDescription[] = "";
const uint32_t FanUnit_CardInfo::kManufacturerAddress = 0x00;
const uint32_t FanUnit_CardInfo::kManufacturerSize = 32;

const char FanUnit_CardInfo::kBoardNameStringName[] = "BoardNameString";
const char FanUnit_CardInfo::kBoardNameName[] = "BoardName";
const char FanUnit_CardInfo::kBoardNameDescription[] = "";
const uint32_t FanUnit_CardInfo::kBoardNameAddress = 0x20;
const uint32_t FanUnit_CardInfo::kBoardNameSize = 16;

const char FanUnit_CardInfo::kBoardRevisionName[] = "BoardRevision";
const char FanUnit_CardInfo::kBoardRevisionDescription[] = "";
const uint32_t FanUnit_CardInfo::kBoardRevisionAddress = 0x30;
const uint32_t FanUnit_CardInfo::kBoardRevisionSize = 16;

const char FanUnit_CardInfo::kSerialNumberName[] = "SerialNumber";
const char FanUnit_CardInfo::kSerialNumberDescription[] = "";
const uint32_t FanUnit_CardInfo::kSerialNumberAddress = 0x40;
const uint32_t FanUnit_CardInfo::kSerialNumberSize = 16;

const char FanUnit_CardInfo::kOperatingHoursName[] = "OperatingHours";
const char FanUnit_CardInfo::kOperatingHoursDescription[] = "Operating hours of the FanUnit as a whole";
const uint32_t FanUnit_CardInfo::kOperatingHoursAddress = 0x60;
const uint32_t FanUnit_CardInfo::kOperatingHoursSize = 4;

const char FanUnit_CardInfo::kFanAOperatingHoursName[] = "FanAOperatingHours";
const char FanUnit_CardInfo::kFanAOperatingHoursDescription[] = "Operating hours of Fan A";
const uint32_t FanUnit_CardInfo::kFanAOperatingHoursAddress = 0x64;
const uint32_t FanUnit_CardInfo::kFanAOperatingHoursSize = 4;

const char FanUnit_CardInfo::kFanBOperatingHoursName[] = "FanBOperatingHours";
const char FanUnit_CardInfo::kFanBOperatingHoursDescription[] = "Operating hours of Fan B";
const uint32_t FanUnit_CardInfo::kFanBOperatingHoursAddress = 0x68;
const uint32_t FanUnit_CardInfo::kFanBOperatingHoursSize = 4;

// const char FanUnit_CardInfo::kFanMOperatingHoursName[] = "FanMOperatingHours";
// const char FanUnit_CardInfo::kFanMOperatingHoursDescription[] = "Operating hours of Fan M";
// const uint32_t FanUnit_CardInfo::kFanMOperatingHoursAddress = 0x6C;
// const uint32_t FanUnit_CardInfo::kFanMOperatingHoursSize = 4;

const char FanUnit_CardInfo::kMD5Name[] = "MD5";
const char FanUnit_CardInfo::kMD5Description[] = "";
const uint32_t FanUnit_CardInfo::kMD5Address = 0x80;
const uint32_t FanUnit_CardInfo::kMD5Size = 32;

const char FanUnit_CardInfo::kUUIDName[] = "UUID";
const char FanUnit_CardInfo::kUUIDDescription[] = "";
const uint32_t FanUnit_CardInfo::kUUIDAddress = 0xFC;
const uint32_t FanUnit_CardInfo::kUUIDSize = 4;

FanUnit_CardInfo::FanUnit_CardInfo(
  Logger::ILogger *logger,
  const std::string &path)
  : I2CDevice24C02(
  logger,
  kName,
  kDescription,
  kAddress,
  path) {
  // Create value manufacturer
  manufacturer_ = Create().ParameterIn<std::string>(
    kManufacturerName,
    kManufacturerDescription,
    Types::Affiliation::Enum::SHARED).AddAndReturnPointer();
  manufacturer_->CreateAccessor().I2CEEProm<std::string>(
    GetDevicePath(),
    kManufacturerAddress,
    kManufacturerSize,
    "",
    "");

  boardNameString_ = Create().ParameterIn<std::string>(
    kBoardNameStringName,
    kBoardNameDescription,
    Types::Affiliation::Enum::SHARED).AddAndReturnPointer();
  boardNameString_->CreateAccessor().I2CEEProm<std::string>(
    GetDevicePath(),
    kBoardNameAddress,
    kBoardNameSize,
    "",
    "");

  boardName_ = Create().ParameterIn<Types::ModuleType>(
    kBoardNameName,
    kBoardNameDescription,
    Types::Affiliation::Enum::SHARED).AddAndReturnPointer();
  boardName_->CreateAccessor().I2CEEProm<std::string>(
    GetDevicePath(),
    kBoardNameAddress,
    kBoardNameSize,
    "",
    "");

  boardRevision_ = Create().ParameterIn<std::string>(
    kBoardRevisionName,
    kBoardRevisionDescription,
    Types::Affiliation::Enum::SHARED).AddAndReturnPointer();
  boardRevision_->CreateAccessor().I2CEEProm<std::string>(
    GetDevicePath(),
    kBoardRevisionAddress,
    kBoardRevisionSize,
    "",
    "");

  serialNumber_ = Create().ParameterIn<std::string>(
    kSerialNumberName,
    kSerialNumberDescription,
    Types::Affiliation::Enum::SHARED).AddAndReturnPointer();
  serialNumber_->CreateAccessor().I2CEEProm<std::string>(
    GetDevicePath(),
    kSerialNumberAddress,
    kSerialNumberSize,
    "",
    "");

  operatingHours_ = Create().ItemInOut<std::chrono::minutes>(
    kOperatingHoursName,
    kOperatingHoursDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::minutes(15),
    Types::Affiliation::Enum::SHARED,
    Types::WriteStrategy::Enum::ON_CHANGE,
    true).AddAndReturnPointer();
  operatingHours_->CreateAccessor().I2CEEProm<uint32_t>(
    GetDevicePath(),
    kOperatingHoursAddress,
    kOperatingHoursSize,
    15,
    1);
  operatingHours_->CreateAction().Miscellaneous().Increment(
    operatingHours_->GetValueIn(),
    std::chrono::minutes(15));

  fanAOperatingHours_ = Create().ItemInOut<std::chrono::minutes>(
    kFanAOperatingHoursName,
    kFanAOperatingHoursDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::minutes(15),
    Types::Affiliation::Enum::SHARED,
    Types::WriteStrategy::Enum::ON_CHANGE,
    true).AddAndReturnPointer();
  fanAOperatingHours_->CreateAccessor().I2CEEProm<uint32_t>(
    GetDevicePath(),
    kFanAOperatingHoursAddress,
    kFanAOperatingHoursSize,
    15,
    1);
  fanAOperatingHours_->CreateAction().Miscellaneous().Increment(
    operatingHours_->GetValueIn(),
    std::chrono::minutes(15));

  fanBOperatingHours_ = Create().ItemInOut<std::chrono::minutes>(
    kFanBOperatingHoursName,
    kFanBOperatingHoursDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::minutes(15),
    Types::Affiliation::Enum::SHARED,
    Types::WriteStrategy::Enum::ON_CHANGE,
    true).AddAndReturnPointer();
  fanBOperatingHours_->CreateAccessor().I2CEEProm<uint32_t>(
    GetDevicePath(),
    kFanBOperatingHoursAddress,
    kFanBOperatingHoursSize,
    15,
    1);
  fanBOperatingHours_->CreateAction().Miscellaneous().Increment(
    operatingHours_->GetValueIn(),
    std::chrono::minutes(15));

//  fanMOperatingHours_ = Create().ItemInOut<std::chrono::minutes>(
//    kFanMOperatingHoursName,
//    kFanMOperatingHoursDescription,
//    Types::ValueGroup::Enum::NONE,
//    std::chrono::minutes(15),
//    true).AddAndReturnPointer();
//  fanMOperatingHours_->CreateAccessor().I2CEEProm<uint32_t>(
//    this,
//    kFanMOperatingHoursAddress,
//    kFanMOperatingHoursSize,
//    15,
//    1);
//  fanMOperatingHours_->CreateAction().Miscellaneous().Increment(
//    operatingHours_->GetValueIn(),
//    std::chrono::minutes(15));

  md5_ = Create().ParameterIn<std::string>(
    kMD5Name,
    kMD5Description,
    Types::Affiliation::Enum::SHARED).AddAndReturnPointer();
  md5_->CreateAccessor().I2CEEProm<std::string>(
    GetDevicePath(),
    kMD5Address,
    kMD5Size,
    "",
    "");

  uuid_ = Create().ParameterIn<std::string>(
    kUUIDName,
    kUUIDDescription,
    Types::Affiliation::Enum::SHARED).AddAndReturnPointer();
  uuid_->CreateAccessor().I2CEEProm<std::string>(
    GetDevicePath(),
    kUUIDAddress,
    kUUIDSize,
    "",
    "");
}

Framework::Components::ParameterItemIn<std::string> *FanUnit_CardInfo::Manufacturer() const {
  return manufacturer_;
}

Framework::Components::ParameterItemIn<std::string> *FanUnit_CardInfo::CardTypeString() const {
  return boardNameString_;
}

Framework::Components::ParameterItemIn<Types::ModuleType> *FanUnit_CardInfo::CardType() const {
  return boardName_;
}

Framework::Components::ParameterItemIn<std::string> *FanUnit_CardInfo::BoardRevision() const {
  return boardRevision_;
}

Framework::Components::ParameterItemIn<std::string> *FanUnit_CardInfo::SerialNumber() const {
  return serialNumber_;
}

Framework::Components::ItemInOut<std::chrono::minutes> *FanUnit_CardInfo::OperatingHours() const {
  return operatingHours_;
}

Framework::Components::ItemInOut<std::chrono::minutes> *FanUnit_CardInfo::FanAOperatingHours() const {
  return fanAOperatingHours_;
}

Framework::Components::ItemInOut<std::chrono::minutes> *FanUnit_CardInfo::FanBOperatingHours() const {
  return fanBOperatingHours_;
}

// Framework::Components::ItemInOut<std::chrono::minutes> *FanUnit_CardInfo::FanMOperatingHours() const {
//   return fanMOperatingHours_;
// }

Framework::Components::ParameterItemIn<std::string> *FanUnit_CardInfo::MD5() const {
  return md5_;
}

Framework::Components::ParameterItemIn<std::string> *FanUnit_CardInfo::UUID() const {
  return uuid_;
}

}  // namespace SystemModules::Modules::FanUnit
