/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Builder/Actions/ActionBuilder.h"
#include "FanUnit_CardInfo.h"
#include "FanUnit_Fan.h"
#include "FanUnit.h"

namespace SystemModules::Modules::FanUnit {

const char FanUnit::kName[] = "FanUnit";
const char FanUnit::kDescription[] = "";
const char FanUnit::kPath[] = "/sys/bus/i2c/devices/0-0071/channel-4";

const char FanUnit::kFanAName[] = "FanA";
const uint16_t FanUnit::kFanAAddress = 0x18;

// const char FanUnit::kFanMName[] = "FanM";
// const uint16_t FanUnit::kFanMAddress = 0x19;

const char FanUnit::kFanBName[] = "FanB";
const uint16_t FanUnit::kFanBAddress = 0x4d;

FanUnit::FanUnit(
  Logger::ILogger *logger,
  const Modules::Configuration::ConfigurationCommon &configCommon,
  const Configuration::ConfigurationFanUnit &config)
  : Framework::Components::Container(
  logger,
  kName,
  kDescription) {
  (void) config;
  // Create card info
  auto cardInfo_obj = std::make_unique<FanUnit_CardInfo>(
    logger,
    kPath);
  cardInfo_ = cardInfo_obj.get();
  AddComponent(std::move(cardInfo_obj));
  // Create fan A
  auto fanA_obj = std::make_unique<FanUnit_Fan>(
    logger,
    configCommon,
    kFanAName,
    kFanAAddress,
    kPath,
    Types::MonitoringFunctionId::Enum::SCU_MON_W_FUNCTION_fan_speed_A);
  fanA_ = fanA_obj.get();
  AddComponent(std::move(fanA_obj));
  // Create fan M
//  auto fanM_obj = std::make_unique<FanUnit_Fan>(
//    logger,
//    configCommon,
//    kFanMName,
//    kFanMAddress,
//    kPath,
//    Types::MonitoringFunctionId::Enum::SCU_MON_W_FUNCTION_fan_speed_M);
//  fanM_ = fanM_obj.get();
//  AddComponent(std::move(fanM_obj));
  // Create fan B
  auto fanB_obj = std::make_unique<FanUnit_Fan>(
    logger,
    configCommon,
    kFanBName,
    kFanBAddress,
    kPath,
    Types::MonitoringFunctionId::Enum::SCU_MON_W_FUNCTION_fan_speed_B);
  fanB_ = fanB_obj.get();
  AddComponent(std::move(fanB_obj));

  // Create and add actions
  FanA()->Enable()->CreateAction().StaticValue(
    1);  // open loop mode

  FanA()->PWM()->CreateAction().FanUnit().FanSpeed(
    nullptr,
    FanA()->Speed(),
    FanA()->Enable(),
    FanB()->Speed(),
    FanB()->Enable(),
//    FanM()->Speed(),
//    FanM()->Enable());
    FanB()->Speed(),
    FanB()->Enable());
//
//  FanM()->Enable()->CreateAction().StaticValue(
//    1);  // open loop mode
//  FanM()->PWM()->CreateAction().FanUnit().FanSpeed(
//    nullptr,
//    FanM()->Speed(),
//    FanM()->Enable(),
//    FanA()->Speed(),
//    FanA()->Enable(),
//    FanB()->Speed(),
//    FanB()->Enable());

  FanB()->Enable()->CreateAction().StaticValue(
      1);  // open loop mode
  FanB()->PWM()->CreateAction().FanUnit().FanSpeed(
    nullptr,
    FanB()->Speed(),
    FanB()->Enable(),
//    FanM()->Speed(),
//    FanM()->Enable(),
    FanB()->Speed(),
    FanB()->Enable(),
    FanA()->Speed(),
    FanA()->Enable());
}

FanUnit_CardInfo *FanUnit::CardInfo() const {
  return cardInfo_;
}

FanUnit_Fan *FanUnit::FanA() const {
  return fanA_;
}
//
//  FanUnit_Fan *FanUnit::FanM() const {
//    return fanM_;
//  }

FanUnit_Fan *FanUnit::FanB() const {
  return fanB_;
}

}  // namespace SystemModules::Modules::FanUnit
