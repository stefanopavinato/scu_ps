/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "SystemModules/Framework/Components/ItemIn.h"
#include "SystemModules/Framework/Components/ItemOut.h"
#include "SystemModules/Framework/Components/DeviceAMC6821.h"
#include "SystemModules/Modules/Configuration/ConfigurationCommon.h"

namespace SystemModules::Modules::FanUnit {
class FanUnit_Fan : public Framework::Components::DeviceAMC6821 {
 public:
  FanUnit_Fan(
    Logger::ILogger *logger,
    const Modules::Configuration::ConfigurationCommon &configCommon,
    const std::string &name,
    const uint16_t &address,
    const std::string &basePath,
    const Types::MonitoringFunctionId::Enum &warningId);

  ~FanUnit_Fan() override = default;

  [[nodiscard]] Framework::Components::ItemIn<Types::Value<double>> *Speed() const;

  [[nodiscard]] Framework::Components::ItemIn<Types::Value<int32_t>> *Temperature() const;

  [[nodiscard]] Framework::Components::ItemOut<int32_t> *Enable() const;

  [[nodiscard]] Framework::Components::ItemOut<uint8_t> *PWM() const;

 private:
  Framework::Components::ItemIn<Types::Value<double>> *speed_;
  Framework::Components::ItemIn<Types::Value<int32_t>> *temperature_;
  Framework::Components::ItemOut<int32_t> *enable_;
  Framework::Components::ItemOut<uint8_t> *pwm_;

  static const char kDescription[];

  static const uint32_t kOnDelay_ms;
  static const uint32_t kOffDelay_ms;

  static const char kEnableName[];
  static const char kEnableDescription[];
  static const char kEnableSubPath[];

  static const char kPwmName[];
  static const char kPwmDescription[];
  static const char kPwmSubPath[];

  static const char kSpeedName[];
  static const char kSpeedDescription[];
  static const char kSpeedSubPath[];

  static const char kTemperatureName[];
  static const char kTemperatureDescription[];
  static const char kTemperatureSubPath[];
};
}  // namespace SystemModules::Modules::FanUnit
