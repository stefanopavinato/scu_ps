/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Builder/Accessors/ReadAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Accessors/WriteAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemOutBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemInBuilder.h"
#include "FanUnit_Fan.h"

namespace SystemModules::Modules::FanUnit {

const char FanUnit_Fan::kDescription[] = "";

const uint32_t FanUnit_Fan::kOnDelay_ms = 5000;
const uint32_t FanUnit_Fan::kOffDelay_ms = 5000;

const char FanUnit_Fan::kEnableName[] = "Enable";
const char FanUnit_Fan::kEnableDescription[] = "Enables the fan";
const char FanUnit_Fan::kEnableSubPath[] = "/pwm1_enable";

const char FanUnit_Fan::kPwmName[] = "PWM1";
const char FanUnit_Fan::kPwmDescription[] = "Sets the speed of the fan";
const char FanUnit_Fan::kPwmSubPath[] = "/pwm1";

const char FanUnit_Fan::kSpeedName[] = "Speed";
const char FanUnit_Fan::kSpeedDescription[] = "Current speed of the fan";
const char FanUnit_Fan::kSpeedSubPath[] = "/fan1_input";

const char FanUnit_Fan::kTemperatureName[] = "Temperature";
const char FanUnit_Fan::kTemperatureDescription[] = "Temperature of the fan's controller chip";
const char FanUnit_Fan::kTemperatureSubPath[] = "/temp1_input";

FanUnit_Fan::FanUnit_Fan(
  Logger::ILogger *logger,
  const Modules::Configuration::ConfigurationCommon &configCommon,
  const std::string &name,
  const uint16_t &address,
  const std::string &path,
  const Types::MonitoringFunctionId::Enum &warningId)
  : DeviceAMC6821(
  logger,
  name,
  kDescription,
  address,
  path) {
  // Get update interval
  auto updateInterval = configCommon.UpdateInterval()->Slow()->Value();
  // Cerate enable out-value
  enable_ = Create().ItemOut<int32_t>(
    kEnableName,
    kEnableDescription,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::SHARED,
    Types::WriteStrategy::Enum::ON_UPDATE,
    true,
    0).AddAndReturnPointer();
  enable_->CreateAccessor().File<int32_t, std::string>(
    std::string(Get()).append(kEnableSubPath),
    1,
    1);
  // Create pwm out-value
  pwm_ = Create().ItemOut<uint8_t>(
    kPwmName,
    kPwmDescription,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::SHARED,
    Types::WriteStrategy::Enum::ON_CHANGE,
    true,
    180).AddAndReturnPointer();
  pwm_->CreateAccessor().File<uint8_t, std::string>(
    std::string(Get()).append(kPwmSubPath),
    1,
    1);
  // Create speed in-value
  speed_ = Create().ItemIn<Types::Value<double>>(
    kSpeedName,
    kSpeedDescription,
    Types::ValueGroup::Enum::SPEED,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::SHARED,
    Types::Unit::Enum::ROTATIONAL_SPEED,
    Types::UnitPrefix::Enum::NONE,
    1).AddAndReturnPointer();
  speed_->CreateAccessor().File<double, std::string>(
    std::string(Get()).append(kSpeedSubPath),
    1,
    1);
  speed_->CreateMonitoringFunction().IsLowerThan(
    Types::AlertSeverity::Enum::WARNING,
    warningId,
    std::chrono::milliseconds(kOnDelay_ms),
    std::chrono::milliseconds(kOffDelay_ms),
    Types::Value<double>(
      2000,
      Types::Unit::Enum::ROTATIONAL_SPEED,
      Types::UnitPrefix::Enum::NONE));
  // Create temperature in-value
  temperature_ = Create().ItemIn<Types::Value<int32_t>>(
    kTemperatureName,
    kTemperatureDescription,
    Types::ValueGroup::Enum::TEMPERATURE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::SHARED,
    Types::Unit::Enum::DEGREE_CELSIUS,
    Types::UnitPrefix::Enum::MILLI).AddAndReturnPointer();
  temperature_->CreateAccessor().File<int32_t, std::string>(
    std::string(Get()).append(kTemperatureSubPath),
    1,
    1);
}

Framework::Components::ItemIn<Types::Value<double>> *FanUnit_Fan::Speed() const {
  return speed_;
}

Framework::Components::ItemIn<Types::Value<int32_t>> *FanUnit_Fan::Temperature() const {
  return temperature_;
}

Framework::Components::ItemOut<int32_t> *FanUnit_Fan::Enable() const {
  return enable_;
}

Framework::Components::ItemOut<uint8_t> *FanUnit_Fan::PWM() const {
  return pwm_;
}

}  // namespace SystemModules::Modules::FanUnit
