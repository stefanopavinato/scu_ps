/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Builder/Accessors/ReadAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemInBuilder.h"
#include "SystemModules/Framework/Builder/Components/ComponentBuilder.h"
#include "PowerSupplyAnalogIn2.h"

namespace SystemModules::Modules::PowerSupply {

const char PowerSupplyAnalogIn2::kName[] = "AnalogIn2";
const char PowerSupplyAnalogIn2::kDescription[] =
  "Current and voltage values from INA220-chip on the power supply backplane measuring the STANDBY Supply";
const uint16_t PowerSupplyAnalogIn2::kAddress = 0x41;


const char PowerSupplyAnalogIn2::kStandbySupplyCurrentName[] = "StandbySupplyCurrent";
const char PowerSupplyAnalogIn2::kStandbySupplyCurrentDescription[] = "STANDBY Supply Current";
const char PowerSupplyAnalogIn2::kStandbySupplyCurrentSubPath[] = "in0_input";

const char PowerSupplyAnalogIn2::kStandbySupplyVoltageName[] = "StandbySupplyVoltage";
const char PowerSupplyAnalogIn2::kStandbySupplyVoltageDescription[] = "STANDBY Supply Voltage";
const char PowerSupplyAnalogIn2::kStandbySupplyVoltageSubPath[] = "in1_input";

PowerSupplyAnalogIn2::PowerSupplyAnalogIn2(
  Logger::ILogger *logger,
  const Modules::Configuration::ConfigurationCommon &configCommon,
  const std::string &path,
  const std::string &devicePath,
  const uint16_t &slot)
  : Framework::Components::I2CDeviceINA220(
  logger,
  kName,
  kDescription,
  slot + 1,
  kAddress,
  path,
  devicePath) {
  // Get update intervaval
  auto updateInterval = configCommon.UpdateInterval()->Slow()->Value();

  standbySupplyCurrent_ = Create().ItemIn<Types::Value<int32_t>>(
    kStandbySupplyCurrentName,
    kStandbySupplyCurrentDescription,
    Types::ValueGroup::Enum::SUPPLY,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::SHARED,
    Types::Unit::Enum::AMPERE,
    Types::UnitPrefix::Enum::MILLI).AddAndReturnPointer();
  standbySupplyCurrent_->CreateAccessor().File<int32_t, std::string>(
    GetDevicePath().append(kStandbySupplyCurrentSubPath),
    1,
    1);
  standbySupplyVoltage_ = Create().ItemIn<Types::Value<int32_t>>(
    kStandbySupplyVoltageName,
    kStandbySupplyVoltageDescription,
    Types::ValueGroup::Enum::SUPPLY,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::SHARED,
    Types::Unit::Enum::VOLTAGE,
    Types::UnitPrefix::Enum::MILLI).AddAndReturnPointer();
  standbySupplyVoltage_->CreateAccessor().File<int32_t, std::string>(
    GetDevicePath().append(kStandbySupplyVoltageSubPath),
    1,
    1);
}

Framework::Components::ItemIn<Types::Value<int32_t>> *PowerSupplyAnalogIn2::StandbySupplyCurrent() const {
  return standbySupplyCurrent_;
}

Framework::Components::ItemIn<Types::Value<int32_t>> *PowerSupplyAnalogIn2::StandbySupplyVoltage() const {
  return standbySupplyVoltage_;
}

}  // namespace SystemModules::Modules::PowerSupply
