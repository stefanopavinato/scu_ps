/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Types/Value.h"
#include "SystemModules/Framework/Components/ItemIn.h"
#include "SystemModules/Framework/Components/I2CDeviceINA220.h"
#include "SystemModules/Modules/Configuration/ConfigurationCommon.h"

namespace SystemModules::Modules::PowerSupply {
class PowerSupplyAnalogIn1 : public Framework::Components::I2CDeviceINA220 {
 public:
  PowerSupplyAnalogIn1(
    Logger::ILogger *logger,
    const Modules::Configuration::ConfigurationCommon &configCommon,
    const std::string &path,
    const std::string &devicePath,
    const uint16_t &slot);

  [[nodiscard]] Framework::Components::ItemIn<Types::Value<int32_t>> *MainSupplyCurrent() const;

  [[nodiscard]] Framework::Components::ItemIn<Types::Value<int32_t>> *MainSupplyVoltage() const;

 private:
  Framework::Components::ItemIn<Types::Value<int32_t>> *mainSupplyCurrent_;

  Framework::Components::ItemIn<Types::Value<int32_t>> *mainSupplyVoltage_;

  static const char kName[];
  static const char kDescription[];
  static const uint16_t kAddress;

  static const char kMainSupplyCurrentName[];
  static const char kMainSupplyCurrentDescription[];
  static const char kMainSupplyCurrentSubPath[];

  static const char kMainSupplyVoltageName[];
  static const char kMainSupplyVoltageDescription[];
  static const char kMainSupplyVoltageSubPath[];
};

}  // namespace SystemModules::Modules::PowerSupply
