/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "SystemModules/Modules/Configuration/ConfigurationCommon.h"
#include "SystemModules/Modules/Configuration/ConfigurationPowerSupply.h"
#include "SystemModules/Framework/Components/Container.h"

namespace SystemModules::Modules::PowerSupply {
class PowerSupplyAnalogIn1;

class PowerSupplyAnalogIn2;

// class PowerSupplyDIO; tbd: rejz to be added later
class PowerSupply : public Framework::Components::Container {
 public:
  explicit PowerSupply(
    Logger::ILogger *logger,
    const Modules::Configuration::ConfigurationCommon &configCommon,
    const Configuration::ConfigurationPowerSupply &config);

  [[nodiscard]] PowerSupplyAnalogIn1 *AnalogIn1() const;

  [[nodiscard]] PowerSupplyAnalogIn2 *AnalogIn2() const;

 private:
  PowerSupplyAnalogIn1 *analogIn1_;
  PowerSupplyAnalogIn2 *analogIn2_;

  static const char kPowerSupplyName[];
  static const char kPowerSupplyDescription[];

  static const char kManagement[];
  static const char kManagementDescription[];
};
}  // namespace SystemModules::Modules::PowerSupply
