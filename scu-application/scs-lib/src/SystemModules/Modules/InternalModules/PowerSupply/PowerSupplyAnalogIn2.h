/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Types/Value.h"
#include "SystemModules/Framework/Components/ItemIn.h"
#include "SystemModules/Framework/Components/I2CDeviceINA220.h"
#include "SystemModules/Modules/Configuration/ConfigurationCommon.h"

namespace SystemModules::Modules::PowerSupply {
class PowerSupplyAnalogIn2 : public Framework::Components::I2CDeviceINA220 {
 public:
  PowerSupplyAnalogIn2(
    Logger::ILogger *logger,
    const Modules::Configuration::ConfigurationCommon &configCommon,
    const std::string &path,
    const std::string &devicePath,
    const uint16_t &slot);

  [[nodiscard]] Framework::Components::ItemIn<Types::Value<int32_t>> *StandbySupplyCurrent() const;

  [[nodiscard]] Framework::Components::ItemIn<Types::Value<int32_t>> *StandbySupplyVoltage() const;

 private:
  Framework::Components::ItemIn<Types::Value<int32_t>> *standbySupplyCurrent_;

  Framework::Components::ItemIn<Types::Value<int32_t>> *standbySupplyVoltage_;

  static const char kName[];
  static const char kDescription[];
  static const uint16_t kAddress;

  static const char kStandbySupplyCurrentName[];
  static const char kStandbySupplyCurrentDescription[];
  static const char kStandbySupplyCurrentSubPath[];

  static const char kStandbySupplyVoltageName[];
  static const char kStandbySupplyVoltageDescription[];
  static const char kStandbySupplyVoltageSubPath[];
};

}  // namespace SystemModules::Modules::PowerSupply
