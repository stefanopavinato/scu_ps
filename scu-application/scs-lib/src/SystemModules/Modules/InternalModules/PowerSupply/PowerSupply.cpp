/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <memory>
#include "Logger/ILogger.h"
#include "PowerSupplyAnalogIn1.h"
#include "PowerSupplyAnalogIn2.h"
#include "PowerSupply.h"

namespace SystemModules::Modules::PowerSupply {

const char PowerSupply::kPowerSupplyName[] = "POWER_SUPPLY";
const char PowerSupply::kPowerSupplyDescription[] = "Power Supply unit of the SCU";

PowerSupply::PowerSupply(
  Logger::ILogger *logger,
  const Modules::Configuration::ConfigurationCommon &configCommon,
  const Configuration::ConfigurationPowerSupply &config)
  : SystemModules::Framework::Components::Container(
  logger,
  kPowerSupplyName,
  kPowerSupplyDescription) {
  // Create INA220 device measuring MAIN Supply
  auto analogIn1 = std::make_unique<PowerSupplyAnalogIn1>(
    logger,
    configCommon,
    config.PathI2CApplication()->GetValue(),
    "/sys/bus/i2c/devices/0-0071/channel-4/26-0045",  // TBD
    25);  // TBD
  analogIn1_ = analogIn1.get();
  AddComponent(std::move(analogIn1));

  // Create INA220 device measuring STANDBY Supply
  auto analogIn2 = std::make_unique<PowerSupplyAnalogIn2>(
    logger,
    configCommon,
    config.PathI2CApplication()->GetValue(),
    "devicePath",  // TBD
    25);  // TBD, i2c bus is 26
  analogIn2_ = analogIn2.get();
  AddComponent(std::move(analogIn2));
  // ToDo Create GPIO device
}

PowerSupplyAnalogIn1 *PowerSupply::AnalogIn1() const {
  return analogIn1_;
}

PowerSupplyAnalogIn2 *PowerSupply::AnalogIn2() const {
  return analogIn2_;
}

}  // namespace SystemModules::Modules::PowerSupply
