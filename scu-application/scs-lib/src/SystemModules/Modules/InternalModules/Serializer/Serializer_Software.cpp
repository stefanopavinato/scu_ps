/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Types/VariantValue.h"
#include "Serializer_SvnInfo.h"
#include "Serializer_Software.h"

namespace SystemModules::Modules {

const char Serializer_Software::kName[] = "Software";
const char Serializer_Software::kDescription[] = "";

const char Serializer_Software::kPlatformName[] = "Platform";
const char Serializer_Software::kPlatformDescription[] = "";

const char Serializer_Software::kApplicationName[] = "Application";
const char Serializer_Software::kApplicationDescription[] = "";

Serializer_Software::Serializer_Software(
  Logger::ILogger *logger,
  const Configuration::ConfigurationSerializer &config)
  : Framework::Components::Container(
    logger,
    kName,
    kDescription) {
  // Add platform
  auto platform = std::make_unique<Serializer_SvnInfo>(
    logger,
    *config.Software()->Platform(),
    kPlatformName,
    kPlatformDescription);
  platform_ = platform.get();
  AddComponent(std::move(platform));
  // Add application
  auto application = std::make_unique<Serializer_SvnInfo>(
    logger,
    *config.Software()->Application(),
    kApplicationName,
    kApplicationDescription);
  application_ = application.get();
  AddComponent(std::move(application));
}

Serializer_SvnInfo *Serializer_Software::Platform() const {
  return platform_;
}

Serializer_SvnInfo *Serializer_Software::Application() const {
  return application_;
}
}  // namespace SystemModules::Modules
