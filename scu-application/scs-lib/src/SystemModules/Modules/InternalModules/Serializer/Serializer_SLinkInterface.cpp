/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Builder/Accessors/ReadAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Accessors/ReadWriteAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Actions/ActionBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemInBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemOutBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemInOutBuilder.h"
#include "Serializer_SLinkInterface.h"

namespace SystemModules::Modules::Serializer {

const char Serializer_SLinkInterface::kDescription[] = "";

const char Serializer_SLinkInterface::kTransceiverName[] = "Transceiver";
const uint32_t Serializer_SLinkInterface::kTransceiverAddress = 4;

const char Serializer_SLinkInterface::kLocalIdName[] = "LocalId";
const char Serializer_SLinkInterface::kLocalIdDescription[] = "";
const uint32_t Serializer_SLinkInterface::kLocalIdAddressOffset = 28;
const uint32_t Serializer_SLinkInterface::kLocalIdPosition = 0;
const uint32_t Serializer_SLinkInterface::kLocalIdSize = 16;

const char Serializer_SLinkInterface::kLocalStatusName[] = "LocalStatus";
const char Serializer_SLinkInterface::kLocalStatusDescription[] = "";
const uint32_t Serializer_SLinkInterface::kLocalStatusAddressOffset = 32;
const uint32_t Serializer_SLinkInterface::kLocalStatusPosition = 16;
const uint32_t Serializer_SLinkInterface::kLocalStatusSize = 3;

const char Serializer_SLinkInterface::kTxFastChannelSequenceNumberErrorsName[] =
  "TxFastChannelSequenceNumberErrors";
const char Serializer_SLinkInterface::kTxFastChannelSequenceNumberErrorsDescription[] = "";
const uint32_t Serializer_SLinkInterface::kTxFastChannelSequenceNumberErrorsAddressOffset = 44;
const uint32_t Serializer_SLinkInterface::kTxFastChannelSequenceNumberErrorsPosition = 0;
const uint32_t Serializer_SLinkInterface::kTxFastChannelSequenceNumberErrorsSize = 32;

const char Serializer_SLinkInterface::kTxNestedChannelSequenceNumberErrorsName[] =
  "TxNestedChannelSequenceNumberErrors";
const char Serializer_SLinkInterface::kTxNestedChannelSequenceNumberErrorsDescription[] = "";
const uint32_t Serializer_SLinkInterface::kTxNestedChannelSequenceNumberErrorsAddressOffset = 48;
const uint32_t Serializer_SLinkInterface::kTxNestedChannelSequenceNumberErrorsPosition = 0;
const uint32_t Serializer_SLinkInterface::kTxNestedChannelSequenceNumberErrorsSize = 32;

const char Serializer_SLinkInterface::kTxNestedChannelFSMErrorsName[] =
  "TxNestedChannelFSMErrors";
const char Serializer_SLinkInterface::kTxNestedChannelFSMErrorsDescription[] = "";
const uint32_t Serializer_SLinkInterface::kTxNestedChannelFSMErrorsAddressOffset = 52;
const uint32_t Serializer_SLinkInterface::kTxNestedChannelFSMErrorsPosition = 0;
const uint32_t Serializer_SLinkInterface::kTxNestedChannelFSMErrorsSize = 32;

const char Serializer_SLinkInterface::kAssociatedGtLaneName[] = "AssociatedGtLane";
const char Serializer_SLinkInterface::kAssociatedGtLaneDescription[] = "";
const uint32_t Serializer_SLinkInterface::kAssociatedGtLaneAddressOffset = 60;
const uint32_t Serializer_SLinkInterface::kAssociatedGtLanePosition = 0;
const uint32_t Serializer_SLinkInterface::kAssociatedGtLaneSize = 8;

const char Serializer_SLinkInterface::kSLinkClearErrorsName[] = "SLinkErrorsClear";
const char Serializer_SLinkInterface::kSLinkClearErrorsDescription[] = "";
const uint32_t Serializer_SLinkInterface::kSLinkClearErrorsAddressOffset = 24;
const uint32_t Serializer_SLinkInterface::kSLinkClearErrorsPosition = 0;
const uint32_t Serializer_SLinkInterface::kSLinkClearErrorsSize = 1;

Serializer_SLinkInterface::Serializer_SLinkInterface(
  Logger::ILogger *logger,
  std::string const &name,
  const Modules::Configuration::ConfigurationCommon &configCommon,
  uint64_t const &baseAddress,
  uint64_t const &sLinkAddress)
  : Framework::Components::Container(
  logger,
  name,
  kDescription) {
  // Get update interval
  auto updateInterval = configCommon.UpdateInterval()->Medium()->Value();

  auto transceiver = std::make_unique<Common::Transceiver>(
      logger,
      configCommon,
      kTransceiverName,
      sLinkAddress + kTransceiverAddress);
  transceiver_ = transceiver.get();
  AddComponent((std::move(transceiver)));

  localId_ = Create().ItemIn<uint16_t>(
    kLocalIdName,
    kLocalIdDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  localId_->CreateAccessor().ScuRegister(
    sLinkAddress + kLocalIdAddressOffset,
    kLocalIdPosition,
    kLocalIdSize);

  localStatus_ = Create().ItemInOut<Types::ScuStatus>(
    kLocalStatusName,
    kLocalStatusDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    false).AddAndReturnPointer();
  localStatus_->CreateAccessor().ScuRegister(
      sLinkAddress + kLocalStatusAddressOffset,
    kLocalStatusPosition,
    kLocalStatusSize);
  localStatus_->CreateAction().StaticValue(Types::ScuStatus());

  txFastChannelSequenceNumberErrors_ = Create().ItemIn<uint32_t>(
    kTxFastChannelSequenceNumberErrorsName,
    kTxFastChannelSequenceNumberErrorsDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  txFastChannelSequenceNumberErrors_->CreateAccessor().ScuRegister(
    sLinkAddress + kTxFastChannelSequenceNumberErrorsAddressOffset,
    kTxFastChannelSequenceNumberErrorsPosition,
    kTxFastChannelSequenceNumberErrorsSize);

  txNestedChannelSequenceNumberErrors_ = Create().ItemIn<uint32_t>(
    kTxNestedChannelSequenceNumberErrorsName,
    kTxNestedChannelSequenceNumberErrorsDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  txNestedChannelSequenceNumberErrors_->CreateAccessor().ScuRegister(
    sLinkAddress + kTxNestedChannelSequenceNumberErrorsAddressOffset,
    kTxNestedChannelSequenceNumberErrorsPosition,
    kTxNestedChannelSequenceNumberErrorsSize);

  txNestedChannelFSMErrors_ = Create().ItemIn<uint32_t>(
    kTxNestedChannelFSMErrorsName,
    kTxNestedChannelFSMErrorsDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  txNestedChannelFSMErrors_->CreateAccessor().ScuRegister(
      sLinkAddress + kTxNestedChannelFSMErrorsAddressOffset,
    kTxNestedChannelFSMErrorsPosition,
    kTxNestedChannelFSMErrorsSize);

  associatedGtLane_ = Create().ItemIn<uint8_t>(
    kAssociatedGtLaneName,
    kAssociatedGtLaneDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  associatedGtLane_->CreateAccessor().ScuRegister(
    baseAddress + kAssociatedGtLaneAddressOffset,
    kAssociatedGtLanePosition,
    kAssociatedGtLaneSize);

  sLinkClearErrors_ = Create().ItemOut<bool>(
      kSLinkClearErrorsName,
      kSLinkClearErrorsDescription,
      std::chrono::milliseconds(updateInterval),
      Types::Affiliation::Enum::EXCLUSIVE,
      Types::WriteStrategy::Enum::ON_CHANGE,
      false,
      false).AddAndReturnPointer();
  sLinkClearErrors_->CreateAccessor().ScuRegister(
      sLinkAddress + kSLinkClearErrorsAddressOffset,
      kSLinkClearErrorsPosition,
      kSLinkClearErrorsSize);
  sLinkClearErrors_->CreateAction().StaticValue(
      false);
}

SystemModules::Modules::Common::Transceiver *Serializer_SLinkInterface::Transceiver() const {
  return transceiver_;
}

Framework::Components::ItemIn<uint16_t> *Serializer_SLinkInterface::LocalId() const {
  return localId_;
}

Framework::Components::ItemInOut<Types::ScuStatus> *Serializer_SLinkInterface::LocalStatus() const {
  return localStatus_;
}

Framework::Components::ItemIn<uint32_t> *Serializer_SLinkInterface::TxFastChannelSequenceNumberErrors() const {
  return txFastChannelSequenceNumberErrors_;
}

Framework::Components::ItemIn<uint32_t> *Serializer_SLinkInterface::TxNestedChannelSequenceNumberErrors() const {
  return txNestedChannelSequenceNumberErrors_;
}

Framework::Components::ItemIn<uint32_t> *Serializer_SLinkInterface::TxNestedChannelFSMErrors() const {
  return txNestedChannelFSMErrors_;
}

Framework::Components::ItemIn<uint8_t> *Serializer_SLinkInterface::AssociatedGtLane() const {
  return associatedGtLane_;
}

Framework::Components::ItemOut<bool> *Serializer_SLinkInterface::SLinkClearErrors() const {
  return sLinkClearErrors_;
}

}  // namespace SystemModules::Modules::Serializer
