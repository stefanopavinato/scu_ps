/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Builder/Components/ItemOutBuilder.h"
#include "SystemModules/Framework/Builder/Components/ComponentBuilder.h"
#include "Serializer_Gpio.h"

namespace SystemModules::Modules {

const char Serializer_Gpio::kName[] = "Gpio";
const char Serializer_Gpio::kDescription[] = "";

const char Serializer_Gpio::kPowerEnablePullUpDriveName[] = "PowerEnablePullUpDrive";
const char Serializer_Gpio::kPowerEnablePullUpDriveDescription[] = "";
const uint32_t Serializer_Gpio::kPowerEnablePullUpDriveGpio = 503;

Serializer_Gpio::Serializer_Gpio(
  Logger::ILogger *logger,
  const Modules::Configuration::ConfigurationCommon &configCommon,
  bool *powerEnablePullUpDrive)
  : Container(
  logger,
  kName,
  kDescription) {
  auto updateInterval = configCommon.UpdateInterval()->Medium()->Value();
  // Setup power enable pull up drive
  powerEnablePullUpDrive_ = Create().ItemOut<bool>(
    kPowerEnablePullUpDriveName,
    kPowerEnablePullUpDriveDescription,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_CHANGE,
    true,
    false).AddAndReturnPointer();
  powerEnablePullUpDrive_->CreateAccessor().DigitalOutput(
    kPowerEnablePullUpDriveGpio,
    false);
  powerEnablePullUpDrive_->CreateAction().ReadValue(
    powerEnablePullUpDrive);
  // Add action welche alle mc states überwacht und abhängig davon den ausgang schaltet
  // Hierfür wird Referenz auf mc's benötigt.
}

Framework::Components::ItemOut<bool> *Serializer_Gpio::PowerEnablePullUpDrive() const {
  return powerEnablePullUpDrive_;
}

}  // namespace SystemModules::Modules

