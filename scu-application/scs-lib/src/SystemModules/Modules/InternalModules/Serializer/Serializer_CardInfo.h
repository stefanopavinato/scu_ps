/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "SystemModules/Framework/Components/ItemIn.h"
#include "SystemModules/Framework/Components/ItemInOut.h"
#include "SystemModules/Framework/Components/ParameterItemIn.h"
#include "SystemModules/Framework/Components/I2CDevice24C02.h"

namespace SystemModules::Modules {
class Serializer_CardInfo : public Framework::Components::I2CDevice24C02 {
 public:
  Serializer_CardInfo(
    Logger::ILogger *logger,
    const std::string &path);

  [[nodiscard]] Framework::Components::ParameterItemIn<std::string> *Manufacturer() const;

  [[nodiscard]] Framework::Components::ParameterItemIn<std::string> *CardTypeString() const;

  [[nodiscard]] Framework::Components::ParameterItemIn<Types::ModuleType> *CardType() const;

  [[nodiscard]] Framework::Components::ParameterItemIn<std::string> *BoardRevision() const;

  [[nodiscard]] Framework::Components::ParameterItemIn<std::string> *SerialNumber() const;

  [[nodiscard]] Framework::Components::ItemInOut<std::chrono::minutes> *OperatingHours() const;

  [[nodiscard]] Framework::Components::ParameterItemIn<std::string> *MD5() const;

  [[nodiscard]] Framework::Components::ParameterItemIn<std::string> *UUID() const;

 private:
  Framework::Components::ParameterItemIn<std::string> *manufacturer_;
  Framework::Components::ParameterItemIn<std::string> *boardNameString_;
  Framework::Components::ParameterItemIn<Types::ModuleType> *boardName_;
  Framework::Components::ParameterItemIn<std::string> *boardRevision_;
  Framework::Components::ParameterItemIn<std::string> *serialNumber_;
  Framework::Components::ItemInOut<std::chrono::minutes> *operatingHours_;
  Framework::Components::ParameterItemIn<std::string> *md5_;
  Framework::Components::ParameterItemIn<std::string> *uuid_;

  static const char kName[];
  static const char kDescription[];
  static const uint16_t kAddress;


  static const char kManufacturerName[];
  static const char kManufacturerDescription[];
  static const uint32_t kManufacturerAddress;
  static const uint32_t kManufacturerSize;

  static const char kBoardNameStringName[];
  static const char kBoardNameName[];
  static const char kBoardNameDescription[];
  static const uint32_t kBoardNameAddress;
  static const uint32_t kBoardNameSize;

  static const char kBoardRevisionName[];
  static const char kBoardRevisionDescription[];
  static const uint32_t kBoardRevisionAddress;
  static const uint32_t kBoardRevisionSize;

  static const char kSerialNumberName[];
  static const char kSerialNumberDescription[];
  static const uint32_t kSerialNumberAddress;
  static const uint32_t kSerialNumberSize;

  static const char kOperatingHoursName[];
  static const char kOperatingHoursDescription[];
  static const uint32_t kOperatingHoursAddress;
  static const uint32_t kOperatingHoursSize;

  static const char kMD5Name[];
  static const char kMD5Description[];
  static const uint32_t kMD5Address;
  static const uint32_t kMD5Size;

  static const char kUUIDName[];
  static const char kUUIDDescription[];
  static const uint32_t kUUIDAddress;
  static const uint32_t kUUIDSize;
};
}  // namespace SystemModules::Modules
