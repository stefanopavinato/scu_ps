/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Builder/Actions/ActionBuilder.h"
#include "SystemModules/Framework/Builder/Accessors/ReadAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Accessors/WriteAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Accessors/ReadWriteAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemInOutBuilder.h"
#include "SystemModules/Framework/Builder/Components/ParameterItemInBuilder.h"
#include "SystemModules/Framework/Builder/Components/ComponentBuilder.h"
#include "Serializer_CardInfo.h"

namespace SystemModules::Modules {

const char Serializer_CardInfo::kName[] = "EEProm";
const char Serializer_CardInfo::kDescription[] = "Default management EEProm";
const uint16_t Serializer_CardInfo::kAddress = 0x50;

const char Serializer_CardInfo::kManufacturerName[] = "Manufacturer";
const char Serializer_CardInfo::kManufacturerDescription[] = "";
const uint32_t Serializer_CardInfo::kManufacturerAddress = 0x00;
const uint32_t Serializer_CardInfo::kManufacturerSize = 32;

const char Serializer_CardInfo::kBoardNameStringName[] = "BoardNameString";
const char Serializer_CardInfo::kBoardNameName[] = "BoardName";
const char Serializer_CardInfo::kBoardNameDescription[] = "";
const uint32_t Serializer_CardInfo::kBoardNameAddress = 0x20;
const uint32_t Serializer_CardInfo::kBoardNameSize = 16;

const char Serializer_CardInfo::kBoardRevisionName[] = "BoardRevision";
const char Serializer_CardInfo::kBoardRevisionDescription[] = "";
const uint32_t Serializer_CardInfo::kBoardRevisionAddress = 0x30;
const uint32_t Serializer_CardInfo::kBoardRevisionSize = 16;

const char Serializer_CardInfo::kSerialNumberName[] = "SerialNumber";
const char Serializer_CardInfo::kSerialNumberDescription[] = "";
const uint32_t Serializer_CardInfo::kSerialNumberAddress = 0x40;
const uint32_t Serializer_CardInfo::kSerialNumberSize = 16;

const char Serializer_CardInfo::kOperatingHoursName[] = "OperatingHours";
const char Serializer_CardInfo::kOperatingHoursDescription[] = "";
const uint32_t Serializer_CardInfo::kOperatingHoursAddress = 0x60;
const uint32_t Serializer_CardInfo::kOperatingHoursSize = 4;

const char Serializer_CardInfo::kMD5Name[] = "MD5";
const char Serializer_CardInfo::kMD5Description[] = "";
const uint32_t Serializer_CardInfo::kMD5Address = 0x80;
const uint32_t Serializer_CardInfo::kMD5Size = 32;

const char Serializer_CardInfo::kUUIDName[] = "UUID";
const char Serializer_CardInfo::kUUIDDescription[] = "";
const uint32_t Serializer_CardInfo::kUUIDAddress = 0xFC;
const uint32_t Serializer_CardInfo::kUUIDSize = 4;

Serializer_CardInfo::Serializer_CardInfo(
  Logger::ILogger *logger,
  const std::string &path)
  : I2CDevice24C02(
  logger,
  kName,
  kDescription,
  kAddress,
  path) {
  // Create value manufacturer
  manufacturer_ = Create().ParameterIn<std::string>(
    kManufacturerName,
    kManufacturerDescription,
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  manufacturer_->CreateAccessor().I2CEEProm<std::string>(
    GetDevicePath(),
    kManufacturerAddress,
    kManufacturerSize,
    "",
    "");
  boardNameString_ = Create().ParameterIn<std::string>(
    kBoardNameStringName,
    kBoardNameDescription,
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  boardNameString_->CreateAccessor().I2CEEProm<std::string>(
    GetDevicePath(),
    kBoardNameAddress,
    kBoardNameSize,
    "",
    "");
  boardName_ = Create().ParameterIn<Types::ModuleType>(
    kBoardNameName,
    kBoardNameDescription,
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  boardName_->CreateAccessor().I2CEEProm<std::string>(
    GetDevicePath(),
    kBoardNameAddress,
    kBoardNameSize,
    "",
    "");

  boardRevision_ = Create().ParameterIn<std::string>(
    kBoardRevisionName,
    kBoardRevisionDescription,
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  boardRevision_->CreateAccessor().I2CEEProm<std::string>(
    GetDevicePath(),
    kBoardRevisionAddress,
    kBoardRevisionSize,
    "",
    "");

  serialNumber_ = Create().ParameterIn<std::string>(
    kSerialNumberName,
    kSerialNumberDescription,
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  serialNumber_->CreateAccessor().I2CEEProm<std::string>(
    GetDevicePath(),
    kSerialNumberAddress,
    kSerialNumberSize,
    "",
    "");

  operatingHours_ = Create().ItemInOut<std::chrono::minutes>(
    kOperatingHoursName,
    kOperatingHoursDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::minutes(15),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_CHANGE,
    true).AddAndReturnPointer();
  operatingHours_->CreateAccessor().I2CEEProm<uint32_t>(
    GetDevicePath(),
    kOperatingHoursAddress,
    kOperatingHoursSize,
    15,
    1);
  operatingHours_->CreateAction().Miscellaneous().Increment(
    operatingHours_->GetValueIn(),
    std::chrono::minutes(15));

  md5_ = Create().ParameterIn<std::string>(
    kMD5Name,
    kMD5Description,
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  md5_->CreateAccessor().I2CEEProm<std::string>(
    GetDevicePath(),
    kMD5Address,
    kMD5Size,
    "",
    "");

  uuid_ = Create().ParameterIn<std::string>(
    kUUIDName,
    kUUIDDescription,
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  uuid_->CreateAccessor().I2CEEProm<std::string>(
    GetDevicePath(),
    kUUIDAddress,
    kUUIDSize,
    "",
    "");
}

Framework::Components::ParameterItemIn<std::string> *Serializer_CardInfo::Manufacturer() const {
  return manufacturer_;
}

Framework::Components::ParameterItemIn<std::string> *Serializer_CardInfo::CardTypeString() const {
  return boardNameString_;
}

Framework::Components::ParameterItemIn<Types::ModuleType> *Serializer_CardInfo::CardType() const {
  return boardName_;
}

Framework::Components::ParameterItemIn<std::string> *Serializer_CardInfo::BoardRevision() const {
  return boardRevision_;
}

Framework::Components::ParameterItemIn<std::string> *Serializer_CardInfo::SerialNumber() const {
  return serialNumber_;
}

Framework::Components::ItemInOut<std::chrono::minutes> *Serializer_CardInfo::OperatingHours() const {
  return operatingHours_;
}

Framework::Components::ParameterItemIn<std::string> *Serializer_CardInfo::MD5() const {
  return md5_;
}

Framework::Components::ParameterItemIn<std::string> *Serializer_CardInfo::UUID() const {
  return uuid_;
}

}  // namespace SystemModules::Modules
