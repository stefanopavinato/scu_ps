/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "SystemModules/Framework/Components/ItemOut.h"
#include "SystemModules/Framework/Components/Container.h"
#include "SystemModules/Modules/Configuration/ConfigurationCommon.h"

namespace SystemModules::Modules {
class Serializer_Gpio : public Framework::Components::Container {
 public:
  Serializer_Gpio(
    Logger::ILogger *logger,
    const Modules::Configuration::ConfigurationCommon &configCommon,
    bool *powerEnablePullUpDrive);

  [[nodiscard]] Framework::Components::ItemOut<bool> *PowerEnablePullUpDrive() const;

 private:
  Framework::Components::ItemOut<bool> *powerEnablePullUpDrive_;

  static const char kName[];
  static const char kDescription[];

  static const char kPowerEnablePullUpDriveName[];
  static const char kPowerEnablePullUpDriveDescription[];
  static const uint32_t kPowerEnablePullUpDriveGpio;
};

}  // namespace SystemModules::Modules
