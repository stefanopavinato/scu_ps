/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "SystemModules/Framework/Components/ParameterItemIn.h"
#include "SystemModules/Framework/Components/Container.h"
#include "SystemModules/Modules/Configuration/ConfigurationSerializerSvnInfo.h"

namespace SystemModules::Modules {
class Serializer_SvnInfo : public Framework::Components::Container {
 public:
  Serializer_SvnInfo(
    Logger::ILogger *logger,
    const Configuration::ConfigurationSerializerSvnInfo &config,
    const std::string &name,
    const std::string &description);

  [[nodiscard]] Framework::Components::ParameterItemIn<uint32_t> *Version() const;

  [[nodiscard]] Framework::Components::ParameterItemIn<std::string> *Branch() const;

  [[nodiscard]] Framework::Components::ParameterItemIn<uint32_t> *Timestamp() const;

  [[nodiscard]] Framework::Components::ParameterItemIn<std::string> *Date() const;

  [[nodiscard]] Framework::Components::ParameterItemIn<std::string> *Time() const;

  [[nodiscard]] Framework::Components::ParameterItemIn<bool> *IsDirty() const;

 private:
  Framework::Components::ParameterItemIn<uint32_t> *version_;

  Framework::Components::ParameterItemIn<std::string> *branch_;

  Framework::Components::ParameterItemIn<uint32_t> *timestamp_;

  Framework::Components::ParameterItemIn<std::string> *date_;

  Framework::Components::ParameterItemIn<std::string> *time_;

  Framework::Components::ParameterItemIn<bool> *isDirty_;

  static const char kVersionName[];
  static const char kVersionDescription[];

  static const char kBranchName[];
  static const char kBranchDescription[];

  static const char kTimestampName[];
  static const char kTimestampDescription[];

  static const char kDateName[];
  static const char kDateDescription[];

  static const char kTimeName[];
  static const char kTimeDescription[];

  static const char kIsDirtyName[];
  static const char kIsDirtyDescription[];
};

}  // namespace SystemModules::Modules
