/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Types/ScuStatus.h"
#include "SystemModules/Modules/Common/Transceiver.h"
#include "SystemModules/Framework/Components/ItemIn.h"
#include "SystemModules/Framework/Components/ItemOut.h"
#include "SystemModules/Framework/Components/ItemInOut.h"
#include "SystemModules/Framework/Components/Container.h"
#include "SystemModules/Modules/Configuration/ConfigurationCommon.h"

namespace SystemModules::Modules::Serializer {
class Serializer_SLinkInterface : public Framework::Components::Container {
 public:
  Serializer_SLinkInterface(
    Logger::ILogger *logger,
    std::string const &name,
    const Modules::Configuration::ConfigurationCommon &configCommon,
    uint64_t const &baseAddress,
    uint64_t const &sLinkAddress);

  [[nodiscard]] SystemModules::Modules::Common::Transceiver *Transceiver() const;

  [[nodiscard]] Framework::Components::ItemIn <uint16_t> *LocalId() const;

  [[nodiscard]] Framework::Components::ItemInOut <Types::ScuStatus> *LocalStatus() const;

  [[nodiscard]] Framework::Components::ItemIn <uint32_t> *TxFastChannelSequenceNumberErrors() const;

  [[nodiscard]] Framework::Components::ItemIn <uint32_t> *TxNestedChannelSequenceNumberErrors() const;

  [[nodiscard]] Framework::Components::ItemIn <uint32_t> *TxNestedChannelFSMErrors() const;

  [[nodiscard]] Framework::Components::ItemIn <uint8_t> *AssociatedGtLane() const;

  [[nodiscard]] Framework::Components::ItemOut<bool> *SLinkClearErrors() const;

 private:
  SystemModules::Modules::Common::Transceiver *transceiver_;

  Framework::Components::ItemIn <uint16_t> *localId_;

  Framework::Components::ItemInOut <Types::ScuStatus> *localStatus_;

  Framework::Components::ItemIn <uint32_t> *txFastChannelSequenceNumberErrors_;

  Framework::Components::ItemIn <uint32_t> *txNestedChannelSequenceNumberErrors_;

  Framework::Components::ItemIn <uint32_t> *txNestedChannelFSMErrors_;

  Framework::Components::ItemIn <uint8_t> *associatedGtLane_;

  Framework::Components::ItemOut<bool> *sLinkClearErrors_;

  static const char kDescription[];

  static const char kTransceiverName[];
  static const uint32_t kTransceiverAddress;

  static const char kLocalIdName[];
  static const char kLocalIdDescription[];
  static const uint32_t kLocalIdAddressOffset;
  static const uint32_t kLocalIdPosition;
  static const uint32_t kLocalIdSize;

  static const char kLocalStatusName[];
  static const char kLocalStatusDescription[];
  static const uint32_t kLocalStatusAddressOffset;
  static const uint32_t kLocalStatusPosition;
  static const uint32_t kLocalStatusSize;

  static const char kTxFastChannelSequenceNumberErrorsName[];
  static const char kTxFastChannelSequenceNumberErrorsDescription[];
  static const uint32_t kTxFastChannelSequenceNumberErrorsAddressOffset;
  static const uint32_t kTxFastChannelSequenceNumberErrorsPosition;
  static const uint32_t kTxFastChannelSequenceNumberErrorsSize;

  static const char kTxNestedChannelSequenceNumberErrorsName[];
  static const char kTxNestedChannelSequenceNumberErrorsDescription[];
  static const uint32_t kTxNestedChannelSequenceNumberErrorsAddressOffset;
  static const uint32_t kTxNestedChannelSequenceNumberErrorsPosition;
  static const uint32_t kTxNestedChannelSequenceNumberErrorsSize;

  static const char kTxNestedChannelFSMErrorsName[];
  static const char kTxNestedChannelFSMErrorsDescription[];
  static const uint32_t kTxNestedChannelFSMErrorsAddressOffset;
  static const uint32_t kTxNestedChannelFSMErrorsPosition;
  static const uint32_t kTxNestedChannelFSMErrorsSize;

  static const char kAssociatedGtLaneName[];
  static const char kAssociatedGtLaneDescription[];
  static const uint32_t kAssociatedGtLaneAddressOffset;
  static const uint32_t kAssociatedGtLanePosition;
  static const uint32_t kAssociatedGtLaneSize;

  static const char kSLinkClearErrorsName[];
  static const char kSLinkClearErrorsDescription[];
  static const uint32_t kSLinkClearErrorsAddressOffset;
  static const uint32_t kSLinkClearErrorsPosition;
  static const uint32_t kSLinkClearErrorsSize;
};
}  // namespace SystemModules::Modules::Serializer

// 0 bis 3 Instanzen eines "SLink Interface"
// wieviele kann über Register an Addresse 14 ausgelesen werden. Für jede "SLink Interface"-Instanz
// Jede "SLink Interface"-instanz verfügt über folgende InputItems:
//  - local ID (uint16_t)
//  - local status (gleich wie status register oben)
//  - Tx Fast Channel Sequence number errors (uint32_t)
//  - Tx Nested Channel Sequence number errors (uint32_t)
//  - Tx Nested Channel FSM errors (uint32_t)
//  Associated gt_lane (uint8_t)
//


