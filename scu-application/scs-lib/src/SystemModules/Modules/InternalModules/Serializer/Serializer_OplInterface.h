/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Types/OplStatus.h"
#include "SystemModules/Modules/Common/Transceiver.h"
#include "SystemModules/Framework/Components/ItemIn.h"
#include "SystemModules/Framework/Components/ItemOut.h"
#include "SystemModules/Framework/Components/ParameterItemIn.h"
#include "SystemModules/Framework/Components/Container.h"
#include "SystemModules/Modules/Configuration/ConfigurationCommon.h"
#include "SystemModules/Modules/Configuration/ConfigurationSerailizer.h"

namespace SystemModules::Modules::Serializer {
class Serializer_OplInterface : public Framework::Components::Container {
 public:
  Serializer_OplInterface(
    Logger::ILogger *logger,
    const Modules::Configuration::ConfigurationCommon &configCommon,
    const Configuration::ConfigurationSerializer &configuration);

  [[nodiscard]] SystemModules::Modules::Common::Transceiver *TransceiverA() const;

  [[nodiscard]]  SystemModules::Modules::Common::Transceiver *TransceiverB() const;

  [[nodiscard]] Framework::Components::ItemIn <bool> *GlobalBeamPermit() const;

  [[nodiscard]] Framework::Components::ItemIn <Types::OplStatus> *USOplNeighbourStatus() const;

  [[nodiscard]] Framework::Components::ItemIn <Types::OplStatus> *USOplNeighbourAfterNextStatus() const;

  [[nodiscard]] Framework::Components::ItemIn <Types::OplStatus> *DSOplNeighbourStatus() const;

  [[nodiscard]] Framework::Components::ItemIn <Types::OplStatus> *DSOplNeighbourAfterNextStatus() const;

  [[nodiscard]] Framework::Components::ItemIn <Types::OplStatus> *USOplTxLocal() const;

  [[nodiscard]] Framework::Components::ItemIn <Types::OplStatus> *USOplTxNeighour() const;

  [[nodiscard]] Framework::Components::ItemIn <Types::OplStatus> *DSOplTxLocal() const;

  [[nodiscard]] Framework::Components::ItemIn <Types::OplStatus> *DSOplTxNeighour() const;

  [[nodiscard]] Framework::Components::ItemIn <uint16_t> *DSOplErrors() const;

  [[nodiscard]] Framework::Components::ItemIn <uint16_t> *USOplErrors() const;

  [[nodiscard]] Framework::Components::ItemIn <uint32_t> *DSOplEncodingSupervisionErrors() const;

  [[nodiscard]] Framework::Components::ItemIn <uint32_t> *DSOplLifeSignErrors() const;

  [[nodiscard]] Framework::Components::ItemIn <uint32_t> *DSOplRxFlagErrors() const;

  [[nodiscard]] Framework::Components::ItemIn <uint32_t> *DSOplTxFrameDropped() const;

  [[nodiscard]] Framework::Components::ItemIn <uint32_t> *DSOplRxFrameDropped() const;

  [[nodiscard]] Framework::Components::ItemIn <uint32_t> *DSOplRxFrameErrors() const;

  [[nodiscard]] Framework::Components::ItemIn <uint32_t> *DSOplNeighbourStateErrors() const;

  [[nodiscard]] Framework::Components::ItemIn <uint32_t> *DSOplNeighbourAfterNextStateErrors() const;

  [[nodiscard]] Framework::Components::ItemIn <uint32_t> *USOplEncodingSupervisionErrors() const;

  [[nodiscard]] Framework::Components::ItemIn <uint32_t> *USOplLifeSignErrors() const;

  [[nodiscard]] Framework::Components::ItemIn <uint32_t> *USOplRxFlagErrors() const;

  [[nodiscard]] Framework::Components::ItemIn <uint32_t> *USOplTxFrameDropped() const;

  [[nodiscard]] Framework::Components::ItemIn <uint32_t> *USOplRxFrameDropped() const;

  [[nodiscard]] Framework::Components::ItemIn <uint32_t> *USOplRxFrameErrors() const;

  [[nodiscard]] Framework::Components::ItemIn <uint32_t> *USOplNeighbourStateErrors() const;

  [[nodiscard]] Framework::Components::ItemIn <uint32_t> *USOplNeighbourAfterNextStateErrors() const;

  [[nodiscard]] Framework::Components::ItemOut<bool> *OplClearErrors() const;

  [[nodiscard]] Framework::Components::ParameterItemIn<bool> *IntermediateNode() const;

  [[nodiscard]] Framework::Components::ItemIn <uint32_t> *OplStatusCollection() const;

  [[nodiscard]] Framework::Components::ParameterItemIn <uint32_t> *OplModeRegister() const;

  [[nodiscard]] Framework::Components::ParameterItemIn <uint32_t> *TransceiverRouteOplGtARegister() const;

  [[nodiscard]] Framework::Components::ItemIn <uint32_t> *DSOplErrorsMaxValue() const;

  [[nodiscard]] Framework::Components::ItemIn <uint32_t> *USOplErrorsMaxValue() const;

  [[nodiscard]] Framework::Components::ParameterItemIn <uint32_t> *LeakyBucketMaxValueRegister() const;

  [[nodiscard]] Framework::Components::ParameterItemIn <uint32_t> *LeakyBucketThresholdRegister() const;

  [[nodiscard]] Framework::Components::ParameterItemIn <uint32_t> *LeakyBucketIncrementValueRegister() const;


 private:
  SystemModules::Modules::Common::Transceiver *transceiverA_;

  SystemModules::Modules::Common::Transceiver *transceiverB_;

  Framework::Components::ItemIn <bool> *globalBeamPermit_;

  Framework::Components::ItemIn <Types::OplStatus> *usOplNeighbourStatus_;

  Framework::Components::ItemIn <Types::OplStatus> *usOplNeighbourAfterNextStatus_;

  Framework::Components::ItemIn <Types::OplStatus> *dsOplNeighbourStatus_;

  Framework::Components::ItemIn <Types::OplStatus> *dsOplNeighbourAfterNextStatus_;

  Framework::Components::ItemIn <Types::OplStatus> *usOplTxLocal_;

  Framework::Components::ItemIn <Types::OplStatus> *usOplTxNeighour_;

  Framework::Components::ItemIn <Types::OplStatus> *dsOplTxLocal_;

  Framework::Components::ItemIn <Types::OplStatus> *dsOplTxNeighour_;

  Framework::Components::ItemIn <uint16_t> *dsOplErrors_;

  Framework::Components::ItemIn <uint16_t> *usOplErrors_;

  Framework::Components::ItemIn <uint32_t> *dsOplEncodingSupervisionErrors_;

  Framework::Components::ItemIn <uint32_t> *dsOplLifeSignErrors_;

  Framework::Components::ItemIn <uint32_t> *dsOplRxFlagErrors_;

  Framework::Components::ItemIn <uint32_t> *dsOplTxFrameDropped_;

  Framework::Components::ItemIn <uint32_t> *dsOplRxFrameDropped_;

  Framework::Components::ItemIn <uint32_t> *dsOplRxFrameErrors_;

  Framework::Components::ItemIn <uint32_t> *dsOplNeighbourStateErrors_;

  Framework::Components::ItemIn <uint32_t> *dsOplNeighbourAfterNextStateErrors_;

  Framework::Components::ItemIn <uint32_t> *usOplEncodingSupervisionErrors_;

  Framework::Components::ItemIn <uint32_t> *usOplLifeSignErrors_;

  Framework::Components::ItemIn <uint32_t> *usOplRxFlagErrors_;

  Framework::Components::ItemIn <uint32_t> *usOplTxFrameDropped_;

  Framework::Components::ItemIn <uint32_t> *usOplRxFrameDropped_;

  Framework::Components::ItemIn <uint32_t> *usOplRxFrameErrors_;

  Framework::Components::ItemIn <uint32_t> *usOplNeighbourStateErrors_;

  Framework::Components::ItemIn <uint32_t> *usOplNeighbourAfterNextStateErrors_;

  Framework::Components::ItemOut<bool> *oplClearErrors_;

  Framework::Components::ParameterItemIn<bool> *intermediateNode_;

  Framework::Components::ItemIn <uint32_t> *oplStatusCollection_;

  Framework::Components::ParameterItemIn <uint32_t> *oplModeRegister_;

  Framework::Components::ParameterItemIn <uint32_t> *transceiverRouteOplGtARegister_;

  Framework::Components::ItemIn <uint32_t> *dsOplErrorsMaxValue_;

  Framework::Components::ItemIn <uint32_t> *usOplErrorsMaxValue_;

  Framework::Components::ParameterItemIn <uint32_t> *leakyBucketMaxValueRegister_;

  Framework::Components::ParameterItemIn <uint32_t> *leakyBucketThresholdRegister_;

  Framework::Components::ParameterItemIn <uint32_t> *leakyBucketIncrementValueRegister_;

  static const char kName[];
  static const char kDescription[];

  static const char kTransceiverAName[];
  static const uint32_t kTransceiverAAddress;

  static const char kTransceiverBName[];
  static const uint32_t kTransceiverBAddress;

  static const char kGlobalBeamPermitName[];
  static const char kGlobalBeamPermitDescription[];
  static const uint32_t kGlobalBeamPermitAddress;
  static const uint32_t kGlobalBeamPermitPosition;
  static const uint32_t kGlobalBeamPermitSize;

  static const char kUSOplNeighbourStatusName[];
  static const char kUSOplNeighbourStatusDescription[];
  static const uint32_t kUSOplNeighbourStatusAddress;
  static const uint32_t kUSOplNeighbourStatusPosition;
  static const uint32_t kUSOplNeighbourStatusSize;

  static const char kUSOplNeighbourAfterNextStatusName[];
  static const char kUSOplNeighbourAfterNextStatusDescription[];
  static const uint32_t kUSOplNeighbourAfterNextStatusAddress;
  static const uint32_t kUSOplNeighbourAfterNextStatusPosition;
  static const uint32_t kUSOplNeighbourAfterNextStatusSize;

  static const char kDSOplNeighbourStatusName[];
  static const char kDSOplNeighbourStatusDescription[];
  static const uint32_t kDSOplNeighbourStatusAddress;
  static const uint32_t kDSOplNeighbourStatusPosition;
  static const uint32_t kDSOplNeighbourStatusSize;

  static const char kDSOplNeighbourAfterNextStatusName[];
  static const char kDSOplNeighbourAfterNextStatusDescription[];
  static const uint32_t kDSOplNeighbourAfterNextStatusAddress;
  static const uint32_t kDSOplNeighbourAfterNextStatusPosition;
  static const uint32_t kDSOplNeighbourAfterNextStatusSize;

  static const char kUSOplTxLocalName[];
  static const char kUSOplTxLocalDescription[];
  static const uint32_t kUSOplTxLocalAddress;
  static const uint32_t kUSOplTxLocalPosition;
  static const uint32_t kUSOplTxLocalSize;

  static const char kUSOplTxNeighourName[];
  static const char kUSOplTxNeighourDescription[];
  static const uint32_t kUSOplTxNeighourAddress;
  static const uint32_t kUSOplTxNeighourPosition;
  static const uint32_t kUSOplTxNeighourSize;

  static const char kDSOplTxLocalName[];
  static const char kDSOplTxLocalDescription[];
  static const uint32_t kDSOplTxLocalAddress;
  static const uint32_t kDSOplTxLocalPosition;
  static const uint32_t kDSOplTxLocalSize;

  static const char kDSOplTxNeighourName[];
  static const char kDSOplTxNeighourDescription[];
  static const uint32_t kDSOplTxNeighourAddress;
  static const uint32_t kDSOplTxNeighourPosition;
  static const uint32_t kDSOplTxNeighourSize;

  static const char kDSOplErrorsName[];
  static const char kDSOplErrorsDescription[];
  static const uint32_t kDSOplErrorsAddress;
  static const uint32_t kDSOplErrorsPosition;
  static const uint32_t kDSOplErrorsSize;

  static const char kUSOplErrorsName[];
  static const char kUSOplErrorsDescription[];
  static const uint32_t kUSOplErrorsAddress;
  static const uint32_t kUSOplErrorsPosition;
  static const uint32_t kUSOplErrorsSize;

  static const char kDSOplEncodingSupervisionErrorsName[];
  static const char kDSOplEncodingSupervisionErrorsDescription[];
  static const uint32_t kDSOplEncodingSupervisionErrorsAddress;
  static const uint32_t kDSOplEncodingSupervisionErrorsPosition;
  static const uint32_t kDSOplEncodingSupervisionErrorsSize;

  static const char kDSOplLifeSignErrorsName[];
  static const char kDSOplLifeSignErrorsDescription[];
  static const uint32_t kDSOplLifeSignErrorsAddress;
  static const uint32_t kDSOplLifeSignErrorsPosition;
  static const uint32_t kDSOplLifeSignErrorsSize;

  static const char kDSOplRxFlagErrorsName[];
  static const char kDSOplRxFlagErrorsDescription[];
  static const uint32_t kDSOplRxFlagErrorsAddress;
  static const uint32_t kDSOplRxFlagErrorsPosition;
  static const uint32_t kDSOplRxFlagErrorsSize;

  static const char kDSOplTxFrameDroppedName[];
  static const char kDSOplTxFrameDroppedDescription[];
  static const uint32_t kDSOplTxFrameDroppedAddress;
  static const uint32_t kDSOplTxFrameDroppedPosition;
  static const uint32_t kDSOplTxFrameDroppedSize;

  static const char kDSOplRxFrameDroppedName[];
  static const char kDSOplRxFrameDroppedDescription[];
  static const uint32_t kDSOplRxFrameDroppedAddress;
  static const uint32_t kDSOplRxFrameDroppedPosition;
  static const uint32_t kDSOplRxFrameDroppedSize;

  static const char kDSOplRxFrameErrorsName[];
  static const char kDSOplRxFrameErrorsDescription[];
  static const uint32_t kDSOplRxFrameErrorsAddress;
  static const uint32_t kDSOplRxFrameErrorsPosition;
  static const uint32_t kDSOplRxFrameErrorsSize;

  static const char kDSOplNeighbourStateErrorsName[];
  static const char kDSOplNeighbourStateErrorsDescription[];
  static const uint32_t kDSOplNeighbourStateErrorsAddress;
  static const uint32_t kDSOplNeighbourStateErrorsPosition;
  static const uint32_t kDSOplNeighbourStateErrorsSize;

  static const char kDSOplNeighbourAfterNextStateErrorsName[];
  static const char kDSOplNeighbourAfterNextStateErrorsDescription[];
  static const uint32_t kDSOplNeighbourAfterNextStateErrorsAddress;
  static const uint32_t kDSOplNeighbourAfterNextStateErrorsPosition;
  static const uint32_t kDSOplNeighbourAfterNextStateErrorsSize;

  static const char kUSOplEncodingSupervisionErrorsName[];
  static const char kUSOplEncodingSupervisionErrorsDescription[];
  static const uint32_t kUSOplEncodingSupervisionErrorsAddress;
  static const uint32_t kUSOplEncodingSupervisionErrorsPosition;
  static const uint32_t kUSOplEncodingSupervisionErrorsSize;

  static const char kUSOplLifeSignErrorsName[];
  static const char kUSOplLifeSignErrorsDescription[];
  static const uint32_t kUSOplLifeSignErrorsAddress;
  static const uint32_t kUSOplLifeSignErrorsPosition;
  static const uint32_t kUSOplLifeSignErrorsSize;

  static const char kUSOplRxFlagErrorsName[];
  static const char kUSOplRxFlagErrorsDescription[];
  static const uint32_t kUSOplRxFlagErrorsAddress;
  static const uint32_t kUSOplRxFlagErrorsPosition;
  static const uint32_t kUSOplRxFlagErrorsSize;

  static const char kUSOplTxFrameDroppedName[];
  static const char kUSOplTxFrameDroppedDescription[];
  static const uint32_t kUSOplTxFrameDroppedAddress;
  static const uint32_t kUSOplTxFrameDroppedPosition;
  static const uint32_t kUSOplTxFrameDroppedSize;

  static const char kUSOplRxFrameDroppedName[];
  static const char kUSOplRxFrameDroppedDescription[];
  static const uint32_t kUSOplRxFrameDroppedAddress;
  static const uint32_t kUSOplRxFrameDroppedPosition;
  static const uint32_t kUSOplRxFrameDroppedSize;

  static const char kUSOplRxFrameErrorsName[];
  static const char kUSOplRxFrameErrorsDescription[];
  static const uint32_t kUSOplRxFrameErrorsAddress;
  static const uint32_t kUSOplRxFrameErrorsPosition;
  static const uint32_t kUSOplRxFrameErrorsSize;

  static const char kUSOplNeighbourStateErrorsName[];
  static const char kUSOplNeighbourStateErrorsDescription[];
  static const uint32_t kUSOplNeighbourStateErrorsAddress;
  static const uint32_t kUSOplNeighbourStateErrorsPosition;
  static const uint32_t kUSOplNeighbourStateErrorsSize;

  static const char kUSOplNeighbourAfterNextStateErrorsName[];
  static const char kUSOplNeighbourAfterNextStateErrorsDescription[];
  static const uint32_t kUSOplNeighbourAfterNextStateErrorsAddress;
  static const uint32_t kUSOplNeighbourAfterNextStateErrorsPosition;
  static const uint32_t kUSOplNeighbourAfterNextStateErrorsSize;

  static const char kOplClearErrorsName[];
  static const char kOplClearErrorsDescription[];
  static const uint32_t kOplClearErrorsAddressOffset;
  static const uint32_t kOplClearErrorsPosition;
  static const uint32_t kOplClearErrorsSize;

  static const char kIntermediateNodeName[];
  static const char kIntermediateNodeDescription[];
  static const uint32_t kIntermediateNodeAddress;
  static const uint32_t kIntermediateNodePosition;
  static const uint32_t kIntermediateNodeSize;

  static const char kOplStatusCollectionName[];
  static const char kOplStatusCollectionDescription[];
  static const uint32_t kOplStatusCollectionAddress;
  static const uint32_t kOplStatusCollectionPosition;
  static const uint32_t kOplStatusCollectionSize;

  static const char kOplModeRegisterName[];
  static const char kOplModeRegisterDescription[];
  static const uint32_t kOplModeRegisterAddress;
  static const uint32_t kOplModeRegisterPosition;
  static const uint32_t kOplModeRegisterSize;

  static const char kTransceiverRouteOplGtARegisterName[];
  static const char kTransceiverRouteOplGtARegisterDescription[];
  static const uint32_t kTransceiverRouteOplGtARegisterAddress;
  static const uint32_t kTransceiverRouteOplGtARegisterPosition;
  static const uint32_t kTransceiverRouteOplGtARegisterSize;

  static const char kDSOplErrorsMaxName[];
  static const char kDSOplErrorsMaxDescription[];
  static const uint32_t kDSOplErrorsMaxAddress;
  static const uint32_t kDSOplErrorsMaxPosition;
  static const uint32_t kDSOplErrorsMaxSize;

  static const char kUSOplErrorsMaxName[];
  static const char kUSOplErrorsMaxDescription[];
  static const uint32_t kUSOplErrorsMaxAddress;
  static const uint32_t kUSOplErrorsMaxPosition;
  static const uint32_t kUSOplErrorsMaxSize;

  static const char kLeakyBucketMaxValueName[];
  static const char kLeakyBucketMaxValueDescription[];
  static const uint32_t kLeakyBucketMaxValueAddress;
  static const uint32_t kLeakyBucketMaxValuePosition;
  static const uint32_t kLeakyBucketMaxValueSize;

  static const char kLeakyBucketThresholdName[];
  static const char kLeakyBucketThresholdDescription[];
  static const uint32_t kLeakyBucketThresholdAddress;
  static const uint32_t kLeakyBucketThresholdPosition;
  static const uint32_t kLeakyBucketThresholdSize;

  static const char kLeakyBucketIncrementValueName[];
  static const char kLeakyBucketIncrementValueDescription[];
  static const uint32_t kLeakyBucketIncrementValueAddress;
  static const uint32_t kLeakyBucketIncrementValuePosition;
  static const uint32_t kLeakyBucketIncrementValueSize;
};
}  // namespace SystemModules::Modules::Serializer
