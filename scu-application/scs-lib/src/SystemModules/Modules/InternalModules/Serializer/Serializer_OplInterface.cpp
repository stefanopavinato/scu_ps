/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Builder/Accessors/ReadAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Actions/ActionBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemInBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemOutBuilder.h"
#include "SystemModules/Framework/Builder/Components/ParameterItemInBuilder.h"
#include "SystemModules/Modules/Common/Transceiver.h"
#include "Serializer_OplInterface.h"

namespace SystemModules::Modules::Serializer {

const char Serializer_OplInterface::kName[] = "OplInterface";
const char Serializer_OplInterface::kDescription[] = "";

const char Serializer_OplInterface::kTransceiverAName[] = "TransceiverA";
const uint32_t Serializer_OplInterface::kTransceiverAAddress = 76;

const char Serializer_OplInterface::kTransceiverBName[] = "TransceiverB";
const uint32_t Serializer_OplInterface::kTransceiverBAddress = 100;

const char Serializer_OplInterface::kGlobalBeamPermitName[] = "GlobalBeamPermit";
const char Serializer_OplInterface::kGlobalBeamPermitDescription[] = "";
const uint32_t Serializer_OplInterface::kGlobalBeamPermitAddress = 68;
const uint32_t Serializer_OplInterface::kGlobalBeamPermitPosition = 0;
const uint32_t Serializer_OplInterface::kGlobalBeamPermitSize = 1;

const char Serializer_OplInterface::kUSOplNeighbourStatusName[] = "USOplNeighbourStatus";
const char Serializer_OplInterface::kUSOplNeighbourStatusDescription[] = "";
const uint32_t Serializer_OplInterface::kUSOplNeighbourStatusAddress = 68;
const uint32_t Serializer_OplInterface::kUSOplNeighbourStatusPosition = 3;
const uint32_t Serializer_OplInterface::kUSOplNeighbourStatusSize = 3;

const char Serializer_OplInterface::kUSOplNeighbourAfterNextStatusName[] = "USOplNeighbourAfterNextStatus";
const char Serializer_OplInterface::kUSOplNeighbourAfterNextStatusDescription[] = "";
const uint32_t Serializer_OplInterface::kUSOplNeighbourAfterNextStatusAddress = 68;
const uint32_t Serializer_OplInterface::kUSOplNeighbourAfterNextStatusPosition = 6;
const uint32_t Serializer_OplInterface::kUSOplNeighbourAfterNextStatusSize = 3;

const char Serializer_OplInterface::kDSOplNeighbourStatusName[] = "DSOplNeighbourStatus";
const char Serializer_OplInterface::kDSOplNeighbourStatusDescription[] = "";
const uint32_t Serializer_OplInterface::kDSOplNeighbourStatusAddress = 68;
const uint32_t Serializer_OplInterface::kDSOplNeighbourStatusPosition = 9;
const uint32_t Serializer_OplInterface::kDSOplNeighbourStatusSize = 3;

const char Serializer_OplInterface::kDSOplNeighbourAfterNextStatusName[] = "DSOplNeighbourAfterNextStatus";
const char Serializer_OplInterface::kDSOplNeighbourAfterNextStatusDescription[] = "";
const uint32_t Serializer_OplInterface::kDSOplNeighbourAfterNextStatusAddress = 68;
const uint32_t Serializer_OplInterface::kDSOplNeighbourAfterNextStatusPosition = 12;
const uint32_t Serializer_OplInterface::kDSOplNeighbourAfterNextStatusSize = 3;

const char Serializer_OplInterface::kUSOplTxLocalName[] = "USOplTxLocal";
const char Serializer_OplInterface::kUSOplTxLocalDescription[] = "";
const uint32_t Serializer_OplInterface::kUSOplTxLocalAddress = 68;
const uint32_t Serializer_OplInterface::kUSOplTxLocalPosition = 21;
const uint32_t Serializer_OplInterface::kUSOplTxLocalSize = 3;

const char Serializer_OplInterface::kUSOplTxNeighourName[] = "USOplTxNeighour";
const char Serializer_OplInterface::kUSOplTxNeighourDescription[] = "";
const uint32_t Serializer_OplInterface::kUSOplTxNeighourAddress = 68;
const uint32_t Serializer_OplInterface::kUSOplTxNeighourPosition = 24;
const uint32_t Serializer_OplInterface::kUSOplTxNeighourSize = 3;

const char Serializer_OplInterface::kDSOplTxLocalName[] = "DSOplTxLocal";
const char Serializer_OplInterface::kDSOplTxLocalDescription[] = "";
const uint32_t Serializer_OplInterface::kDSOplTxLocalAddress = 68;
const uint32_t Serializer_OplInterface::kDSOplTxLocalPosition = 15;
const uint32_t Serializer_OplInterface::kDSOplTxLocalSize = 3;

const char Serializer_OplInterface::kDSOplTxNeighourName[] = "DSOplTxNeighour";
const char Serializer_OplInterface::kDSOplTxNeighourDescription[] = "";
const uint32_t Serializer_OplInterface::kDSOplTxNeighourAddress = 68;
const uint32_t Serializer_OplInterface::kDSOplTxNeighourPosition = 18;
const uint32_t Serializer_OplInterface::kDSOplTxNeighourSize = 3;

const char Serializer_OplInterface::kUSOplErrorsName[] = "DSOplErrors";
const char Serializer_OplInterface::kUSOplErrorsDescription[] = "";
const uint32_t Serializer_OplInterface::kUSOplErrorsAddress = 72;
const uint32_t Serializer_OplInterface::kUSOplErrorsPosition = 0;
const uint32_t Serializer_OplInterface::kUSOplErrorsSize = 16;

const char Serializer_OplInterface::kDSOplErrorsName[] = "USOplErrors";
const char Serializer_OplInterface::kDSOplErrorsDescription[] = "";
const uint32_t Serializer_OplInterface::kDSOplErrorsAddress = 96;
const uint32_t Serializer_OplInterface::kDSOplErrorsPosition = 0;
const uint32_t Serializer_OplInterface::kDSOplErrorsSize = 16;

const char Serializer_OplInterface::kDSOplEncodingSupervisionErrorsName[] = "DSOplEncodingSupervisionErrors";
const char Serializer_OplInterface::kDSOplEncodingSupervisionErrorsDescription[] = "";
const uint32_t Serializer_OplInterface::kDSOplEncodingSupervisionErrorsAddress = 124;
const uint32_t Serializer_OplInterface::kDSOplEncodingSupervisionErrorsPosition = 0;
const uint32_t Serializer_OplInterface::kDSOplEncodingSupervisionErrorsSize = 32;

const char Serializer_OplInterface::kDSOplLifeSignErrorsName[] = "DSOplLifeSignErrors";
const char Serializer_OplInterface::kDSOplLifeSignErrorsDescription[] = "";
const uint32_t Serializer_OplInterface::kDSOplLifeSignErrorsAddress = 128;
const uint32_t Serializer_OplInterface::kDSOplLifeSignErrorsPosition = 0;
const uint32_t Serializer_OplInterface::kDSOplLifeSignErrorsSize = 32;

const char Serializer_OplInterface::kDSOplRxFlagErrorsName[] = "DSOplRxFlagErrors";
const char Serializer_OplInterface::kDSOplRxFlagErrorsDescription[] = "";
const uint32_t Serializer_OplInterface::kDSOplRxFlagErrorsAddress = 132;
const uint32_t Serializer_OplInterface::kDSOplRxFlagErrorsPosition = 0;
const uint32_t Serializer_OplInterface::kDSOplRxFlagErrorsSize = 32;

const char Serializer_OplInterface::kDSOplTxFrameDroppedName[] = "DSOplTxFrameDropped";
const char Serializer_OplInterface::kDSOplTxFrameDroppedDescription[] = "";
const uint32_t Serializer_OplInterface::kDSOplTxFrameDroppedAddress = 136;
const uint32_t Serializer_OplInterface::kDSOplTxFrameDroppedPosition = 0;
const uint32_t Serializer_OplInterface::kDSOplTxFrameDroppedSize = 32;

const char Serializer_OplInterface::kDSOplRxFrameDroppedName[] = "DSOplRxFrameDropped";
const char Serializer_OplInterface::kDSOplRxFrameDroppedDescription[] = "";
const uint32_t Serializer_OplInterface::kDSOplRxFrameDroppedAddress = 140;
const uint32_t Serializer_OplInterface::kDSOplRxFrameDroppedPosition = 0;
const uint32_t Serializer_OplInterface::kDSOplRxFrameDroppedSize = 32;

const char Serializer_OplInterface::kDSOplRxFrameErrorsName[] = "DSOplRxFrameErrors";
const char Serializer_OplInterface::kDSOplRxFrameErrorsDescription[] = "";
const uint32_t Serializer_OplInterface::kDSOplRxFrameErrorsAddress = 144;
const uint32_t Serializer_OplInterface::kDSOplRxFrameErrorsPosition = 0;
const uint32_t Serializer_OplInterface::kDSOplRxFrameErrorsSize = 32;

const char Serializer_OplInterface::kDSOplNeighbourStateErrorsName[] = "DSOplNeighbourStateErrors";
const char Serializer_OplInterface::kDSOplNeighbourStateErrorsDescription[] = "";
const uint32_t Serializer_OplInterface::kDSOplNeighbourStateErrorsAddress = 148;
const uint32_t Serializer_OplInterface::kDSOplNeighbourStateErrorsPosition = 0;
const uint32_t Serializer_OplInterface::kDSOplNeighbourStateErrorsSize = 32;

const char Serializer_OplInterface::kDSOplNeighbourAfterNextStateErrorsName[] = "DSOplNeighbourAfterNextStateErrors";
const char Serializer_OplInterface::kDSOplNeighbourAfterNextStateErrorsDescription[] = "";
const uint32_t Serializer_OplInterface::kDSOplNeighbourAfterNextStateErrorsAddress = 152;
const uint32_t Serializer_OplInterface::kDSOplNeighbourAfterNextStateErrorsPosition = 0;
const uint32_t Serializer_OplInterface::kDSOplNeighbourAfterNextStateErrorsSize = 32;

const char Serializer_OplInterface::kUSOplEncodingSupervisionErrorsName[] = "USOplEncodingSupervisionErrors";
const char Serializer_OplInterface::kUSOplEncodingSupervisionErrorsDescription[] = "";
const uint32_t Serializer_OplInterface::kUSOplEncodingSupervisionErrorsAddress = 156;
const uint32_t Serializer_OplInterface::kUSOplEncodingSupervisionErrorsPosition = 0;
const uint32_t Serializer_OplInterface::kUSOplEncodingSupervisionErrorsSize = 32;

const char Serializer_OplInterface::kUSOplLifeSignErrorsName[] = "USOplLifeSignErrors";
const char Serializer_OplInterface::kUSOplLifeSignErrorsDescription[] = "";
const uint32_t Serializer_OplInterface::kUSOplLifeSignErrorsAddress = 160;
const uint32_t Serializer_OplInterface::kUSOplLifeSignErrorsPosition = 0;
const uint32_t Serializer_OplInterface::kUSOplLifeSignErrorsSize = 32;

const char Serializer_OplInterface::kUSOplRxFlagErrorsName[] = "USOplRxFlagErrors";
const char Serializer_OplInterface::kUSOplRxFlagErrorsDescription[] = "";
const uint32_t Serializer_OplInterface::kUSOplRxFlagErrorsAddress = 164;
const uint32_t Serializer_OplInterface::kUSOplRxFlagErrorsPosition = 0;
const uint32_t Serializer_OplInterface::kUSOplRxFlagErrorsSize = 32;

const char Serializer_OplInterface::kUSOplTxFrameDroppedName[] = "USOplTxFrameDropped";
const char Serializer_OplInterface::kUSOplTxFrameDroppedDescription[] = "";
const uint32_t Serializer_OplInterface::kUSOplTxFrameDroppedAddress = 168;
const uint32_t Serializer_OplInterface::kUSOplTxFrameDroppedPosition = 0;
const uint32_t Serializer_OplInterface::kUSOplTxFrameDroppedSize = 32;

const char Serializer_OplInterface::kUSOplRxFrameDroppedName[] = "USOplRxFrameDropped";
const char Serializer_OplInterface::kUSOplRxFrameDroppedDescription[] = "";
const uint32_t Serializer_OplInterface::kUSOplRxFrameDroppedAddress = 172;
const uint32_t Serializer_OplInterface::kUSOplRxFrameDroppedPosition = 0;
const uint32_t Serializer_OplInterface::kUSOplRxFrameDroppedSize = 32;

const char Serializer_OplInterface::kUSOplRxFrameErrorsName[] = "USOplRxFrameErrors";
const char Serializer_OplInterface::kUSOplRxFrameErrorsDescription[] = "";
const uint32_t Serializer_OplInterface::kUSOplRxFrameErrorsAddress = 176;
const uint32_t Serializer_OplInterface::kUSOplRxFrameErrorsPosition = 0;
const uint32_t Serializer_OplInterface::kUSOplRxFrameErrorsSize = 32;

const char Serializer_OplInterface::kUSOplNeighbourStateErrorsName[] = "USOplNeighbourStateErrors";
const char Serializer_OplInterface::kUSOplNeighbourStateErrorsDescription[] = "";
const uint32_t Serializer_OplInterface::kUSOplNeighbourStateErrorsAddress = 180;
const uint32_t Serializer_OplInterface::kUSOplNeighbourStateErrorsPosition = 0;
const uint32_t Serializer_OplInterface::kUSOplNeighbourStateErrorsSize = 32;

const char Serializer_OplInterface::kUSOplNeighbourAfterNextStateErrorsName[] = "USOplNeighbourAfterNextStateErrors";
const char Serializer_OplInterface::kUSOplNeighbourAfterNextStateErrorsDescription[] = "";
const uint32_t Serializer_OplInterface::kUSOplNeighbourAfterNextStateErrorsAddress = 184;
const uint32_t Serializer_OplInterface::kUSOplNeighbourAfterNextStateErrorsPosition = 0;
const uint32_t Serializer_OplInterface::kUSOplNeighbourAfterNextStateErrorsSize = 32;

const char Serializer_OplInterface::kDSOplErrorsMaxName[] = "DSOplErrorsMaxValue";
const char Serializer_OplInterface::kDSOplErrorsMaxDescription[] = "";
const uint32_t Serializer_OplInterface::kDSOplErrorsMaxAddress = 188;
const uint32_t Serializer_OplInterface::kDSOplErrorsMaxPosition = 0;
const uint32_t Serializer_OplInterface::kDSOplErrorsMaxSize = 32;

const char Serializer_OplInterface::kUSOplErrorsMaxName[] = "USOplErrorsMaxValue";
const char Serializer_OplInterface::kUSOplErrorsMaxDescription[] = "";
const uint32_t Serializer_OplInterface::kUSOplErrorsMaxAddress = 192;
const uint32_t Serializer_OplInterface::kUSOplErrorsMaxPosition = 0;
const uint32_t Serializer_OplInterface::kUSOplErrorsMaxSize = 32;

const char Serializer_OplInterface::kLeakyBucketMaxValueName[] = "LeakyBucketMaxValue";
const char Serializer_OplInterface::kLeakyBucketMaxValueDescription[] =
    "Limit for the LeakyBucket value. If exceeded an EBI will be created";
const uint32_t Serializer_OplInterface::kLeakyBucketMaxValueAddress = 196;
const uint32_t Serializer_OplInterface::kLeakyBucketMaxValuePosition = 0;
const uint32_t Serializer_OplInterface::kLeakyBucketMaxValueSize = 32;

const char Serializer_OplInterface::kLeakyBucketIncrementValueName[] = "LeakyBucketIncrementValue";
const char Serializer_OplInterface::kLeakyBucketIncrementValueDescription[] =
    "Increment value with which the LeakyBucket will be incremented upon an error";
const uint32_t Serializer_OplInterface::kLeakyBucketIncrementValueAddress = 200;
const uint32_t Serializer_OplInterface::kLeakyBucketIncrementValuePosition = 0;
const uint32_t Serializer_OplInterface::kLeakyBucketIncrementValueSize = 32;

const char Serializer_OplInterface::kLeakyBucketThresholdName[] = "LeakyBucketThreshold";
const char Serializer_OplInterface::kLeakyBucketThresholdDescription[] =
    "Threshold value for the LeakyBucket LifeSign difference check";
const uint32_t Serializer_OplInterface::kLeakyBucketThresholdAddress = 204;
const uint32_t Serializer_OplInterface::kLeakyBucketThresholdPosition = 0;
const uint32_t Serializer_OplInterface::kLeakyBucketThresholdSize = 32;

// Tbd OplClearErrors, Adress kBaseAddress_CFG + 120, 0, 1, kann nur geschrieben werden ItemOut
const char Serializer_OplInterface::kOplClearErrorsName[] = "OplErrorsClear";
const char Serializer_OplInterface::kOplClearErrorsDescription[] = "";
const uint32_t Serializer_OplInterface::kOplClearErrorsAddressOffset = 120;
const uint32_t Serializer_OplInterface::kOplClearErrorsPosition = 0;
const uint32_t Serializer_OplInterface::kOplClearErrorsSize = 1;

const char Serializer_OplInterface::kIntermediateNodeName[] = "IntermediateNode";
const char Serializer_OplInterface::kIntermediateNodeDescription[] = "";
const uint32_t Serializer_OplInterface::kIntermediateNodeAddress = 52;
const uint32_t Serializer_OplInterface::kIntermediateNodePosition = 0;
const uint32_t Serializer_OplInterface::kIntermediateNodeSize = 1;

const char Serializer_OplInterface::kOplStatusCollectionName[] = "OplStatusCollection";
const char Serializer_OplInterface::kOplStatusCollectionDescription[] = "";
const uint32_t Serializer_OplInterface::kOplStatusCollectionAddress = 68;
const uint32_t Serializer_OplInterface::kOplStatusCollectionPosition = 0;
const uint32_t Serializer_OplInterface::kOplStatusCollectionSize = 32;

const char Serializer_OplInterface::kOplModeRegisterName[] = "OplModeRegister";
const char Serializer_OplInterface::kOplModeRegisterDescription[] = "1=OPL Interface enabled, 0=OPL Interface disabled";
const uint32_t Serializer_OplInterface::kOplModeRegisterAddress = 44;
const uint32_t Serializer_OplInterface::kOplModeRegisterPosition = 0;
const uint32_t Serializer_OplInterface::kOplModeRegisterSize = 1;

const char Serializer_OplInterface::kTransceiverRouteOplGtARegisterName[] = "TransceiverRouteOplGtARegister";
const char Serializer_OplInterface::kTransceiverRouteOplGtARegisterDescription[] = "";
const uint32_t Serializer_OplInterface::kTransceiverRouteOplGtARegisterAddress = 48;
const uint32_t Serializer_OplInterface::kTransceiverRouteOplGtARegisterPosition = 0;
const uint32_t Serializer_OplInterface::kTransceiverRouteOplGtARegisterSize = 8;

Serializer_OplInterface::Serializer_OplInterface(
  Logger::ILogger *logger,
  const Modules::Configuration::ConfigurationCommon &configCommon,
  const Configuration::ConfigurationSerializer &configuration)
  : Framework::Components::Container(
  logger,
  kName,
  kDescription) {
  // Get update intervaval
  auto updateInterval = configCommon.UpdateInterval()->Medium()->Value();

  auto baseAddress_CFG = configuration.Address()->Base()->CFG()->Value();

  auto transceiverA = std::make_unique<Common::Transceiver>(
    logger,
    configCommon,
    kTransceiverAName,
    baseAddress_CFG + kTransceiverAAddress);
  transceiverA_ = transceiverA.get();
  AddComponent((std::move(transceiverA)));

  auto transceiverB = std::make_unique<Common::Transceiver>(
    logger,
    configCommon,
    kTransceiverBName,
    baseAddress_CFG + kTransceiverBAddress);
  transceiverB_ = transceiverB.get();
  AddComponent((std::move(transceiverB)));

  globalBeamPermit_ = Create().ItemIn<bool>(
    kGlobalBeamPermitName,
    kGlobalBeamPermitDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  globalBeamPermit_->CreateAccessor().ScuRegister(
    baseAddress_CFG + kGlobalBeamPermitAddress,
    kGlobalBeamPermitPosition,
    kGlobalBeamPermitSize);

  usOplNeighbourStatus_ = Create().ItemIn<Types::OplStatus>(
    kUSOplNeighbourStatusName,
    kUSOplNeighbourStatusDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  usOplNeighbourStatus_->CreateAccessor().ScuRegister(
    baseAddress_CFG + kUSOplNeighbourStatusAddress,
    kUSOplNeighbourStatusPosition,
    kUSOplNeighbourStatusSize);

  usOplNeighbourAfterNextStatus_ = Create().ItemIn<Types::OplStatus>(
    kUSOplNeighbourAfterNextStatusName,
    kUSOplNeighbourAfterNextStatusDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  usOplNeighbourAfterNextStatus_->CreateAccessor().ScuRegister(
    baseAddress_CFG + kUSOplNeighbourAfterNextStatusAddress,
    kUSOplNeighbourAfterNextStatusPosition,
    kUSOplNeighbourAfterNextStatusSize);

  dsOplNeighbourStatus_ = Create().ItemIn<Types::OplStatus>(
    kDSOplNeighbourStatusName,
    kDSOplNeighbourStatusDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  dsOplNeighbourStatus_->CreateAccessor().ScuRegister(
    baseAddress_CFG + kDSOplNeighbourStatusAddress,
    kDSOplNeighbourStatusPosition,
    kDSOplNeighbourStatusSize);

  dsOplNeighbourAfterNextStatus_ = Create().ItemIn<Types::OplStatus>(
      kDSOplNeighbourAfterNextStatusName,
      kDSOplNeighbourAfterNextStatusDescription,
      Types::ValueGroup::Enum::NONE,
      std::chrono::milliseconds(updateInterval),
      Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  dsOplNeighbourAfterNextStatus_->CreateAccessor().ScuRegister(
      baseAddress_CFG + kDSOplNeighbourAfterNextStatusAddress,
      kDSOplNeighbourAfterNextStatusPosition,
      kDSOplNeighbourAfterNextStatusSize);

  usOplTxLocal_ = Create().ItemIn<Types::OplStatus>(
      kUSOplTxLocalName,
      kUSOplTxLocalDescription,
      Types::ValueGroup::Enum::NONE,
      std::chrono::milliseconds(updateInterval),
      Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  usOplTxLocal_->CreateAccessor().ScuRegister(
      baseAddress_CFG + kUSOplTxLocalAddress,
      kUSOplTxLocalPosition,
      kUSOplTxLocalSize);

  usOplTxNeighour_ = Create().ItemIn<Types::OplStatus>(
      kUSOplTxNeighourName,
      kUSOplTxNeighourDescription,
      Types::ValueGroup::Enum::NONE,
      std::chrono::milliseconds(updateInterval),
      Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  usOplTxNeighour_->CreateAccessor().ScuRegister(
      baseAddress_CFG + kUSOplTxNeighourAddress,
      kUSOplTxNeighourPosition,
      kUSOplTxNeighourSize);

  dsOplTxLocal_ = Create().ItemIn<Types::OplStatus>(
      kDSOplTxLocalName,
      kDSOplTxLocalDescription,
      Types::ValueGroup::Enum::NONE,
      std::chrono::milliseconds(updateInterval),
      Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  dsOplTxLocal_->CreateAccessor().ScuRegister(
      baseAddress_CFG + kDSOplTxLocalAddress,
      kDSOplTxLocalPosition,
      kDSOplTxLocalSize);

  dsOplTxNeighour_ = Create().ItemIn<Types::OplStatus>(
      kDSOplTxNeighourName,
      kDSOplTxNeighourDescription,
      Types::ValueGroup::Enum::NONE,
      std::chrono::milliseconds(updateInterval),
      Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  dsOplTxNeighour_->CreateAccessor().ScuRegister(
      baseAddress_CFG + kDSOplTxNeighourAddress,
      kDSOplTxNeighourPosition,
      kDSOplTxNeighourSize);

  dsOplErrors_ = Create().ItemIn<uint16_t>(
    kDSOplErrorsName,
    kDSOplErrorsDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  dsOplErrors_->CreateAccessor().ScuRegister(
    baseAddress_CFG + kDSOplErrorsAddress,
    kDSOplErrorsPosition,
    kDSOplErrorsSize);

  usOplErrors_ = Create().ItemIn<uint16_t>(
    kUSOplErrorsName,
    kUSOplErrorsDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  usOplErrors_->CreateAccessor().ScuRegister(
    baseAddress_CFG + kUSOplErrorsAddress,
    kUSOplErrorsPosition,
    kUSOplErrorsSize);

  dsOplEncodingSupervisionErrors_ = Create().ItemIn<uint32_t>(
    kDSOplEncodingSupervisionErrorsName,
    kDSOplEncodingSupervisionErrorsDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  dsOplEncodingSupervisionErrors_->CreateAccessor().ScuRegister(
    baseAddress_CFG + kDSOplEncodingSupervisionErrorsAddress,
    kDSOplEncodingSupervisionErrorsPosition,
    kDSOplEncodingSupervisionErrorsSize);

  dsOplLifeSignErrors_ = Create().ItemIn<uint32_t>(
    kDSOplLifeSignErrorsName,
    kDSOplLifeSignErrorsDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  dsOplLifeSignErrors_->CreateAccessor().ScuRegister(
    baseAddress_CFG + kDSOplLifeSignErrorsAddress,
    kDSOplLifeSignErrorsPosition,
    kDSOplLifeSignErrorsSize);

  dsOplRxFlagErrors_ = Create().ItemIn<uint32_t>(
    kDSOplRxFlagErrorsName,
    kDSOplRxFlagErrorsDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  dsOplRxFlagErrors_->CreateAccessor().ScuRegister(
    baseAddress_CFG + kDSOplRxFlagErrorsAddress,
    kDSOplRxFlagErrorsPosition,
    kDSOplRxFlagErrorsSize);

  dsOplTxFrameDropped_ = Create().ItemIn<uint32_t>(
    kDSOplTxFrameDroppedName,
    kDSOplTxFrameDroppedDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  dsOplTxFrameDropped_->CreateAccessor().ScuRegister(
    baseAddress_CFG + kDSOplTxFrameDroppedAddress,
    kDSOplTxFrameDroppedPosition,
    kDSOplTxFrameDroppedSize);

  dsOplRxFrameDropped_ = Create().ItemIn<uint32_t>(
    kDSOplRxFrameDroppedName,
    kDSOplRxFrameDroppedDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  dsOplRxFrameDropped_->CreateAccessor().ScuRegister(
    baseAddress_CFG + kDSOplRxFrameDroppedAddress,
    kDSOplRxFrameDroppedPosition,
    kDSOplRxFrameDroppedSize);

  dsOplRxFrameErrors_ = Create().ItemIn<uint32_t>(
    kDSOplRxFrameErrorsName,
    kDSOplRxFrameErrorsDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  dsOplRxFrameErrors_->CreateAccessor().ScuRegister(
    baseAddress_CFG + kDSOplRxFrameErrorsAddress,
    kDSOplRxFrameErrorsPosition,
    kDSOplRxFrameErrorsSize);

  dsOplNeighbourStateErrors_ = Create().ItemIn<uint32_t>(
    kDSOplNeighbourStateErrorsName,
    kDSOplNeighbourStateErrorsDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  dsOplNeighbourStateErrors_->CreateAccessor().ScuRegister(
    baseAddress_CFG + kDSOplNeighbourStateErrorsAddress,
    kDSOplNeighbourStateErrorsPosition,
    kDSOplNeighbourStateErrorsSize);

  dsOplNeighbourAfterNextStateErrors_ = Create().ItemIn<uint32_t>(
    kDSOplNeighbourAfterNextStateErrorsName,
    kDSOplNeighbourAfterNextStateErrorsDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  dsOplNeighbourAfterNextStateErrors_->CreateAccessor().ScuRegister(
    baseAddress_CFG + kDSOplNeighbourAfterNextStateErrorsAddress,
    kDSOplNeighbourAfterNextStateErrorsPosition,
    kDSOplNeighbourAfterNextStateErrorsSize);

  usOplEncodingSupervisionErrors_ = Create().ItemIn<uint32_t>(
    kUSOplEncodingSupervisionErrorsName,
    kUSOplEncodingSupervisionErrorsDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  usOplEncodingSupervisionErrors_->CreateAccessor().ScuRegister(
    baseAddress_CFG + kUSOplEncodingSupervisionErrorsAddress,
    kUSOplEncodingSupervisionErrorsPosition,
    kUSOplEncodingSupervisionErrorsSize);

  usOplLifeSignErrors_ = Create().ItemIn<uint32_t>(
    kUSOplLifeSignErrorsName,
    kUSOplLifeSignErrorsDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  usOplLifeSignErrors_->CreateAccessor().ScuRegister(
    baseAddress_CFG + kUSOplLifeSignErrorsAddress,
    kUSOplLifeSignErrorsPosition,
    kUSOplLifeSignErrorsSize);

  usOplRxFlagErrors_ = Create().ItemIn<uint32_t>(
    kUSOplRxFlagErrorsName,
    kUSOplRxFlagErrorsDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  usOplRxFlagErrors_->CreateAccessor().ScuRegister(
    baseAddress_CFG + kUSOplRxFlagErrorsAddress,
    kUSOplRxFlagErrorsPosition,
    kUSOplRxFlagErrorsSize);

  usOplTxFrameDropped_ = Create().ItemIn<uint32_t>(
    kUSOplTxFrameDroppedName,
    kUSOplTxFrameDroppedDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  usOplTxFrameDropped_->CreateAccessor().ScuRegister(
    baseAddress_CFG + kUSOplTxFrameDroppedAddress,
    kUSOplTxFrameDroppedPosition,
    kUSOplTxFrameDroppedSize);

  usOplRxFrameDropped_ = Create().ItemIn<uint32_t>(
    kUSOplRxFrameDroppedName,
    kUSOplRxFrameDroppedDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  usOplRxFrameDropped_->CreateAccessor().ScuRegister(
    baseAddress_CFG + kUSOplRxFrameDroppedAddress,
    kUSOplRxFrameDroppedPosition,
    kUSOplRxFrameDroppedSize);

  usOplRxFrameErrors_ = Create().ItemIn<uint32_t>(
    kUSOplRxFrameErrorsName,
    kUSOplRxFrameErrorsDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  usOplRxFrameErrors_->CreateAccessor().ScuRegister(
    baseAddress_CFG + kUSOplRxFrameErrorsAddress,
    kUSOplRxFrameErrorsPosition,
    kUSOplRxFrameErrorsSize);

  usOplNeighbourStateErrors_ = Create().ItemIn<uint32_t>(
    kUSOplNeighbourStateErrorsName,
    kUSOplNeighbourStateErrorsDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  usOplNeighbourStateErrors_->CreateAccessor().ScuRegister(
    baseAddress_CFG + kUSOplNeighbourStateErrorsAddress,
    kUSOplNeighbourStateErrorsPosition,
    kUSOplNeighbourStateErrorsSize);

  usOplNeighbourAfterNextStateErrors_ = Create().ItemIn<uint32_t>(
    kUSOplNeighbourAfterNextStateErrorsName,
    kUSOplNeighbourAfterNextStateErrorsDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  usOplNeighbourAfterNextStateErrors_->CreateAccessor().ScuRegister(
    baseAddress_CFG + kUSOplNeighbourAfterNextStateErrorsAddress,
    kUSOplNeighbourAfterNextStateErrorsPosition,
    kUSOplNeighbourAfterNextStateErrorsSize);

  oplClearErrors_ = Create().ItemOut<bool>(
    kOplClearErrorsName,
    kOplClearErrorsDescription,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_CHANGE,
    false,
    false).AddAndReturnPointer();
  oplClearErrors_->CreateAccessor().ScuRegister(
    baseAddress_CFG + kOplClearErrorsAddressOffset,
    kOplClearErrorsPosition,
    kOplClearErrorsSize);
  oplClearErrors_->CreateAction().StaticValue(
    false);

  intermediateNode_ = Create().ParameterIn<bool>(
    kIntermediateNodeName,
    kIntermediateNodeDescription,
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  intermediateNode_->CreateAccessor().ScuRegister(
    baseAddress_CFG + kIntermediateNodeAddress,
    kIntermediateNodePosition,
    kIntermediateNodeSize);

  oplStatusCollection_ = Create().ItemIn<uint32_t>(
      kOplStatusCollectionName,
      kOplStatusCollectionDescription,
      Types::ValueGroup::Enum::NONE,
      std::chrono::milliseconds(updateInterval),
      Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  oplStatusCollection_->CreateAccessor().ScuRegister(
      baseAddress_CFG + kOplStatusCollectionAddress,
      kOplStatusCollectionPosition,
      kOplStatusCollectionSize);

  oplModeRegister_ = Create().ParameterIn<uint32_t>(
      kOplModeRegisterName,
      kOplModeRegisterDescription,
      Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  oplModeRegister_->CreateAccessor().ScuRegister(
      baseAddress_CFG + kOplModeRegisterAddress,
      kOplModeRegisterPosition,
      kOplModeRegisterSize);

  transceiverRouteOplGtARegister_ = Create().ParameterIn<uint32_t>(
      kTransceiverRouteOplGtARegisterName,
      kTransceiverRouteOplGtARegisterDescription,
      Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  transceiverRouteOplGtARegister_->CreateAccessor().ScuRegister(
      baseAddress_CFG + kTransceiverRouteOplGtARegisterAddress,
      kTransceiverRouteOplGtARegisterPosition,
      kTransceiverRouteOplGtARegisterSize);

  dsOplErrorsMaxValue_ = Create().ItemIn<uint32_t>(
      kDSOplErrorsMaxName,
      kDSOplErrorsMaxDescription,
      Types::ValueGroup::Enum::NONE,
      std::chrono::milliseconds(updateInterval),
      Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  dsOplErrorsMaxValue_->CreateAccessor().ScuRegister(
      baseAddress_CFG + kDSOplErrorsMaxAddress,
      kDSOplErrorsMaxPosition,
      kDSOplErrorsMaxSize);

  usOplErrorsMaxValue_ = Create().ItemIn<uint32_t>(
      kUSOplErrorsMaxName,
      kUSOplErrorsMaxDescription,
      Types::ValueGroup::Enum::NONE,
      std::chrono::milliseconds(updateInterval),
      Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  usOplErrorsMaxValue_->CreateAccessor().ScuRegister(
      baseAddress_CFG + kUSOplErrorsMaxAddress,
      kUSOplErrorsMaxPosition,
      kUSOplErrorsMaxSize);


  leakyBucketMaxValueRegister_ = Create().ParameterIn<uint32_t>(
      kLeakyBucketMaxValueName,
      kLeakyBucketMaxValueDescription,
      Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  leakyBucketMaxValueRegister_->CreateAccessor().ScuRegister(
      baseAddress_CFG + kLeakyBucketMaxValueAddress,
      kLeakyBucketMaxValuePosition,
      kLeakyBucketMaxValueSize);

  leakyBucketThresholdRegister_ = Create().ParameterIn<uint32_t>(
      kLeakyBucketThresholdName,
      kLeakyBucketThresholdDescription,
      Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  leakyBucketThresholdRegister_->CreateAccessor().ScuRegister(
      baseAddress_CFG + kLeakyBucketThresholdAddress,
      kLeakyBucketThresholdPosition,
      kLeakyBucketThresholdSize);

  leakyBucketIncrementValueRegister_ = Create().ParameterIn<uint32_t>(
      kLeakyBucketIncrementValueName,
      kLeakyBucketIncrementValueDescription,
      Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  leakyBucketIncrementValueRegister_->CreateAccessor().ScuRegister(
      baseAddress_CFG + kLeakyBucketIncrementValueAddress,
      kLeakyBucketIncrementValuePosition,
      kLeakyBucketIncrementValueSize);
}

SystemModules::Modules::Common::Transceiver *Serializer_OplInterface::TransceiverA() const {
  return transceiverA_;
}

SystemModules::Modules::Common::Transceiver *Serializer_OplInterface::TransceiverB() const {
  return transceiverB_;
}

Framework::Components::ItemIn<bool> *Serializer_OplInterface::GlobalBeamPermit() const {
  return globalBeamPermit_;
}

Framework::Components::ItemIn<Types::OplStatus> *Serializer_OplInterface::USOplNeighbourStatus() const {
  return usOplNeighbourStatus_;
}

Framework::Components::ItemIn<Types::OplStatus> *Serializer_OplInterface::USOplNeighbourAfterNextStatus() const {
  return usOplNeighbourAfterNextStatus_;
}

Framework::Components::ItemIn<Types::OplStatus> *Serializer_OplInterface::DSOplNeighbourStatus() const {
  return dsOplNeighbourStatus_;
}

Framework::Components::ItemIn<Types::OplStatus> *Serializer_OplInterface::DSOplNeighbourAfterNextStatus() const {
  return dsOplNeighbourAfterNextStatus_;
}

Framework::Components::ItemIn<Types::OplStatus> *Serializer_OplInterface::USOplTxLocal() const {
  return usOplTxLocal_;
}

Framework::Components::ItemIn<Types::OplStatus> *Serializer_OplInterface::USOplTxNeighour() const {
  return usOplTxNeighour_;
}

Framework::Components::ItemIn<Types::OplStatus> *Serializer_OplInterface::DSOplTxLocal() const {
  return dsOplTxLocal_;
}

Framework::Components::ItemIn<Types::OplStatus> *Serializer_OplInterface::DSOplTxNeighour() const {
  return dsOplTxNeighour_;
}

Framework::Components::ItemIn<uint16_t> *Serializer_OplInterface::DSOplErrors() const {
  return dsOplErrors_;
}

Framework::Components::ItemIn<uint16_t> *Serializer_OplInterface::USOplErrors() const {
  return usOplErrors_;
}

Framework::Components::ItemIn<uint32_t> *Serializer_OplInterface::DSOplEncodingSupervisionErrors() const {
  return dsOplEncodingSupervisionErrors_;
}

Framework::Components::ItemIn<uint32_t> *Serializer_OplInterface::DSOplLifeSignErrors() const {
  return dsOplLifeSignErrors_;
}

Framework::Components::ItemIn<uint32_t> *Serializer_OplInterface::DSOplRxFlagErrors() const {
  return dsOplRxFlagErrors_;
}

Framework::Components::ItemIn<uint32_t> *Serializer_OplInterface::DSOplTxFrameDropped() const {
  return dsOplTxFrameDropped_;
}

Framework::Components::ItemIn<uint32_t> *Serializer_OplInterface::DSOplRxFrameDropped() const {
  return dsOplRxFrameDropped_;
}

Framework::Components::ItemIn<uint32_t> *Serializer_OplInterface::DSOplRxFrameErrors() const {
  return dsOplRxFrameErrors_;
}

Framework::Components::ItemIn<uint32_t> *Serializer_OplInterface::DSOplNeighbourStateErrors() const {
  return dsOplNeighbourStateErrors_;
}

Framework::Components::ItemIn<uint32_t> *Serializer_OplInterface::DSOplNeighbourAfterNextStateErrors() const {
  return dsOplNeighbourAfterNextStateErrors_;
}
Framework::Components::ItemIn<uint32_t> *Serializer_OplInterface::USOplEncodingSupervisionErrors() const {
  return usOplEncodingSupervisionErrors_;
}

Framework::Components::ItemIn<uint32_t> *Serializer_OplInterface::USOplLifeSignErrors() const {
  return usOplLifeSignErrors_;
}

Framework::Components::ItemIn<uint32_t> *Serializer_OplInterface::USOplRxFlagErrors() const {
  return usOplRxFlagErrors_;
}

Framework::Components::ItemIn<uint32_t> *Serializer_OplInterface::USOplTxFrameDropped() const {
  return usOplTxFrameDropped_;
}

Framework::Components::ItemIn<uint32_t> *Serializer_OplInterface::USOplRxFrameDropped() const {
  return usOplRxFrameDropped_;
}

Framework::Components::ItemIn<uint32_t> *Serializer_OplInterface::USOplRxFrameErrors() const {
  return usOplRxFrameErrors_;
}

Framework::Components::ItemIn<uint32_t> *Serializer_OplInterface::USOplNeighbourStateErrors() const {
  return usOplNeighbourStateErrors_;
}

Framework::Components::ItemIn<uint32_t> *Serializer_OplInterface::USOplNeighbourAfterNextStateErrors() const {
  return usOplNeighbourAfterNextStateErrors_;
}

Framework::Components::ItemOut<bool> *Serializer_OplInterface::OplClearErrors() const {
  return oplClearErrors_;
}

Framework::Components::ParameterItemIn<bool> *Serializer_OplInterface::IntermediateNode() const {
  return intermediateNode_;
}

Framework::Components::ItemIn<uint32_t> *Serializer_OplInterface::OplStatusCollection() const {
  return oplStatusCollection_;
}

Framework::Components::ParameterItemIn<uint32_t> *Serializer_OplInterface::OplModeRegister() const {
  return oplModeRegister_;
}

Framework::Components::ParameterItemIn<uint32_t> *Serializer_OplInterface::TransceiverRouteOplGtARegister() const {
  return transceiverRouteOplGtARegister_;
}

Framework::Components::ItemIn<uint32_t> *Serializer_OplInterface::DSOplErrorsMaxValue() const {
  return dsOplErrorsMaxValue_;
}

Framework::Components::ItemIn<uint32_t> *Serializer_OplInterface::USOplErrorsMaxValue() const {
  return usOplErrorsMaxValue_;
}

Framework::Components::ParameterItemIn<uint32_t> *Serializer_OplInterface::LeakyBucketMaxValueRegister() const {
  return leakyBucketMaxValueRegister_;
}

Framework::Components::ParameterItemIn<uint32_t> *Serializer_OplInterface::LeakyBucketThresholdRegister() const {
  return leakyBucketThresholdRegister_;
}

Framework::Components::ParameterItemIn<uint32_t> *Serializer_OplInterface::LeakyBucketIncrementValueRegister() const {
  return leakyBucketIncrementValueRegister_;
}

}  // namespace SystemModules::Modules::Serializer
