/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Builder/Actions/ActionBuilder.h"
#include "SystemModules/Framework/Builder/Accessors/ReadAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Accessors/WriteAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemInBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemOutBuilder.h"
#include "Serializer_SfpModule.h"

namespace SystemModules::Modules::Serializer {

const char Serializer_SfpModule::kDescription[] = "";

const char Serializer_SfpModule::kPresentName[] = "Present";
const char Serializer_SfpModule::kPresentDescription[] = "";
const uint32_t Serializer_SfpModule::kPresentAddressOffset = 0;

const char Serializer_SfpModule::kTxFaultName[] = "TxFault";
const char Serializer_SfpModule::kTxFaultDescription[] = "";
const uint32_t Serializer_SfpModule::kTxFaultAddressOffset = 1;

const char Serializer_SfpModule::kLosName[] = "RxLos";
const char Serializer_SfpModule::kLosDescription[] = "";
const uint32_t Serializer_SfpModule::kLosAddressOffset = 2;

const char Serializer_SfpModule::kRateSelect0Name[] = "RateSelect0";
const char Serializer_SfpModule::kRateSelect0Description[] = "";
const uint32_t Serializer_SfpModule::kRateSelect0AddressOffset = 4;

const char Serializer_SfpModule::kRateSelect1Name[] = "RateSelect1";
const char Serializer_SfpModule::kRateSelect1Description[] = "";
const uint32_t Serializer_SfpModule::kRateSelect1AddressOffset = 5;

const char Serializer_SfpModule::kLocatorName[] = "Locator";
const char Serializer_SfpModule::kLocatorDescription[] = "";
const uint32_t Serializer_SfpModule::kLocatorAddressOffset = 6;

const char Serializer_SfpModule::kLinkStateName[] = "LinkState";
const char Serializer_SfpModule::kLinkStateDescription[] = "";
const uint32_t Serializer_SfpModule::kLinkStateAddressOffset = 7;

const char Serializer_SfpModule::kTxDisableName[] = "TxDisable";
const char Serializer_SfpModule::kTxDisableDescription[] = "";
const uint32_t Serializer_SfpModule::kTxDisableAddressOffset = 3;

Serializer_SfpModule::Serializer_SfpModule(
  Logger::ILogger *logger,
  const Modules::Configuration::ConfigurationCommon &configCommon,
  std::string const &name,
  uint32_t const &baseAddress)
  : Framework::Components::Container(
  logger,
  name,
  kDescription) {
  // Get update intervaval
  auto updateInterval = configCommon.UpdateInterval()->Medium()->Value();
  // Setup items
  present_ = Create().ItemIn<bool>(
    kPresentName,
    kPresentDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  present_->CreateAccessor().DigitalInput(
    baseAddress + kPresentAddressOffset,
    true);

  txFault_ = Create().ItemIn<bool>(
    kTxFaultName,
    kTxFaultDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  txFault_->CreateAccessor().DigitalInput(
    baseAddress + kTxFaultAddressOffset,
    false);

  los_ = Create().ItemIn<bool>(
    kLosName,
    kLosDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  los_->CreateAccessor().DigitalInput(
    baseAddress + kLosAddressOffset,
    false);

  // Set high-rate as default
  rateSelect0_ = Create().ItemOut<bool>(
    kRateSelect0Name,
    kRateSelect0Description,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    true,
    false).AddAndReturnPointer();
  rateSelect0_->CreateAccessor().DigitalOutput(
    baseAddress + kRateSelect0AddressOffset,
    false);
  rateSelect0_->CreateAction().StaticValue(
    true);

  // Set high-rate as default
  rateSelect1_ = Create().ItemOut<bool>(
    kRateSelect1Name,
    kRateSelect1Description,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    true,
    false).AddAndReturnPointer();
  rateSelect1_->CreateAccessor().DigitalOutput(
    baseAddress + kRateSelect1AddressOffset,
    false);
  rateSelect1_->CreateAction().StaticValue(
    true);

  locator_ = Create().ItemOut<bool>(
    kLocatorName,
    kLocatorDescription,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    true,
    false).AddAndReturnPointer();
  locator_->CreateAccessor().DigitalOutput(
    baseAddress + kLocatorAddressOffset,
    false);
  locator_->CreateAction().StaticValue(
    false);

  linkState_ = Create().ItemOut<bool>(
    kLinkStateName,
    kLinkStateDescription,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    false,
    false).AddAndReturnPointer();
  linkState_->CreateAccessor().DigitalOutput(
    baseAddress + kLinkStateAddressOffset,
    false);
  linkState_->CreateAction().StaticValue(
    false);

  txDisable_ = Create().ItemOut<bool>(
    kTxDisableName,
    kTxDisableDescription,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    true,
    false).AddAndReturnPointer();
  txDisable_->CreateAccessor().DigitalOutput(
    baseAddress + kTxDisableAddressOffset,
    false);
  txDisable_->CreateAction().StaticValue(
    false);
}

Framework::Components::ItemIn<bool> *Serializer_SfpModule::Present() const {
  return present_;
}

Framework::Components::ItemIn<bool> *Serializer_SfpModule::TxFault() const {
  return txFault_;
}

Framework::Components::ItemIn<bool> *Serializer_SfpModule::Los() const {
  return los_;
}

Framework::Components::ItemOut<bool> *Serializer_SfpModule::RateSelect0() const {
  return rateSelect0_;
}

Framework::Components::ItemOut<bool> *Serializer_SfpModule::RateSelect1() const {
  return rateSelect1_;
}

Framework::Components::ItemOut<bool> *Serializer_SfpModule::Locator() const {
  return locator_;
}

Framework::Components::ItemOut<bool> *Serializer_SfpModule::LinkState() const {
  return linkState_;
}

Framework::Components::ItemOut<bool> *Serializer_SfpModule::TxDisable() const {
  return txDisable_;
}
}  // namespace SystemModules::Modules::Serializer
