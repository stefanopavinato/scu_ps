/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Builder/Actions/ActionBuilder.h"
#include "SystemModules/Framework/Builder/Actions/ActionBuilderSerializer.h"
#include "SystemModules/Framework/Builder/Accessors/ReadAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Accessors/WriteAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Accessors/ReadWriteAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemInBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemOutBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemInOutBuilder.h"
#include "SystemModules/Framework/Builder/Components/ParameterItemInBuilder.h"
#include "SystemModules/Framework/Builder/Components/ComponentBuilder.h"
#include "Serializer.h"

namespace SystemModules::Modules::Serializer {

const char Serializer::kName[] = "Serializer";
const char Serializer::kDescription[] = "SCU Serializer";

const char Serializer::kI2CPathBase[] = "/sys/bus/i2c/devices/";

const uint32_t Serializer::kBoardGPIOBaseAddress = 400;

const char Serializer::kUpperTemperatureName[] = "UpperTemperature";
const char Serializer::kUpperTemperatureDescription[] = "Temperature at the upper pcb edge.";
const uint16_t Serializer::kUpperTemperatureAddress = 0x48;

const char Serializer::kSfpModule1Name[] = "SfpModule1";
const uint32_t Serializer::kSfpModule1BaseAddress = 448;

const char Serializer::kSfpModule2Name[] = "SfpModule2";
const uint32_t Serializer::kSfpModule2BaseAddress = 456;

const char Serializer::kSfpModule3Name[] = "SfpModule3";
const uint32_t Serializer::kSfpModule3BaseAddress = 432;

const char Serializer::kSLinkInterface1Name[] = "SLinkInterface1";

const char Serializer::kSLinkInterface2Name[] = "SLinkInterface2";

const char Serializer::kSLinkInterface3Name[] = "SLinkInterface3";

const char Serializer::kSlotName[] = "Slot";
const char Serializer::kSlotDescription[] = "";
const uint32_t Serializer::kSlotId = 499;

const char Serializer::kFrontHandleName[] = "FrontHandle";
const char Serializer::kFrontHandleDescription[] = "";
const uint32_t Serializer::kFrontHandleAddress = 232;

const char Serializer::kPgmClkStatusLockDetectName[] = "PgmClkStatusLockDetect";
const char Serializer::kPgmClkStatusLockDetectDescription[] = "";
const uint32_t Serializer::kPgmClkStatusLockDetectOffset = 16;

const char Serializer::kPgmClkStatusHoldoverName[] = "PgmClkStatusHoldover";
const char Serializer::kPgmClkStatusHoldoverDescription[] = "";
const uint32_t Serializer::kPgmClkStatusHoldoverOffset = 17;

const char Serializer::kPgmClkStatusClkIn0Name[] = "PgmClkStatusClkIn0";
const char Serializer::kPgmClkStatusClkIn0Description[] = "";
const uint32_t Serializer::kPgmClkStatusClkIn0Offset = 18;

const char Serializer::kPgmClkStatusClkIn1Name[] = "PgmClkStatusClkIn1";
const char Serializer::kPgmClkStatusClkIn1Description[] = "";
const uint32_t Serializer::kPgmClkStatusClkIn1Offset = 19;

const char Serializer::kPgmClkSyncName[] = "PgmClkSync";
const char Serializer::kPgmClkSyncDescription[] = "";
const uint32_t Serializer::kPgmClkSyncOffset = 20;

const char Serializer::kMainPowerSupplyPowerGoodName[] = "MainPowerSupplyPowerGood";
const char Serializer::kMainPowerSupplyPowerGoodDescription[] = "Main Power Supply, Power Good";
const uint32_t Serializer::kMainPowerSupplyPowerGoodOffset = 24;

const char Serializer::kMainPowerSupplyNotAlertName[] = "MainPowerSupplyNotAlert";
const char Serializer::kMainPowerSupplyNotAlertDescription[] = "Main Power Supply, NotAlert";
const uint32_t Serializer::kMainPowerSupplyNotAlertOffset = 25;

const char Serializer::kUSBPowerStatusName[] = "USBPowerStatus";
const char Serializer::kUSBPowerStatusDescription[] = "USB Power Status";
const uint32_t Serializer::kUSBPowerStatusOffset = 29;

const char Serializer::kVccInternal0V85NotAlertName[] = "VccInternal0V85NotAlert";
const char Serializer::kVccInternal0V85NotAlertDescription[] = "Internal 0.85V Supply, Not Alert";
const uint32_t Serializer::kVccInternal0V85NotAlertOffset = 28;

const char Serializer::kTemperatureSensorAlertName[] = "TemperatureSensorAlert";
const char Serializer::kTemperatureSensorAlertDescription[] = "Temperature Sensor Alert";
const uint32_t Serializer::kTemperatureSensorAlertOffset = 26;

const char Serializer::kHdcDrdyInterruptName[] = "HdcDrdyInterrupt";
const char Serializer::kHdcDrdyInterruptDescription[] = "Humidity and Temperature Sensor Interrupt";
const uint32_t Serializer::kHdcDrdyInterruptOffset = 27;

const char Serializer::kPLBuildTimestampName[] = "PLBuildTimestamp";
const char Serializer::kPLBuildTimestampDescription[] = "Build timestamp of firmware (PL-part).";
const uint32_t Serializer::kPLBuildTimestampPosition = 4;

const char Serializer::kPLBuildVersionName[] = "PLBuildVersion";
const char Serializer::kPLBuildVersionDescription[] = "Build version of firmware (PL-part).";
const uint32_t Serializer::kPLBuildVersionPosition = 8;

const char Serializer::kPLLocalIDName[] = "PLLocalID";
const char Serializer::kPLLocalIDDescription[] = "Local ID of firmware (PL-part).";
const uint32_t Serializer::kPLLocalIDPosition = 12;

const char Serializer::kNumberOfOplInterfacesName[] = "NumberOfOplInterfaces";
const char Serializer::kNumberOfOplInterfacesDescription[] =
  "Number of OPL Interfaces instantiated.";
const uint32_t Serializer::kNumberOfOplInterfacesAddress = 44;

const char Serializer::kNumberOfSLinkInterfacesName[] = "NumberOfSLinkInterfaces";
const char Serializer::kNumberOfSLinkInterfacesDescription[] =
  "Number of SLink Interfaces instantiated.";
const uint32_t Serializer::kNumberOfSLinkInterfacesAddress = 56;

const char Serializer::kStatusName[] = "SCUStatus";
const char Serializer::kStatusDescription[] = "SCU Status";
const uint32_t Serializer::kStatusAddressPosition = 16;

const char Serializer::kBeamSwitchOffStatusCollectionName[] = "BeamSwitchOffStatusCollection";
const char Serializer::kBeamSwitchOffStatusCollectionDescription[] =
  "Collection of Beam Switch-Off status items.";
const uint32_t Serializer::kBeamSwitchOffStatusCollectionAddressPosition = 28;

const char Serializer::kSWBIRequestName[] = "SWBIRequest";
const char Serializer::kSWBIRequestDescription[] =
  "Software BI Request. 0x9==Do not request BI, all others==request BI";
const uint32_t Serializer::kSWBIRequestPosition = 32;

const char Serializer::kSWRBIRequestName[] = "SWRBIRequest";
const char Serializer::kSWRBIRequestDescription[] =
  "Regular Software BI Request. 0x9==Do not request RBI, all others==request RBI";
const uint32_t Serializer::kSWRBIRequestPosition = 36;

const char Serializer::kSWEBIRequestName[] = "SWEBIRequest";
const char Serializer::kSWEBIRequestDescription[] =
  "Emergency Software BI Request. 0x9==Do not request EBI, all others==request EBI";
const uint32_t Serializer::kSWEBIRequestPosition = 40;

Serializer::Serializer(
  Logger::ILogger *logger,
  bool * powerEnablePullUpDrive,
  const Modules::Configuration::Modules::Configuration_Serializer &configSerializer,
  const Modules::Configuration::ConfigurationCommon &configCommon,
  const Configuration::ConfigurationSerializer &configuration)
  : Framework::Components::Container(
  logger,
  kName,
  kDescription) {
  // Register base addresses
  auto baseAddress_CFG = configuration.Address()->Base()->CFG()->Value();

  // Get update interval
  auto updateInterval = configCommon.UpdateInterval()->Medium()->Value();

  // Create software
  auto software_obj = std::make_unique<Serializer_Software>(
    logger,
    configuration);
  software_ = software_obj.get();
  AddComponent(std::move(software_obj));

  // Create card info
  auto cardInfo_obj = std::make_unique<Serializer_CardInfo>(
    logger,
    std::string(kI2CPathBase).append("1-0070/channel-4/"));
  cardInfo_ = cardInfo_obj.get();
  AddComponent(std::move(cardInfo_obj));

  auto gpio = std::make_unique<Serializer_Gpio>(
    logger,
    configCommon,
    powerEnablePullUpDrive);
  gpio_ = gpio.get();
  AddComponent(std::move(gpio));

  auto sfpModule1 = std::make_unique<Serializer_SfpModule>(
    logger,
    configCommon,
    kSfpModule1Name,
    kSfpModule1BaseAddress);
  sfpModule1_ = sfpModule1.get();
  AddComponent(std::move(sfpModule1));

  auto sfpModule2 = std::make_unique<Serializer_SfpModule>(
    logger,
    configCommon,
    kSfpModule2Name,
    kSfpModule2BaseAddress);
  sfpModule2_ = sfpModule2.get();
  AddComponent(std::move(sfpModule2));

  auto sfpModule3 = std::make_unique<Serializer_SfpModule>(
    logger,
    configCommon,
    kSfpModule3Name,
    kSfpModule3BaseAddress);
  sfpModule3_ = sfpModule3.get();
  AddComponent(std::move(sfpModule3));

  auto sLinkInterface1 = std::make_unique<Serializer_SLinkInterface>(
    logger,
    kSLinkInterface1Name,
    configCommon,
    configuration.Address()->Base()->CFG()->Value(),
    configuration.Address()->Base()->SLINK()->Value());
  sLinkInterface1_ = sLinkInterface1.get();
  AddComponent(std::move(sLinkInterface1));

  auto sLinkInterface2 = std::make_unique<Serializer_SLinkInterface>(
    logger,
    kSLinkInterface2Name,
    configCommon,
    configuration.Address()->Base()->CFG()->Value(),
    configuration.Address()->Base()->SLINK()->Value() +
    configuration.Address()->Offset()->SLINK()->Value());
  sLinkInterface2_ = sLinkInterface2.get();
  AddComponent(std::move(sLinkInterface2));

  auto sLinkInterface3 = std::make_unique<Serializer_SLinkInterface>(
    logger,
    kSLinkInterface3Name,
    configCommon,
    configuration.Address()->Base()->CFG()->Value(),
    configuration.Address()->Base()->SLINK()->Value() +
    configuration.Address()->Offset()->SLINK()->Value() * 2);
  sLinkInterface3_ = sLinkInterface3.get();
  AddComponent(std::move(sLinkInterface3));

  auto oplInterface = std::make_unique<Serializer_OplInterface>(
    logger,
    configCommon,
    configuration);
  oplInterface_ = oplInterface.get();
  AddComponent(std::move(oplInterface));
/*
  auto iniFile = std::make_unique<Serializer_IniFile>(
      logger);
  iniFile_ = iniFile.get();
  AddComponent(std::move(iniFile));
*/
  slot_ = Create().ParameterIn<bool>(
    kSlotName,
    kSlotDescription,
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  slot_->CreateAccessor().DigitalInput(
    kSlotId,
    false);

  // connect to zynq io (see U100-17, MIO06)
  frontHandle_ = Create().ItemIn<bool>(
    kFrontHandleName,
    kFrontHandleDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  frontHandle_->CreateAccessor().DigitalInput(
    kFrontHandleAddress,
    false);

  // Pgm Clock Status
  pgmClkStatusLockDetect_ = Create().ItemIn<bool>(
    kPgmClkStatusLockDetectName,
    kPgmClkStatusLockDetectDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  pgmClkStatusLockDetect_->CreateAccessor().DigitalInput(
    kBoardGPIOBaseAddress + kPgmClkStatusLockDetectOffset,
    false);
  pgmClkStatusHoldover_ = Create().ItemIn<bool>(
    kPgmClkStatusHoldoverName,
    kPgmClkStatusHoldoverDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  pgmClkStatusHoldover_->CreateAccessor().DigitalInput(
    kBoardGPIOBaseAddress + kPgmClkStatusHoldoverOffset,
    false);
  pgmClkStatusClkIn0_ = Create().ItemIn<bool>(
    kPgmClkStatusClkIn0Name,
    kPgmClkStatusClkIn0Description,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  pgmClkStatusClkIn0_->CreateAccessor().DigitalInput(
    kBoardGPIOBaseAddress + kPgmClkStatusClkIn0Offset,
    false);
  pgmClkStatusClkIn1_ = Create().ItemIn<bool>(
    kPgmClkStatusClkIn1Name,
    kPgmClkStatusClkIn1Description,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  pgmClkStatusClkIn1_->CreateAccessor().DigitalInput(
    kBoardGPIOBaseAddress + kPgmClkStatusClkIn1Offset,
    false);
  pgmClkSync_ = Create().ItemIn<bool>(
    kPgmClkSyncName,
    kPgmClkSyncDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  pgmClkSync_->CreateAccessor().DigitalInput(
    kBoardGPIOBaseAddress + kPgmClkSyncOffset,
    false);
  // Power status
  mainPowerSupplyPowerGood_ = Create().ItemIn<bool>(
    kMainPowerSupplyPowerGoodName,
    kMainPowerSupplyPowerGoodDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  mainPowerSupplyPowerGood_->CreateAccessor().DigitalInput(
    kBoardGPIOBaseAddress + kMainPowerSupplyPowerGoodOffset,
    false);
  mainPowerSupplyNotAlert_ = Create().ItemIn<bool>(
    kMainPowerSupplyNotAlertName,
    kMainPowerSupplyNotAlertDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  mainPowerSupplyNotAlert_->CreateAccessor().DigitalInput(
    kBoardGPIOBaseAddress + kMainPowerSupplyNotAlertOffset,
    false);
  usbPowerStatus_ = Create().ItemIn<bool>(
    kUSBPowerStatusName,
    kUSBPowerStatusDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  usbPowerStatus_->CreateAccessor().DigitalInput(
    kBoardGPIOBaseAddress + kUSBPowerStatusOffset,
    false);
  vccInternal0V85NotAlert_ = Create().ItemIn<bool>(
    kVccInternal0V85NotAlertName,
    kVccInternal0V85NotAlertDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  vccInternal0V85NotAlert_->CreateAccessor().DigitalInput(
    kBoardGPIOBaseAddress + kVccInternal0V85NotAlertOffset,
    false);
  // Temperature Alerts
  temperatureSensorAlert_ = Create().ItemIn<bool>(
    kTemperatureSensorAlertName,
    kTemperatureSensorAlertDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  temperatureSensorAlert_->CreateAccessor().DigitalInput(
    kBoardGPIOBaseAddress + kTemperatureSensorAlertOffset,
    true);  // the polarity of the Alert output is by default "low active". See datasheet of chip
  hdcDrdyInterrupt_ = Create().ItemIn<bool>(
    kHdcDrdyInterruptName,
    kHdcDrdyInterruptDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  hdcDrdyInterrupt_->CreateAccessor().DigitalInput(
      kBoardGPIOBaseAddress + kHdcDrdyInterruptOffset,
    true);  // the polarity of the Interrupt output is by default "low active". See datasheet of chip

  // Setup Firmware Build Timestamp input
  plBuildTimestamp_ = Create().ParameterIn<uint32_t>(  // TBD: Welcher Datentyp? Der Wert ist in sekunden UNIX epoch
    kPLBuildTimestampName,
    kPLBuildTimestampDescription,
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();  // TBD: muss eigentlich nur einmal gelesen werden
  plBuildTimestamp_->CreateAccessor().ScuRegister(
      baseAddress_CFG + kPLBuildTimestampPosition,
    0,
    32);

  // Setup PL local ID input
  plLocalID_ = Create().ParameterIn<uint32_t>(
      kPLLocalIDName,
      kPLLocalIDDescription,
      Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  plLocalID_->CreateAccessor().ScuRegister(
      baseAddress_CFG + kPLLocalIDPosition,
      0,
      32);

  // Setup Firmware GIT revision short hash input
  plBuildVersion_ = Create().ParameterIn<uint32_t>(
      kPLBuildVersionName,
      kPLBuildVersionDescription,
      Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  plBuildVersion_->CreateAccessor().ScuRegister(
      baseAddress_CFG + kPLBuildVersionPosition,
      0,
      32);

  // Number of "SLink Interfaces"
  numberOfSLinkInterfaces_ = Create().ParameterIn<uint32_t>(
    kNumberOfSLinkInterfacesName,
    kNumberOfSLinkInterfacesDescription,
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  numberOfSLinkInterfaces_->CreateAccessor().ScuRegister(
      baseAddress_CFG + kNumberOfSLinkInterfacesAddress,
    0,
    32);

  // Number of "OPL Interface"
  numberOfOplInterfaces_ = Create().ParameterIn<uint32_t>(
    kNumberOfOplInterfacesName,
    kNumberOfOplInterfacesDescription,
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  numberOfOplInterfaces_->CreateAccessor().ScuRegister(
      baseAddress_CFG + kNumberOfOplInterfacesAddress,
    0,
    32);

  // Setup SCU State inout
  // ToDo: Die Software sollte den status periodisch setzen und zwar gemäss
  // ihrem Zustand. Dazu ist eine State-machine zu implementieren
  // und mit Monitoring Functions sowie Front-Handle zu "verknüpfen"
  status_ = Create().ItemInOut<Types::ScuStatus>(
    kStatusName,
    kStatusDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    false).AddAndReturnPointer();
  // ToDo: Wird in der Regel von der SW geschrieben, kann aber von der
  // Firmware überschrieben werden wenn die SW zu lange nicht schreibt!
  status_->CreateAccessor().ScuRegister(
      baseAddress_CFG + kStatusAddressPosition,
    0,
    8);  // ??
  status_->CreateAction().Serializer().Status(
    GetModuleHandler());

  // Setup Beam Switch-Off Status collection
  beamSwitchOffStatusCollection_ = Create().ItemIn<uint32_t>(
    kBeamSwitchOffStatusCollectionName,
    kBeamSwitchOffStatusCollectionDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  beamSwitchOffStatusCollection_->CreateAccessor().ScuRegister(
      baseAddress_CFG + kBeamSwitchOffStatusCollectionAddressPosition,
    0,
    32);

  // Setup SW Beam Inhibit Request output (default value = 9)
  swBIRequest_ = Create().ItemOut<uint8_t>(
    kSWBIRequestName,
    kSWBIRequestDescription,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    true,
    0).AddAndReturnPointer();
  swBIRequest_->CreateAccessor().ScuRegister(
      baseAddress_CFG + kSWBIRequestPosition,
    0,
    8);
  swBIRequest_->CreateAction().StaticValue(
    9);

  // Setup SW Regular Beam Inhibit Request output (default value = 9)
  swRBIRequest_ = Create().ItemOut<uint8_t>(
    kSWRBIRequestName,
    kSWRBIRequestDescription,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    true,
    0).AddAndReturnPointer();
  swRBIRequest_->CreateAccessor().ScuRegister(
      baseAddress_CFG + kSWRBIRequestPosition,
    0,
    8);
  swRBIRequest_->CreateAction().StaticValue(
    9);

  // Setup SW Emergency Beam Inhibit Request output (default value = 9)
  swEBIRequest_ = Create().ItemOut<uint8_t>(
    kSWEBIRequestName,
    kSWEBIRequestDescription,
    std::chrono::milliseconds(updateInterval),
    Types::Affiliation::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    true,
    0).AddAndReturnPointer();
  swEBIRequest_->CreateAccessor().ScuRegister(
      baseAddress_CFG + kSWEBIRequestPosition,
    0,
    8);
  swEBIRequest_->CreateAction().StaticValue(
    9);

  // Create hot swap controller
  auto hotSwapController = std::make_unique<Modules::Common::HotSwapController>(
    logger,
    *configSerializer.HotSwapController(),
    configCommon,
    std::string(kI2CPathBase).append("1-0070/channel-4/"),
    0x4a);
  hotSwapController_ = hotSwapController.get();
  AddComponent(std::move(hotSwapController));

  // Create humidity sensor
  auto humiditySensor = std::make_unique<Modules::Common::HumiditySensor>(
    logger,
    configCommon,
    std::string(kI2CPathBase).append("1-0070/channel-4/"),
    0x41);
  humiditySensor_ = humiditySensor.get();
  AddComponent(std::move(humiditySensor));

  // Create temperature
  auto upperTemperature = std::make_unique<Modules::Temperature>(
    logger,
    configCommon,
    kUpperTemperatureName,
    kUpperTemperatureDescription,
    kUpperTemperatureAddress,
    std::string(kI2CPathBase).append("1-0070/channel-4/"),
    Types::MonitoringFunctionId::Enum::SCU_MON_W_FUNCTION_temp_upper_PCB_Edge,
    Types::MonitoringFunctionId::Enum::SCU_MON_E_FUNCTION_temp_upper_PCB_Edge);
  upperTemperature_ = upperTemperature.get();
  AddComponent(std::move(upperTemperature));
}

Serializer_Software *Serializer::Software() const {
  return software_;
}

Serializer_CardInfo *Serializer::CardInfo() const {
  return cardInfo_;
}

Serializer_Gpio *Serializer::Gpio() const {
  return gpio_;
}

Serializer_SfpModule *Serializer::SfpModule1() const {
  return sfpModule1_;
}

Serializer_SfpModule *Serializer::SfpModule2() const {
  return sfpModule2_;
}

Serializer_SfpModule *Serializer::SfpModule3() const {
  return sfpModule3_;
}

Serializer_OplInterface *Serializer::OplInterface() const {
  return oplInterface_;
}

Serializer_SLinkInterface *Serializer::SLinkInterface1() const {
  return sLinkInterface1_;
}

Serializer_SLinkInterface *Serializer::SLinkInterface2() const {
  return sLinkInterface2_;
}

Serializer_SLinkInterface *Serializer::SLinkInterface3() const {
  return sLinkInterface3_;
}

Framework::Components::ParameterItemIn<bool> *Serializer::Slot() {
  return slot_;
}

Framework::Components::ItemIn<bool> *Serializer::FrontHandle() const {
  return frontHandle_;
}

Framework::Components::ItemIn<bool> *Serializer::PgmClkStatusLockDetect() const {
  return pgmClkStatusLockDetect_;
}

Framework::Components::ItemIn<bool> *Serializer::PgmClkStatusHoldover() const {
  return pgmClkStatusHoldover_;
}

Framework::Components::ItemIn<bool> *Serializer::PgmClkStatusClkIn0() const {
  return pgmClkStatusClkIn0_;
}

Framework::Components::ItemIn<bool> *Serializer::PgmClkStatusClkIn1() const {
  return pgmClkStatusClkIn1_;
}

Framework::Components::ItemIn<bool> *Serializer::PgmClkSync() const {
  return pgmClkSync_;
}

Framework::Components::ItemIn<bool> *Serializer::MainPowerSupplyPowerGood() const {
  return mainPowerSupplyPowerGood_;
}

Framework::Components::ItemIn<bool> *Serializer::MainPowerSupplyNotAlert() const {
  return mainPowerSupplyNotAlert_;
}

Framework::Components::ItemIn<bool> *Serializer::USBPowerStatus() const {
  return usbPowerStatus_;
}

Framework::Components::ItemIn<bool> *Serializer::VccInternal0V85NotAlert() const {
  return vccInternal0V85NotAlert_;
}

Framework::Components::ItemIn<bool> *Serializer::TemperatureSensorAlert() const {
  return temperatureSensorAlert_;
}

Framework::Components::ItemIn<bool> *Serializer::HdcDrdyInterrupt() const {
  return hdcDrdyInterrupt_;
}

Framework::Components::ParameterItemIn<uint32_t> *Serializer::PlBuildTimestamp() const {
  return plBuildTimestamp_;
}

Framework::Components::ParameterItemIn<uint32_t> *Serializer::PlBuildVersion() const {
  return plBuildVersion_;
}

Framework::Components::ParameterItemIn<uint32_t> *Serializer::PlLocalID() const {
  return plLocalID_;
}

Framework::Components::ParameterItemIn<uint32_t> *Serializer::NumberOfOplInterfaces() const {
  return numberOfOplInterfaces_;
}

Framework::Components::ParameterItemIn<uint32_t> *Serializer::NumberOfSLinkInterfaces() const {
  return numberOfSLinkInterfaces_;
}

Framework::Components::ItemInOut<Types::ScuStatus> *Serializer::Status() const {
  return status_;
}

Framework::Components::ItemOut<uint8_t> *Serializer::SwBIRequest() const {
  return swBIRequest_;
}

Framework::Components::ItemOut<uint8_t> *Serializer::SwRBIRequest() const {
  return swRBIRequest_;
}

Framework::Components::ItemOut<uint8_t> *Serializer::SwEBIRequest() const {
  return swEBIRequest_;
}

Modules::Common::HotSwapController *Serializer::HotSwapController() const {
  return hotSwapController_;
}

Modules::Common::HumiditySensor *Serializer::HumiditySensor() const {
  return humiditySensor_;
}

Modules::Temperature *Serializer::UpperTemperature() const {
  return upperTemperature_;
}

}  // namespace SystemModules::Modules::Serializer
