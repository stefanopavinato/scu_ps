/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Types/VariantValue.h"
#include "SystemModules/Framework/Builder/Components/ParameterItemInBuilder.h"
#include "SystemModules/Framework/Builder/Components/ComponentBuilder.h"
#include "Serializer_SvnInfo.h"

namespace SystemModules::Modules {

const char Serializer_SvnInfo::kVersionName[] = "Version";
const char Serializer_SvnInfo::kVersionDescription[] = "Selected svn revision";

const char Serializer_SvnInfo::kBranchName[] = "Branch";
const char Serializer_SvnInfo::kBranchDescription[] = "Selected svn branch";

const char Serializer_SvnInfo::kTimestampName[] = "Timestamp";
const char Serializer_SvnInfo::kTimestampDescription[] = "Build timestamp in seconds since 1.1.1970";

const char Serializer_SvnInfo::kDateName[] = "Date";
const char Serializer_SvnInfo::kDateDescription[] = "Build date";

const char Serializer_SvnInfo::kTimeName[] = "Time";
const char Serializer_SvnInfo::kTimeDescription[] = "Build time";

const char Serializer_SvnInfo::kIsDirtyName[] = "IsDirty";
const char Serializer_SvnInfo::kIsDirtyDescription[] =
  "true, if the compiled version does not match the version in the repository";

Serializer_SvnInfo::Serializer_SvnInfo(
  Logger::ILogger *logger,
  const Configuration::ConfigurationSerializerSvnInfo &config,
  const std::string &name,
  const std::string &description)
  : Framework::Components::Container(
    logger,
    name,
    description) {

  version_ = Create().ParameterIn<uint32_t>(
    kVersionName,
    kVersionDescription,
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  version_->CreateAccessor().InternalStaticLocation(config.Version()->Value());

  branch_ = Create().ParameterIn<std::string>(
    kBranchName,
    kBranchDescription,
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  branch_->CreateAccessor().InternalStaticLocation(config.Branch()->Value());

  timestamp_ = Create().ParameterIn<uint32_t>(
    kTimestampName,
    kTimestampDescription,
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  timestamp_->CreateAccessor().InternalStaticLocation(config.Timestamp()->Value());

  date_ = Create().ParameterIn<std::string>(
    kDateName,
    kDateDescription,
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  date_->CreateAccessor().InternalStaticLocation(config.Date()->Value());

  time_ = Create().ParameterIn<std::string>(
    kTimeName,
    kTimeDescription,
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  time_->CreateAccessor().InternalStaticLocation(config.Time()->Value());

  isDirty_ = Create().ParameterIn<bool>(
    kIsDirtyName,
    kIsDirtyDescription,
    Types::Affiliation::Enum::EXCLUSIVE).AddAndReturnPointer();
  isDirty_->CreateAccessor().InternalStaticLocation(config.IsDirty()->Value());
}

Framework::Components::ParameterItemIn<uint32_t> *Serializer_SvnInfo::Version() const {
  return version_;
}

Framework::Components::ParameterItemIn<std::string> *Serializer_SvnInfo::Branch() const {
  return branch_;
}

Framework::Components::ParameterItemIn<uint32_t> *Serializer_SvnInfo::Timestamp() const {
  return timestamp_;
}

Framework::Components::ParameterItemIn<std::string> *Serializer_SvnInfo::Date() const {
  return date_;
}

Framework::Components::ParameterItemIn<std::string> *Serializer_SvnInfo::Time() const {
  return time_;
}

Framework::Components::ParameterItemIn<bool> *Serializer_SvnInfo::IsDirty() const {
  return isDirty_;
}

}  // namespace SystemModules::Modules
