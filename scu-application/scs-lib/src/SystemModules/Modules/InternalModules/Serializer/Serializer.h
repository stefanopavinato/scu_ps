/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <cstdint>
#include "Types/ScuStatus.h"
#include "SystemModules/Framework/Components/ItemIn.h"
#include "SystemModules/Framework/Components/ItemOut.h"
#include "SystemModules/Framework/Components/ItemInOut.h"
#include "SystemModules/Framework/Components/ParameterItemIn.h"
#include "SystemModules/Framework/Components/Container.h"
#include "SystemModules/Modules/Configuration/ConfigurationSerailizer.h"
#include "SystemModules/Modules/Configuration/Modules/Configuration_Serializer.h"
#include "SystemModules/Modules/MezzanineCards/Common/HotSwapController.h"
#include "SystemModules/Modules/MezzanineCards/Common/HumiditySensor.h"
#include "SystemModules/Modules/MezzanineCards/Management/Temperature.h"
#include "Serializer_Software.h"
#include "Serializer_CardInfo.h"
#include "Serializer_Gpio.h"
#include "Serializer_SfpModule.h"
#include "Serializer_OplInterface.h"
#include "Serializer_SLinkInterface.h"

namespace SystemModules::Modules::Serializer {
class Serializer : public Framework::Components::Container {
 public:
  explicit Serializer(
    Logger::ILogger *logger,
    bool * powerEnablePullUpDrive,
    const Modules::Configuration::Modules::Configuration_Serializer &configSerializer,
    const Modules::Configuration::ConfigurationCommon &configCommon,
    const Configuration::ConfigurationSerializer &configuration);

  [[nodiscard]] Serializer_Software *Software() const;

  [[nodiscard]] Serializer_CardInfo *CardInfo() const;

  [[nodiscard]] Serializer_Gpio *Gpio() const;

  [[nodiscard]] Serializer_SfpModule *SfpModule1() const;

  [[nodiscard]] Serializer_SfpModule *SfpModule2() const;

  [[nodiscard]] Serializer_SfpModule *SfpModule3() const;

  [[nodiscard]] Serializer_OplInterface *OplInterface() const;

  [[nodiscard]] Serializer_SLinkInterface *SLinkInterface1() const;

  [[nodiscard]] Serializer_SLinkInterface *SLinkInterface2() const;

  [[nodiscard]] Serializer_SLinkInterface *SLinkInterface3() const;

  [[nodiscard]] Framework::Components::ParameterItemIn<bool> *Slot();

  [[nodiscard]] Framework::Components::ItemIn<bool> *FrontHandle() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *PgmClkStatusLockDetect() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *PgmClkStatusHoldover() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *PgmClkStatusClkIn0() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *PgmClkStatusClkIn1() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *PgmClkSync() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *MainPowerSupplyPowerGood() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *MainPowerSupplyNotAlert() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *USBPowerStatus() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *VccInternal0V85NotAlert() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *TemperatureSensorAlert() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *HdcDrdyInterrupt() const;

  [[nodiscard]] Framework::Components::ParameterItemIn<uint32_t> *PlBuildTimestamp() const;

  [[nodiscard]] Framework::Components::ParameterItemIn<uint32_t> *PlBuildVersion() const;

  [[nodiscard]] Framework::Components::ParameterItemIn<uint32_t> *PlLocalID() const;

  [[nodiscard]] Framework::Components::ParameterItemIn<uint16_t> *Id() const;

  [[nodiscard]] Framework::Components::ParameterItemIn<uint32_t> *NumberOfOplInterfaces() const;

  [[nodiscard]] Framework::Components::ParameterItemIn<uint32_t> *NumberOfSLinkInterfaces() const;

  [[nodiscard]] Framework::Components::ItemInOut<Types::ScuStatus> *Status() const;

  [[nodiscard]] Framework::Components::ItemOut<uint8_t> *SwBIRequest() const;

  [[nodiscard]] Framework::Components::ItemOut<uint8_t> *SwRBIRequest() const;

  [[nodiscard]] Framework::Components::ItemOut<uint8_t> *SwEBIRequest() const;

  [[nodiscard]] Modules::Common::HotSwapController *HotSwapController() const;

  [[nodiscard]] Modules::Common::HumiditySensor *HumiditySensor() const;

  [[nodiscard]] Modules::Temperature *UpperTemperature() const;

 private:
  Serializer_Software *software_;

  Serializer_CardInfo *cardInfo_;

  Serializer_Gpio *gpio_;

  Serializer_SfpModule *sfpModule1_;

  Serializer_SfpModule *sfpModule2_;

  Serializer_SfpModule *sfpModule3_;

  Serializer_SLinkInterface *sLinkInterface1_;

  Serializer_SLinkInterface *sLinkInterface2_;

  Serializer_SLinkInterface *sLinkInterface3_;

  Serializer_OplInterface *oplInterface_;

  Framework::Components::ParameterItemIn<bool> *slot_;

  Framework::Components::ItemIn<bool> *frontHandle_;

  Framework::Components::ItemIn<bool> *pgmClkStatusLockDetect_;

  Framework::Components::ItemIn<bool> *pgmClkStatusHoldover_;

  Framework::Components::ItemIn<bool> *pgmClkStatusClkIn0_;

  Framework::Components::ItemIn<bool> *pgmClkStatusClkIn1_;

  Framework::Components::ItemIn<bool> *pgmClkSync_;

  Framework::Components::ItemIn<bool> *mainPowerSupplyPowerGood_;

  Framework::Components::ItemIn<bool> *mainPowerSupplyNotAlert_;

  Framework::Components::ItemIn<bool> *usbPowerStatus_;

  Framework::Components::ItemIn<bool> *vccInternal0V85NotAlert_;

  Framework::Components::ItemIn<bool> *temperatureSensorAlert_;

  Framework::Components::ItemIn<bool> *hdcDrdyInterrupt_;

  Framework::Components::ParameterItemIn<uint32_t> *plBuildTimestamp_;

  Framework::Components::ParameterItemIn<uint32_t> *plBuildVersion_;

  Framework::Components::ParameterItemIn<uint32_t> *plLocalID_;

  Framework::Components::ParameterItemIn<uint32_t> *numberOfOplInterfaces_;

  Framework::Components::ParameterItemIn<uint32_t> *numberOfSLinkInterfaces_;

  Framework::Components::ItemInOut<Types::ScuStatus> *status_;

  Framework::Components::ItemIn<uint32_t> *beamSwitchOffStatusCollection_;

  Framework::Components::ItemOut<uint8_t> *swBIRequest_;

  Framework::Components::ItemOut<uint8_t> *swRBIRequest_;

  Framework::Components::ItemOut<uint8_t> *swEBIRequest_;

  Modules::Common::HotSwapController *hotSwapController_;

  Modules::Common::HumiditySensor *humiditySensor_;

  Modules::Temperature *upperTemperature_;

  static const char kName[];
  static const char kDescription[];

  static const char kI2CPathBase[];

  static const uint32_t kBoardGPIOBaseAddress;

  static const char kUpperTemperatureName[];
  static const char kUpperTemperatureDescription[];
  static const uint16_t kUpperTemperatureAddress;

  static const char kSfpModule1Name[];
  static const uint32_t kSfpModule1BaseAddress;

  static const char kSfpModule2Name[];
  static const uint32_t kSfpModule2BaseAddress;

  static const char kSfpModule3Name[];
  static const uint32_t kSfpModule3BaseAddress;

  static const char kSLinkInterface1Name[];

  static const char kSLinkInterface2Name[];

  static const char kSLinkInterface3Name[];

  static const char kSlotName[];
  static const char kSlotDescription[];
  static const uint32_t kSlotId;

  static const char kFrontHandleName[];
  static const char kFrontHandleDescription[];
  static const uint32_t kFrontHandleAddress;

  static const char kPgmClkStatusLockDetectName[];
  static const char kPgmClkStatusLockDetectDescription[];
  static const uint32_t kPgmClkStatusLockDetectOffset;

  static const char kPgmClkStatusHoldoverName[];
  static const char kPgmClkStatusHoldoverDescription[];
  static const uint32_t kPgmClkStatusHoldoverOffset;

  static const char kPgmClkStatusClkIn0Name[];
  static const char kPgmClkStatusClkIn0Description[];
  static const uint32_t kPgmClkStatusClkIn0Offset;

  static const char kPgmClkStatusClkIn1Name[];
  static const char kPgmClkStatusClkIn1Description[];
  static const uint32_t kPgmClkStatusClkIn1Offset;

  static const char kPgmClkSyncName[];
  static const char kPgmClkSyncDescription[];
  static const uint32_t kPgmClkSyncOffset;

  static const char kMainPowerSupplyPowerGoodName[];
  static const char kMainPowerSupplyPowerGoodDescription[];
  static const uint32_t kMainPowerSupplyPowerGoodOffset;

  static const char kMainPowerSupplyNotAlertName[];
  static const char kMainPowerSupplyNotAlertDescription[];
  static const uint32_t kMainPowerSupplyNotAlertOffset;

  static const char kUSBPowerStatusName[];
  static const char kUSBPowerStatusDescription[];
  static const uint32_t kUSBPowerStatusOffset;

  static const char kVccInternal0V85NotAlertName[];
  static const char kVccInternal0V85NotAlertDescription[];
  static const uint32_t kVccInternal0V85NotAlertOffset;

  static const char kTemperatureSensorAlertName[];
  static const char kTemperatureSensorAlertDescription[];
  static const uint32_t kTemperatureSensorAlertOffset;

  static const char kHdcDrdyInterruptName[];
  static const char kHdcDrdyInterruptDescription[];
  static const uint32_t kHdcDrdyInterruptOffset;

  static const char kPLBuildTimestampName[];
  static const char kPLBuildTimestampDescription[];
  static const uint32_t kPLBuildTimestampPosition;

  static const char kPLBuildVersionName[];
  static const char kPLBuildVersionDescription[];
  static const uint32_t kPLBuildVersionPosition;

  static const char kPLLocalIDName[];
  static const char kPLLocalIDDescription[];
  static const uint32_t kPLLocalIDPosition;

  static const char kNumberOfOplInterfacesName[];
  static const char kNumberOfOplInterfacesDescription[];
  static const uint32_t kNumberOfOplInterfacesAddress;

  static const char kNumberOfSLinkInterfacesName[];
  static const char kNumberOfSLinkInterfacesDescription[];
  static const uint32_t kNumberOfSLinkInterfacesAddress;

  static const char kStatusName[];
  static const char kStatusDescription[];
  static const uint32_t kStatusAddressPosition;

  static const char kBeamSwitchOffStatusCollectionName[];
  static const char kBeamSwitchOffStatusCollectionDescription[];
  static const uint32_t kBeamSwitchOffStatusCollectionAddressPosition;

  static const char kSWBIRequestName[];
  static const char kSWBIRequestDescription[];
  static const uint32_t kSWBIRequestPosition;

  static const char kSWRBIRequestName[];
  static const char kSWRBIRequestDescription[];
  static const uint32_t kSWRBIRequestPosition;

  static const char kSWEBIRequestName[];
  static const char kSWEBIRequestDescription[];
  static const uint32_t kSWEBIRequestPosition;
};
}  // namespace SystemModules::Modules::Serializer
