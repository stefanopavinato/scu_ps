/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "SystemModules/Framework/Components/ItemIn.h"
#include "SystemModules/Framework/Components/ItemOut.h"
#include "SystemModules/Framework/Components/Container.h"
#include "SystemModules/Modules/Configuration/ConfigurationCommon.h"

namespace SystemModules::Modules::Serializer {
class Serializer_SfpModule : public Framework::Components::Container {
 public:
  Serializer_SfpModule(
    Logger::ILogger *logger,
    const Modules::Configuration::ConfigurationCommon &configCommon,
    std::string const &name,
    uint32_t const &baseAddress);

  [[nodiscard]] Framework::Components::ItemIn<bool> *Present() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *TxFault() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *Los() const;

  [[nodiscard]] Framework::Components::ItemOut<bool> *RateSelect0() const;

  [[nodiscard]] Framework::Components::ItemOut<bool> *RateSelect1() const;

  [[nodiscard]] Framework::Components::ItemOut<bool> *Locator() const;

  [[nodiscard]] Framework::Components::ItemOut<bool> *LinkState() const;

  [[nodiscard]] Framework::Components::ItemOut<bool> *TxDisable() const;

 private:
  Framework::Components::ItemIn<bool> *present_;

  Framework::Components::ItemIn<bool> *txFault_;

  Framework::Components::ItemIn<bool> *los_;

  Framework::Components::ItemOut<bool> *rateSelect0_;

  Framework::Components::ItemOut<bool> *rateSelect1_;

  Framework::Components::ItemOut<bool> *locator_;

  Framework::Components::ItemOut<bool> *linkState_;

  Framework::Components::ItemOut<bool> *txDisable_;

  static const char kDescription[];

  static const char kPresentName[];
  static const char kPresentDescription[];
  static const uint32_t kPresentAddressOffset;

  static const char kTxFaultName[];
  static const char kTxFaultDescription[];
  static const uint32_t kTxFaultAddressOffset;

  static const char kLosName[];
  static const char kLosDescription[];
  static const uint32_t kLosAddressOffset;

  static const char kRateSelect0Name[];
  static const char kRateSelect0Description[];
  static const uint32_t kRateSelect0AddressOffset;

  static const char kRateSelect1Name[];
  static const char kRateSelect1Description[];
  static const uint32_t kRateSelect1AddressOffset;

  static const char kLocatorName[];
  static const char kLocatorDescription[];
  static const uint32_t kLocatorAddressOffset;

  static const char kLinkStateName[];
  static const char kLinkStateDescription[];
  static const uint32_t kLinkStateAddressOffset;

  static const char kTxDisableName[];
  static const char kTxDisableDescription[];
  static const uint32_t kTxDisableAddressOffset;
};
}  // namespace SystemModules::Modules::Serializer

// 3 x Instanz eines "SFP Module"
//
// SFP Module besitzt folgende Input Items (Via I2C IO-Expander ausgelesen, U137, U139)
// - SFP Present
// - SFP Tx Fault
// - SFP Los
// Sowie die folgenden Output Items (ebenfalls via I2C)
// - SFP Rate Select 0 --> default wert muss gesetzt werden,
// gasi fragen und kommentar "set high-rate as default" im code hinterlegen. (im ini-file)
// - SFP Rate Select 1 --> default wert muss gesetzt werden,
// gasi fragen und kommentar "set high-rate as default" im code hinterlegen. (im ini-file)
// - SFP Locator
// - SFP Link State --> ?? gasi fragen ob default wert gesetzt werden muss
// - TXDISABLE ? (im ini-file)

