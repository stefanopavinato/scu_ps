/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <memory>
#include "Logger/ILogger.h"
#include "SystemModules/Modules/InternalModules/States/IMStateInitialize1.h"
#include "InternalModuleFactory.h"
#include "ModuleInternal.h"

namespace SystemModules::Framework::Components {

const char ModuleInternal::kDescription[] = "";
const char ModuleInternal::kComponentType[] = "ModuleInternal";

ModuleInternal::ModuleInternal(
  Logger::ILogger *logger,
  Modules::Configuration::Configuration *configuration,
  const std::string &name,
  WorkloadBalancing::WorkloadBalanceHandler *workloadBalanceHandler,
  const Types::ModuleType::Enum &moduleType)
  : ModuleBase(
  logger,
  name,
  kDescription,
  kComponentType,
  workloadBalanceHandler)
  , state_(std::make_unique<States::IMStateInitialize1>(this))
  , configuration_(configuration)
  , moduleType_(moduleType)
  , application_(nullptr) {
}

Types::ModuleState ModuleInternal::GetStateId() const {
  return state_->GetId();
}

Types::AlertSeverity::Enum ModuleInternal::Update(
  const std::chrono::steady_clock::time_point &timePointNow) {
  state_->OnUpdate();
  return Types::AlertSeverity::Enum::OK;
}

void ModuleInternal::SetState(std::unique_ptr<States::IMStateBase> stateNew) {
  Logger()->Info(GetName().append(" to ").append(stateNew->GetId().ToString()));
  state_->OnExit();
  state_ = std::move(stateNew);
  state_->OnEntry();
}

bool *ModuleInternal::PowerEnablePullUpDrive() {
  return &powerEnablePullUpDrive_;
}

bool ModuleInternal::InstallApplication() {
  try {
    // Create application
    auto application = SystemModules::Modules::InternalModuleFactory::Create(
      Logger(),
      this,
      *configuration_,
      moduleType_.GetEnum());
    // Check if application holds an object
    if (!application) {
      Logger()->Error("Application not found");
      return false;
    }
    // Add application
    application_ = application.value().get();
    AddComponent(std::move(application.value()));
    // Initialize hardware
    if (!application_->Initialize(workloadBalanceHandler_)) {
      Logger()->Error("Initialization failure");
      return false;
    }
  } catch (const std::invalid_argument &ia) {
    application_ = nullptr;
    Logger()->Error(ia.what());
    return false;
  }
  return true;
}

void ModuleInternal::UninstallApplication() {
  // Check if application holds an object
  if (!application_) {
    Logger()->Error("Application not found");
    return;
  }
  // De-Initialize application
  (void) application_->DeInitialize(workloadBalanceHandler_);
  // Remove application from components
  RemoveComponent(application_->GetName());
  // Reset application pointer
  application_ = nullptr;
}

}  // namespace SystemModules::Framework::Components
