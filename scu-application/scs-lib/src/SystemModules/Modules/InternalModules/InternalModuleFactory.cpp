/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Modules/Configuration/Configuration.h"
#include "SystemModules/Framework/Components/ModuleHandler.h"
#include "SystemModules/Modules/InternalModules/Serializer/Serializer.h"
#include "SystemModules/Modules/InternalModules/PowerSupply/PowerSupply.h"
#include "SystemModules/Modules/InternalModules/FanUnit/FanUnit.h"
#include "InternalModuleFactory.h"

namespace SystemModules::Modules {
std::experimental::optional<std::unique_ptr<Framework::Components::Container>> InternalModuleFactory::Create(
  Logger::ILogger *logger,
  SystemModules::Framework::Components::ModuleInternal * module,
  const Modules::Configuration::Configuration &configuration,
  const Types::ModuleType::Enum &type) {
  switch (type) {
    case Types::ModuleType::Enum::SCU_SER: {
      return std::make_unique<SystemModules::Modules::Serializer::Serializer>(
        logger,
        module->PowerEnablePullUpDrive(),
        *configuration.Modules()->Serializer(),
        *configuration.Common(),
        *configuration.Serializer());
    }
    case Types::ModuleType::Enum::SCU_PWSUP: {
      return std::make_unique<SystemModules::Modules::PowerSupply::PowerSupply>(
        logger,
        *configuration.Common(),
        *configuration.PowerSupply());
    }
    case Types::ModuleType::Enum::SCU_FU: {
      return std::make_unique<SystemModules::Modules::FanUnit::FanUnit>(
        logger,
        *configuration.Common(),
        *configuration.FanUnit());
    }
    default: {
      return std::experimental::nullopt;
    }
  }
  return std::experimental::nullopt;
}
}  // namespace SystemModules::Modules
