/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "SystemModules/Modules/Configuration/Configuration.h"
#include "SystemModules/Framework/Components/ModuleBase.h"

namespace Logger {
class ILogger;
}  // namespace Logger
namespace SystemModules::Framework {
namespace States {
class IMStateBase;
}  // namespace States
namespace Components {
class ModuleInternal : public ModuleBase {
  friend class States::IMStateBase;

 public:
  ModuleInternal(
    Logger::ILogger *logger,
    Modules::Configuration::Configuration *configuration,
    const std::string &name,
    WorkloadBalancing::WorkloadBalanceHandler *workloadBalanceHandler,
    const Types::ModuleType::Enum &moduleType);

  [[nodiscard]] Types::ModuleState GetStateId() const override;

  [[nodiscard]] bool InstallApplication();

  void UninstallApplication();

  bool *PowerEnablePullUpDrive();

  [[nodiscard]] Types::AlertSeverity::Enum Update(
    const std::chrono::steady_clock::time_point &timePointNow) override;

 protected:
  void SetState(std::unique_ptr<States::IMStateBase> stateNew);

 private:
  std::unique_ptr<States::IMStateBase> state_;

  bool powerEnablePullUpDrive_;

  Modules::Configuration::Configuration *configuration_;

  const Types::ModuleType moduleType_;

  SystemModules::Framework::Components::Container *application_;

  static const char kDescription[];
  static const char kComponentType[];
};
}  // namespace Components
}  // namespace SystemModules::Framework
