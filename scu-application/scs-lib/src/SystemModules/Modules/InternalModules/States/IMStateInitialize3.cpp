/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Modules/InternalModules/ModuleInternal.h"
#include "IMStateRun.h"
#include "IMStateInitialize3.h"

namespace SystemModules::Framework::States {

IMStateInitialize3::IMStateInitialize3(Components::ModuleInternal *module)
  : IMStateBase(Types::ModuleState::Enum::INITIALIZE3, module) {
}

void IMStateInitialize3::OnEntry() {
}

void IMStateInitialize3::OnExit() {
}

void IMStateInitialize3::OnUpdate() {
  *Module()->PowerEnablePullUpDrive() = true;
  // Switch to state run
  SetState(std::make_unique<IMStateRun>(Module()));
}

}  // namespace SystemModules::Framework::States
