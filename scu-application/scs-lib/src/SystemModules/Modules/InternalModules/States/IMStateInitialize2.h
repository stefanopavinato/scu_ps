/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "IMStateBase.h"

namespace SystemModules::Framework::States {
class IMStateInitialize2 : public IMStateBase {
 public:
  explicit IMStateInitialize2(Components::ModuleInternal *module);

  ~IMStateInitialize2() override = default;

  void OnEntry() final;

  void OnExit() final;

  void OnUpdate() final;
};
}  // namespace SystemModules::Framework::States
