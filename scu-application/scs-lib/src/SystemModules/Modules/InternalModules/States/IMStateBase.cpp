/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <unordered_map>
#include <queue>
#include <chrono>
#include "SystemModules/Framework/Components/Container.h"
#include "SystemModules/Modules/InternalModules/ModuleInternal.h"
#include "IMStateBase.h"

namespace SystemModules::Framework::States {
IMStateBase::IMStateBase(
  const Types::ModuleState::Enum &id,
  Components::ModuleInternal *module)
  : id_(id), module_(module) {
}

Types::ModuleState IMStateBase::GetId() const {
  return id_;
}

Components::ModuleInternal *IMStateBase::Module() const {
  return module_;
}

void IMStateBase::SetState(std::unique_ptr<IMStateBase> state) {
  module_->SetState(std::move(state));
}

}  // namespace SystemModules::Framework::States
