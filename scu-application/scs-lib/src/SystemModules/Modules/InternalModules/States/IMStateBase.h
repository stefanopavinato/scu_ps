/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Types/ModuleState.h"

namespace SystemModules::Framework {
namespace Components {
class ModuleInternal;
}  // namespace Components
namespace States {
class IMStateBase {
 public:
  IMStateBase(
    const Types::ModuleState::Enum &id,
    Components::ModuleInternal *module);

  virtual ~IMStateBase() = default;

  virtual void OnEntry() = 0;

  virtual void OnExit() = 0;

  virtual void OnUpdate() = 0;

  [[nodiscard]] Types::ModuleState GetId() const;

  void SetState(std::unique_ptr<IMStateBase> stateNew);

  [[nodiscard]] Components::ModuleInternal *Module() const;

 private:
  Types::ModuleState id_;

  Components::ModuleInternal *module_;
};
}  // namespace States
}  // namespace SystemModules::Framework
