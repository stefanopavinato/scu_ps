/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <unistd.h>
#include <arpa/inet.h>
#include "ClientBase.h"

namespace Communication {
namespace ConnectionListener {
class ConnectionListenerTCP;
}  // namespace ConnectionListener
namespace Clients {
class ClientTCP : public ClientBase {
  friend class ConnectionListener::ConnectionListenerTCP;

 public:
  ClientTCP(
    Logger::ILogger *logger,
    Protocols::ProtocolBase *protocol,
    std::unique_ptr<StreamSplitter::StreamSplitterBase> streamSplitter);

  ClientTCP(
    Logger::ILogger *logger,
    const std::string & ipAddress,
    const uint16_t & port,
    Protocols::ProtocolBase *protocol,
    std::unique_ptr<StreamSplitter::StreamSplitterBase> streamSplitter);

  ~ClientTCP() override = default;

  bool Connect(
    const std::string & ipAddress,
    const uint16_t & port);

  bool Connect(
    const int32_t &serverSocketFileDescriptor) override;

  struct sockaddr_in socketAddress_;

  socklen_t socketAddressLength_;

 protected:
  [[nodiscard]] bool Run() override;

  [[nodiscard]] bool Initialize() override;

  [[nodiscard]] bool DeInitialize() override;

  [[nodiscard]] bool Send(std::unique_ptr<std::vector<char>> stream) override;

 private:
  static const char kName[];
};
}  // namespace Clients
}  // namespace Communication
