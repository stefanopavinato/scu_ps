/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <string>
#include <memory>
#include <vector>
#include "Utils/ThreadBase.h"
#include "Communication/Types/ProtocolType.h"

namespace Messages {
class Message;
}  // namespace Messages
namespace Communication {
class ClientHandler;
namespace StreamSplitter {
class StreamSplitterBase;
}  // namespace StreamSplitter
namespace Protocols {
class ProtocolBase;
}  // namespace Protocols
namespace Clients {
class ClientBase : public Utils::ThreadBase {
  friend class Communication::ClientHandler;

 public:
  explicit ClientBase(
    Logger::ILogger *logger,
    std::string name,
    Protocols::ProtocolBase *protocol,
    std::unique_ptr<StreamSplitter::StreamSplitterBase> streamSplitter);

  ~ClientBase() override = default;

  [[nodiscard]] std::string GetName() const;

  [[nodiscard]] int32_t GetFileDescriptor() const;

  [[nodiscard]] std::string GetIp() const;

  bool SetMessageHandler(Messages::MessageVisitorBase *messageHandler);

  [[nodiscard]] Protocols::ProtocolBase *GetProtocol() const;

  bool Send(Messages::MessageBase *message);

 protected:
  virtual bool Connect(const int32_t &serverSocketFileDescriptor) = 0;

  void SetFileDescriptor(const int32_t &fileDescriptor);

  void SetIp(const std::string &ip);

  [[nodiscard]] virtual bool Send(std::unique_ptr<std::vector<char>> stream) = 0;

  bool Receive(std::unique_ptr<std::vector<char>> input);

  Logger::ILogger *const logger_;

 private:
  const std::string name_;

  int32_t fileDescriptor_;

  std::string ip_;

  Protocols::ProtocolBase *const protocol_;

  std::unique_ptr<StreamSplitter::StreamSplitterBase> streamSplitter_;

  Messages::MessageVisitorBase *messageHandler_;
};
}  // namespace Clients
}  // namespace Communication
