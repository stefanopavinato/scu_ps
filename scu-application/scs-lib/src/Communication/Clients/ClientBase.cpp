/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Logger/ILogger.h"
#include "Messages/MessageItem.h"
#include "Messages/Message.h"
#include "Communication/StreamSplitter/StreamSplitterBase.h"
#include "Communication/Protocols/ProtocolBase.h"
#include "ClientBase.h"

namespace Communication::Clients {

ClientBase::ClientBase(
  Logger::ILogger *logger,
  std::string name,
  Protocols::ProtocolBase *const protocol,
  std::unique_ptr<StreamSplitter::StreamSplitterBase> streamSplitter)
  : ThreadBase(logger, name)
  , logger_(logger)
  , name_(std::move(name))
  , protocol_(protocol)
  , streamSplitter_(std::move(streamSplitter)) {
}

std::string ClientBase::GetName() const {
  return name_;
}

int ClientBase::GetFileDescriptor() const {
  return fileDescriptor_;
}

void ClientBase::SetFileDescriptor(const int32_t &fileDescriptor) {
  fileDescriptor_ = fileDescriptor;
}

std::string ClientBase::GetIp() const {
  return ip_;
}

void ClientBase::SetIp(const std::string &ip) {
  ip_ = ip;
}

Protocols::ProtocolBase *ClientBase::GetProtocol() const {
  return protocol_;
}

bool ClientBase::SetMessageHandler(Messages::MessageVisitorBase *messageHandler) {
  if (messageHandler == nullptr) {
    logger_->Error("Message handler not set");
    return false;
  }
  messageHandler_ = messageHandler;
  return true;
}

bool ClientBase::Send(Messages::MessageBase *message) {
  // Serialize message
  auto stream = protocol_->Serialize(message);
  if (!stream) {
    logger_->Error("Failed to serialize message");
    return false;
  }
  // Pack stream
  auto output = streamSplitter_->Pack(std::move(stream.value()));
  if (!output) {
    logger_->Error("Failed to pack stream");
    return false;
  }
  // Send stream
  return Send(std::move(output.value()));
}

bool ClientBase::Receive(std::unique_ptr<std::vector<char>> input) {
  // Split input stream
  auto stream = streamSplitter_->Unpack(std::move(input));
  if (!stream) {
    // Wait for further data
    logger_->Error("Wait for further data");
    return true;
  }
  // ToDo: crc could be checked here (future extension)
  // ToDo: un-compressing of data could be processed here (future extension)
  // Deserialize message
  auto message = protocol_->Deserialize(std::move(stream.value()));
  if (!message) {
    logger_->Error("Failed to deserialize message");
    return false;
  }
  // Add client attributes to message
  message.value()->SetClientFileDescriptor(
    GetFileDescriptor());
  message.value()->SetSender(
    ResourceSharing::Address(GetIp()));
  // ToDo: Set receiver
  // Process message
  if (messageHandler_ == nullptr) {
    logger_->Error("Message handler not set");
    return false;
  }
  // Pass message to message handler
  messageHandler_->OnMessageReceived(message->get());
  return true;
}

}  // namespace Communication::Clients
