/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <cstring>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include "Logger/ILogger.h"
#include "Communication/StreamSplitter/StreamSplitterBase.h"
#include "Communication/Protocols/ProtocolBase.h"
#include "ClientTCP.h"

namespace Communication::Clients {

const char ClientTCP::kName[] = "ClientTCP";

ClientTCP::ClientTCP(
  Logger::ILogger *logger,
  Protocols::ProtocolBase *protocol,
  std::unique_ptr<StreamSplitter::StreamSplitterBase> streamSplitter)
  : ClientBase(
  logger,
  kName,
  protocol,
  std::move(streamSplitter))
  , socketAddress_({})
  , socketAddressLength_(sizeof(socketAddress_)) {
}

bool ClientTCP::Connect(const int32_t &serverSocketFileDescriptor) {
  // Get client address
  if (getpeername(
    GetFileDescriptor(),
    reinterpret_cast<sockaddr *>(&socketAddress_),
    &socketAddressLength_)) {
    logger_->Error("Failed to get peer name");
    return false;
  }
  // Convert client address
  char tempIpAddress[16] = "";
  inet_ntop(
    AF_INET,
    &socketAddress_.sin_addr,
    tempIpAddress,
    sizeof(tempIpAddress));
  SetIp(std::string(tempIpAddress));
  // Write log info
  logger_->Info(
    std::string("Client: ip = ").append(GetIp())
      .append(", protocol = ").append(GetProtocol()->GetProtocolType().ToString())
      .append(", fd = ").append(std::to_string(GetFileDescriptor())));

  return true;
}

bool ClientTCP::Connect(
  const std::string & ipAddress,
  const uint16_t & port) {
  auto fileDescriptor = socket(AF_INET, SOCK_STREAM, 0);
  if (fileDescriptor < 0) {
//    Logger()->Error(std::string("Failed to create socket: ").append(strerror(errno)));
    return false;
  }
  SetFileDescriptor(fileDescriptor);

  struct sockaddr_in serverSocketAddress{};
  serverSocketAddress.sin_family = AF_INET;
  serverSocketAddress.sin_port = htons(port);
  // Convert ipv4 and ipv6 address from text to binary form
  if (inet_pton(AF_INET, ipAddress.c_str(), &serverSocketAddress.sin_addr) <= 0) {
    logger_->Error(std::string("Invalid address/Address not supported: ").append(strerror(errno)));
    close(GetFileDescriptor());
    return false;
  }
  if (connect(fileDescriptor, (struct sockaddr *)&serverSocketAddress, sizeof(serverSocketAddress)) < 0) {
//    Logger()->Error(std::string("Failed to connect client: ").append(strerror(errno)));
    close(GetFileDescriptor());
    return false;
  }
  SetIp(ipAddress);
  // Write log info
  logger_->Info(
    std::string("Client: ip = ").append(GetIp())
      .append(", protocol = ").append(GetProtocol()->GetProtocolType().ToString())
      .append(", fd = ").append(std::to_string(fileDescriptor)));

  return true;
}

bool ClientTCP::Send(std::unique_ptr<std::vector<char>> stream) {
  if (stream->empty()) {
    logger_->Error("Stream empty");
    return false;
  }
  // Write stream
  auto nBytes = send(GetFileDescriptor(), stream->data(), stream->size(), 0);
  if (nBytes != static_cast<ssize_t>(stream->size())) {
    logger_->Error("Failed to write socket");
    return false;
  }
  return true;
}

bool ClientTCP::Run() {
  // Wait for input data
  struct timeval tv{};
  tv.tv_sec = 0;
  tv.tv_usec = 100000;

  fd_set rfds;
  FD_ZERO(&rfds);
  FD_SET(GetFileDescriptor(), &rfds);

  auto n = select(GetFileDescriptor() + 1, &rfds, nullptr, nullptr, &tv);
  if (n < 0) {
    logger_->Error("Select error");
    return false;
  } else if (n == 0) {
    // Select timeout
    return true;
  }
  // Receive data
  auto inBuffer = std::make_unique<std::vector<char>>();
  inBuffer->resize(1024);
  auto nBytes = recv(
    GetFileDescriptor(),
    inBuffer->data(),
    inBuffer->size(),
    0);
  // Disconnect because of remote logout
  if (nBytes == 0) {
    logger_->Info("Client remote logout");
    return false;
  }
  // Disconnect because of error
  if (nBytes < 0) {
    logger_->Error(std::string("Client socket error: ").append(std::strerror(errno)));
    return false;
  }
  // Handle received string
  inBuffer->resize(static_cast<uint32_t>(nBytes));
  // Process received data
  if (!Receive(std::move(inBuffer))) {
    logger_->Error("Client error");
    return false;
  }
  return true;
}

bool ClientTCP::Initialize() {
  return true;
}

bool ClientTCP::DeInitialize() {
  shutdown(GetFileDescriptor(), SHUT_RDWR);
  close(GetFileDescriptor());
  // Write log info
  logger_->Info(
    std::string("ip = ").append(GetIp())
      .append(", protocol = ").append(GetProtocol()->GetProtocolType().ToString())
      .append(", fd = ").append(std::to_string(GetFileDescriptor())));
  return false;
}
}  // namespace Communication::Clients
