/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <unistd.h>

#include <utility>
#include "Logger/ILogger.h"
#include "Messages/MessageItem.h"
#include "Messages/Message.h"
#include "Messages/MessageLock.h"
#include "ResourceSharing/Address.h"
#include "Communication/StreamSplitter/StreamSplitterBase.h"
#include "Communication/Protocols/ProtocolBase.h"
#include "Communication/Clients/ClientBase.h"
#include "Communication/ConnectionListener/ConnectionListenerBase.h"
#include "ClientHandler.h"

namespace Communication {

const size_t ClientHandler::kMaxClients = 4;

const char ClientHandler::kName[] = "ClientHandler";

ClientHandler::ClientHandler(
  Logger::ILogger *logger,
  std::string lockIp)
  : Utils::ThreadBase(logger, kName)
  , IClientHandlerAddClient()
  , logger_(logger)
  , lockIp_(std::move(lockIp)) {
}

bool ClientHandler::AddConnectionListener(
  std::unique_ptr<ConnectionListener::ConnectionListenerBase> connectionListener) {
  std::unique_lock<std::mutex> lock(mutexClients_);
  // Check if connection listener already exists
  auto connectionListenerOld = connectionListeners_.find(connectionListener->GetFileDescriptor());
  if (connectionListenerOld != connectionListeners_.end()) {
    // Remove old connection listener
    connectionListenerOld->second->Stop();
    clients_.erase(connectionListenerOld->second->GetFileDescriptor());
  }
  // Connect listener
  if (!connectionListener->Start()) {
    logger_->Error("Failed to initialize connection listener");
    return false;
  }
  // Set client handler
  if (!connectionListener->SetClientHandler(this)) {
    logger_->Error("Failed to set client handler");
    return false;
  }
  // Emplace connection listener
  connectionListeners_.emplace(connectionListener->GetFileDescriptor(), std::move(connectionListener));
  return true;
}

bool ClientHandler::HasClient(const std::string &address) {
  std::unique_lock<std::mutex> lock(mutexClients_);
  for (const auto &client : lockClients_) {
    if (client->GetIp() == address) {
      return true;
    }
  }
  return false;
}

bool ClientHandler::AddClient(std::unique_ptr<Clients::ClientBase> clientNew) {
  std::unique_lock<std::mutex> lock(mutexClients_);
  switch (clientNew->protocol_->GetProtocolType().GetValue()) {
    case Types::ProtocolType::Enum::FBIS_NETWORK:
    case Types::ProtocolType::Enum::JSON: {
      // Check if client already exists
      auto client = clients_.find(clientNew->GetFileDescriptor());
      if (client != clients_.end()) {
        // Remove old client
        client->second->Stop();
        clients_.erase(client->second->GetFileDescriptor());
      }
      // Check maximum client count
      if (clients_.size() >= kMaxClients) {
        return false;
      }
      // Start client
      if (!clientNew->Start()) {
        return false;
      }
      // Emplace client
      clients_.emplace(clientNew->GetFileDescriptor(), std::move(clientNew));
      return true;
    }
    case Types::ProtocolType::Enum::ARBITRATION: {
      // Check if client already exists
      for (const auto &client : lockClients_) {
        // If a connection already exists, then the handler with the lower
        // ip address decides which client should be used further
        if (clientNew->GetIp() == client->GetIp() &&
            clientNew->GetIp() > lockIp_) {
          // The new client has to be deleted
          clientNew->Stop();
          return true;
        }
      }
      // Start client
      if (!clientNew->Start()) {
        return false;
      }
      // Emplace client
      lockClients_.push_back(std::move(clientNew));
      return true;
    }
    default: {
      return false;
    }
  }
}

bool ClientHandler::OnMessageReceived(Messages::MessageBase *message) {
  return message->Accept(this);
}

bool ClientHandler::Visit(Messages::Message * message) {
  std::unique_lock<std::mutex> lock(mutexClients_);
  auto client = clients_.find(message->GetClientFileDescriptor());
  if (client == clients_.end()) {
    logger_->Error(std::string("Client not found (fd = ")
                   .append(std::to_string(message->GetClientFileDescriptor()).append(")")));
    return false;
  }
  if (!client->second->Send(message)) {
    logger_->Error("Failed to send message");
  }
  return true;
}

bool ClientHandler::Visit(Messages::MessageLock * message) {
  std::unique_lock<std::mutex> lock(mutexClients_);
  for (const auto &client : lockClients_) {
    if (client->GetIp() == message->GetReceiver().GetIp()) {
      if (!client->Send(reinterpret_cast<Messages::Message *>(message))) {
        logger_->Error("Failed to send message");
        return false;
      }
      return true;
    }
  }
  logger_->Error("Client not found");
  return false;
}

bool ClientHandler::Run() {
  std::this_thread::sleep_for(std::chrono::milliseconds(100));
  std::unique_lock<std::mutex> lock(mutexClients_);
  // Update all connection listeners
  for (auto iterator = connectionListeners_.begin();
       iterator != connectionListeners_.end();) {
    if (!iterator->second->IsRunning()) {
      // Remove connection listener
      iterator->second->Stop();
      iterator = connectionListeners_.erase(iterator);
      continue;
    }
    iterator++;
  }
  // Update all clients
  for (auto iterator = clients_.begin();
       iterator != clients_.end();) {
    if (!iterator->second->IsRunning()) {
      // Remove client
      iterator->second->Stop();
      iterator = clients_.erase(iterator);
      continue;
    }
    iterator++;
  }
  // Update all lock clients
  for (auto iterator = lockClients_.begin();
       iterator != lockClients_.end();) {
    if (!iterator->get()->IsRunning()) {
      // Remove client
      iterator->get()->Stop();
      iterator = lockClients_.erase(iterator);
      continue;
    }
    iterator++;
  }
  return true;
}


bool ClientHandler::Initialize() {
  return true;
}

bool ClientHandler::DeInitialize() {
  // Finish all lock clients
  for (auto iterator = lockClients_.begin();
       iterator != lockClients_.end();) {
    iterator->get()->Stop();
    iterator = lockClients_.erase(iterator);
  }
  // Finish all  clients
  for (auto iterator = clients_.begin();
       iterator != clients_.end();) {
    iterator->second->Stop();
    iterator = clients_.erase(iterator);
  }
  // Finish all lock clients
  for (auto iterator = connectionListeners_.begin();
       iterator != connectionListeners_.end();) {
    iterator->second->Stop();
    iterator = connectionListeners_.erase(iterator);
  }
  return false;
}

}  // namespace Communication
