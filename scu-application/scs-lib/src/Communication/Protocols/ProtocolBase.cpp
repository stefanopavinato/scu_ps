/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Logger/ILogger.h"
#include "ProtocolBase.h"

namespace Communication::Protocols {

ProtocolBase::ProtocolBase(
  Logger::ILogger *logger,
  const Types::ProtocolType::Enum &protocolType)
  : logger_(logger), protocolType_(Types::ProtocolType(protocolType)) {
}

std::experimental::optional<std::unique_ptr<std::vector<char>>> ProtocolBase::Serialize(
  Messages::MessageBase *message) {
  if (!OnMessageReceived(message)) {
    return std::experimental::nullopt;
  }
  return std::move(out_);
}

Logger::ILogger *ProtocolBase::Logger() const {
  return logger_;
}

Types::ProtocolType ProtocolBase::GetProtocolType() const {
  return protocolType_;
}

}  // namespace Communication::Protocols
