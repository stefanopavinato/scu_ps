/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <experimental/optional>
#include <memory>
#include "Messages/MessageTemplate.h"
#include "Messages/MessageItemProperty.h"
#include "Communication/Types/RequestId.h"

namespace Communication::Protocols::FbisNetwork {
class FbisMessageFactory {
 public:
  static std::experimental::optional<std::unique_ptr<Messages::MessageTemplate<Messages::MessageItemProperty>>>
  DeSerialize(
    const Communication::Types::RequestId &requestId,
    std::unique_ptr<std::vector<char>> input);

  static std::experimental::optional<std::unique_ptr<std::vector<char>>> Serialize(
    const Messages::MessageTemplate<Messages::MessageItemProperty> &message);
};
}  // namespace Communication::Protocols::FbisNetwork
