/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Types/Value.h"
#include "Types/VariantValue.h"
#include "Types/ModuleType.h"
#include "Messages/Types/MessageItemState.h"
#include "Messages/Types/DataStatus.h"
#include "Messages/Visitor/VisitorPropertyGet.h"
#include "Messages/Visitor/VisitorPropertySet.h"
#include "Messages/MessageItemProperty.h"
#include "Messages/MessageTemplate.h"
#include "LVDSMCDatalinkErrorsClear.h"

namespace Communication::Protocols::FbisNetwork {

const char LVDSMCDatalinkErrorsClear::kBoardName[] = "BoardName";
const char LVDSMCDatalinkErrorsClear::kMezzanineCardPresent[] = "MezzanineCardPresent";
const char LVDSMCDatalinkErrorsClear::kDatalinkPort1TimeExpectationTimeoutsResetName[] =
    "MCDriverPort1/Status/DatalinkTimeExpectationTimeoutCounterReset";
const char LVDSMCDatalinkErrorsClear::kDatalinkPort1LifeSignErrorsResetName[] =
    "MCDriverPort1/Status/DatalinkLifeSignErrorCounterReset";
const char LVDSMCDatalinkErrorsClear::kDatalinkPort1DecoderErrorsResetName[] =
    "MCDriverPort1/Status/DatalinkDecoderErrorCounterReset";
const char LVDSMCDatalinkErrorsClear::kDatalinkPort2TimeExpectationTimeoutsResetName[] =
    "MCDriverPort2/Status/DatalinkTimeExpectationTimeoutCounterReset";
const char LVDSMCDatalinkErrorsClear::kDatalinkPort2LifeSignErrorsResetName[] =
    "MCDriverPort2/Status/DatalinkLifeSignErrorCounterReset";
const char LVDSMCDatalinkErrorsClear::kDatalinkPort2DecoderErrorsResetName[] =
    "MCDriverPort2/Status/DatalinkDecoderErrorCounterReset";
const char LVDSMCDatalinkErrorsClear::kDatalinkPort3TimeExpectationTimeoutsResetName[] =
    "MCDriverPort3/Status/DatalinkTimeExpectationTimeoutCounterReset";
const char LVDSMCDatalinkErrorsClear::kDatalinkPort3LifeSignErrorsResetName[] =
    "MCDriverPort3/Status/DatalinkLifeSignErrorCounterReset";
const char LVDSMCDatalinkErrorsClear::kDatalinkPort3DecoderErrorsResetName[] =
    "MCDriverPort3/Status/DatalinkDecoderErrorCounterReset";

std::experimental::optional<std::unique_ptr<Messages::MessageTemplate<Messages::MessageItemProperty>>>
LVDSMCDatalinkErrorsClear::Deserialize(std::unique_ptr<std::vector<char>> input) {
  // Create message
  auto message = std::make_unique<Messages::MessageTemplate<Messages::MessageItemProperty>>();
  // Set request id
  message->SetRequestId(Communication::Types::RequestId::Enum::SCU_LVDSMC_DATALINK_ERRORS_CLEAR);
  // Check message length
  auto length = (*input).size();
  if (length != 6) {  // outer frame 5 chars, inner frame 1 char
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_REQUEST_FORMAT);
    return std::move(message);
  }
  // Decode slot
  auto slot = static_cast<uint8_t>((*input)[kFirstParam]);
  if (slot < kSlotMin || slot > kSlotMax) {
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::SLOT_NOT_VALID);
    return std::move(message);
  }
  // Add mezzanine card board name
  message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
          kBoardName,
          std::string("MC").append(std::to_string(slot)).append("/Management/EEProm/BoardName"),
          std::make_unique<Messages::Visitor::VisitorPropertyGet>(
              Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add mezzanine card present item
  message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
          kMezzanineCardPresent,
          std::string("MC").append(std::to_string(slot)).append("/CardDetectionItems/MezzanineCardPresent"),
          std::make_unique<Messages::Visitor::VisitorPropertyGet>(
              Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Clear error counters
  ::Types::TestVariantValue value;
  value.SetValue(true);
  message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
          kDatalinkPort1TimeExpectationTimeoutsResetName,
          std::string("MC").append(std::to_string(slot))
              .append("/Application/MCDriver/").append(kDatalinkPort1TimeExpectationTimeoutsResetName),
          std::make_unique<Messages::Visitor::VisitorPropertySet>(
              Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT_FORCE_ONCE,
              ::Types::ValueType::Enum::BOOL,
              value)));
  message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
          kDatalinkPort1LifeSignErrorsResetName,
          std::string("MC").append(std::to_string(slot))
              .append("/Application/MCDriver/").append(kDatalinkPort1LifeSignErrorsResetName),
          std::make_unique<Messages::Visitor::VisitorPropertySet>(
              Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT_FORCE_ONCE,
              ::Types::ValueType::Enum::BOOL,
              value)));
  message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
          kDatalinkPort1DecoderErrorsResetName,
          std::string("MC").append(std::to_string(slot))
              .append("/Application/MCDriver/").append(kDatalinkPort1DecoderErrorsResetName),
          std::make_unique<Messages::Visitor::VisitorPropertySet>(
              Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT_FORCE_ONCE,
              ::Types::ValueType::Enum::BOOL,
              value)));
  message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
          kDatalinkPort2TimeExpectationTimeoutsResetName,
          std::string("MC").append(std::to_string(slot))
              .append("/Application/MCDriver/").append(kDatalinkPort2TimeExpectationTimeoutsResetName),
          std::make_unique<Messages::Visitor::VisitorPropertySet>(
              Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT_FORCE_ONCE,
              ::Types::ValueType::Enum::BOOL,
              value)));
  message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
          kDatalinkPort2LifeSignErrorsResetName,
          std::string("MC").append(std::to_string(slot))
              .append("/Application/MCDriver/").append(kDatalinkPort2LifeSignErrorsResetName),
          std::make_unique<Messages::Visitor::VisitorPropertySet>(
              Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT_FORCE_ONCE,
              ::Types::ValueType::Enum::BOOL,
              value)));
  message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
          kDatalinkPort2DecoderErrorsResetName,
          std::string("MC").append(std::to_string(slot))
              .append("/Application/MCDriver/").append(kDatalinkPort2DecoderErrorsResetName),
          std::make_unique<Messages::Visitor::VisitorPropertySet>(
              Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT_FORCE_ONCE,
              ::Types::ValueType::Enum::BOOL,
              value)));
  message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
          kDatalinkPort3TimeExpectationTimeoutsResetName,
          std::string("MC").append(std::to_string(slot))
              .append("/Application/MCDriver/").append(kDatalinkPort3TimeExpectationTimeoutsResetName),
          std::make_unique<Messages::Visitor::VisitorPropertySet>(
              Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT_FORCE_ONCE,
              ::Types::ValueType::Enum::BOOL,
              value)));
  message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
          kDatalinkPort3LifeSignErrorsResetName,
          std::string("MC").append(std::to_string(slot))
              .append("/Application/MCDriver/").append(kDatalinkPort3LifeSignErrorsResetName),
          std::make_unique<Messages::Visitor::VisitorPropertySet>(
              Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT_FORCE_ONCE,
              ::Types::ValueType::Enum::BOOL,
              value)));
  message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
          kDatalinkPort3DecoderErrorsResetName,
          std::string("MC").append(std::to_string(slot))
              .append("/Application/MCDriver/").append(kDatalinkPort3DecoderErrorsResetName),
          std::make_unique<Messages::Visitor::VisitorPropertySet>(
              Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT_FORCE_ONCE,
              ::Types::ValueType::Enum::BOOL,
              value)));
  return std::move(message);
}

std::experimental::optional<std::unique_ptr<std::vector<char>>> LVDSMCDatalinkErrorsClear::Serialize(
    const Messages::MessageTemplate<Messages::MessageItemProperty> &message) {
  // Prepare out vector
  auto out = std::make_unique<std::vector<char>>();
  auto dataStatus = message.GetDataStatus();
  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    for (const auto &item : message.GetMessageItems()) {
      if (item->ItemState()->GetValue() != Messages::Types::MessageItemState::Enum::OK) {
        dataStatus = Messages::Types::DataStatus::Enum::INFORMATION_NOT_AVAILABLE;
        break;
      }
    }
  }
  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    // ======== Check MC ========
    if (const auto &item = message.GetMessageItem(kMezzanineCardPresent)) {
      if (!item.value()->GetValue<bool>().value()) {
        dataStatus = Messages::Types::DataStatus::Enum::MC_NOT_PRESENT;
      }
    }
  }
  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    if (const auto &item = message.GetMessageItem(kBoardName)) {
      if (const auto &retVal = item.value()->GetValue<::Types::ModuleType>()) {
        if (retVal.value().GetEnum() != ::Types::ModuleType::Enum::SCU_LVDSMC) {
          dataStatus = Messages::Types::DataStatus::Enum::WRONG_MEZZANINE_CARD;
        }
      } else {
        dataStatus = Messages::Types::DataStatus::Enum::INFORMATION_NOT_AVAILABLE;
      }
    }
  }

  // ======== Serialize "Data Status" ========
  out->push_back(static_cast<char>(dataStatus));
  return std::move(out);
}

}  // namespace Communication::Protocols::FbisNetwork
