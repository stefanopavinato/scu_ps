/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Types/Value.h"
#include "Types/ModuleType.h"
#include "Messages/Types/MessageItemState.h"
#include "Messages/Types/DataStatus.h"
#include "Messages/Visitor/VisitorPropertyGet.h"
#include "Messages/MessageItemProperty.h"
#include "Messages/MessageTemplate.h"
#include "SERStatusRead.h"

namespace Communication::Protocols::FbisNetwork {

const char SERStatusRead::kOperatingHours[] = "OperatingHours";

const char SERStatusRead::kPresent[] = "Present";
const char SERStatusRead::kTxFault[] = "TxFault";
const char SERStatusRead::kRxLos[] = "RxLos";
const char SERStatusRead::kTxDisable[] = "TxDisable";
const char SERStatusRead::kRateSelect0[] = "RateSelect0";
const char SERStatusRead::kRateSelect1[] = "RateSelect1";
const char SERStatusRead::kLinkState[] = "LinkState";
const char SERStatusRead::kPgmClkStatusLockDetect[] = "PgmClkStatusLockDetec";
const char SERStatusRead::kPgmClkStatusHoldover[] = "PgmClkStatusHoldover";
const char SERStatusRead::kPgmClkStatusClkIn0[] = "PgmClkStatusClkIn0";
const char SERStatusRead::kPgmClkStatusClkIn1[] = "PgmClkStatusClkIn1";
const char SERStatusRead::kPgmClkSync[] = "PgmClkSync";
const char SERStatusRead::kMainPowerSupplyPowerGood[] = "MainPowerSupplyPowerGood";
const char SERStatusRead::kMainPowerSupplyNotAlert[] = "MainPowerSupplyNotAlert";
const char SERStatusRead::kUSBPowerStatus[] = "USBPowerStatus";
const char SERStatusRead::kVccInternal0V85NotAlert[] = "VccInternal0V85NotAlert";
const char SERStatusRead::kTemperatureSensorAlert[] = "TemperatureSensorAlert";
const char SERStatusRead::kHdcDrdyInterrupt[] = "HdcDrdyInterrupt";

const char SERStatusRead::kHumidity[] = "Humidity";
const char SERStatusRead::kTemperatureAtHumidity[] = "TemperatureAtHumidity";
const char SERStatusRead::kMainCurrent[] = "MainCurrent";
const char SERStatusRead::kInternalMainVoltage[] = "InternalMainVoltage";
const char SERStatusRead::kInternalStandbyVoltage[] = "InternalStandbyVoltage";
const char SERStatusRead::kInternal3V3Voltage[] = "Internal3V3Voltage";
const char SERStatusRead::kInternal1V8Voltage[] = "Internal1V8Voltage";

const int32_t SERStatusRead::kCurrentScale = 50;
const int32_t SERStatusRead::kVoltageScale = 200;

std::experimental::optional<std::unique_ptr<Messages::MessageTemplate<Messages::MessageItemProperty>>>
SERStatusRead::Deserialize(std::unique_ptr<std::vector<char>> input) {
  // Create message
  auto message = std::make_unique<Messages::MessageTemplate<Messages::MessageItemProperty>>();
  // Set request id
  message->SetRequestId(Communication::Types::RequestId::Enum::SCU_SERIALIZER_STATUS_READ);
  // Check message length
  auto length = (*input).size();
  if (length != 5) {  // outer frame 5 chars, inner frame 0 char
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_REQUEST_FORMAT);
    return std::move(message);
  }
  // Add item operating hours
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kOperatingHours,
      std::string("Serializer").append("/Serializer/EEProm/OperatingHours"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add sfp modules
  for (uint8_t transceiverIndex = 1; transceiverIndex < 4; transceiverIndex++) {
    message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
        std::string(kTxDisable).append(std::to_string(transceiverIndex)),
        std::string("Serializer/Serializer/SfpModule")
          .append(std::to_string(transceiverIndex)).append("/TxDisable"),
        std::make_unique<Messages::Visitor::VisitorPropertyGet>(
          Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT)));
    message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
        std::string(kRateSelect0).append(std::to_string(transceiverIndex)),
        std::string("Serializer/Serializer/SfpModule")
          .append(std::to_string(transceiverIndex)).append("/RateSelect0"),
        std::make_unique<Messages::Visitor::VisitorPropertyGet>(
          Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT)));
    message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
        std::string(kRateSelect1).append(std::to_string(transceiverIndex)),
        std::string("Serializer/Serializer/SfpModule")
          .append(std::to_string(transceiverIndex)).append("/RateSelect1"),
        std::make_unique<Messages::Visitor::VisitorPropertyGet>(
          Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT)));
    message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
        std::string(kLinkState).append(std::to_string(transceiverIndex)),
        std::string("Serializer/Serializer/SfpModule")
          .append(std::to_string(transceiverIndex)).append("/LinkState"),
        std::make_unique<Messages::Visitor::VisitorPropertyGet>(
          Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT)));
    message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
        std::string(kPresent).append(std::to_string(transceiverIndex)),
        std::string("Serializer/Serializer/SfpModule")
          .append(std::to_string(transceiverIndex)).append("/Present"),
        std::make_unique<Messages::Visitor::VisitorPropertyGet>(
          Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
    message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
        std::string(kTxFault).append(std::to_string(transceiverIndex)),
        std::string("Serializer/Serializer/SfpModule").append(std::to_string(transceiverIndex)).append("/TxFault"),
        std::make_unique<Messages::Visitor::VisitorPropertyGet>(
          Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
    message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
        std::string(kRxLos).append(std::to_string(transceiverIndex)),
        std::string("Serializer/Serializer/SfpModule").append(std::to_string(transceiverIndex)).append("/RxLos"),
        std::make_unique<Messages::Visitor::VisitorPropertyGet>(
          Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  }
  // Add pgm clock status
  {
    message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
        kPgmClkStatusLockDetect,
        std::string("Serializer/Serializer").append("/PgmClkStatusLockDetect"),
        std::make_unique<Messages::Visitor::VisitorPropertyGet>(
          Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
    message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
        kPgmClkStatusHoldover,
        std::string("Serializer/Serializer").append("/PgmClkStatusHoldover"),
        std::make_unique<Messages::Visitor::VisitorPropertyGet>(
          Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
    message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
        kPgmClkStatusClkIn0,
        std::string("Serializer/Serializer").append("/PgmClkStatusClkIn0"),
        std::make_unique<Messages::Visitor::VisitorPropertyGet>(
          Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
    message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
        kPgmClkStatusClkIn1,
        std::string("Serializer/Serializer").append("/PgmClkStatusClkIn1"),
        std::make_unique<Messages::Visitor::VisitorPropertyGet>(
          Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
    message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
        kPgmClkSync,
        std::string("Serializer/Serializer").append("/PgmClkSync"),
        std::make_unique<Messages::Visitor::VisitorPropertyGet>(
          Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  }
  // Add power status
  {
    message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
        kMainPowerSupplyPowerGood,
        std::string("Serializer/Serializer").append("/MainPowerSupplyPowerGood"),
        std::make_unique<Messages::Visitor::VisitorPropertyGet>(
          Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
    message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
        kMainPowerSupplyNotAlert,
        std::string("Serializer/Serializer").append("/MainPowerSupplyNotAlert"),
        std::make_unique<Messages::Visitor::VisitorPropertyGet>(
          Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
    message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
        kUSBPowerStatus,
        std::string("Serializer/Serializer").append("/USBPowerStatus"),
        std::make_unique<Messages::Visitor::VisitorPropertyGet>(
          Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
    message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
        kVccInternal0V85NotAlert,
        std::string("Serializer/Serializer").append("/VccInternal0V85NotAlert"),
        std::make_unique<Messages::Visitor::VisitorPropertyGet>(
          Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  }
  // Add temperature status
  {
    message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
        kTemperatureSensorAlert,
        std::string("Serializer/Serializer").append("/TemperatureSensorAlert"),
        std::make_unique<Messages::Visitor::VisitorPropertyGet>(
          Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
    message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
        kHdcDrdyInterrupt,
        std::string("Serializer/Serializer").append("/HdcDrdyInterrupt"),
        std::make_unique<Messages::Visitor::VisitorPropertyGet>(
          Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  }
  // Add humidity
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kHumidity,
      std::string("Serializer/Serializer").append("/HumiditySensor/Humidity"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add item internal main voltage
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kTemperatureAtHumidity,
      std::string("Serializer/Serializer").append("/HumiditySensor/Temperature"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add item main current
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kMainCurrent,
      std::string("Serializer/Serializer").append("/HotSwapController/MainCurrent"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add item internal main voltage
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kInternalMainVoltage,
      std::string("Serializer/Serializer").append("/HotSwapController/MainVoltage"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add item internal standby voltage
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kInternalStandbyVoltage,
      std::string("Serializer/Serializer").append("/HotSwapController/StandbyVoltage"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Return message
  return std::move(message);
}

std::experimental::optional<std::unique_ptr<std::vector<char>>> SERStatusRead::Serialize(
  const Messages::MessageTemplate<Messages::MessageItemProperty> &message) {
  // Prepare out vector
  auto out = std::make_unique<std::vector<char>>();
  auto dataStatus = message.GetDataStatus();
  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    for (const auto &item : message.GetMessageItems()) {
      if (item->ItemState()->GetValue() != Messages::Types::MessageItemState::Enum::OK) {
        dataStatus = Messages::Types::DataStatus::Enum::INFORMATION_NOT_AVAILABLE;
        break;
      }
    }
  }

  // ======== Serialize "Data Status" ========
  FbisMessageBase::Serialize(out.get(), static_cast<uint8_t>(dataStatus));

  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    // ======== Serialize "MC Operating Hours" ========
    if (const auto &item = message.GetMessageItem(kOperatingHours)) {
      if (const auto &retVal = item.value()->GetValue<std::chrono::minutes>()) {
        auto operationTime = static_cast<uint32_t>(retVal.value().count() / 15);  // Cast to 15 min! (add const)
        FbisMessageBase::Serialize(out.get(), operationTime);
      }
    }
    // ======== Serialize "SFP Control/Status" ========
    for (uint8_t transceiverIndex = 1; transceiverIndex < 4; transceiverIndex++) {
      uint8_t sfpStatus = 0;
      if (const auto &item = message.GetMessageItem(std::string(kTxDisable)
                                                      .append(std::to_string(transceiverIndex)))) {
        if (const auto &retVal = item.value()->GetValue<bool>()) {
          auto txDisable = static_cast<bool>(retVal.value());
          if (txDisable) {
            sfpStatus |= 0x01;
          }
        }
      }
      if (const auto &item = message.GetMessageItem(std::string(kRateSelect0)
                                                      .append(std::to_string(transceiverIndex)))) {
        if (const auto &retVal = item.value()->GetValue<bool>()) {
          auto rateSelect = static_cast<bool>(retVal.value());
          if (rateSelect) {
            sfpStatus |= 0x02;
          }
        }
      }
      if (const auto &item1 = message.GetMessageItem(std::string(kRateSelect1)
                                                       .append(std::to_string(transceiverIndex)))) {
        if (const auto &retVal = item1.value()->GetValue<bool>()) {
          auto rateSelect = static_cast<bool>(retVal.value());
          if (rateSelect) {
            sfpStatus |= 0x04;
          }
        }
      }
      if (const auto &item = message.GetMessageItem(std::string(kLinkState)
                                                      .append(std::to_string(transceiverIndex)))) {
        if (const auto &retVal = item.value()->GetValue<bool>()) {
          auto txFault = static_cast<bool>(retVal.value());
          if (txFault) {
            sfpStatus |= 0x08;
          }
        }
      }
      if (const auto &item = message.GetMessageItem(std::string(kPresent)
                                                      .append(std::to_string(transceiverIndex)))) {
        if (const auto &retVal = item.value()->GetValue<bool>()) {
          auto notpresent = !static_cast<bool>(retVal.value());
          if (notpresent) {
            sfpStatus |= 0x10;
          }
        }
      }
      if (const auto &item = message.GetMessageItem(std::string(kTxFault)
                                                      .append(std::to_string(transceiverIndex)))) {
        if (const auto &retVal = item.value()->GetValue<bool>()) {
          auto txFault = static_cast<bool>(retVal.value());
          if (txFault) {
            sfpStatus |= 0x20;
          }
        }
      }
      if (const auto &item = message.GetMessageItem(std::string(kRxLos)
                                                      .append(std::to_string(transceiverIndex)))) {
        if (const auto &retVal = item.value()->GetValue<bool>()) {
          auto rxLos = static_cast<bool>(retVal.value());
          if (rxLos) {
            sfpStatus |= 0x40;
          }
        }
      }
      FbisMessageBase::Serialize(out.get(), sfpStatus);
    }
    // ======== Serialize "PGM CLK Status" ========
    {
      uint8_t pgmClkStatus = 0;
      if (const auto &item = message.GetMessageItem(kPgmClkStatusLockDetect)) {
        if (const auto &retVal = item.value()->GetValue<bool>()) {
          auto itemVal = static_cast<bool>(retVal.value());
          if (itemVal) {
            pgmClkStatus |= 0x01;
          }
        }
      }
      if (const auto &item = message.GetMessageItem(kPgmClkStatusHoldover)) {
        if (const auto &retVal = item.value()->GetValue<bool>()) {
          auto itemVal = static_cast<bool>(retVal.value());
          if (itemVal) {
            pgmClkStatus |= 0x02;
          }
        }
      }
      if (const auto &item = message.GetMessageItem(kPgmClkStatusClkIn0)) {
        if (const auto &retVal = item.value()->GetValue<bool>()) {
          auto itemVal = static_cast<bool>(retVal.value());
          if (itemVal) {
            pgmClkStatus |= 0x04;
          }
        }
      }
      if (const auto &item = message.GetMessageItem(kPgmClkStatusClkIn1)) {
        if (const auto &retVal = item.value()->GetValue<bool>()) {
          auto itemVal = static_cast<bool>(retVal.value());
          if (itemVal) {
            pgmClkStatus |= 0x08;
          }
        }
      }
      if (const auto &item = message.GetMessageItem(kPgmClkSync)) {
        if (const auto &retVal = item.value()->GetValue<bool>()) {
          auto itemVal = static_cast<bool>(retVal.value());
          if (itemVal) {
            pgmClkStatus |= 0x10;
          }
        }
      }
      FbisMessageBase::Serialize(out.get(), pgmClkStatus);
    }
    // ======== Serialize "Power Status" ========
    {
      uint8_t powerStatus = 0;
      if (const auto &item = message.GetMessageItem(kMainPowerSupplyPowerGood)) {
        if (const auto &retVal = item.value()->GetValue<bool>()) {
          auto itemVal = static_cast<bool>(retVal.value());
          if (itemVal) {
            powerStatus |= 0x01;
          }
        }
      }
      if (const auto &item = message.GetMessageItem(kMainPowerSupplyNotAlert)) {
        if (const auto &retVal = item.value()->GetValue<bool>()) {
          auto itemVal = static_cast<bool>(retVal.value());
          if (itemVal) {
            powerStatus |= 0x02;
          }
        }
      }
      if (const auto &item = message.GetMessageItem(kUSBPowerStatus)) {
        if (const auto &retVal = item.value()->GetValue<bool>()) {
          auto itemVal = static_cast<bool>(retVal.value());
          if (itemVal) {
            powerStatus |= 0x04;
          }
        }
      }
      if (const auto &item = message.GetMessageItem(kVccInternal0V85NotAlert)) {
        if (const auto &retVal = item.value()->GetValue<bool>()) {
          auto itemVal = static_cast<bool>(retVal.value());
          if (itemVal) {
            powerStatus |= 0x08;
          }
        }
      }
      FbisMessageBase::Serialize(out.get(), powerStatus);
    }
    // ======== Serialize "Temperature Status" ========
    {
      uint8_t temperatureStatus = 0;
      if (const auto &item = message.GetMessageItem(kTemperatureSensorAlert)) {
        if (const auto &retVal = item.value()->GetValue<bool>()) {
          auto itemVal = static_cast<bool>(retVal.value());
          if (itemVal) {
            temperatureStatus |= 0x01;
          }
        }
      }
      if (const auto &item = message.GetMessageItem(kHdcDrdyInterrupt)) {
        if (const auto &retVal = item.value()->GetValue<bool>()) {
          auto itemVal = static_cast<bool>(retVal.value());
          if (itemVal) {
            temperatureStatus |= 0x02;
          }
        }
      }
      FbisMessageBase::Serialize(out.get(), temperatureStatus);
    }
    // ======== Serialize "Humidity" ========
    if (const auto &item = message.GetMessageItem(kHumidity)) {
      if (const auto &retVal = item.value()->GetValue<::Types::Value<int32_t>>()) {
        auto current = static_cast<int16_t>(retVal.value().GetValue());
        FbisMessageBase::Serialize(out.get(), current);
      }
    }
    // ======== Serialize "Temperature at Humidity Sensor" ========
    if (const auto &item = message.GetMessageItem(kTemperatureAtHumidity)) {
      if (const auto &retVal = item.value()->GetValue<::Types::Value<int32_t>>()) {
        auto voltage = static_cast<int32_t>(retVal.value().GetValue());
        FbisMessageBase::Serialize(out.get(), voltage);
      }
    }
    // ======== Serialize "MAIN Supply Current" ========
    if (const auto &item = message.GetMessageItem(kMainCurrent)) {
      if (const auto &retVal = item.value()->GetValue<::Types::Value<int32_t>>()) {
        auto current = static_cast<int8_t>((retVal.value().GetValue() + kCurrentScale / 2) / kCurrentScale);
        FbisMessageBase::Serialize(out.get(), current);
      }
    }
    // ======== Serialize "Internal MAIN Supply Voltage" ========
    if (const auto &item = message.GetMessageItem(kInternalMainVoltage)) {
      if (const auto &retVal = item.value()->GetValue<::Types::Value<int32_t>>()) {
        auto voltage = static_cast<int8_t>((retVal.value().GetValue() + kVoltageScale / 2) / kVoltageScale);
        FbisMessageBase::Serialize(out.get(), voltage);
      }
    }
    // ======== Serialize "Internal STANDBY Supply Voltage" ========
    if (const auto &item = message.GetMessageItem(kInternalStandbyVoltage)) {
      if (const auto &retVal = item.value()->GetValue<::Types::Value<int32_t>>()) {
        auto voltage = static_cast<int8_t>((retVal.value().GetValue() + kVoltageScale / 2) / kVoltageScale);
        FbisMessageBase::Serialize(out.get(), voltage);
      }
    }
  }
  return std::move(out);
}

}  // namespace Communication::Protocols::FbisNetwork
