/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "FbisMessageBase.h"

namespace Communication::Protocols::FbisNetwork {

const uint32_t FbisMessageBase::kFirstParam = 3;
const uint32_t FbisMessageBase::kSlotMin = 1;
const uint32_t FbisMessageBase::kSlotMax = 12;

bool FbisMessageBase::Serialize(std::vector<char> *out, const int32_t &value) {
  if (!out) {
    return false;
  }
  out->push_back(static_cast<char>((value >> 24) & 0x000000FF));
  out->push_back(static_cast<char>((value >> 16) & 0x000000FF));
  out->push_back(static_cast<char>((value >> 8) & 0x000000FF));
  out->push_back(static_cast<char>((value >> 0) & 0x000000FF));
  return true;
}

bool FbisMessageBase::Serialize(std::vector<char> *out, const int16_t &value) {
  if (!out) {
    return false;
  }
  out->push_back(static_cast<char>((value >> 8) & 0x00FF));
  out->push_back(static_cast<char>((value >> 0) & 0x00FF));
  return true;
}

bool FbisMessageBase::Serialize(std::vector<char> *out, const int8_t &value) {
  if (!out) {
    return false;
  }
  out->push_back(static_cast<char>(value));
  return true;
}

bool FbisMessageBase::Serialize(std::vector<char> *out, const uint32_t &value) {
  if (!out) {
    return false;
  }
  out->push_back(static_cast<char>((value >> 24) & 0x000000FF));
  out->push_back(static_cast<char>((value >> 16) & 0x000000FF));
  out->push_back(static_cast<char>((value >> 8) & 0x000000FF));
  out->push_back(static_cast<char>((value >> 0) & 0x000000FF));
  return true;
}

bool FbisMessageBase::Serialize(std::vector<char> *out, const uint16_t &value) {
  if (!out) {
    return false;
  }
  out->push_back(static_cast<char>((value >> 8) & 0x00FF));
  out->push_back(static_cast<char>((value >> 0) & 0x00FF));
  return true;
}

bool FbisMessageBase::Serialize(std::vector<char> *out, const uint8_t &value) {
  if (!out) {
    return false;
  }
  out->push_back(static_cast<char>(value));
  return true;
}

}  // namespace Communication::Protocols::FbisNetwork
