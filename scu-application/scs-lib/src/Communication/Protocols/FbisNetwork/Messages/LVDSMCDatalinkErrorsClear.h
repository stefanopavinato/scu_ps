/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <experimental/optional>
#include <memory>
#include "FbisMessageBase.h"

namespace Messages {
template<typename T>
class MessageTemplate;

class MessageItemProperty;
}  // namespace Messages
namespace Communication::Protocols::FbisNetwork {
class LVDSMCDatalinkErrorsClear : public FbisMessageBase {
 public:
  static std::experimental::optional<std::unique_ptr<Messages::MessageTemplate<Messages::MessageItemProperty>>>
  Deserialize(std::unique_ptr<std::vector<char>> input);

  static std::experimental::optional<std::unique_ptr<std::vector<char>>> Serialize(
      const Messages::MessageTemplate<Messages::MessageItemProperty> &message);

 private:
  static const char kBoardName[];
  static const char kMezzanineCardPresent[];
  static const char kDatalinkPort1TimeExpectationTimeoutsResetName[];
  static const char kDatalinkPort1LifeSignErrorsResetName[];
  static const char kDatalinkPort1DecoderErrorsResetName[];
  static const char kDatalinkPort2TimeExpectationTimeoutsResetName[];
  static const char kDatalinkPort2LifeSignErrorsResetName[];
  static const char kDatalinkPort2DecoderErrorsResetName[];
  static const char kDatalinkPort3TimeExpectationTimeoutsResetName[];
  static const char kDatalinkPort3LifeSignErrorsResetName[];
  static const char kDatalinkPort3DecoderErrorsResetName[];
};
}  // namespace Communication::Protocols::FbisNetwork
