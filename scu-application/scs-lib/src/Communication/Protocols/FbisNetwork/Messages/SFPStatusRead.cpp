/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Types/Value.h"
#include "Types/ModuleType.h"
#include "Messages/Types/MessageItemState.h"
#include "Messages/Types/DataStatus.h"
#include "Messages/Visitor/VisitorPropertyGet.h"
#include "Messages/MessageItemProperty.h"
#include "Messages/MessageTemplate.h"
#include "SFPStatusRead.h"

namespace Communication::Protocols::FbisNetwork {

const uint32_t SFPStatusRead::kTransceiverIndexMin = 1;
const uint32_t SFPStatusRead::kTransceiverIndexMax = 4;

const char SFPStatusRead::kTxFault[] = "TxFault";
const char SFPStatusRead::kRxLos[] = "RxLos";
const char SFPStatusRead::kRateSelect0[] = "RateSelect0";
const char SFPStatusRead::kRateSelect1[] = "RateSelect1";
const char SFPStatusRead::kTxDisable[] = "TxDisable";
const char SFPStatusRead::kPresent[] = "Present";

std::experimental::optional<std::unique_ptr<Messages::MessageTemplate<Messages::MessageItemProperty>>>
SFPStatusRead::Deserialize(std::unique_ptr<std::vector<char>> input) {
  // Create message
  auto message = std::make_unique<Messages::MessageTemplate<Messages::MessageItemProperty>>();
  // Set request id
  message->SetRequestId(Communication::Types::RequestId::Enum::SCU_SFP_STATUS_READ);
  // Check message length
  auto length = (*input).size();
  if (length != 6) {  // outer frame 5 chars, inner frame 1 char
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_REQUEST_FORMAT);
    return std::move(message);
  }
  // Decode transceiverIndex
  auto transceiverIndex = static_cast<uint8_t>((*input)[kFirstParam]);
  if (transceiverIndex < kTransceiverIndexMin || transceiverIndex > kTransceiverIndexMax) {
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::SLOT_NOT_VALID);
    return std::move(message);
  }
  // Add MC Driver status signals
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kTxFault,
      std::string("Serializer/Serializer/SfpModule").append(std::to_string(transceiverIndex)).append("/TxFault"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kRxLos,
      std::string("Serializer/Serializer/SfpModule").append(std::to_string(transceiverIndex)).append("/RxLos"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kRateSelect0,
      std::string("Serializer/Serializer/SfpModule")
        .append(std::to_string(transceiverIndex)).append("/RateSelect0"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT)));
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kRateSelect1,
      std::string("Serializer/Serializer/SfpModule")
        .append(std::to_string(transceiverIndex)).append("/RateSelect1"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT)));
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kTxDisable,
      std::string("Serializer/Serializer/SfpModule").append(std::to_string(transceiverIndex)).append("/TxDisable"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT)));
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kPresent,
      std::string("Serializer/Serializer/SfpModule").append(std::to_string(transceiverIndex)).append("/Present"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Return message
  return std::move(message);
}

std::experimental::optional<std::unique_ptr<std::vector<char>>> SFPStatusRead::Serialize(
  const Messages::MessageTemplate<Messages::MessageItemProperty> &message) {
  // Prepare out vector
  auto out = std::make_unique<std::vector<char>>();
  auto dataStatus = message.GetDataStatus();
  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    for (const auto &item : message.GetMessageItems()) {
      if (item->ItemState()->GetValue() != Messages::Types::MessageItemState::Enum::OK) {
        dataStatus = Messages::Types::DataStatus::Enum::INFORMATION_NOT_AVAILABLE;
        break;
      }
    }
  }

  // ======== Serialize "Data Status" ========
  FbisMessageBase::Serialize(out.get(), static_cast<uint8_t>(dataStatus));

  // Serialize SFP status
  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    uint8_t sfpStatus = 0;
    if (const auto &item = message.GetMessageItem(kTxFault)) {
      if (const auto &retVal = item.value()->GetValue<bool>()) {
        auto txFault = static_cast<bool>(retVal.value());
        if (txFault) {
          sfpStatus |= 0x01;
        }
      }
    }
    if (const auto &item = message.GetMessageItem(kRxLos)) {
      if (const auto &retVal = item.value()->GetValue<bool>()) {
        auto rxLos = static_cast<bool>(retVal.value());
        if (rxLos) {
          sfpStatus |= 0x02;
        }
      }
    }
    if (const auto &item0 = message.GetMessageItem(kRateSelect0)) {
      if (const auto &item1 = message.GetMessageItem(kRateSelect1)) {
        if (const auto &retVal0 = item0.value()->GetValue<bool>()) {
          if (const auto &retVal1 = item1.value()->GetValue<bool>()) {
            auto rateSelect = static_cast<bool>(retVal0.value()) && static_cast<bool>(retVal1.value());
            if (rateSelect) {
              sfpStatus |= 0x04;
            }
          }
        }
      }
    }
    if (const auto &item = message.GetMessageItem(kTxDisable)) {
      if (const auto &retVal = item.value()->GetValue<bool>()) {
        auto txDisable = static_cast<bool>(retVal.value());
        if (txDisable) {
          sfpStatus |= 0x08;
        }
      }
    }
    if (const auto &item = message.GetMessageItem(kPresent)) {
      if (const auto &retVal = item.value()->GetValue<bool>()) {
        auto present = static_cast<bool>(retVal.value());
        if (present) {
          sfpStatus |= 0x10;
        }
      }
    }
    FbisMessageBase::Serialize(out.get(), sfpStatus);
  }
  return std::move(out);
}
}  // namespace Communication::Protocols::FbisNetwork
