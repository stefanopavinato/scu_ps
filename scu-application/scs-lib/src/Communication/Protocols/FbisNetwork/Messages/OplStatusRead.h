/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <experimental/optional>
#include <memory>
#include "FbisMessageBase.h"

namespace Messages {
template<typename T>
class MessageTemplate;

class MessageItemProperty;
}  // namespace Messages
namespace Communication::Protocols::FbisNetwork {
class OplStatusRead : public FbisMessageBase {
 public:
  static std::experimental::optional<std::unique_ptr<Messages::MessageTemplate<Messages::MessageItemProperty>>>
  Deserialize(std::unique_ptr<std::vector<char>> input);

  static std::experimental::optional<std::unique_ptr<std::vector<char>>> Serialize(
    const Messages::MessageTemplate<Messages::MessageItemProperty> &message);

 private:
  static const uint32_t kTransceiverIndexMin;
  static const uint32_t kTransceiverIndexMax;

  static const char kOplStatusCollection[];
  static const char kDSOplErrors[];
  static const char kUSOplErrors[];
  static const char kDSOplEncodingSupervisionErrors[];
  static const char kDSOplLifeSignErrors[];
  static const char kDSOplRxFlagErrors[];
  static const char kDSOplTxFrameDropped[];
  static const char kDSOplRxFrameDropped[];
  static const char kDSOplRxFrameErrors[];
  static const char kDSOplNeighbourStateErrors[];
  static const char kDSOplNeighbourAfterNextStateErrors[];
  static const char kUSOplEncodingSupervisionErrors[];
  static const char kUSOplLifeSignErrors[];
  static const char kUSOplRxFlagErrors[];
  static const char kUSOplTxFrameDropped[];
  static const char kUSOplRxFrameDropped[];
  static const char kUSOplRxFrameErrors[];
  static const char kUSOplNeighbourStateErrors[];
  static const char kUSOplNeighbourAfterNextStateErrors[];
  static const char kDSOplErrorsMaxValue[];
  static const char kUSOplErrorsMaxValue[];
};
}  // namespace Communication::Protocols::FbisNetwork
