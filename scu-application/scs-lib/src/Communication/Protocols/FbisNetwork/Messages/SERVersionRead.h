/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <experimental/optional>
#include <memory>
#include "FbisMessageBase.h"

namespace Messages {
template<typename T>
class MessageTemplate;

class MessageItemProperty;
}  // namespace Messages
namespace Communication::Protocols::FbisNetwork {
class SERVersionRead : public FbisMessageBase {
 public:
  static std::experimental::optional<std::unique_ptr<Messages::MessageTemplate<Messages::MessageItemProperty>>>
  Deserialize(std::unique_ptr<std::vector<char>> input);

  static std::experimental::optional<std::unique_ptr<std::vector<char>>> Serialize(
    const Messages::MessageTemplate<Messages::MessageItemProperty> &message);

 private:
  static const char kPSVersion[];
  static const char kPSBranch[];
  static const char kPSBuildTimeStamp[];
  static const char kPSIsDirty[];
  static const char kPTVersion[];
  static const char kPTBranch[];
  static const char kPTBuildTimeStamp[];
  static const char kPTIsDirty[];
  static const char kPLBuildVersion[];
  static const char kPLLocalId[];
  static const char kPLBuildTimestamp[];
};
}  // namespace Communication::Protocols::FbisNetwork
