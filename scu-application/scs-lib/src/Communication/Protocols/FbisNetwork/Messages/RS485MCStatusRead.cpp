/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Types/Value.h"
#include "Types/ModuleType.h"
#include "Messages/Types/MessageItemState.h"
#include "Messages/Types/DataStatus.h"
#include "Messages/Visitor/VisitorPropertyGet.h"
#include "Messages/MessageItemProperty.h"
#include "Messages/MessageTemplate.h"
#include "RS485MCStatusRead.h"

namespace Communication::Protocols::FbisNetwork {

const char RS485MCStatusRead::kBoardName[] = "BoardName";
const char RS485MCStatusRead::kMezzanineCardPresent[] = "MezzanineCardPresent";
const char RS485MCStatusRead::kMezzanineCardEnable[] = "MezzanineCardEnable";
const char RS485MCStatusRead::kMezzanineCardReset[] = "MezzanineCardReset";
const char RS485MCStatusRead::kDriverEnable[] = "DriverEnable";
const char RS485MCStatusRead::kDriverReset[] = "DriverReset";
const char RS485MCStatusRead::kOperatingHours[] = "OperatingHours";
const char RS485MCStatusRead::kCollectionOverride[] = "CollectionOverride";
const char RS485MCStatusRead::kCollectionStatus1[] = "CollectionStatus1";
const char RS485MCStatusRead::kCollectionStatus2[] = "CollectionStatus2";
const char RS485MCStatusRead::kCollectionStatus3[] = "CollectionStatus3";
const char RS485MCStatusRead::kCollectionStatus4[] = "CollectionStatus4";
const char RS485MCStatusRead::kPort1Signal0Loss[] = "Port1Signal0Loss";
const char RS485MCStatusRead::kPort1Signal1Loss[] = "Port1Signal1Loss";
const char RS485MCStatusRead::kPort1Signal2Loss[] = "Port1Signal2Loss";
const char RS485MCStatusRead::kPort1Signal3Loss[] = "Port1Signal3Loss";
const char RS485MCStatusRead::kPort2Signal0Loss[] = "Port2Signal0Loss";
const char RS485MCStatusRead::kPort2Signal1Loss[] = "Port2Signal1Loss";
const char RS485MCStatusRead::kPort2Signal2Loss[] = "Port2Signal2Loss";
const char RS485MCStatusRead::kPort2Signal3Loss[] = "Port2Signal3Loss";
const char RS485MCStatusRead::kTemperatureEthernetSwitch[] = "EthernetSwitch";
const char RS485MCStatusRead::kMainCurrent[] = "MainCurrent";
const char RS485MCStatusRead::kInternalMainVoltage[] = "InternalMainVoltage";
const char RS485MCStatusRead::kInternalStandbyVoltage[] = "InternalStandbyVoltage";
const char RS485MCStatusRead::kInternal3V3Voltage[] = "Internal3V3Voltage";
const char RS485MCStatusRead::kInternal2V5Voltage[] = "Internal2V5Voltage";
const char RS485MCStatusRead::kInternal1V8Voltage[] = "Internal1V8Voltage";
const char RS485MCStatusRead::kInternal1V2Voltage[] = "Internal1V2Voltage";

const int32_t RS485MCStatusRead::kCurrentScale = 50;
const int32_t RS485MCStatusRead::kVoltageScale = 200;

std::experimental::optional<std::unique_ptr<Messages::MessageTemplate<Messages::MessageItemProperty>>>
RS485MCStatusRead::Deserialize(std::unique_ptr<std::vector<char>> input) {
  // Create message
  auto message = std::make_unique<Messages::MessageTemplate<Messages::MessageItemProperty>>();
  // Set request id
  message->SetRequestId(Communication::Types::RequestId::Enum::SCU_RS485MC_STATUS_READ);
  // Check message length
  auto length = (*input).size();
  if (length != 6) {  // outer frame 5 chars, inner frame 1 char
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_REQUEST_FORMAT);
    return std::move(message);
  }
  // Decode slot
  auto slot = static_cast<uint8_t>((*input)[kFirstParam]);
  if (slot < kSlotMin || slot > kSlotMax) {
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::SLOT_NOT_VALID);
    return std::move(message);
  }
  // Add mezzanine card board name
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kBoardName,
      std::string("MC").append(std::to_string(slot)).append(
        "/Management/EEProm/BoardName"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add mezzanine card present item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kMezzanineCardPresent,
      std::string("MC").append(std::to_string(slot)).append(
        "/CardDetectionItems/MezzanineCardPresent"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add mezzanine card power enable item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kMezzanineCardEnable,
      std::string("MC").append(std::to_string(slot)).append(
        "/CardDetectionItems/MezzanineCardEnable"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add mezzanine card reset item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kMezzanineCardReset,
      std::string("MC").append(std::to_string(slot)).append(
        "/CardDetectionItems/MezzanineCardReset"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add driver enable item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kDriverEnable,
      std::string("MC").append(std::to_string(slot)).append(
        "/CardDetectionItems/DriverEnable"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT_WRITE)));
  // Add driver reset item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kDriverReset,
      std::string("MC").append(std::to_string(slot)).append(
        "/CardDetectionItems/DriverReset"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT_WRITE)));
  // Add item operating hours
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kOperatingHours,
      std::string("MC").append(std::to_string(slot)).append(
        "/Management/EEProm/OperatingHours"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add MC Driver input signal override collection
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kCollectionOverride,
      std::string("MC").append(std::to_string(slot)).append(
        "/Application/MCDriver/Status/").append(kCollectionOverride),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kCollectionStatus1,
      std::string("MC").append(std::to_string(slot)).append(
        "/Application/MCDriver/Status/").append(kCollectionStatus1),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kCollectionStatus2,
      std::string("MC").append(std::to_string(slot)).append(
        "/Application/MCDriver/Status/").append(kCollectionStatus2),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kCollectionStatus3,
      std::string("MC").append(std::to_string(slot)).append(
        "/Application/MCDriver/Status/").append(kCollectionStatus3),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kCollectionStatus4,
      std::string("MC").append(std::to_string(slot)).append(
        "/Application/MCDriver/Status/").append(kCollectionStatus4),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add Signal Loss Port1_Signal0 item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kPort1Signal0Loss,
      std::string("MC").append(std::to_string(slot)).append(
        "/Application/Gpios/Port1Signal0Loss"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add Signal Loss Port1_Signal1 item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kPort1Signal1Loss,
      std::string("MC").append(std::to_string(slot)).append(
        "/Application/Gpios/Port1Signal1Loss"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add Signal Loss Port1_Signal2 item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kPort1Signal2Loss,
      std::string("MC").append(std::to_string(slot)).append(
        "/Application/Gpios/Port1Signal2Loss"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add Signal Loss Port1_Signal3 item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kPort1Signal3Loss,
      std::string("MC").append(std::to_string(slot)).append(
        "/Application/Gpios/Port1Signal3Loss"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add Signal Loss Port2_Signal0 item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kPort2Signal0Loss,
      std::string("MC").append(std::to_string(slot)).append(
        "/Application/Gpios/Port2Signal0Loss"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add Signal Loss Port2_Signal1 item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kPort2Signal1Loss,
      std::string("MC").append(std::to_string(slot)).append(
        "/Application/Gpios/Port2Signal1Loss"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add Signal Loss Port2_Signal2 item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kPort2Signal2Loss,
      std::string("MC").append(std::to_string(slot)).append(
        "/Application/Gpios/Port2Signal2Loss"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add Signal Loss Port2_Signal3 item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kPort2Signal3Loss,
      std::string("MC").append(std::to_string(slot)).append(
        "/Application/Gpios/Port2Signal3Loss"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add item Temperature Sensor close to the Ethernet Switch
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kTemperatureEthernetSwitch,
      std::string("MC").append(std::to_string(slot)).append(
        "/Application/Temperature/").append(kTemperatureEthernetSwitch),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add item internal main current
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kMainCurrent,
      std::string("MC").append(std::to_string(slot)).append(
        "/Application/HotSwapController/MainCurrent"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add item internal main voltage
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kInternalMainVoltage,
      std::string("MC").append(std::to_string(slot)).append(
        "/Application/HotSwapController/MainVoltage"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add item internal standby voltage
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kInternalStandbyVoltage,
      std::string("MC").append(std::to_string(slot)).append(
        "/Application/HotSwapController/StandbyVoltage"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add item internal 3V3 voltage
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kInternal3V3Voltage,
      std::string("MC").append(std::to_string(slot)).append(
        "/Application/VoltageMonitor/Voltage3V3"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add item internal 2V5 voltage
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kInternal2V5Voltage,
      std::string("MC").append(std::to_string(slot)).append(
        "/Application/VoltageMonitor/Voltage2V5"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add item internal 1V8 voltage
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kInternal1V8Voltage,
      std::string("MC").append(std::to_string(slot)).append(
        "/Application/VoltageMonitor/Voltage1V8"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add item internal 1V2 voltage
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kInternal1V2Voltage,
      std::string("MC").append(std::to_string(slot)).append(
        "/Application/VoltageMonitor/Voltage1V2"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Return message
  return std::move(message);
}

std::experimental::optional<std::unique_ptr<std::vector<char>>> RS485MCStatusRead::Serialize(
  const Messages::MessageTemplate<Messages::MessageItemProperty> &message) {
  // Prepare out vector
  auto out = std::make_unique<std::vector<char>>();
  auto dataStatus = message.GetDataStatus();
  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    for (const auto &item : message.GetMessageItems()) {
      if (item->ItemState()->GetValue() != Messages::Types::MessageItemState::Enum::OK) {
        dataStatus = Messages::Types::DataStatus::Enum::INFORMATION_NOT_AVAILABLE;
        break;
      }
    }
  }
  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    // ======== Check MC ========
    if (const auto &item = message.GetMessageItem(kMezzanineCardPresent)) {
      if (!item.value()->GetValue<bool>().value()) {
        dataStatus = Messages::Types::DataStatus::Enum::MC_NOT_PRESENT;
      }
    }
  }
  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    if (const auto &item = message.GetMessageItem(kBoardName)) {
      if (const auto &retVal = item.value()->GetValue<::Types::ModuleType>()) {
        if (retVal.value().GetEnum() != ::Types::ModuleType::Enum::SCU_RS485MC) {
          dataStatus = Messages::Types::DataStatus::Enum::WRONG_MEZZANINE_CARD;
        }
      } else {
        dataStatus = Messages::Types::DataStatus::Enum::INFORMATION_NOT_AVAILABLE;
      }
    }
  }

  // ======== Serialize "Data Status" ========
  FbisMessageBase::Serialize(out.get(), static_cast<uint8_t>(dataStatus));

  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    // ======== Serialize "Platform Control Signal Status" ========
    // Parse platform control signal status
    uint8_t platformControlSignalStatus = 0x00;
    // Serialize mezzanine card present item
    if (const auto &item = message.GetMessageItem(kMezzanineCardPresent)) {
      if (const auto &retVal = item.value()->GetValue<bool>()) {
        if (retVal.value()) {
          platformControlSignalStatus |= 0x01;
        }
      }
    }
    // Serialize mezzanine card power enable item
    if (const auto &item = message.GetMessageItem(kMezzanineCardEnable)) {
      if (const auto &retVal = item.value()->GetValue<bool>()) {
        if (retVal.value()) {
          platformControlSignalStatus |= 0x02;
        }
      }
    }
    // Serialize mezzanine card reset item
    if (const auto &item = message.GetMessageItem(kMezzanineCardReset)) {
      if (const auto &retVal = item.value()->GetValue<bool>()) {
        if (retVal.value()) {
          platformControlSignalStatus |= 0x04;
        }
      }
    }
    FbisMessageBase::Serialize(out.get(), platformControlSignalStatus);
    // ======== Serialize "MC_Driver Status" ========
    // Parse driver status
    uint8_t driverStatus = 0x00;
    // Serialize driver enable item
    if (const auto &item = message.GetMessageItem(kDriverEnable)) {
      if (const auto &retVal = item.value()->GetValue<bool>()) {
        if (retVal.value()) {
          driverStatus |= 0x01;
        }
      }
    }
    // Serialize driver reset item
    if (const auto &item = message.GetMessageItem(kDriverReset)) {
      if (const auto &retVal = item.value()->GetValue<bool>()) {
        if (!retVal.value()) {
          driverStatus |= 0x02;
        }
      }
    }
    FbisMessageBase::Serialize(out.get(), driverStatus);
    // ======== Serialize "MC Operating Hours" ========
    if (const auto &item = message.GetMessageItem(kOperatingHours)) {
      if (const auto &retVal = item.value()->GetValue<std::chrono::minutes>()) {
        auto hours = static_cast<uint32_t>(retVal.value().count() / 15);
        FbisMessageBase::Serialize(out.get(), hours);
      }
    }
    // ======== Serialize "Overriding of RS485MC Input Signals" ========
    // Serialize mezzanine card Override signals
    if (const auto &item = message.GetMessageItem(kCollectionOverride)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        FbisMessageBase::Serialize(out.get(), retVal.value());
      }
    }
    // ======== Serialize "Details" ========
    // Serialize MC Driver status signals
    if (const auto &item = message.GetMessageItem(kCollectionStatus4)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        FbisMessageBase::Serialize(out.get(), retVal.value());
      }
    }
    if (const auto &item = message.GetMessageItem(kCollectionStatus3)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        FbisMessageBase::Serialize(out.get(), retVal.value());
      }
    }
    if (const auto &item = message.GetMessageItem(kCollectionStatus2)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        FbisMessageBase::Serialize(out.get(), retVal.value());
      }
    }
    if (const auto &item = message.GetMessageItem(kCollectionStatus1)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        FbisMessageBase::Serialize(out.get(), retVal.value());
      }
    }
    // ======== Serialize "Signal Loss Detection" ========
    uint8_t signalLossDetection = 0x00;
    if (const auto &item = message.GetMessageItem(kPort1Signal0Loss)) {
      if (const auto &retVal = item.value()->GetValue<bool>()) {
        if (retVal.value()) {
          signalLossDetection |= 0x01;
        }
      }
    }
    if (const auto &item = message.GetMessageItem(kPort1Signal1Loss)) {
      if (const auto &retVal = item.value()->GetValue<bool>()) {
        if (retVal.value()) {
          signalLossDetection |= 0x02;
        }
      }
    }
    if (const auto &item = message.GetMessageItem(kPort1Signal2Loss)) {
      if (const auto &retVal = item.value()->GetValue<bool>()) {
        if (retVal.value()) {
          signalLossDetection |= 0x04;
        }
      }
    }
    if (const auto &item = message.GetMessageItem(kPort1Signal3Loss)) {
      if (const auto &retVal = item.value()->GetValue<bool>()) {
        if (retVal.value()) {
          signalLossDetection |= 0x08;
        }
      }
    }
    if (const auto &item = message.GetMessageItem(kPort2Signal0Loss)) {
      if (const auto &retVal = item.value()->GetValue<bool>()) {
        if (retVal.value()) {
          signalLossDetection |= 0x10;
        }
      }
    }
    if (const auto &item = message.GetMessageItem(kPort2Signal1Loss)) {
      if (const auto &retVal = item.value()->GetValue<bool>()) {
        if (retVal.value()) {
          signalLossDetection |= 0x20;
        }
      }
    }
    if (const auto &item = message.GetMessageItem(kPort2Signal2Loss)) {
      if (const auto &retVal = item.value()->GetValue<bool>()) {
        if (retVal.value()) {
          signalLossDetection |= 0x40;
        }
      }
    }
    if (const auto &item = message.GetMessageItem(kPort2Signal3Loss)) {
      if (const auto &retVal = item.value()->GetValue<bool>()) {
        if (retVal.value()) {
          signalLossDetection |= 0x80;
        }
      }
    }
    FbisMessageBase::Serialize(out.get(), signalLossDetection);
    // ======== Serialize "Temperature at Ethernet Switch" ========
    if (const auto &item = message.GetMessageItem(kTemperatureEthernetSwitch)) {
      if (const auto &retVal = item.value()->GetValue<::Types::Value<int32_t>>()) {
        auto temperature = static_cast<int32_t>(retVal.value().GetValue());
        FbisMessageBase::Serialize(out.get(), temperature);
      } else {
        FbisMessageBase::Serialize(out.get(), static_cast<int32_t>(0x80000001));
      }
    }
    // ======== Serialize "MAIN Supply Current" ========
    if (const auto &item = message.GetMessageItem(kMainCurrent)) {
      if (const auto &retVal = item.value()->GetValue<::Types::Value<int32_t>>()) {
        auto current = static_cast<int8_t>(retVal.value().GetValue() / kCurrentScale);
        FbisMessageBase::Serialize(out.get(), current);
      }
    }
    // ======== Serialize "Internal MAIN Supply Voltage" ========
    if (const auto &item = message.GetMessageItem(kInternalMainVoltage)) {
      if (const auto &retVal = item.value()->GetValue<::Types::Value<int32_t>>()) {
        auto voltage = static_cast<int8_t>(retVal.value().GetValue() / kVoltageScale);
        FbisMessageBase::Serialize(out.get(), voltage);
      }
    }
    // ======== Serialize "Internal STANDBY Supply Voltage" ========
    if (const auto &item = message.GetMessageItem(kInternalStandbyVoltage)) {
      if (const auto &retVal = item.value()->GetValue<::Types::Value<int32_t>>()) {
        auto voltage = static_cast<int8_t>(retVal.value().GetValue() / kVoltageScale);
        FbisMessageBase::Serialize(out.get(), voltage);
      }
    }
    // ======== Serialize "Internal 3V3 Supply Voltage" ========
    if (const auto &item = message.GetMessageItem(kInternal3V3Voltage)) {
      if (const auto &retVal = item.value()->GetValue<::Types::Value<int32_t>>()) {
        auto voltage = static_cast<int8_t>(retVal.value().GetValue() / kVoltageScale);
        FbisMessageBase::Serialize(out.get(), voltage);
      }
    }
    // ======== Serialize "Internal 2V5 Supply Voltage" ========
    if (const auto &item = message.GetMessageItem(kInternal2V5Voltage)) {
      if (const auto &retVal = item.value()->GetValue<::Types::Value<int32_t>>()) {
        auto voltage = static_cast<int8_t>(retVal.value().GetValue() / kVoltageScale);
        FbisMessageBase::Serialize(out.get(), voltage);
      }
    }
    // ======== Serialize "Internal 1V8 Supply Voltage" ========
    if (const auto &item = message.GetMessageItem(kInternal1V8Voltage)) {
      if (const auto &retVal = item.value()->GetValue<::Types::Value<int32_t>>()) {
        auto voltage = static_cast<int8_t>(retVal.value().GetValue() / kVoltageScale);
        FbisMessageBase::Serialize(out.get(), voltage);
      }
    }
    // ======== Serialize "Internal 1V2 Supply Voltage" ========
    if (const auto &item = message.GetMessageItem(kInternal1V2Voltage)) {
      if (const auto &retVal = item.value()->GetValue<::Types::Value<int32_t>>()) {
        auto voltage = static_cast<int8_t>(retVal.value().GetValue() / kVoltageScale);
        FbisMessageBase::Serialize(out.get(), voltage);
      }
    }
  }
  return std::move(out);
}
}  // namespace Communication::Protocols::FbisNetwork
