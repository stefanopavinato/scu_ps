/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Types/Value.h"
#include "Types/ModuleType.h"
#include "Messages/Types/MessageItemState.h"
#include "Messages/Types/DataStatus.h"
#include "Messages/Visitor/VisitorPropertyGet.h"
#include "Messages/MessageItemProperty.h"
#include "Messages/MessageTemplate.h"
#include "SMODInfoRead.h"

namespace Communication::Protocols::FbisNetwork {

const char SMODInfoRead::kMezzanineCardPresent[] = "MezzanineCardPresent";
const char SMODInfoRead::kManufacturer[] = "Manufacturer";
const char SMODInfoRead::kBoardName[] = "BoardNameString";
const char SMODInfoRead::kRevision[] = "BoardRevision";
const char SMODInfoRead::kSerialNumber[] = "SerialNumber";
const char SMODInfoRead::kUniqueID[] = "UUID";

std::experimental::optional<std::unique_ptr<Messages::MessageTemplate<Messages::MessageItemProperty>>>
SMODInfoRead::Deserialize(std::unique_ptr<std::vector<char>> input) {
  // Create message
  auto message = std::make_unique<Messages::MessageTemplate<Messages::MessageItemProperty>>();
  // Set request id
  message->SetRequestId(Communication::Types::RequestId::Enum::SCU_SMOD_INFO_READ);
  // Check message length
  auto length = (*input).size();
  if (length != 6) {  // outer frame 5 chars, inner frame 1 char
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_REQUEST_FORMAT);
    return std::move(message);
  }
  // Decode SMOD
  auto smod = static_cast<uint8_t>((*input)[kFirstParam]);
  std::string itemPath = "";
  switch (smod) {
    case 1:  // SCU Power Supply slot 1
      message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::SLOT_NOT_VALID);
      return std::move(message);
      break;
    case 2:  // SCU Power Supply slot 2
      message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::SLOT_NOT_VALID);
      return std::move(message);
      break;
    case 10:  // Serializer
      itemPath = std::string("Serializer").append("/Serializer/EEProm/");
      message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::OK);
      break;
    case 20:  // MC Slot 1
    case 21:  // MC Slot 2
    case 22:  // MC Slot 3
    case 23:  // MC Slot 4
    case 24:  // MC Slot 5
    case 25:  // MC Slot 6
    case 26:  // MC Slot 7
    case 27:  // MC Slot 8
    case 28:  // MC Slot 9
    case 29:  // MC Slot 10
    case 30:  // MC Slot 11
    case 31:  // MC Slot 12
      // Add mezzanine card present item
      message->AddMessageItem(
        std::make_unique<Messages::MessageItemProperty>(
          kMezzanineCardPresent,
          std::string("MC").append(std::to_string(smod - 19)).append("/CardDetectionItems/MezzanineCardPresent"),
          std::make_unique<Messages::Visitor::VisitorPropertyGet>(
            Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
      itemPath = std::string("MC").append(std::to_string(smod - 19)).append("/Management/EEProm/");
      message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::OK);
      break;
    case 40:  // SCU Fan Unit
      itemPath = std::string("FanUnit").append("/FanUnit/EEProm/");
      message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::OK);
      break;
    default:
      message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::SLOT_NOT_VALID);
      return std::move(message);
      break;
  }
  // Add SMOD manufacturer item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kManufacturer,
      std::string(itemPath).append(kManufacturer),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add SMOD card name
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kBoardName,
      std::string(itemPath).append(kBoardName),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add SMOD card board revision number
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kRevision,
      std::string(itemPath).append(kRevision),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add SMOD card serial number
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kSerialNumber,
      std::string(itemPath).append(kSerialNumber),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add SMOD card unique ID
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kUniqueID,
      std::string(itemPath).append(kUniqueID),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Return message
  return std::move(message);
}

std::experimental::optional<std::unique_ptr<std::vector<char>>> SMODInfoRead::Serialize(
  const Messages::MessageTemplate<Messages::MessageItemProperty> &message) {
  // Prepare out vector
  auto out = std::make_unique<std::vector<char>>();
  auto dataStatus = message.GetDataStatus();
  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    for (const auto &item : message.GetMessageItems()) {
      if (item->ItemState()->GetValue() != Messages::Types::MessageItemState::Enum::OK) {
        dataStatus = Messages::Types::DataStatus::Enum::INFORMATION_NOT_AVAILABLE;
        break;
      }
    }
  }
  if ((dataStatus == Messages::Types::DataStatus::Enum::OK) ||
      (dataStatus == Messages::Types::DataStatus::Enum::INFORMATION_NOT_AVAILABLE)) {
    // ======== Check MC ========
    if (const auto &item = message.GetMessageItem(kMezzanineCardPresent)) {
      if (item) {  // true (existing), if mezzanine card read
        if (!item.value()->GetValue<bool>().value()) {
          dataStatus = Messages::Types::DataStatus::Enum::MC_NOT_PRESENT;
        }
      }
    }
  }
  // ======== Serialize "Data Status" ========
  out->push_back(static_cast<char>(dataStatus));
  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    // Serialize SMOD card manufacturer
    if (const auto &item = message.GetMessageItem(kManufacturer)) {
      if (const auto &retVal = item.value()->GetValue<std::string>()) {
        std::string strValue = retVal.value();
        for (char &c : strValue) {
          out->push_back(c);
        }
        for (std::string::size_type i = 0; i < (32 - strValue.length()); i++) {
          out->push_back(0);
        }
      }
    }
    // Serialize SMOD card name
    if (const auto &item = message.GetMessageItem(kBoardName)) {
      if (const auto &retVal = item.value()->GetValue<std::string>()) {
        std::string strValue = retVal.value();
        for (char &c : strValue) {
          out->push_back(c);
        }
        for (std::string::size_type i = 0; i < (16 - strValue.length()); i++) {
          out->push_back(0);
        }
      }
    }
    // Serialize SMOD card revision
    if (const auto &item = message.GetMessageItem(kRevision)) {
      if (const auto &retVal = item.value()->GetValue<std::string>()) {
        std::string strValue = retVal.value();
        for (char &c : strValue) {
          out->push_back(c);
        }
        for (std::string::size_type i = 0; i < (16 - strValue.length()); i++) {
          out->push_back(0);
        }
      }
    }
    // Serialize SMOD card serial number
    if (const auto &item = message.GetMessageItem(kSerialNumber)) {
      if (const auto &retVal = item.value()->GetValue<std::string>()) {
        std::string strValue = retVal.value();
        for (char &c : strValue) {
          out->push_back(c);
        }
        for (std::string::size_type i = 0; i < (16 - strValue.length()); i++) {
          out->push_back(0);
        }
      }
    }
    // Serialize SMOD card unique id
    if (const auto &item = message.GetMessageItem(kUniqueID)) {
      if (const auto &retVal = item.value()->GetValue<std::string>()) {
        std::string strValue = retVal.value();
        for (char &c : strValue) {
          out->push_back(c);
        }
        for (std::string::size_type i = 0; i < (4 - strValue.length()); i++) {
          out->push_back(0);
        }
      }
    }
  }
  return std::move(out);
}

}  // namespace Communication::Protocols::FbisNetwork
