/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Types/Value.h"
#include "Messages/Types/MessageItemState.h"
#include "Messages/Types/DataStatus.h"
#include "Messages/Visitor/VisitorPropertyGet.h"
#include "Messages/MessageItemProperty.h"
#include "Messages/MessageTemplate.h"
#include "FbisMessageBase.h"
#include "FUTemperatureRead.h"

namespace Communication::Protocols::FbisNetwork {

const char FUTemperatureRead::kFanUnitControllerATemperature[] = "FanA/Temperature";
// const char FUTemperatureRead::kFanUnitControllerMTemperature[] = "FanM/Temperature";
const char FUTemperatureRead::kFanUnitControllerBTemperature[] = "FanB/Temperature";

std::experimental::optional<std::unique_ptr<Messages::MessageTemplate<Messages::MessageItemProperty>>>
FUTemperatureRead::Deserialize(std::unique_ptr<std::vector<char>> input) {
  // Create message
  auto message = std::make_unique<Messages::MessageTemplate<Messages::MessageItemProperty>>();
  auto length = (*input).size();
  if (length != 5) {  // outer frame 5 chars, inner frame 0 char
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_REQUEST_FORMAT);
    return std::move(message);
  }
  // Set request id
  message->SetRequestId(Communication::Types::RequestId::Enum::SCU_FAN_UNIT_TEMPERATURE_READ);
  // Add Fan Unit Controller A temperature item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kFanUnitControllerATemperature,
      std::string("FanUnit/FanUnit/").append(kFanUnitControllerATemperature),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
//  // Add Fan Unit Controller M temperature item
//  message->AddMessageItem(
//    std::make_unique<Messages::MessageItemProperty>(
//      kFanUnitControllerMTemperature,
//      std::string("FanUnit/FanUnit/").append(kFanUnitControllerMTemperature),
//      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
//        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add Fan Unit Controller B temperature item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kFanUnitControllerBTemperature,
      std::string("FanUnit/FanUnit/").append(kFanUnitControllerATemperature),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Return message
  (void) input;
  return std::move(message);
}

std::experimental::optional<std::unique_ptr<std::vector<char>>> FUTemperatureRead::Serialize(
  const Messages::MessageTemplate<Messages::MessageItemProperty> &message) {
  // Prepare out vector
  auto out = std::make_unique<std::vector<char>>();
  auto dataStatus = message.GetDataStatus();
  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    for (const auto &item : message.GetMessageItems()) {
      if (item->ItemState()->GetValue() != Messages::Types::MessageItemState::Enum::OK) {
        dataStatus = Messages::Types::DataStatus::Enum::INFORMATION_NOT_AVAILABLE;
        break;
      }
    }
  }
  // ======== Serialize "Data Status" ========
  FbisMessageBase::Serialize(out.get(), static_cast<uint8_t>(dataStatus));

  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    // ======== Serialize "SCU Fan Unit Temperatures" ========
    // Serialize Fan Unit Controller A temperature  item
    if (const auto &item = message.GetMessageItem(kFanUnitControllerATemperature)) {
      if (const auto &retVal = item.value()->GetValue<::Types::Value<int32_t>>()) {
        auto temperature = static_cast<int32_t>(retVal.value().GetValue());
        FbisMessageBase::Serialize(out.get(), temperature);
      }
    }
    // Serialize Fan Unit Controller M temperature  item
    FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(0x00000000));
//    if (const auto &item = message.GetMessageItem(kFanUnitControllerMTemperature)) {
//      if (const auto &retVal = item.value()->GetValue<::Types::Value<int32_t>>()) {
//        auto temperature = static_cast<int32_t>(retVal.value().GetValue());
//        FbisMessageBase::Serialize(out.get(), temperature);
//      }
//    }
    // Serialize Fan Unit Controller B temperature  item
    if (const auto &item = message.GetMessageItem(kFanUnitControllerBTemperature)) {
      if (const auto &retVal = item.value()->GetValue<::Types::Value<int32_t>>()) {
        auto temperature = static_cast<int32_t>(retVal.value().GetValue());
        FbisMessageBase::Serialize(out.get(), temperature);
      }
    }
  }
  return std::move(out);
}

}  // namespace Communication::Protocols::FbisNetwork
