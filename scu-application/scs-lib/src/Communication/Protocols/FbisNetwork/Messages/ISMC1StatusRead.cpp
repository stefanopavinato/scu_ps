/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Types/Value.h"
#include "Types/ModuleType.h"
#include "Messages/Types/MessageItemState.h"
#include "Messages/Types/DataStatus.h"
#include "Messages/Visitor/VisitorPropertyGet.h"
#include "Messages/MessageItemProperty.h"
#include "Messages/MessageTemplate.h"
#include "ISMC1StatusRead.h"

namespace Communication::Protocols::FbisNetwork {

const char ISMC1StatusRead::kBoardName[] = "BoardName";
const char ISMC1StatusRead::kMezzanineCardPresent[] = "MezzanineCardPresent";
const char ISMC1StatusRead::kMezzanineCardEnable[] = "MezzanineCardEnable";
const char ISMC1StatusRead::kMezzanineCardReset[] = "MezzanineCardReset";
const char ISMC1StatusRead::kDriverEnable[] = "DriverEnable";
const char ISMC1StatusRead::kDriverReset[] = "DriverReset";
const char ISMC1StatusRead::kOperatingHours[] = "OperatingHours";
const char ISMC1StatusRead::kMCDriverInputSignalOverride[] = "MCDriverInputSignalOverride";
const char ISMC1StatusRead::kMCDriverOutputSignalOverride[] = "MCDriverOutputSignalOverride";
const char ISMC1StatusRead::kMCDriverStatus1[] = "MCDriverStatus1";
const char ISMC1StatusRead::kMCDriverStatus2[] = "MCDriverStatus2";
const char ISMC1StatusRead::kTriggerInputRSSI[] = "TriggerInputRSSI";
const char ISMC1StatusRead::kMagnetronPSFeedbackRSSI[] = "MagnetronPSFeedbackRSSI";
const char ISMC1StatusRead::kRFFeedbackRSSI[] = "RFFeedbackRSSI";
const char ISMC1StatusRead::kMainCurrent[] = "MainCurrent";
const char ISMC1StatusRead::kInternalMainVoltage[] = "InternalMainVoltage";
const char ISMC1StatusRead::kInternalStandbyVoltage[] = "InternalStandbyVoltage";
const char ISMC1StatusRead::kInternal3V3Voltage[] = "Internal3V3Voltage";
const char ISMC1StatusRead::kInternal1V8Voltage[] = "Internal1V8Voltage";

const int32_t ISMC1StatusRead::kCurrentScale = 50;
const int32_t ISMC1StatusRead::kVoltageScale = 200;

std::experimental::optional<std::unique_ptr<Messages::MessageTemplate<Messages::MessageItemProperty>>>
ISMC1StatusRead::Deserialize(std::unique_ptr<std::vector<char>> input) {
  // Create message
  auto message = std::make_unique<Messages::MessageTemplate<Messages::MessageItemProperty>>();
  // Set request id
  message->SetRequestId(Communication::Types::RequestId::Enum::SCU_ISMC1_STATUS_READ);
  // Check message length
  auto length = (*input).size();
  if (length != 6) {  // outer frame 5 chars, inner frame 1 char
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_REQUEST_FORMAT);
    return std::move(message);
  }
  // Decode slot
  auto slot = static_cast<uint8_t>((*input)[kFirstParam]);
  if (slot < kSlotMin || slot > kSlotMax) {
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::SLOT_NOT_VALID);
    return std::move(message);
  }
  // Add mezzanine card board name
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kBoardName,
      std::string("MC").append(std::to_string(slot)).append("/Management/EEProm/BoardName"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add mezzanine card present item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kMezzanineCardPresent,
      std::string("MC").append(std::to_string(slot)).append("/CardDetectionItems/MezzanineCardPresent"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add mezzanine card power enable item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kMezzanineCardEnable,
      std::string("MC").append(std::to_string(slot)).append("/CardDetectionItems/MezzanineCardEnable"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add mezzanine card reset item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kMezzanineCardReset,
      std::string("MC").append(std::to_string(slot)).append("/CardDetectionItems/MezzanineCardReset"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add driver enable item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kDriverEnable,
      std::string("MC").append(std::to_string(slot)).append("/CardDetectionItems/DriverEnable"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT_WRITE)));
  // Add driver reset item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kDriverReset,
      std::string("MC").append(std::to_string(slot)).append("/CardDetectionItems/DriverReset"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT_WRITE)));
  // Add item operating hours
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kOperatingHours,
      std::string("MC").append(std::to_string(slot)).append("/Management/EEProm/OperatingHours"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add MC Driver Input Signal override collection
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kMCDriverInputSignalOverride,
      std::string("MC").append(std::to_string(slot))
        .append("/Application/MCDriver/ISMC1Override/InputSignalCollection"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add MC Driver Output Signal override collection
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kMCDriverOutputSignalOverride,
      std::string("MC").append(std::to_string(slot))
        .append("/Application/MCDriver/ISMC1Override/OutputSignalCollection"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add MC Driver status signals
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kMCDriverStatus1,
      std::string("MC").append(std::to_string(slot)).append("/Application/MCDriver/Status/Collection1"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kMCDriverStatus2,
      std::string("MC").append(std::to_string(slot)).append("/Application/MCDriver/Status/Collection2"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add TriggerInputRSSI
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kTriggerInputRSSI,
      std::string("MC").append(std::to_string(slot))
        .append("/Application/ReceiverStrengthIndication/TriggerInputRSSI"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add MagnetronPSFeedbackRSSI
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kMagnetronPSFeedbackRSSI,
      std::string("MC").append(std::to_string(slot))
        .append("/Application/ReceiverStrengthIndication/MagnetronPSFeedbackRSSI"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add item MagnetronPSFeedbackRSSI
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kRFFeedbackRSSI,
      std::string("MC").append(std::to_string(slot))
        .append("/Application/ReceiverStrengthIndication/RFFeedbackRSSI"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add item main current
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kMainCurrent,
      std::string("MC").append(std::to_string(slot)).append("/Application/HotSwapController/MainCurrent"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add item internal main voltage
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kInternalMainVoltage,
      std::string("MC").append(std::to_string(slot)).append("/Application/HotSwapController/MainVoltage"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add item internal standby voltage
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kInternalStandbyVoltage,
      std::string("MC").append(std::to_string(slot)).append("/Application/HotSwapController/StandbyVoltage"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add item internal 3V3 voltage
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kInternal3V3Voltage,
      std::string("MC").append(std::to_string(slot)).append("/Application/VoltageMonitor/Voltage3V3"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add item internal 1V8 voltage
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kInternal1V8Voltage,
      std::string("MC").append(std::to_string(slot)).append("/Application/VoltageMonitor/Voltage1V8"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Return message
  return std::move(message);
}

std::experimental::optional<std::unique_ptr<std::vector<char>>> ISMC1StatusRead::Serialize(
  const Messages::MessageTemplate<Messages::MessageItemProperty> &message) {
  // Prepare out vector
  auto out = std::make_unique<std::vector<char>>();
  auto dataStatus = message.GetDataStatus();
  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    for (const auto &item : message.GetMessageItems()) {
      if (item->ItemState()->GetValue() != Messages::Types::MessageItemState::Enum::OK) {
        dataStatus = Messages::Types::DataStatus::Enum::INFORMATION_NOT_AVAILABLE;
        break;
      }
    }
  }
  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    // ======== Check MC ========
    if (const auto &item = message.GetMessageItem(kMezzanineCardPresent)) {
      if (!item.value()->GetValue<bool>().value()) {
        dataStatus = Messages::Types::DataStatus::Enum::MC_NOT_PRESENT;
      }
    }
  }
  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    if (const auto &item = message.GetMessageItem(kBoardName)) {
      if (const auto &retVal = item.value()->GetValue<::Types::ModuleType>()) {
        if (retVal.value().GetEnum() != ::Types::ModuleType::Enum::SCU_ISMC1) {
          dataStatus = Messages::Types::DataStatus::Enum::WRONG_MEZZANINE_CARD;
        }
      } else {
        dataStatus = Messages::Types::DataStatus::Enum::INFORMATION_NOT_AVAILABLE;
      }
    }
  }

  // ======== Serialize "Data Status" ========
  FbisMessageBase::Serialize(out.get(), static_cast<uint8_t>(dataStatus));

  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    // ======== Serialize "Platform Control Signal Status" ========
    // Parse platform control signal status
    uint8_t platformControlSignalStatus = 0x00;
    // Serialize mezzanine card present item
    if (const auto &item = message.GetMessageItem(kMezzanineCardPresent)) {
      if (const auto &retVal = item.value()->GetValue<bool>()) {
        if (retVal.value()) {
          platformControlSignalStatus |= 0x01;
        }
      }
    }
    // Serialize mezzanine card power enable item
    if (const auto &item = message.GetMessageItem(kMezzanineCardEnable)) {
      if (const auto &retVal = item.value()->GetValue<bool>()) {
        if (retVal.value()) {
          platformControlSignalStatus |= 0x02;
        }
      }
    }
    // Serialize mezzanine card reset item
    if (const auto &item = message.GetMessageItem(kMezzanineCardReset)) {
      if (const auto &retVal = item.value()->GetValue<bool>()) {
        if (retVal.value()) {
          platformControlSignalStatus |= 0x04;
        }
      }
    }
    FbisMessageBase::Serialize(out.get(), platformControlSignalStatus);
    // ======== Serialize "MC_Driver Status" ========
    // Parse driver status
    uint8_t driverStatus = 0x00;
    // Serialize driver enable item
    if (const auto &item = message.GetMessageItem(kDriverEnable)) {
      if (const auto &retVal = item.value()->GetValue<bool>()) {
        if (retVal.value()) {
          driverStatus |= 0x01;
        }
      }
    }
    // Serialize driver reset item
    if (const auto &item = message.GetMessageItem(kDriverReset)) {
      if (const auto &retVal = item.value()->GetValue<bool>()) {
        if (!retVal.value()) {  // MCDriverReset is inverted in FBIS-Network-Protocol
          driverStatus |= 0x02;
        }
      }
    }
    FbisMessageBase::Serialize(out.get(), driverStatus);
    // ======== Serialize "MC Operating Hours" ========
    if (const auto &item = message.GetMessageItem(kOperatingHours)) {
      if (const auto &retVal = item.value()->GetValue<std::chrono::minutes>()) {
        auto operationTime = static_cast<uint32_t>(retVal.value().count() / 15);  // Cast to 15 min! (add const)
        FbisMessageBase::Serialize(out.get(), operationTime);
      }
    }
    // ======== Serialize "Overriding of ISMC1 Input Signals" ========
    if (const auto &item = message.GetMessageItem(kMCDriverInputSignalOverride)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        auto inputSignalOverride = static_cast<int32_t>(retVal.value());
        FbisMessageBase::Serialize(out.get(), inputSignalOverride);
      }
    }
    // ======== Serialize "Overriding of ISMC1 Output Signals" ========
    if (const auto &item = message.GetMessageItem(kMCDriverOutputSignalOverride)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        auto outputSignalOverride = static_cast<int32_t>(retVal.value());
        FbisMessageBase::Serialize(out.get(), outputSignalOverride);
      }
    }
    // ======== Serialize "Details" ========
    // Serialize MC Driver status signals, second first
    if (const auto &item = message.GetMessageItem(kMCDriverStatus2)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        auto collection2 = static_cast<int32_t>(retVal.value());
        FbisMessageBase::Serialize(out.get(), collection2);
      }
    }
    if (const auto &item = message.GetMessageItem(kMCDriverStatus1)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        auto collection1 = static_cast<int32_t>(retVal.value());
        FbisMessageBase::Serialize(out.get(), collection1);
      }
    }
    // ======== Serialize "Beam Permit Signal Current" ========
    if (const auto &item = message.GetMessageItem(kTriggerInputRSSI)) {
      if (const auto &retVal = item.value()->GetValue<::Types::Value<int32_t>>()) {
        auto current = static_cast<int16_t>(retVal.value().GetValue() / 10);  // change unit from uA to 10uA
        FbisMessageBase::Serialize(out.get(), current);
      }
    }
    // ======== Serialize "Ready Signal Current" ========
    if (const auto &item = message.GetMessageItem(kMagnetronPSFeedbackRSSI)) {
      if (const auto &retVal = item.value()->GetValue<::Types::Value<int32_t>>()) {
        auto current = static_cast<int16_t>(retVal.value().GetValue() / 10);  // change unit from uA to 10uA
        FbisMessageBase::Serialize(out.get(), current);
      }
    }
    // ======== Serialize "Beam Permit and Ready Signals Supply Voltage" ========
    if (const auto &item = message.GetMessageItem(kRFFeedbackRSSI)) {
      if (const auto &retVal = item.value()->GetValue<::Types::Value<int32_t>>()) {
        auto voltage = static_cast<int16_t>(retVal.value().GetValue());
        FbisMessageBase::Serialize(out.get(), voltage);
      }
    }
    // ======== Serialize "MAIN Supply Current" ========
    if (const auto &item = message.GetMessageItem(kMainCurrent)) {
      if (const auto &retVal = item.value()->GetValue<::Types::Value<int32_t>>()) {
        auto current = static_cast<int8_t>((retVal.value().GetValue() + kCurrentScale / 2) / kCurrentScale);
        FbisMessageBase::Serialize(out.get(), current);
      }
    }
    // ======== Serialize "Internal MAIN Supply Voltage" ========
    if (const auto &item = message.GetMessageItem(kInternalMainVoltage)) {
      if (const auto &retVal = item.value()->GetValue<::Types::Value<int32_t>>()) {
        auto voltage = static_cast<int8_t>((retVal.value().GetValue() + kVoltageScale / 2) / kVoltageScale);
        FbisMessageBase::Serialize(out.get(), voltage);
      }
    }
    // ======== Serialize "Internal STANDBY Supply Voltage" ========
    if (const auto &item = message.GetMessageItem(kInternalStandbyVoltage)) {
      if (const auto &retVal = item.value()->GetValue<::Types::Value<int32_t>>()) {
        auto voltage = static_cast<int8_t>((retVal.value().GetValue() + kVoltageScale / 2) / kVoltageScale);
        FbisMessageBase::Serialize(out.get(), voltage);
      }
    }
    // ======== Serialize "Internal 3V3 Supply Voltage" ========
    if (const auto &item = message.GetMessageItem(kInternal3V3Voltage)) {
      if (const auto &retVal = item.value()->GetValue<::Types::Value<int32_t>>()) {
        auto voltage = static_cast<int8_t>((retVal.value().GetValue() + kVoltageScale / 2) / kVoltageScale);
        FbisMessageBase::Serialize(out.get(), voltage);
      }
    }
    // ======== Serialize "Internal 1V8 Supply Voltage" ========
    if (const auto &item = message.GetMessageItem(kInternal1V8Voltage)) {
      if (const auto &retVal = item.value()->GetValue<::Types::Value<int32_t>>()) {
        auto voltage = static_cast<int8_t>((retVal.value().GetValue() + kVoltageScale / 2) / kVoltageScale);
        FbisMessageBase::Serialize(out.get(), voltage);
      }
    }
  }
  return std::move(out);
}

}  // namespace Communication::Protocols::FbisNetwork
