/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Types/Value.h"
#include "Types/ModuleType.h"
#include "Messages/Types/MessageItemState.h"
#include "Messages/Types/DataStatus.h"
#include "Messages/Visitor/VisitorPropertyGet.h"
#include "Messages/MessageItemProperty.h"
#include "Messages/MessageTemplate.h"
#include "SERTemperatureRead.h"

namespace Communication::Protocols::FbisNetwork {

const char SERTemperatureRead::kUpperPCBEdgeTemperature[] = "UpperPCBEdgeTemperature";

std::experimental::optional<std::unique_ptr<Messages::MessageTemplate<Messages::MessageItemProperty>>>
SERTemperatureRead::Deserialize(std::unique_ptr<std::vector<char>> input) {
  // Create message
  auto message = std::make_unique<Messages::MessageTemplate<Messages::MessageItemProperty>>();
  // Set request id
  message->SetRequestId(Communication::Types::RequestId::Enum::SCU_SERIALIZER_TEMPERATURE_READ);
  // Check message length
  auto length = (*input).size();
  if (length != 5) {  // outer frame 5 chars, inner frame 0 char
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_REQUEST_FORMAT);
    return std::move(message);
  }
  // Add upper PCB edge temperature item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kUpperPCBEdgeTemperature,
      std::string("Serializer").append("/Serializer/UpperTemperature/Value"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Return message
  return std::move(message);
}

std::experimental::optional<std::unique_ptr<std::vector<char>>> SERTemperatureRead::Serialize(
  const Messages::MessageTemplate<Messages::MessageItemProperty> &message) {
  // Prepare out vector
  auto out = std::make_unique<std::vector<char>>();
  auto dataStatus = message.GetDataStatus();
  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    for (const auto &item : message.GetMessageItems()) {
      if (item->ItemState()->GetValue() != Messages::Types::MessageItemState::Enum::OK) {
        dataStatus = Messages::Types::DataStatus::Enum::INFORMATION_NOT_AVAILABLE;
        break;
      }
    }
  }

  // ======== Serialize "Data Status" ========
  FbisMessageBase::Serialize(out.get(), static_cast<uint8_t>(dataStatus));

  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    // ======== Serialize "Upper PCB Edge Temperature" ========
    if (const auto &item = message.GetMessageItem(kUpperPCBEdgeTemperature)) {
      if (const auto &retVal = item.value()->GetValue<::Types::Value<int32_t>>()) {
        auto temperature = static_cast<int32_t>(retVal.value().GetValue());
        FbisMessageBase::Serialize(out.get(), temperature);
      }
    }
  }
  return std::move(out);
}

}  // namespace Communication::Protocols::FbisNetwork
