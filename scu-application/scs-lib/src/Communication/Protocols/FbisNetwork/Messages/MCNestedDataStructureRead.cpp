/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Types/Value.h"
#include "Types/ModuleType.h"
#include "Messages/Types/MessageItemState.h"
#include "Messages/Types/DataStatus.h"
#include "Messages/Visitor/VisitorPropertyGet.h"
#include "Messages/MessageItemProperty.h"
#include "Messages/MessageTemplate.h"
#include "MCNestedDataStructureRead.h"
#include "SystemModules/Modules/MezzanineCards/Common/ScuArray.h"

namespace Communication::Protocols::FbisNetwork {

const char MCNestedDataStructureRead::kNestedDataStructureRead[] = "NestedDataStructureRead";

std::experimental::optional <std::unique_ptr<Messages::MessageTemplate<Messages::MessageItemProperty>>>
MCNestedDataStructureRead::Deserialize(std::unique_ptr <std::vector<char>> input) {
  // Create message
  auto message = std::make_unique<Messages::MessageTemplate<Messages::MessageItemProperty>>();
  // Set request id
  message->SetRequestId(Communication::Types::RequestId::Enum::SCU_MC_NESTED_DATA_STRUCTURE_READ);
  // Check message length
  auto length = (*input).size();
  if (length != 6) {  // outer frame 5 chars, inner frame 1 char
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_REQUEST_FORMAT);
    return std::move(message);
  }
  // Decode slot
  auto slot = static_cast<uint8_t>((*input)[kFirstParam]);
  if (slot < kSlotMin || slot > kSlotMax) {
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::SLOT_NOT_VALID);
    return std::move(message);
  }
  // Add fast frame signals read
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kNestedDataStructureRead,
      std::string("MC").append(std::to_string(slot)).append("/Management/ScuArray/Nds"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Return message
  return std::move(message);
}

std::experimental::optional <std::unique_ptr<std::vector < char>>>

MCNestedDataStructureRead::Serialize(
  const Messages::MessageTemplate<Messages::MessageItemProperty> &message) {
  // Prepare out vector
  auto out = std::make_unique < std::vector < char >> ();
  auto dataStatus = message.GetDataStatus();
  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    for (const auto &item : message.GetMessageItems()) {
      if (item->ItemState()->GetValue() != Messages::Types::MessageItemState::Enum::OK) {
        dataStatus = Messages::Types::DataStatus::Enum::INFORMATION_NOT_AVAILABLE;
        break;
      }
    }
  }

  // ======== Serialize "Data Status" ========
  FbisMessageBase::Serialize(out.get(), static_cast<uint8_t>(dataStatus));

  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    // ======== Serialize "fast frame signals" ========
    if (const auto &item = message.GetMessageItem(kNestedDataStructureRead)) {
      if (const auto &fastFrameSignalVector = item.value()->GetValue < std::vector < uint32_t >> ()) {
        for (const auto fFSignal : fastFrameSignalVector.value()) {
          FbisMessageBase::Serialize(out.get(), static_cast<int8_t>(fFSignal >> 16));
          FbisMessageBase::Serialize(out.get(), static_cast<int8_t>(fFSignal >> 8));
          FbisMessageBase::Serialize(out.get(), static_cast<int8_t>(fFSignal));
        }
      }
    }
  }
  return std::move(out);
}

}  // namespace Communication::Protocols::FbisNetwork
