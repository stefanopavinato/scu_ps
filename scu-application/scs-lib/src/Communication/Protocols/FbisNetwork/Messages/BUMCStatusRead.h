/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <experimental/optional>
#include <memory>
#include "FbisMessageBase.h"

namespace Messages {
template<typename T>
class MessageTemplate;

class MessageItemProperty;
}  // namespace Messages
namespace Communication::Protocols::FbisNetwork {
class BUMCStatusRead : public FbisMessageBase {
 public:
  static std::experimental::optional<std::unique_ptr<Messages::MessageTemplate<Messages::MessageItemProperty>>>
  Deserialize(std::unique_ptr<std::vector<char>> input);

  static std::experimental::optional<std::unique_ptr<std::vector<char>>> Serialize(
    const Messages::MessageTemplate<Messages::MessageItemProperty> &message);

 private:
  static const char kBoardName[];
  static const char kMezzanineCardPresent[];
  static const char kMezzanineCardEnable[];
  static const char kMezzanineCardReset[];
  static const char kDriverEnable[];
  static const char kDriverReset[];
  static const char kOperatingHours[];
  static const char kRwReg1[];
  static const char kRwReg2[];
  static const char kRwReg3[];
  static const char kStatReg1[];
  static const char kStatReg2[];
  static const char kStatReg3[];
  static const char kStatReg4[];
  static const char kStatReg5[];
  static const char kStatReg6[];
};
}  // namespace Communication::Protocols::FbisNetwork
