/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <experimental/optional>
#include <memory>

namespace Messages {
template<typename T>
class MessageTemplate;

class MessageItemProperty;
}  // namespace Messages
namespace Communication {
namespace Protocols {
namespace FbisNetwork {
class FUTemperatureRead {
 public:
  static std::experimental::optional<std::unique_ptr<Messages::MessageTemplate<Messages::MessageItemProperty>>>
  Deserialize(std::unique_ptr<std::
  vector<char>> input);

  static std::experimental::optional<std::unique_ptr<std::vector<char>>> Serialize(
    const Messages::MessageTemplate<Messages::MessageItemProperty> &message);

 private:
  static const char kFanUnitControllerATemperature[];
//  static const char kFanUnitControllerMTemperature[];
  static const char kFanUnitControllerBTemperature[];
};
}  // namespace FbisNetwork
}  // namespace Protocols
}  // namespace Communication
