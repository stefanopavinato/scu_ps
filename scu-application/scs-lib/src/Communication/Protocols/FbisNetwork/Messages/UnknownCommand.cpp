/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Types/Value.h"
#include "Types/ModuleType.h"
#include "Messages/Types/MessageItemState.h"
#include "Messages/Types/DataStatus.h"
#include "Messages/Visitor/VisitorPropertyGet.h"
#include "Messages/MessageItemProperty.h"
#include "Messages/MessageTemplate.h"
#include "UnknownCommand.h"

namespace Communication::Protocols::FbisNetwork {

std::experimental::optional<std::unique_ptr<Messages::MessageTemplate<Messages::MessageItemProperty>>>
UnknownCommand::Deserialize(const Communication::Types::RequestId &requestId) {
  // Create message
  auto message = std::make_unique<Messages::MessageTemplate<Messages::MessageItemProperty>>();
  // Set request id
  message->SetRequestId(requestId.GetValue());
  // Check message length
  message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::COMMAND_NOT_IMPLEMENTED);
  return std::move(message);
}

std::experimental::optional<std::unique_ptr<std::vector<char>>> UnknownCommand::Serialize(
  const Messages::MessageTemplate<Messages::MessageItemProperty> &message) {
  // Prepare out vector
  auto out = std::make_unique<std::vector<char>>();
  auto dataStatus = message.GetDataStatus();

  // ======== Serialize "Data Status" ========
  FbisMessageBase::Serialize(out.get(), static_cast<uint8_t>(dataStatus));
  return std::move(out);
}

}  // namespace Communication::Protocols::FbisNetwork
