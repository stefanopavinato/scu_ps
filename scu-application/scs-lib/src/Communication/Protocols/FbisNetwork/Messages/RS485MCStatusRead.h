/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <experimental/optional>
#include <memory>
#include "FbisMessageBase.h"

namespace Messages {
template<typename T>
class MessageTemplate;

class MessageItemProperty;
}  // namespace Messages
namespace Communication::Protocols::FbisNetwork {
class RS485MCStatusRead : public FbisMessageBase {
 public:
  static std::experimental::optional<std::unique_ptr<Messages::MessageTemplate<Messages::MessageItemProperty>>>
  Deserialize(std::unique_ptr<std::vector<char>> input);

  static std::experimental::optional<std::unique_ptr<std::vector<char>>> Serialize(
    const Messages::MessageTemplate<Messages::MessageItemProperty> &message);

 private:
  static const int32_t kCurrentScale;
  static const int32_t kVoltageScale;

  static const char kBoardName[];
  static const char kMezzanineCardPresent[];
  static const char kMezzanineCardEnable[];
  static const char kMezzanineCardReset[];
  static const char kDriverEnable[];
  static const char kDriverReset[];
  static const char kOperatingHours[];
  static const char kCollectionOverride[];
  static const char kCollectionStatus1[];
  static const char kCollectionStatus2[];
  static const char kCollectionStatus3[];
  static const char kCollectionStatus4[];
  static const char kPort1Signal0Loss[];
  static const char kPort1Signal1Loss[];
  static const char kPort1Signal2Loss[];
  static const char kPort1Signal3Loss[];
  static const char kPort2Signal0Loss[];
  static const char kPort2Signal1Loss[];
  static const char kPort2Signal2Loss[];
  static const char kPort2Signal3Loss[];
  static const char kTemperatureEthernetSwitch[];
  static const char kMainCurrent[];
  static const char kInternalMainVoltage[];
  static const char kInternalStandbyVoltage[];
  static const char kInternal3V3Voltage[];
  static const char kInternal2V5Voltage[];
  static const char kInternal1V8Voltage[];
  static const char kInternal1V2Voltage[];
};
}  // namespace Communication::Protocols::FbisNetwork
