/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Types/Value.h"
#include "Types/VariantValue.h"
#include "Messages/Types/MessageItemState.h"
#include "Messages/Types/DataStatus.h"
#include "Messages/Visitor/VisitorPropertyGet.h"
#include "Messages/Visitor/VisitorPropertySet.h"
#include "Messages/MessageItemProperty.h"
#include "Messages/MessageTemplate.h"
#include "FanUnitFanSpeedWrite.h"

namespace Communication::Protocols::FbisNetwork {

const int32_t FanUnitFanSpeedWrite::kFanNumberPosition = 3;
const int32_t FanUnitFanSpeedWrite::kFanSpeedPosition = 4;
const int32_t FanUnitFanSpeedWrite::kFanSpeedMin = 0;  // Input from command
const int32_t FanUnitFanSpeedWrite::kFanSpeedMax = 255;
const int32_t FanUnitFanSpeedWrite::kFanSpeedAuto = 255;
const int32_t FanUnitFanSpeedWrite::kFanSpeedPwmMin = 254;  // to set at the controller
const int32_t FanUnitFanSpeedWrite::kFanSpeedPwmMax = 110;

std::experimental::optional<std::unique_ptr<Messages::MessageTemplate<Messages::MessageItemProperty>>>
FanUnitFanSpeedWrite::Deserialize(std::unique_ptr<std::vector<char>> input) {
  // Create message
  auto message = std::make_unique<Messages::MessageTemplate<Messages::MessageItemProperty>>();
  // Set request id
  message->SetRequestId(Communication::Types::RequestId::Enum::FAN_SPEED_WRITE);
  // Check message length
  auto length = (*input).size();
  if (length != 7) {  // outer frame 5 chars, inner frame 2 char
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_REQUEST_FORMAT);
    return std::move(message);
  }
  // Decode index of fan
  auto fanNumber = static_cast<uint8_t>((*input)[kFanNumberPosition]);
  std::string fanName;
  switch (fanNumber) {
    case 0: {
      fanName = "FanA";
      break;
    }
//    case 1: {
//      fanName = "FanM";
//      break;
//    }
    case 2: {
      fanName = "FanB";
      break;
    }
    default: {
      message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_SIGNAL_INDEX);
      return std::move(message);
    }
  }
  // Decode value of signal to be overridden and check if valid
  auto fanPWMValue = ::Types::TestVariantValue();
  fanPWMValue.template SetValue(static_cast<uint8_t>(255));
  auto fanSpeed = static_cast<uint8_t>((*input)[kFanSpeedPosition]);
  if (fanSpeed == kFanSpeedAuto) {
    message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
        "default-value",
        std::string("FanUnit/FanUnit/").append(fanName).append("/PWM1"),
        std::make_unique<Messages::Visitor::VisitorPropertySet>(
          Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT_FORCE_RELEASE,
          ::Types::ValueType::Enum::UINT8,
          fanPWMValue)));
  } else if ((fanSpeed >= kFanSpeedMin) && (fanSpeed <= kFanSpeedMax)) {
    auto fanPWM = (fanSpeed - kFanSpeedMin);
    fanPWM *= (kFanSpeedPwmMin - kFanSpeedPwmMax);  // speed on chip is inverted
    fanPWM /= (kFanSpeedMax - kFanSpeedMin);  // speed on chip is inverted
    fanPWM = kFanSpeedPwmMin - fanPWM;

    // write fan pwm
    auto fanPWMValue = ::Types::TestVariantValue();
    fanPWMValue.template SetValue(static_cast<uint8_t>(fanPWM));
    message->AddMessageItem(
        std::make_unique<Messages::MessageItemProperty>(
            "default-value",
            std::string("FanUnit/FanUnit/").append(fanName).append("/PWM1"),
            std::make_unique<Messages::Visitor::VisitorPropertySet>(
                Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT_FORCE,
                ::Types::ValueType::Enum::UINT8,
                fanPWMValue)));
  }
  return std::move(message);
}

std::experimental::optional<std::unique_ptr<std::vector<char>>> FanUnitFanSpeedWrite::Serialize(
  const Messages::MessageTemplate<Messages::MessageItemProperty> &message) {
  // Prepare out vector
  auto out = std::make_unique<std::vector<char>>();
  auto dataStatus = message.GetDataStatus();
  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    for (const auto &item : message.GetMessageItems()) {
      if (item->ItemState()->GetValue() != Messages::Types::MessageItemState::Enum::OK) {
        dataStatus = Messages::Types::DataStatus::Enum::INFORMATION_NOT_AVAILABLE;
        break;
      }
    }
  }

  // ======== Serialize "Data Status" ========
  out->push_back(static_cast<char>(dataStatus));
  return std::move(out);
}

}  // namespace Communication::Protocols::FbisNetwork
