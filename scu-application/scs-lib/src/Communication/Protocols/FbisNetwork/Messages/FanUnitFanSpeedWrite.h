/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <experimental/optional>
#include <memory>
#include "FbisMessageBase.h"

namespace Messages {
template<typename T>
class MessageTemplate;

class MessageItemProperty;
}  // namespace Messages
namespace Communication::Protocols::FbisNetwork {
class FanUnitFanSpeedWrite : public FbisMessageBase {
 public:
  static std::experimental::optional<std::unique_ptr<Messages::MessageTemplate<Messages::MessageItemProperty>>>
  Deserialize(std::unique_ptr<std::vector<char>> input);

  static std::experimental::optional<std::unique_ptr<std::vector<char>>> Serialize(
    const Messages::MessageTemplate<Messages::MessageItemProperty> &message);

 private:
  static const int32_t kFanNumberPosition;
  static const int32_t kFanSpeedPosition;
  static const int32_t kFanSpeedMin;
  static const int32_t kFanSpeedMax;
  static const int32_t kFanSpeedAuto;
  static const int32_t kFanSpeedPwmMin;
  static const int32_t kFanSpeedPwmMax;
};
}  // namespace Communication::Protocols::FbisNetwork
