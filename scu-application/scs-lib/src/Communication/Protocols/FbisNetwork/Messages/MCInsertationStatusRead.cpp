/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Types/Value.h"
#include "Types/ModuleType.h"
#include "Types/ModuleState.h"
#include "Messages/Types/MessageItemState.h"
#include "Messages/Types/DataStatus.h"
#include "Messages/Visitor/VisitorPropertyGet.h"
#include "Messages/MessageItemProperty.h"
#include "Messages/MessageTemplate.h"
#include "MCInsertionStatusRead.h"

namespace Communication::Protocols::FbisNetwork {

const char MCInsertionStatusRead::kMCInsertionStatus[] = "MCInsertionStatus";

std::experimental::optional<std::unique_ptr<Messages::MessageTemplate<Messages::MessageItemProperty>>>
MCInsertionStatusRead::Deserialize(std::unique_ptr<std::vector<char>> input) {
  // Create message
  auto message = std::make_unique<Messages::MessageTemplate<Messages::MessageItemProperty>>();
  // Set request id
  message->SetRequestId(Communication::Types::RequestId::Enum::SCU_MC_INSERTION_STATUS_READ);
  // Check message length
  auto length = (*input).size();
  if (length != 6) {  // outer frame 5 chars, inner frame 1 char
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_REQUEST_FORMAT);
    return std::move(message);
  }
  // Decode slot
  auto slot = static_cast<uint8_t>((*input)[kFirstParam]);
  if (slot < kSlotMin || slot > kSlotMax) {
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::SLOT_NOT_VALID);
    return std::move(message);
  }
      // Add MC Insertion Status item
      message->AddMessageItem(
        std::make_unique<Messages::MessageItemProperty>(
          kMCInsertionStatus,
          std::string("MC").append(std::to_string(slot)),  // Prüfen of Pfad korrekt
          std::make_unique<Messages::Visitor::VisitorPropertyGet>(
            Messages::Visitor::Types::AttributeType::Enum::MODULE_STATE)));
  // Return message
  return std::move(message);
}

std::experimental::optional<std::unique_ptr<std::vector<char>>> MCInsertionStatusRead::Serialize(
  const Messages::MessageTemplate<Messages::MessageItemProperty> &message) {
  // Prepare out vector
  auto out = std::make_unique<std::vector<char>>();
  auto dataStatus = message.GetDataStatus();
  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    for (const auto &item : message.GetMessageItems()) {
      if (item->ItemState()->GetValue() != Messages::Types::MessageItemState::Enum::OK) {
        dataStatus = Messages::Types::DataStatus::Enum::INFORMATION_NOT_AVAILABLE;
        break;
      }
    }
  }
  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    if (const auto &item = message.GetMessageItem(kMCInsertionStatus)) {
      if (const auto &retVal = item.value()->GetValue<::Types::ModuleState>()) {
        auto moduleStatus = static_cast<int8_t>(retVal.value().GetEnum());
        out->push_back(static_cast<char>(dataStatus));
        FbisMessageBase::Serialize(out.get(), moduleStatus);
      } else {
        out->push_back(static_cast<char>(Messages::Types::DataStatus::Enum::INFORMATION_NOT_AVAILABLE));
      }
    } else {
      out->push_back(static_cast<char>(Messages::Types::DataStatus::Enum::INFORMATION_NOT_AVAILABLE));
    }
  } else {
    out->push_back(static_cast<char>(Messages::Types::DataStatus::Enum::INFORMATION_NOT_AVAILABLE));
  }
  return std::move(out);
}
}  // namespace Communication::Protocols::FbisNetwork
