/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Types/Value.h"
#include "Types/VariantValue.h"
#include "Types/ModuleType.h"
#include "Messages/Types/MessageItemState.h"
#include "Messages/Types/DataStatus.h"
#include "Messages/Visitor/VisitorPropertyGet.h"
#include "Messages/Visitor/VisitorPropertySet.h"
#include "Messages/MessageItemProperty.h"
#include "Messages/MessageTemplate.h"
#include "CLMCDatalinkErrorsClear.h"

namespace Communication::Protocols::FbisNetwork {

const char CLMCDatalinkErrorsClear::kBoardName[] = "BoardName";
const char CLMCDatalinkErrorsClear::kMezzanineCardPresent[] = "MezzanineCardPresent";
const char CLMCDatalinkErrorsClear::kDatalinkTimeExpectationTimeoutsResetName[] =
                                                "DatalinkTimeExpectationTimeoutsReset";
const char CLMCDatalinkErrorsClear::kDatalinkLifeSignErrorsResetName[] = "DatalinkLifeSignErrorsReset";

std::experimental::optional<std::unique_ptr<Messages::MessageTemplate<Messages::MessageItemProperty>>>
CLMCDatalinkErrorsClear::Deserialize(std::unique_ptr<std::vector<char>> input) {
  // Create message
  auto message = std::make_unique<Messages::MessageTemplate<Messages::MessageItemProperty>>();
  // Set request id
  message->SetRequestId(Communication::Types::RequestId::Enum::SCU_CLMC_DATALINK_ERRORS_CLEAR);
  // Check message length
  auto length = (*input).size();
  if (length != 6) {  // outer frame 5 chars, inner frame 1 char
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_REQUEST_FORMAT);
    return std::move(message);
  }
  // Decode slot
  auto slot = static_cast<uint8_t>((*input)[kFirstParam]);
  if (slot < kSlotMin || slot > kSlotMax) {
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::SLOT_NOT_VALID);
    return std::move(message);
  }
  // Add mezzanine card board name
  message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
          kBoardName,
          std::string("MC").append(std::to_string(slot)).append("/Management/EEProm/BoardName"),
          std::make_unique<Messages::Visitor::VisitorPropertyGet>(
              Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add mezzanine card present item
  message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
          kMezzanineCardPresent,
          std::string("MC").append(std::to_string(slot)).append("/CardDetectionItems/MezzanineCardPresent"),
          std::make_unique<Messages::Visitor::VisitorPropertyGet>(
              Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Clear error counters
  ::Types::TestVariantValue value;
  value.SetValue(true);
  message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
          kDatalinkTimeExpectationTimeoutsResetName,
          std::string("MC").append(std::to_string(slot))
              .append("/Application/MCDriver/Status/").append(kDatalinkTimeExpectationTimeoutsResetName),
          std::make_unique<Messages::Visitor::VisitorPropertySet>(
              Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT_FORCE_ONCE,
              ::Types::ValueType::Enum::BOOL,
              value)));
  message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
          kDatalinkLifeSignErrorsResetName,
          std::string("MC").append(std::to_string(slot))
              .append("/Application/MCDriver/Status/").append(kDatalinkLifeSignErrorsResetName),
          std::make_unique<Messages::Visitor::VisitorPropertySet>(
              Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT_FORCE_ONCE,
              ::Types::ValueType::Enum::BOOL,
              value)));
  return std::move(message);
}

std::experimental::optional<std::unique_ptr<std::vector<char>>> CLMCDatalinkErrorsClear::Serialize(
    const Messages::MessageTemplate<Messages::MessageItemProperty> &message) {
  // Prepare out vector
  auto out = std::make_unique<std::vector<char>>();
  auto dataStatus = message.GetDataStatus();
  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    for (const auto &item : message.GetMessageItems()) {
      if (item->ItemState()->GetValue() != Messages::Types::MessageItemState::Enum::OK) {
        dataStatus = Messages::Types::DataStatus::Enum::INFORMATION_NOT_AVAILABLE;
        break;
      }
    }
  }
  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    // ======== Check MC ========
    if (const auto &item = message.GetMessageItem(kMezzanineCardPresent)) {
      if (!item.value()->GetValue<bool>().value()) {
        dataStatus = Messages::Types::DataStatus::Enum::MC_NOT_PRESENT;
      }
    }
  }
  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    if (const auto &item = message.GetMessageItem(kBoardName)) {
      if (const auto &retVal = item.value()->GetValue<::Types::ModuleType>()) {
        if (retVal.value().GetEnum() != ::Types::ModuleType::Enum::SCU_CLMC) {
          dataStatus = Messages::Types::DataStatus::Enum::WRONG_MEZZANINE_CARD;
        }
      } else {
        dataStatus = Messages::Types::DataStatus::Enum::INFORMATION_NOT_AVAILABLE;
      }
    }
  }

  // ======== Serialize "Data Status" ========
  out->push_back(static_cast<char>(dataStatus));
  return std::move(out);
}

}  // namespace Communication::Protocols::FbisNetwork
