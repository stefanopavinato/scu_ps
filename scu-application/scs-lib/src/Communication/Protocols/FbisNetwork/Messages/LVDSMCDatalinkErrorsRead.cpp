/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Types/Value.h"
#include "Types/VariantValue.h"
#include "Types/ModuleType.h"
#include "Messages/Types/MessageItemState.h"
#include "Messages/Types/DataStatus.h"
#include "Messages/Visitor/VisitorPropertyGet.h"
#include "Messages/Visitor/VisitorPropertySet.h"
#include "Messages/MessageItemProperty.h"
#include "Messages/MessageTemplate.h"
#include "LVDSMCDatalinkErrorsRead.h"

namespace Communication::Protocols::FbisNetwork {

const char LVDSMCDatalinkErrorsRead::kBoardName[] = "BoardName";
const char LVDSMCDatalinkErrorsRead::kMezzanineCardPresent[] = "MezzanineCardPresent";
const char LVDSMCDatalinkErrorsRead::kDatalinkPort1TimeExpectationTimeoutsName[] =
    "MCDriverPort1/Status/DatalinkTimeExpectationTimeoutCounter";
const char LVDSMCDatalinkErrorsRead::kDatalinkPort1LifeSignErrorsName[] =
    "MCDriverPort1/Status/DatalinkLifeSignErrorCounter";
const char LVDSMCDatalinkErrorsRead::kDatalinkPort1DecoderErrorsName[] =
    "MCDriverPort1/Status/DatalinkDecoderErrorCounter";
const char LVDSMCDatalinkErrorsRead::kDatalinkPort2TimeExpectationTimeoutsName[] =
    "MCDriverPort2/Status/DatalinkTimeExpectationTimeoutCounter";
const char LVDSMCDatalinkErrorsRead::kDatalinkPort2LifeSignErrorsName[] =
    "MCDriverPort2/Status/DatalinkLifeSignErrorCounter";
const char LVDSMCDatalinkErrorsRead::kDatalinkPort2DecoderErrorsName[] =
    "MCDriverPort2/Status/DatalinkDecoderErrorCounter";
const char LVDSMCDatalinkErrorsRead::kDatalinkPort3TimeExpectationTimeoutsName[] =
    "MCDriverPort3/Status/DatalinkTimeExpectationTimeoutCounter";
const char LVDSMCDatalinkErrorsRead::kDatalinkPort3LifeSignErrorsName[] =
    "MCDriverPort3/Status/DatalinkLifeSignErrorCounter";
const char LVDSMCDatalinkErrorsRead::kDatalinkPort3DecoderErrorsName[] =
    "MCDriverPort3/Status/DatalinkDecoderErrorCounter";


std::experimental::optional<std::unique_ptr<Messages::MessageTemplate<Messages::MessageItemProperty>>>
LVDSMCDatalinkErrorsRead::Deserialize(std::unique_ptr<std::vector<char>> input) {
  // Create message
  auto message = std::make_unique<Messages::MessageTemplate<Messages::MessageItemProperty>>();
  // Set request id
  message->SetRequestId(Communication::Types::RequestId::Enum::SCU_LVDSMC_DATALINK_ERRORS_READ);
  // Check message length
  auto length = (*input).size();
  if (length != 6) {  // outer frame 5 chars, inner frame 1 char
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_REQUEST_FORMAT);
    return std::move(message);
  }
  // Decode slot
  auto slot = static_cast<uint8_t>((*input)[kFirstParam]);
  if (slot < kSlotMin || slot > kSlotMax) {
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::SLOT_NOT_VALID);
    return std::move(message);
  }
  // Add mezzanine card board name
  message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
          kBoardName,
          std::string("MC").append(std::to_string(slot)).append("/Management/EEProm/BoardName"),
          std::make_unique<Messages::Visitor::VisitorPropertyGet>(
              Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add mezzanine card present item
  message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
          kMezzanineCardPresent,
          std::string("MC").append(std::to_string(slot)).append("/CardDetectionItems/MezzanineCardPresent"),
          std::make_unique<Messages::Visitor::VisitorPropertyGet>(
              Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
          kDatalinkPort1TimeExpectationTimeoutsName,
          std::string("MC").append(std::to_string(slot))
              .append("/Application/MCDriver/").append(kDatalinkPort1TimeExpectationTimeoutsName),
          std::make_unique<Messages::Visitor::VisitorPropertyGet>(
              Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
          kDatalinkPort1LifeSignErrorsName,
          std::string("MC").append(std::to_string(slot))
              .append("/Application/MCDriver/").append(kDatalinkPort1LifeSignErrorsName),
          std::make_unique<Messages::Visitor::VisitorPropertyGet>(
              Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
          kDatalinkPort1DecoderErrorsName,
          std::string("MC").append(std::to_string(slot))
              .append("/Application/MCDriver/").append(kDatalinkPort1DecoderErrorsName),
          std::make_unique<Messages::Visitor::VisitorPropertyGet>(
              Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));

  message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
          kDatalinkPort2TimeExpectationTimeoutsName,
          std::string("MC").append(std::to_string(slot))
              .append("/Application/MCDriver/").append(kDatalinkPort2TimeExpectationTimeoutsName),
          std::make_unique<Messages::Visitor::VisitorPropertyGet>(
              Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
          kDatalinkPort2LifeSignErrorsName,
          std::string("MC").append(std::to_string(slot))
              .append("/Application/MCDriver/").append(kDatalinkPort2LifeSignErrorsName),
          std::make_unique<Messages::Visitor::VisitorPropertyGet>(
              Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
          kDatalinkPort2DecoderErrorsName,
          std::string("MC").append(std::to_string(slot))
              .append("/Application/MCDriver/").append(kDatalinkPort2DecoderErrorsName),
          std::make_unique<Messages::Visitor::VisitorPropertyGet>(
              Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));

  message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
          kDatalinkPort3TimeExpectationTimeoutsName,
          std::string("MC").append(std::to_string(slot))
              .append("/Application/MCDriver/").append(kDatalinkPort3TimeExpectationTimeoutsName),
          std::make_unique<Messages::Visitor::VisitorPropertyGet>(
              Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
          kDatalinkPort3LifeSignErrorsName,
          std::string("MC").append(std::to_string(slot))
              .append("/Application/MCDriver/").append(kDatalinkPort3LifeSignErrorsName),
          std::make_unique<Messages::Visitor::VisitorPropertyGet>(
              Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
          kDatalinkPort3DecoderErrorsName,
          std::string("MC").append(std::to_string(slot))
              .append("/Application/MCDriver/").append(kDatalinkPort3DecoderErrorsName),
          std::make_unique<Messages::Visitor::VisitorPropertyGet>(
              Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  return std::move(message);
}

std::experimental::optional<std::unique_ptr<std::vector<char>>> LVDSMCDatalinkErrorsRead::Serialize(
    const Messages::MessageTemplate<Messages::MessageItemProperty> &message) {
  // Prepare out vector
  auto out = std::make_unique<std::vector<char>>();
  auto dataStatus = message.GetDataStatus();
  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    for (const auto &item : message.GetMessageItems()) {
      if (item->ItemState()->GetValue() != Messages::Types::MessageItemState::Enum::OK) {
        dataStatus = Messages::Types::DataStatus::Enum::INFORMATION_NOT_AVAILABLE;
        break;
      }
    }
  }
  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    // ======== Check MC ========
    if (const auto &item = message.GetMessageItem(kMezzanineCardPresent)) {
      if (!item.value()->GetValue<bool>().value()) {
        dataStatus = Messages::Types::DataStatus::Enum::MC_NOT_PRESENT;
      }
    }
  }
  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    if (const auto &item = message.GetMessageItem(kBoardName)) {
      if (const auto &retVal = item.value()->GetValue<::Types::ModuleType>()) {
        if (retVal.value().GetEnum() != ::Types::ModuleType::Enum::SCU_LVDSMC) {
          dataStatus = Messages::Types::DataStatus::Enum::WRONG_MEZZANINE_CARD;
        }
      } else {
        dataStatus = Messages::Types::DataStatus::Enum::INFORMATION_NOT_AVAILABLE;
      }
    }
  }

  // ======== Serialize "Data Status" ========
  out->push_back(static_cast<char>(dataStatus));
  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    // Serialize Datalink Port1 TimeExpectation-timeout Error Counter
    if (const auto &item = message.GetMessageItem(kDatalinkPort1TimeExpectationTimeoutsName)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // Serialize Datalink Port1 LifeSign Error Counter
    if (const auto &item = message.GetMessageItem(kDatalinkPort1LifeSignErrorsName)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // Serialize Datalink Port1 DecoderError Error Counter
    if (const auto &item = message.GetMessageItem(kDatalinkPort1DecoderErrorsName)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // Serialize Datalink Port2 TimeExpectation-timeout Error Counter
    if (const auto &item = message.GetMessageItem(kDatalinkPort2TimeExpectationTimeoutsName)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // Serialize Datalink Port2 LifeSign Error Counter
    if (const auto &item = message.GetMessageItem(kDatalinkPort2LifeSignErrorsName)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // Serialize Datalink Port2 DecoderError Error Counter
    if (const auto &item = message.GetMessageItem(kDatalinkPort2DecoderErrorsName)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // Serialize Datalink Port3 TimeExpectation-timeout Error Counter
    if (const auto &item = message.GetMessageItem(kDatalinkPort3TimeExpectationTimeoutsName)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // Serialize Datalink Port3 LifeSign Error Counter
    if (const auto &item = message.GetMessageItem(kDatalinkPort3LifeSignErrorsName)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // Serialize Datalink Port3 DecoderError Error Counter
    if (const auto &item = message.GetMessageItem(kDatalinkPort3DecoderErrorsName)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
  }
  return std::move(out);
}

}  // namespace Communication::Protocols::FbisNetwork
