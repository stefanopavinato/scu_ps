/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Types/Value.h"
#include "Types/ModuleType.h"
#include "Messages/Types/MessageItemState.h"
#include "Messages/Types/DataStatus.h"
#include "Messages/Visitor/VisitorPropertyGet.h"
#include "Messages/MessageItemProperty.h"
#include "Messages/MessageTemplate.h"
#include "FanUnitStatusRead.h"

namespace Communication::Protocols::FbisNetwork {

const char FanUnitStatusRead::kFanUnitOperatingHours[] = "OperatingHours";
const char FanUnitStatusRead::kFanASpeed[] = "FanA/Speed";
// const char FanUnitStatusRead::kFanMSpeed[] = "FanM/Speed";
const char FanUnitStatusRead::kFanBSpeed[] = "FanB/Speed";
const char FanUnitStatusRead::kFanAOperatingHours[] = "FanAOperatingHours";
// const char FanUnitStatusRead::kFanMOperatingHours[] = "FanMOperatingHours";
const char FanUnitStatusRead::kFanBOperatingHours[] = "FanBOperatingHours";

std::experimental::optional<std::unique_ptr<Messages::MessageTemplate<Messages::MessageItemProperty>>>
FanUnitStatusRead::Deserialize(std::unique_ptr<std::vector<char>> input) {
  // Create message
  auto message = std::make_unique<Messages::MessageTemplate<Messages::MessageItemProperty>>();
  // Set request id
  message->SetRequestId(Communication::Types::RequestId::Enum::SCU_FAN_UNIT_STATUS_READ);
  // Check message length
  auto length = (*input).size();
  if (length != 5) {  // outer frame 5 chars, inner frame 0 char
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_REQUEST_FORMAT);
    return std::move(message);
  }
  // Add item operating hours
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
        kFanUnitOperatingHours,
      std::string("FanUnit/FanUnit/EEProm/").append(kFanUnitOperatingHours),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add FanASpeed
  message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
          kFanASpeed,
          std::string("FanUnit/FanUnit/").append(kFanASpeed),
          std::make_unique<Messages::Visitor::VisitorPropertyGet>(
              Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
//  // Add FanMSpeed
//  message->AddMessageItem(
//      std::make_unique<Messages::MessageItemProperty>(
//          kFanMSpeed,
//          std::string("FanUnit/FanUnit/").append(kFanMSpeed),
//          std::make_unique<Messages::Visitor::VisitorPropertyGet>(
//              Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add FanBSpeed
  message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
          kFanBSpeed,
          std::string("FanUnit/FanUnit/").append(kFanBSpeed),
          std::make_unique<Messages::Visitor::VisitorPropertyGet>(
              Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add FanA operating hours
  message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
          kFanAOperatingHours,
          std::string("FanUnit/FanUnit/EEProm/").append(kFanAOperatingHours),
          std::make_unique<Messages::Visitor::VisitorPropertyGet>(
              Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
//  // Add FanM operating hours
//  message->AddMessageItem(
//      std::make_unique<Messages::MessageItemProperty>(
//          kFanMOperatingHours,
//          std::string("FanUnit/FanUnit/EEProm/").append(kFanMOperatingHours),
//          std::make_unique<Messages::Visitor::VisitorPropertyGet>(
//              Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add FanB operating hours
  message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
          kFanBOperatingHours,
          std::string("FanUnit/FanUnit/EEProm/").append(kFanBOperatingHours),
          std::make_unique<Messages::Visitor::VisitorPropertyGet>(
              Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Return message
  return std::move(message);
}

std::experimental::optional<std::unique_ptr<std::vector<char>>> FanUnitStatusRead::Serialize(
  const Messages::MessageTemplate<Messages::MessageItemProperty> &message) {
  // Prepare out vector
  auto out = std::make_unique<std::vector<char>>();
  auto dataStatus = message.GetDataStatus();
  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    for (const auto &item : message.GetMessageItems()) {
      if (item->ItemState()->GetValue() != Messages::Types::MessageItemState::Enum::OK) {
        dataStatus = Messages::Types::DataStatus::Enum::INFORMATION_NOT_AVAILABLE;
        break;
      }
    }
  }

  // ======== Serialize "Data Status" ========
  FbisMessageBase::Serialize(out.get(), static_cast<uint8_t>(dataStatus));

  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    // ======== Serialize "FanUnit Operating Hours" ========
    if (const auto &item = message.GetMessageItem(kFanUnitOperatingHours)) {
      if (const auto &retVal = item.value()->GetValue<std::chrono::minutes>()) {
        auto operationTime = static_cast<uint32_t>(retVal.value().count() / 15);  // Cast to 15 min!
        FbisMessageBase::Serialize(out.get(), operationTime);
      }
    }
    // ======== Serialize "FanASpeed" ========
    if (const auto &item = message.GetMessageItem(kFanASpeed)) {
      if (const auto &retVal = item.value()->GetValue<::Types::Value<double>>()) {
        auto current = static_cast<int16_t>(retVal.value().GetValue());
        FbisMessageBase::Serialize(out.get(), current);
      }
    }
    // ======== Serialize "FanMSpeed" ========
    FbisMessageBase::Serialize(out.get(), static_cast<int16_t>(0x0000));
//    if (const auto &item = message.GetMessageItem(kFanMSpeed)) {
//      if (const auto &retVal = item.value()->GetValue<::Types::Value<double>>()) {
//        auto current = static_cast<int16_t>(retVal.value().GetValue());
//        FbisMessageBase::Serialize(out.get(), current);
//      }
//    }
    // ======== Serialize "FanBSpeed" ========
    if (const auto &item = message.GetMessageItem(kFanBSpeed)) {
      if (const auto &retVal = item.value()->GetValue<::Types::Value<double>>()) {
        auto current = static_cast<int16_t>(retVal.value().GetValue());
        FbisMessageBase::Serialize(out.get(), current);
      }
    }
    // ======== Serialize "FanA Operating Hours" ========
    if (const auto &item = message.GetMessageItem(kFanAOperatingHours)) {
      if (const auto &retVal = item.value()->GetValue<std::chrono::minutes>()) {
        auto operationTime = static_cast<uint32_t>(retVal.value().count() / 15);  // Cast to 15 min!
        FbisMessageBase::Serialize(out.get(), operationTime);
      }
    }
    // ======== Serialize "FanM Operating Hours" ========
    FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(0x00000000));
//    if (const auto &item = message.GetMessageItem(kFanMOperatingHours)) {
//      if (const auto &retVal = item.value()->GetValue<std::chrono::minutes>()) {
//        auto operationTime = static_cast<uint32_t>(retVal.value().count() / 15);  // Cast to 15 min!
//        FbisMessageBase::Serialize(out.get(), operationTime);
//      }
//    }
    // ======== Serialize "FanB Operating Hours" ========
    if (const auto &item = message.GetMessageItem(kFanBOperatingHours)) {
      if (const auto &retVal = item.value()->GetValue<std::chrono::minutes>()) {
        auto operationTime = static_cast<uint32_t>(retVal.value().count() / 15);  // Cast to 15 min!
        FbisMessageBase::Serialize(out.get(), operationTime);
      }
    }
  }
  return std::move(out);
}

}  // namespace Communication::Protocols::FbisNetwork
