/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Types/Value.h"
#include "Types/ModuleType.h"
#include "Messages/Types/MessageItemState.h"
#include "Messages/Types/DataStatus.h"
#include "Messages/Visitor/VisitorPropertyGet.h"
#include "Messages/MessageItemProperty.h"
#include "Messages/MessageTemplate.h"
#include "BUMCStatusRead.h"

namespace Communication::Protocols::FbisNetwork {

const char BUMCStatusRead::kBoardName[] = "BoardName";
const char BUMCStatusRead::kMezzanineCardPresent[] = "MezzanineCardPresent";
const char BUMCStatusRead::kMezzanineCardEnable[] = "MezzanineCardEnable";
const char BUMCStatusRead::kMezzanineCardReset[] = "MezzanineCardReset";
const char BUMCStatusRead::kDriverEnable[] = "DriverEnable";
const char BUMCStatusRead::kDriverReset[] = "DriverReset";
const char BUMCStatusRead::kOperatingHours[] = "OperatingHours";
const char BUMCStatusRead::kRwReg1[] = "RwReg1";
const char BUMCStatusRead::kRwReg2[] = "RwReg2";
const char BUMCStatusRead::kRwReg3[] = "RwReg3";
const char BUMCStatusRead::kStatReg1[] = "StatReg1";
const char BUMCStatusRead::kStatReg2[] = "StatReg2";
const char BUMCStatusRead::kStatReg3[] = "StatReg3";
const char BUMCStatusRead::kStatReg4[] = "StatReg4";
const char BUMCStatusRead::kStatReg5[] = "StatReg5";
const char BUMCStatusRead::kStatReg6[] = "StatReg6";

std::experimental::optional<std::unique_ptr<Messages::MessageTemplate<Messages::MessageItemProperty>>>
BUMCStatusRead::Deserialize(std::unique_ptr<std::vector<char>> input) {
  // Create message
  auto message = std::make_unique<Messages::MessageTemplate<Messages::MessageItemProperty>>();
  // Set request id
  message->SetRequestId(Communication::Types::RequestId::Enum::SCU_BUMC_STATUS_READ);
  // Check message length
  auto length = (*input).size();
  if (length != 6) {  // outer frame 5 chars, inner frame 1 char
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_REQUEST_FORMAT);
    return std::move(message);
  }
  // Decode slot
  auto slot = static_cast<uint8_t>((*input)[kFirstParam]);
  if (slot < kSlotMin || slot > kSlotMax) {
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::SLOT_NOT_VALID);
    return std::move(message);
  }
  // Add mezzanine card board name
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kBoardName,
      std::string("MC").append(std::to_string(slot)).append(
        "/Management/EEProm/BoardName"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add mezzanine card present item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kMezzanineCardPresent,
      std::string("MC").append(std::to_string(slot)).append(
        "/CardDetectionItems/MezzanineCardPresent"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add mezzanine card power enable item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kMezzanineCardEnable,
      std::string("MC").append(std::to_string(slot)).append(
        "/CardDetectionItems/MezzanineCardEnable"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add mezzanine card reset item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kMezzanineCardReset,
      std::string("MC").append(std::to_string(slot)).append(
        "/CardDetectionItems/MezzanineCardReset"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add driver enable item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kDriverEnable,
      std::string("MC").append(std::to_string(slot)).append(
        "/CardDetectionItems/DriverEnable"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT_WRITE)));
  // Add driver reset item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kDriverReset,
      std::string("MC").append(std::to_string(slot)).append(
        "/CardDetectionItems/DriverReset"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT_WRITE)));
  // Add item operating hours
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kOperatingHours,
      std::string("MC").append(std::to_string(slot)).append(
        "/Management/EEProm/OperatingHours"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kRwReg1,
      std::string("MC").append(std::to_string(slot)).append(
        "/Application/MCDriver/").append(kRwReg1),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
          kRwReg2,
          std::string("MC").append(std::to_string(slot)).append(
              "/Application/MCDriver/").append(kRwReg2),
          std::make_unique<Messages::Visitor::VisitorPropertyGet>(
              Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
          kRwReg3,
          std::string("MC").append(std::to_string(slot)).append(
              "/Application/MCDriver/").append(kRwReg3),
          std::make_unique<Messages::Visitor::VisitorPropertyGet>(
              Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add MC Driver input signal override collection
  message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
          kStatReg1,
          std::string("MC").append(std::to_string(slot)).append(
              "/Application/MCDriver/").append(kStatReg1),
          std::make_unique<Messages::Visitor::VisitorPropertyGet>(
              Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
          kStatReg2,
          std::string("MC").append(std::to_string(slot)).append(
              "/Application/MCDriver/").append(kStatReg2),
          std::make_unique<Messages::Visitor::VisitorPropertyGet>(
              Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
          kStatReg3,
          std::string("MC").append(std::to_string(slot)).append(
              "/Application/MCDriver/").append(kStatReg3),
          std::make_unique<Messages::Visitor::VisitorPropertyGet>(
              Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
          kStatReg4,
          std::string("MC").append(std::to_string(slot)).append(
              "/Application/MCDriver/").append(kStatReg4),
          std::make_unique<Messages::Visitor::VisitorPropertyGet>(
              Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
          kStatReg5,
          std::string("MC").append(std::to_string(slot)).append(
              "/Application/MCDriver/").append(kStatReg5),
          std::make_unique<Messages::Visitor::VisitorPropertyGet>(
              Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
          kStatReg6,
          std::string("MC").append(std::to_string(slot)).append(
              "/Application/MCDriver/").append(kStatReg6),
          std::make_unique<Messages::Visitor::VisitorPropertyGet>(
              Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Return message
  return std::move(message);
}

std::experimental::optional<std::unique_ptr<std::vector<char>>> BUMCStatusRead::Serialize(
  const Messages::MessageTemplate<Messages::MessageItemProperty> &message) {
  // Prepare out vector
  auto out = std::make_unique<std::vector<char>>();
  auto dataStatus = message.GetDataStatus();
  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    for (const auto &item : message.GetMessageItems()) {
      if (item->ItemState()->GetValue() != Messages::Types::MessageItemState::Enum::OK) {
        dataStatus = Messages::Types::DataStatus::Enum::INFORMATION_NOT_AVAILABLE;
        break;
      }
    }
  }
  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    // ======== Check MC ========
    if (const auto &item = message.GetMessageItem(kMezzanineCardPresent)) {
      if (!item.value()->GetValue<bool>().value()) {
        dataStatus = Messages::Types::DataStatus::Enum::MC_NOT_PRESENT;
      }
    }
  }
  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    if (const auto &item = message.GetMessageItem(kBoardName)) {
      if (const auto &retVal = item.value()->GetValue<::Types::ModuleType>()) {
        if (retVal.value().GetEnum() != ::Types::ModuleType::Enum::SCU_BUMC) {
          dataStatus = Messages::Types::DataStatus::Enum::WRONG_MEZZANINE_CARD;
        }
      } else {
        dataStatus = Messages::Types::DataStatus::Enum::INFORMATION_NOT_AVAILABLE;
      }
    }
  }

  // ======== Serialize "Data Status" ========
  FbisMessageBase::Serialize(out.get(), static_cast<uint8_t>(dataStatus));

  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    // ======== Serialize "Platform Control Signal Status" ========
    // Parse platform control signal status
    uint8_t platformControlSignalStatus = 0x00;
    // Serialize mezzanine card present item
    if (const auto &item = message.GetMessageItem(kMezzanineCardPresent)) {
      if (const auto &retVal = item.value()->GetValue<bool>()) {
        if (retVal.value()) {
          platformControlSignalStatus |= 0x01;
        }
      }
    }
    // Serialize mezzanine card power enable item
    if (const auto &item = message.GetMessageItem(kMezzanineCardEnable)) {
      if (const auto &retVal = item.value()->GetValue<bool>()) {
        if (retVal.value()) {
          platformControlSignalStatus |= 0x02;
        }
      }
    }
    // Serialize mezzanine card reset item
    if (const auto &item = message.GetMessageItem(kMezzanineCardReset)) {
      if (const auto &retVal = item.value()->GetValue<bool>()) {
        if (retVal.value()) {
          platformControlSignalStatus |= 0x04;
        }
      }
    }
    FbisMessageBase::Serialize(out.get(), platformControlSignalStatus);
    // ======== Serialize "MC_Driver Status" ========
    // Parse driver status
    uint8_t driverStatus = 0x00;
    // Serialize driver enable item
    if (const auto &item = message.GetMessageItem(kDriverEnable)) {
      if (const auto &retVal = item.value()->GetValue<bool>()) {
        if (retVal.value()) {
          driverStatus |= 0x01;
        }
      }
    }
    // Serialize driver reset item
    if (const auto &item = message.GetMessageItem(kDriverReset)) {
      if (const auto &retVal = item.value()->GetValue<bool>()) {
        if (!retVal.value()) {
          driverStatus |= 0x02;
        }
      }
    }
    FbisMessageBase::Serialize(out.get(), driverStatus);
    // ======== Serialize "MC Operating Hours" ========
    if (const auto &item = message.GetMessageItem(kOperatingHours)) {
      if (const auto &retVal = item.value()->GetValue<std::chrono::minutes>()) {
        auto hours = static_cast<uint32_t>(retVal.value().count() / 15);
        FbisMessageBase::Serialize(out.get(), hours);
      }
    }
    // ======== Serialize "Overriding of BUMC Input Signals" ========
    if (const auto &item = message.GetMessageItem(kRwReg1)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        auto reg = static_cast<uint32_t>(retVal.value());
        FbisMessageBase::Serialize(out.get(), reg);
      }
    }
    if (const auto &item = message.GetMessageItem(kRwReg2)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        auto reg = static_cast<uint32_t>(retVal.value());
        FbisMessageBase::Serialize(out.get(), reg);
      }
    }
    if (const auto &item = message.GetMessageItem(kRwReg3)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        auto reg = static_cast<uint32_t>(retVal.value());
        FbisMessageBase::Serialize(out.get(), reg);
      }
    }
    if (const auto &item = message.GetMessageItem(kStatReg1)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        auto reg = static_cast<uint32_t>(retVal.value());
        FbisMessageBase::Serialize(out.get(), reg);
      }
    }
    if (const auto &item = message.GetMessageItem(kStatReg2)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        auto reg = static_cast<uint32_t>(retVal.value());
        FbisMessageBase::Serialize(out.get(), reg);
      }
    }
    if (const auto &item = message.GetMessageItem(kStatReg3)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        auto reg = static_cast<uint32_t>(retVal.value());
        FbisMessageBase::Serialize(out.get(), reg);
      }
    }
    if (const auto &item = message.GetMessageItem(kStatReg4)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        auto reg = static_cast<uint32_t>(retVal.value());
        FbisMessageBase::Serialize(out.get(), reg);
      }
    }
    if (const auto &item = message.GetMessageItem(kStatReg5)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        auto reg = static_cast<uint32_t>(retVal.value());
        FbisMessageBase::Serialize(out.get(), reg);
      }
    }
    if (const auto &item = message.GetMessageItem(kStatReg6)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        auto reg = static_cast<uint32_t>(retVal.value());
        FbisMessageBase::Serialize(out.get(), reg);
      }
    }
  }
  return std::move(out);
}

}  // namespace Communication::Protocols::FbisNetwork
