/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <experimental/optional>
#include <memory>
#include "FbisMessageBase.h"

namespace Messages {
template<typename T>
class MessageTemplate;

class MessageItemProperty;
}  // namespace Messages
namespace Communication::Protocols::FbisNetwork {
class CLMCStatusRead : public FbisMessageBase {
 public:
  static std::experimental::optional<std::unique_ptr<Messages::MessageTemplate<Messages::MessageItemProperty>>>
  Deserialize(std::unique_ptr<std::vector<char>> input);

  static std::experimental::optional<std::unique_ptr<std::vector<char>>> Serialize(
    const Messages::MessageTemplate<Messages::MessageItemProperty> &message);

 private:
  static const int32_t kCurrentScale;
  static const int32_t kVoltageScale;

  static const char kBoardName[];
  static const char kMezzanineCardPresent[];
  static const char kMezzanineCardEnable[];
  static const char kMezzanineCardReset[];
  static const char kDriverEnable[];
  static const char kDriverReset[];
  static const char kOperatingHours[];
  static const char kMCDriverOverride[];
  static const char kMCDriverStatus[];
  static const char kLiveSignCounter[];
  static const char kPBDA[];
  static const char kPBDB[];
  static const char kPBMA[];
  static const char kPBMB[];
  static const char kSenderId[];
  static const char kDatalinkDebugInfo[];
  static const char kBeamPermitCurrent[];
  static const char kReadyCurrent[];
  static const char kBeamPermitAndReadyVoltage[];
  static const char kRedundantBeamPermitCurrent[];
  static const char kRedundantReadyCurrent[];
  static const char kRedundantBeamPermitAndReadyVoltage[];
  static const char kMainCurrent[];
  static const char kInternalMainVoltage[];
  static const char kInternalStandbyVoltage[];
  static const char kInternal3V3Voltage[];
  static const char kInternal1V8Voltage[];
};
}  // namespace Communication::Protocols::FbisNetwork
