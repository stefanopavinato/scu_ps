/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Types/Value.h"
#include "Types/ModuleType.h"
#include "Messages/Types/MessageItemState.h"
#include "Messages/Types/DataStatus.h"
#include "Messages/Visitor/VisitorPropertyGet.h"
#include "Messages/MessageItemProperty.h"
#include "Messages/MessageTemplate.h"
#include "SERVersionRead.h"

namespace Communication::Protocols::FbisNetwork {

const char SERVersionRead::kPSVersion[] = "PSVersion";
const char SERVersionRead::kPSBranch[] = "PSBranch";
const char SERVersionRead::kPSBuildTimeStamp[] = "PSBuildTimeStamp";
const char SERVersionRead::kPSIsDirty[] = "PSIsDirty";
const char SERVersionRead::kPTVersion[] = "PTVersion";
const char SERVersionRead::kPTBranch[] = "PTBranch";
const char SERVersionRead::kPTBuildTimeStamp[] = "PTBuildTimeStamp";
const char SERVersionRead::kPTIsDirty[] = "PTIsDirty";
const char SERVersionRead::kPLBuildVersion[] = "PLBuildVersion";
const char SERVersionRead::kPLLocalId[] = "PLLocalId";
const char SERVersionRead::kPLBuildTimestamp[] = "PLBuildTimestamp";

std::experimental::optional<std::unique_ptr<Messages::MessageTemplate<Messages::MessageItemProperty>>>
SERVersionRead::Deserialize(std::unique_ptr<std::vector<char>> input) {
  // Create message
  auto message = std::make_unique<Messages::MessageTemplate<Messages::MessageItemProperty>>();
  // Set request id
  message->SetRequestId(Communication::Types::RequestId::Enum::SCU_SER_VERSION_READ);
  // Check message length
  auto length = (*input).size();
  if (length != 5) {  // outer frame 5 chars, inner frame 0 char
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_REQUEST_FORMAT);
    return std::move(message);
  }
  // Add PS Version item
  message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
          kPSVersion,
          std::string("Serializer").append("/Serializer/Software/Application/Version"),
          std::make_unique<Messages::Visitor::VisitorPropertyGet>(
              Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add PSBranch item
  message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
          kPSBranch,
          std::string("Serializer").append("/Serializer/Software/Application/Branch"),
          std::make_unique<Messages::Visitor::VisitorPropertyGet>(
              Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add PSBuildTimeStamp item
  message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
          kPSBuildTimeStamp,
          std::string("Serializer").append("/Serializer/Software/Application/Timestamp"),
          std::make_unique<Messages::Visitor::VisitorPropertyGet>(
              Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add PSIsDirty item
  message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
          kPSIsDirty,
          std::string("Serializer").append("/Serializer/Software/Application/IsDirty"),
          std::make_unique<Messages::Visitor::VisitorPropertyGet>(
              Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add PTVersion item
  message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
          kPTVersion,
          std::string("Serializer").append("/Serializer/Software/Platform/Version"),
          std::make_unique<Messages::Visitor::VisitorPropertyGet>(
              Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add PTBranch item
  message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
          kPTBranch,
          std::string("Serializer").append("/Serializer/Software/Platform/Branch"),
          std::make_unique<Messages::Visitor::VisitorPropertyGet>(
              Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add PTBuildTimeStamp item
  message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
          kPTBuildTimeStamp,
          std::string("Serializer").append("/Serializer/Software/Platform/Timestamp"),
          std::make_unique<Messages::Visitor::VisitorPropertyGet>(
              Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add kPTPTIsDirty item
  message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
          kPTIsDirty,
          std::string("Serializer").append("/Serializer/Software/Platform/IsDirty"),
          std::make_unique<Messages::Visitor::VisitorPropertyGet>(
              Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add PLBuildVersion item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kPLBuildVersion,
      std::string("Serializer").append("/Serializer/PLBuildVersion"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add PLLocalId item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kPLLocalId,
      std::string("Serializer").append("/Serializer/PLLocalID"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add PLBuildTimestamp item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kPLBuildTimestamp,
      std::string("Serializer").append("/Serializer/PLBuildTimestamp"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Return message
  return std::move(message);
}

std::experimental::optional<std::unique_ptr<std::vector<char>>> SERVersionRead::Serialize(
  const Messages::MessageTemplate<Messages::MessageItemProperty> &message) {
  // Prepare out vector
  auto out = std::make_unique<std::vector<char>>();
  auto dataStatus = message.GetDataStatus();
  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    for (const auto &item : message.GetMessageItems()) {
      if (item->ItemState()->GetValue() != Messages::Types::MessageItemState::Enum::OK) {
        dataStatus = Messages::Types::DataStatus::Enum::INFORMATION_NOT_AVAILABLE;
        break;
      }
    }
  }

  // ======== Serialize "Data Status" ========
  FbisMessageBase::Serialize(out.get(), static_cast<uint8_t>(dataStatus));

  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    // ======== Serialize "PSVersion" ========
    if (const auto &item = message.GetMessageItem(kPSVersion)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        auto version = static_cast<uint32_t>(retVal.value());
        FbisMessageBase::Serialize(out.get(), version);
      }
    }
    // Serialize kPSBranch
    if (const auto &item = message.GetMessageItem(kPSBranch)) {
      if (const auto &retVal = item.value()->GetValue<std::string>()) {
        std::string strValue = retVal.value();
        for (char &c : strValue) {
          out->push_back(c);
        }
        for (std::string::size_type i = 0; i < (32 - strValue.length()); i++) {
          out->push_back(0);
        }
      }
    }
    // ======== Serialize "PSBuildTimeStamp" ========
    if (const auto &item = message.GetMessageItem(kPSBuildTimeStamp)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        auto buildTimestamp = static_cast<uint32_t>(retVal.value());
        FbisMessageBase::Serialize(out.get(), buildTimestamp);
      }
    }
    // ======== Serialize "kPSIsDirty" ========
    if (const auto &item = message.GetMessageItem(kPSIsDirty)) {
      if (const auto &retVal = item.value()->GetValue<bool>()) {
        auto val = retVal.value();
        if (val) {
          FbisMessageBase::Serialize(out.get(), static_cast<uint8_t>(1));
        } else {
          FbisMessageBase::Serialize(out.get(), static_cast<uint8_t>(0));
        }
      }
    }
    // ======== Serialize "PTVersion" ========
    if (const auto &item = message.GetMessageItem(kPTVersion)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        auto version = static_cast<uint32_t>(retVal.value());
        FbisMessageBase::Serialize(out.get(), version);
      }
    }
    // Serialize kPTBranch
    if (const auto &item = message.GetMessageItem(kPTBranch)) {
      if (const auto &retVal = item.value()->GetValue<std::string>()) {
        std::string strValue = retVal.value();
        for (char &c : strValue) {
          out->push_back(c);
        }
        for (std::string::size_type i = 0; i < (32 - strValue.length()); i++) {
          out->push_back(0);
        }
      }
    }
    // ======== Serialize "PTBuildTimeStamp" ========
    if (const auto &item = message.GetMessageItem(kPTBuildTimeStamp)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        auto buildTimestamp = static_cast<uint32_t>(retVal.value());
        FbisMessageBase::Serialize(out.get(), buildTimestamp);
      }
    }
    // ======== Serialize "kPTIsDirty" ========
    if (const auto &item = message.GetMessageItem(kPTIsDirty)) {
      if (const auto &retVal = item.value()->GetValue<bool>()) {
        auto val = retVal.value();
        if (val) {
          FbisMessageBase::Serialize(out.get(), static_cast<uint8_t>(1));
        } else {
          FbisMessageBase::Serialize(out.get(), static_cast<uint8_t>(0));
        }
      }
    }

    // ======== Serialize "plBuildVersion" ========
    if (const auto &item = message.GetMessageItem(kPLBuildVersion)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        auto plBuildVersion = static_cast<uint32_t>(retVal.value());
        FbisMessageBase::Serialize(out.get(), plBuildVersion);
      } else {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(0xDEADBEEF));
      }
    }

    // ======== Serialize "PLBuildTimestamp" ========
    if (const auto &item = message.GetMessageItem(kPLLocalId)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        auto plBuildTimestamp = static_cast<uint32_t>(retVal.value());
        FbisMessageBase::Serialize(out.get(), plBuildTimestamp);
      } else {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(0xDEADBEEF));
      }
    }

    // ======== Serialize "PLBuildTimestamp" ========
    if (const auto &item = message.GetMessageItem(kPLBuildTimestamp)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        auto plBuildTimestamp = static_cast<uint32_t>(retVal.value());
        FbisMessageBase::Serialize(out.get(), plBuildTimestamp);
      } else {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(0xDEADBEEF));
      }
    }
  }
  return std::move(out);
}

}  // namespace Communication::Protocols::FbisNetwork
