/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Types/Value.h"
#include "Types/ModuleType.h"
#include "Messages/Types/MessageItemState.h"
#include "Messages/Types/DataStatus.h"
#include "Messages/Visitor/VisitorPropertyGet.h"
#include "Messages/MessageItemProperty.h"
#include "Messages/MessageTemplate.h"
#include "OplTransceiverStatusRead.h"

namespace Communication::Protocols::FbisNetwork {

const uint32_t OplTransceiverStatusRead::kTransceiverIndexMin = 1;
const uint32_t OplTransceiverStatusRead::kTransceiverIndexMax = 2;

const char OplTransceiverStatusRead::kStatus[] = "Status";
const char OplTransceiverStatusRead::kSoftErrorCounter[] = "SoftErrorCounter";
const char OplTransceiverStatusRead::kHardErrorCounter[] = "HardErrorCounter";
const char OplTransceiverStatusRead::kChannelDownCounter[] = "ChannelDownCounter";
const char OplTransceiverStatusRead::kLaneDownCounter[] = "LaneDownCounter";

std::experimental::optional<std::unique_ptr<Messages::MessageTemplate<Messages::MessageItemProperty>>>
OplTransceiverStatusRead::Deserialize(std::unique_ptr<std::vector<char>> input) {
  // Create message
  auto message = std::make_unique<Messages::MessageTemplate<Messages::MessageItemProperty>>();
  // Set request id
  message->SetRequestId(Communication::Types::RequestId::Enum::SCU_SCU_OPL_TRANSCEIVER_STATUS_READ);
  // Check message length
  auto length = (*input).size();
  if (length != 6) {  // outer frame 5 chars, inner frame 1 char
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_REQUEST_FORMAT);
    return std::move(message);
  }
  // Decode transceiverIndex
  auto transceiverIndex = static_cast<uint8_t>((*input)[kFirstParam]);
  if (transceiverIndex < kTransceiverIndexMin || transceiverIndex > kTransceiverIndexMax) {
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::SLOT_NOT_VALID);
    return std::move(message);
  }

  auto transceiverName = std::string("Transceiver");
  switch (transceiverIndex) {
    case 1:
      transceiverName.append("A");
      break;
    case 2:
      transceiverName.append("B");
      break;
    default:
      message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::SLOT_NOT_VALID);
      return std::move(message);
  }
  // Add status signals
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
        kStatus,
      std::string("/Serializer/Serializer/OplInterface/")
         .append(transceiverName).append("/").append(kStatus),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
        kSoftErrorCounter,
      std::string("/Serializer/Serializer/OplInterface/")
          .append(transceiverName).append("/").append(kSoftErrorCounter),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
        kHardErrorCounter,
      std::string("/Serializer/Serializer/OplInterface/")
          .append(transceiverName).append("/").append(kHardErrorCounter),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
        kChannelDownCounter,
      std::string("/Serializer/Serializer/OplInterface/")
          .append(transceiverName).append("/").append(kChannelDownCounter),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
        kLaneDownCounter,
      std::string("/Serializer/Serializer/OplInterface/")
          .append(transceiverName).append("/").append(kLaneDownCounter),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Return message
  return std::move(message);
}

std::experimental::optional<std::unique_ptr<std::vector<char>>> OplTransceiverStatusRead::Serialize(
  const Messages::MessageTemplate<Messages::MessageItemProperty> &message) {
  // Prepare out vector
  auto out = std::make_unique<std::vector<char>>();
  auto dataStatus = message.GetDataStatus();
  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    for (const auto &item : message.GetMessageItems()) {
      if (item->ItemState()->GetValue() != Messages::Types::MessageItemState::Enum::OK) {
        dataStatus = Messages::Types::DataStatus::Enum::INFORMATION_NOT_AVAILABLE;
        break;
      }
    }
  }

  // ======== Serialize "Data Status" ========
  FbisMessageBase::Serialize(out.get(), static_cast<uint8_t>(dataStatus));

  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    // Serialize Transceiver status
    if (const auto &item = message.GetMessageItem(kStatus)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint8_t>(retVal.value()));
      }
    }
    // Serialize Soft Error Counter
    if (const auto &item = message.GetMessageItem(kSoftErrorCounter)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // Serialize Hard Error Counter
    if (const auto &item = message.GetMessageItem(kHardErrorCounter)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // Serialize Channel Down Counter
    if (const auto &item = message.GetMessageItem(kChannelDownCounter)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // Serialize Lane Down Counter
    if (const auto &item = message.GetMessageItem(kLaneDownCounter)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
  }
  return std::move(out);
}
}  // namespace Communication::Protocols::FbisNetwork
