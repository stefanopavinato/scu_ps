/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Types/Value.h"
#include "Types/VariantValue.h"
#include "Messages/Types/MessageItemState.h"
#include "Messages/Types/DataStatus.h"
#include "Messages/Visitor/VisitorPropertyGet.h"
#include "Messages/Visitor/VisitorPropertySet.h"
#include "Messages/MessageItemProperty.h"
#include "Messages/MessageTemplate.h"
#include "SWBeamSwitchOffRequestWrite.h"

namespace Communication::Protocols::FbisNetwork {

const int32_t SWBeamSwitchOffRequestWrite::kSignalIndexPosition = 3;
const int32_t SWBeamSwitchOffRequestWrite::kSignalValuePosition = 4;
const int32_t SWBeamSwitchOffRequestWrite::kSignalIndexMin = 1;
const int32_t SWBeamSwitchOffRequestWrite::kSignalIndexMax = 3;

std::experimental::optional<std::unique_ptr<Messages::MessageTemplate<Messages::MessageItemProperty>>>
SWBeamSwitchOffRequestWrite::Deserialize(std::unique_ptr<std::vector<char>> input) {
  // Create message
  auto message = std::make_unique<Messages::MessageTemplate<Messages::MessageItemProperty>>();
  // Set request id
  message->SetRequestId(Communication::Types::RequestId::Enum::SCU_BEAM_SWITCH_OFF_REQUEST_WRITE);
  // Check message length
  auto length = (*input).size();
  if (length != 7) {  // outer frame 5 chars, inner frame 2 char
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_REQUEST_FORMAT);
    return std::move(message);
  }
  // Decode index of signal to be overridden
  auto signalIndex = static_cast<uint8_t>((*input)[kSignalIndexPosition]);
  if (signalIndex < kSignalIndexMin || signalIndex > kSignalIndexMax) {
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_SIGNAL_INDEX);
    return std::move(message);
  }
  // Decode value of signal to be overridden and check if valid
  auto signalValueTest = static_cast<uint8_t>((*input)[kSignalValuePosition]);
  if (signalValueTest > 1) {
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_SIGNAL_VALUE);
    return std::move(message);
  }

  auto signalValue = static_cast<bool>((*input)[kSignalValuePosition]);
  std::string signalName;
  switch (signalIndex) {
    case 1: {
      signalName = "SWBIRequest";
      break;
    }
    case 2: {
      signalName = "SWRBIRequest";
      break;
    }
    case 3: {
      signalName = "SWEBIRequest";
      break;
    }
    default: {
      message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_SIGNAL_INDEX);
      return std::move(message);
    }
  }
  if (signalValue) {
    // Cause switch-off request
    ::Types::TestVariantValue value;
    value.SetValue(static_cast<uint8_t>(0));
    message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
        "default-value",
        std::string("Serializer").append("/Serializer/").append(signalName),
        std::make_unique<Messages::Visitor::VisitorPropertySet>(
          Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT_FORCE,
          ::Types::ValueType::Enum::UINT8,
          value)));  // we need to write any value different to 0x9 to cause a SW BI/RBI/EBI switch-off request
  } else {
    // Clear switch-off request
    ::Types::TestVariantValue value;
    value.SetValue(static_cast<uint8_t>(9));
    message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
        "default-value",
        std::string("Serializer").append("/Serializer/").append(signalName),
        std::make_unique<Messages::Visitor::VisitorPropertySet>(
          Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT_FORCE_RELEASE,
          ::Types::ValueType::Enum::UINT8,
          value)));
  }
  return std::move(message);
}

std::experimental::optional<std::unique_ptr<std::vector<char>>> SWBeamSwitchOffRequestWrite::Serialize(
  const Messages::MessageTemplate<Messages::MessageItemProperty> &message) {
  // Prepare out vector
  auto out = std::make_unique<std::vector<char>>();
  auto dataStatus = message.GetDataStatus();
  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    for (const auto &item : message.GetMessageItems()) {
      if (item->ItemState()->GetValue() != Messages::Types::MessageItemState::Enum::OK) {
        dataStatus = Messages::Types::DataStatus::Enum::INFORMATION_NOT_AVAILABLE;
        break;
      }
    }
  }

  // ======== Serialize "Data Status" ========
  out->push_back(static_cast<char>(dataStatus));
  return std::move(out);
}

}  // namespace Communication::Protocols::FbisNetwork
