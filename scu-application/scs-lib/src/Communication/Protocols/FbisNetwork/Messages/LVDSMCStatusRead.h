/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <experimental/optional>
#include <memory>
#include "FbisMessageBase.h"

namespace Messages {
template<typename T>
class MessageTemplate;

class MessageItemProperty;
}  // namespace Messages
namespace Communication::Protocols::FbisNetwork {
class LVDSMCStatusRead : public FbisMessageBase {
 public:
  static std::experimental::optional<std::unique_ptr<Messages::MessageTemplate<Messages::MessageItemProperty>>>
  Deserialize(std::unique_ptr<std::vector<char>> input);

  static std::experimental::optional<std::unique_ptr<std::vector<char>>> Serialize(
    const Messages::MessageTemplate<Messages::MessageItemProperty> &message);

 private:
  static const int32_t kCurrentScale;
  static const int32_t kVoltageScale;

  static const char kBoardName[];
  static const char kMezzanineCardPresent[];
  static const char kMezzanineCardEnable[];
  static const char kMezzanineCardReset[];
  static const char kDriverEnable[];
  static const char kDriverReset[];
  static const char kOperatingHours[];
  static const char kSignalOverrideDiscretePort1[];
  static const char kSignalOverrideDiscretePort2[];
  static const char kSignalOverrideDiscretePort3[];
  static const char kSignalOverrideDatalinkPort1[];
  static const char kSignalOverrideDatalinkPort2[];
  static const char kSignalOverrideDatalinkPort3[];
  static const char kMCDriverStatus1[];
  static const char kMCDriverStatus2[];
  static const char kMCDriverStatus3[];
  static const char kSenderID1[];
  static const char kSenderID2[];
  static const char kSenderID3[];
  static const char kLiveSignCounter1[];
  static const char kLiveSignCounter2[];
  static const char kLiveSignCounter3[];
  static const char kMainCurrent[];
  static const char kInternalMainVoltage[];
  static const char kInternalStandbyVoltage[];
  static const char kInternal3V3Voltage[];
  static const char kInternal1V8Voltage[];
};
}  // namespace Communication::Protocols::FbisNetwork
