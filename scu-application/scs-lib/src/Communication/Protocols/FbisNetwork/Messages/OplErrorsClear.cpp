/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Types/Value.h"
#include "Types/VariantValue.h"
#include "Messages/Types/MessageItemState.h"
#include "Messages/Types/DataStatus.h"
#include "Messages/Visitor/VisitorPropertyGet.h"
#include "Messages/Visitor/VisitorPropertySet.h"
#include "Messages/MessageItemProperty.h"
#include "Messages/MessageTemplate.h"
#include "OplErrorsClear.h"

namespace Communication::Protocols::FbisNetwork {

const char OplErrorsClear::kOplErrorsClearName[] = "OplErrorsClear";

std::experimental::optional<std::unique_ptr<Messages::MessageTemplate<Messages::MessageItemProperty>>>
OplErrorsClear::Deserialize(std::unique_ptr<std::vector<char>> input) {
  // Create message
  auto message = std::make_unique<Messages::MessageTemplate<Messages::MessageItemProperty>>();
  // Set request id
  message->SetRequestId(Communication::Types::RequestId::Enum::SCU_SCU_OPL_ERRORS_CLEAR);
  // Check message length
  auto length = (*input).size();
  if (length != 5) {  // outer frame 5 chars, inner frame 0 char
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_REQUEST_FORMAT);
    return std::move(message);
  }
  // Cause switch-off request
  ::Types::TestVariantValue value;
  value.SetValue(true);
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      "default-value",
      std::string("Serializer").append("/Serializer/OplInterface/").append(kOplErrorsClearName),
      std::make_unique<Messages::Visitor::VisitorPropertySet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT_FORCE_ONCE,
        ::Types::ValueType::Enum::BOOL,
        value)));
  return std::move(message);
}

std::experimental::optional<std::unique_ptr<std::vector<char>>> OplErrorsClear::Serialize(
  const Messages::MessageTemplate<Messages::MessageItemProperty> &message) {
  // Prepare out vector
  auto out = std::make_unique<std::vector<char>>();
  auto dataStatus = message.GetDataStatus();
  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    for (const auto &item : message.GetMessageItems()) {
      if (item->ItemState()->GetValue() != Messages::Types::MessageItemState::Enum::OK) {
        dataStatus = Messages::Types::DataStatus::Enum::INFORMATION_NOT_AVAILABLE;
        break;
      }
    }
  }

  // ======== Serialize "Data Status" ========
  out->push_back(static_cast<char>(dataStatus));
  return std::move(out);
}

}  // namespace Communication::Protocols::FbisNetwork
