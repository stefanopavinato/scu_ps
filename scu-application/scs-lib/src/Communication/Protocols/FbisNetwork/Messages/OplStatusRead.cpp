/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Types/Value.h"
#include "Types/ModuleType.h"
#include "Messages/Types/MessageItemState.h"
#include "Messages/Types/DataStatus.h"
#include "Messages/Visitor/VisitorPropertyGet.h"
#include "Messages/MessageItemProperty.h"
#include "Messages/MessageTemplate.h"
#include "OplStatusRead.h"

namespace Communication::Protocols::FbisNetwork {

const uint32_t OplStatusRead::kTransceiverIndexMin = 1;
const uint32_t OplStatusRead::kTransceiverIndexMax = 2;

const char OplStatusRead::kOplStatusCollection[] = "OplStatusCollection";
const char OplStatusRead::kDSOplErrors[] = "DSOplErrors";
const char OplStatusRead::kUSOplErrors[] = "USOplErrors";
const char OplStatusRead::kDSOplEncodingSupervisionErrors[] = "DSOplEncodingSupervisionErrors";
const char OplStatusRead::kDSOplLifeSignErrors[] = "DSOplLifeSignErrors";
const char OplStatusRead::kDSOplRxFlagErrors[] = "DSOplRxFlagErrors";
const char OplStatusRead::kDSOplTxFrameDropped[] = "DSOplTxFrameDropped";
const char OplStatusRead::kDSOplRxFrameDropped[] = "DSOplRxFrameDropped";
const char OplStatusRead::kDSOplRxFrameErrors[] = "DSOplRxFrameErrors";
const char OplStatusRead::kDSOplNeighbourStateErrors[] = "DSOplNeighbourStateErrors";
const char OplStatusRead::kDSOplNeighbourAfterNextStateErrors[] = "DSOplNeighbourAfterNextStateErrors";
const char OplStatusRead::kUSOplEncodingSupervisionErrors[] = "USOplEncodingSupervisionErrors";
const char OplStatusRead::kUSOplLifeSignErrors[] = "USOplLifeSignErrors";
const char OplStatusRead::kUSOplRxFlagErrors[] = "USOplRxFlagErrors";
const char OplStatusRead::kUSOplTxFrameDropped[] = "USOplTxFrameDropped";
const char OplStatusRead::kUSOplRxFrameDropped[] = "USOplRxFrameDropped";
const char OplStatusRead::kUSOplRxFrameErrors[] = "USOplRxFrameErrors";
const char OplStatusRead::kUSOplNeighbourStateErrors[] = "USOplNeighbourStateErrors";
const char OplStatusRead::kUSOplNeighbourAfterNextStateErrors[] = "USOplNeighbourAfterNextStateErrors";
const char OplStatusRead::kDSOplErrorsMaxValue[] = "DSOplErrorsMaxValue";
const char OplStatusRead::kUSOplErrorsMaxValue[] = "USOplErrorsMaxValue";

std::experimental::optional<std::unique_ptr<Messages::MessageTemplate<Messages::MessageItemProperty>>>
OplStatusRead::Deserialize(std::unique_ptr<std::vector<char>> input) {
  // Create message
  auto message = std::make_unique<Messages::MessageTemplate<Messages::MessageItemProperty>>();
  // Set request id
  message->SetRequestId(Communication::Types::RequestId::Enum::SCU_SCU_OPL_STATUS_READ);
  // Check message length
  auto length = (*input).size();
  if (length != 5) {  // outer frame 5 chars, inner frame 0 char
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_REQUEST_FORMAT);
    return std::move(message);
  }
  // Add OplStatusCollection item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kOplStatusCollection,
      std::string("/Serializer/Serializer/OplInterface/").append(kOplStatusCollection),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add DSOplErrors item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kDSOplErrors,
      std::string("/Serializer/Serializer/OplInterface/").append(kDSOplErrors),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add USOplErrors item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kUSOplErrors,
      std::string("/Serializer/Serializer/OplInterface/").append(kUSOplErrors),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add DSOplEncodingSupervisionErrors item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kDSOplEncodingSupervisionErrors,
      std::string("/Serializer/Serializer/OplInterface/").append(kDSOplEncodingSupervisionErrors),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add DSOplLifeSignErrors item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kDSOplLifeSignErrors,
      std::string("/Serializer/Serializer/OplInterface/").append(kDSOplLifeSignErrors),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add DSOplRxFlagErrors item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kDSOplRxFlagErrors,
      std::string("/Serializer/Serializer/OplInterface/").append(kDSOplRxFlagErrors),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add DSOplTxFrameDropped item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kDSOplTxFrameDropped,
      std::string("/Serializer/Serializer/OplInterface/").append(kDSOplTxFrameDropped),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add DSOplRxFrameDropped item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kDSOplRxFrameDropped,
      std::string("/Serializer/Serializer/OplInterface/").append(kDSOplRxFrameDropped),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add DSOplRxFrameErrors item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kDSOplRxFrameErrors,
      std::string("/Serializer/Serializer/OplInterface/").append(kDSOplRxFrameErrors),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add DSOplNeighbourStateErrors item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kDSOplNeighbourStateErrors,
      std::string("/Serializer/Serializer/OplInterface/").append(kDSOplNeighbourStateErrors),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add DSOplNeighbourAfterNextStateErrors item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kDSOplNeighbourAfterNextStateErrors,
      std::string("/Serializer/Serializer/OplInterface/").append(kDSOplNeighbourAfterNextStateErrors),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add USOplEncodingSupervisionErrors item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kUSOplEncodingSupervisionErrors,
      std::string("/Serializer/Serializer/OplInterface/").append(kUSOplEncodingSupervisionErrors),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add USOplLifeSignErrors item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kUSOplLifeSignErrors,
      std::string("/Serializer/Serializer/OplInterface/").append(kUSOplLifeSignErrors),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add USOplRxFlagErrors item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kUSOplRxFlagErrors,
      std::string("/Serializer/Serializer/OplInterface/").append(kUSOplRxFlagErrors),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add USOplTxFrameDropped item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kUSOplTxFrameDropped,
      std::string("/Serializer/Serializer/OplInterface/").append(kUSOplTxFrameDropped),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add USOplRxFrameDropped item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kUSOplRxFrameDropped,
      std::string("/Serializer/Serializer/OplInterface/").append(kUSOplRxFrameDropped),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add USOplRxFrameErrors item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kUSOplRxFrameErrors,
      std::string("/Serializer/Serializer/OplInterface/").append(kUSOplRxFrameErrors),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add USOplNeighbourStateErrors item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kUSOplNeighbourStateErrors,
      std::string("/Serializer/Serializer/OplInterface/").append(kUSOplNeighbourStateErrors),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add USOplNeighbourAfterNextStateErrors item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kUSOplNeighbourAfterNextStateErrors,
      std::string("/Serializer/Serializer/OplInterface/").append(kUSOplNeighbourAfterNextStateErrors),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add DSOplErrorsMaxValue item
  message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
          kDSOplErrorsMaxValue,
          std::string("/Serializer/Serializer/OplInterface/").append(kDSOplErrorsMaxValue),
          std::make_unique<Messages::Visitor::VisitorPropertyGet>(
              Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add USOplErrorsMaxValue item
  message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
          kUSOplErrorsMaxValue,
          std::string("/Serializer/Serializer/OplInterface/").append(kUSOplErrorsMaxValue),
          std::make_unique<Messages::Visitor::VisitorPropertyGet>(
              Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Return message
  return std::move(message);
}

std::experimental::optional<std::unique_ptr<std::vector<char>>> OplStatusRead::Serialize(
  const Messages::MessageTemplate<Messages::MessageItemProperty> &message) {
  // Prepare out vector
  auto out = std::make_unique<std::vector<char>>();
  auto dataStatus = message.GetDataStatus();
  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    for (const auto &item : message.GetMessageItems()) {
      if (item->ItemState()->GetValue() != Messages::Types::MessageItemState::Enum::OK) {
        dataStatus = Messages::Types::DataStatus::Enum::INFORMATION_NOT_AVAILABLE;
        break;
      }
    }
  }

  // ======== Serialize "Data Status" ========
  FbisMessageBase::Serialize(out.get(), static_cast<uint8_t>(dataStatus));

  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    // ======== Serialize "kOplStatusCollection" ========
    if (const auto &item = message.GetMessageItem(kOplStatusCollection)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // ======== Serialize "kDSOplErrors" ========
    if (const auto &item = message.GetMessageItem(kDSOplErrors)) {
      if (const auto &retVal = item.value()->GetValue<uint16_t>()) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint16_t>(retVal.value()));
      }
    }
    // ======== Serialize "kUSOplErrors" ========
    if (const auto &item = message.GetMessageItem(kUSOplErrors)) {
      if (const auto &retVal = item.value()->GetValue<uint16_t>()) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint16_t>(retVal.value()));
      }
    }
    // ======== Serialize "kDSOplEncodingSupervisionErrors" ========
    if (const auto &item = message.GetMessageItem(kDSOplEncodingSupervisionErrors)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // ======== Serialize "kDSOplLifeSignErrors" ========
    if (const auto &item = message.GetMessageItem(kDSOplLifeSignErrors)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // ======== Serialize "kDSOplRxFlagErrors" ========
    if (const auto &item = message.GetMessageItem(kDSOplRxFlagErrors)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // ======== Serialize "kDSOplTxFrameDropped" ========
    if (const auto &item = message.GetMessageItem(kDSOplTxFrameDropped)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // ======== Serialize "kDSOplRxFrameDropped" ========
    if (const auto &item = message.GetMessageItem(kDSOplRxFrameDropped)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // ======== Serialize "kDSOplRxFrameErrors" ========
    if (const auto &item = message.GetMessageItem(kDSOplRxFrameErrors)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // ======== Serialize "kDSOplNeighbourStateErrors" ========
    if (const auto &item = message.GetMessageItem(kDSOplNeighbourStateErrors)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // ======== Serialize "kDSOplNeighbourAfterNextStateErrors" ========
    if (const auto &item = message.GetMessageItem(kDSOplNeighbourAfterNextStateErrors)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // ======== Serialize "kUSOplEncodingSupervisionErrors" ========
    if (const auto &item = message.GetMessageItem(kUSOplEncodingSupervisionErrors)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // ======== Serialize "kUSOplLifeSignErrors" ========
    if (const auto &item = message.GetMessageItem(kUSOplLifeSignErrors)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // ======== Serialize "kUSOplRxFlagErrors" ========
    if (const auto &item = message.GetMessageItem(kUSOplRxFlagErrors)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // ======== Serialize "kUSOplTxFrameDropped" ========
    if (const auto &item = message.GetMessageItem(kUSOplTxFrameDropped)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // ======== Serialize "kUSOplRxFrameDropped" ========
    if (const auto &item = message.GetMessageItem(kUSOplRxFrameDropped)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // ======== Serialize "kUSOplRxFrameErrors" ========
    if (const auto &item = message.GetMessageItem(kUSOplRxFrameErrors)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // ======== Serialize "kUSOplNeighbourStateErrors" ========
    if (const auto &item = message.GetMessageItem(kUSOplNeighbourStateErrors)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // ======== Serialize "kUSOplNeighbourAfterNextStateErrors" ========
    if (const auto &item = message.GetMessageItem(kUSOplNeighbourAfterNextStateErrors)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // ======== Serialize "kUSOplErrorsMaxValue" ========
    if (const auto &item = message.GetMessageItem(kUSOplErrorsMaxValue)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // ======== Serialize "kDSOplErrorsMaxValue" ========
    if (const auto &item = message.GetMessageItem(kDSOplErrorsMaxValue)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    return std::move(out);
  }
}
}  // namespace Communication::Protocols::FbisNetwork
