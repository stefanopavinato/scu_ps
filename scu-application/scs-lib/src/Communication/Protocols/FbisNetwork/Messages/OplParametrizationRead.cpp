/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Types/Value.h"
#include "Types/ModuleType.h"
#include "Messages/Types/MessageItemState.h"
#include "Messages/Types/DataStatus.h"
#include "Messages/Visitor/VisitorPropertyGet.h"
#include "Messages/MessageItemProperty.h"
#include "Messages/MessageTemplate.h"
#include "OplParametrizationRead.h"

namespace Communication::Protocols::FbisNetwork {

const char OplParametrizationRead::kOplModeRegister[] = "OplModeRegister";
const char OplParametrizationRead::kTransceiverRouteOplGtARegister[] = "TransceiverRouteOplGtARegister";
const char OplParametrizationRead::kIntermediateNode[] = "IntermediateNode";
const char OplParametrizationRead::kLeakyBucketMaxValueRegister[] = "LeakyBucketMaxValue";
const char OplParametrizationRead::kLeakyBucketThresholdRegister[] = "LeakyBucketThreshold";
const char OplParametrizationRead::kLeakyBucketIncrementValueRegister[] = "LeakyBucketIncrementValue";

std::experimental::optional<std::unique_ptr<Messages::MessageTemplate<Messages::MessageItemProperty>>>
OplParametrizationRead::Deserialize(std::unique_ptr<std::vector<char>> input) {
  // Create message
  auto message = std::make_unique<Messages::MessageTemplate<Messages::MessageItemProperty>>();
  // Set request id
  message->SetRequestId(Communication::Types::RequestId::Enum::SCU_SCU_OPL_PARAMETRIZATION_READ);
  // Check message length
  auto length = (*input).size();
  if (length != 5) {  // outer frame 5 chars, inner frame 0 char
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_REQUEST_FORMAT);
    return std::move(message);
  }
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
        kOplModeRegister,
      std::string("/Serializer/Serializer/OplInterface/").append(kOplModeRegister),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kIntermediateNode,
      std::string("/Serializer/Serializer/OplInterface/").append(kIntermediateNode),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
          kTransceiverRouteOplGtARegister,
          std::string("/Serializer/Serializer/OplInterface/").append(kTransceiverRouteOplGtARegister),
          std::make_unique<Messages::Visitor::VisitorPropertyGet>(
              Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
          kLeakyBucketMaxValueRegister,
          std::string("/Serializer/Serializer/OplInterface/").append(kLeakyBucketMaxValueRegister),
          std::make_unique<Messages::Visitor::VisitorPropertyGet>(
              Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
          kLeakyBucketThresholdRegister,
          std::string("/Serializer/Serializer/OplInterface/").append(kLeakyBucketThresholdRegister),
          std::make_unique<Messages::Visitor::VisitorPropertyGet>(
              Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
          kLeakyBucketIncrementValueRegister,
          std::string("/Serializer/Serializer/OplInterface/").append(kLeakyBucketIncrementValueRegister),
          std::make_unique<Messages::Visitor::VisitorPropertyGet>(
              Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Return message
  return std::move(message);
}

std::experimental::optional<std::unique_ptr<std::vector<char>>> OplParametrizationRead::Serialize(
  const Messages::MessageTemplate<Messages::MessageItemProperty> &message) {
  // Prepare out vector
  auto out = std::make_unique<std::vector<char>>();
  auto dataStatus = message.GetDataStatus();
  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    for (const auto &item : message.GetMessageItems()) {
      if (item->ItemState()->GetValue() != Messages::Types::MessageItemState::Enum::OK) {
        dataStatus = Messages::Types::DataStatus::Enum::INFORMATION_NOT_AVAILABLE;
        break;
      }
    }
  }

  // ======== Serialize "Data Status" ========
  FbisMessageBase::Serialize(out.get(), static_cast<uint8_t>(dataStatus));

  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    // Serialize OPL enabled
    if (const auto &item = message.GetMessageItem(kOplModeRegister)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint8_t>((retVal.value()) & 0x01));
      }
    }
    // Serialize OPL Mode (end-node, intermediate-node)
    if (const auto &item = message.GetMessageItem(kIntermediateNode)) {
      if (const auto &retVal = item.value()->GetValue<bool>()) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint8_t>((retVal.value()) & 0x01));
      }
    }
    // Serialize gt_lane
    if (const auto &item = message.GetMessageItem(kTransceiverRouteOplGtARegister)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint8_t>(retVal.value()));
      }
    }
    // Serialize LeakyBucket MaxValue
    if (const auto &item = message.GetMessageItem(kLeakyBucketMaxValueRegister)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // Serialize LeakyBucket IncrementValue
    if (const auto &item = message.GetMessageItem(kLeakyBucketIncrementValueRegister)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // Serialize LeakyBucket Threshold
    if (const auto &item = message.GetMessageItem(kLeakyBucketThresholdRegister)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
  }
  return std::move(out);
}
}  // namespace Communication::Protocols::FbisNetwork
