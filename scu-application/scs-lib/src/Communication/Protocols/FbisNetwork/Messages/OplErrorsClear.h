/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <experimental/optional>
#include <memory>
#include "FbisMessageBase.h"

namespace Messages {
template<typename T>
class MessageTemplate;

class MessageItemProperty;
}  // namespace Messages
namespace Communication::Protocols::FbisNetwork {
class OplErrorsClear : public FbisMessageBase {
 public:
  static std::experimental::optional<std::unique_ptr<Messages::MessageTemplate<Messages::MessageItemProperty>>>
  Deserialize(std::unique_ptr<std::vector<char>> input);

  static std::experimental::optional<std::unique_ptr<std::vector<char>>> Serialize(
    const Messages::MessageTemplate<Messages::MessageItemProperty> &message);

 private:
  static const char kOplErrorsClearName[];
};
}  // namespace Communication::Protocols::FbisNetwork
