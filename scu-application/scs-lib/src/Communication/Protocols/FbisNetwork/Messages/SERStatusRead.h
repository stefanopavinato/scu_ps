/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <experimental/optional>
#include <memory>
#include "FbisMessageBase.h"

namespace Messages {
template<typename T>
class MessageTemplate;

class MessageItemProperty;
}  // namespace Messages
namespace Communication::Protocols::FbisNetwork {
class SERStatusRead : public FbisMessageBase {
 public:
  static std::experimental::optional<std::unique_ptr<Messages::MessageTemplate<Messages::MessageItemProperty>>>
  Deserialize(std::unique_ptr<std::vector<char>> input);

  static std::experimental::optional<std::unique_ptr<std::vector<char>>> Serialize(
    const Messages::MessageTemplate<Messages::MessageItemProperty> &message);

 private:
  static const int32_t kCurrentScale;
  static const int32_t kVoltageScale;

  static const char kOperatingHours[];

  static const char kPresent[];
  static const char kTxFault[];
  static const char kRxLos[];
  static const char kTxDisable[];
  static const char kRateSelect0[];
  static const char kRateSelect1[];
  static const char kLinkState[];
  static const char kPgmClkStatusLockDetect[];
  static const char kPgmClkStatusHoldover[];
  static const char kPgmClkStatusClkIn0[];
  static const char kPgmClkStatusClkIn1[];
  static const char kPgmClkSync[];
  static const char kMainPowerSupplyPowerGood[];
  static const char kMainPowerSupplyNotAlert[];
  static const char kUSBPowerStatus[];
  static const char kVccInternal0V85NotAlert[];
  static const char kTemperatureSensorAlert[];
  static const char kHdcDrdyInterrupt[];

  static const char kHumidity[];
  static const char kTemperatureAtHumidity[];
  static const char kMainCurrent[];
  static const char kInternalMainVoltage[];
  static const char kInternalStandbyVoltage[];
  static const char kInternal3V3Voltage[];
  static const char kInternal1V8Voltage[];
};
}  // namespace Communication::Protocols::FbisNetwork
