/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Types/Value.h"
#include "Types/ModuleType.h"
#include "Messages/Types/MessageItemState.h"
#include "Messages/Types/DataStatus.h"
#include "Messages/Visitor/VisitorPropertyGet.h"
#include "Messages/MessageItemProperty.h"
#include "Messages/MessageTemplate.h"
#include "CLMCStatusRead.h"

namespace Communication::Protocols::FbisNetwork {

const char CLMCStatusRead::kBoardName[] = "BoardName";
const char CLMCStatusRead::kMezzanineCardPresent[] = "MezzanineCardPresent";
const char CLMCStatusRead::kMezzanineCardEnable[] = "MezzanineCardEnable";
const char CLMCStatusRead::kMezzanineCardReset[] = "MezzanineCardReset";
const char CLMCStatusRead::kDriverEnable[] = "DriverEnable";
const char CLMCStatusRead::kDriverReset[] = "DriverReset";
const char CLMCStatusRead::kOperatingHours[] = "OperatingHours";
const char CLMCStatusRead::kMCDriverOverride[] = "MCDriverOverride";
const char CLMCStatusRead::kMCDriverStatus[] = "MCDriverStatus";
const char CLMCStatusRead::kLiveSignCounter[] = "LiveSignCounter";
const char CLMCStatusRead::kPBDA[] = "PBDA";
const char CLMCStatusRead::kPBDB[] = "PBDB";
const char CLMCStatusRead::kPBMA[] = "PBMA";
const char CLMCStatusRead::kPBMB[] = "PBMB";
const char CLMCStatusRead::kSenderId[] = "SenderId";
const char CLMCStatusRead::kDatalinkDebugInfo[] = "DatalinkDebugInfo";
const char CLMCStatusRead::kBeamPermitCurrent[] = "BeamPermitCurrent";
const char CLMCStatusRead::kReadyCurrent[] = "ReadyCurrent";
const char CLMCStatusRead::kBeamPermitAndReadyVoltage[] = "BeamPermitAndReadyVoltage";
const char CLMCStatusRead::kRedundantBeamPermitCurrent[] = "RedundantBeamPermitCurrent";
const char CLMCStatusRead::kRedundantReadyCurrent[] = "RedundantReadyCurrent";
const char CLMCStatusRead::kRedundantBeamPermitAndReadyVoltage[] = "RedundantBeamPermitAndReadyVoltage";
const char CLMCStatusRead::kMainCurrent[] = "MainCurrent";
const char CLMCStatusRead::kInternalMainVoltage[] = "InternalMainVoltage";
const char CLMCStatusRead::kInternalStandbyVoltage[] = "InternalStandbyVoltage";
const char CLMCStatusRead::kInternal3V3Voltage[] = "Internal3V3Voltage";
const char CLMCStatusRead::kInternal1V8Voltage[] = "Internal1V8Voltage";

const int32_t CLMCStatusRead::kCurrentScale = 50;
const int32_t CLMCStatusRead::kVoltageScale = 200;

std::experimental::optional<std::unique_ptr<Messages::MessageTemplate<Messages::MessageItemProperty>>>
CLMCStatusRead::Deserialize(std::unique_ptr<std::vector<char>> input) {
  // Create message
  auto message = std::make_unique<Messages::MessageTemplate<Messages::MessageItemProperty>>();
  // Set request id
  message->SetRequestId(Communication::Types::RequestId::Enum::SCU_CLMC_STATUS_READ);
  // Check message length
  auto length = (*input).size();
  if (length != 6) {  // outer frame 5 chars, inner frame 1 char
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_REQUEST_FORMAT);
    return std::move(message);
  }
  // Decode slot
  auto slot = static_cast<uint8_t>((*input)[kFirstParam]);
  if (slot < kSlotMin || slot > kSlotMax) {
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::SLOT_NOT_VALID);
    return std::move(message);
  }
  // Add mezzanine card board name
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kBoardName,
      std::string("MC").append(std::to_string(slot)).append("/Management/EEProm/BoardName"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add mezzanine card present item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kMezzanineCardPresent,
      std::string("MC").append(std::to_string(slot)).append("/CardDetectionItems/MezzanineCardPresent"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add mezzanine card power enable item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kMezzanineCardEnable,
      std::string("MC").append(std::to_string(slot)).append("/CardDetectionItems/MezzanineCardEnable"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add mezzanine card reset item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kMezzanineCardReset,
      std::string("MC").append(std::to_string(slot)).append("/CardDetectionItems/MezzanineCardReset"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add driver enable item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kDriverEnable,
      std::string("MC").append(std::to_string(slot)).append("/CardDetectionItems/DriverEnable"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT_WRITE)));
  // Add driver reset item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kDriverReset,
      std::string("MC").append(std::to_string(slot)).append("/CardDetectionItems/DriverReset"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT_WRITE)));
  // Add item operating hours
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kOperatingHours,
      std::string("MC").append(std::to_string(slot)).append("/Management/EEProm/OperatingHours"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add MC Driver override signal collection
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kMCDriverOverride,
      std::string("MC").append(std::to_string(slot)).append("/Application/MCDriver/Override/Collection"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add MC Driver status signal collection
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kMCDriverStatus,
      std::string("MC").append(std::to_string(slot)).append("/Application/MCDriver/Status/Collection"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add live sign counter
  message->AddMessageItem(
      std::make_unique<Messages::MessageItemProperty>(
          kLiveSignCounter,
          std::string("MC").append(std::to_string(slot)).append("/Application/MCDriver/Status/LiveSignCounter"),
          std::make_unique<Messages::Visitor::VisitorPropertyGet>(
              Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add status PBD A
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kPBDA,
      std::string("MC").append(std::to_string(slot)).append("/Application/MCDriver/Status/PbdA"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add status PBD B
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kPBDB,
      std::string("MC").append(std::to_string(slot)).append("/Application/MCDriver/Status/PbdB"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add status PBM A
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kPBMA,
      std::string("MC").append(std::to_string(slot)).append("/Application/MCDriver/Status/PbmA"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add status PBM B
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kPBMB,
      std::string("MC").append(std::to_string(slot)).append("/Application/MCDriver/Status/PbmB"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add item datalink sender id
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kSenderId,
      std::string("MC").append(std::to_string(slot)).append("/Application/MCDriver/Status/SenderId"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add item datalink debug info
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
        kDatalinkDebugInfo,
      std::string("MC").append(std::to_string(slot)).append("/Application/MCDriver/Status/DatalinkDebugInfo"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add item beam permit current
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kBeamPermitCurrent,
      std::string("MC").append(std::to_string(slot)).append("/Application/AnalogIn1/BeamPermitCurrent"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add item ready current
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kReadyCurrent,
      std::string("MC").append(std::to_string(slot)).append("/Application/AnalogIn2/ReadyCurrent"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add item beam permit and ready voltage
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kBeamPermitAndReadyVoltage,
      std::string("MC").append(std::to_string(slot)).append(
        "/Application/AnalogIn1/BeamPermitAndReadyVoltage"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add item redundant beam permit current
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kRedundantBeamPermitCurrent,
      std::string("MC").append(std::to_string(slot)).append(
        "/Application/AnalogIn3/RedundantBeamPermitCurrent"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add item redundant ready current
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kRedundantReadyCurrent,
      std::string("MC").append(std::to_string(slot)).append("/Application/AnalogIn4/RedundantReadyCurrent"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add item redundant beam permit and ready voltage
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kRedundantBeamPermitAndReadyVoltage,
      std::string("MC").append(std::to_string(slot)).append(
        "/Application/AnalogIn3/RedundantBeamPermitAndReadyVoltage"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add item main current
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kMainCurrent,
      std::string("MC").append(std::to_string(slot)).append("/Application/HotSwapController/MainCurrent"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add item internal main voltage
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kInternalMainVoltage,
      std::string("MC").append(std::to_string(slot)).append("/Application/HotSwapController/MainVoltage"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add item internal standby voltage
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kInternalStandbyVoltage,
      std::string("MC").append(std::to_string(slot)).append("/Application/HotSwapController/StandbyVoltage"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add item internal 3V3 voltage
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kInternal3V3Voltage,
      std::string("MC").append(std::to_string(slot)).append("/Application/VoltageMonitor/Voltage3V3"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add item internal 1V8 voltage
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kInternal1V8Voltage,
      std::string("MC").append(std::to_string(slot)).append("/Application/VoltageMonitor/Voltage1V8"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Return message
  return std::move(message);
}

std::experimental::optional<std::unique_ptr<std::vector<char>>> CLMCStatusRead::Serialize(
  const Messages::MessageTemplate<Messages::MessageItemProperty> &message) {
  // Prepare out vector
  auto out = std::make_unique<std::vector<char>>();
  auto dataStatus = message.GetDataStatus();
  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    for (const auto &item : message.GetMessageItems()) {
      if (item->ItemState()->GetValue() != Messages::Types::MessageItemState::Enum::OK) {
        dataStatus = Messages::Types::DataStatus::Enum::INFORMATION_NOT_AVAILABLE;
        break;
      }
    }
  }
  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    // ======== Check MC ========
    if (const auto &item = message.GetMessageItem(kMezzanineCardPresent)) {
      if (!item.value()->GetValue<bool>().value()) {
        dataStatus = Messages::Types::DataStatus::Enum::MC_NOT_PRESENT;
      }
    }
  }
  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    if (const auto &item = message.GetMessageItem(kBoardName)) {
      if (const auto &retVal = item.value()->GetValue<::Types::ModuleType>()) {
        if (retVal.value().GetEnum() != ::Types::ModuleType::Enum::SCU_CLMC) {
          dataStatus = Messages::Types::DataStatus::Enum::WRONG_MEZZANINE_CARD;
        }
      } else {
        dataStatus = Messages::Types::DataStatus::Enum::INFORMATION_NOT_AVAILABLE;
      }
    }
  }

  // ======== Serialize "Data Status" ========
  FbisMessageBase::Serialize(out.get(), static_cast<uint8_t>(dataStatus));

  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    // ======== Serialize "Platform Control Signal Status" ========
    // Parse platform control signal status
    uint8_t platformControlSignalStatus = 0x00;
    // Serialize mezzanine card present item
    if (const auto &item = message.GetMessageItem(kMezzanineCardPresent)) {
      if (const auto &retVal = item.value()->GetValue<bool>()) {
        if (retVal.value()) {
          platformControlSignalStatus |= 0x01;
        }
      }
    }
    // Serialize mezzanine card power enable item
    if (const auto &item = message.GetMessageItem(kMezzanineCardEnable)) {
      if (const auto &retVal = item.value()->GetValue<bool>()) {
        if (retVal.value()) {
          platformControlSignalStatus |= 0x02;
        }
      }
    }
    // Serialize mezzanine card reset item
    if (const auto &item = message.GetMessageItem(kMezzanineCardReset)) {
      if (const auto &retVal = item.value()->GetValue<bool>()) {
        if (retVal.value()) {
          platformControlSignalStatus |= 0x04;
        }
      }
    }
    FbisMessageBase::Serialize(out.get(), platformControlSignalStatus);
    // ======== Serialize "MC_Driver Status" ========
    // Parse driver status
    uint8_t driverStatus = 0x00;
    // Serialize driver enable item
    if (const auto &item = message.GetMessageItem(kDriverEnable)) {
      if (const auto &retVal = item.value()->GetValue<bool>()) {
        if (retVal.value()) {
          driverStatus |= 0x01;
        }
      }
    }
    // Serialize driver reset item
    if (const auto &item = message.GetMessageItem(kDriverReset)) {
      if (const auto &retVal = item.value()->GetValue<bool>()) {
        if (!retVal.value()) {  // MCDriverReset is inverted in FBIS-Network-Protocol
          driverStatus |= 0x02;
        }
      }
    }
    FbisMessageBase::Serialize(out.get(), driverStatus);
    // ======== Serialize "MC Operating Hours" ========
    if (const auto &item = message.GetMessageItem(kOperatingHours)) {
      if (const auto &retVal = item.value()->GetValue<std::chrono::minutes>()) {
        auto operationTime = static_cast<uint32_t>(retVal.value().count() / 15);  // Cast to 15 min! (add const)
        FbisMessageBase::Serialize(out.get(), operationTime);
      }
    }
    // ======== Serialize "Overriding of CLMC Input Signals" ========
    if (const auto &item = message.GetMessageItem(kMCDriverOverride)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        FbisMessageBase::Serialize(out.get(), retVal.value());
      }
    }
    // ======== Serialize "Datalink DebugInfos" ========
    if (const auto &item = message.GetMessageItem(kDatalinkDebugInfo)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        FbisMessageBase::Serialize(out.get(), retVal.value());
      }
    }
    // ======== Serialize "Details" ========
    // Serialize item datalink sender id
    if (const auto &item = message.GetMessageItem(kSenderId)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        FbisMessageBase::Serialize(out.get(), retVal.value());
      }
    }
    // Serialize status PBM B
    if (const auto &item = message.GetMessageItem(kPBMB)) {
      if (const auto &retVal = item.value()->GetValue<uint16_t>()) {
        FbisMessageBase::Serialize(out.get(), retVal.value());
      }
    }
    // Serialize status PBM A
    if (const auto &item = message.GetMessageItem(kPBMA)) {
      if (const auto &retVal = item.value()->GetValue<uint16_t>()) {
        FbisMessageBase::Serialize(out.get(), retVal.value());
      }
    }
    // Serialize status PBD B
    if (const auto &item = message.GetMessageItem(kPBDB)) {
      if (const auto &retVal = item.value()->GetValue<uint16_t>()) {
        FbisMessageBase::Serialize(out.get(), retVal.value());
      }
    }
    // Serialize status PBD A
    if (const auto &item = message.GetMessageItem(kPBDA)) {
      if (const auto &retVal = item.value()->GetValue<uint16_t>()) {
        FbisMessageBase::Serialize(out.get(), retVal.value());
      }
    }
    // Serialize live sign counter
    if (const auto &item = message.GetMessageItem(kLiveSignCounter)) {
      if (const auto &retVal = item.value()->GetValue<uint16_t>()) {
        FbisMessageBase::Serialize(out.get(), retVal.value());
      }
    }
    // Serialize MC Driver status signals
    if (const auto &item = message.GetMessageItem(kMCDriverStatus)) {
      if (const auto &retVal = item.value()->GetValue<uint16_t>()) {
        FbisMessageBase::Serialize(out.get(), retVal.value());
      }
    }
    // ======== Serialize "Beam Permit Signal Current" ========
    if (const auto &item = message.GetMessageItem(kBeamPermitCurrent)) {
      if (const auto &retVal = item.value()->GetValue<::Types::Value<int32_t>>()) {
        auto current = static_cast<int16_t>(retVal.value().GetValue() / 10);  // change unit from uA to 10uA
        FbisMessageBase::Serialize(out.get(), current);
      }
    }
    // ======== Serialize "Ready Signal Current" ========
    if (const auto &item = message.GetMessageItem(kReadyCurrent)) {
      if (const auto &retVal = item.value()->GetValue<::Types::Value<int32_t>>()) {
        auto current = static_cast<int16_t>(retVal.value().GetValue() / 10);  // change unit from uA to 10uA
        FbisMessageBase::Serialize(out.get(), current);
      }
    }
    // ======== Serialize "Beam Permit and Ready Signals Supply Voltage" ========
    if (const auto &item = message.GetMessageItem(kBeamPermitAndReadyVoltage)) {
      if (const auto &retVal = item.value()->GetValue<::Types::Value<int32_t>>()) {
        auto voltage = static_cast<int16_t>(retVal.value().GetValue());
        FbisMessageBase::Serialize(out.get(), voltage);
      }
    }
    // ======== Serialize "Redundant Beam Permit Signal Current" ========
    if (const auto &item = message.GetMessageItem(kRedundantBeamPermitCurrent)) {
      if (const auto &retVal = item.value()->GetValue<::Types::Value<int32_t>>()) {
        auto current = static_cast<int16_t>(retVal.value().GetValue() / 10);  // change unit from uA to 10uA
        FbisMessageBase::Serialize(out.get(), current);
      }
    }
    // ======== Serialize "Redundant Ready Signal Current" ========
    if (const auto &item = message.GetMessageItem(kRedundantReadyCurrent)) {
      if (const auto &retVal = item.value()->GetValue<::Types::Value<int32_t>>()) {
        auto current = static_cast<int16_t>(retVal.value().GetValue() / 10);  // change unit from uA to 10uA
        FbisMessageBase::Serialize(out.get(), current);
      }
    }
    // ======== Serialize "Redundant Beam Permit and Ready Signals Supply Voltage" ========
    if (const auto &item = message.GetMessageItem(kRedundantBeamPermitAndReadyVoltage)) {
      if (const auto &retVal = item.value()->GetValue<::Types::Value<int32_t>>()) {
        auto voltage = static_cast<int16_t>(retVal.value().GetValue());
        FbisMessageBase::Serialize(out.get(), voltage);
      }
    }
    // ======== Serialize "MAIN Supply Current" ========
    if (const auto &item = message.GetMessageItem(kMainCurrent)) {
      if (const auto &retVal = item.value()->GetValue<::Types::Value<int32_t>>()) {
        auto current = static_cast<int8_t>((retVal.value().GetValue() + kCurrentScale / 2) / kCurrentScale);
        FbisMessageBase::Serialize(out.get(), current);
      }
    }
    // ======== Serialize "Internal MAIN Supply Voltage" ========
    if (const auto &item = message.GetMessageItem(kInternalMainVoltage)) {
      if (const auto &retVal = item.value()->GetValue<::Types::Value<int32_t>>()) {
        auto voltage = static_cast<int8_t>((retVal.value().GetValue() + kVoltageScale / 2) / kVoltageScale);
        FbisMessageBase::Serialize(out.get(), voltage);
      }
    }
    // ======== Serialize "Internal STANDBY Supply Voltage" ========
    if (const auto &item = message.GetMessageItem(kInternalStandbyVoltage)) {
      if (const auto &retVal = item.value()->GetValue<::Types::Value<int32_t>>()) {
        auto voltage = static_cast<int8_t>((retVal.value().GetValue() + kVoltageScale / 2) / kVoltageScale);
        FbisMessageBase::Serialize(out.get(), voltage);
      }
    }
    // ======== Serialize "Internal 3V3 Supply Voltage" ========
    if (const auto &item = message.GetMessageItem(kInternal3V3Voltage)) {
      if (const auto &retVal = item.value()->GetValue<::Types::Value<int32_t>>()) {
        auto voltage = static_cast<int8_t>((retVal.value().GetValue() + kVoltageScale / 2) / kVoltageScale);
        FbisMessageBase::Serialize(out.get(), voltage);
      }
    }
    // ======== Serialize "Internal 1V8 Supply Voltage" ========
    if (const auto &item = message.GetMessageItem(kInternal1V8Voltage)) {
      if (const auto &retVal = item.value()->GetValue<::Types::Value<int32_t>>()) {
        auto voltage = static_cast<int8_t>((retVal.value().GetValue() + kVoltageScale / 2) / kVoltageScale);
        FbisMessageBase::Serialize(out.get(), voltage);
      }
    }
  }
  return std::move(out);
}

}  // namespace Communication::Protocols::FbisNetwork
