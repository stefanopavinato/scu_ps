/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <vector>
#include <cstdint>

namespace Communication::Protocols::FbisNetwork {
class FbisMessageBase {
 public:
  static bool Serialize(std::vector<char> *out, const int16_t &value);

  static bool Serialize(std::vector<char> *out, const int8_t &value);

  static bool Serialize(std::vector<char> *out, const uint32_t &value);

  static bool Serialize(std::vector<char> *out, const uint16_t &value);

  static bool Serialize(std::vector<char> *out, const uint8_t &value);

  static bool Serialize(std::vector<char> *out, const int32_t &value);

 protected:
  static const uint32_t kFirstParam;
  static const uint32_t kSlotMin;
  static const uint32_t kSlotMax;
};
}  // namespace Communication::Protocols::FbisNetwork
