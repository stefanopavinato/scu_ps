/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Types/Value.h"
#include "Types/ModuleType.h"
#include "Messages/Types/MessageItemState.h"
#include "Messages/Types/DataStatus.h"
#include "Messages/Visitor/VisitorPropertyGet.h"
#include "Messages/MessageItemProperty.h"
#include "Messages/MessageTemplate.h"
#include "LVDSMCStatusRead.h"

namespace Communication::Protocols::FbisNetwork {

const char LVDSMCStatusRead::kBoardName[] = "BoardName";
const char LVDSMCStatusRead::kMezzanineCardPresent[] = "MezzanineCardPresent";
const char LVDSMCStatusRead::kMezzanineCardEnable[] = "MezzanineCardEnable";
const char LVDSMCStatusRead::kMezzanineCardReset[] = "MezzanineCardReset";
const char LVDSMCStatusRead::kDriverEnable[] = "DriverEnable";
const char LVDSMCStatusRead::kDriverReset[] = "DriverReset";
const char LVDSMCStatusRead::kOperatingHours[] = "OperatingHours";
const char LVDSMCStatusRead::kSignalOverrideDiscretePort1[] = "SignalOverrideDiscretePort1";
const char LVDSMCStatusRead::kSignalOverrideDiscretePort2[] = "SignalOverrideDiscretePort2";
const char LVDSMCStatusRead::kSignalOverrideDiscretePort3[] = "SignalOverrideDiscretePort3";
const char LVDSMCStatusRead::kSignalOverrideDatalinkPort1[] = "SignalOverrideDatalinkPort1";
const char LVDSMCStatusRead::kSignalOverrideDatalinkPort2[] = "SignalOverrideDatalinkPort2";
const char LVDSMCStatusRead::kSignalOverrideDatalinkPort3[] = "SignalOverrideDatalinkPort3";
const char LVDSMCStatusRead::kMCDriverStatus1[] = "MCDriverStatus1";
const char LVDSMCStatusRead::kMCDriverStatus2[] = "MCDriverStatus2";
const char LVDSMCStatusRead::kMCDriverStatus3[] = "MCDriverStatus3";
const char LVDSMCStatusRead::kSenderID1[] = "SenderID1";
const char LVDSMCStatusRead::kSenderID2[] = "SenderID2";
const char LVDSMCStatusRead::kSenderID3[] = "SenderID3";
const char LVDSMCStatusRead::kLiveSignCounter1[] = "LiveSignCounter1";
const char LVDSMCStatusRead::kLiveSignCounter2[] = "LiveSignCounter2";
const char LVDSMCStatusRead::kLiveSignCounter3[] = "LiveSignCounter3";
const char LVDSMCStatusRead::kMainCurrent[] = "MainCurrent";
const char LVDSMCStatusRead::kInternalMainVoltage[] = "InternalMainVoltage";
const char LVDSMCStatusRead::kInternalStandbyVoltage[] = "InternalStandbyVoltage";
const char LVDSMCStatusRead::kInternal3V3Voltage[] = "Internal3V3Voltage";
const char LVDSMCStatusRead::kInternal1V8Voltage[] = "Internal1V8Voltage";

const int32_t LVDSMCStatusRead::kCurrentScale = 50;
const int32_t LVDSMCStatusRead::kVoltageScale = 200;

std::experimental::optional<std::unique_ptr<Messages::MessageTemplate<Messages::MessageItemProperty>>>
LVDSMCStatusRead::Deserialize(std::unique_ptr<std::vector<char>> input) {
  // Create message
  auto message = std::make_unique<Messages::MessageTemplate<Messages::MessageItemProperty>>();
  // Set request id
  message->SetRequestId(Communication::Types::RequestId::Enum::SCU_LVDSMC_STATUS_READ);
  // Check message length
  auto length = (*input).size();
  if (length != 6) {  // outer frame 5 chars, inner frame 1 char
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_REQUEST_FORMAT);
    return std::move(message);
  }
  // Decode slot
  auto slot = static_cast<uint8_t>((*input)[kFirstParam]);
  if (slot < kSlotMin || slot > kSlotMax) {
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::SLOT_NOT_VALID);
    return std::move(message);
  }
  // Add mezzanine card board name
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kBoardName,
      std::string("MC").append(std::to_string(slot)).append(
        "/Management/EEProm/BoardName"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add mezzanine card present item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kMezzanineCardPresent,
      std::string("MC").append(std::to_string(slot)).append(
        "/CardDetectionItems/MezzanineCardPresent"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add mezzanine card power enable item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kMezzanineCardEnable,
      std::string("MC").append(std::to_string(slot)).append(
        "/CardDetectionItems/MezzanineCardEnable"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add mezzanine card reset item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kMezzanineCardReset,
      std::string("MC").append(std::to_string(slot)).append(
        "/CardDetectionItems/MezzanineCardReset"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add driver enable item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kDriverEnable,
      std::string("MC").append(std::to_string(slot)).append(
        "/CardDetectionItems/DriverEnable"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT_WRITE)));
  // Add driver reset item
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kDriverReset,
      std::string("MC").append(std::to_string(slot)).append(
        "/CardDetectionItems/DriverReset"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT_WRITE)));
  // Add item operating hours
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kOperatingHours,
      std::string("MC").append(std::to_string(slot)).append(
        "/Management/EEProm/OperatingHours"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add MC Driver input signal override collection
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kSignalOverrideDatalinkPort3,
      std::string("MC").append(std::to_string(slot)).append(
        "/Application/MCDriver/MCDriverPort3/SignalOverride/DatalinkSignalOverrideCollection"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kSignalOverrideDatalinkPort2,
      std::string("MC").append(std::to_string(slot)).append(
        "/Application/MCDriver/MCDriverPort2/SignalOverride/DatalinkSignalOverrideCollection"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kSignalOverrideDatalinkPort1,
      std::string("MC").append(std::to_string(slot)).append(
        "/Application/MCDriver/MCDriverPort1/SignalOverride/DatalinkSignalOverrideCollection"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kSignalOverrideDiscretePort3,
      std::string("MC").append(std::to_string(slot)).append(
        "/Application/MCDriver/MCDriverPort3/SignalOverride/DiscreteSignalOverrideCollection"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kSignalOverrideDiscretePort2,
      std::string("MC").append(std::to_string(slot)).append(
        "/Application/MCDriver/MCDriverPort2/SignalOverride/DiscreteSignalOverrideCollection"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kSignalOverrideDiscretePort1,
      std::string("MC").append(std::to_string(slot)).append(
        "/Application/MCDriver/MCDriverPort1/SignalOverride/DiscreteSignalOverrideCollection"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add MCDriverStatus1
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kMCDriverStatus1,
      std::string("MC").append(std::to_string(slot)).append(
        "/Application/MCDriver/MCDriverPort1/Status/StatusItemCollection"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add MCDriverStatus2
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kMCDriverStatus2,
      std::string("MC").append(std::to_string(slot)).append(
        "/Application/MCDriver/MCDriverPort2/Status/StatusItemCollection"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add MCDriverStatus3
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kMCDriverStatus3,
      std::string("MC").append(std::to_string(slot)).append(
        "/Application/MCDriver/MCDriverPort3/Status/StatusItemCollection"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add Sender ID
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kSenderID1,
      std::string("MC").append(std::to_string(slot)).append(
        "/Application/MCDriver/MCDriverPort1/Status/SenderID"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kSenderID2,
      std::string("MC").append(std::to_string(slot)).append(
        "/Application/MCDriver/MCDriverPort2/Status/SenderID"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kSenderID3,
      std::string("MC").append(std::to_string(slot)).append(
        "/Application/MCDriver/MCDriverPort3/Status/SenderID"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add LiveSign Counter
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kLiveSignCounter1,
      std::string("MC").append(std::to_string(slot)).append(
        "/Application/MCDriver/MCDriverPort1/Status/LiveSignCounter"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kLiveSignCounter2,
      std::string("MC").append(std::to_string(slot)).append(
        "/Application/MCDriver/MCDriverPort2/Status/LiveSignCounter"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kLiveSignCounter3,
      std::string("MC").append(std::to_string(slot)).append(
        "/Application/MCDriver/MCDriverPort3/Status/LiveSignCounter"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add item main current
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kMainCurrent,
      std::string("MC").append(std::to_string(slot)).append(
        "/Application/HotSwapController/MainCurrent"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add item internal main voltage
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kInternalMainVoltage,
      std::string("MC").append(std::to_string(slot)).append(
        "/Application/HotSwapController/MainVoltage"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add item internal standby voltage
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kInternalStandbyVoltage,
      std::string("MC").append(std::to_string(slot)).append(
        "/Application/HotSwapController/StandbyVoltage"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add item internal 3V3 voltage
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kInternal3V3Voltage,
      std::string("MC").append(std::to_string(slot)).append(
        "/Application/VoltageMonitor/Voltage3V3"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Add item internal 1V8 voltage
  message->AddMessageItem(
    std::make_unique<Messages::MessageItemProperty>(
      kInternal1V8Voltage,
      std::string("MC").append(std::to_string(slot)).append(
        "/Application/VoltageMonitor/Voltage1V8"),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN)));
  // Return message
  return std::move(message);
}

std::experimental::optional<std::unique_ptr<std::vector<char>>> LVDSMCStatusRead::Serialize(
  const Messages::MessageTemplate<Messages::MessageItemProperty> &message) {
  // Prepare out vector
  auto out = std::make_unique<std::vector<char>>();
  auto dataStatus = message.GetDataStatus();
  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    for (const auto &item : message.GetMessageItems()) {
      if (item->ItemState()->GetValue() != Messages::Types::MessageItemState::Enum::OK) {
        dataStatus = Messages::Types::DataStatus::Enum::INFORMATION_NOT_AVAILABLE;
        break;
      }
    }
  }
  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    // ======== Check MC ========
    if (const auto &item = message.GetMessageItem(kMezzanineCardPresent)) {
      if (!item.value()->GetValue<bool>().value()) {
        dataStatus = Messages::Types::DataStatus::Enum::MC_NOT_PRESENT;
      }
    }
  }
  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    if (const auto &item = message.GetMessageItem(kBoardName)) {
      if (const auto &retVal = item.value()->GetValue<::Types::ModuleType>()) {
        if (retVal.value().GetEnum() != ::Types::ModuleType::Enum::SCU_LVDSMC) {
          dataStatus = Messages::Types::DataStatus::Enum::WRONG_MEZZANINE_CARD;
        }
      } else {
        dataStatus = Messages::Types::DataStatus::Enum::INFORMATION_NOT_AVAILABLE;
      }
    }
  }

  // ======== Serialize "Data Status" ========
  FbisMessageBase::Serialize(out.get(), static_cast<uint8_t>(dataStatus));

  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    // ======== Serialize "Platform Control Signal Status" ========
    // Parse platform control signal status
    uint8_t platformControlSignalStatus = 0x00;
    // Serialize mezzanine card present item
    if (const auto &item = message.GetMessageItem(kMezzanineCardPresent)) {
      if (const auto &retVal = item.value()->GetValue<bool>()) {
        if (retVal.value()) {
          platformControlSignalStatus |= 0x01;
        }
      }
    }
    // Serialize mezzanine card power enable item
    if (const auto &item = message.GetMessageItem(kMezzanineCardEnable)) {
      if (const auto &retVal = item.value()->GetValue<bool>()) {
        if (retVal.value()) {
          platformControlSignalStatus |= 0x02;
        }
      }
    }
    // Serialize mezzanine card reset item
    if (const auto &item = message.GetMessageItem(kMezzanineCardReset)) {
      if (const auto &retVal = item.value()->GetValue<bool>()) {
        if (retVal.value()) {
          platformControlSignalStatus |= 0x04;
        }
      }
    }
    FbisMessageBase::Serialize(out.get(), platformControlSignalStatus);
    // ======== Serialize "MC_Driver Status" ========
    // Parse driver status
    uint8_t driverStatus = 0x00;
    // Serialize driver enable item
    if (const auto &item = message.GetMessageItem(kDriverEnable)) {
      if (const auto &retVal = item.value()->GetValue<bool>()) {
        if (retVal.value()) {
          driverStatus |= 0x01;
        }
      }
    }
    // Serialize driver reset item
    if (const auto &item = message.GetMessageItem(kDriverReset)) {
      if (const auto &retVal = item.value()->GetValue<bool>()) {
        if (!retVal.value()) {
          driverStatus |= 0x02;
        }
      }
    }
    FbisMessageBase::Serialize(out.get(), driverStatus);
    // ======== Serialize "MC Operating Hours" ========
    if (const auto &item = message.GetMessageItem(kOperatingHours)) {
      if (const auto &retVal = item.value()->GetValue<std::chrono::minutes>()) {
        auto hours = static_cast<uint32_t>(retVal.value().count() / 15);
        FbisMessageBase::Serialize(out.get(), hours);
      }
    }
    // ======== Serialize "Overriding of LVDSMC Input Signals" ========
    out->push_back(0x00);
    if (const auto &item = message.GetMessageItem(kSignalOverrideDatalinkPort3)) {
      if (const auto &retVal = item.value()->GetValue<uint8_t>()) {
        auto inputSignalOverride = static_cast<uint8_t>(retVal.value());
        FbisMessageBase::Serialize(out.get(), inputSignalOverride);
      }
    }
    if (const auto &item = message.GetMessageItem(kSignalOverrideDatalinkPort2)) {
      if (const auto &retVal = item.value()->GetValue<uint8_t>()) {
        auto inputSignalOverride = static_cast<uint8_t>(retVal.value());
        FbisMessageBase::Serialize(out.get(), inputSignalOverride);
      }
    }
    if (const auto &item = message.GetMessageItem(kSignalOverrideDatalinkPort1)) {
      if (const auto &retVal = item.value()->GetValue<uint8_t>()) {
        auto inputSignalOverride = static_cast<uint8_t>(retVal.value());
        FbisMessageBase::Serialize(out.get(), inputSignalOverride);
      }
    }
    out->push_back(0x00);
    if (const auto &item = message.GetMessageItem(kSignalOverrideDiscretePort3)) {
      if (const auto &retVal = item.value()->GetValue<uint8_t>()) {
        auto inputSignalOverride = static_cast<int8_t>(retVal.value());
        FbisMessageBase::Serialize(out.get(), inputSignalOverride);
      }
    }
    if (const auto &item = message.GetMessageItem(kSignalOverrideDiscretePort2)) {
      if (const auto &retVal = item.value()->GetValue<uint8_t>()) {
        auto inputSignalOverride = static_cast<int8_t>(retVal.value());
        FbisMessageBase::Serialize(out.get(), inputSignalOverride);
      }
    }
    if (const auto &item = message.GetMessageItem(kSignalOverrideDiscretePort1)) {
      if (const auto &retVal = item.value()->GetValue<uint8_t>()) {
        auto inputSignalOverride = static_cast<int8_t>(retVal.value());
        FbisMessageBase::Serialize(out.get(), inputSignalOverride);
      }
    }
    // ======== Serialize "Details" ========
    // Serialize MC Driver status signals
    if (const auto &item = message.GetMessageItem(kLiveSignCounter3)) {
      if (const auto &retVal = item.value()->GetValue<uint8_t>()) {
        auto mcDriverStatus = static_cast<int16_t>(retVal.value());
        FbisMessageBase::Serialize(out.get(), mcDriverStatus);
      }
    }
    if (const auto &item = message.GetMessageItem(kSenderID3)) {
      if (const auto &retVal = item.value()->GetValue<uint16_t>()) {
        auto mcDriverStatus = static_cast<int16_t>(retVal.value());
        FbisMessageBase::Serialize(out.get(), mcDriverStatus);
      }
    }
    if (const auto &item = message.GetMessageItem(kLiveSignCounter2)) {
      if (const auto &retVal = item.value()->GetValue<uint8_t>()) {
        auto mcDriverStatus = static_cast<int16_t>(retVal.value());
        FbisMessageBase::Serialize(out.get(), mcDriverStatus);
      }
    }
    if (const auto &item = message.GetMessageItem(kSenderID2)) {
      if (const auto &retVal = item.value()->GetValue<uint16_t>()) {
        auto mcDriverStatus = static_cast<int16_t>(retVal.value());
        FbisMessageBase::Serialize(out.get(), mcDriverStatus);
      }
    }
    if (const auto &item = message.GetMessageItem(kLiveSignCounter1)) {
      if (const auto &retVal = item.value()->GetValue<uint8_t>()) {
        auto mcDriverStatus = static_cast<int16_t>(retVal.value());
        FbisMessageBase::Serialize(out.get(), mcDriverStatus);
      }
    }
    if (const auto &item = message.GetMessageItem(kSenderID1)) {
      if (const auto &retVal = item.value()->GetValue<uint16_t>()) {
        auto mcDriverStatus = static_cast<int16_t>(retVal.value());
        FbisMessageBase::Serialize(out.get(), mcDriverStatus);
      }
    }
    if (const auto &item = message.GetMessageItem(kMCDriverStatus3)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        auto mcDriverStatus = static_cast<int32_t>(retVal.value());
        FbisMessageBase::Serialize(out.get(), mcDriverStatus);
      }
    }
    if (const auto &item = message.GetMessageItem(kMCDriverStatus2)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        auto mcDriverStatus = static_cast<int32_t>(retVal.value());
        FbisMessageBase::Serialize(out.get(), mcDriverStatus);
      }
    }
    if (const auto &item = message.GetMessageItem(kMCDriverStatus1)) {
      if (const auto &retVal = item.value()->GetValue<uint32_t>()) {
        auto mcDriverStatus = static_cast<int32_t>(retVal.value());
        FbisMessageBase::Serialize(out.get(), mcDriverStatus);
      }
    }
    // ======== Serialize "MAIN Supply Current" ========
    if (const auto &item = message.GetMessageItem(kMainCurrent)) {
      if (const auto &retVal = item.value()->GetValue<::Types::Value<int32_t>>()) {
        auto current = static_cast<int8_t>(retVal.value().GetValue() / kCurrentScale);
        FbisMessageBase::Serialize(out.get(), current);
      }
    }
    // ======== Serialize "Internal MAIN Supply Voltage" ========
    if (const auto &item = message.GetMessageItem(kInternalMainVoltage)) {
      if (const auto &retVal = item.value()->GetValue<::Types::Value<int32_t>>()) {
        auto voltage = static_cast<int8_t>(retVal.value().GetValue() / kVoltageScale);
        FbisMessageBase::Serialize(out.get(), voltage);
      }
    }
    // ======== Serialize "Internal STANDBY Supply Voltage" ========
    if (const auto &item = message.GetMessageItem(kInternalStandbyVoltage)) {
      if (const auto &retVal = item.value()->GetValue<::Types::Value<int32_t>>()) {
        auto voltage = static_cast<int8_t>(retVal.value().GetValue() / kVoltageScale);
        FbisMessageBase::Serialize(out.get(), voltage);
      }
    }
    // ======== Serialize "Internal 3V3 Supply Voltage" ========
    if (const auto &item = message.GetMessageItem(kInternal3V3Voltage)) {
      if (const auto &retVal = item.value()->GetValue<::Types::Value<int32_t>>()) {
        auto voltage = static_cast<int8_t>(retVal.value().GetValue() / kVoltageScale);
        FbisMessageBase::Serialize(out.get(), voltage);
      }
    }
    // ======== Serialize "Internal 1V8 Supply Voltage" ========
    if (const auto &item = message.GetMessageItem(kInternal1V8Voltage)) {
      if (const auto &retVal = item.value()->GetValue<::Types::Value<int32_t>>()) {
        auto voltage = static_cast<int8_t>(retVal.value().GetValue() / kVoltageScale);
        FbisMessageBase::Serialize(out.get(), voltage);
      }
    }
  }
  return std::move(out);
}

}  // namespace Communication::Protocols::FbisNetwork
