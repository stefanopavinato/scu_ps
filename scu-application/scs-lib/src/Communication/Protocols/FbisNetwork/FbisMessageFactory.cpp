/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Messages/MessageItemProperty.h"
#include "Messages/UnknownCommand.h"
#include "Messages/MessageTemplate.h"
#include "Messages/MCExpectedRead.h"
#include "Messages/SERVersionRead.h"
#include "Messages/SMODInfoRead.h"
#include "Messages/BeamSwitchOffRequest.h"
#include "Messages/SWBeamSwitchOffRequestWrite.h"
#include "Messages/MCFastFrameSignalsRead.h"
#include "Messages/MCNestedDataStructureRead.h"
#include "Messages/MCUserDataRead.h"
#include "Messages/SFPStatusRead.h"
#include "Messages/SERStatusRead.h"
#include "Messages/CLMCStatusRead.h"
#include "Messages/CLMCDatalinkErrorsRead.h"
#include "Messages/CLMCDatalinkErrorsClear.h"
#include "Messages/CLMCSignalOverride.h"
#include "Messages/LVDSMCStatusRead.h"
#include "Messages/LVDSMCSignalOverride.h"
#include "Messages/LVDSMCDatalinkErrorsRead.h"
#include "Messages/LVDSMCDatalinkErrorsClear.h"
#include "Messages/RS485MCStatusRead.h"
#include "Messages/RS485MCSignalOverride.h"
#include "Messages/ISMC1StatusRead.h"
#include "Messages/ISMC1SignalOverride.h"
#include "Messages/ISMC2StatusRead.h"
#include "Messages/ISMC2SignalOverride.h"
#include "Messages/CHMCStatusRead.h"
#include "Messages/CHMCSignalOverride.h"
#include "Messages/BUMCStatusRead.h"
#include "Messages/FanUnitStatusRead.h"
#include "Messages/FanUnitFanSpeedWrite.h"
#include "Messages/MCTemperatureRead.h"
#include "Messages/SERTemperatureRead.h"
#include "Messages/FUTemperatureRead.h"
#include "Messages/SLinkTransceiverStatusRead.h"
#include "Messages/SLinkErrorsClear.h"
#include "Messages/OplTransceiverStatusRead.h"
#include "Messages/OplParametrizationRead.h"
#include "Messages/OplStatusRead.h"
#include "Messages/OplErrorsClear.h"
#include "Messages/MCInsertionStatusRead.h"
#include "FbisMessageFactory.h"

namespace Communication::Protocols::FbisNetwork {

std::experimental::optional<std::unique_ptr<Messages::MessageTemplate<Messages::MessageItemProperty>>>
FbisMessageFactory::DeSerialize(
  const Communication::Types::RequestId &requestId,
  std::unique_ptr<std::vector<char>> input) {
  switch (requestId.GetValue()) {
    case Types::RequestId::Enum::FIRMWARE_REGISTER_VALUE_READ: {
      return UnknownCommand::Deserialize(requestId);
    }
    case Types::RequestId::Enum::FIRMWARE_REGISTER_VALUE_WRITE: {
      return UnknownCommand::Deserialize(requestId);
    }
    case Types::RequestId::Enum::SCU_SER_VERSION_READ: {
      return SERVersionRead::Deserialize(std::move(input));
//      return UnknownCommand::Deserialize(requestId);
    }
    case Types::RequestId::Enum::SCU_SMOD_INFO_READ: {
      return SMODInfoRead::Deserialize(std::move(input));
    }
    case Types::RequestId::Enum::SCU_BEAM_SWITCH_OFF_REQUEST_STATUS: {
      return BeamSwitchOffRequest::Deserialize(std::move(input));
    }
    case Types::RequestId::Enum::SCU_BEAM_SWITCH_OFF_REQUEST_WRITE: {
      return SWBeamSwitchOffRequestWrite::Deserialize(std::move(input));
    }
    case Types::RequestId::Enum::SCU_SFP_STATUS_READ: {
      return SFPStatusRead::Deserialize(std::move(input));
    }
    case Types::RequestId::Enum::SCU_TRANSCEIVER_STATUS_READ: {
      return UnknownCommand::Deserialize(requestId);
    }
    case Types::RequestId::Enum::SCU_MC_TEMPERATURE_READ: {
      return MCTemperatureRead::Deserialize(std::move(input));
    }
    case Types::RequestId::Enum::SCU_SERIALIZER_TEMPERATURE_READ: {
      return SERTemperatureRead::Deserialize(std::move(input));
    }
    case Types::RequestId::Enum::SCU_FAN_UNIT_TEMPERATURE_READ: {
      return FUTemperatureRead::Deserialize(std::move(input));
    }
    case Types::RequestId::Enum::SCU_PSU_TEMPERATURE_READ: {
      return UnknownCommand::Deserialize(requestId);
    }
    case Types::RequestId::Enum::SCU_SERIALIZER_STATUS_READ: {
      return SERStatusRead::Deserialize(std::move(input));
    }
    case Types::RequestId::Enum::SCU_SERIALIZER_OPERATING_HOURS_WRITE: {
      return UnknownCommand::Deserialize(requestId);
    }
    case Types::RequestId::Enum::SCU_EVENTHANDLER_STATUS_READ: {
      return UnknownCommand::Deserialize(requestId);
    }
    case Types::RequestId::Enum::SCU_FAN_UNIT_STATUS_READ: {
      return FanUnitStatusRead::Deserialize(std::move(input));
    }
    case Types::RequestId::Enum::FAN_SPEED_WRITE: {
      return FanUnitFanSpeedWrite::Deserialize(std::move(input));
    }
    case Types::RequestId::Enum::FAN_OPERATING_HOURS_WRITE: {
      return UnknownCommand::Deserialize(requestId);
    }
    case Types::RequestId::Enum::SCU_PSU_STATUS_READ: {
      return UnknownCommand::Deserialize(requestId);
    }
    case Types::RequestId::Enum::SCU_MC_INSERTION_STATUS_READ: {
      return MCInsertionStatusRead::Deserialize(std::move(input));
    }
    case Types::RequestId::Enum::SCU_EXPECTED_MC_TYPE_READ: {
      return MCExpectedRead::Deserialize(std::move(input));
    }
    case Types::RequestId::Enum::SCU_MC_FAST_FRAME_SIGNAL_READ: {
      return MCFastFrameSignalsRead::Deserialize(std::move(input));
    }
    case Types::RequestId::Enum::SCU_MC_NESTED_DATA_STRUCTURE_READ: {
      return MCNestedDataStructureRead::Deserialize(std::move(input));
    }
    case Types::RequestId::Enum::SCU_MC_USER_DATA_READ: {
      return MCUserDataRead::Deserialize(std::move(input));
    }
    case Types::RequestId::Enum::SCU_MC_OPERATING_HOURS_WRITE: {
      return UnknownCommand::Deserialize(requestId);
    }
    case Types::RequestId::Enum::SCU_MC_RELAIS_SWITCHING_COUNT_WRITE: {
      return UnknownCommand::Deserialize(requestId);
    }
    case Types::RequestId::Enum::SCU_CLMC_STATUS_READ: {
      return CLMCStatusRead::Deserialize(std::move(input));
    }
    case Types::RequestId::Enum::SCU_CLMC_SIGNAL_OVERRIDE: {
      return CLMCSignalOverride::Deserialize(std::move(input));
    }
    case Types::RequestId::Enum::SCU_CLMC_DATALINK_ERRORS_READ: {
      return CLMCDatalinkErrorsRead::Deserialize(std::move(input));
    }
    case Types::RequestId::Enum::SCU_CLMC_DATALINK_ERRORS_CLEAR: {
      return CLMCDatalinkErrorsClear::Deserialize(std::move(input));
    }
    case Types::RequestId::Enum::SCU_RS485MC_STATUS_READ: {
      return RS485MCStatusRead::Deserialize(std::move(input));
    }
    case Types::RequestId::Enum::SCU_RS485MC_SIGNAL_OVERRIDE: {
      return RS485MCSignalOverride::Deserialize(std::move(input));
    }
    case Types::RequestId::Enum::SCU_RS485MC_ETH_STATUS_READ: {
      return UnknownCommand::Deserialize(requestId);
    }
    case Types::RequestId::Enum::SCU_RS485MC_ETH_SWITCH_RESET: {
      return UnknownCommand::Deserialize(requestId);
    }
    case Types::RequestId::Enum::SCU_RS485MC_ETH_POWER_DOWN: {
      return UnknownCommand::Deserialize(requestId);
    }
    case Types::RequestId::Enum::SCU_LVDSMC_STATUS_READ: {
      return LVDSMCStatusRead::Deserialize(std::move(input));
    }
    case Types::RequestId::Enum::SCU_LVDSMC_SIGNAL_OVERRIDE: {
      return LVDSMCSignalOverride::Deserialize(std::move(input));
    }
    case Types::RequestId::Enum::SCU_LVDSMC_DATALINK_ERRORS_READ: {
      return LVDSMCDatalinkErrorsRead::Deserialize(std::move(input));
    }
    case Types::RequestId::Enum::SCU_LVDSMC_DATALINK_ERRORS_CLEAR: {
      return LVDSMCDatalinkErrorsClear::Deserialize(std::move(input));
    }
    case Types::RequestId::Enum::SCU_ISMC1_STATUS_READ: {
      return ISMC1StatusRead::Deserialize(std::move(input));
    }
    case Types::RequestId::Enum::SCU_ISMC1_SIGNAL_OVERRIDE: {
      return ISMC1SignalOverride::Deserialize(std::move(input));
    }
    case Types::RequestId::Enum::SCU_ISMC2_STATUS_READ: {
      return ISMC2StatusRead::Deserialize(std::move(input));
    }
    case Types::RequestId::Enum::SCU_ISMC2_SIGNAL_OVERRIDE: {
      return ISMC2SignalOverride::Deserialize(std::move(input));
    }
    case Types::RequestId::Enum::SCU_BUMC_STATUS_READ: {
      return BUMCStatusRead::Deserialize(std::move(input));
    }
    case Types::RequestId::Enum::SCU_CHMC_STATUS_READ: {
      return CHMCStatusRead::Deserialize(std::move(input));
    }
    case Types::RequestId::Enum::SCU_CHMC_SIGNAL_OVERRIDE: {
      return CHMCSignalOverride::Deserialize(std::move(input));
    }
    case Types::RequestId::Enum::SCU_MONFUN_STATUS_LIST_READ: {
      return UnknownCommand::Deserialize(requestId);
    }
    case Types::RequestId::Enum::SCU_MONFUN_ERROR_CLEAR: {
      return UnknownCommand::Deserialize(requestId);
    }
    case Types::RequestId::Enum::SCU_SCU_SLINK_TRANSCEIVER_STATUS_READ: {
      return SLinkTransceiverStatusRead::Deserialize(std::move(input));
    }
    case Types::RequestId::Enum::SCU_SCU_SLINK_ERRORS_CLEAR: {
      return SLinkErrorsClear::Deserialize(std::move(input));
    }
    case Types::RequestId::Enum::SCU_SCU_OPL_STATUS_READ: {
      return OplStatusRead::Deserialize(std::move(input));
    }
    case Types::RequestId::Enum::SCU_SCU_OPL_TRANSCEIVER_STATUS_READ: {
      return OplTransceiverStatusRead::Deserialize(std::move(input));
    }
    case Types::RequestId::Enum::SCU_SCU_OPL_PARAMETRIZATION_READ: {
      return OplParametrizationRead::Deserialize(std::move(input));
    }
    case Types::RequestId::Enum::SCU_SCU_OPL_ERRORS_CLEAR: {
      return OplErrorsClear::Deserialize(std::move(input));
    }
    case Types::RequestId::Enum::SCU_ECHO: {
      return UnknownCommand::Deserialize(requestId);
    }
    case Types::RequestId::Enum::NONE: {
      return UnknownCommand::Deserialize(requestId);
    }
  }
  return UnknownCommand::Deserialize(requestId);
}

std::experimental::optional<std::unique_ptr<std::vector<char>>> FbisMessageFactory::Serialize(
  const Messages::MessageTemplate<Messages::MessageItemProperty> &message) {
  switch (message.GetRequestId().GetValue()) {
    case Types::RequestId::Enum::FIRMWARE_REGISTER_VALUE_READ: {
      return UnknownCommand::Serialize(message);
    }
    case Types::RequestId::Enum::FIRMWARE_REGISTER_VALUE_WRITE: {
      return UnknownCommand::Serialize(message);
    }
    case Types::RequestId::Enum::SCU_SER_VERSION_READ: {
      return SERVersionRead::Serialize(message);
//      return UnknownCommand::Serialize(message);
    }
    case Types::RequestId::Enum::SCU_SMOD_INFO_READ: {
      return SMODInfoRead::Serialize(message);
    }
    case Types::RequestId::Enum::SCU_BEAM_SWITCH_OFF_REQUEST_STATUS: {
      return BeamSwitchOffRequest::Serialize(message);
    }
    case Types::RequestId::Enum::SCU_BEAM_SWITCH_OFF_REQUEST_WRITE: {
      return SWBeamSwitchOffRequestWrite::Serialize(message);
    }
    case Types::RequestId::Enum::SCU_SFP_STATUS_READ: {
      return SFPStatusRead::Serialize(message);
    }
    case Types::RequestId::Enum::SCU_TRANSCEIVER_STATUS_READ: {
      return UnknownCommand::Serialize(message);
    }
    case Types::RequestId::Enum::SCU_MC_TEMPERATURE_READ: {
      return MCTemperatureRead::Serialize(message);
    }
    case Types::RequestId::Enum::SCU_SERIALIZER_TEMPERATURE_READ: {
      return SERTemperatureRead::Serialize(message);
    }
    case Types::RequestId::Enum::SCU_FAN_UNIT_TEMPERATURE_READ: {
      return FUTemperatureRead::Serialize(message);
    }
    case Types::RequestId::Enum::SCU_PSU_TEMPERATURE_READ: {
      return UnknownCommand::Serialize(message);
    }
    case Types::RequestId::Enum::SCU_SERIALIZER_STATUS_READ: {
      return SERStatusRead::Serialize(message);
    }
    case Types::RequestId::Enum::SCU_SERIALIZER_OPERATING_HOURS_WRITE: {
      return UnknownCommand::Serialize(message);
    }
    case Types::RequestId::Enum::SCU_EVENTHANDLER_STATUS_READ: {
      return UnknownCommand::Serialize(message);
    }
    case Types::RequestId::Enum::SCU_FAN_UNIT_STATUS_READ: {
      return FanUnitStatusRead::Serialize(message);
    }
    case Types::RequestId::Enum::FAN_SPEED_WRITE: {
      return FanUnitFanSpeedWrite::Serialize(message);
    }
    case Types::RequestId::Enum::FAN_OPERATING_HOURS_WRITE: {
      return UnknownCommand::Serialize(message);
    }
    case Types::RequestId::Enum::SCU_PSU_STATUS_READ: {
      return UnknownCommand::Serialize(message);
    }
    case Types::RequestId::Enum::SCU_MC_INSERTION_STATUS_READ: {
      return MCInsertionStatusRead::Serialize(message);
    }
    case Types::RequestId::Enum::SCU_EXPECTED_MC_TYPE_READ: {
      return MCExpectedRead::Serialize(message);
    }
    case Types::RequestId::Enum::SCU_MC_FAST_FRAME_SIGNAL_READ: {
      return MCFastFrameSignalsRead::Serialize(message);
    }
    case Types::RequestId::Enum::SCU_MC_NESTED_DATA_STRUCTURE_READ: {
      return MCNestedDataStructureRead::Serialize(message);
    }
    case Types::RequestId::Enum::SCU_MC_USER_DATA_READ: {
      return MCUserDataRead::Serialize(message);
    }
    case Types::RequestId::Enum::SCU_MC_OPERATING_HOURS_WRITE: {
      return UnknownCommand::Serialize(message);
    }
    case Types::RequestId::Enum::SCU_MC_RELAIS_SWITCHING_COUNT_WRITE: {
      return UnknownCommand::Serialize(message);
    }
    case Types::RequestId::Enum::SCU_CLMC_STATUS_READ: {
      return CLMCStatusRead::Serialize(message);
    }
    case Types::RequestId::Enum::SCU_CLMC_SIGNAL_OVERRIDE: {
      return CLMCSignalOverride::Serialize(message);
    }
    case Types::RequestId::Enum::SCU_CLMC_DATALINK_ERRORS_READ: {
      return CLMCDatalinkErrorsRead::Serialize(message);
    }
    case Types::RequestId::Enum::SCU_CLMC_DATALINK_ERRORS_CLEAR: {
      return CLMCDatalinkErrorsClear::Serialize(message);
    }
    case Types::RequestId::Enum::SCU_RS485MC_STATUS_READ: {
      return RS485MCStatusRead::Serialize(message);
    }
    case Types::RequestId::Enum::SCU_RS485MC_SIGNAL_OVERRIDE: {
      return RS485MCSignalOverride::Serialize(message);
    }
    case Types::RequestId::Enum::SCU_RS485MC_ETH_STATUS_READ: {
      return UnknownCommand::Serialize(message);
    }
    case Types::RequestId::Enum::SCU_RS485MC_ETH_SWITCH_RESET: {
      return UnknownCommand::Serialize(message);
    }
    case Types::RequestId::Enum::SCU_RS485MC_ETH_POWER_DOWN: {
      return UnknownCommand::Serialize(message);
    }
    case Types::RequestId::Enum::SCU_LVDSMC_STATUS_READ: {
      return LVDSMCStatusRead::Serialize(message);
    }
    case Types::RequestId::Enum::SCU_LVDSMC_SIGNAL_OVERRIDE: {
      return LVDSMCSignalOverride::Serialize(message);
    }
    case Types::RequestId::Enum::SCU_LVDSMC_DATALINK_ERRORS_READ: {
      return LVDSMCDatalinkErrorsRead::Serialize(message);
    }
    case Types::RequestId::Enum::SCU_LVDSMC_DATALINK_ERRORS_CLEAR: {
      return LVDSMCDatalinkErrorsClear::Serialize(message);
    }
    case Types::RequestId::Enum::SCU_ISMC1_STATUS_READ: {
      return ISMC1StatusRead::Serialize(message);
    }
    case Types::RequestId::Enum::SCU_ISMC1_SIGNAL_OVERRIDE: {
      return ISMC1SignalOverride::Serialize(message);
    }
    case Types::RequestId::Enum::SCU_ISMC2_STATUS_READ: {
      return ISMC2StatusRead::Serialize(message);
    }
    case Types::RequestId::Enum::SCU_ISMC2_SIGNAL_OVERRIDE: {
      return ISMC2SignalOverride::Serialize(message);
    }
    case Types::RequestId::Enum::SCU_BUMC_STATUS_READ: {
      return BUMCStatusRead::Serialize(message);
    }
    case Types::RequestId::Enum::SCU_CHMC_STATUS_READ: {
      return CHMCStatusRead::Serialize(message);
    }
    case Types::RequestId::Enum::SCU_CHMC_SIGNAL_OVERRIDE: {
      return CHMCSignalOverride::Serialize(message);
    }
    case Types::RequestId::Enum::SCU_MONFUN_STATUS_LIST_READ: {
      return UnknownCommand::Serialize(message);
    }
    case Types::RequestId::Enum::SCU_MONFUN_ERROR_CLEAR: {
      return UnknownCommand::Serialize(message);
    }
    case Types::RequestId::Enum::SCU_SCU_SLINK_TRANSCEIVER_STATUS_READ: {
      return SLinkTransceiverStatusRead::Serialize(message);
    }
    case Types::RequestId::Enum::SCU_SCU_SLINK_ERRORS_CLEAR: {
      return SLinkErrorsClear::Serialize(message);
    }
    case Types::RequestId::Enum::SCU_SCU_OPL_STATUS_READ: {
      return OplStatusRead::Serialize(message);
    }
    case Types::RequestId::Enum::SCU_SCU_OPL_TRANSCEIVER_STATUS_READ: {
      return OplTransceiverStatusRead::Serialize(message);
    }
    case Types::RequestId::Enum::SCU_SCU_OPL_PARAMETRIZATION_READ: {
      return OplParametrizationRead::Serialize(message);
    }
    case Types::RequestId::Enum::SCU_SCU_OPL_ERRORS_CLEAR: {
      return OplErrorsClear::Serialize(message);
    }
    case Types::RequestId::Enum::SCU_ECHO: {
      return UnknownCommand::Serialize(message);
    }
    case Types::RequestId::Enum::NONE: {
      return UnknownCommand::Serialize(message);
    }
  }
  return UnknownCommand::Serialize(message);
}

}  // namespace Communication::Protocols::FbisNetwork
