/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <string>

namespace Communication {
namespace Protocols {
namespace FbisNetwork {
namespace Types {
class FbisSystemComponent {
 public:
  enum class Enum {
    SCU = 1,
    DLN = 2,
    NONE
  };

  FbisSystemComponent();

  FbisSystemComponent(const FbisSystemComponent &other);

  explicit FbisSystemComponent(const Enum &value);

  explicit FbisSystemComponent(const uint32_t &value);

  [[nodiscard]] static std::string GetTypeName();

  [[nodiscard]] Enum GetEnum() const;

  void SetEnum(const Enum &value);

  FbisSystemComponent &operator=(const FbisSystemComponent &other);

  [[nodiscard]] std::string ToString() const;

  void FromString(const std::string &str);

  void FromValue(const uint32_t &value);

  bool operator==(const FbisSystemComponent &other) const;

  bool operator!=(const FbisSystemComponent &other) const;

  friend std::ostream &operator<<(std::ostream &out, const FbisSystemComponent &moduleType) {
    return out << moduleType.ToString();
  }

 private:
  Enum value_;

  static const char kTypeName[];

  static const char kSCU[];
  static const char kDLN[];
  static const char kNone[];
};
}  // namespace Types
}  // namespace FbisNetwork
}  // namespace Protocols
}  // namespace Communication
