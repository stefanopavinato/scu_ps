/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "FbisSystemComponent.h"

namespace Communication {
namespace Protocols {
namespace FbisNetwork {
namespace Types {

const char FbisSystemComponent::kTypeName[] = "FbisSystemComponent";

const char FbisSystemComponent::kSCU[] = "SCU";
const char FbisSystemComponent::kDLN[] = "DLN";
const char FbisSystemComponent::kNone[] = "NONE";

FbisSystemComponent::FbisSystemComponent()
  : value_(Enum::NONE) {
}

FbisSystemComponent::FbisSystemComponent(const FbisSystemComponent &other)
  : value_(other.GetEnum()) {
}

FbisSystemComponent::FbisSystemComponent(const Enum &value)
  : value_(value) {
}

FbisSystemComponent::FbisSystemComponent(const uint32_t &value)
  : value_(Enum::NONE) {
  FromValue(value);
}

std::string FbisSystemComponent::GetTypeName() {
  return kTypeName;
}

FbisSystemComponent::Enum FbisSystemComponent::GetEnum() const {
  return value_;
}

void FbisSystemComponent::SetEnum(const FbisSystemComponent::Enum &value) {
  value_ = value;
}

FbisSystemComponent &FbisSystemComponent::operator=(const FbisSystemComponent &other) {
  if (this != &other) {
    value_ = other.GetEnum();
  }
  return *this;
}

bool FbisSystemComponent::operator==(const FbisSystemComponent &other) const {
  return (value_ == other.GetEnum());
}

bool FbisSystemComponent::operator!=(const FbisSystemComponent &other) const {
  return (value_ != other.GetEnum());
}

std::string FbisSystemComponent::ToString() const {
  switch (value_) {
    case Enum::SCU: {
      return kSCU;
    }
    case Enum::DLN: {
      return kDLN;
    }
    case Enum::NONE: {
      return kNone;
    }
  }
  return kNone;
}

void FbisSystemComponent::FromString(const std::string &str) {
  if (str == kSCU) {
    value_ = Enum::SCU;
  } else if (str == kDLN) {
    value_ = Enum::DLN;
  } else {
    value_ = Enum::NONE;
  }
}

void FbisSystemComponent::FromValue(const uint32_t &value) {
  auto tempVal = static_cast<Enum>(value);
  if (tempVal == Enum::SCU) {
    value_ = Enum::SCU;
  } else if (tempVal == Enum::DLN) {
    value_ = Enum::DLN;
  } else {
    value_ = Enum::NONE;
  }
}

}  // namespace Types
}  // namespace FbisNetwork
}  // namespace Protocols
}  // namespace Communication
