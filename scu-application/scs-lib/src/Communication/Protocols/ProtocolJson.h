/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "ProtocolBase.h"

namespace Communication::Protocols {
class ProtocolJson : public ProtocolBase {
 public:
  explicit ProtocolJson(
    Logger::ILogger *logger);

  ~ProtocolJson() override = default;

  [[nodiscard]] std::experimental::optional<std::unique_ptr<Messages::MessageBase>> Deserialize(
    std::unique_ptr<std::vector<char>> input) const override;

  bool OnMessageReceived(Messages::MessageBase *message) override;

  bool Visit(Messages::Message *message) override;

 private:
  static const char kMessageItems[];
};
}  // namespace Communication::Protocols
