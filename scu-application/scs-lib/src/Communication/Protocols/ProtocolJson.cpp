/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <experimental/optional>
#include <json/json.h>
#include <sstream>
#include "Logger/ILogger.h"
#include "Messages/MessageTemplate.h"
#include "Communication/Protocols/GenericTerminal/JsonMessageItemFactory.h"
#include "ProtocolJson.h"

namespace Communication::Protocols {

const char ProtocolJson::kMessageItems[] = "messageItems";

ProtocolJson::ProtocolJson(Logger::ILogger *logger)
  : ProtocolBase(
  logger,
  Types::ProtocolType::Enum::JSON) {
}

bool ProtocolJson::OnMessageReceived(Messages::MessageBase *message) {
  return message->Accept(this);
}

bool ProtocolJson::Visit(Messages::Message *message) {
  auto messageJson = dynamic_cast<Messages::MessageTemplate<Messages::MessageItemJson> *>(message);
  if (messageJson->GetMessageTemplateItems().size() > 1) {
    Logger()->Warning("Only the first message item will be sent.");
  } else if (messageJson->GetMessageTemplateItems().empty()) {
    Logger()->Warning("No message item found.");
    return false;
  }
  // Get json value
  auto jsonValue = Json::JsonMessageItemFactory::Serialize(
  *messageJson->GetMessageTemplateItems().front());
  // Create styled writer
  auto str = jsonValue.value().toStyledString();
  // Set output vector
  out_ = std::make_unique<std::vector<char>>();
  out_->insert(
    std::begin(*out_),
    std::begin(str),
    std::end(str));
  return true;
}

std::experimental::optional<std::unique_ptr<Messages::MessageBase>> ProtocolJson::Deserialize(
  std::unique_ptr<std::vector<char>> input) const {
  // Parse input to json
  ::Json::Value value;
  std::string errors;
  ::Json::CharReaderBuilder builder;

  auto inputStream = std::stringstream();
  inputStream << input->data();

  if (!::Json::parseFromStream(builder, inputStream, &value, &errors)) {
    Logger()->Error("Failed to parse json");
    return std::experimental::nullopt;
  }
  // Parse message items array
  if (!value.isMember(kMessageItems) || !value[kMessageItems].isArray()) {
    return std::experimental::nullopt;
  }
  // Create message
  auto message = std::make_unique<Messages::MessageTemplate<Messages::MessageItemJson>>();
  // Iterate message items array
  for (const auto &item : value[kMessageItems]) {
    // Parse message items
    auto messageItem = Json::JsonMessageItemFactory::DeSerialize(item);
    if (!messageItem) {
      Logger()->Error("Failed to create message");
      return std::experimental::nullopt;
    }
    message->AddMessageItem(std::move(messageItem.value()));
  }
  return std::move(message);
}

}  // namespace Communication::Protocols
