/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Messages/MessageVisitorBase.h"
#include "ProtocolBase.h"

namespace Communication::Protocols {
class ProtocolArbitration: public ProtocolBase {
 public:
  explicit ProtocolArbitration(
    Logger::ILogger * logger);

  ~ProtocolArbitration() override = default;

  [[nodiscard]] std::experimental::optional<std::unique_ptr<Messages::MessageBase>> Deserialize(
    std::unique_ptr<std::vector<char>> input) const override;

  bool OnMessageReceived(Messages::MessageBase *message) override;

  bool Visit(Messages::MessageLock * message) override;
};
}  // namespace Communication::Protocols
