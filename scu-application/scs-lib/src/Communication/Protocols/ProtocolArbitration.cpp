/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Logger/ILogger.h"
#include "Messages/MessageLock.h"
#include "Messages/Message.h"
#include "ProtocolArbitration.h"

namespace Communication::Protocols {

ProtocolArbitration::ProtocolArbitration(Logger::ILogger * logger)
  : ProtocolBase(
  logger,
  Types::ProtocolType::Enum::ARBITRATION) {
}

bool ProtocolArbitration::OnMessageReceived(Messages::MessageBase *message) {
  return message->Accept(this);
}

bool ProtocolArbitration::Visit(Messages::MessageLock * message) {
  // Clear out vector
  out_ = std::make_unique<std::vector<char>>();
  // Serialize lock id
  auto lockId = message->GetLockId();
  out_->push_back(static_cast<char>((lockId >> 24) & 0xFF));
  out_->push_back(static_cast<char>((lockId >> 16) & 0xFF));
  out_->push_back(static_cast<char>((lockId >> 8) & 0xFF));
  out_->push_back(static_cast<char>((lockId >> 0) & 0xFF));
  // Serialize status
  auto status = message->GetLockStatus();
  out_->push_back(static_cast<char>(status.GetValue()));
  // Serialize timestamp time
  auto time = message->GetTimeStamp().GetTime();
  out_->push_back(static_cast<char>((time >> 24) & 0xFF));
  out_->push_back(static_cast<char>((time >> 16) & 0xFF));
  out_->push_back(static_cast<char>((time >> 8) & 0xFF));
  out_->push_back(static_cast<char>((time >> 0) & 0xFF));
  // Serialize timestamp priority
  auto priority = message->GetTimeStamp().GetPriority();
  out_->push_back(static_cast<char>((priority >> 24) & 0xFF));
  out_->push_back(static_cast<char>((priority >> 16) & 0xFF));
  out_->push_back(static_cast<char>((priority >> 8) & 0xFF));
  out_->push_back(static_cast<char>((priority >> 0) & 0xFF));
  return true;
}

std::experimental::optional<std::unique_ptr<Messages::MessageBase>> ProtocolArbitration::Deserialize(
  std::unique_ptr<std::vector<char>> input) const {
  // Deserializer lock id
  uint32_t lockId = input->at(0) << 24;
  lockId |= input->at(1) << 16;
  lockId |= input->at(2) << 8;
  lockId |= input->at(3) << 0;
  // Deserializer status
  auto lockStatusEnum = ResourceSharing::LockStatus::ToEnum(input->at(4));
  // Deserialize timestamp time
  uint32_t time = input->at(5) << 24;
  time |= input->at(6) << 16;
  time |= input->at(7) << 8;
  time |= input->at(8) << 0;
  // Deserialize timestamp priority
  uint32_t priority = input->at(9) << 24;
  priority |= input->at(10) << 16;
  priority |= input->at(11) << 8;
  priority |= input->at(12) << 0;
  // Create message
  auto message = std::make_unique<Messages::MessageLock>(
    lockId,
    ResourceSharing::TimeStamp(time, priority),
    ResourceSharing::LockStatus(lockStatusEnum));
  return std::move(message);
}

}  // namespace Communication::Protocols
