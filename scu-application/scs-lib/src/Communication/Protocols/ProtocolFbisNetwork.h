/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "ProtocolBase.h"

namespace Communication::Protocols {
class ProtocolFbisNetwork : public ProtocolBase {
 public:
  explicit ProtocolFbisNetwork(Logger::ILogger *logger);

  ~ProtocolFbisNetwork() override = default;

  [[nodiscard]] std::experimental::optional<std::unique_ptr<Messages::MessageBase>> Deserialize(
    std::unique_ptr<std::vector<char>> input) const override;

  bool OnMessageReceived(Messages::MessageBase *message) override;

  bool Visit(Messages::Message *message) override;
};
}  // namespace Communication::Protocols
