/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <memory>
#include <vector>
#include <experimental/optional>
#include "Communication/Types/ProtocolType.h"
#include "Messages/MessageVisitorBase.h"

namespace Logger {
class ILogger;
}  // namespace Logger
namespace Messages {
class Message;
}  // namespace Messages
namespace Communication::Protocols {
class ProtocolBase : public Messages::MessageVisitorBase {
 public:
  ProtocolBase(
    Logger::ILogger *logger,
    const Types::ProtocolType::Enum &protocolType);

  virtual ~ProtocolBase() = default;

  [[nodiscard]] Logger::ILogger *Logger() const;

  [[nodiscard]] Types::ProtocolType GetProtocolType() const;

  [[nodiscard]] std::experimental::optional<std::unique_ptr<std::vector<char>>> Serialize(
    Messages::MessageBase *message);

  [[nodiscard]] virtual std::experimental::optional<std::unique_ptr<Messages::MessageBase>> Deserialize(
    std::unique_ptr<std::vector<char>> input) const = 0;

 protected:
  std::unique_ptr<std::vector<char>> out_;

 private:
  Logger::ILogger *const logger_;

  const Types::ProtocolType protocolType_;
};
}  // namespace Communication::Protocols
