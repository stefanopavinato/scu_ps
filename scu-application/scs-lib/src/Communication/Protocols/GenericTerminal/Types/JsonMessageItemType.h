/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <string>

namespace Communication::Protocols::Json::Types {
class JsonMessageItemType {
 public:
  enum class Enum {
    SET_REQUEST,
    SET_RESPONSE,
    GET_REQUEST,
    GET_RESPONSE,
    NONE
  };

  JsonMessageItemType();

  explicit JsonMessageItemType(const Enum &value);

  explicit JsonMessageItemType(const std::string &name);

  [[nodiscard]] static std::string GetTypeName();

  [[nodiscard]] Enum GetValue() const;

  void SetValue(const Enum &value);

  [[nodiscard]] bool operator==(const JsonMessageItemType &other) const;

  [[nodiscard]] std::string ToString() const;

  void FromString(const std::string &str);

  void FromValue(const uint32_t &value);

 private:
  Enum value_;

  static const char kTypeName[];

 public:
  static const char kSetRequest[];
  static const char kSetResponse[];
  static const char kGetRequest[];
  static const char kGetResponse[];
  static const char kNone[];
};
}  // namespace Communication::Protocols::Json::Types
