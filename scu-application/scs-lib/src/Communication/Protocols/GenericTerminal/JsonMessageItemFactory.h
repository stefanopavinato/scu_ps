/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <experimental/optional>
#include <memory>
#include <json/json.h>
#include "Messages/MessageItemJson.h"
#include "Messages/Message.h"
#include "Types/JsonMessageItemType.h"

namespace Communication::Protocols::Json {
class JsonMessageItemFactory {
 public:
  static std::experimental::optional<std::unique_ptr<Messages::MessageItemJson>> DeSerialize(
    const ::Json::Value &input);

  static std::experimental::optional<::Json::Value> Serialize(
    const Messages::MessageItemJson &messageItem);

  static std::experimental::optional<::Json::Value> Serialize(
    const Types::JsonMessageItemType &request,
    const std::string &path,
    const Messages::Visitor::Types::AttributeType &attribute,
    const ::Types::ValueType &valueType,
    const std::string &value,
    const int32_t &levels,
    const int32_t &details);

 private:
  static const char kMessageType[];
  static const char kData[];
  static const char kStatus[];
};

}  // namespace Communication::Protocols::Json
