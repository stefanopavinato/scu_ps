/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <experimental/optional>
#include <memory>
#include <json/json.h>
#include "Messages/MessageItem.h"
#include "Messages/Message.h"

namespace Communication::Protocols::Json {
class GenericTerminalMessageAns {
 public:
  static std::experimental::optional<std::unique_ptr<Messages::MessageItem>> DeSerialize(
    const ::Json::Value &input);

  static std::experimental::optional<std::unique_ptr<std::vector<char>>> Serialize(
    const Messages::MessageItem &message);
};

}  // namespace Communication::Protocols::Json
