/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <json/json.h>
#include "Types/JsonDetails.h"
#include "Messages/Visitor/VisitorOverview.h"
#include "Communication/Protocols/GenericTerminal/Types/JsonMessageItemType.h"
#include "JsonMessageOverviewRequest.h"

namespace Communication::Protocols::Json {

const char JsonMessageOverviewRequest::kPath[] = "path";
const char JsonMessageOverviewRequest::kLevels[] = "levels";
const char JsonMessageOverviewRequest::kDetails[] = "details";

::Json::Value JsonMessageOverviewRequest::Serialize(
  const std::string &path,
  const int32_t &levels,
  const int32_t &details) {
  auto json = ::Json::Value();
  json[kPath] = path;
  json[kLevels] = levels;
  json[kDetails] = details;
  return json;
}

std::experimental::optional<std::unique_ptr<Messages::MessageItemJson>> JsonMessageOverviewRequest::DeSerialize(
  const ::Json::Value &input) {
  // Parse path
  if (!input.isMember(kPath) || !input[kPath].isString()) {
    return std::experimental::nullopt;
  }
  std::string path = input[kPath].asString();
  // Parse levels
  if (!input.isMember(kLevels) || !input[kLevels].isInt()) {
    return std::experimental::nullopt;
  }
  int32_t levels = input[kLevels].asInt();
  // Parse details
  if (!input.isMember(kDetails) || !input[kDetails].isInt()) {
    return std::experimental::nullopt;
  }
  auto details = ::Types::JsonDetails(input[kDetails].asInt());
  // Set message item
  auto messageItem = std::make_unique<Messages::MessageItemJson>(
    "name",
    path,
    std::make_unique<Messages::Visitor::VisitorOverview>(
      levels,
      details));
  return std::move(messageItem);
}

}  // namespace Communication::Protocols::Json
