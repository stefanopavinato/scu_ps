/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <experimental/optional>
#include <memory>
#include "Messages/MessageItemJson.h"
#include "Messages/Message.h"

namespace Communication::Protocols::Json {
class JsonMessageOverviewRequest {
 public:
  static ::Json::Value Serialize(
    const std::string &path,
    const int32_t &levels,
    const int32_t &details);

  static std::experimental::optional<std::unique_ptr<Messages::MessageItemJson>> DeSerialize(
    const ::Json::Value &input);

 private:
  static const char kMessageType[];
  static const char kData[];
  static const char kPath[];
  static const char kLevels[];
  static const char kDetails[];
};

}  // namespace Communication::Protocols::Json
