/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Messages/Visitor/VisitorJsonSet.h"
#include "Communication/Protocols/GenericTerminal/Types/JsonMessageItemType.h"
#include "JsonMessageItemSetRequest.h"

namespace Communication::Protocols::Json {

const char JsonMessageItemSetRequest::kPath[] = "path";
const char JsonMessageItemSetRequest::kAttribute[] = "attribute";
const char JsonMessageItemSetRequest::kValueType[] = "valueType";
const char JsonMessageItemSetRequest::kValue[] = "value";

::Json::Value JsonMessageItemSetRequest::Serialize(
  const std::string &path,
  const Messages::Visitor::Types::AttributeType &attribute,
  const ::Types::ValueType &valueType,
  const std::string &value) {
  ::Json::Value json;
  json[kPath] = path;
  json[kAttribute] = attribute.ToString();
  json[kValueType] = valueType.ToString();
  json[kValue] = value;
  return json;
}

std::experimental::optional<std::unique_ptr<Messages::MessageItemJson>> JsonMessageItemSetRequest::DeSerialize(
  const ::Json::Value &input) {
  // Decleare variables
  ::Types::TestVariantValue value;
  ::Types::ValueType valueType;
  // Parse path
  if (!input.isMember(kPath) || !input[kPath].isString()) {
    return std::experimental::nullopt;
  }
  std::string path = input[kPath].asString();
  // Parse attribute
  if (!input.isMember(kAttribute) || !input[kAttribute].isString()) {
    return std::experimental::nullopt;
  }
  auto attribute = Messages::Visitor::Types::AttributeType(input[kAttribute].asString());
  // Parse value type
  if (!input.isMember(kValueType) || !input[kValueType].isString()) {
    return std::experimental::nullopt;
  }
  if (attribute.GetValue() == Messages::Visitor::Types::AttributeType::Enum::UPDATE_INTERVAL ||
      attribute.GetValue() == Messages::Visitor::Types::AttributeType::Enum::VALUE_IN_FORCE ||
      attribute.GetValue() == Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT_FORCE ||
      attribute.GetValue() == Messages::Visitor::Types::AttributeType::Enum::VALUE_IN_FORCE_ONCE ||
      attribute.GetValue() == Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT_FORCE_ONCE) {
    valueType = ::Types::ValueType(input[kValueType].asString());
    // parse value
    if (!value.FromString(valueType, input[kValue].asString())) {
      return std::experimental::nullopt;
    }
  }
  // Set message item
  auto messageItem = std::make_unique<Messages::MessageItemJson>(
    "name",
    path,
    std::make_unique<Messages::Visitor::VisitorJsonSet>(
      attribute.GetValue(),
      valueType.GetValue(),
      value));

  return std::move(messageItem);
}

}  // namespace Communication::Protocols::Json
