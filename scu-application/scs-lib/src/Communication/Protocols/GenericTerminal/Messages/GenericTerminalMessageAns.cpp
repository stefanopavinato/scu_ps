/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <vector>
#include "GenericTerminalMessageAns.h"

namespace Communication::Protocols::Json {

std::experimental::optional<std::unique_ptr<Messages::MessageItem>> GenericTerminalMessageAns::DeSerialize(
  const ::Json::Value &input) {
  // Get message type
  (void) input;
  return std::experimental::nullopt;
}

std::experimental::optional<std::unique_ptr<std::vector<char>>> GenericTerminalMessageAns::Serialize(
  const Messages::MessageItem &message) {
  (void) message;
  return std::experimental::nullopt;
}

}  // namespace Communication::Protocols::Json
