/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <experimental/optional>
#include <memory>
#include <json/json.h>
#include "Types/ValueType.h"
#include "Messages/MessageItemJson.h"

namespace Communication::Protocols::Json {
class JsonMessageItemSetRequest {
 public:
  static ::Json::Value Serialize(
    const std::string &path,
    const Messages::Visitor::Types::AttributeType &attribute,
    const ::Types::ValueType &valueType,
    const std::string &value);

  static std::experimental::optional<std::unique_ptr<Messages::MessageItemJson>> DeSerialize(
    const ::Json::Value &input);

 private:
  static const char kPath[];
  static const char kAttribute[];
  static const char kValueType[];
  static const char kValue[];
};

}  // namespace Communication::Protocols::Json
