/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <iostream>
#include <vector>
#include "Communication/Protocols/GenericTerminal/Types/JsonMessageItemType.h"
#include "Messages/JsonMessageItemSetRequest.h"
#include "Messages/JsonMessageOverviewRequest.h"
#include "Messages/GenericTerminalMessageAns.h"
#include "JsonMessageItemFactory.h"

namespace Communication::Protocols::Json {

const char JsonMessageItemFactory::kMessageType[] = "messageType";
const char JsonMessageItemFactory::kData[] = "data";
const char JsonMessageItemFactory::kStatus[] = "status";

std::experimental::optional<::Json::Value> JsonMessageItemFactory::Serialize(
  const Messages::MessageItemJson &messageItem) {
  return messageItem.GetVisitor()->GetJson();
}

std::experimental::optional<::Json::Value> JsonMessageItemFactory::Serialize(
  const Types::JsonMessageItemType &request,
  const std::string &path,
  const Messages::Visitor::Types::AttributeType &attribute,
  const ::Types::ValueType &valueType,
  const std::string &value,
  const int32_t &levels,
  const int32_t &details) {
  // Create item
  auto json = ::Json::Value();
  switch (request.GetValue()) {
    case Types::JsonMessageItemType::Enum::SET_REQUEST: {
      json[kMessageType] = request.ToString();
      json[kData] = JsonMessageItemSetRequest::Serialize(
        path,
        attribute,
        valueType,
        value);
      break;
    }
    case Types::JsonMessageItemType::Enum::SET_RESPONSE: {
      return std::experimental::nullopt;
    }
    case Types::JsonMessageItemType::Enum::GET_REQUEST: {
      json[kMessageType] = request.ToString();
      json[kData] = JsonMessageOverviewRequest::Serialize(
        path,
        levels,
        details);
      break;
    }
    case Types::JsonMessageItemType::Enum::GET_RESPONSE: {
      return std::experimental::nullopt;
    }
    case Types::JsonMessageItemType::Enum::NONE: {
      std::cerr << "Error: request not set." << std::endl;
      return std::experimental::nullopt;
    }
  }
  return std::move(json);
}

std::experimental::optional<std::unique_ptr<Messages::MessageItemJson>> JsonMessageItemFactory::DeSerialize(
  const ::Json::Value &input) {
  // Parse message item type
  if (!input.isMember(kMessageType) && !input[kMessageType].isString()) {
    return std::experimental::nullopt;
  }
  auto messageType = Types::JsonMessageItemType(input[kMessageType].asString());
  // Parse data
  if (!input.isMember(kData) && !input[kData].isObject()) {
    return std::experimental::nullopt;
  }
  // Deserialize message
  switch (messageType.GetValue()) {
    case Types::JsonMessageItemType::Enum::SET_REQUEST: {
      return JsonMessageItemSetRequest::DeSerialize(input[kData]);
    }
    case Types::JsonMessageItemType::Enum::SET_RESPONSE: {
      return std::experimental::nullopt;
    }
    case Types::JsonMessageItemType::Enum::GET_REQUEST: {
      return JsonMessageOverviewRequest::DeSerialize(input[kData]);
    }
    case Types::JsonMessageItemType::Enum::GET_RESPONSE: {
      return std::experimental::nullopt;
    }
    case Types::JsonMessageItemType::Enum::NONE: {
      return std::experimental::nullopt;
    }
  }
  return std::experimental::nullopt;
}

}  // namespace Communication::Protocols::Json
