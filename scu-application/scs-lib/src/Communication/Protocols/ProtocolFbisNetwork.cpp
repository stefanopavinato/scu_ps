/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Utils/CRC16.h"
#include "Logger/ILogger.h"
#include "Messages/MessageItem.h"
#include "Messages/Message.h"
#include "Communication/Protocols/FbisNetwork/Types/FbisSystemComponent.h"
#include "Communication/Types/RequestId.h"
#include "Communication/Protocols/FbisNetwork/FbisMessageFactory.h"
#include "ProtocolFbisNetwork.h"

namespace Communication::Protocols {

ProtocolFbisNetwork::ProtocolFbisNetwork(Logger::ILogger *logger)
  : ProtocolBase(
  logger,
  Types::ProtocolType::Enum::FBIS_NETWORK) {
}


bool ProtocolFbisNetwork::OnMessageReceived(Messages::MessageBase *message) {
  return message->Accept(this);
}

bool ProtocolFbisNetwork::Visit(Messages::Message *message) {
  // Serialize message
  auto messageFbis = dynamic_cast<Messages::MessageTemplate<Messages::MessageItemProperty> *>(message);
  // Create out buffer
  out_ = std::make_unique<std::vector<char>>();
  // Add SCOM
  out_->push_back(static_cast<char>(FbisNetwork::Types::FbisSystemComponent::Enum::SCU));
  // Add request id
  out_->push_back(static_cast<char>(message->GetRequestId().GetValue()));
  // Add status
  out_->push_back((static_cast<char>(message->MessageStatus()->GetValue())));

  if (message->MessageStatus()->GetValue() != Messages::Types::MessageStatus::Enum::OK) {
    out_->push_back(static_cast<char>(0x00));
    out_->push_back(static_cast<char>(0x00));
  } else {
    auto data = FbisNetwork::FbisMessageFactory::Serialize(*messageFbis);
    if (!data) {
      Logger()->Error("Failed to serialize message");
      return false;
    }
    // Add data length
    auto dataLength = data.value()->size();
    out_->push_back(static_cast<char>((dataLength >> 8) & 0x000000FF));
    out_->push_back(static_cast<char>(dataLength & 0x000000FF));
    // Add data
    out_->insert(
      std::end(*out_),
      std::begin(*(data.value())),
      std::end(*(data.value())));
  }
  // Calculate crc
  auto crc = Utils::CRC16::Calculate(*out_);
  // Append crc
  out_->push_back(static_cast<char>((crc >> 8) & 0x00FF));
  out_->push_back(static_cast<char>(crc & 0x00FF));
  // Return vector
  return true;
}

std::experimental::optional<std::unique_ptr<Messages::MessageBase>> ProtocolFbisNetwork::Deserialize(
  std::unique_ptr<std::vector<char>> input) const {
  // Check crc
  if (Utils::CRC16::Calculate(*input) != 0) {
    Logger()->Error("CRC error");
    auto message = std::make_unique<Messages::MessageTemplate<Messages::MessageItemProperty>>();
    message->MessageStatus()->SetValue(Messages::Types::MessageStatus::Enum::CRC_ERROR);
    return std::move(message);
  }
  // Get SCOM
  auto scom = FbisNetwork::Types::FbisSystemComponent(
    static_cast<uint32_t>((*input)[0]));
  if (scom.GetEnum() != FbisNetwork::Types::FbisSystemComponent::Enum::SCU) {
    Logger()->Error("Wrong scom");
    auto message = std::make_unique<Messages::MessageTemplate<Messages::MessageItemProperty>>();
    message->MessageStatus()->SetValue(Messages::Types::MessageStatus::Enum::WRONG_SYSTEM_COMPONENT);
    return std::move(message);
  }
  // Get request id
  auto requestId = Types::RequestId(
    static_cast<uint32_t>((*input)[1]));
  if (requestId.GetValue() == Types::RequestId::Enum::NONE) {
    Logger()->Error("Wrong request id");
    auto message = std::make_unique<Messages::MessageTemplate<Messages::MessageItemProperty>>();
    message->MessageStatus()->SetValue(Messages::Types::MessageStatus::Enum::INVALID_REQUEST_ID);
    return std::move(message);
  }
  auto messageLength = static_cast<uint32_t>((*input)[2]) + 5;
  if (messageLength != input->size()) {
    Logger()->Error("Wrong data size");
    auto message = std::make_unique<Messages::MessageTemplate<Messages::MessageItemProperty>>();
    message->MessageStatus()->SetValue(Messages::Types::MessageStatus::Enum::INVALID_DATA_LENGTH);
    return std::move(message);
  }
  // Create internal message
  auto message = FbisNetwork::FbisMessageFactory::DeSerialize(
    requestId,
    std::move(input));
  if (!message) {
    Logger()->Error("Failed to create message");
    return std::experimental::nullopt;
  }
  // Return internal message
  return std::move(message.value());
}

}  // namespace Communication::Protocols
