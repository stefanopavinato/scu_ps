/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <memory>
#include <list>
#include <unordered_map>
#include "Utils/ThreadBase.h"
#include "ResourceSharing/Address.h"
#include "Messages/MessageVisitorBase.h"
#include "IClientHandlerAddClient.h"

namespace Logger {
class ILogger;
}  // namespace Logger
namespace Communication {
namespace ConnectionListener {
class ConnectionListenerBase;
}  // namespace ConnectionListener
class ClientHandler : public Utils::ThreadBase, public IClientHandlerAddClient {
 public:
  ClientHandler(
    Logger::ILogger *logger,
    std::string lockIp);

  ~ClientHandler() override = default;

  bool AddConnectionListener(
    std::unique_ptr<ConnectionListener::ConnectionListenerBase> connectionListener);

  bool AddClient(
    std::unique_ptr<Clients::ClientBase> clientNew) override;

  [[nodiscard]] bool HasClient(const std::string &address) override;

  bool OnMessageReceived(Messages::MessageBase *message) override;

  bool Visit(Messages::Message * message) override;

  bool Visit(Messages::MessageLock * message) override;

 private:
  [[nodiscard]] bool Run() override;

  [[nodiscard]] bool Initialize() override;

  [[nodiscard]] bool DeInitialize() override;

  Logger::ILogger *const logger_;

  std::mutex mutexClients_;

  std::unordered_map<int32_t, std::unique_ptr<ConnectionListener::ConnectionListenerBase>> connectionListeners_;

  std::unordered_map<int32_t, std::unique_ptr<Clients::ClientBase>> clients_;

  std::list<std::unique_ptr<Clients::ClientBase>> lockClients_;

  const std::string lockIp_;

  static const size_t kMaxClients;

  static const char kName[];
};
}  // namespace Communication
