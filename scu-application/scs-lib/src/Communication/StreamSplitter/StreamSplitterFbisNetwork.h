/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "StreamSplitterBase.h"

namespace Communication::StreamSplitter {
class StreamSplitterFbisNetwork : public StreamSplitterBase {
 public:
  ~StreamSplitterFbisNetwork() override = default;

  std::experimental::optional<std::unique_ptr<std::vector<char>>> Unpack(
    std::unique_ptr<std::vector<char>> input) override;

  std::experimental::optional<std::unique_ptr<std::vector<char>>> Pack(
    std::unique_ptr<std::vector<char>> message) override;

 private:
  std::unique_ptr<std::vector<char>> streamSegment_;

  uint32_t length_;
};
}  // namespace Communication::StreamSplitter
