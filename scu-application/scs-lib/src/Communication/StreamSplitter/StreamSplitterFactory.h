/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <memory>
#include <experimental/optional>
#include "Communication/Types/ProtocolType.h"

namespace Communication {
namespace StreamSplitter {
class StreamSplitterBase;

class StreamSplitterFactory {
 public:
  static std::experimental::optional<std::unique_ptr<StreamSplitterBase>> Create(
    const Types::ProtocolType::Enum &protocolType);
};

}  // namespace StreamSplitter
}  // namespace Communication
