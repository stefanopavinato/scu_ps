/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <memory>
#include <vector>
#include <experimental/optional>

namespace Communication {
namespace StreamSplitter {
class StreamSplitterBase {
 public:
  virtual ~StreamSplitterBase() = default;

  virtual std::experimental::optional<std::unique_ptr<std::vector<char>>> Unpack(
    std::unique_ptr<std::vector<char>> input) = 0;

  virtual std::experimental::optional<std::unique_ptr<std::vector<char>>> Pack(
    std::unique_ptr<std::vector<char>> message) = 0;
};
}  // namespace StreamSplitter
}  // namespace Communication
