/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "StreamSplitterFbisNetwork.h"
#include "StreamSplitterTerminal.h"
#include "StreamSplitterFactory.h"

namespace Communication::StreamSplitter {

std::experimental::optional<std::unique_ptr<StreamSplitterBase>> StreamSplitterFactory::Create(
  const Types::ProtocolType::Enum &protocolType) {
  switch (protocolType) {
    case Types::ProtocolType::Enum::FBIS_NETWORK: {
      return std::make_unique<StreamSplitterFbisNetwork>();
    }
    case Types::ProtocolType::Enum::JSON: {
      return std::make_unique<StreamSplitterTerminal>();
    }
    case Types::ProtocolType::Enum::ARBITRATION: {
      return std::make_unique<StreamSplitterTerminal>();
    }
    case Types::ProtocolType::Enum::NONE: {
      return std::experimental::nullopt;
    }
  }
  return std::experimental::nullopt;
}

}  // namespace Communication::StreamSplitter
