/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "StreamSplitterBase.h"

namespace Communication::StreamSplitter {
class StreamSplitterTerminal : public StreamSplitterBase {
 public:
  ~StreamSplitterTerminal() override = default;

  std::experimental::optional<std::unique_ptr<std::vector<char>>> Unpack(
    std::unique_ptr<std::vector<char>> input) override;

  std::experimental::optional<std::unique_ptr<std::vector<char>>> Pack(
    std::unique_ptr<std::vector<char>> message) override;

 private:
  std::unique_ptr<std::vector<char>> streamSegment_;

  uint32_t length_;

  static const uint32_t kSizeLength;
};
}  // namespace Communication::StreamSplitter
