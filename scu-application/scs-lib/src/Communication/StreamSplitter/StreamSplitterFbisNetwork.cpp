/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "StreamSplitterFbisNetwork.h"

namespace Communication::StreamSplitter {

std::experimental::optional<std::unique_ptr<std::vector<char>>> StreamSplitterFbisNetwork::Unpack(
  std::unique_ptr<std::vector<char>> input) {
  // Append stream segment
  if (!streamSegment_) {
    streamSegment_ = std::move(input);
  } else {
    streamSegment_->insert(
      std::end(*streamSegment_),
      std::begin(*input),
      std::end(*input));
  }
  // Test minimum stream segment length
  if (streamSegment_->size() < 4) {
    return std::experimental::nullopt;
  }
  // Get length
  if (length_ == 0) {
    length_ = (static_cast<uint32_t>((*streamSegment_)[2]) & 0x00FF);
    length_ += 5;
//    length_ += ((static_cast<uint32_t>((*streamSegment_)[3]) & 0x00FF) << 8);
//    length_ += 6;
  }
  // Check stream segment length
  if (streamSegment_->size() < length_) {
    return std::experimental::nullopt;
  }
  // Resize out stream
  streamSegment_->resize(length_);
  // Reset length
  length_ = 0;
  return std::move(streamSegment_);
}

std::experimental::optional<std::unique_ptr<std::vector<char>>> StreamSplitterFbisNetwork::Pack(
  std::unique_ptr<std::vector<char>> message) {
  return std::move(message);
}

}  // namespace Communication::StreamSplitter

