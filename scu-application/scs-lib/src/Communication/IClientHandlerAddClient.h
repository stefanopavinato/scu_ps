/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <memory>
#include "Messages/MessageVisitorBase.h"
#include "ResourceSharing/Address.h"

namespace Communication {
namespace Clients {
class ClientBase;
}  // namespace Clients
class IClientHandlerAddClient : public Messages::MessageVisitorBase {
 public:
  IClientHandlerAddClient() = default;

  virtual ~IClientHandlerAddClient() = default;

  [[nodiscard]] virtual bool HasClient(const std::string &address) = 0;

  virtual bool AddClient(std::unique_ptr<Clients::ClientBase> client) = 0;
};
}  // namespace Communication
