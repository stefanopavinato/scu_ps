/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Logger/ILogger.h"
#include "Communication/StreamSplitter/StreamSplitterBase.h"
#include "Communication/StreamSplitter/StreamSplitterFactory.h"
#include "Communication/Protocols/ProtocolBase.h"
#include "Communication/Clients/ClientTCP.h"
#include "Communication/ClientHandler.h"
#include "ConnectionListenerTCP.h"

namespace Communication::ConnectionListener {

const char ConnectionListenerTCP::kName[] = "ConnectionListenerTCP";

const uint32_t ConnectionListenerTCP::kMaxClients = 5;

ConnectionListenerTCP::ConnectionListenerTCP(
  Logger::ILogger *logger,
  Messages::MessageVisitorBase *messageHandler,
  Protocols::ProtocolBase *protocol,
  const uint16_t &port)
  : ConnectionListenerBase(
  logger,
  messageHandler,
  kName,
  protocol), port_(port) {
}

uint16_t ConnectionListenerTCP::GetPort() const {
  return port_;
}

bool ConnectionListenerTCP::Run() {
  // Get protocol
  auto protocol = GetProtocol();
  if (!protocol) {
    logger_->Error("Protocol not set");
    return false;
  }
  // Create stream splitter
  auto streamSplitter = StreamSplitter::StreamSplitterFactory::Create(
    protocol->GetProtocolType().GetValue());
  if (!streamSplitter) {
    logger_->Error("Stream splitter not set");
    return false;
  }
  // Create client
  auto client = std::make_unique<Clients::ClientTCP>(
    logger_,
    protocol,
    std::move(streamSplitter.value()));

  struct timeval tv{};
  tv.tv_sec = 0;
  tv.tv_usec = 100000;

  fd_set rfds;
  FD_ZERO(&rfds);
  FD_SET(GetFileDescriptor(), &rfds);

  auto n = select(GetFileDescriptor() + 1, &rfds, nullptr, nullptr, &tv);
  if (n < 0) {
    logger_->Error("Select error");
    return false;
  } else if (n == 0) {
    // Select timeout
    return true;
  }

  // Accept connection and get file descriptor
  auto fileDescriptor = accept(
    GetFileDescriptor(),
    reinterpret_cast<sockaddr *>(&client->socketAddress_),
    &client->socketAddressLength_);
  if (fileDescriptor < 0) {
    logger_->Error("Failed to accept connection");
    return false;
  }
  client->SetFileDescriptor(fileDescriptor);

  // Connect client
  if (!client->Connect(GetFileDescriptor())) {
    logger_->Error("Failed to connect new client");
    return true;
  }
  // Set message handler
  if (!client->SetMessageHandler(GetMessageHandler())) {
    logger_->Error("Failed to set message handler");
    return false;
  }
  // Get client handler
  auto clientHandler = GetClientHandler();
  if (!clientHandler) {
    logger_->Error("Client handler not set");
    return true;
  }
  // Add client to client handler
  if (!clientHandler->AddClient(std::move(client))) {
    logger_->Error("Failed to add client");
    return true;
  }
  return true;
}

bool ConnectionListenerTCP::Initialize() {
  // Setup socket listener
  SetFileDescriptor(socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK, 0));
  if (GetFileDescriptor() < 0) {
    logger_->Error("Socket listener create error");
    return false;
  }

  int reuse = 1;
  auto a = setsockopt(
    GetFileDescriptor(),
    SOL_SOCKET, SO_REUSEADDR,
    (const char *) &reuse,
    sizeof(reuse));
  if (a < 0) {
    logger_->Error("Failed to set socket options");
    return false;
  }
  // Bind server to socket
  listenerSocketAddress_.sin_family = AF_INET;
  listenerSocketAddress_.sin_port = htons(port_);
  listenerSocketAddress_.sin_addr.s_addr = htonl(INADDR_ANY);

  auto r = bind(
    GetFileDescriptor(),
    (struct sockaddr *) &listenerSocketAddress_,
    sizeof(listenerSocketAddress_));
  if (r < 0) {
    logger_->Error("Failed to bind socket");
    return false;
  }
  // Start listening
  r = listen(GetFileDescriptor(), kMaxClients);
  if (r < 0) {
    logger_->Error("Failure during listening");
    return false;
  }
  // Write log info
  logger_->Info(std::string("Protocol ").append(GetProtocol()->GetProtocolType().ToString())
                  .append(" activated on port: ").append(std::to_string(port_)));

  return true;
}

bool ConnectionListenerTCP::DeInitialize() {
  shutdown(GetFileDescriptor(), SHUT_RDWR);
  close(GetFileDescriptor());

  // Write log info
  logger_->Info(
    std::string("protocol = ").append(GetProtocol()->GetProtocolType().ToString())
      .append(", port = ").append(std::to_string(port_))
      .append(", fd = ").append(std::to_string(GetFileDescriptor())));
  return false;
}
}  // namespace Communication::ConnectionListener
