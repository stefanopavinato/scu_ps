/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <netinet/in.h>
#include "ConnectionListenerBase.h"

namespace Communication::ConnectionListener {
class ConnectionListenerTCP: public ConnectionListenerBase {
 public:
  ConnectionListenerTCP(
    Logger::ILogger *logger,
    Messages::MessageVisitorBase *messageHandler,
    Protocols::ProtocolBase *protocol,
    const uint16_t &port);

  ~ConnectionListenerTCP() override = default;

  [[nodiscard]] uint16_t GetPort() const;

 protected:
  [[nodiscard]] bool Run() override;

  [[nodiscard]] bool Initialize() override;

  [[nodiscard]] bool DeInitialize() override;

 private:
  const uint16_t port_;

  struct sockaddr_in listenerSocketAddress_{};

  static const char kName[];

  static const uint32_t kMaxClients;
};
}  // namespace Communication::ConnectionListener
