/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Utils/ThreadBase.h"

namespace Communication {
class IClientHandlerAddClient;
class ClientHandler;
namespace Protocols {
class ProtocolBase;
}  // namespace Protocols
namespace ConnectionListener {
class ConnectionListenerBase : Utils::ThreadBase {
  friend class Communication::ClientHandler;

 public:
  ConnectionListenerBase(
    Logger::ILogger *logger,
    Messages::MessageVisitorBase *messageHandler,
    std::string name,
    Protocols::ProtocolBase *protocol);

  ~ConnectionListenerBase() override = default;

  [[nodiscard]] std::string GetName() const;

  [[nodiscard]] int32_t GetFileDescriptor() const;

  [[nodiscard]] Protocols::ProtocolBase *GetProtocol() const;

 protected:
  void SetFileDescriptor(const int32_t &fileDescriptor);

  [[nodiscard]] IClientHandlerAddClient *GetClientHandler() const;

  [[nodiscard]] Messages::MessageVisitorBase *GetMessageHandler() const;

  bool SetClientHandler(IClientHandlerAddClient *clientHandler);

  Logger::ILogger *const logger_;

 private:
  Messages::MessageVisitorBase *const messageHandler_;

  const std::string name_;

  int32_t fileDescriptor_;

  Protocols::ProtocolBase *const protocol_;

  IClientHandlerAddClient *clientHandler_;
};
}  // namespace ConnectionListener
}  // namespace Communication
