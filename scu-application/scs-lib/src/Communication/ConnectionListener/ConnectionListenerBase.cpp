/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Logger/ILogger.h"
#include "Communication/Protocols/ProtocolBase.h"
#include "Communication/IClientHandlerAddClient.h"
#include "ConnectionListenerBase.h"

namespace Communication::ConnectionListener {

ConnectionListenerBase::ConnectionListenerBase(
  Logger::ILogger *logger,
  Messages::MessageVisitorBase *messageHandler,
  std::string name,
  Protocols::ProtocolBase *protocol)
  : Utils::ThreadBase(logger, name)
  , logger_(logger)
  , messageHandler_(messageHandler)
  , name_(std::move(name))
  , fileDescriptor_(0)
  , protocol_(protocol)
  , clientHandler_(nullptr) {
}

std::string ConnectionListenerBase::GetName() const {
  return name_;
}

int32_t ConnectionListenerBase::GetFileDescriptor() const {
  return fileDescriptor_;
}

void ConnectionListenerBase::SetFileDescriptor(const int32_t &fileDescriptor) {
  fileDescriptor_ = fileDescriptor;
}

Protocols::ProtocolBase *ConnectionListenerBase::GetProtocol() const {
  return protocol_;
}

IClientHandlerAddClient *ConnectionListenerBase::GetClientHandler() const {
  return clientHandler_;
}

Messages::MessageVisitorBase *ConnectionListenerBase::GetMessageHandler() const {
  return messageHandler_;
}

bool ConnectionListenerBase::SetClientHandler(IClientHandlerAddClient *clientHandler) {
  if (clientHandler == nullptr) {
    logger_->Error("Client handler not set");
    return false;
  }
  clientHandler_ = clientHandler;
  return true;
}
}  // namespace Communication::ConnectionListener
