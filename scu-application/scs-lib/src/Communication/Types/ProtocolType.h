/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <string>

namespace Communication {
namespace Types {
class ProtocolType {
 public:
  enum class Enum {
    FBIS_NETWORK,
    JSON,
    ARBITRATION,
    NONE
  };

  ProtocolType();

  explicit ProtocolType(const Enum &value);

  [[nodiscard]] static std::string GetTypeName();

  [[nodiscard]] Enum GetValue() const;

  void SetValue(const Enum &value);

  [[nodiscard]] bool operator==(const ProtocolType &other) const;

  [[nodiscard]] std::string ToString() const;

  void FromString(const std::string &str);

 private:
  Enum value_;

  static const char kTypeName[];

  static const char kFbisNetwork[];
  static const char kJson[];
  static const char kArbitration[];
  static const char kNone[];
};
}  // namespace Types
}  // namespace Communication
