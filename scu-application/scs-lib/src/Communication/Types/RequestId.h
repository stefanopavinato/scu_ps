/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <string>

namespace Communication {
namespace Types {
class RequestId {
 public:
  enum class Enum {
    FIRMWARE_REGISTER_VALUE_READ = 0,
    FIRMWARE_REGISTER_VALUE_WRITE = 1,
    SCU_SER_VERSION_READ = 5,
    SCU_SMOD_INFO_READ = 10,
    SCU_BEAM_SWITCH_OFF_REQUEST_STATUS = 15,
    SCU_BEAM_SWITCH_OFF_REQUEST_WRITE = 16,
    SCU_SFP_STATUS_READ = 17,
    SCU_TRANSCEIVER_STATUS_READ = 18,
    SCU_MC_TEMPERATURE_READ = 20,
    SCU_SERIALIZER_TEMPERATURE_READ = 21,
    SCU_FAN_UNIT_TEMPERATURE_READ = 22,
    SCU_PSU_TEMPERATURE_READ = 23,
    SCU_SERIALIZER_STATUS_READ = 30,
    SCU_SERIALIZER_OPERATING_HOURS_WRITE = 31,
    SCU_EVENTHANDLER_STATUS_READ = 35,
    SCU_FAN_UNIT_STATUS_READ = 40,
    FAN_SPEED_WRITE = 41,
    FAN_OPERATING_HOURS_WRITE = 42,
    SCU_PSU_STATUS_READ = 50,
    SCU_MC_INSERTION_STATUS_READ = 59,
    SCU_EXPECTED_MC_TYPE_READ = 60,
    SCU_MC_FAST_FRAME_SIGNAL_READ = 61,
    SCU_MC_NESTED_DATA_STRUCTURE_READ = 62,
    SCU_MC_USER_DATA_READ = 63,
    SCU_MC_OPERATING_HOURS_WRITE = 70,
    SCU_MC_RELAIS_SWITCHING_COUNT_WRITE = 71,
    SCU_CLMC_STATUS_READ = 80,
    SCU_CLMC_SIGNAL_OVERRIDE = 81,
    SCU_CLMC_DATALINK_ERRORS_READ = 85,
    SCU_CLMC_DATALINK_ERRORS_CLEAR = 86,
    SCU_RS485MC_STATUS_READ = 90,
    SCU_RS485MC_SIGNAL_OVERRIDE = 91,
    SCU_RS485MC_ETH_STATUS_READ = 92,
    SCU_RS485MC_ETH_SWITCH_RESET = 93,
    SCU_RS485MC_ETH_POWER_DOWN = 94,
    SCU_LVDSMC_STATUS_READ = 100,
    SCU_LVDSMC_SIGNAL_OVERRIDE = 101,
    SCU_LVDSMC_DATALINK_ERRORS_READ = 105,
    SCU_LVDSMC_DATALINK_ERRORS_CLEAR = 106,
    SCU_ISMC1_STATUS_READ = 110,
    SCU_ISMC1_SIGNAL_OVERRIDE = 111,
    SCU_ISMC2_STATUS_READ = 120,
    SCU_ISMC2_SIGNAL_OVERRIDE = 121,
    SCU_BUMC_STATUS_READ = 130,
    SCU_CHMC_STATUS_READ = 140,
    SCU_CHMC_SIGNAL_OVERRIDE = 141,
    SCU_MONFUN_STATUS_LIST_READ = 180,
    SCU_MONFUN_ERROR_CLEAR = 185,
    SCU_SCU_SLINK_TRANSCEIVER_STATUS_READ = 201,
    SCU_SCU_SLINK_ERRORS_CLEAR = 203,
    SCU_SCU_OPL_STATUS_READ = 220,
    SCU_SCU_OPL_TRANSCEIVER_STATUS_READ = 221,
    SCU_SCU_OPL_PARAMETRIZATION_READ = 222,
    SCU_SCU_OPL_ERRORS_CLEAR = 223,
    SCU_ECHO = 0xee,
    NONE = 0xff
  };

  RequestId();

  explicit RequestId(const Enum &value);

  explicit RequestId(const uint32_t &value);

  [[nodiscard]] static std::string GetTypeName();

  [[nodiscard]] Enum GetValue() const;

  void SetValue(const Enum &value);

  [[nodiscard]] std::string ToString() const;

  void FromString(const std::string &str);

  void FromValue(const uint32_t &str);

  [[nodiscard]] bool operator==(const RequestId &other) const;

 private:
  Enum value_;

  static const char kTypeName[];

  static const char kFirmwareRegisterValueRead[];
  static const char kFirmwareRegisterValueWrite[];
  static const char kScuSerializerVersionRead[];
  static const char kInfoRead[];
  static const char kScuBeamSwitchOffRequestStatus[];
  static const char kScuBeamSwitchOffRequestWrite[];
  static const char kScuSfpStatusRead[];
  static const char kScuTransceiverStatusRead[];
  static const char kMcTemperatureRead[];
  static const char kScuSerializerTemperatureRead[];
  static const char kScuFanUnitTemperatureRead[];
  static const char kScuPsuTemperatureRead[];
  static const char kScuSerializerStatusRead[];
  static const char kScuSerializerOperatingHoursWrite[];
  static const char kScuEventhandlerStatusRead[];
  static const char kScuFanUnitStatusRead[];
  static const char kFanSpeedWrite[];
  static const char kFanOperatingHoursWrite[];
  static const char kScuPsuStatusRead[];
  static const char kScuMcInsertionStatusRead[];
  static const char kScuExpectedMcTypeRead[];
  static const char kScuMcFastFrameSignalRead[];
  static const char kScuMcNestedDataStructureRead[];
  static const char kScuMcUserDataRead[];
  static const char kScuMcOperatingHoursWrite[];
  static const char kScuMcRelaisSwitchingCountWrite[];
  static const char kScuClmcStatusRead[];
  static const char kScuClmcSignalOverride[];
  static const char kScuClmcDatalinkErrorsRead[];
  static const char kScuClmcDatalinkErrorsClear[];
  static const char kScuRs485McStatusRead[];
  static const char kScuRs485McSignalOverride[];
  static const char kScuRs485McEthStatusRead[];
  static const char kScuRs485McEthSwitchReset[];
  static const char kScuRs485McEthPowerDown[];
  static const char kScuLvdsmcStatusRead[];
  static const char kScuLvdsmcSignalOverride[];
  static const char kScuLvdsmcDatalinkErrorsRead[];
  static const char kScuLvdsmcDatalinkErrorsClear[];
  static const char kScuIsmc1StatusRead[];
  static const char kScuIsmc1SignalOverrride[];
  static const char kScuIsmc2StatusRead[];
  static const char kScuIsmc2SignalOverride[];
  static const char kScuBumcStatusRead[];
  static const char kScuChmcStatusRead[];
  static const char kScuChmcSignalOverride[];
  static const char kScuMonfunStatusListRead[];
  static const char kScuMonfunErrorClear[];
  static const char kScuSLinkTransceiverStatusRead[];
  static const char kScuSLinkErrorsClear[];
  static const char kScuOplStatusRead[];
  static const char kScuOplTransceiverStatusRead[];
  static const char kScuOplParametrizationRead[];
  static const char kScuOplErrorsClear[];
  static const char kScuEcho[];
  static const char kNone[];
};

}  // namespace Types
}  // namespace Communication
