/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "RequestId.h"

namespace Communication {
namespace Types {

const char RequestId::kTypeName[] = "RequestId";

const char RequestId::kFirmwareRegisterValueRead[] = "FIRMWARE_REGISTER_VALUE_READ";
const char RequestId::kFirmwareRegisterValueWrite[] = "FIRMWARE_REGISTER_VALUE_WRITE";
const char RequestId::kScuSerializerVersionRead[] = "SCU_SER_VERSION_READ";
const char RequestId::kInfoRead[] = "INFO_READ";
const char RequestId::kScuBeamSwitchOffRequestStatus[] = "SCU_BEAM_SWITCH_OFF_REQUEST_STATUS";
const char RequestId::kScuBeamSwitchOffRequestWrite[] = "SCU_BEAM_SWITCH_OFF_REQUEST_WRITE";
const char RequestId::kScuSfpStatusRead[] = "SCU_SFP_STATUS_READ";
const char RequestId::kScuTransceiverStatusRead[] = "SCU_TRANSCEIVER_STATUS_READ";
const char RequestId::kMcTemperatureRead[] = "MC_TEMPERATURE_READ";
const char RequestId::kScuSerializerTemperatureRead[] = "SCU_SERIALIZER_TEMPERATURE_READ";
const char RequestId::kScuFanUnitTemperatureRead[] = "SCU_FAN_UNIT_TEMPERATURE_READ";
const char RequestId::kScuPsuTemperatureRead[] = "SCU_PSU_TEMPERATURE_READ";
const char RequestId::kScuSerializerStatusRead[] = "SCU_SERIALIZER_STATUS_READ";
const char RequestId::kScuSerializerOperatingHoursWrite[] = "SCU_SERIALIZER_OPERATING_HOURS_WRITE";
const char RequestId::kScuEventhandlerStatusRead[] = "SCU_EVENTHANDLER_STATUS_READ";
const char RequestId::kScuFanUnitStatusRead[] = "SCU_FAN_UNIT_STATUS_READ";
const char RequestId::kFanSpeedWrite[] = "FAN_SPEED_WRITE";
const char RequestId::kFanOperatingHoursWrite[] = "FAN_OPERATING_HOURS_WRITE";
const char RequestId::kScuPsuStatusRead[] = "SCU_PSU_STATUS_READ";
const char RequestId::kScuMcInsertionStatusRead[] = "SCU_MC_INSERTION_STATUS_READ";
const char RequestId::kScuExpectedMcTypeRead[] = "SCU_EXPECTED_MC_TYPE_READ";
const char RequestId::kScuMcFastFrameSignalRead[] = "SCU_MC_FAST_FRAME_SIGNAL_READ";
const char RequestId::kScuMcNestedDataStructureRead[] = "SCU_MC_NESTED_DATA_STRUCTURE_READ";
const char RequestId::kScuMcUserDataRead[] = "SCU_MC_USER_DATA_READ";
const char RequestId::kScuMcOperatingHoursWrite[] = "SCU_MC_OPERATING_HOURS_WRITE";
const char RequestId::kScuMcRelaisSwitchingCountWrite[] = "SCU_MC_RELAIS_SWITCHING_COUNT_WRITE";
const char RequestId::kScuClmcStatusRead[] = "SCU_CLMC_STATUS_READ";
const char RequestId::kScuClmcSignalOverride[] = "SCU_CLMC_SIGNAL_OVERRIDE";
const char RequestId::kScuClmcDatalinkErrorsRead[] = "SCU_CLMC_DATALINK_ERRORS_READ";
const char RequestId::kScuClmcDatalinkErrorsClear[] = "SCU_CLMC_DATALINK_ERRORS_CLEAR";
const char RequestId::kScuRs485McStatusRead[] = "SCU_RS485MC_STATUS_READ";
const char RequestId::kScuRs485McSignalOverride[] = "SCU_RS485MC_SIGNAL_OVERRIDE";
const char RequestId::kScuRs485McEthStatusRead[] = "SCU_RS485MC_ETH_STATUS_READ";
const char RequestId::kScuRs485McEthSwitchReset[] = "SCU_RS485MC_ETH_SWITCH_RESET";
const char RequestId::kScuRs485McEthPowerDown[] = "SCU_RS485MC_ETH_POWER_DOWN";
const char RequestId::kScuLvdsmcStatusRead[] = "SCU_LVDSMC_STATUS_READ";
const char RequestId::kScuLvdsmcSignalOverride[] = "SCU_LVDSMC_SIGNAL_OVERRIDE";
const char RequestId::kScuLvdsmcDatalinkErrorsRead[] = "SCU_LVDSMC_DATALINK_ERRORS_READ";
const char RequestId::kScuLvdsmcDatalinkErrorsClear[] = "SCU_LVDSMC_DATALINK_ERRORS_CLEAR";
const char RequestId::kScuIsmc1StatusRead[] = "SCU_ISMC1_STATUS_READ";
const char RequestId::kScuIsmc1SignalOverrride[] = "SCU_ISMC1_SIGNAL_OVERRIDE";
const char RequestId::kScuIsmc2StatusRead[] = "SCU_ISMC2_STATUS_READ";
const char RequestId::kScuIsmc2SignalOverride[] = "SCU_ISMC2_SIGNAL_OVERRIDE";
const char RequestId::kScuBumcStatusRead[] = "SCU_BUMC_STATUS_READ";
const char RequestId::kScuChmcStatusRead[] = "SCU_CHMC_STATUS_READ";
const char RequestId::kScuChmcSignalOverride[] = "SCU_CHMC_SIGNAL_OVERRIDE";
const char RequestId::kScuMonfunStatusListRead[] = "SCU_MONFUN_STATUS_LIST_READ";
const char RequestId::kScuMonfunErrorClear[] = "SCU_MONFUN_ERROR_CLEAR";
const char RequestId::kScuSLinkTransceiverStatusRead[] = "SCU_SCU_SLINK_TRANSCEIVER_STATUS_READ";
const char RequestId::kScuSLinkErrorsClear[] = "SCU_SCU_SLink_ERRORS_CLEAR";
const char RequestId::kScuOplStatusRead[] = "SCU_SCU_OPL_STATUS_READ";
const char RequestId::kScuOplTransceiverStatusRead[] = "SCU_SCU_OPL_TRANSCEIVER_STATUS_READ";
const char RequestId::kScuOplParametrizationRead[] = "SCU_SCU_OPL_PARAMETRIZATION_READ";
const char RequestId::kScuOplErrorsClear[] = "SCU_SCU_OPL_ERRORS_CLEAR";
const char RequestId::kScuEcho[] = "SCU_ECHO";
const char RequestId::kNone[] = "NONE";

RequestId::RequestId()
  : value_(Enum::NONE) {
}

RequestId::RequestId(const Enum &value)
  : value_(value) {
}

std::string RequestId::GetTypeName() {
  return kTypeName;
}

RequestId::RequestId(const uint32_t &value)
  : value_(Enum::NONE) {
  FromValue(value);
}

RequestId::Enum RequestId::GetValue() const {
  return value_;
}

void RequestId::SetValue(const RequestId::Enum &value) {
  value_ = value;
}

bool RequestId::operator==(const RequestId &other) const {
  return value_ == other.GetValue();
}

std::string RequestId::ToString() const {
  switch (value_) {
    case Enum::FIRMWARE_REGISTER_VALUE_READ: {
      return kFirmwareRegisterValueRead;
    }
    case Enum::FIRMWARE_REGISTER_VALUE_WRITE: {
      return kFirmwareRegisterValueWrite;
    }
    case Enum::SCU_SER_VERSION_READ: {
      return kScuSerializerVersionRead;
    }
    case Enum::SCU_SMOD_INFO_READ: {
      return kInfoRead;
    }
    case Enum::SCU_BEAM_SWITCH_OFF_REQUEST_STATUS: {
      return kScuBeamSwitchOffRequestStatus;
    }
    case Enum::SCU_BEAM_SWITCH_OFF_REQUEST_WRITE: {
      return kScuBeamSwitchOffRequestWrite;
    }
    case Enum::SCU_SFP_STATUS_READ: {
      return kScuSfpStatusRead;
    }
    case Enum::SCU_TRANSCEIVER_STATUS_READ: {
      return kScuTransceiverStatusRead;
    }
    case Enum::SCU_MC_TEMPERATURE_READ: {
      return kMcTemperatureRead;
    }
    case Enum::SCU_SERIALIZER_TEMPERATURE_READ: {
      return kScuSerializerTemperatureRead;
    }
    case Enum::SCU_FAN_UNIT_TEMPERATURE_READ: {
      return kScuFanUnitTemperatureRead;
    }
    case Enum::SCU_PSU_TEMPERATURE_READ: {
      return kScuPsuTemperatureRead;
    }
    case Enum::SCU_SERIALIZER_STATUS_READ: {
      return kScuSerializerStatusRead;
    }
    case Enum::SCU_SERIALIZER_OPERATING_HOURS_WRITE: {
      return kScuSerializerOperatingHoursWrite;
    }
    case Enum::SCU_EVENTHANDLER_STATUS_READ: {
      return kScuEventhandlerStatusRead;
    }
    case Enum::SCU_FAN_UNIT_STATUS_READ: {
      return kScuFanUnitStatusRead;
    }
    case Enum::FAN_SPEED_WRITE: {
      return kFanSpeedWrite;
    }
    case Enum::FAN_OPERATING_HOURS_WRITE: {
      return kFanOperatingHoursWrite;
    }
    case Enum::SCU_PSU_STATUS_READ: {
      return kScuPsuStatusRead;
    }
    case Enum::SCU_MC_INSERTION_STATUS_READ: {
      return kScuMcInsertionStatusRead;
    }
    case Enum::SCU_EXPECTED_MC_TYPE_READ: {
      return kScuExpectedMcTypeRead;
    }
    case Enum::SCU_MC_FAST_FRAME_SIGNAL_READ: {
      return kScuMcFastFrameSignalRead;
    }
    case Enum::SCU_MC_NESTED_DATA_STRUCTURE_READ: {
      return kScuMcNestedDataStructureRead;
    }
    case Enum::SCU_MC_USER_DATA_READ: {
      return kScuMcUserDataRead;
    }
    case Enum::SCU_MC_OPERATING_HOURS_WRITE: {
      return kScuMcOperatingHoursWrite;
    }
    case Enum::SCU_MC_RELAIS_SWITCHING_COUNT_WRITE: {
      return kScuMcRelaisSwitchingCountWrite;
    }
    case Enum::SCU_CLMC_STATUS_READ: {
      return kScuClmcStatusRead;
    }
    case Enum::SCU_CLMC_SIGNAL_OVERRIDE: {
      return kScuClmcSignalOverride;
    }
    case Enum::SCU_CLMC_DATALINK_ERRORS_READ: {
      return kScuClmcDatalinkErrorsRead;
    }
    case Enum::SCU_CLMC_DATALINK_ERRORS_CLEAR: {
      return kScuClmcDatalinkErrorsClear;
    }
    case Enum::SCU_RS485MC_STATUS_READ: {
      return kScuRs485McStatusRead;
    }
    case Enum::SCU_RS485MC_SIGNAL_OVERRIDE: {
      return kScuRs485McSignalOverride;
    }
    case Enum::SCU_RS485MC_ETH_STATUS_READ: {
      return kScuRs485McEthStatusRead;
    }
    case Enum::SCU_RS485MC_ETH_SWITCH_RESET: {
      return kScuRs485McEthSwitchReset;
    }
    case Enum::SCU_RS485MC_ETH_POWER_DOWN: {
      return kScuRs485McEthPowerDown;
    }
    case Enum::SCU_LVDSMC_STATUS_READ: {
      return kScuLvdsmcStatusRead;
    }
    case Enum::SCU_LVDSMC_SIGNAL_OVERRIDE: {
      return kScuLvdsmcSignalOverride;
    }
    case Enum::SCU_LVDSMC_DATALINK_ERRORS_READ: {
      return kScuLvdsmcDatalinkErrorsRead;
    }
    case Enum::SCU_LVDSMC_DATALINK_ERRORS_CLEAR: {
      return kScuLvdsmcDatalinkErrorsClear;
    }
    case Enum::SCU_ISMC1_STATUS_READ: {
      return kScuIsmc1StatusRead;
    }
    case Enum::SCU_ISMC1_SIGNAL_OVERRIDE: {
      return kScuIsmc1SignalOverrride;
    }
    case Enum::SCU_ISMC2_STATUS_READ: {
      return kScuIsmc2StatusRead;
    }
    case Enum::SCU_ISMC2_SIGNAL_OVERRIDE: {
      return kScuIsmc2SignalOverride;
    }
    case Enum::SCU_BUMC_STATUS_READ: {
      return kScuBumcStatusRead;
    }
    case Enum::SCU_CHMC_STATUS_READ: {
      return kScuChmcStatusRead;
    }
    case Enum::SCU_CHMC_SIGNAL_OVERRIDE: {
      return kScuChmcSignalOverride;
    }
    case Enum::SCU_MONFUN_STATUS_LIST_READ: {
      return kScuMonfunStatusListRead;
    }
    case Enum::SCU_MONFUN_ERROR_CLEAR: {
      return kScuMonfunErrorClear;
    }
    case Enum::SCU_SCU_SLINK_TRANSCEIVER_STATUS_READ: {
      return kScuSLinkTransceiverStatusRead;
    }
    case Enum::SCU_SCU_SLINK_ERRORS_CLEAR: {
      return kScuSLinkErrorsClear;
    }
    case Enum::SCU_SCU_OPL_STATUS_READ: {
      return kScuOplStatusRead;
    }
    case Enum::SCU_SCU_OPL_TRANSCEIVER_STATUS_READ: {
      return kScuOplTransceiverStatusRead;
    }
    case Enum::SCU_SCU_OPL_PARAMETRIZATION_READ: {
      return kScuOplParametrizationRead;
    }
    case Enum::SCU_SCU_OPL_ERRORS_CLEAR: {
      return kScuOplErrorsClear;
    }
    case Enum::SCU_ECHO: {
      return kScuEcho;
    }
    case Enum::NONE: {
      return kNone;
    }
  }
  return kNone;
}

void RequestId::FromString(const std::string &str) {
  if (str == kFirmwareRegisterValueRead) {
    value_ = Enum::FIRMWARE_REGISTER_VALUE_READ;
  } else if (str == kFirmwareRegisterValueWrite) {
    value_ = Enum::FIRMWARE_REGISTER_VALUE_WRITE;
  } else if (str == kScuSerializerVersionRead) {
    value_ = Enum::SCU_SER_VERSION_READ;
  } else if (str == kInfoRead) {
    value_ = Enum::SCU_SMOD_INFO_READ;
  } else if (str == kScuBeamSwitchOffRequestStatus) {
    value_ = Enum::SCU_BEAM_SWITCH_OFF_REQUEST_STATUS;
  } else if (str == kScuBeamSwitchOffRequestWrite) {
    value_ = Enum::SCU_BEAM_SWITCH_OFF_REQUEST_WRITE;
  } else if (str == kScuSfpStatusRead) {
    value_ = Enum::SCU_SFP_STATUS_READ;
  } else if (str == kScuTransceiverStatusRead) {
    value_ = Enum::SCU_TRANSCEIVER_STATUS_READ;
  } else if (str == kMcTemperatureRead) {
    value_ = Enum::SCU_MC_TEMPERATURE_READ;
  } else if (str == kScuSerializerTemperatureRead) {
    value_ = Enum::SCU_SERIALIZER_TEMPERATURE_READ;
  } else if (str == kScuFanUnitTemperatureRead) {
    value_ = Enum::SCU_FAN_UNIT_TEMPERATURE_READ;
  } else if (str == kScuPsuTemperatureRead) {
    value_ = Enum::SCU_PSU_TEMPERATURE_READ;
  } else if (str == kScuSerializerStatusRead) {
    value_ = Enum::SCU_SERIALIZER_STATUS_READ;
  } else if (str == kScuSerializerOperatingHoursWrite) {
    value_ = Enum::SCU_SERIALIZER_OPERATING_HOURS_WRITE;
  } else if (str == kScuEventhandlerStatusRead) {
    value_ = Enum::SCU_EVENTHANDLER_STATUS_READ;
  } else if (str == kScuFanUnitStatusRead) {
    value_ = Enum::SCU_FAN_UNIT_STATUS_READ;
  } else if (str == kFanSpeedWrite) {
    value_ = Enum::FAN_SPEED_WRITE;
  } else if (str == kFanOperatingHoursWrite) {
    value_ = Enum::FAN_OPERATING_HOURS_WRITE;
  } else if (str == kScuPsuStatusRead) {
    value_ = Enum::SCU_PSU_STATUS_READ;
  } else if (str == kScuMcInsertionStatusRead) {
    value_ = Enum::SCU_MC_INSERTION_STATUS_READ;
  } else if (str == kScuExpectedMcTypeRead) {
    value_ = Enum::SCU_EXPECTED_MC_TYPE_READ;
  } else if (str == kScuMcFastFrameSignalRead) {
    value_ = Enum::SCU_MC_FAST_FRAME_SIGNAL_READ;
  } else if (str == kScuMcNestedDataStructureRead) {
    value_ = Enum::SCU_MC_NESTED_DATA_STRUCTURE_READ;
  } else if (str == kScuMcUserDataRead) {
    value_ = Enum::SCU_MC_USER_DATA_READ;
  } else if (str == kScuMcOperatingHoursWrite) {
    value_ = Enum::SCU_MC_OPERATING_HOURS_WRITE;
  } else if (str == kScuMcRelaisSwitchingCountWrite) {
    value_ = Enum::SCU_MC_RELAIS_SWITCHING_COUNT_WRITE;
  } else if (str == kScuClmcStatusRead) {
    value_ = Enum::SCU_CLMC_STATUS_READ;
  } else if (str == kScuClmcSignalOverride) {
    value_ = Enum::SCU_CLMC_SIGNAL_OVERRIDE;
  } else if (str == kScuClmcDatalinkErrorsRead) {
    value_ = Enum::SCU_CLMC_DATALINK_ERRORS_READ;
  } else if (str == kScuClmcDatalinkErrorsClear) {
    value_ = Enum::SCU_CLMC_DATALINK_ERRORS_CLEAR;
  } else if (str == kScuRs485McStatusRead) {
    value_ = Enum::SCU_RS485MC_STATUS_READ;
  } else if (str == kScuRs485McSignalOverride) {
    value_ = Enum::SCU_RS485MC_SIGNAL_OVERRIDE;
  } else if (str == kScuRs485McEthStatusRead) {
    value_ = Enum::SCU_RS485MC_ETH_STATUS_READ;
  } else if (str == kScuRs485McEthSwitchReset) {
    value_ = Enum::SCU_RS485MC_ETH_SWITCH_RESET;
  } else if (str == kScuRs485McEthPowerDown) {
    value_ = Enum::SCU_RS485MC_ETH_POWER_DOWN;
  } else if (str == kScuLvdsmcStatusRead) {
    value_ = Enum::SCU_LVDSMC_STATUS_READ;
  } else if (str == kScuLvdsmcSignalOverride) {
    value_ = Enum::SCU_LVDSMC_SIGNAL_OVERRIDE;
  } else if (str == kScuLvdsmcDatalinkErrorsRead) {
    value_ = Enum::SCU_LVDSMC_DATALINK_ERRORS_READ;
  } else if (str == kScuLvdsmcDatalinkErrorsClear) {
    value_ = Enum::SCU_LVDSMC_DATALINK_ERRORS_CLEAR;
  } else if (str == kScuIsmc1StatusRead) {
    value_ = Enum::SCU_ISMC1_STATUS_READ;
  } else if (str == kScuIsmc1SignalOverrride) {
    value_ = Enum::SCU_ISMC1_SIGNAL_OVERRIDE;
  } else if (str == kScuIsmc2StatusRead) {
    value_ = Enum::SCU_ISMC2_STATUS_READ;
  } else if (str == kScuIsmc2SignalOverride) {
    value_ = Enum::SCU_ISMC2_SIGNAL_OVERRIDE;
  } else if (str == kScuBumcStatusRead) {
    value_ = Enum::SCU_BUMC_STATUS_READ;
  } else if (str == kScuChmcStatusRead) {
    value_ = Enum::SCU_CHMC_STATUS_READ;
  } else if (str == kScuChmcSignalOverride) {
    value_ = Enum::SCU_CHMC_SIGNAL_OVERRIDE;
  } else if (str == kScuMonfunStatusListRead) {
    value_ = Enum::SCU_MONFUN_STATUS_LIST_READ;
  } else if (str == kScuMonfunErrorClear) {
    value_ = Enum::SCU_MONFUN_ERROR_CLEAR;
  } else if (str == kScuSLinkTransceiverStatusRead) {
    value_ = Enum::SCU_SCU_SLINK_TRANSCEIVER_STATUS_READ;
  } else if (str == kScuSLinkErrorsClear) {
    value_ = Enum::SCU_SCU_SLINK_ERRORS_CLEAR;
  } else if (str == kScuOplStatusRead) {
    value_ = Enum::SCU_SCU_OPL_STATUS_READ;
  } else if (str == kScuOplTransceiverStatusRead) {
    value_ = Enum::SCU_SCU_OPL_TRANSCEIVER_STATUS_READ;
  } else if (str == kScuOplParametrizationRead) {
    value_ = Enum::SCU_SCU_OPL_PARAMETRIZATION_READ;
  } else if (str == kScuOplErrorsClear) {
    value_ = Enum::SCU_SCU_OPL_ERRORS_CLEAR;
  } else if (str == kScuEcho) {
    value_ = Enum::SCU_ECHO;
  } else {
    value_ = Enum::NONE;
  }
}

void RequestId::FromValue(const uint32_t &value) {
  Enum tempVal = static_cast<Enum>(value);
  if (tempVal == Enum::FIRMWARE_REGISTER_VALUE_READ) {
    value_ = Enum::FIRMWARE_REGISTER_VALUE_READ;
  } else if (tempVal == Enum::FIRMWARE_REGISTER_VALUE_WRITE) {
    value_ = Enum::FIRMWARE_REGISTER_VALUE_WRITE;
  } else if (tempVal == Enum::SCU_SER_VERSION_READ) {
    value_ = Enum::SCU_SER_VERSION_READ;
  } else if (tempVal == Enum::SCU_SMOD_INFO_READ) {
    value_ = Enum::SCU_SMOD_INFO_READ;
  } else if (tempVal == Enum::SCU_BEAM_SWITCH_OFF_REQUEST_STATUS) {
    value_ = Enum::SCU_BEAM_SWITCH_OFF_REQUEST_STATUS;
  } else if (tempVal == Enum::SCU_BEAM_SWITCH_OFF_REQUEST_WRITE) {
    value_ = Enum::SCU_BEAM_SWITCH_OFF_REQUEST_WRITE;
  } else if (tempVal == Enum::SCU_SFP_STATUS_READ) {
    value_ = Enum::SCU_SFP_STATUS_READ;
  } else if (tempVal == Enum::SCU_TRANSCEIVER_STATUS_READ) {
    value_ = Enum::SCU_TRANSCEIVER_STATUS_READ;
  } else if (tempVal == Enum::SCU_MC_TEMPERATURE_READ) {
    value_ = Enum::SCU_MC_TEMPERATURE_READ;
  } else if (tempVal == Enum::SCU_SERIALIZER_TEMPERATURE_READ) {
    value_ = Enum::SCU_SERIALIZER_TEMPERATURE_READ;
  } else if (tempVal == Enum::SCU_FAN_UNIT_TEMPERATURE_READ) {
    value_ = Enum::SCU_FAN_UNIT_TEMPERATURE_READ;
  } else if (tempVal == Enum::SCU_PSU_TEMPERATURE_READ) {
    value_ = Enum::SCU_PSU_TEMPERATURE_READ;
  } else if (tempVal == Enum::SCU_SERIALIZER_STATUS_READ) {
    value_ = Enum::SCU_SERIALIZER_STATUS_READ;
  } else if (tempVal == Enum::SCU_SERIALIZER_OPERATING_HOURS_WRITE) {
    value_ = Enum::SCU_SERIALIZER_OPERATING_HOURS_WRITE;
  } else if (tempVal == Enum::SCU_EVENTHANDLER_STATUS_READ) {
    value_ = Enum::SCU_EVENTHANDLER_STATUS_READ;
  } else if (tempVal == Enum::SCU_FAN_UNIT_STATUS_READ) {
    value_ = Enum::SCU_FAN_UNIT_STATUS_READ;
  } else if (tempVal == Enum::FAN_SPEED_WRITE) {
    value_ = Enum::FAN_SPEED_WRITE;
  } else if (tempVal == Enum::FAN_OPERATING_HOURS_WRITE) {
    value_ = Enum::FAN_OPERATING_HOURS_WRITE;
  } else if (tempVal == Enum::SCU_PSU_STATUS_READ) {
    value_ = Enum::SCU_PSU_STATUS_READ;
  } else if (tempVal == Enum::SCU_MC_INSERTION_STATUS_READ) {
    value_ = Enum::SCU_MC_INSERTION_STATUS_READ;
  } else if (tempVal == Enum::SCU_EXPECTED_MC_TYPE_READ) {
    value_ = Enum::SCU_EXPECTED_MC_TYPE_READ;
  } else if (tempVal == Enum::SCU_MC_FAST_FRAME_SIGNAL_READ) {
    value_ = Enum::SCU_MC_FAST_FRAME_SIGNAL_READ;
  } else if (tempVal == Enum::SCU_MC_NESTED_DATA_STRUCTURE_READ) {
    value_ = Enum::SCU_MC_NESTED_DATA_STRUCTURE_READ;
  } else if (tempVal == Enum::SCU_MC_USER_DATA_READ) {
    value_ = Enum::SCU_MC_USER_DATA_READ;
  } else if (tempVal == Enum::SCU_MC_OPERATING_HOURS_WRITE) {
    value_ = Enum::SCU_MC_OPERATING_HOURS_WRITE;
  } else if (tempVal == Enum::SCU_MC_RELAIS_SWITCHING_COUNT_WRITE) {
    value_ = Enum::SCU_MC_RELAIS_SWITCHING_COUNT_WRITE;
  } else if (tempVal == Enum::SCU_CLMC_STATUS_READ) {
    value_ = Enum::SCU_CLMC_STATUS_READ;
  } else if (tempVal == Enum::SCU_CLMC_SIGNAL_OVERRIDE) {
    value_ = Enum::SCU_CLMC_SIGNAL_OVERRIDE;
  } else if (tempVal == Enum::SCU_CLMC_DATALINK_ERRORS_READ) {
    value_ = Enum::SCU_CLMC_DATALINK_ERRORS_READ;
  } else if (tempVal == Enum::SCU_CLMC_DATALINK_ERRORS_CLEAR) {
    value_ = Enum::SCU_CLMC_DATALINK_ERRORS_CLEAR;
  } else if (tempVal == Enum::SCU_RS485MC_STATUS_READ) {
    value_ = Enum::SCU_RS485MC_STATUS_READ;
  } else if (tempVal == Enum::SCU_RS485MC_SIGNAL_OVERRIDE) {
    value_ = Enum::SCU_RS485MC_SIGNAL_OVERRIDE;
  } else if (tempVal == Enum::SCU_RS485MC_ETH_STATUS_READ) {
    value_ = Enum::SCU_RS485MC_ETH_STATUS_READ;
  } else if (tempVal == Enum::SCU_RS485MC_ETH_SWITCH_RESET) {
    value_ = Enum::SCU_RS485MC_ETH_SWITCH_RESET;
  } else if (tempVal == Enum::SCU_RS485MC_ETH_POWER_DOWN) {
    value_ = Enum::SCU_RS485MC_ETH_POWER_DOWN;
  } else if (tempVal == Enum::SCU_LVDSMC_STATUS_READ) {
    value_ = Enum::SCU_LVDSMC_STATUS_READ;
  } else if (tempVal == Enum::SCU_LVDSMC_SIGNAL_OVERRIDE) {
    value_ = Enum::SCU_LVDSMC_SIGNAL_OVERRIDE;
  } else if (tempVal == Enum::SCU_LVDSMC_DATALINK_ERRORS_READ) {
    value_ = Enum::SCU_LVDSMC_DATALINK_ERRORS_READ;
  } else if (tempVal == Enum::SCU_LVDSMC_DATALINK_ERRORS_CLEAR) {
    value_ = Enum::SCU_LVDSMC_DATALINK_ERRORS_CLEAR;
  } else if (tempVal == Enum::SCU_ISMC1_STATUS_READ) {
    value_ = Enum::SCU_ISMC1_STATUS_READ;
  } else if (tempVal == Enum::SCU_ISMC1_SIGNAL_OVERRIDE) {
    value_ = Enum::SCU_ISMC1_SIGNAL_OVERRIDE;
  } else if (tempVal == Enum::SCU_ISMC2_STATUS_READ) {
    value_ = Enum::SCU_ISMC2_STATUS_READ;
  } else if (tempVal == Enum::SCU_ISMC2_SIGNAL_OVERRIDE) {
    value_ = Enum::SCU_ISMC2_SIGNAL_OVERRIDE;
  } else if (tempVal == Enum::SCU_BUMC_STATUS_READ) {
    value_ = Enum::SCU_BUMC_STATUS_READ;
  } else if (tempVal == Enum::SCU_CHMC_STATUS_READ) {
    value_ = Enum::SCU_CHMC_STATUS_READ;
  } else if (tempVal == Enum::SCU_CHMC_SIGNAL_OVERRIDE) {
    value_ = Enum::SCU_CHMC_SIGNAL_OVERRIDE;
  } else if (tempVal == Enum::SCU_MONFUN_STATUS_LIST_READ) {
    value_ = Enum::SCU_MONFUN_STATUS_LIST_READ;
  } else if (tempVal == Enum::SCU_MONFUN_ERROR_CLEAR) {
    value_ = Enum::SCU_MONFUN_ERROR_CLEAR;
  } else if (tempVal == Enum::SCU_SCU_SLINK_TRANSCEIVER_STATUS_READ) {
    value_ = Enum::SCU_SCU_SLINK_TRANSCEIVER_STATUS_READ;
  } else if (tempVal == Enum::SCU_SCU_SLINK_ERRORS_CLEAR) {
    value_ = Enum::SCU_SCU_SLINK_ERRORS_CLEAR;
  } else if (tempVal == Enum::SCU_SCU_OPL_STATUS_READ) {
    value_ = Enum::SCU_SCU_OPL_STATUS_READ;
  } else if (tempVal == Enum::SCU_SCU_OPL_TRANSCEIVER_STATUS_READ) {
    value_ = Enum::SCU_SCU_OPL_TRANSCEIVER_STATUS_READ;
  } else if (tempVal == Enum::SCU_SCU_OPL_PARAMETRIZATION_READ) {
    value_ = Enum::SCU_SCU_OPL_PARAMETRIZATION_READ;
  } else if (tempVal == Enum::SCU_SCU_OPL_ERRORS_CLEAR) {
    value_ = Enum::SCU_SCU_OPL_ERRORS_CLEAR;
  } else if (tempVal == Enum::SCU_ECHO) {
    value_ = Enum::SCU_ECHO;
  } else {
    value_ = Enum::NONE;
  }
}

}  // namespace Types
}  // namespace Communication
