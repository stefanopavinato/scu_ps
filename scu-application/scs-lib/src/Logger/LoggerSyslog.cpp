/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <syslog.h>

#include <utility>
#include "LoggerSyslog.h"

namespace Logger {

LoggerSyslog::LoggerSyslog(
  std::string applicationName,
  const Types::AlertSeverity::Enum &logLevel)
  : LoggerBase((logLevel))
  , applicationName_(std::move(applicationName)) {
}

void LoggerSyslog::LogFunc(
  const std::string &path,
  const std::string &function,
  const int &line,
  const Types::AlertSeverity::Enum &alertSeverity,
  const std::string &message) const {
  // Get file name
  size_t end = path.rfind('/') + 1;
  std::string fileName = path.substr(end);
  // Set message
  std::string outString;
  outString.append(fileName).append(":").append(std::to_string(line)).append(" ");
  outString.append(function).append("(): ");
  outString.append(message);

  int32_t syslogSeverity = 0;
  switch (alertSeverity) {
    case Types::AlertSeverity::Enum::OK: {
      return;
    }
    case Types::AlertSeverity::Enum::DEBUG: {
      syslogSeverity = LOG_DEBUG;
      break;
    }
    case Types::AlertSeverity::Enum::INFORMATIONAL: {
      syslogSeverity = LOG_INFO;
      break;
    }
    case Types::AlertSeverity::Enum::NOTICE: {
      syslogSeverity = LOG_NOTICE;
      break;
    }
    case Types::AlertSeverity::Enum::WARNING: {
      syslogSeverity = LOG_WARNING;
      break;
    }
    case Types::AlertSeverity::Enum::ERROR: {
      syslogSeverity = LOG_ERR;
      break;
    }
  }
  // Write message
  openlog(applicationName_.c_str(), LOG_PID, LOG_USER);
  syslog(syslogSeverity, "%s", outString.c_str());
  closelog();
}

}  // namespace Logger
