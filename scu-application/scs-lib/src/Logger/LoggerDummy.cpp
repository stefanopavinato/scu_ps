/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "LoggerDummy.h"

namespace Logger {

LoggerDummy::LoggerDummy()
  : LoggerBase(Types::AlertSeverity::Enum::DEBUG) {
}

void LoggerDummy::LogFunc(const std::string &path,
                          const std::string &function,
                          const int &line,
                          const Types::AlertSeverity::Enum &alertSeverity,
                          const std::string &message) const {
  (void) path;
  (void) function;
  (void) line;
  (void) alertSeverity;
  (void) message;
}
}  // namespace Logger
