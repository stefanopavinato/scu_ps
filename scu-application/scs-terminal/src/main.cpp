/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <cstring>
#include <iostream>
#include <arpa/inet.h>
#include <getopt.h>
#include "version.h"
#include <Communication/Protocols/GenericTerminal/JsonMessageItemFactory.h>
#include <Communication/StreamSplitter/StreamSplitterTerminal.h>

static const struct option long_options[] = {
  {"help",      no_argument,       nullptr, 'h'},
  {"request",   required_argument, nullptr, 'r'},
  {"path",      required_argument, nullptr, 'p'},
  {"attribute", required_argument, nullptr, 'a'},
  {"type",      required_argument, nullptr, 't'},
  {"value",     required_argument, nullptr, 'v'},
  {"levels",    required_argument, nullptr, 'l'},
  {"details",   required_argument, nullptr, 'd'},
  {"list",      required_argument, nullptr, 0},
  {"version",   no_argument,       nullptr, 1}
};

int main(int argc, char *argv[]) {
  Communication::Protocols::Json::Types::JsonMessageItemType request;
  std::string path = "/";
  Messages::Visitor::Types::AttributeType attribute;
  Types::ValueType valueType;
  std::string value;
  int32_t levels = 0;
  int32_t details = 0;
  // Parse arguments
  int index;
  int result;
  while ((result = getopt_long(argc, argv, "hr:p:a:t:v:l:d:", long_options, &index)) != -1) {
    if (result == -1) {
      break;
    }
    switch (result) {
      // Request
      case 'r': {
        request.FromString(std::string(optarg));
        if (request.GetValue() == Communication::Protocols::Json::Types::JsonMessageItemType::Enum::NONE) {
          std::cerr << "Error: visitor type valid." << std::endl;
          return EXIT_FAILURE;
        }
        break;
      }
        // Path
      case 'p': {
        path = std::string(optarg);
        break;
      }
        // Attribute
      case 'a': {
        attribute.FromString(std::string(optarg));
        if (attribute.GetValue() == Messages::Visitor::Types::AttributeType::Enum::NONE) {
          std::cerr << "Error: attribute not valid." << std::endl;
          return EXIT_FAILURE;
        }
        break;
      }
        // Value type
      case 't': {
        valueType.FromString(std::string(optarg));
        if (valueType.GetValue() == Types::ValueType::Enum::NONE) {
          std::cerr << "Error: value type not valid." << std::endl;
          return EXIT_FAILURE;
        }
        break;
      }
        // Value
      case 'v': {
        value = std::string(optarg);
        break;
      }
        // Levels
      case 'l': {
        try {
          levels = std::stoi(optarg);
        } catch (const std::exception &e) {
          std::cerr << "Error: depth value not valid" << std::endl;
          return EXIT_FAILURE;
        }
        break;
      }
        // Details
      case 'd': {
        try {
          details = std::stoi(optarg);
        } catch (const std::exception &e) {
          std::cerr << "Error: depth value not valid" << std::endl;
          return EXIT_FAILURE;
        }
        break;
      }
        // List parameters
      case 0: {
        if (std::string(optarg) == "request") {
          std::cout << "Requests:" << std::endl;
          std::cout << "  - " << Communication::Protocols::Json::Types::JsonMessageItemType::kSetRequest;
          std::cout << " (usage: scs-terminal <-r Set><-p path><-a attribute>[-t type][-v value])" << std::endl;
          std::cout << "  - " << Communication::Protocols::Json::Types::JsonMessageItemType::kGetRequest;
          std::cout << " (usage: scs-terminal <-r Get><-p path>[-l levels][-d details])" << std::endl;
          std::cout << std::endl;
        }
        if (std::string(optarg) == "attributes") {
          std::cout << "Attributes:" << std::endl;
          std::cout << "  - " << Messages::Visitor::Types::AttributeType::kComponentType << std::endl;
          std::cout << "  - " << Messages::Visitor::Types::AttributeType::kName << std::endl;
          std::cout << "  - " << Messages::Visitor::Types::AttributeType::kDescription << std::endl;
          std::cout << "  - " << Messages::Visitor::Types::AttributeType::kAlertSeverity << std::endl;
          std::cout << "  - " << Messages::Visitor::Types::AttributeType::kClass << std::endl;
          std::cout << "  - " << Messages::Visitor::Types::AttributeType::kUpdateInterval << std::endl;
          std::cout << "  - " << Messages::Visitor::Types::AttributeType::kModuleReset << std::endl;
          std::cout << "  - " << Messages::Visitor::Types::AttributeType::kModuleStop << std::endl;
          std::cout << "  - " << Messages::Visitor::Types::AttributeType::kModuleStopRelease << std::endl;
          std::cout << "  - " << Messages::Visitor::Types::AttributeType::kValueIn << std::endl;
          std::cout << "  - " << Messages::Visitor::Types::AttributeType::kValueInRead << std::endl;
          std::cout << "  - " << Messages::Visitor::Types::AttributeType::kValueInForce << std::endl;
          std::cout << "  - " << Messages::Visitor::Types::AttributeType::kValueInForceOnce << std::endl;
          std::cout << "  - " << Messages::Visitor::Types::AttributeType::kValueInIsForced << std::endl;
          std::cout << "  - " << Messages::Visitor::Types::AttributeType::kValueInForceRelease << std::endl;
          std::cout << "  - " << Messages::Visitor::Types::AttributeType::kValueOut << std::endl;
          std::cout << "  - " << Messages::Visitor::Types::AttributeType::kValueOutWrite << std::endl;
          std::cout << "  - " << Messages::Visitor::Types::AttributeType::kValueOutForce << std::endl;
          std::cout << "  - " << Messages::Visitor::Types::AttributeType::kValueOutIsForced << std::endl;
          std::cout << "  - " << Messages::Visitor::Types::AttributeType::kValueOutForceOnce << std::endl;
          std::cout << "  - " << Messages::Visitor::Types::AttributeType::kValueOutForceRelease << std::endl;
          std::cout << "  - " << Messages::Visitor::Types::AttributeType::kStatusReset << std::endl;
          std::cout << std::endl;
        }
        if (std::string(optarg) == "types") {
          std::cout << "Types:" << std::endl;
          std::cout << "  - " << ::Types::ValueType::kValueInt32 << std::endl;
          std::cout << "  - " << ::Types::ValueType::kValueInt16 << std::endl;
          std::cout << "  - " << ::Types::ValueType::kValueInt8 << std::endl;
          std::cout << "  - " << ::Types::ValueType::kValueUInt32 << std::endl;
          std::cout << "  - " << ::Types::ValueType::kValueUInt16 << std::endl;
          std::cout << "  - " << ::Types::ValueType::kValueUInt8 << std::endl;
          std::cout << "  - " << ::Types::ValueType::kValueDouble << std::endl;
          std::cout << "  - " << ::Types::ValueType::kValueFloat << std::endl;
          std::cout << "  - " << ::Types::ValueType::kInt32 << std::endl;
          std::cout << "  - " << ::Types::ValueType::kInt16 << std::endl;
          std::cout << "  - " << ::Types::ValueType::kInt8 << std::endl;
          std::cout << "  - " << ::Types::ValueType::kUInt32 << std::endl;
          std::cout << "  - " << ::Types::ValueType::kUInt16 << std::endl;
          std::cout << "  - " << ::Types::ValueType::kUInt8 << std::endl;
          std::cout << "  - " << ::Types::ValueType::kDouble << std::endl;
          std::cout << "  - " << ::Types::ValueType::kFloat << std::endl;
          std::cout << "  - " << ::Types::ValueType::kBool << std::endl;
          std::cout << "  - " << ::Types::ValueType::kString << std::endl;
          std::cout << "  - " << ::Types::ValueType::kAlertSeverity << std::endl;
          std::cout << "  - " << ::Types::ValueType::kModuleType << std::endl;
          std::cout << "  - " << ::Types::ValueType::kChronoMinutes << std::endl;
          std::cout << "  - " << ::Types::ValueType::kChronoMilliseconds << std::endl;
          std::cout << std::endl;
        }
        if (std::string(optarg) == "levels") {
          std::cout << "Levels:" << std::endl;
          std::cout << std::endl;
        }
        return EXIT_SUCCESS;
      }
        // List version
      case 1: {
        std::cout << "Revision:   " << REVISION << std::endl;
        std::cout << "Branch:     " << BRANCH << std::endl;
        std::cout << "Build date: " << BUILD_DATE << std::endl;
        std::cout << "Build time: " << BUILD_TIME << std::endl;
#ifdef VERSION_IS_DIRTY
        std::cout << "Version is dirty!" << std::endl;
#endif
        return EXIT_SUCCESS;
      }
        // Help
      case 'h':
      default: {
        std::cout << "Usage: scs-terminal [options]..." << std::endl;
        std::cout << "Access scs-service on local machine." << std::endl;
        std::cout << "Options:" << std::endl;
        std::cout << " -r, --request <request>                       set request type" << std::endl;
        std::cout << " -p, --path <path>                             set component path" << std::endl;
        std::cout << " -a, --attribute <attribute>                   set attribute of the component" << std::endl;
        std::cout << " -t, --type <type>                             set type of value" << std::endl;
        std::cout << " -v, --value <value>                           set value" << std::endl;
        std::cout << " -l, --levels <levels>                         set maximum tree level" << std::endl;
        std::cout << " -d, --details <details>                       set level of detail" << std::endl;
        std::cout << " -h, --help                                    display this help and exit" << std::endl;
        std::cout << "     --list <request|attributes|types|levels>  display available parameter" << std::endl;
        std::cout << "     --version                                 displays the installed version" << std::endl;
        std::cout << std::endl;
      }
        return EXIT_SUCCESS;
    }
  }
  // Create message item
  auto messageItem = Communication::Protocols::Json::JsonMessageItemFactory::Serialize(
    request,
    path,
    attribute,
    valueType,
    value,
    levels,
    details);
  if (!messageItem) {
    return EXIT_FAILURE;
  }
  Json::Value message;
  message["messageItems"][0] = messageItem.value();
  // Create json string
  auto out1 = message.toStyledString();
  // Convert request to char vector
  auto out = std::make_unique<std::vector<char>>();
  out->insert(
    std::begin(*out),
    std::begin(out1),
    std::end(out1));
  // std::cout << out->data() << std::endl;
  // Set address
  auto address = "127.0.0.1";
  // Create stream splitter
  auto streamSplitter = Communication::StreamSplitter::StreamSplitterTerminal();
  // Pack data
  auto outBuffer = streamSplitter.Pack(std::move(out));
  // Create socket
  auto sock = socket(AF_INET, SOCK_STREAM, 0);
  if (sock < 0) {
    std::cerr << "Error: " << std::strerror(errno) << std::endl;
    return EXIT_FAILURE;
  }
  // Fill in server ip address
  struct sockaddr_in server{};
  server.sin_family = AF_INET;
  server.sin_port = htons(20002);
  // Convert IP-address to binary form
  if (inet_pton(AF_INET, address, &server.sin_addr) <= 0) {
    std::cerr << "Error: " << std::strerror(errno) << std::endl;
    return EXIT_FAILURE;
  }
  // Connect
  if (connect(sock, (struct sockaddr *) &server, sizeof(server)) < 0) {
    std::cerr << "Error: " << std::strerror(errno) << std::endl;
    return EXIT_FAILURE;
  }
  // Send data
  {
    auto nBytes = send(
      sock,
      outBuffer.value()->data(),
      outBuffer.value()->size(),
      0);
    if (nBytes < 0) {
      std::cerr << "Error: " << std::strerror(errno) << std::endl;
      return EXIT_FAILURE;
    }
  }
  // Receive data
  bool inBufferComplete = false;
  while (!inBufferComplete) {
    auto inBuffer = std::make_unique<std::vector<char>>();
    inBuffer->resize(1024);
    {
      auto nBytes = recv(
        sock,
        inBuffer->data(),
        inBuffer->size(),
        0);
      if (nBytes < 0) {
        std::cerr << "Error: " << std::strerror(errno) << std::endl;
        return EXIT_FAILURE;
      }
    }
    // Unpack data
    auto inData = streamSplitter.Unpack(std::move(inBuffer));
    if (inData) {
      inBufferComplete = true;
      // Print result
      std::cout << inData.value()->data() << std::endl;
      return EXIT_SUCCESS;
    }
  }

  return EXIT_FAILURE;
}
