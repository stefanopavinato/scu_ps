

set(SCS_BINARY "scu-terminal")

# collect sources
file(GLOB_RECURSE SRC_BINARY "src/*.c*")
file(GLOB_RECURSE HDR_BINARY "src/*.h*")

######## Versioning ########
if (SUBVERSION_FOUND)
    if (EXISTS "${PROJECT_SOURCE_DIR}/../.svn")
        Subversion_WC_INFO(${PROJECT_SOURCE_DIR} Project)

        file(WRITE svnversion.h.txt "${Project_WC_INFO}\n \n")

        file(STRINGS svnversion.h.txt lines)
        foreach (line IN LISTS lines)
            string(FIND ${line} "Relative URL:" relativeUrl)
            if (NOT ${relativeUrl} STREQUAL "-1")
                string(REGEX REPLACE "Relative URL: \\^/" "" BRANCH ${line})
            endif ()
        endforeach ()
        file(REMOVE svnversion.h.txt)

        set(REVISION ${Project_WC_REVISION})
        set(BRANCH ${BRANCH})
        set(VERSION_IS_DIRTY ON)
    else ()
        if (NOT DEFINED REVISION)
            message(WARNING "Variable REVISION not defined")
        endif ()
        if (NOT DEFINED BRANCH)
            message(WARNING "Variable BRANCH not defined")
        endif ()
        set(SVN_VERSION_IS_DIRTY OFF)
    endif ()
endif ()

string(TIMESTAMP BUILD_DATE "%Y.%m.%d")
string(TIMESTAMP BUILD_TIME "%H:%M:%S")
set(BUILD_DATE ${BUILD_DATE})
set(BUILD_TIME ${BUILD_TIME})

# configure version.h file with the collected information
configure_file(${CMAKE_CURRENT_SOURCE_DIR}/version.h.in ${CMAKE_CURRENT_BINARY_DIR}/version.h @ONLY)

# prepare application target
add_executable(${SCS_BINARY} ${SRC_BINARY})

# include directories
target_include_directories(${SCS_BINARY}
        PUBLIC
        ${CMAKE_CURRENT_BINARY_DIR}
        PRIVATE
        src
        )

# link to dependencies
target_link_libraries(${SCS_BINARY}
        PRIVATE
        scs-lib
        )

# set compiler flags
target_compile_options(${SCS_BINARY} PRIVATE ${SCS_CFLAGS})

# test steps
setup_cpplint(${SCS_BINARY} ${SRC_BINARY} ${HDR_BINARY})
# setup_cppcheck(${SCS_BINARY} ${SRC_BINARY} ${HDR_BINARY})
# setup_cppclean(${SCS_BINARY} ${SRC_BINARY} ${HDR_BINARY})

# documentation
#setup_doxygen(${SCS_BINARY} "src" ${SRC_BINARY} ${HDR_BINARY})

# create and install the service
install(TARGETS ${SCS_BINARY}
        RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
        )
