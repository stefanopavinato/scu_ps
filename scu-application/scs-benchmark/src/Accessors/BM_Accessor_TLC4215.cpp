/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <benchmark/benchmark.h>
#include <Logger/LoggerDummy.h>
#include <SystemModules/Framework/Components/I2CDeviceLTC4215.h>
#include <SystemModules/Framework/Accessors/AccessorFile.h>

#define ADDRESS 0x49
#define PATH "/sys/bus/i2c/devices/0-0070/channel-2"

static void BM_Device_TLC4215_Read(benchmark::State& state) {
  auto logger = Logger::LoggerDummy();

  auto device = SystemModules::Framework::Components::I2CDeviceLTC4215(
    &logger,
    "name",
    "description",
    ADDRESS,
    PATH);

  auto accessor = SystemModules::Framework::Accessors::AccessorFile<int32_t, int32_t, std::string>(
    &logger,
    std::string(device.GetDevicePath()).append("/in1_input"),
    1,
    1);
  accessor.Initialize();

  int32_t value;
  for (auto _ : state) {
    accessor.Read(&value);
  }

  (void) accessor.DeInitialize();
  (void) device.DeInitialize();
}
// Register the function as benchmark
BENCHMARK(BM_Device_TLC4215_Read);
