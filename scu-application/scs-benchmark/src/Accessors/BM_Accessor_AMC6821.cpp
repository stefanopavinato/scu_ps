/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <benchmark/benchmark.h>
#include <Logger/LoggerDummy.h>
#include <SystemModules/Framework/Accessors/AccessorFile.h>

#define PATH "/sys/bus/i2c/devices/0-0071/channel-4/26-0018/hwmon/hwmon1/pwm1"

static void BM_Accessor_AMC6821_Read(benchmark::State& state) {
  auto logger = Logger::LoggerDummy();
  auto accessor = SystemModules::Framework::Accessors::AccessorFile<uint8_t, uint8_t, std::string>(
    &logger,
    PATH,
    1,
    1);
  accessor.Initialize();

  uint8_t value;
  for (auto _ : state) {
    if (!accessor.Read(&value)) {
      std::cout << "Failed to read value" << std::endl;
      return;
    }
  }

  accessor.DeInitialize();
}
// Register the function as benchmark
// BENCHMARK(BM_Accessor_AMC6821_Read);

static void BM_Accessor_AMC6821_Write(benchmark::State& state) {
  auto logger = Logger::LoggerDummy();
  auto accessor = SystemModules::Framework::Accessors::AccessorFile<uint8_t, uint8_t, std::string>(
    &logger,
    PATH,
    1,
    1);
  accessor.Initialize();

  for (auto _ : state) {
    if (!accessor.Write(200)) {
      std::cout << "Failed to write value" << std::endl;
      return;
    }
  }

  accessor.DeInitialize();
}
// Register the function as benchmark
// BENCHMARK(BM_Accessor_AMC6821_Write);
