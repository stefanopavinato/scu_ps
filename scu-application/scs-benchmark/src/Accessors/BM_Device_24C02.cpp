/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <iostream>
#include <benchmark/benchmark.h>
#include <Logger/LoggerDummy.h>
#include <SystemModules/Framework/Components/I2CDevice24C02.h>

#define ADDRESS 0x50
#define PATH "/sys/bus/i2c/devices/0-0071/channel-4"

static void BM_Device_24C02_Read(benchmark::State& state) {
  auto logger = Logger::LoggerDummy();
  for (auto _ : state) {
    try {
      SystemModules::Framework::Components::I2CDevice24C02(
        &logger,
        "name",
        "description",
        ADDRESS,
        PATH);
    } catch (...) {
      std::cout << "Failed to create device" << std::endl;
      return;
    }
  }
}
// Register the function as benchmark
// BENCHMARK(BM_Device_24C02_Read);

static void BM_Device_24C02_Write(benchmark::State& state) {
  auto logger = Logger::LoggerDummy();

  auto device = SystemModules::Framework::Components::I2CDevice24C02(
    &logger,
    "name",
    "description",
    ADDRESS,
    PATH);

  auto value = std::vector<char>();
  value.resize(16);
  value[0] = '2';
  for (auto _ : state) {
    if (value[0] == '6') {
      value[0] = '2';
    } else {
      value[0] = '6';
    }
    device.SetValue(0x30, 16, value);
    (void) device.Update(std::chrono::steady_clock::now());
  }
}
// Register the function as benchmark
// BENCHMARK(BM_Device_24C02_Write);
