/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <string>
#include <arpa/inet.h>
#include <unistd.h>
#include <SystemModules/Framework/Accessors/AccessorGPIO.h>
#include "System.h"

namespace Utils::System {

#ifdef PETALINUX_BUILD
const char System::kIfconfig[] = "ifconfig";
const char System::kEthtool[] = "ethtool";
const char System::kRoute[] = "route";
#else
const char System::kIfconfig[] = "/sbin/ifconfig";
const char System::kEthtool[] = "/sbin/ethtool";
const char System::kRoute[] = "/sbin/route";
#endif

const uint32_t System::kSerializerSlotId = 499;

const char System::kSerializerSection[] = "FBIS-SCU-SER";
const char System::kFanUnitSection[] = "FBIS-SCU-FU";
const char System::kFbisSection[] = "FBIS-TERMINAL";
const char System::kJsonSection[] = "JSON-TERMINAL";
const char System::kArbitrationSection[] = "ARBITRATION";

const char System::kHostnameAKey[] = "SER_A_NAME";
const char System::kHostnameADefaultValue[] = "SCS1600_A";

const char System::kHostnameBKey[] = "SER_B_NAME";
const char System::kHostnameBDefaultValue[] = "SCS1600_B";

// ToDo: Error if no default values provided (future extension)
const char System::kMacAddressA0Key[] = "MAC_ADDR_A";
const char System::kIpAddressA0Key[] = "TCPIP_ADDR_A";
const char System::kMacAddressB0Key[] = "MAC_ADDR_B";
const char System::kIpAddressB0Key[] = "TCPIP_ADDR_B";
const char System::kNetmaskKey[] = "SER_NETMASK";
const char System::kGateway0Key[] = "GATEWAY_IP";

const char System::kStartupSpeed[] = "STARTUP_SPEED_PWM";
const char System::kFanAEnable[] = "FAN_A_Enable";
const char System::kFanBEnable[] = "FAN_B_Enable";
const char System::kFanMEnable[] = "FAN_M_Enable";

const char System::kMaxConnects[] = "MAX_CONNECTS";
const char System::kPortNumber[] = "PORT_NUMBER";
const char System::kMaxWaitCounts[] = "CONNECTION_MAX_WAITCOUNTS";

const char System::kMacAddressA1[] = "00:0A:35:00:23:11";
const char System::kIpAddressA1[] = "192.168.1.200";

const char System::kMacAddressB1[] = "00:0A:35:00:23:12";
const char System::kIpAddressB1[] = "192.168.1.201";

const char System::kNetmask1[] = "255.255.255.0";

const uint16_t System::kJsonPortDefaultValue = 20002;
const uint16_t System::kArbitrationPortDefaultValue = 20003;

System::System(
  Logger::ILogger *logger,
  const std::string &iniFilePath)
  : logger_(logger), iniFile_(minIni(iniFilePath)) {
}


bool System::SetupNetwork() {
  std::string hostname;
  std::string macAddress0;
  std::string ipAddress0;
  std::string netmask0;
  std::string gateway0;
  std::string macAddress1;
  std::string ipAddress1;
  std::string netmask1;
  struct sockaddr_in sa{};
  logger_->Info("Setup Network: start");
  // Get serializer slot
  bool serializerSlot;
  auto gpio = SystemModules::Framework::Accessors::AccessorGPIO(
    logger_,
    kSerializerSlotId,
    Types::Direction::Enum::IN,
    false);
  if (!gpio.Initialize()) {
    logger_->Error("Failed to initialize serializer slot gpio");
    return false;
  }
  if (!gpio.Read(&serializerSlot)) {
    logger_->Error("Failed to read serializer slot gpio");
    return false;
  }
  if (!gpio.DeInitialize()) {
    logger_->Error("Failed to de-initialize serializer slot gpio");
    return false;
  }
  // Parse ini-file
  if (serializerSlot == 0) {
    logger_->Info("Serializer slot A detected");

    hostname = iniFile_.gets(
      kSerializerSection,
      kHostnameAKey,
      kHostnameADefaultValue);

    macAddress0 = iniFile_.gets(
      kSerializerSection,
      kMacAddressA0Key);

    ipAddress0 = iniFile_.gets(
      kSerializerSection,
      kIpAddressA0Key);

    macAddress1 = kMacAddressA1;
    ipAddress1 = kIpAddressA1;
  } else {
    logger_->Info("Serializer slot B detected");

    hostname = iniFile_.gets(
      kSerializerSection,
      kHostnameBKey,
      kHostnameBDefaultValue);

    macAddress0 = iniFile_.gets(
      kSerializerSection,
      kMacAddressB0Key);

    ipAddress0 = iniFile_.gets(
      kSerializerSection,
      kIpAddressB0Key);

    macAddress1 = kMacAddressB1;
    ipAddress1 = kIpAddressB1;
  }

  netmask0 = iniFile_.gets(
    kSerializerSection,
    kNetmaskKey);
  netmask1 = kNetmask1;

  gateway0 = iniFile_.gets(
    kSerializerSection,
    kGateway0Key);

  // Validate parameter
  logger_->Info(std::string("ip address0: ").append(ipAddress0));
  if (inet_pton(AF_INET, ipAddress0.c_str(), &sa.sin_addr) <= 0) {
    logger_->Error("eth0: invalid ip address");
    return false;
  }
  logger_->Info(std::string("ip address1: ").append(ipAddress1));
  if (inet_pton(AF_INET, ipAddress1.c_str(), &sa.sin_addr) <= 0) {
    logger_->Error("eth1: invalid ip address");
    return false;
  }
  logger_->Info(std::string("mac address0: ").append(macAddress0));
  logger_->Info(std::string("mac address1: ").append(macAddress1));
  logger_->Info(std::string("netmask0: ").append(netmask0));
  if (macAddress0.empty() ||
      macAddress1.empty() ||
      netmask0.empty() ||
      netmask1.empty()) {
    logger_->Error("empty parameter found");
    return false;
  }
  // Setup hostname
  logger_->Info(std::string("Set hostname to: ").append(hostname));
  sethostname(hostname.c_str(), hostname.length());

  // Setup eth0
  logger_->Info(std::string("Set eth0 to: ").append(ipAddress0).append("/").append(netmask0));
  system(std::string(kIfconfig).append(" eth0 down").c_str());
  system(std::string(kIfconfig).append(" eth0 hw ether ").append(macAddress0).c_str());
  system(std::string(kIfconfig).append(" eth0 ").append(ipAddress0).append(" netmask ").append(netmask0).c_str());
  system(std::string(kIfconfig).append(" eth0 up").c_str());
  system(std::string(kRoute).append(" add default gw ").append(gateway0).c_str());

  // Setup eth1
  logger_->Info(std::string("Set eth1 to: ").append(ipAddress1).append("/").append(netmask1));
  system(std::string(kIfconfig).append(" eth1 down").c_str());
  system(std::string(kIfconfig).append(" eth1 hw ether ").append(macAddress1).c_str());
  system(std::string(kIfconfig).append(" eth1 ").append(ipAddress1).append(" netmask ").append(netmask1).c_str());
  system(std::string(kIfconfig).append(" eth1 up").c_str());
  system(std::string(kEthtool).append(" -s eth1 speed 100 duplex full autoneg off").c_str());

  logger_->Info("Setup Network: done");
  return true;
}

bool System::GetFbisPort(
  uint16_t *port) {
  auto retVal = iniFile_.geti(
    kFbisSection,
    kPortNumber);
  *port = static_cast<uint16_t>(retVal);
  return true;
}

bool System::GetJsonPort(
  uint16_t *port) {
  auto retVal = iniFile_.geti(
    kJsonSection,
    kPortNumber,
    kJsonPortDefaultValue);
  *port = static_cast<uint16_t>(retVal);
  return true;
}

bool System::GetArbitrationPort(
  uint16_t * port) {
  auto retVal = iniFile_.geti(
    kArbitrationSection,
    kPortNumber,
    kArbitrationPortDefaultValue);
  *port = static_cast<uint16_t>(retVal);
  return true;
}

}  // namespace Utils::System
