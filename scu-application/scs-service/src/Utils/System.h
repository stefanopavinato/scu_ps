/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <minini/minIni.h>
#include <Logger/ILogger.h>

namespace Utils::System {
class System {
 public:
  System(
    Logger::ILogger *logger,
    const std::string &iniFilePath);

  [[nodiscard]] bool SetupNetwork();

  [[nodiscard]] bool GetFbisPort(
    uint16_t *port);

  [[nodiscard]] bool GetJsonPort(
    uint16_t *port);

  [[nodiscard]] bool GetArbitrationPort(
    uint16_t * port);

 private:
  Logger::ILogger *logger_;

  minIni iniFile_;

  static const char kIfconfig[];
  static const char kEthtool[];
  static const char kRoute[];

  static const uint32_t kSerializerSlotId;

  static const char kSerializerSection[];
  static const char kFanUnitSection[];
  static const char kFbisSection[];
  static const char kJsonSection[];
  static const char kArbitrationSection[];

  static const char kHostnameAKey[];
  static const char kHostnameADefaultValue[];

  static const char kHostnameBKey[];
  static const char kHostnameBDefaultValue[];

  static const char kMacAddressA0Key[];
  static const char kIpAddressA0Key[];
  static const char kMacAddressB0Key[];
  static const char kIpAddressB0Key[];
  static const char kGateway0Key[];
  static const char kNetmaskKey[];

  static const char kStartupSpeed[];
  static const char kFanAEnable[];
  static const char kFanBEnable[];
  static const char kFanMEnable[];

  static const char kMaxConnects[];
  static const char kPortNumber[];
  static const char kMaxWaitCounts[];

  static const char kMacAddressA1[];
  static const char kIpAddressA1[];

  static const char kMacAddressB1[];
  static const char kIpAddressB1[];

  static const char kNetmask1[];

  static const uint16_t kJsonPortDefaultValue;
  static const uint16_t kArbitrationPortDefaultValue;
//  SFP_TXDISABLE=0
//  SFP_RATE_SEL0=1
//  SFP_RATE_SEL1=1
};
}  // namespace Utils::System
