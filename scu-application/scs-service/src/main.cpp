/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <memory>
#include <getopt.h>
#include <iostream>
#include <fstream>
#include <Logger/LoggerDebug.h>
#include <Logger/LoggerSyslog.h>
#include <Logger/LoggerHandler.h>
#include <Messages/MessageHandler.h>
#include <Communication/StreamSplitter/StreamSplitterBase.h>
#include <Communication/Protocols/ProtocolFbisNetwork.h>
#include <Communication/Protocols/ProtocolJson.h>
#include <Communication/Protocols/ProtocolArbitration.h>
#include <Communication/ConnectionListener/ConnectionListenerTCP.h>
#include <Communication/Clients/ClientBase.h>
#include <Communication/ClientHandler.h>
#include <ResourceSharing/LockHandler.h>
#include <WorkloadBalancing/WorkloadBalanceHandler.h>
#include <SystemModules/Framework/Accessors/AccessorScuRegister.h>
#include <SystemModules/Framework/Components/ModuleHandler.h>
#include <SystemModules/Modules/MezzanineCards/States/MCStateBase.h>
#include <SystemModules/Modules/MezzanineCards/ModuleMezzanineCard.h>
#include <SystemModules/Modules/Configuration/Configuration.h>
#include <SystemModules/Modules/InternalModules/States/IMStateBase.h>
#include <SystemModules/Modules/InternalModules/ModuleInternal.h>
#include "version.h"
#include "Utils/System.h"

#define UPDATE_INTERVAL_us 20000

#define DEFAULT_INI_FILE_PATH "/etc/default/scs.d/ini-file"
#define CONFIGURATION_FILE_PATH "/home/root/configuration.json"

static const struct option long_options[] = {
  {"help",    no_argument,       nullptr, 'h'},
  {"path",    required_argument, nullptr, 'p'},
  {"version", no_argument,       nullptr, 1}
};

int main(int argc, char *argv[]) {
  std::string iniFilePath = DEFAULT_INI_FILE_PATH;
  // Parse arguments
  int index;
  int result;
  while ((result = getopt_long(argc, argv, "hp:", long_options, &index)) != -1) {
    if (result == -1) {
      break;
    }
    switch (result) {
      // List version
      case 1: {
        std::cout << "Revision:   " << REVISION << std::endl;
        std::cout << "Branch:     " << BRANCH << std::endl;
        std::cout << "Build date: " << BUILD_DATE << std::endl;
        std::cout << "Build time: " << BUILD_TIME << std::endl;
#ifdef VERSION_IS_DIRTY
        std::cout << "Version is dirty!" << std::endl;
#endif
        return EXIT_SUCCESS;
      }
        // Path
      case 'p': {
        iniFilePath = std::string(optarg);
        break;
      }
        // Help
      case 'h':
      default: {
        std::cout << "Usage: scs-service [options]..." << std::endl;
        std::cout << "The scs-service monitors the inputs and the outputs of the device." << std::endl;
        std::cout << "Options:" << std::endl;
        std::cout << "     --version    displays the installed version" << std::endl;
        std::cout << std::endl;
      }
        return EXIT_SUCCESS;
    }
  }
  // Create and setup logger
  auto loggerHandler = Logger::LoggerHandler();
  // Add debug logger
  if (!loggerHandler.AddLogger(std::make_unique<Logger::LoggerDebug>(
    Types::AlertSeverity::Enum::INFORMATIONAL))) {
    loggerHandler.Error("Failed to add debug logger to logger handler");
    return EXIT_FAILURE;
  }
  // Add syslog logger
  if (!loggerHandler.AddLogger(std::make_unique<Logger::LoggerSyslog>(
    "scs",
    Types::AlertSeverity::Enum::INFORMATIONAL))) {
    loggerHandler.Error("Failed to add syslog logger to logger handler");
    return EXIT_FAILURE;
  }
  // Reset scu registers
  SystemModules::Framework::Accessors::AccessorScuRegister<bool>::ResetHW(
    &loggerHandler);
  // Parse configuration
  loggerHandler.Info("Parse configuration");
  auto configuration = SystemModules::Modules::Configuration::Configuration(
    &loggerHandler,
    iniFilePath);
  // Save configuration
  auto outFile = std::ofstream();
  outFile.open(CONFIGURATION_FILE_PATH);
  outFile << configuration.ToJson().toStyledString();
  outFile.close();
  // Get serializer specific configuration
  std::string ip;
  std::string ipSite;
  if (configuration.Serializer()->DetectedSlot()->Value() == "A") {
    ip = configuration.Serializer()->SlotA()->Network()->Interface0()->IpAddress()->Value();
    ipSite = configuration.Serializer()->SlotB()->Network()->Interface0()->IpAddress()->Value();
  } else {
    ip = configuration.Serializer()->SlotB()->Network()->Interface0()->IpAddress()->Value();
    ipSite = configuration.Serializer()->SlotA()->Network()->Interface0()->IpAddress()->Value();
  }
  // Setup ini file parser
  loggerHandler.Info("Setup ini file parser");
  auto systemSetup = Utils::System::System(
    &loggerHandler,
    iniFilePath);

#ifdef SETUP_NETWORK
  // Setup network
  loggerHandler.Info("Setup network");
  if (!systemSetup.SetupNetwork()) {
    loggerHandler.Error("Failed to setup network");
    return EXIT_FAILURE;
  }
#endif
  // Create workload balance handler
  loggerHandler.Info("Start workload balance handler");
  auto workloadBalanceHandler = WorkloadBalancing::WorkloadBalanceHandler(
    &loggerHandler);
  // Create module handler
  loggerHandler.Info("Create module handler");
  auto moduleHandler = SystemModules::Framework::Components::ModuleHandler(
    &loggerHandler,
    "Scu",
    std::chrono::microseconds(UPDATE_INTERVAL_us));
  // Add group alert severities
  moduleHandler.AddGroupAlertSeverity(Types::ValueGroup::Enum::SUPPLY);
  moduleHandler.AddGroupAlertSeverity(Types::ValueGroup::Enum::SPEED);
  moduleHandler.AddGroupAlertSeverity(Types::ValueGroup::Enum::TEMPERATURE);
  moduleHandler.AddGroupAlertSeverity(Types::ValueGroup::Enum::NONE);
  // Crate message handler
  loggerHandler.Info("Create message handler");
  auto messageHandler = Messages::MessageHandler(&loggerHandler);
  // Create client handler
  loggerHandler.Info("Create client handler");
  auto clientHandler = Communication::ClientHandler(&loggerHandler, ip);
  // Create supported protocols
  auto protocolFBIS = Communication::Protocols::ProtocolFbisNetwork(&loggerHandler);
  auto protocolGenericTerminal = Communication::Protocols::ProtocolJson(&loggerHandler);
  auto protocolArbitration = Communication::Protocols::ProtocolArbitration(&loggerHandler);
  // Create lock handler
  loggerHandler.Info("Create lock handler");
  auto lockHandler = ResourceSharing::LockHandler(
    &loggerHandler,
    ResourceSharing::Address(ip),
    &protocolArbitration,
    std::chrono::milliseconds(10));
  // Set client handler reference
  lockHandler.SetClientHandler(&clientHandler);
  // Add sites to lock handler
  loggerHandler.Info("Add sites to lock handler");
  lockHandler.AddSite(ResourceSharing::Address(ipSite));
  // Set lock handler
  moduleHandler.SetLockHandler(&lockHandler);
  workloadBalanceHandler.SetLockHandler(&lockHandler);
  // Setup update handler
  if (!workloadBalanceHandler.AddUpdateHandler(
    Types::Affiliation::Enum::EXCLUSIVE,
    std::chrono::milliseconds(100))) {
    return EXIT_FAILURE;
  }
  if (!workloadBalanceHandler.AddUpdateHandler(
    Types::Affiliation::Enum::SHARED,
    std::chrono::milliseconds(100))) {
    return EXIT_FAILURE;
  }
  // Create serializer
  auto serializer = std::make_unique<SystemModules::Framework::Components::ModuleInternal>(
    &loggerHandler,
    &configuration,
    "Serializer",
    &workloadBalanceHandler,
    Types::ModuleType::Enum::SCU_SER);
  if (!moduleHandler.AddModule(std::move(serializer))) {
    loggerHandler.Error("Failed to add serializer to module handler");
    return EXIT_FAILURE;
  }
  // Create and add power units
  /*
  auto powerSupply = std::make_unique<SystemModules::Framework::Components::ModuleInternal>(
    &loggerHandler,
    &configuration,
    "PowerSupply",
    Types::ModuleType::Enum::SCU_PWSUP);
  if (!moduleHandler.AddModule(std::move(powerSupply))) {
    loggerHandler.Error("Failed to add fan unit to module handler");
    return EXIT_FAILURE;
  }
  */
  // Create and add fan unit
  auto fanUnit = std::make_unique<SystemModules::Framework::Components::ModuleInternal>(
      &loggerHandler,
      &configuration,
      "FanUnit",
      &workloadBalanceHandler,
      Types::ModuleType::Enum::SCU_FU);
  if (!moduleHandler.AddModule(std::move(fanUnit))) {
    loggerHandler.Error("Failed to add fan unit to module handler");
    return EXIT_FAILURE;
  }
  // Create and add mezzanine cards
  loggerHandler.Info("Create and add mezzanine cards");
  for (uint32_t i = 0; i < 12; ++i) {
    auto mezzanineCard = std::make_unique<SystemModules::Framework::Components::ModuleMezzanineCard>(
      &loggerHandler,
      &configuration,
      configuration.Common(),
      configuration.Slots().at(i),
      &workloadBalanceHandler);
    if (!moduleHandler.AddModule(std::move(mezzanineCard))) {
      loggerHandler.Error("Failed to add mezzanine card to module handler");
      return EXIT_FAILURE;
    }
  }
  // Initialize message handler
  loggerHandler.Info("Initialize message handler");
  if (!messageHandler.Initialize(&moduleHandler, &clientHandler)) {
    loggerHandler.Error("Failed to initialize message handler");
    return EXIT_FAILURE;
  }
  // Add client listeners
  loggerHandler.Info("Add client listeners");
  uint16_t fbisPort;
  if (!systemSetup.GetFbisPort(&fbisPort)) {
    loggerHandler.Error("Failed to parse fbis port from config file");
    return EXIT_FAILURE;
  }
  if (!clientHandler.AddConnectionListener(
    std::make_unique<Communication::ConnectionListener::ConnectionListenerTCP>(
      &loggerHandler,
      &messageHandler,
      &protocolFBIS,
      fbisPort))) {
    loggerHandler.Error("Failed to add fbis connection listener to client handler");
    return EXIT_FAILURE;
  }
  uint16_t jsonPort;
  if (!systemSetup.GetJsonPort(&jsonPort)) {
    loggerHandler.Error("Failed to parse json port from config file");
    return EXIT_FAILURE;
  }
  if (!clientHandler.AddConnectionListener(
    std::make_unique<Communication::ConnectionListener::ConnectionListenerTCP>(
      &loggerHandler,
      &messageHandler,
      &protocolGenericTerminal,
      jsonPort))) {
    loggerHandler.Error("Failed to add generic terminal connection listener to client handler");
    return EXIT_FAILURE;
  }
  uint16_t arbitrationPort;
  if (!systemSetup.GetArbitrationPort(&arbitrationPort)) {
    loggerHandler.Error("Failed to parse fbis port from config file");
    return EXIT_FAILURE;
  }
  if (!clientHandler.AddConnectionListener(
    std::make_unique<Communication::ConnectionListener::ConnectionListenerTCP>(
      &loggerHandler,
      &lockHandler,
      &protocolArbitration,
      arbitrationPort))) {
    loggerHandler.Error("Failed to add arbitration connection listener to client handler");
    return EXIT_FAILURE;
  }
  // Initialize client handler
  loggerHandler.Info("Start client handler");
  if (!clientHandler.Start()) {
    return EXIT_FAILURE;
  }
  // Run lock handler
  loggerHandler.Info("Start lock handler");
  if (!lockHandler.Start()) {
    return EXIT_FAILURE;
  }
  // Make sure, that the lock handler could connect to the other sites before running the module handler
  std::this_thread::sleep_for(std::chrono::seconds(5));
  // Run workload balance handler
  loggerHandler.Info("Start workload balance handler");
  if (!workloadBalanceHandler.Start()) {
    return EXIT_FAILURE;
  }
  // Run module handler
  loggerHandler.Info("Start module handler");
  if (!moduleHandler.Start()) {
    return EXIT_FAILURE;
  }
  // Run application for predefined time for tests
  // std::this_thread::sleep_for(std::chrono::seconds(20));
  // Observe handler states
  while (moduleHandler.IsRunning() &&
         workloadBalanceHandler.IsRunning() &&
         lockHandler.IsRunning() &&
         clientHandler.IsRunning()) {
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
  }
  // Stop module handler
  loggerHandler.Info("Stop module handler");
  moduleHandler.Stop();
  // Stop workload balance handler
  loggerHandler.Info("Stop workload balance handler");
  workloadBalanceHandler.Stop();
  // Stop lock handler
  loggerHandler.Info("Stop lock handler");
  lockHandler.Stop();
  // Stop client handler
  loggerHandler.Info("Stop client handler");
  clientHandler.Stop();
  // Exit program
  return EXIT_SUCCESS;
}
