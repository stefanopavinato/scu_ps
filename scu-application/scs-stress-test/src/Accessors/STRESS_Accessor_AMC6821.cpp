/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <iostream>
#include <Logger/LoggerDummy.h>
#include <SystemModules/Framework/Accessors/AccessorFile.h>
#include "STRESS_Accessor_AMC6821.h"

#define PATH "/sys/bus/i2c/devices/0-0071/channel-4/26-0018/hwmon/hwmon1/pwm1"

void STRESS_Accessor_AMC6821::Read(const std::chrono::seconds &seconds) {
  std::cout << "Stress AMC6821: Start" << std::endl;
  auto logger = Logger::LoggerDummy();
  auto accessor = SystemModules::Framework::Accessors::AccessorFile<uint8_t, uint8_t, std::string>(
    &logger,
    PATH,
    1,
    1);
  accessor.Initialize();

  uint8_t value;
  int32_t iterations = 0;
  int32_t failures = 0;
  auto stop = std::chrono::steady_clock::now() + seconds;
  while (std::chrono::steady_clock::now() < stop) {
    iterations++;
    if (!accessor.Read(&value)) {
      failures++;
    }
  }

  (void) accessor.DeInitialize();
  std::cout << "Stress AMC6821: Stop" << std::endl;
  std::cout << "Failures: " << failures << std::endl;
  std::cout << "Iterations: " << iterations << std::endl;
}
