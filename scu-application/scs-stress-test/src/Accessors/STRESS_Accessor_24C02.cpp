/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <fcntl.h>
#include <unistd.h>

#include <iostream>
#include <Logger/LoggerDummy.h>
#include <SystemModules/Framework/Components/I2CDevice24C02.h>
#include <SystemModules/Framework/Accessors/AccessorEEProm.h>
#include "STRESS_Accessor_24C02.h"

#define ADDRESS 0x50
#define PATH "/sys/bus/i2c/devices/0-0071/channel-4"
#define POSITION 4
#define LENGTH 16

void STRESS_Accessor_24C02::Read(const std::chrono::seconds &seconds) {
  std::cout << "Stress 24C02: Start" << std::endl;
  auto logger = Logger::LoggerDummy();
  auto device = SystemModules::Framework::Components::I2CDevice24C02(
    &logger,
    "name",
    "description",
    ADDRESS,
    PATH);

  auto accessor = SystemModules::Framework::Accessors::AccessorEEProm<std::string>(
    &logger,
    device.GetDevicePath(),
    POSITION,
    LENGTH,
    "",
    "");
  accessor.Initialize();

  std::string value;
  int32_t iterations = 0;
  int32_t failures = 0;
  auto stop = std::chrono::steady_clock::now() + seconds;
  while (std::chrono::steady_clock::now() < stop) {
    iterations++;
    if (!accessor.Read(&value)) {
      failures++;
    }
  }

  (void) accessor.DeInitialize();
  (void) device.DeInitialize();
  std::cout << "Stress 24C02: Stop" << std::endl;
  std::cout << "Failures: " << failures << std::endl;
  std::cout << "Iterations: " << iterations << std::endl;
}

void STRESS_Accessor_24C02::Write(const std::chrono::seconds &seconds) {
  std::cout << "Stress 24C02: Start" << std::endl;
  auto logger = Logger::LoggerDummy();
  auto device = SystemModules::Framework::Components::I2CDevice24C02(
    &logger,
    "name",
    "description",
    ADDRESS,
    PATH);

  auto accessor = SystemModules::Framework::Accessors::AccessorEEProm<std::string>(
    &logger,
    device.GetDevicePath(),
    POSITION,
    LENGTH,
    "",
    "");
  accessor.Initialize();

  std::string value = "0123456789ABCDEF";
  int32_t iterations = 0;
  int32_t failures = 0;
  auto stop = std::chrono::steady_clock::now() + seconds;
  while (std::chrono::steady_clock::now() < stop) {
    iterations++;
    if (!accessor.Write(value)) {
      failures++;
    }
  }

  (void) accessor.DeInitialize();
  (void) device.DeInitialize();
  std::cout << "Stress 24C02: Stop" << std::endl;
  std::cout << "Failures: " << failures << std::endl;
  std::cout << "Iterations: " << iterations << std::endl;
}

void STRESS_Accessor_24C02::ReadFile(const std::chrono::seconds &seconds) {
  uint32_t retVal;
  auto file = fopen(std::string(PATH).append("/26-0050/eeprom").c_str(), "rb");
  if (file != nullptr) {
    fseek(file, 0, SEEK_SET);
    fread(&retVal, sizeof(uint32_t), 1, file);
    fclose(file);
  }
}

void STRESS_Accessor_24C02::WriteP(const std::chrono::seconds &seconds) {
  int device;
  if ((device = open(std::string(PATH).append("/26-0050/eeprom").c_str(), O_RDWR)) < 0) {
    perror("Can not open I2C Bus");
    exit(1);
  }

  int i;
  uint8_t data[16];

  for (i=0; i < 1; i++) {
    data[0]= (unsigned char) (1);  // Data Byte
    data[1]= (unsigned char) (2);  // Data Byte
    data[2]= (unsigned char) (3);  // Data Byte
    data[3]= (unsigned char) (4);  // Data Byte
    data[4]= (unsigned char) (5);  // Data Byte
    data[5]= (unsigned char) (6);  // Data Byte
    data[6]= (unsigned char) (7);  // Data Byte
    data[7]= (unsigned char) (8);  // Data Byte
    data[8]= (unsigned char) (9);  // Data Byte
    data[9]= (unsigned char) (10);  // Data Byte
    data[10]= (unsigned char) (11);  // Data Byte
    data[11]= (unsigned char) (12);  // Data Byte
    data[12]= (unsigned char) (13);  // Data Byte
    data[13]= (unsigned char) (14);  // Data Byte
    data[14]= (unsigned char) (15);  // Data Byte
    data[15]= (unsigned char) (16);  // Data Byte

//  Page-Size is configured in device tree
//    https://elixir.bootlin.com/linux/v4.0/source/Documentation/devicetree/bindings/eeprom.txt
//    ioctl(device, I2C_SLAVE, 0x71);

    pwrite(device, data, sizeof(data), i * 8 + 12);

    printf("Out : %d\n", i);
  }
}
// Register the function as benchmark
// BENCHMARK(BM_Device_24C02_Write);
