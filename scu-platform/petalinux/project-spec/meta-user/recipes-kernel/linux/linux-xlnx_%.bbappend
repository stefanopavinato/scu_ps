SRC_URI += "file://user_2019-05-15-14-23-00.cfg \
            file://user_2019-07-01-13-37-00.cfg \
            file://user_2019-08-19-17-07-00.cfg \
            file://user_2019-08-26-16-22-00.cfg \
            file://user_2019-09-10-17-20-00.cfg \
            file://user_2019-09-12-08-54-00.cfg \
            file://user_2019-09-26-11-00-00.cfg \
            file://user_2019-09-30-14-10-00.cfg \
            file://user_2019-09-30-15-25-00.cfg \
            file://user_2019-09-30-17-26-00.cfg \
            file://user_2019-10-01-15-33-00.cfg \
            file://user_2019-10-02-11-28-00.cfg \
            file://user_2019-10-14-16-36-00.cfg \
            file://user_2019-10-21-14-54-00.cfg \
            file://user_2019-10-21-16-20-00.cfg \
            file://user_2019-10-31-16-05-00.cfg \
            file://user_2019-11-04-10-14-00.cfg \
            file://user_2019-11-06-12-09-00.cfg \
            file://user_2019-11-11-10-40-00.cfg \
            file://user_2019-11-12-10-26-00.cfg \
            file://user_2019-11-12-13-15-00.cfg \
            file://user_2019-11-12-14-59-00.cfg \
            file://user_2019-11-12-16-13-00.cfg \
            file://user_2019-11-12-17-43-00.cfg \
            file://user_2019-11-12-17-59-00.cfg \
            file://user_2019-11-13-09-31-00.cfg \
            file://user_2019-11-14-12-33-00.cfg \
            file://user_2019-11-18-14-25-00.cfg \
            file://user_2019-11-18-15-33-00.cfg \
            file://user_2019-11-18-16-46-00.cfg \
            file://user_2019-12-10-15-22-00.cfg \
            file://user_2020-02-19-16-12-00.cfg \
            file://user_2020-03-19-13-55-00.cfg \
            file://user_2020-03-30-09-47-00.cfg \
            file://user_2020-03-31-22-38-00.cfg \
            file://user_2020-04-01-16-07-00.cfg \
            file://user_2020-04-06-16-49-00.cfg \
            file://user_2020-04-14-15-43-00.cfg \
            file://user_2020-04-16-10-44-00.cfg \
            "

FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"
FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI += "file://0001-i2c-cadence-driver-fix.patch"
SRC_URI += "file://receive_buf_roll_over.patch"
SRC_URI += "file://hold_bit_clear_depends_on_receive_count.patch"
SRC_URI += "file://i2c-cadence-retry-once.patch"

