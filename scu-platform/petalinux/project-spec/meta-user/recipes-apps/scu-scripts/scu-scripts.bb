SUMMARY = "scu-scripts"
SECTION = "PETALINUX/apps"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = " \
    file://generic-eeprom.sh \
    file://print_I2C_status.sh \
"

FILES_${PN} += "/home/root/scripts"

do_install_append() {
    install -d ${D}/home/root/scripts
    install -m 0755 ${WORKDIR}/generic-eeprom.sh ${D}/home/root/scripts/generic-eeprom.sh
    install -m 0755 ${WORKDIR}/print_I2C_status.sh ${D}/home/root/scripts/print_I2C_status.sh
}

