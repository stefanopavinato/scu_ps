################################################################################
# Generic EEPROM operations
# Copyright (C) 2019 IOxOS Technologies SA
################################################################################

EEPROM_MANUFACTURER_OFFSET=0x00
EEPROM_MANUFACTURER_LENGTH=32
EEPROM_BOARD_NAME_OFFSET=0x20
EEPROM_BOARD_NAME_LENGTH=16
EEPROM_BOARD_REVISION_OFFSET=0x30
EEPROM_BOARD_REVISION_LENGTH=16
EEPROM_SERIAL_NUMBER_OFFSET=0x40
EEPROM_SERIAL_NUMBER_LENGTH=16
EEPROM_INFO_LENGTH=80
EEPROM_MD5_OFFSET=${EEPROM_INFO_LENGTH}
EEPROM_MD5_LENGTH=32
EEPROM_UID_OFFSET=0xFC
EEPROM_UID_LENGTH=4

clear_eeprom()
{
    USAGE="USAGE: clear_eeprom <eeprom path>"
    [[ "$1" == "" ]] && echo $USAGE && return
    EEPROM_PATH=$1
    dd if=/dev/zero ibs=256 count=1 2>/dev/null | tr "\000" "\377" >${EEPROM_PATH}
}

write_eeprom_SMOD_info()
{
    USAGE="USAGE: write_eeprom_SMOD_info <manufacturer> <board name> <board revision> <serial number> <eeprom path>"
    [[ "$5" == "" ]] && echo $USAGE && return
    MANUFACTURER=$(_pad_string_to_length "$1" ${EEPROM_MANUFACTURER_LENGTH})
    BOARD_NAME=$(_pad_string_to_length "$2" ${EEPROM_BOARD_NAME_LENGTH})
    BOARD_REVISION=$(_pad_string_to_length "$3" ${EEPROM_BOARD_REVISION_LENGTH})
    SERIAL_NUMBER=$(_pad_string_to_length "$4" ${EEPROM_SERIAL_NUMBER_LENGTH})
    EEPROM_PATH=$5
    BOARD_INFO="${MANUFACTURER}${BOARD_NAME}${BOARD_REVISION}${SERIAL_NUMBER}"
    echo -n "${BOARD_INFO}" >${EEPROM_PATH}
    echo -n "${BOARD_INFO}" | md5sum | head -c ${EEPROM_MD5_LENGTH} | xxd -r -p >/tmp/md5
    dd if=/tmp/md5 of=${EEPROM_PATH} bs=1 seek=${EEPROM_MD5_OFFSET} >/dev/null 2>&1
}

load_eeprom_SMOD_info()
{
    USAGE="USAGE: load_eeprom_SMOD_info <eeprom path> <info directory path>"
    [[ "$2" == "" ]] && echo $USAGE && return
    EEPROM_PATH=$1
    INFO_DIR_PATH=$2
    EEPROM_INFO=$(head -c ${EEPROM_INFO_LENGTH} ${EEPROM_PATH})
    echo -n "${EEPROM_INFO}" | md5sum | head -c ${EEPROM_MD5_LENGTH} | xxd -r -p >/tmp/md5_expected
    dd if=${EEPROM_PATH} of=/tmp/md5_stored bs=1 count=16 skip=${EEPROM_MD5_OFFSET} >/dev/null 2>&1
    COMPARE=$(cmp /tmp/md5_expected /tmp/md5_stored)
    mkdir -p ${INFO_DIR_PATH}
    if [[ "$COMPARE" != "" ]]; then
#        echo "load_eeprom_info: Invalid EEPROM contents (bad MD5 hash) [$@]"
        MANUFACTURER="<invalid>"
        BOARD_NAME="<invalid>"
        BOARD_REVISION="<invalid>"
        SERIAL_NUMBER="<invalid>"
    else
        MANUFACTURER=${EEPROM_INFO:${EEPROM_MANUFACTURER_OFFSET}:${EEPROM_MANUFACTURER_LENGTH}}
        BOARD_NAME=${EEPROM_INFO:${EEPROM_BOARD_NAME_OFFSET}:${EEPROM_BOARD_NAME_LENGTH}}
        BOARD_REVISION=${EEPROM_INFO:${EEPROM_BOARD_REVISION_OFFSET}:${EEPROM_BOARD_REVISION_LENGTH}}
        SERIAL_NUMBER=${EEPROM_INFO:${EEPROM_SERIAL_NUMBER_OFFSET}:${EEPROM_SERIAL_NUMBER_LENGTH}}
    fi
    echo -n $MANUFACTURER >${INFO_DIR_PATH}/manufacturer
    echo -n $BOARD_NAME >${INFO_DIR_PATH}//board_name
    echo -n $BOARD_REVISION >${INFO_DIR_PATH}/board_revision
    echo -n $SERIAL_NUMBER >${INFO_DIR_PATH}/serial_number
    EEPROM_UID=$(xxd -s ${EEPROM_UID_OFFSET} -l ${EEPROM_UID_LENGTH} -p -g 4 ${EEPROM_PATH})
    echo -n $EEPROM_UID >${INFO_DIR_PATH}/uid
}

print_eeprom_SMOD_info()
{
    USAGE="USAGE: print_eeprom_SMOD_info <info directory path>"
    [[ "$1" == "" ]] && echo $USAGE && return
    INFO_DIR_PATH=$1
    if [[ -f ${INFO_DIR_PATH}/manufacturer ]]; then
        echo "Manufacturer:   " $(cat ${INFO_DIR_PATH}/manufacturer)
        echo "Board name:     " $(cat ${INFO_DIR_PATH}/board_name)
        echo "Board revision: " $(cat ${INFO_DIR_PATH}/board_revision)
        echo "Serial number:  " $(cat ${INFO_DIR_PATH}/serial_number)
        echo "Unique ID:      " $(cat ${INFO_DIR_PATH}/uid)
    else
        echo "Info not loaded"
    fi
}

function _pad_string_to_length()
{
    STRING="$1"
    LENGTH="$2"
    PADDING=$(printf '%*s' $LENGTH)
    printf '%s%*.*s' "$STRING" 0 $(($LENGTH - ${#STRING} )) "$PADDING"
}

EEPROM_OPERATINGHOURS_OFFSET=0x60
EEPROM_OPERATINGHOURS_LENGTH=4

write_eeprom_mc_operatinghours()
{
    USAGE="USAGE: write_eeprom_mc_operatinghours <mc_operatinghours (hex, unit 15 minutes)> <eeprom path>"
    [[ "$2" == "" ]] && echo $USAGE && return
    MC_OPERATINGHOURS=$1
	OPERATINGHOURS_OFFSET=$((${EEPROM_OPERATINGHOURS_OFFSET}/${EEPROM_OPERATINGHOURS_LENGTH}))
    EEPROM_PATH=$2
	dd if=/dev/zero of=${EEPROM_PATH} bs=${EEPROM_OPERATINGHOURS_LENGTH} count=1 seek=${OPERATINGHOURS_OFFSET} >/dev/null 2>&1
	echo -n ${MC_OPERATINGHOURS} | xxd -r -p >/tmp/oph
    dd if=/tmp/oph of=${EEPROM_PATH} bs=${EEPROM_OPERATINGHOURS_LENGTH} seek=${OPERATINGHOURS_OFFSET} >/dev/null 2>&1
}

write_eeprom_fanUnit_operatinghours()
{
    USAGE="USAGE: write_eeprom_fanUnit_operatinghours <operatinghours (hex, unit 15 minutes)> <selection: 0=FU, 1=FanA, 2=FanB> <eeprom path>"
    [[ "$3" == "" ]] && echo $USAGE && return
    MC_OPERATINGHOURS=$1
	OPERATINGHOURS_OFFSET=$((${EEPROM_OPERATINGHOURS_OFFSET}/${EEPROM_OPERATINGHOURS_LENGTH}+$2))
    EEPROM_PATH=$3
	dd if=/dev/zero of=${EEPROM_PATH} bs=${EEPROM_OPERATINGHOURS_LENGTH} count=1 seek=${OPERATINGHOURS_OFFSET} >/dev/null 2>&1
	echo -n ${MC_OPERATINGHOURS} | xxd -r -p >/tmp/oph
    dd if=/tmp/oph of=${EEPROM_PATH} bs=${EEPROM_OPERATINGHOURS_LENGTH} seek=${OPERATINGHOURS_OFFSET} >/dev/null 2>&1
}