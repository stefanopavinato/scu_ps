I2C0_ADDRESS_OFFSET=0xFF020000
I2C1_ADDRESS_OFFSET=0xFF030000

print_I2C_status()
{
    USAGE="USAGE: print_I2C_status <I2C Index> (I2C Index= {0,1})"
    [[ "$1" == "" ]] && echo $USAGE && return
    I2C_INDEX=$1 

    if [[ $I2C_INDEX == "0" ]]; then
        echo "Reading I2C Status of I2C with Index: 0"
	base=I2C0_ADDRESS_OFFSET
    elif [[ $I2C_INDEX=="1" ]]; then
        echo "Reading I2C Status of I2C with Index: 1"
	base=I2C1_ADDRESS_OFFSET
    else
        echo $USAGE && return
    fi

  
    
    _print_register "Configuration  Control_Reg0            " ${base} 0x00
    _print_register "Data           I2C_address_reg0        " ${base} 0x08
    _print_register "               I2C_data_reg0           " ${base} 0x0C
    _print_register "               Transfer_size_register0 " ${base} 0x14
    _print_register "               Slave_mon_pause_reg0    " ${base} 0x18
    _print_register "               Time_out_reg0           " ${base} 0x1C
    _print_register "               Status_reg0             " ${base} 0x04
    _print_register "               interrupt_mask_reg0     " ${base} 0x10
    _print_register "               interrupt_enable_reg0   " ${base} 0x20
    _print_register "               interrupt_enable_reg0   " ${base} 0x24
    _print_register "               interrupt_disable_reg0  " ${base} 0x28
}



function _print_register
{
   # param1 = string
   # param2 = register base
   # param3 = register offset

    addr=$(($2+$3))
    val=`devmem ${addr}`
    echo "$1 : ${val}"
}

