SUMMARY = "scu-application"
SECTION = "PETALINUX/apps"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = " \
    svn://swords.zhaw.ch/svn/iamp_ess_fbis_software/;protocol="https";module="${BRANCH}";user="PetalinuxBuildUser";pswd="PetalinuxIAMPESS" \
    file://scu-application \
    file://ini-file \
"

BRANCH ?= "trunk/scu-application"
PV = "1.0+svn${SRCPV}"
SRCREV ?= "766"

S = "${WORKDIR}/${BRANCH}"

inherit pkgconfig cmake update-rc.d

OECMAKE_FIND_ROOT_PATH_MODE_PROGRAM = "BOTH"

EXTRA_OECMAKE = "-DREVISION="${SRCREV}" \
                 -DBRANCH="${BRANCH}" \
                 -DUSE_SETUP_NETWORK=ON \
                 -DENABLE_TESTING=OFF \
                 -DPETALINUX_BUILD=ON \
                "

INITSCRIPT_NAME = "scu-application"

INI_FILE_NAME = "ini-file"

DEPENDS = " \
    jsoncpp \
    scu-registers \
    hdc2010 \
    minini \
"

do_install_append() {
    install -d ${D}${sysconfdir}/init.d
    install -m 0755 ${WORKDIR}/${INITSCRIPT_NAME} ${D}/${sysconfdir}/init.d/${INITSCRIPT_NAME}
    install -d ${D}/etc/default/scs.d
    install -m 0755 ${WORKDIR}/${INI_FILE_NAME} ${D}/etc/default/scs.d/${INI_FILE_NAME}
}

