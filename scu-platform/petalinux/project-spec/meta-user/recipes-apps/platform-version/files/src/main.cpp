/// \copyright  2020 Sotronik GmbH

#include <iostream>
#include "version.h"

int main(int argc, char* argv[]) {
  (void) argc;
  (void) argv;
  std::cout << "Revision:   " << REVISION << std::endl;
  std::cout << "Branch:     " << BRANCH << std::endl;
  std::cout << "Build date: " << BUILD_DATE << std::endl;
  std::cout << "Build time: " << BUILD_TIME << std::endl;
#ifdef VERSION_IS_DIRTY
  std::cout << "Version is dirty!" << std::endl;
#endif
  return EXIT_SUCCESS;
}

