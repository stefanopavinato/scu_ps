#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/interrupt.h>
#include <linux/irq.h>
#include <asm/io.h>
#include <linux/fs.h>		
#include <linux/uaccess.h>	
#include <linux/signal.h>	
#include <linux/cdev.h>         
#include <asm/irq.h>
#include <linux/sched.h>
#include <linux/platform_device.h>
#include <linux/of.h>
#include <linux/of_device.h>
#include <linux/of_address.h>
#include <linux/of_irq.h>
//#include <stdlib.h> //not available in driver
 
MODULE_LICENSE("GPL");

#define WRITE_IOCTL _IOW('a','a', unsigned long*)    // ioctl macro
#define READ_IOCTL  _IOR('a','b', unsigned long*)    // ioctl macro
#define READ_ADDRESS_IOCTL  _IOWR('a','c', unsigned long*)    // ioctl macro

#define FIRST_VAL   0x0 
#define BUF_LEN 80			   // Max buffer length
#define SUCCESS 0			   // Success return value

#define SCU_REGISTERS_NAME        "scu-registers"  // Name of the scu registers device        
#define SCU_REGISTERS_COUNT                   64   // Maximum number of timer devices 
#define SCU_REGISTERS_MINOR_START              0   // First minor number                   

struct scu_register_device {
  struct device *dev_ctl;                  // Timer control device                     
};

struct scu_register {
  struct cdev cdev;
  dev_t dev_id;
  struct scu_register_device *reg;
};

struct scu_register scu_register; 			   // Driver main data structure

void *pSCU_REG_BASE;				   // Pointer to base register of scu_register
//static int Device_Open = 0;		   // Device status
static char msg[BUF_LEN];			   // The msg to send to the device
static struct class *sysfs_class;	   // Sysfs class
struct resource res; 				   // Device resource

static int scu_register_probe (struct platform_device *pdev); 
static int scu_register_remove (struct platform_device *pdev);
 
static const struct of_device_id scu_register_match[] = {
    {
		.compatible = "scu-registers",
    },
};

static struct platform_driver SCU_REGISTERS_driver= {
	.driver		= {
		.name	= SCU_REGISTERS_NAME,
		.owner	= THIS_MODULE,
		.of_match_table = of_match_ptr(scu_register_match),
	},
	.probe		= scu_register_probe,
	.remove		= scu_register_remove,
};

static unsigned long address = FIRST_VAL;

// -------------------------------------------------------------------------------------------------------------------
// ioctl routine to do access from user space to kernel space
// -------------------------------------------------------------------------------------------------------------------
static long scu_register_ioctl(struct file *file, unsigned int cmd, unsigned long arg){
	int retval = 0;
	unsigned long value;
	
	switch(cmd) {
		case WRITE_IOCTL:
			if (copy_from_user(&value ,(unsigned long*) arg, sizeof(value))!= 0) {    // Read command from user space
				printk("ioctl - WRITE - ERROR\n");
				return -EFAULT;										// Return error if it failed
			}
		 
//            printk("ioctl - write address is 0x%lx\n", address);
//            printk("ioctl - write value is 0x%lx\n", value);
			iowrite32(value, pSCU_REG_BASE + address);		// address must be set before with READ_ADDRESS_IOCTL
	
			return SUCCESS;									// Return 0 to application
            break;
            
        case READ_IOCTL:
			value = ioread32(pSCU_REG_BASE + address);
			if (copy_to_user((unsigned long*) arg, &value, sizeof(value)) != 0) {	// Send register value
				printk("ioctl - READ - ERROR\n");
				retval = -EFAULT;
			}
			else { 
				retval = 0;
			}
        break;
        
        case READ_ADDRESS_IOCTL:
			if (copy_from_user(&address ,(unsigned long*) arg, sizeof(address))!= 0) {    // Read command from user space
				return -EFAULT;										// Return error if it failed
			}
            address &= 0xfffffffc; //align address
//            printk("ioctl - read address is %lx\n", address);
//            if (address >= 0x1000) { 
            if (address >= resource_size(&res)) { 
                printk("ioctl - invalid address\n");
				return -EFAULT;										// Return error if it failed
            }
            
			value = ioread32(pSCU_REG_BASE + address);
			if (copy_to_user((unsigned long*) arg, &value, sizeof(value)) != 0) {	// Send register value
				printk("ioctl - READ Address - ERROR\n");
				retval = -EFAULT;
			}
			else { 
				retval = 0;
			}
        break;
        }
        
    return retval;
}

// -------------------------------------------------------------------------------------------------------------------
// write routine (called when write() is used in user-space)
// -------------------------------------------------------------------------------------------------------------------
ssize_t scu_register_write(struct file *flip, const char *buf, size_t length, loff_t *offset) {
	unsigned long scu_registerval;

    memset(msg, 0, sizeof(msg));
	if (copy_from_user(msg, buf, length) != 0) {			// Read buffer from user space
		return -EFAULT;						// Return error if it failed
	}
//    scu_registerval = (unsigned long)atoi(msg);
    printk("from kernel msg: %s\n",msg);
    sscanf(msg, "%lx", &scu_registerval);
    printk("from kernel scu-registers: 0x%lx\n", scu_registerval);

	iowrite32(scu_registerval, pSCU_REG_BASE + address);	// 

    return SUCCESS;
}

// -------------------------------------------------------------------------------------------------------------------
// read routine (called when read() is used in user-space)
// -------------------------------------------------------------------------------------------------------------------
ssize_t scu_register_read(struct file *flip, char *buf, size_t length, loff_t *offset) {
	unsigned long scu_registerval;
       
	scu_registerval = ioread32(pSCU_REG_BASE + address);
    printk("from kernel scu-registers: %lx\n", scu_registerval);
	memset(msg, 0, sizeof(msg));
	sprintf(msg, "%lx", scu_registerval);
	if (copy_to_user(buf, &msg, length) != 0) {	// Send counter value
		return -EFAULT;
	}
	else { 
		return 0;
	}
}

// -------------------------------------------------------------------------------------------------------------------
// open routine (called when a device opens /dev/scu_register)
// -------------------------------------------------------------------------------------------------------------------
static int scu_register_open(struct inode *inode, struct file *file) {
	try_module_get(THIS_MODULE);
	return 0;
}

// -------------------------------------------------------------------------------------------------------------------
// close routine (called whne a device closes /dev/scu_register)
// -------------------------------------------------------------------------------------------------------------------
static int scu_register_close(struct inode *inode, struct file *file) {
	module_put(THIS_MODULE);
	return 0;
}

// -------------------------------------------------------------------------------------------------------------------
// device init and file operations
// -------------------------------------------------------------------------------------------------------------------
struct file_operations scu_register_fops = {
	.owner   = THIS_MODULE,
	.read    = scu_register_read,	// Read()
	.write   = scu_register_write,	// Write()
	.open    = scu_register_open,	// Open()
	.unlocked_ioctl = scu_register_ioctl,  // ioctl()
	.release = scu_register_close,	// Close()
};

// -------------------------------------------------------------------------------------------------------------------
// init module      
// -------------------------------------------------------------------------------------------------------------------
static int __init mod_init(void) {
	printk("scu_register: Module init\n");
	return platform_driver_probe(&SCU_REGISTERS_driver, scu_register_probe);
} 

// -------------------------------------------------------------------------------------------------------------------
// Probe function
// -------------------------------------------------------------------------------------------------------------------
static int scu_register_probe (struct platform_device *pdev) {  
  	int retval = 0;
	int rc = 0;
	dev_t scu_register_dev_id;
	struct scu_register_device *registers;
	const struct of_device_id *match;
	
	printk("scu_register: Probing scu_register driver \n");
	
	// *************************************************************************************************
	// Check if the device if found in the tree
	match = of_match_device(scu_register_match, &pdev->dev);
	if (!match) {
		printk("scu_register: No scu_register device found in tree \n");
		return -ENODEV;
	}
	else {
	  printk("scu_register: Device found in tree \n");
	}
	
	// *************************************************************************************************
	// Map device
	rc = of_address_to_resource(pdev->dev.of_node, 0, &res);
	if (rc) {
	      printk("scu_register: Address to resource failed \n");
	}

	if  (!request_mem_region(res.start, resource_size(&res), SCU_REGISTERS_NAME)) {
	      printk("scu_register: Request map region failed \n");
	}

	pSCU_REG_BASE = of_iomap(pdev->dev.of_node, 0);

	if (!pSCU_REG_BASE) {
	      printk("scu_register: Iomap register failed \n");
	}
	
	printk("scu_register: Base addr %llx, size %llx, virtual register base addr %p\n", res.start, resource_size(&res), pSCU_REG_BASE);
	
	// *************************************************************************************************
	// Device number dynamic allocation 
	retval = alloc_chrdev_region( &scu_register_dev_id, SCU_REGISTERS_MINOR_START, SCU_REGISTERS_COUNT, SCU_REGISTERS_NAME);
	if (retval < 0) {
		printk("scu_register: Error %d cannot allocate device number\n", retval);
		return  (-1);
	}
	else {
		printk("scu_register: Registered with major number:%i \n", MAJOR(scu_register_dev_id));
	}
	scu_register.dev_id = scu_register_dev_id;
	
	// *************************************************************************************************
	// Register driver
	cdev_init(&scu_register.cdev, &scu_register_fops);
	scu_register.cdev.owner = THIS_MODULE;
	scu_register.cdev.ops   = &scu_register_fops;
	retval = cdev_add(&scu_register.cdev, scu_register_dev_id , SCU_REGISTERS_COUNT);
	if(retval) {
		printk("scu_register: Error %d adding device \n", retval);
		return  (-1);
	}
	printk("scu_register: Device added \n");

	// *************************************************************************************************
	// Create sysfs entries - on udev systems this creates the dev files
	sysfs_class = class_create(THIS_MODULE, SCU_REGISTERS_NAME);
	if (IS_ERR(sysfs_class)) {
		printk("scu_register: Error creating device.\n");
		return  (-1);
	}
	printk("scu_register: System class added \n");
	
	// *************************************************************************************************
	// Create scu_register control device in file system
	registers->dev_ctl = device_create(sysfs_class, NULL, scu_register_dev_id, NULL, "scu_register");
	printk("scu_register: Device created \n");
  
  return 0;
}

// -------------------------------------------------------------------------------------------------------------------
// Remove function
// -------------------------------------------------------------------------------------------------------------------
static int scu_register_remove (struct platform_device *pdev) {
	printk("scu_register: Removing module\n");		
	printk("scu_register: Remove done \n");	
  return 0;
}

// -------------------------------------------------------------------------------------------------------------------
// exit module
// -------------------------------------------------------------------------------------------------------------------
static void __exit mod_exit(void) {
	printk("scu_register: Exiting module\n");	
	device_destroy(sysfs_class, scu_register.dev_id);
	class_destroy(sysfs_class);
	cdev_del(&scu_register.cdev);
	unregister_chrdev_region(scu_register.dev_id, SCU_REGISTERS_COUNT);	
	platform_driver_unregister(&SCU_REGISTERS_driver);
	printk("scu_register: Bye !\n");	
}

module_init(mod_init);
module_exit(mod_exit);

MODULE_AUTHOR ("Martin Gassner");
MODULE_DESCRIPTION("Driver for the SCU registers.");
MODULE_LICENSE("GPL v2");
MODULE_ALIAS("custom:scu_register");
