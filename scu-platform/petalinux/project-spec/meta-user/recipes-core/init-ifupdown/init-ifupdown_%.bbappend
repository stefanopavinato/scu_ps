# init-ifupdown_%.bbappend content
  
SRC_URI += " \
        file://myinterfaces \
        file://001fixdns \
        file://resolv.conf.static \
        "

FILESEXTRAPATHS_prepend := "${THISDIR}/files:"
  
RDEPENDS_${PN} += "bash"

# Overwrite interface file with myinterface file in rootfs
do_install_append() {
     install -m 0644 ${WORKDIR}/myinterfaces ${D}${sysconfdir}/network/interfaces
     install -m 0755 ${WORKDIR}/001fixdns ${D}${sysconfdir}/network/if-up.d
     install -m 0644 ${WORKDIR}/resolv.conf.static ${D}${sysconfdir}/resolv.conf.static
}

