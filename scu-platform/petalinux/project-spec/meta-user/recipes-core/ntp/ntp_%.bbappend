# ntp_%.bbappend content
  
SRC_URI += " \
        file://ntp.conf \
        "
  
FILESEXTRAPATHS_prepend := "${THISDIR}/files:"
  
# Overwrite config file
do_install_append() {
     install -m 0644 ${WORKDIR}/ntp.conf ${D}${sysconfdir}/ntp.conf
}

