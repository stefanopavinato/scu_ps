# init-ifupdown_%.bbappend content
  
SRC_URI += " \
        file://syslog-startup.conf \
        "
  
FILESEXTRAPATHS_prepend := "${THISDIR}/files:"
  
# Overwrite config file
do_install_append() {
     install -m 0644 ${WORKDIR}/syslog-startup.conf ${D}${sysconfdir}/syslog-startup.conf
}

