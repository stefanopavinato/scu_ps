#Note: Mention Each package in individual line
#      cascaded representation with line breaks are not valid in this file.
IMAGE_INSTALL_append = " ntp"
IMAGE_INSTALL_append = " ntp-utils"

IMAGE_INSTALL_append = " peekpoke"
IMAGE_INSTALL_append = " gpio-demo"
IMAGE_INSTALL_append = " xvc-driver"
IMAGE_INSTALL_append = " xvcserver"
IMAGE_INSTALL_append = " rapidjson"
IMAGE_INSTALL_append = " jsoncpp"
IMAGE_INSTALL_append = " minini"
IMAGE_INSTALL_append = " libgpiod"

IMAGE_INSTALL_append = " platform-version"
IMAGE_INSTALL_append = " scu-application"
IMAGE_INSTALL_append = " scu-scripts"
IMAGE_INSTALL_append = " scu-registers"
IMAGE_INSTALL_append = " hdc2010"
