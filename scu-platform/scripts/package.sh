#!/bin/bash
{
    # Source petalinux/yocto
    source /data/Xilinx/PetaLinux/2018.3/settings.sh
    # Define paths
    FSLB="images/linux/zynqmp_fsbl.elf"
    FPGA="project-spec/hw-description/*.bit"
    PMUFW="images/linux/pmufw.elf"
    # Switch to project folder
    cd ../petalinux
    # Get hardware description
    petalinux-package --boot --fsbl ${FSLB} --fpga ${FPGA} --pmufw ${PMUFW} --u-boot --force
    echo "the boot files are located in:"
    echo "/images/linux"
    echo "copy the files 'BOOT.BIN' and 'image.ub' into the boot partition of the SDCard"
}
