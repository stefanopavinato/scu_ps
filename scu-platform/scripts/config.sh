#!/bin/bash
{
    BASE_PATH="/data/Svn/scu-platform/scu-platform/petalinux"
    # Source petalinux/yocto
    source /data/Xilinx/PetaLinux/2018.3/settings.sh
    # Select folder containing the ini-file and hdf-file
    cd /data/Svn/scu-instances/scu-instances
    FOLDER=`zenity --file-selection --directory --title="Select scu-instance folder"`
    echo "Selected folder: ${FOLDER}"
    # Copy ini-file into scu-application recipe
    cp ${FOLDER}/ini-file ${BASE_PATH}/project-spec/meta-user/recipes-apps/scu-application/files/ini-file
    # Copy ntp.conf file to ntp recipe
    cp ${FOLDER}/ntp.conf ${BASE_PATH}/project-spec/meta-user/recipes-core/ntp/files/ntp.conf
    # Copy resolv.conf file to base recipe
    cp ${FOLDER}/resolv.conf.static ${BASE_PATH}/project-spec/meta-user/recipes-core/init-ifupdown/files/resolv.conf.static
    # Switch to project folder
    cd ${BASE_PATH}
    # Get hardware description
    petalinux-config --get-hw-description=${FOLDER}
}
