# insert the new sd-card and locate it using one of the following commands

df -h
lsblk
sudo fdisk -l

# typically the sd-card is on /dev/sdb (for the following description we assume this is the case)

# once the sd-card has been located, unmount the drive drive
sudo umount /dev/sdb1

# in the following we will:
# - delete existing partitions, on new sd-cards there is just one, on used there might be two or more
# - create two partitions BOOT and root

sudo fdisk /dev/sdb
d         #delete partition

n         #new partition
p         #primary
1         #partition number
2048      #first sector
2097151   #last sector

n         #new partition
p         #primary
2         #partition number
2097152   #first sector
15126527  #last sector

w         #write partition

# load new configuration 
sudo partprobe -s /dev/sdb

# format the partitions, BOOT with FAT32, root with EXT4
sudo mkfs.vfat -F32 /dev/sdb1
sudo mkfs.ext4 /dev/sdb2

# label the partitions
sudo mlabel -i /dev/sdb1 ::boot
sudo e2label /dev/sdb2 root

# eject the sd-card and physically remove it
# reinsert the sd-card 