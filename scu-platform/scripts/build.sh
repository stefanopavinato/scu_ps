#!/bin/bash
{
    # Source petalinux/yocto
    source /data/Xilinx/PetaLinux/2018.3/settings.sh
    # Switch to project folder
    cd ../petalinux
    # Build
    petalinux-build
}
