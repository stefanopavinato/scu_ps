#! /bin/bash
{
    if [ "$#" -ne 1 ]; then
        echo "Usage: pass the device path as first argument"
        exit
    fi
    # Delay
    DELAY=1

    # Device
    DEVICE=$1
    DEVICE_PATH=$1
    
    # Partition 1
    PARTITION_1="1"
    PARTITION_1_SIZE="+1G"
    PARTITION_1_TYPE="c"

    # Partition 2
    PARTITION_2="2"

    # sudo partprobe -s ${DEVICE_PATH}

    # Check if device is available
    DEVICE_COUNT=$(lsblk | grep -c "${DEVICE}")
    if [ ${DEVICE_COUNT} -lt 0 ]; then
        echo "Device not found"=$
        exit
    fi

    # Unmount sd card
    sudo umount ${DEVICE_PATH}?*

    # Wipe partitions
    sudo wipefs -a ${DEVICE_PATH}

    sudo partprobe -s ${DEVICE_PATH}

    # Write partition table
    echo "Write partition table of device ${DEVICE}: Start"
    (
        sleep ${DELAY}
        echo n                      # Add a new partition
        sleep ${DELAY}
        echo p                      # Primary partition
        sleep ${DELAY}
        echo ${PARTITION_1}         # Partition number
        sleep ${DELAY}
        echo                        # First sector (Accept default: 1)
        sleep ${DELAY}
        echo ${PARTITION_1_SIZE}
        sleep ${DELAY}
        echo J                      # Delete signatur if there is one
        sleep ${DELAY}
        echo n                      # Add a new partition
        sleep ${DELAY}
        echo p                      # Primary partition
        sleep ${DELAY}
        echo ${PARTITION_2}         # Partition number
        sleep ${DELAY}
        echo                        # First sector (Accept default)
        sleep ${DELAY}
        echo                        # Last sector (Accept default)
        sleep ${DELAY}
        echo J                      # Delete signatur if there is one
        sleep ${DELAY}
        echo a                      # Set bootable flag
        sleep ${DELAY}
        echo ${PARTITION_1}
        sleep ${DELAY}
        echo t                      # Set partition type
        sleep ${DELAY}
        echo ${PARTITION_1}
        sleep ${DELAY}
        echo ${PARTITION_1_TYPE}
        sleep ${DELAY}
        echo p                      # Show partition table
        sleep ${DELAY}
        echo w                      # Write changes
        sleep ${DELAY}
    ) | sudo fdisk ${DEVICE_PATH}
    echo "Write partition table of device ${DEVICE}: Done"
    sleep 20
    # Load new configuration 
    echo "Load new configuration of device ${DEVICE}: Start"
    sudo partprobe -s ${DEVICE_PATH}
    sleep ${DELAY}
    echo "Load new configuration of device ${DEVICE}: Done"

    # Get current setup
    PARTITIONS=($(lsblk -np --output KNAME | grep "mmcblk0" | sed 's/ /\n/g'))

    # Unmount sd card
    sudo umount ${DEVICE_PATH}?*

    # Format the partitions
    sudo mkfs.vfat -F32 ${PARTITIONS[1]} 
    (
        echo j
        sleep ${DELAY}
    ) | sudo mkfs.ext4 ${PARTITIONS[2]}
    
    # Label the partitions
    sudo fatlabel ${PARTITIONS[1]} BOOT
    sudo e2label ${PARTITIONS[2]} root

    echo "All done"
}

